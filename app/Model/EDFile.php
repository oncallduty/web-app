<?php

App::uses( 'EDModel', 'Model' );

class EDFile extends EDModel
{
	public $primaryKey = 'ID';

	public function getFiles( $params )
	{
		if (!is_array( $params ) || !isset( $params['folderID'] )) {
			return null;
		}

		$query = array(
			'endpoint' => 'deposition/getFiles',
			'conditions' => ['folderID' => $params['folderID']]
		);

		if (isset( $params['sortBy'] ) && in_array( $params['sortBy'], ['created','name','ID'] )) {
			$query['conditions']['sortBy'] = $params['sortBy'];
		}

		if (isset( $params['sortDir'] ) && in_array( $params['sortDir'], ['ASC','DESC'] )) {
			$query['conditions']['sortDir'] = $params['sortDir'];
		}

		if (isset( $params['pageSize'] ) && $params['pageSize']) {
			$query['conditions']['pageSize'] = $params['pageSize'];
		}

		if (isset( $params['page'] ) && $params['page']) {
			$query['conditions']['page'] = $params['page'];
		}

//print_r($query);
//print_r($this->getDataSource());
		$results = $this->getDataSource()->read( $this, $this->buildQuery( 'all', $query ) );
//print_r($results);

		$files = null;
		if( isset( $results[$this->alias] ) && $results[$this->alias] ) {
			$result = $results[$this->alias];
			if( isset( $result['files'] ) && is_array( $result['files'] ) && $result['files'] ) {
				foreach ($result['files'] as $fileData) {
					$file = new EDFile();
					$file->set( $fileData );
					$files[$fileData['ID']] = $file;
				}
			}
		}
		return $files;
	}

	public function getFileLink( $params )
	{
		if (!is_array( $params ) || !isset( $params['fileID'] )) {
			return null;
		}

		$results = $this->getDataSource()->prepare( $this, $this->buildQuery( 'all', [
			'endpoint' => 'file/download',
			'conditions' => $params
		] ) );

		return isset( $results['url'] ) ? $results['url'].(isset( $results['data'] ) ? '?data='.$results['data'] : '') : '';
	}

	public function getFile( $fileID, $embed=true )
	{
		if (!$fileID)
		{
			return null;
		}

		$query = [
			'endpoint' => 'file/download',
			'conditions' => [
				'fileID' => $fileID,
				'embed' => $embed,
			],
		];

		$ds = $this->getDataSource();

		try {
			// read() expects results in json format, and will throw an exception for non-json results
			$results = $ds->read( $this, $this->buildQuery( 'all', $query ) );
		} catch (Exception $e) {
			$results = $ds::$lastResponse;
		}

		return array( 'results'=>$results, 'headers'=>$ds::$lastHeaders );
	}

	public function saveFolder( $folderID )
	{
		if (!isset( $folderID )) {
			return null;
		}

		$query = [
			'endpoint' => 'file/saveFolder',
			'conditions' => [
				'folderID' => $folderID,
			],
		];

		$ds = $this->getDataSource();
		$ds->passThrough( $this, $this->buildQuery( 'all', $query ) );
	}

	public function sendFolder( $params )
	{
		if( !is_array( $params ) || (!isset( $params['folderID'] ) && !isset( $params['selectedFileIDs'] )) ) {
			return null;
		}

		$results = $this->getDataSource()->read( $this, $this->buildQuery( 'all', [
			'endpoint' => 'file/sendFolder',
			'conditions' => $params
		] ) );

		return (isset( $results['EDFile'] ) ? $results['EDFile'] : null);
	}

	public function sendFile( $params )
	{
		if (!is_array( $params ) || !isset( $params['fileID'] )) {
			return null;
		}

		$results = $this->getDataSource()->read( $this, $this->buildQuery( 'all', [
			'endpoint' => 'file/sendFile',
			'conditions' => $params
		] ) );

		return (isset( $results['EDFile'] ) ? $results['EDFile'] : null);
	}

	public function sendFileWithAnnotations( $params )
	{
		if (!is_array( $params ) || !isset( $params['fileID'] )) {
			return null;
		}

		$results = $this->getDataSource()->read( $this, $this->buildQuery( 'all', [
			'endpoint' => 'file/sendFileWithAnnotations',
			'conditions' => $params
		] ) );

		return (isset( $results['EDFile'] ) ? $results['EDFile'] : null);
	}

	public function annotate( $params )
	{
		if (!is_array( $params ) || !isset( $params['sourceFileID'] )) {
			return null;
		}

		$query = [
			'endpoint' => 'file/annotate',
			'conditions' => $params
		];

		$results = $this->getDataSource()->read( $this, $this->buildQuery( 'all', $query ) );

		return (isset( $results['EDFile'] ) ? $results['EDFile'] : self::$lastResponse);
	}

	public function getPresentationSource( $depositionID, $embed=true )
	{
		if (!$depositionID)
		{
			return null;
		}

		$query = [
			'endpoint' => 'file/getPresentationSource',
			'conditions' => [
				'depositionID' => $depositionID,
				'embed' => $embed,
			],
		];

		$ds = $this->getDataSource();

		try {
			// read() expects results in json format, and will throw an exception for non-json results
			$results = $ds->read( $this, $this->buildQuery( 'all', $query ) );
		} catch (Exception $e) {
			$results = $ds::$lastResponse;
		}

		return array( 'results'=>$results, 'headers'=>$ds::$lastHeaders );
	}

	public function abortTempFile( $depositionID, $fileID )
	{
		if (!$depositionID || !$fileID)
		{
			return null;
		}

		$results = $this->getDataSource()->read( $this, $this->buildQuery( 'all', [
			'endpoint' => 'file/abortTempFile',
			'conditions' =>  [
				'depositionID' => $depositionID,
				'fileID' => $fileID,
			],
		] ) );

		return (isset( $results['EDFile'] ) ? $results['EDFile'] : null);
	}
}
