<?php

App::uses( 'EDModel', 'Model' );

class EDCase extends EDModel
{
	public $primaryKey = 'ID';

	const STATUS_NOTSTARTED = 'N';
	const STATUS_INPROGRESS = 'I';
	const STATUS_FINISHED = 'F';

	/*
	public $_schema = array(
		'ID' => array(
			'type' => 'integer',
			'length' => 20
		),
		'clientID' => array(
			'type' => 'integer',
			'length' => 20
		),
		'typeID' => array(
			'type' => 'string',
			'length' => 1
		),
		'createdBy' => array(
			'type' => 'integer',
			'length' => 20
		),
		'name' => array(
			'type' => 'string',
			'length' => 255
		),
		'number' => array(
			'type' => 'string',
			'length' => 60
		),
		'created' => array(
			'type' => 'date',
		),
	);
	 */

	/**
	 *
	 * @param array $query
	 * @return null|array(EDCase)
	 */
	public function getCases( $query=null )
	{
		if (!$query || !is_array( $query ) ) {
			$query = [];
		}

		$query['endpoint'] = 'cases/getlist';

		$query = $this->buildQuery( 'all', $query );
		if( is_null( $query ) ) {
			return null;
		}

		$results = $this->getDataSource()->read( $this, $query );

		$cases = array();

		if( isset( $results[$this->alias] ) && $results[$this->alias] ) {
			$result = $results[$this->alias];
			if( isset( $result['cases'] ) && is_array( $result['cases'] ) &&  $result['cases'] ) {
				foreach(  $result['cases'] as $caseData ) {
					$case = new EDCase();
					$case->set( $caseData );
					$cases[$caseData['ID']] = $case;
				}
			}
		}

		return $cases;
	}

	/**
	 *
	 * @param array $query
	 * @param mixed $allowedStatus
	 * @param bool $allowEmpty
	 * @return array(EDCase)
	 */
	public static function getFilteredCases( $query, $allowedStatus = [], $allowEmpty = false ) {
		$caseList = (new EDCase() )->getCases( $query );

		if (!is_array( $allowedStatus )) {
			$allowedStatus = [$allowedStatus];
		}

		$caseResults = [];

		foreach ($caseList as $caseContainer) {
			$case = $caseContainer->data['EDCase'];
			foreach ($case['depositions'] as $depIndex => $deposition) {
				if (!in_array( $deposition['statusID'], $allowedStatus )) {
					unset( $case['depositions'][$depIndex] );
				}
			}

			$case['depositions'] = array_values( $case['depositions'] );
			if (count( $case['depositions'] ) > 0 || $allowEmpty) {
				$caseResults[] = $case;
			}
		}

		return $caseResults;
	}

	/**
	 * @return array(EDCase)
	 */
	public static function getAllCases() {
		$caseList = (new EDCase())->getCases();
		$caseResults = [];
		foreach( $caseList as $caseContainer ) {
			$case = $caseContainer->data['EDCase'];
			$case['depositions'] = array_values( $case['depositions'] );
			$caseResults[] = $case;
		}
		return $caseResults;
	}
}
