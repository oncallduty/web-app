<?php

App::uses( 'HttpSocket', 'Network/Http', 'Model/Datasource/Session' );

class EDSource extends DataSource
{
	public $description = 'eDepoze API';

	protected $_schema = array();

	public static $lastResponse;
	public static $lastHeaders;

	public function __construct( $config=array() )
	{
		parent::__construct( $config );
		$this->Http = new HttpSocket();
	}

	public function listSources( $data=NULL )
	{
		return NULL;
	}

	public function describe( $model )
	{
		return $this->_schema;
	}

	public function calculate( Model $model, $queryData=array(), $recursive=NULL )
	{
		return 'COUNT';
	}

	public function read( Model $model, $queryData=array(), $recursive=NULL )
	{

        App::uses('CakeSession', 'Model/Datasource');
		$queryData['conditions']['logId'] = CakeSession::read('Auth.UserLog');


		if( $queryData['fields'] === 'COUNT' ) {
			return array( array( array( 'count'=>1 ) ) );
		}
		$sKey = CakeSession::read( 'EDSource.sKey' );
		if ($sKey) {
			$queryData['conditions']['sKey'] = $sKey;
		}
		$requestURL = "{$this->config['baseURL']}{$queryData['endpoint']}";
		$requestData = json_encode( $queryData['conditions'] );
//print_r($requestURL);
//print_r($requestData);
		$response = $this->Http->post( $requestURL, array( 'data'=>$requestData ) );
//ENABLE THIS TO SEE THE REQUESTS MADE TO THE WM API
//print_r($response);
//die('edsource');
		if( isset( $response->body ) ) {
			static::$lastResponse = $response->body;
		}
		if( isset( $response->headers ) ) {
			static::$lastHeaders = $response->headers;
		}

		$fullResult = json_decode( static::$lastResponse, TRUE );

		if( is_null( $fullResult ) ) {

			$error = json_last_error();
			throw new CakeException( $error );
		}
		$model::$lastResponse = $fullResult;

		if( isset( $fullResult['result'] ) ) {
			return array( $model->alias=>$fullResult['result'] );
		} else {
//echo 'X3';
			if (isset($fullResult['error'])){
				return $fullResult;
			}

			return NULL;
		}
	}

	public function passThrough( Model $model, $queryData=array(), $recursive=NULL ) {
		$sKey = CakeSession::read( 'EDSource.sKey' );
		if ($sKey) {
			$queryData['conditions']['sKey'] = $sKey;
		}
		$requestURL = "{$this->config['baseURL']}{$queryData['endpoint']}";
		$requestData = json_encode( $queryData['conditions'] );
		$fp_tmp = tmpfile();
		$ch = curl_init();
		curl_setopt_array( $ch, [
			CURLOPT_POST => 1,
			CURLOPT_HEADER => 0,
			CURLOPT_URL => $requestURL,
			CURLOPT_FRESH_CONNECT => 1,
			CURLOPT_RETURNTRANSFER => 0,
			CURLOPT_FORBID_REUSE => 1,
			CURLOPT_TIMEOUT => 60,
			CURLOPT_POSTFIELDS => http_build_query( ['data'=>$requestData] )
		] );
		curl_setopt( $ch, CURLOPT_WRITEFUNCTION, function( $ch, $buffer ) use (&$fp_tmp) {
			$length = fwrite( $fp_tmp, $buffer );
			return $length;
		} );
		curl_exec( $ch );
		curl_close( $ch );
		fseek( $fp_tmp, 0 );
		fpassthru( $fp_tmp );
		fclose( $fp_tmp );
	}

	public function prepare( Model $model, $queryData=array())
	{
		if( $queryData['fields'] === 'COUNT' ) {
			return array( array( array( 'count'=>1 ) ) );
		}
		$sKey = CakeSession::read( 'EDSource.sKey' );
		if ($sKey) {
			$queryData['conditions']['sKey'] = $sKey;
		}
		$requestURL = "{$this->config['baseURL']}{$queryData['endpoint']}";
		$requestData = json_encode( $queryData['conditions'] );

		return ['url' => $requestURL, 'data' => $requestData];
	}

}