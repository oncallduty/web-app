<?php

App::uses( 'EDModel', 'Model' );

class EDFolder extends EDModel
{
	public $primaryKey = 'ID';

	/**
	 *
	 * @todo add business rules for visibility
	 * @param type $depositionID
	 * @return null|array(EDFolder)
	 */
	public function getFolders( $depositionID ) {
		if (!$depositionID) {
			return null;
		}

		$query = ['conditions' => ['depositionID' => $depositionID]];
		$query['endpoint'] = 'deposition/getFolders';
		$results = $this->getDataSource()->read( $this, $this->buildQuery( 'all', $query ) );

		$folders = array();

		if( isset( $results[$this->alias] ) && $results[$this->alias] ) {
			$result = $results[$this->alias];
			if( isset( $result['folders'] ) && is_array( $result['folders'] ) && $result['folders'] ) {
				foreach ($result['folders'] as $folderData) {
					$folder = new EDFolder();
					$folder->set( $folderData );
					$folders[$folderData['ID']] = $folder;
				}
			}
		}

		return $folders;
	}
}
