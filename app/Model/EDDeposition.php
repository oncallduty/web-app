<?php

App::uses( 'EDModel', 'Model' );
App::uses( 'EDFolder', 'Model' );

class EDDeposition extends EDModel
{
	public $primaryKey = 'ID';

	public function attendByDepositionID( $depositionID )
	{
		if (!$depositionID) {
			return null;
		}

		$query = [
			'endpoint' => 'deposition/attend',
			'conditions' => [
				'depositionID' => $depositionID,
			]
		];

		$results = $this->getDataSource()->read( $this, $this->buildQuery( 'all', $query ) );

		if( !$results ){
			// We are not allowed to attend. Return the details for why.
			return json_decode( $this->getDataSource()->Http->response->body );
		}
		return $this->getNewDepositionByDataSource( $results, 'deposition', true );
	}

	/**
	 *
	 * @param int $depositionID
	 * @param bool $includeFolders
	 * @return null|EDDeposition
	 */
	public function getDeposition( $depositionID, $includeFolders=false )
	{
		if (!$depositionID)
		{
			return null;
		}

		$query = [
			'endpoint' => 'deposition/get',
			'conditions' => [
				'depositionID' => $depositionID,
				'includeFiles' => (bool)$includeFolders
			],
		];

		$results = $this->getDataSource()->read( $this, $this->buildQuery( 'all', $query ) );

		return $this->getNewDepositionByDataSource( $results, 'deposition', $includeFolders );
	}

	public function setSpeaker( $depositionID, $userID )
	{
		if (!$depositionID) {
			return null;
		}

		$query = [
			'endpoint' => 'deposition/setSpeaker',
			'conditions' => [
				'depositionID' => $depositionID,
				'userID' => $userID,
			]
		];

		$results = $this->getDataSource()->read( $this, $this->buildQuery( 'all', $query ) );

		return $results;
	}

	public function setLink( $conditions, $ignoreModel=false )
	{
		$query = [
			'endpoint' => 'deposition/link',
			'conditions' => $conditions,
		];

		$results = $this->getDataSource()->read( $this, $this->buildQuery( 'all', $query ) );

		if ($ignoreModel) {
			return $results;
		}

		$deposition = $this->getNewDepositionByDataSource( $results, 'targetDeposition' );

		return $deposition;
	}

	protected function getNewDepositionByDataSource( $results, $refs, $includeFiles=true ) {
		$depositions = $this->getNewDepositionsByDataSource( $results, $refs, $includeFiles );
		return (count( $depositions ) > 0 ? $depositions[0] : null);
	}

	protected function getNewDepositionsByDataSource( $results, $refs, $includeFiles=true )
	{
		$depositions = [];

		if (!is_array( $refs ))
		{
			$refs = [$refs];
		}

		if (!empty( $refs ) && isset( $results[$this->alias] ) && $results[$this->alias] ) {
			$result = $results[$this->alias];

			foreach ((is_array( $refs ) ? $refs : [$refs]) as $ref)
			{
				if (!isset( $result[$ref]) || !is_array( $result[$ref] ) || !$result[$ref] ) {
					continue;
				}

				$deposition = new EDDeposition();

				if (isset( $result[$ref]['folders'] ))
				{
					$foldersData = $result[$ref]['folders'];
					unset( $result[$ref]['folders'] );

					if ($includeFiles) {
						$deposition->data['folders'] = [];
						if (is_array( $foldersData ) && count( $foldersData ) > 0)
						{
							foreach ($foldersData as $folderData) {
								$folder = new EDFolder();
								$folder->set( $folderData );
								$deposition->data['folders'][$folderData['ID']] = $folder;
							}
						}
					}
				}

				$deposition->set( $result[$ref] );

				//Ezequiel: Bug fix for attendDeposition in DepositionsController expecting a success in the return response
				$deposition->success = true;

				$depositions[] = $deposition;
			}
		}

		return $depositions;
	}

	public function startDeposition( $depositionID )
	{
		if (!$depositionID) {
			return null;
		}

		$query = [
			'endpoint' => 'deposition/start',
			'conditions' => [
				'depositionID' => $depositionID,
			]
		];

		$results = $this->getDataSource()->read( $this, $this->buildQuery( 'all', $query ) );

		return $results;
	}

	public function finishDeposition( $depositionID, $courtReporterEmail )
	{
		if (!$depositionID) {
			return null;
		}

		$query = [
			'endpoint' => 'deposition/finish',
			'conditions' => [
				'depositionID' => $depositionID,
				'courtReporterEmail' => $courtReporterEmail,
			]
		];

		$results = $this->getDataSource()->read( $this, $this->buildQuery( 'all', $query ) );

		return $results;
	}

	public function kickAttendee( $depositionID, $userID, $guestID, $ban )
	{
		if (!$depositionID) {
			return null;
		}

		$query = [
			'endpoint' => 'deposition/kickAttendee',
			'conditions' => [
				'depositionID' => $depositionID,
				'userID' => $userID,
				'guestID' => $guestID,
				'ban' => $ban,
			]
		];

		$results = $this->getDataSource()->read( $this, $this->buildQuery( 'all', $query ) );

		return $results;
	}

	public function shareFile( $depositionID, $userIDs, $fileID, $fileName )
	{
		if (!$depositionID) {
			return null;
		}

		$query = [
			'endpoint' => 'deposition/shareFile',
			'conditions' => [
				'depositionID' => $depositionID,
				'userIDs' => $userIDs,
				'fileID' => $fileID,
				'name' => $fileName
			]
		];

		$results = $this->getDataSource()->read( $this, $this->buildQuery( 'all', $query ) );

		return $results;
	}

	public function shareFolder( $depositionID, $userIDs, $folderID, $folderName )
	{
		if (!$depositionID) {
			return null;
		}

		$query = [
			'endpoint' => 'deposition/shareFolder',
			'conditions' => [
				'depositionID' => $depositionID,
				'userIDs' => $userIDs,
				'folderID' => $folderID,
				'name' => $folderName
			]
		];

		$results = $this->getDataSource()->read( $this, $this->buildQuery( 'all', $query ) );

		return $results;
	}

	public function introduceFile( $params )
	{
		if (!is_array( $params ) || !isset( $params['depositionID'] )) {
			return null;
		}

		$query = array(
			'endpoint' => 'deposition/introduceExhibit',
			'conditions' => $params
		);

		$results = $this->getDataSource()->read( $this, $this->buildQuery( 'all', $query ) );

		return $results;
	}

	public function getKey( $depositionID )
	{
		if (!$depositionID) {
			return null;
		}

		$query = [
			'endpoint' => 'deposition/key',
			'conditions' => [
				'depoID' => $depositionID,
			]
		];

		$results = $this->getDataSource()->read( $this, $this->buildQuery( 'all', $query ) );

		return $results;
	}

	public function join( $depositionID, $depositionPasscode )
	{
		$query = [
			'endpoint' => 'deposition/join',
			'conditions' => [
				'depositionID' => $depositionID,
				'depositionPasscode' => $depositionPasscode,
			]
		];

		$results = $this->getDataSource()->read( $this, $this->buildQuery( 'all', $query ) );

		return $results;
	}

	public function authorizeWitness( $depositionID, $witnessID, $authorize )
	{
		if( !$depositionID || !$witnessID ) {
			return NULL;
		}

		$query = [
			'endpoint' => 'deposition/authorizeWitness',
			'conditions' => [
				'depositionID' => $depositionID,
				'witnessID' => $witnessID,
				'authorize' => $authorize
			]
		];

		$results = $this->getDataSource()->read( $this, $this->buildQuery( 'all', $query ) );

		return $results;
	}

	public function sendPortalInvite( $sessionID, $emailAddress )
	{
		if (!$sessionID || !$emailAddress ) {
			return NULL;
		}

		$query = [
			'endpoint' => 'deposition/sendCourtReporterPortalInvite',
			'conditions' => [
				'sessionID' => $sessionID,
				'address' => $emailAddress
			]
		];

		$results = $this->getDataSource()->read( $this, $this->buildQuery( 'all', $query ) );

		return $results;
	}
}