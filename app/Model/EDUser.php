<?php

App::uses( 'EDModel', 'Model' );


class EDUser extends EDModel
{
	public $validate = array(
		'username'=>array( 'Username is required'=>array( 'rule'=>'notEmpty', 'message'=>'Username is required.' ) ),
		'password'=>array( 'Password is required'=>array( 'rule'=>'notEmpty', 'message'=>'Please enter your password.' ) ),
	);

	//$validateGuest appears to be unused
//	public $validateGuest = [
//		'depositionid' => ['Deposition ID is required' => ['rule'=>'notEmpty', 'message'=>'Please enter your Deposition ID.' ] ],
//		'passcode' => ['Deposition Passcode is required' => ['rule'=>'notEmpty', 'message'=>'Please enter the Deposition Passcode.' ] ],
//		'name' => ['Name is required' => ['rule'=>'notEmpty', 'message'=>'Please enter your name.' ] ],
//		'email' => ['Email is required' => ['rule'=>'notEmpty', 'message'=>'Please enter your email address.' ] ],
//		'userRole' => ['Role is required' => ['rule'=>'notEmpty', 'message'=>'Please select the role.' ] ],
//	];

	public function memberLogin( $query )
	{
		if( $query && is_array( $query ) ) {
			$query['endpoint'] = 'user/login';
		}

		$query = $this->buildQuery( 'all', $query );
		if( is_null( $query ) ) {
			return null;
		}

		$results = $this->getDataSource()->read( $this, $query );
		return $results;
	}

	public function guestLogin( $query )
	{
		if( $query && is_array( $query ) ) {
			$query['endpoint'] = 'user/guestLogin';
		}

		$query = $this->buildQuery( 'all', $query );
		if (is_null( $query )) {
			return null;
		}

		$results = $this->getDataSource()->read( $this, $query );
		return $results;
	}

	public function witnessLogin( $query )
	{
		if( $query && is_array( $query ) ) {
			$query['endpoint'] = 'user/witnessLogin';
		}
		if( isset( $query['conditions']['loginClass'] ) ) {
			unset( $query['conditions']['loginClass'] );
		}
		if( isset( $query['conditions']['wpPasscode'] ) && $query['conditions']['wpPasscode'] ) {
			$query['conditions']['depositionPasscode'] = $query['conditions']['wpPasscode'];
			unset( $query['conditions']['wpPasscode'] );
			unset( $query['conditions']['tbPasscode'] );
		}
		if( isset( $query['conditions']['tbPasscode'] ) && $query['conditions']['tbPasscode'] ) {
			$query['conditions']['depositionPasscode'] = $query['conditions']['tbPasscode'];
			unset( $query['conditions']['wpPasscode'] );
			unset( $query['conditions']['tbPasscode'] );
		}
		// $this->log( print_r( ['EDUser.witnessLogin',$query], TRUE ), 'webapp' );

		$query = $this->buildQuery( 'all', $query );
		if( is_null( $query ) ) {
			return NULL;
		}

		$results = $this->getDataSource()->read( $this, $query );
		return $results;
	}

	public function passwordReset( $username )
	{
		if (!$username)
		{
			return null;
		}

		$query = $this->buildQuery( 'all', [
			'endpoint' => 'user/recoverPassword',
			'conditions' => [
				'username' => $username,
			]
		] );

		if (is_null( $query )) {
			return null;
		}

		$results = $this->getDataSource()->read( $this, $query );
		return $results;
	}

	public function setSortPreference( $sortObject, $sortBy, $sortOrder )
	{
		if (is_null( $sortObject ) || is_null( $sortBy ) || is_null( $sortOrder ) ) {
			return null;
		}

		$query = $this->buildQuery( 'all', [
			'endpoint' => 'user/setSortPreference',
			'conditions' => [
				'sortObject' => $sortObject,
				'sortBy' => $sortBy,
				'sortOrder' => $sortOrder
			]
		] );

		$results = $this->getDataSource()->read( $this, $query );
		return $results;
	}

	public function setUserPreference( $prefKey, $value )
	{
		if (is_null( $prefKey ) || is_null( $value ) ) {
			return null;
		}

		$query = $this->buildQuery( 'all', [
			'endpoint' => 'user/setUserPreference',
			'conditions' => [
				'prefKey' => $prefKey,
				'value' => $value
			]
		] );

		$results = $this->getDataSource()->read( $this, $query );
		return $results;
	}

	public function setCustomSort( $sortObject, $list, $sessionID )
	{
		if (is_null( $sortObject ) || is_null( $list ) || !is_array( $list )) {
			return null;
		}

		$query = $this->buildQuery( 'all', [
			'endpoint' => 'user/setCustomSort',
			'conditions' => [
				'sortObject' => $sortObject,
				'list' => $list,
				'sessionID' => $sessionID
			]
		] );

		$results = $this->getDataSource()->read( $this, $query );
		return $results;
	}
}
