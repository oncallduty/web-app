<?php

App::uses( 'EDModel', 'Model' );

class EDSearch extends EDModel {

	public function search( $terms, $sessionID, $options=NULL ) {
		if( !$terms || !$sessionID ) {
			return NULL;
		}
		// These options should be coming in as string "1" or "0", need to be casted to bool
		if( $options ) {
			$options['searchContent'] = (bool)$options['searchContent'];
			$options['includeFilename'] = (bool)$options['includeFilename'];
		}

		$query = [
			'endpoint' => 'search/search',
			'conditions' => [
				'terms' => $terms,
				'sessionID' => $sessionID,
				'options' => $options
			]
		];

		try {
			$results = $this->getDataSource()->read( $this, $this->buildQuery( 'all', $query ) );
		} catch( Exception $e ) {
			$this->log( 'EDSearch; Exception while reading Datasource, error:' . $e->getMessage(), 'error' );
			return ['error' => ['code'=>$e->getCode(), 'message'=>$e->getMessage()]];
		}

		return isset( $results['EDSearch'] ) ? $results['EDSearch'] : $results;
	}

}
