<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
	Router::connect( '/users/*', array( 'controller'=>'users', 'action'=>'index' ) );
	Router::connect( '/cases/*', array( 'controller'=>'cases', 'action'=>'index' ) );
	Router::connect( '/depositions/*', array( 'controller'=>'depositions', 'action'=>'index' ) );

	Router::connect( '/signin', array( 'controller' => 'users', 'action' => 'signin' ) );
	Router::connect( '/members', array( 'controller'=>'users', 'action'=>'members' ) );
	Router::connect( '/nonmembers', array( 'controller'=>'users', 'action'=>'nonmembers' ) );
	Router::connect( '/guests', array( 'controller'=>'users', 'action'=>'guests' ) );
	Router::connect( '/witness', array( 'controller'=>'users', 'action'=>'witness' ) );
	Router::connect( '/reset', array( 'controller'=>'users', 'action'=>'reset' ) );

	//BRANDING
	Router::connect( '/lexcity/*', array( 'controller'=>'users', 'action'=>'applyBranding' ) );
	Router::connect( '/edepoze/*', array( 'controller'=>'users', 'action'=>'applyBranding' ) );
	Router::connect( '/advanceddepositions/*', array( 'controller'=>'users', 'action'=>'applyBranding' ) );
	Router::connect( '/exhibitbridge/*', array('controller' => 'users', 'action' => 'applyBranding') );
	Router::connect( '/neesons/*', array('controller' => 'users', 'action' => 'applyBranding') );
	Router::connect( '/depokit/*', array('controller' => 'users', 'action' => 'applyBranding') );
	Router::connect( '/dti/*', array('controller' => 'users', 'action' => 'applyBranding') );

	Router::connect( '/i', array( 'controller'=>'users', 'action'=>'frame' ) );

	Router::connect( '/*', array( 'controller'=>'users', 'action'=>'index' ) );

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
