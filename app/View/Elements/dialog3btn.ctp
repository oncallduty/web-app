<div id="dialog3Btn" data-tag="">
	<div class="popupDialog">
		<div class="popupContents">
			<h4><!-- title --></h4>
			<div class="dialogMsg"><!-- description --></div>
		</div>
		<div class="dialogBtnWrap"></div>
	</div>
</div>
<script type="text/javascript">
function showDialog3Btn( title, description, buttons, small, attrClass, tag )
{
	var dialog3Btn = $('#dialog3Btn');
	$( 'div.popupContents h4', dialog3Btn ).html( (typeof title !== 'undefined' && title !== null ? title : '&nbsp;') );
	$( 'div.dialogMsg', dialog3Btn ).html( (typeof description !== 'undefined' && description !== null ? description : '&nbsp;') );

	if (typeof buttons === 'undefined' || buttons === null) {
		buttons = [];
	} else if (!$.isArray( buttons )){
		buttons = [buttons];
	}

	if( typeof small !== 'undefined' && !!small === true ) {
		$( '.popupDialog', dialog3Btn ).addClass( 'small' );
	} else {
		$( '.popupDialog', dialog3Btn ).removeClass( 'small' );
	}

	if( typeof attrClass === 'string' && attrClass !== null  && attrClass.length > 0 ) {
		dialog3Btn.addClass( attrClass );
	}

	if( typeof tag === 'string' && tag.length > 0 ) {
		dialog3Btn.attr( 'data-tag', tag );
	}

	$( 'div.dialogBtnWrap', dialog3Btn ).empty();

	for (var b in buttons) {
		var btn = buttons[b];
		var btnEl = prepareDialogButton( btn );
		var jqBtn = $(btnEl);
		if( typeof btn.action === 'function' ) {
			jqBtn.off('click').on( 'click', btn.action );
		}
		$( 'div.dialogBtnWrap', dialog3Btn ).append( jqBtn );
	}

	dialog3Btn.show();
	if( $('#ipad_container').hasClass( 'fullscreen' ) ) {
		dialog3BtnCalculatePosition();
	}

	dialog3BtnCalculateMargin();
}

function dismissDialog3Btn() {
	$('#dialog3Btn').hide();// ED-3128 this was fading out previously but caused timing issues with other dialogs
	$('#dialog3Btn .popupDialog').css( 'left', '' );
	$('#dialog3Btn .popupDialog').attr( 'class', 'popupDialog' );
	$('#dialog3Btn').removeClass().attr( 'data-tag', '' );
	window.top.navBar.canHideToolbar( true );
}

function prepareDialogButton( btn ) {
	var btnId = (typeof btn.id !== 'undefined' && btn.id !== null ? ' id="'+btn.id+'"' : '');
	var btnClass = (typeof btn.class !== 'undefined' && btn.class !== null ? btn.class : 'app_bttn');
	var btnLabel = (typeof btn.label !== 'undefined' && btn.label !== null ? btn.label : '&nbsp;');
	var btnInactive = (typeof btn.inactive !== 'undefined' && Boolean( btn.inactive ) === true);
	var btnAction = '';
	if (btnInactive) {
		btnClass += ' inactive';
	} else {
		if( typeof btn.action !== 'function' ) {
			btnAction = ' onclick="'+(typeof btn.action !== 'undefined' && btn.action !== null ? btn.action : '')+'"';
		}
	}
	if (btnClass.length > 0) {
		btnClass = ' class="'+btnClass+'"';
	}
	return '<div'+btnId+btnClass+btnAction+'><span>'+btnLabel+'</span></div>';
}

function dialog3BtnCalculatePosition() {
	var halfWidth = $('#dialog3Btn .popupDialog').width() / 2;
	var dialogHeight = $('#dialog3Btn .popupDialog').height();
	var contentHeight = $('#dialog3Btn .popupContents').height();
	var buttonHeight = $('#dialog3Btn .dialogBtnWrap').height();
	var marginHeight = ((dialogHeight - buttonHeight - contentHeight)/3);
	/*$('#dialog3Btn .popupDialog').css( 'margin', 'auto -'+halfWidth+'px' );
	$('#dialog3Btn .popupContents').css( 'margin-top', marginHeight+"px");
	$('#dialog3Btn .popupContents').css( 'margin-bottom', marginHeight+"px");*/
}

function dialog3BtnCalculateMargin() {
	var dialogHeight = $('#dialog3Btn .popupDialog').height();
	var contentHeight = $('#dialog3Btn .popupContents').height();
	var buttonHeight = $('#dialog3Btn .dialogBtnWrap').height()+80;
	var marginHeight = ((dialogHeight - buttonHeight - contentHeight)/3);
	/*$('#dialog3Btn .popupDialog').css( 'height', (contentHeight + buttonHeight) + 'px' );
	$('#dialog3Btn .popupContents').css( 'margin-top', marginHeight+"px");
	$('#dialog3Btn .popupContents').css( 'margin-bottom', marginHeight+"px");*/
}
</script>
