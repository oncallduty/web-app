<div id="fileSelect" style="display:none;">
	<div class="fsPanelWrap">
		<div class="fsPanelShadow"></div>
		<div id="closeDragHandle" class="drawerTab"></div>
		<div class="fileSelectPanel">
			<div class="navbar">
				<div class="backBtn">
					<p>Back</p>
				</div>
				<div class="depositionLabel"></div>
			</div>
			<div class="content_search_container"></div>
			<div class="fsTablesPanel">
				<div class="fsFoldersPanel">
					<div class="fsLegend">Folders</div>
					<div id="fsFolders">
						<div id="fsSortFolders" class="sortHeader"></div>
						<div id="fsFoldersList">
							<ul></ul>
						</div>
					</div>
				</div>
				<div class="fsFilesPanel">
					<div class="fsLegend">Documents</div>
					<div id="fsFiles">
						<div class="bg"></div>
						<div id="fsSortFiles" class="sortHeader"></div>
						<div id="sortCSFiles" class="sortHeader"></div>
						<div id="fsFilesList">
							<ul></ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="openDragHandle" class="drawerTab"></div>
</div>

<script type="text/javascript">
<?php $this->Html->scriptStart( ['inline'=>FALSE] ); ?>
function FileSelection()
{
	var self = this;
	var fileSelect = $('#fileSelect');
	var fsPanel = $('.fsPanelWrap', fileSelect);
	var fsPanelShadow = $('.fsPanelShadow', fileSelect);
	var openDragHandle = $('#openDragHandle', fileSelect);
	var userSortFolders = null;
	var userSortFiles = null;
	var deposition = null;
	var folders = null;
	var files = null;
	var selectedFolder = null;
	var inCustomSort = null;
	var foldersList = $('#fsFoldersList ul', fsPanel);
	var filesList = $('#fsFilesList ul', fsPanel);
	var user = window.top.webApp.getS( 'user' );
	var oldSearchText = '';
	var newSearchText = '';
	var fileRows = [];
	var fileRowsToShow = [];
	var filterFunction = null;
	var searching = false;
	var enabled = false;
	var bind = this;

	this.toggleFileSelection = function()
	{
//		console.log( 'fileSelection::toggleFileSelection()' );
		var effect = {"effect":"slide", "direction":"left", "duration":400, "easing":"easeInOutQuad"};
		if( fsPanel.is(':visible') ) {
			effect.complete = function() { openDragHandle.show(); };
			fsPanel.hide( effect );
		} else {
			if ($('#social_list_container').is(':visible') ) {
				$('#attendee_list_toggle_wrap').trigger('click');
			}
			openDragHandle.hide();
			effect.complete = function() {
				scrollToSelectedFolder();
				scrollToSelectedFile();
			};
			fsPanel.show( effect );
		}
	};

	this.closeFileSelection = function() {
		//effect.complete = function() { openDragHandle.show(); };
		fsPanel.hide();
		openDragHandle.show();
	};

	this.selectFolder = function( folderID )
	{

		console.log( 'FileSelection::selectFolder() folderID:', folderID );
		folders = deposition.getFolders();
		for( var f in folders ) {
			if( folders[f].ID === folderID ) {
				selectedFolder = folders[f];
//alert('sf:'+folderID);
				console.log('Store `selectedFolderID` folder as: '+folderID);
				window.top.webApp.setItem( 'selectedFolderID', folderID );
/*
if (window.top.webApp.getItem( 'selectedFolderID' ) == null){
	console.log('ERROR - Folder is null');
	alert('gf:'+window.top.webApp.getItem( 'selectedFolderID' ) );
}
*/

//				$('#fsFiles .sortHeader').show();
				sortFolders();
				sortFiles();
				var selectedFile = $('#fsFilesList ul li[data-id="' + window.top.webApp.getItem( 'selectedFileID' ) + '"]');
				if( isset( selectedFile ) && selectedFile.length > 0 ) {
					$('#fsFilesList ul li').removeClass('selected');
					selectedFile.addClass('selected');
				}
				break;
			}
		}
		if (searching) {
			$('.doc_search_container input').focus();
			indexFileRows();
			filterDocs();
		}
	};

	var sortFolders = function()
	{
//		console.log( 'FileSelection::sortFolders()' );
		folders = userSortFolders.sort( folders );
		foldersList.empty();
		var elementHTML = '<li></li>';
		var labelHTML = '<div class="folderLabel"></div>';
		for( var f in folders ) {
			var _folder = folders[f];
			var li = $( elementHTML );
			li.attr( 'data-id', _folder.ID );
			li.addClass( 'sortable' );
			if( selectedFolder && selectedFolder.ID === _folder.ID ) {
				li.addClass( 'selected' );
			}
			if(_folder.caseID > 0) {
				li.addClass('caseFolder');
			}
			var dragHandle = $( '<div class="dragBox"><div class="handle"></div></div>' );
			li.append( dragHandle );
			var label = $( labelHTML );
			label.append( _folder.name );
			li.append( label );
			li.on( 'click', {"folderID":_folder.ID}, function( event ) {
				bind.selectFolder( event.data.folderID );
			} );
			foldersList.append( li );
		}
		var listMinLength = 11;
		if( foldersList.length < listMinLength ) {
			for( var i = (listMinLength - folders.length); i > 0; --i ) {
				foldersList.append( elementHTML );
			}
		}
	};

	var sortFiles = function()
	{
		var searchValues = window.top.searchValues;
		if( searchValues !== null && typeof searchValues !== 'undefined' ) {
			if( typeof window.top.contentSearch === 'function' ) {
				window.top.contentSearch.buildFileList();
			}
			return;
		}
		$('.fsTablesPanel').removeClass( 'contentSearch' );
//		console.log( 'FileSelection::sortFiles()' );
		if( selectedFolder ) {
			if( selectedFolder.numFiles !== selectedFolder.getFileIDs().length ) {
				console.warn( 'numFiles:', selectedFolder.numFiles, 'getFileIDs():', selectedFolder.getFileIDs().length, selectedFolder );
				filesList.addClass( 'loading' );
				top.Datacore.syncFolder( selectedFolder.ID, sortFiles );
				return;
			}
			var _files =  selectedFolder.getFiles();
			files = [];
			for( var f in _files ) {
				var _file = _files[f];
//				if( window.top.Datacore.s.inPresentation && _file.mimeType !== 'application/pdf' ) {
//					continue;
//				}
				files.push( _file );
			}
			files = userSortFiles.sort( files );
		}
		filesList.empty();
		filesList.removeClass( 'loading' );

		var elementHTML = '<li></li>';
		var labelHTML = '<div class="fileLabel"></div>';
		var clientTimezoneOffset = new Date().getTimezoneOffset();
		for( var f in files ) {
			var _file = files[f];
//			if( window.top.Datacore.s.inPresentation && _file.mimeType !== 'application/pdf' ) {
//				continue;
//			}
			var li = $( elementHTML );
			li.attr( 'data-id', _file.ID );
			li.addClass( 'sortable' );
			var dragHandle = $( '<div class="dragBox"><div class="handle"></div></div>' );
			li.append( dragHandle );
			var label = $( labelHTML );
			label.append( _file.name );
			li.append( label );

			var secondLine;
			if ( (user.userType === 'M' || user.userType === 'WM') && selectedFolder.class !== 'Exhibit' ) {
				var exhibitHistoryList = window.top.Datacore.getExhibitsBySourceFileID( _file.ID );
				if ( exhibitHistoryList.length === 0 ) {
					secondLine = '<p class="name">&nbsp;</p>';
				} else {
					exhibitHistoryList = exhibitHistoryList.reverse();
					var first = true;
					secondLine = '<p class="exhibitHistory">';
					for( var i in exhibitHistoryList ) {
						var exHistory = exhibitHistoryList[i];
						var srcFile = window.top.Datacore.getFile( exHistory.exhibitFileID );
						if( typeof srcFile !== 'undefined' && srcFile !== null ) {
							if ( first ) {
								first = false;
								secondLine += 'As Exhibit: ';
							} else {
								secondLine += ', ';
							}
							secondLine += srcFile.name;
						}
					}
					secondLine += '&nbsp;</p>';
				}
			} else if ( user.userType === 'G' ) {
				secondLine = '<p class="name">&nbsp;</p>';
			} else {
				var exhibitHistory = window.top.Datacore.getExhibitByExhibitFileID( _file.ID );
				var localTime;
				var name;
				if ( exhibitHistory === null ) {
					localTime = new Date( _file.created.getTime() );
					name = _file.byUser.firstName + ' ' + _file.byUser.lastName;
				} else {
					localTime = new Date( exhibitHistory.introducedDate );
					name = exhibitHistory.introducedBy;
				}
				localTime.setMinutes( localTime.getMinutes() - clientTimezoneOffset );
				var hours = localTime.getHours();
				var ampm = "AM";
				if ( hours >= 12 ) {
					hours -= 12;
					ampm = "PM";
				}
				if ( hours === 0 ) {
					hours = 12;
				}
				var minutes = ('0' + localTime.getMinutes()).slice( -2 );
				var seconds = ('0' + localTime.getSeconds()).slice( -2 );

				secondLine = '<p class="exhibitHistory">'+((localTime.getMonth()+1) + '/' + localTime.getDate() + '/' + localTime.getFullYear()) + ' ' + hours + ':' + minutes + ':' + seconds + ' ' + ampm + ' By: '+ name + '</p>';
			}

			li.append( secondLine );

			li.on( 'click', {file:_file}, function( event ) {
				_file = event.data.file;
				if( window.top.Datacore.s.inPresentation && _file.mimeType !== 'application/pdf' ) {
					window.top.showDialog3Btn( "Abort Presentation?", "Presentation Mode does not support multimedia files. Opening this multimedia file will end the presentation.", [
						{label: "Cancel", class: 'btn_app_cancel', action: function(){self.closeFileSelection();window.top.dismissDialog3Btn();}},
						{label: "Continue", class: 'btn_app_normal', action: function(){self.closeFileSelection();window.top.dismissDialog3Btn();window.top.webApp.closeOutPrevent(true);window.top.webApp.viewFile(_file.ID);}}
					] );
				} else {
					viewFile( event );
				}
			} );
			filesList.append( li );
		}
		var listMinLength = 11;
		if( filesList.length < listMinLength ) {
			for( var i = (listMinLength - files.length); i > 0; --i ) {
				filesList.append( elementHTML );
			}
		}
	};

	this.startCustomSort = function( type )
	{
//		console.log( 'FileSelection::startCustomSort() type = ' + type );
		if( inCustomSort ) {
			this.endCustomSort();
			$('.doneButton', fsPanel).trigger( 'abortCustomSort' );
		}
		switch( type ) {
			case 'Files':
				inCustomSort = filesList;
				break;
			case 'Folders':
				inCustomSort = foldersList;
				break;
			default:
				return;
		}
		inCustomSort.addClass( 'CustomSort' );
		$('li', inCustomSort).off();
		inCustomSort.sortable( {items:'.sortable', containment:'parent', tolerance: 'pointer'} );
	};

	this.endCustomSort = function()
	{
//		console.log( 'FileSelection::endCustomSort()' );
		if( inCustomSort === null ) {
			return;
		}
		inCustomSort.removeClass( 'CustomSort' );
		if( inCustomSort.hasClass( 'ui-sortable' ) ) {
			inCustomSort.sortable( 'destroy' );
		}
		if( inCustomSort === filesList ) {
			sortFiles();
		} else if( inCustomSort === foldersList ) {
			sortFolders();
		}
		inCustomSort = null;
	};

	this.saveCustomSort = function( data )
	{
//		console.log( 'FileSelection::saveCustomSort() data = ' + data );
		var sortElement;
		switch( data.sortObject ) {
			case 'Files':
				sortElement = filesList;
				break;
			case 'Folders':
				sortElement = foldersList;
				break;
			default:
				return;
		}
		if( !sortElement.hasClass( 'ui-sortable') ) {
			return;
		}
		var list = sortElement.sortable( 'toArray', {"attribute":"data-id"} );
		var sortPosMap = [];
		var listMap = {};
		var sortPositions = [];
		if( inCustomSort === filesList ) {
			for( var f in files ) {
				var _file = files[f];
				sortPosMap[f] = _file.sortPos;
				listMap[_file.ID] = _file;
			}
			for( var i in list ) {
				var _fileID = parseInt( list[i] );
				var _file = listMap[_fileID];
				var sortPos = sortPosMap[i];
//				console.log( _file.name, _file.ID, _file.sortPos, sortPos );
				_file.sortPos = sortPos;
				var customSort = {};
				customSort[_file.ID] = _file.sortPos;
				sortPositions.push( customSort );
			}
			files = userSortFiles.sort( files );
		} else if( inCustomSort === foldersList ) {
			for( var f in folders ) {
				var _folder = folders[f];
				sortPosMap[f] = _folder.sortPos;
				listMap[_folder.ID] = _folder;
			}
			for( var i in list ) {
				var _folderID = parseInt( list[i] );
				var _folder = listMap[_folderID];
				var sortPos = sortPosMap[i];
//				console.log( _folder.name, _folder.ID, _folder.sortPos, sortPos );
				_folder.sortPos = sortPos;
				var customSort = {};
				customSort[_folder.ID] = _folder.sortPos;
				sortPositions.push( customSort );
			}
			folders = userSortFolders.sort( folders );
		}

		$.post( '/users/setCustomSort', {"sortObject":data.sortObject, "list":sortPositions}, function( data, textStatus ) {
			if( textStatus === "success" ) {
				if( data !== null ) {
//					console.log( 'FileSelection::setCustomSort() -- success' );
					return;
				}
			}
//			console.log( 'FileSelection::setCustomSort() -- failure' );
		}, 'json' );
	};

	this.enableFileSelection = function()
	{
		enabled = true;
		console.log( 'FileSelection::enableFileSelection()' );
		deposition = top.webApp.getS( 'deposition' );
		if( typeof deposition === 'undefined' || deposition === null ) {
			return;
		}
		userSortFolders = new top.UserSort( 'Folders', $('#fsFolders #fsSortFolders'), sortFolders, top.fileSelection );
		userSortFiles = new top.UserSort( 'Files', $('#fsFiles #fsSortFiles'), sortFiles, top.fileSelection );

		folders = deposition.getFolders();


		console.log('FileSelection::enableFileSelection() folders = ' +folders);
		if( folders.length > 0 ) {
		sortFolders();

			if( isset( window.top.webApp.getItem( 'selectedFolderID' ) ) ){
				bind.selectFolder( window.top.webApp.getItem( 'selectedFolderID' ) );
			} else if (folders[0].ID) {
				bind.selectFolder( folders[0].ID );
			}
			
		}

		

		

		var searchBtn = $('.search_icon');
		var hideSearchBarBtn = $('#hideSearchBarBtn');
		var searchTextbox = $('.doc_search_container input');
		var closeDragHandle = $('#closeDragHandle', fsPanel);
		var backButton = $('.backBtn', fsPanel);
		var depoLabel = $('.depositionLabel', fsPanel);
		depoLabel.text( deposition.depositionOf );
		openDragHandle.off();
		openDragHandle.on( 'click', this.toggleFileSelection );
		closeDragHandle.off();
		closeDragHandle.on( 'click', this.toggleFileSelection );
		backButton.off();
		backButton.on( 'click', this.toggleFileSelection );
		fsPanelShadow.off();
		fsPanelShadow.on('click', this.toggleFileSelection );

		searchBtn.off();
		searchBtn.on('click', function() {
			startSearch();
		});
		hideSearchBarBtn.off();
		hideSearchBarBtn.on('click', function() {
			stopSearch();
		});
		searchTextbox.off();
		searchTextbox.on('input', function() {
			clearTimeout(filterFunction);
			newSearchText = $(this).val();
			filterFunction = setTimeout(function() {
				if (newSearchText === oldSearchText) {
					return;
				} else {
					oldSearchText = newSearchText;
					filterDocs();
				}
			}, 500);
		});
		fileSelect.show();
	};

	this.disableFileSelection = function()
	{
		enabled = false;
		fileSelect.hide();
	};

	this.refresh = function() {
		if(enabled) {
			self.enableFileSelection();
		}
	};

	var scrollToSelectedFolder = function() {
		//console.log( window.top.webApp);
		console.log(window.top.webApp.getItem( 'selectedFolderID' ));


		var selectedFolder = $('#fsFoldersList ul li[data-id="'+ window.top.webApp.getItem( 'selectedFolderID' ) +'"]');

		if( isset( selectedFolder ) ) {
			$('#fsFoldersList ul li').removeClass('selected');
			selectedFolder.addClass('selected');
			$('#fsFoldersList').scrollTop( selectedFolder.position());
		}
	};

	var scrollToSelectedFile = function() {
		var selectedFile = $('#fsFilesList ul li[data-id="' + window.top.webApp.getItem( 'selectedFileID' ) + '"]');

		if( isset( selectedFile ) && selectedFile.length > 0 ) {
			$('#fsFilesList ul li').removeClass('selected');
			selectedFile.addClass('selected');
			$('#fsFilesList').scrollTop( selectedFile.position().top );
		}
	};

	var viewFile = function(event) {
		if (!window.top.Datacore.s.inPresentation) {
			window.top.webApp.setItem( 'viewFile', JSON.stringify( event.data.file ) );
		}
		window.top.webApp.viewFile(event.data.file.ID);
		window.top.webApp.setItem( 'selectedFileID', event.data.file.ID );
		top.fileSelection.toggleFileSelection();
	};

	var startSearch = function() {
		$('.doc_search_container').addClass('active');
		indexFileRows();
		$('.doc_search_container input').focus();
		searching = true;
	};

	var stopSearch = function() {
		if (!searching) {
			return;
		}
		$('.doc_search_container').removeClass('active');
		redrawFileList( true );
		$('.doc_search_container input').val('');
		fileRows = [];
		fileRowsToShow = [];
		newSearchText = '';
		oldSearchText = '';
		filterFunction = null;
		searching = false;
	};

	var indexFileRows = function() {
		fileRows = [];
		$('#fsFilesList ul li.sortable').each(function() {
			fileRows.push($(this)[0]);
		});
	};

	var filterDocs = function() {
		var fileName;
		for (var rowIndex in fileRows) {
			fileName = fileRows[rowIndex].children[1].innerHTML;
			if (fileName.toLowerCase().indexOf(oldSearchText.toLowerCase()) >= 0) {
				fileRowsToShow.push(rowIndex);
			}
		}
		redrawFileList( false );
	};

	var redrawFileList = function( showAll ) {
		filesList.empty();
		if (showAll) {
			for (var i in fileRows) {
				filesList.append(fileRows[i]);
			}
		} else {
			for (var i in fileRowsToShow) {
				filesList.append(fileRows[fileRowsToShow[i]]);
			}
		}
		fillFileList();
		addViewFileClickToRows();
		fileRowsToShow = [];
	};

	var fillFileList = function() {
		var listLength = filesList.length;
		var filesListMinLength = 11;
		if( listLength < filesListMinLength ) {
			for( var i = (filesListMinLength - listLength); i > 0; --i ) {
				filesList.append( '<li></li>' );
			}
		}
	};

	var addViewFileClickToRows = function() {
		$('#fsFilesList ul li.sortable ').each(function() {
			var fileID = parseInt($(this).attr('data-id'));
			var File = null;
			for ( var f in files ) {
				File = files[f];
				if (File.ID === fileID) {
					break;
				}
			}
			$(this).on( 'click', {file:File}, function( event ) {
				viewFile( event );
			} );
		});
	};
};

//window.top.fileSelection = new FileSelection();

<?php $this->Html->scriptEnd(); ?>
</script>