<div id="dialog1Btn" data-tag="">
	<div class="popupDialog">
		<h2></h2>
		<div class="dialogMsg">
			<div class="dialogMsgInnerWrap"></div>
		</div>
		<div class="dialogOkBtnWrap">
			<button id="dialog1BtnClose" onclick="dismissDialog1Btn()" onkeyup="dismissDialog1Btn()"></button>
		</div>
	</div>
</div>
<script type="text/javascript">
<!-- //
var closeFunction = null;

function showPlainDialog1Btn( title, description, buttonLabel, tag ) {
	$('#dialog1Btn div.popupDialog h2').html( title );
	$('#dialog1Btn div.dialogMsg div.dialogMsgInnerWrap').html( '<span>'+description+'</span>' );
	$('#dialog1BtnClose').removeClass().addClass( 'app_bttn_new' ).html( '<span>'+buttonLabel+'</span>' );
	if( typeof tag === 'string' && tag.length > 0 ) {
		$('#dialog1Btn').attr( 'data-tag', tag );
	}
	$('#dialog1Btn').show();
	dialog1BtnCalculatePosition();
	$('#dialog1BtnClose').focus();
}

function showDialog1Btn( title, description, buttonLabel, attrClass, tag ) {
	$('#dialog1Btn div.popupDialog h2').html( title );
	if (description.length > 0) {
		$('#dialog1Btn div.dialogMsg div.dialogMsgInnerWrap').html( '<span>'+description+'</span>' );
	} else {
		$('#dialog1Btn div.popupDialog h2').addClass( 'nodescription' );
		$('#dialog1Btn div.dialogMsg div.dialogMsgInnerWrap').html( '' );
	}
	if( typeof attrClass === 'string' ) {
		$('#dialog1Btn').addClass( attrClass );
	}
	if( typeof tag === 'string' && tag.length > 0 ) {
		$('#dialog1Btn').attr( 'data-tag', tag );
	}
	if( typeof buttonLabel !== 'string' ) {
		setTimeout( dismissDialog1Btn, 3000 );
	} else {
		$('#dialog1BtnClose').removeClass().addClass( 'dialogOkBtn' ).html( '<span>'+buttonLabel+'</span>' );
		$('#dialog1Btn').show();
		dialog1BtnCalculatePosition();
		$('#dialog1BtnClose').focus();
		return;
	}
	$('#dialog1Btn').show();
	dialog1BtnCalculatePosition();
}

function dismissDialog1Btn() {
	if (typeof closeFunction === 'function') {
		closeFunction();
		closeFunction = null;
	}

	$('#dialog1Btn div.popupDialog h2').removeClass();

	var onComplete = function()
	{
		$('#dialog1Btn').removeAttr( 'class' ).attr( 'data-tag', '' );
	};
	$('#dialog1Btn').fadeOut( 200, onComplete );
}

function setDialog1BtnCloseAction( action ) {
	closeFunction = action;
}

function clearDialog1BtnCloseAction() {
	closeFunction = null;
}

function dialog1BtnCalculatePosition() {
	var halfWidth = $('#dialog1Btn .popupDialog').width() / 2;
	var screenMiddle = $('#dialog1Btn').width() / 2;
	$('#dialog1Btn .popupDialog').css( 'margin', 'auto -'+halfWidth+'px' );
}
// -->
</script>