<?php
$this->Html->script( 'login', array( 'inline'=>false ) );
?>
<div id="loading">
	<div class="spinner">
		<img src="/img/ipad/loading.gif" width="48" height="48" border="0" />
		<div>Loading ...</div>
	</div>
</div>
<div id="dialogJoin">
	<div class="popupDialog">
		<div class="popupContents">
			<h4>Join Session</h4>
			<div class="dialogMsg"><span>Please enter a Session ID and passcode to enter an active session.</span></div>
		</div>
		<div id="join_form_wrap">
			<?= $this->Form->create( 'Join', array( 'default'=>FALSE ) ); ?>
			<?= $this->Form->input( 'EDUser.depositionID', array( 'label'=>'Session ID:' ) ); ?>
			<?= $this->Form->input( 'EDUser.depositionPasscode', array( 'label'=>'Passcode:', 'type'=>'password' ) ); ?>
			<?= $this->Form->end(); ?>
		</div>
		<div class="dialogBtnWrap">
			<div id="btn_join_cancel" class="app_bttn"><span>Cancel</span></div>
			<div id="btn_join" class="app_bttn_new"><span>Join</span></div>
		</div>
	</div>
</div>
<script type="text/javascript">
//dialogjoin
<?php $this->Html->scriptStart( ['inline'=>FALSE] ); ?>
$('#btn_join_cancel').click( function() {
	$('#dialogJoin').fadeOut( '200' );
} );

$("#dialogJoin").keyup(function(event){
    if(event.keyCode == 13){
        $("#btn_join").click();
    }
});

$('#btn_join').click( function() {
	$.ajax({
		async:true,
		beforeSend:function (XMLHttpRequest) {
			$('#dialogJoin').fadeOut( '200' );$("#loading").fadeIn( 200 );},
		complete:function (XMLHttpRequest, textStatus) {
			$("#loading").hide();},
		data:$("#JoinFrameForm").serialize(),
		error:function (XMLHttpRequest, textStatus, errorThrown) {joinError(data, textStatus);},
		success:function (data, textStatus) {joinSuccess(data, textStatus);},
		type:"POST",
		url:"/depositions/join"});
} );

function showDialogJoin() {
	$('#dialogJoin').fadeIn( '200' );
	$('input:text:visible:first').focus();
}

function joinSuccess( data, textStatus )
{
	$("#loading").hide();
	try {
		var response = jQuery.parseJSON( data );
	} catch( err ) {
		displayError( err.message );
	}

	if( typeof( response ) === 'undefined' ) {
		//oops!
		displayError( null, 'Invalid response data' );
		return;
	}

	if (response.hasOwnProperty( 'success' )) {
		if (response.success !== true) {
			if (response.hasOwnProperty( 'error' )) {
				if (response.error.hasOwnProperty( 'code' )) {
					switch (parseInt( response.error.code )) {
						case 0:
						case 1004:
							var title = 'Incorrect Session Credentials';
							break;
						case 1001:
							var title = 'Incorrect Session Credentials';
							break;
						default:
							title = 'Access Denied';
					}
				}
				displayError( response.error.description, null, title );
			}
			return;
		}
	}

	if (response.result.hasOwnProperty( 'needToLinkDepo' ) && response.result.needToLinkDepo) {
		var dialogButtons = [];
		if (response.result.casesLength > 0) {
			dialogButtons.push( {label: 'Browse files',class: 'app_bttn_new', action : 'depoLinkBrowse();'} );
		} else {
			dialogButtons.push( {label: 'Browse files', inactive : true} );
		}
		dialogButtons.push( {label: 'Cancel', action : 'depoLinkCancel();'} );
		dialogButtons.push({label: 'Create New', class: 'app_bttn_new', action: 'depoLinkNew();'});
		var depoLinkInstructions = 'If you already set up a case and/or session for this witness in your eDepoze account, select &quot;Browse Files&quot; and choose your existing case or session.'
				+ '<br /><br />Or Select &quot;Create New&quot; to create a new case and session in your eDepoze account.';
		var depoType = (response.result.deposition.class === 'WitnessPrep' || response.result.deposition.class === 'WPDemo') ? 'Witness Prep' : 'Deposition';
		showDialog3Btn( 'Welcome to the '+response.result.deposition.depositionOf+' '+depoType+'.', depoLinkInstructions, dialogButtons );
		return;
	}

	if( response.result.hasOwnProperty( 'redirect' ) ) {
		window.location.replace( response.result.redirect );
		return;
	}
}

function joinError( data, textStatus )
{
	$("#loading").hide();
	var response = jQuery.parseJSON( data );
}

function depoLinkBrowse()
{
	$('#dialog3Btn').fadeOut( 200 );

	window.location.assign( '/cases/browse?join=1' );
}

function depoLinkCancel()
{
	$('#dialog3Btn').fadeOut( 200 );
}

function depoLinkNew()
{
	$('#dialog3Btn').fadeOut( 200 );

	$("#loading").fadeIn( 200 );

	$.ajax( '/cases/browse?cID=0&dID=0' ).done( function( data, status ) {
		$("#loading").fadeOut( 200 );

		try {
			var response = jQuery.parseJSON( data );
		} catch( err ) {
			displayError( err.message );
		}

		var redirect = null;

		if (typeof( response ) === 'undefined' || response === null) {
			displayError( null, 'Invalid response data' );
			redirect = '/users/logout?ul=2';
		}

		if (response.hasOwnProperty( 'redirect' )) {
			redirect = response.redirect;
		}

		if (redirect) {
			window.location.assign( redirect );
			return;
		}

		if( response.hasOwnProperty( 'success' ) ) {
			if( response.success !== true ) {
				if( response.hasOwnProperty( 'error' ) ) {
					displayError( response.error );
				}
				return;
			}
		}
	} );
}

function displayError( error_msg, log_msg, error_title ) {
	error_msg = (typeof error_msg !== 'undefined' && error_msg !== null ? error_msg : '');
	log_msg = (typeof log_msg !== 'undefined' && log_msg !== null ? log_msg : '');
	error_title = (typeof error_title !== 'undefined' && error_title !== null ? error_title : 'Access Denied');

	if (error_msg.length > 0) {
		if (typeof( showDialog1Btn ) === 'function') {
			setDialog1BtnCloseAction( function() {
				if ($('#EDUserUsername').length > 0) {
					$('#EDUserUsername').focus();
				} else if ($('#EDUserDepositionID').length > 0) {
					$('#EDUserDepositionID').focus();
				}
			} );
			showDialog1Btn(error_title , error_msg, 'OK' );
		} else {
			alert( error_msg );
		}
	}
}
<?php $this->Html->scriptEnd(); ?>
</script>
