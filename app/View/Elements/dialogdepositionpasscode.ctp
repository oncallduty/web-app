<div id="dialogDepositionPasscode">
	<div class="popupDialog">
		<div class="dialogMsg">
			<div class="dialogMsgInnerWrap">
			<div class="dialogTag">Start Date: <span id="startDate"></span></div>
			<div class="dialogTag">Passcode: <span id="passcode"></span></div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
<!-- //

function showDialogDepositionPasscode( startDate, passcode, left ) {
	$('#dialogDepositionPasscode #startDate').text( startDate );
	$('#dialogDepositionPasscode #passcode').text( passcode );
	$('#dialogDepositionPasscode').on( 'click', dismissDialogDepositionPasscode );
	$('#dialogDepositionPasscode').show();
	$('#dialogDepositionPasscode .popupDialog').css( 'left', (left + 10) + 'px' );
}

function dismissDialogDepositionPasscode( event ) {
	if (event.target.id !== $('#dialogDepositionPasscode')[0].id) {
		event.stopPropagation();
		return;
	}
	$('#dialogDepositionPasscode').off( 'click', dismissDialogDepositionPasscode );
	$('#dialogDepositionPasscode #popupDialog').off( 'click' );
	$('#dialogDepositionPasscode').fadeOut( 200 );
}

// -->
</script>