<div id="dialogTooltip" data-tag="" style="display:none;">
	<div class="wrap">
		<h2></h2>
		<div class="dialogMsg"></div>
	</div>
</div>
<script type="text/javascript">
//dialogtooltip
<?php $this->Html->scriptStart( ['inline'=>FALSE] ); ?>
function DialogTooltip()
{
	var dialogTooltip = $('#dialogTooltip');
	var dialogHeading = $('h2', dialogTooltip);
	var dialogMsg = $('.dialogMsg', dialogTooltip);
	var defaultFadeInterval = 100;
	var minFadeInterval = 50;
	var maxFadeInterval = 2000;
	var defaultDelayInterval = 3000;
	var minDelayInterval = 1000;
	var maxDelayInterval = 8000;
	var _fadeInterval;
	var _delayInterval;
	var bind = this;

	this.show = function( title, msg, fadeInterval, delayInterval, disableAutoDismiss )
	{
		_fadeInterval = defaultFadeInterval;
		_delayInterval = defaultDelayInterval;
		title = (typeof title === 'string') ? title : '';
		msg = (typeof msg === 'string') ? msg : '';
		disableAutoDismiss = !!disableAutoDismiss;
		if( typeof fadeInterval !== 'undefined' && fadeInterval !== null ) {
			var _fade = parseInt( fadeInterval );
			if( !isNaN( _fade ) && _fade >= minFadeInterval && _fade <= maxFadeInterval ) {
				_fadeInterval = _fade;
			}
		}
		if( typeof delayInterval !== 'undefined' && delayInterval !== null ) {
			var _delay = parseInt( delayInterval );
			if( !isNaN( _delay ) && _delay >= minDelayInterval && _delay <= maxDelayInterval ) {
				_delayInterval = _delay;
			}
		}
		dialogHeading.text( title );
		dialogMsg.html( msg );
		dialogTooltip.fadeIn( _fadeInterval );
		if( !disableAutoDismiss ) {
			window.setTimeout( function() {
				bind.dismiss( _fadeInterval );
			}, _delayInterval );
		}
	};

	this.dismiss = function( fadeInterval )
	{
		_fadeInterval = defaultFadeInterval;
		if( typeof fadeInterval !== 'undefined' && fadeInterval !== null ) {
			var _fade = parseInt( fadeInterval );
			if( !isNaN( _fade ) && _fade >= minFadeInterval && _fade <= maxFadeInterval ) {
				_fadeInterval = _fade;
			}
		}
		dialogTooltip.fadeOut( _fadeInterval, function() {
			dialogHeading.empty();
			dialogMsg.empty();
		} );
	};
}

window.top.dialogTooltip = new DialogTooltip();
<?php $this->Html->scriptEnd(); ?>
</script>