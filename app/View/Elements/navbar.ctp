<?php $authUser = $this->Session->read( 'Auth.User' ); ?>
<div id="ipad_navbar" class="hideSocialList" style="display:block;">

	<div class="background">
		<div class="toolbar_wrap">

			<table class="toolbar_table">
				<tbody>
					<tr>
						<td class="toolbar_left">
							<div class="navbtn_back toolbar_btn">
								<div class="back_btn" title="Back">
									<p><img src="/asset/images/arr.png">&nbsp;Back</p>
								</div>
							</div>
						</td>
						<td class="toolbar_left">
							<div class="navbtn_cases toolbar_btn" style="display:none;">
								<div class="cases_btn">
									<p><img src="/asset/images/arr.png">&nbsp;Cases</p>
								</div>
							</div>
						</td>
						<td class="toolbar_left">
							<div class="navbtn_logout toolbar_btn" style="display:none;">
								<div class="logout_btn">
									<p>Logout</p>
								</div>
							</div>
						</td>
						<td class="toolbar_left">
							<div class="navbtn_join toolbar_btn" style="display:none;">
								<div class="join_btn">
									<p>Join</p>
								</div>
							</div>
						</td>
						<!--td class="toolbar_left">
							<div class="navbtn_fullscreen toolbar_btn " style="display:none;">
								<div class="fullscreen_btn" title="Full Screen"></div>
							</div>
						</td-->
						<td class="toolbar_left">
							<div class="navbtn_presentation toolbar_btn" style="display:none;">
								<div class="presentation_btn" title="Start/End Presentation"></div>
							</div>
						</td>
						<td class="toolbar_left">
							<div class="navbtn_pause toolbar_btn" style="display:none;">
								<div class="pause_btn" title="Pause"></div>
							</div>
						</td>
						<td class="toolbar_left">
							<div class="navbtn_play toolbar_btn" style="display:none;">
								<div class="play_btn" title="Play"></div>
							</div>
						</td>
						<td class="toolbar_left">
							<div class="navbtn_annotator toolbar_btn" style="display:none;">
								<div class="annotator_btn" title="Pass/Revoke Annotation Role"></div>
							</div>
						</td>
						<td class="toolbar_left">
							<div class="navbtn_download toolbar_btn" style="display:none;">
								<div class="download_btn" title="Download File"></div>
							</div>
						</td>
						<!--td class="toolbar_left">
							<div class="navbtn_send toolbar_btn" style="display:none;">
								<div class="email_btn" title="Email"></div>
							</div>
						</td-->
						<!--td class="toolbar_left">
							<div class="navbtn_search toolbar_btn" style="display:none;">
								<div class="search_btn" title="Search"></div>
							</div>
						</td-->
						
						<td class="toolbar_middle">
						
							
						</td>
					
						<td class="toolbar_right">
							<div class="navbtn_tutorial toolbar_btn" style="display:none;">
								<div class="tutorial_btn">
									<p>Tutorials</p>
								</div>
							</div>
						</td>
						<td class="toolbar_right">
							<div class="btn_app_shadow navbtn_cancel" style="display:none;">
								<p>Cancel</p>
							</div>
						</td>
						<td class="toolbar_right">
							<div class="btn_app_shadow navbtn_skipstamp green" style="display:none;">
								<p>Skip Stamp</p>
							</div>
						</td>
						<td class="toolbar_right">
							<div class="btn_app_shadow navbtn_introduce" style="display:none;">
								<p>Introduce</p>
							</div>
						</td>
						<td class="toolbar_right">
							<div class="btn_app_shadow navbtn_send" style="display:none;">
								<p>Email</p>
							</div>
						</td>
						<td class="toolbar_right">
							<div class="btn_app_shadow navbtn_save" style="display:none;">
								<p>Save</p>
							</div>
						</td>
						<td class="toolbar_right">			
									
							<?php if( $authUser !== null ): ?>

							   <div class="userprofile" style="display: block; float: right;">															   
									<div class="profileImage"><p><?="{$authUser['firstName'][0]}"?></p><span><?="{$authUser['firstName']} {$authUser['lastName']}"?></span></div>						  
							   </div>
							<?php endif; ?>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		
	</div> 
</div>
<script type="text/javascript">
//NavBar
<?php $this->Html->scriptStart( ['inline' => FALSE] ); ?>
function navBarReady()
{
//	console.log( 'navBar::onReady' );
	window.top.navBar = new NavBar();
}
window.top.webApp.onReady( navBarReady );

function NavBar()
{
	/**
	 *
	 * @param {jQuery} toolbarBtn
	 * @param {jQuery} button
	 * @param {Function} click
	 * @returns {Button}
	 */
	var Button = function( toolbarBtn, button, click )
	{
		var self = this;
		var _click = click;
		if( !toolbarBtn || typeof toolbarBtn !== 'object' || !toolbarBtn.jquery ) {
			return null;
		}
		if( !button || typeof button !== 'object' || !button.jquery ) {
			button = toolbarBtn;
		}
		this.enable = function()
		{
			toolbarBtn.show();
			if( typeof _click === 'function' ) {
				button.off( 'click' );
				button.on( 'click', _click );
			}
			if( isset( window.top.navBar ) ) {
				window.top.navBar.adjustTitle();
			}
			return self;
		};
		this.disable = function()
		{
			toolbarBtn.hide();
			button.off();
			if( isset( window.top.navBar ) ) {
				window.top.navBar.adjustTitle();
			}
			return self;
		};
		this.visible = function()
		{
			if( typeof _click === 'function' ) {
				button.off( 'click' );
				button.on( 'click', _click );
			}
			toolbarBtn.css( 'visibility', '' );
			if( isset( window.top.navBar ) ) {
				window.top.navBar.adjustTitle();
			}
			return self;
		};
		this.invisible = function()
		{
			button.off();
			toolbarBtn.css( 'visibility', 'hidden' );
			if( isset( window.top.navBar ) ) {
				window.top.navBar.adjustTitle();
			}
			return self;
		};
		this.intangible = function() {
			self.addClass('intangible');
			button.off( 'click' );
			button.on( 'click', jQuery.noop );
		};
		this.tangible = function() {
			if(self.hasClass('intangible')){
				self.removeClass('intangible');
			}
			if( typeof _click === 'function' ) {
				button.off( 'click' );
				button.on( 'click', _click );
			}
		};
		this.onClick = function( handler )
		{
//			console.log( 'NavBar::Button::onClick' );
			if( typeof handler === 'function' ) {
//				console.log( 'NavBar::Button::onClick -- using handler:', handler );
				button.off( 'click' );
				_click = handler;
				button.on( 'click', _click );
			} else {
				console.log( 'NavBar::Button::onClick -- invalid handler:', handler );
			}
			return self;
		};
		this.click = function()
		{
			button.trigger( 'click' );
		};
		this.label = function( label )
		{
			var p = $('p', button);
			if( typeof p !== 'undefined' && p !== null && p.length > 0 && typeof label === 'string' ) {
				p.text( label );
			}
			return self;
		};
		this.addClass = function( c )
		{
			button.addClass( c );
			return self;
		};
		this.removeClass = function( c )
		{
			button.removeClass( c );
			return self;
		};
		this.hasClass = function( c ) {
			return button.hasClass( c );
		}
	};

	var self = this;
	var allButtons = [];
	var tbLogo = $('#toolbar_logo');
	var tbTitle = $('.toolbar_middle .toolbar_title');
	var tbWelcome = $('.toolbar_right .welcome_user');
	var navToolbar = $('#ipad_navbar');
	this.editorActions = $('#ipad_navbar .editor_actions');
	this.editActions = $('#ipad_navbar .edit_actions');
	this.pageActions = $('#ipad_navbar .page_actions');
	this.searchUI = $('#ipad_navbar .search_ui');
	this.ButtonClass = Button;
	var isFullscreen = false;
	var tbDisplayTimeout = null;
	var canHideFullscreenToolbar = true;
	var tbAnimateDuration = 200;
	var tbHideDelay = 6000;
	this.pathname = null;
	this.adjustTitleOnShow = false;

	function isset( v ) {
		return (typeof v !== 'undefined' && v !== null);
	}

	this.adjustTitle = function _adjustTitle() {
		if( isset( tbTitle ) && tbTitle.is( ":visible" ) ) {
			window.top.navBar.hideTitle();
			window.top.navBar.showTitle();
		} else {
			this.adjustTitleOnShow = true;
		}
	};

	var hideToolbar = function( fast )
	{
		//console.log( 'navbar::hideToolbar(): fast = ' + fast, canHideFullscreenToolbar );
		if( tbDisplayTimeout !== null) {
			window.clearTimeout( tbDisplayTimeout );
			tbDisplayTimeout = null;
		}
		if( !canHideFullscreenToolbar ) {
			return;
		}
		if($("input#search_text").is(":focus")) {
			return;
		}
		var duration = tbAnimateDuration;
		if( typeof fast !== 'undefined' && fast !== null && !!fast ) {
			duration = 200;
		}
		navToolbar.animate( {"top":'-' + navToolbar.height() + 'px'}, duration, "swing", function() {
			navToolbar.hide();
		} );
		$('#social_list_container, #social_transcript_tab, #liveFeed', window.top.document).switchClass( 'classic fullscreenShort', 'fullscreenTall', {duration : tbAnimateDuration, easing : 'linear'} );
	};

	this.showToolbar = function()
	{
		console.log( 'navbar::showToolbar()', tbHideDelay );
		/*if( isFullscreen && window.top.webApp.isWitness() && !window.top.webApp.iAmAnnotator() && tbTitle.text() === '') {
			navToolbar.hide();
			return;
		}*/
		navToolbar.show();
		$('#social_list_container, #social_transcript_tab, #liveFeed', window.top.document).switchClass( 'classic fullscreenTall', 'fullscreenShort', {duration : tbAnimateDuration, easing : 'linear'} );
		navToolbar.animate( {"top":0}, tbAnimateDuration );
		if( tbDisplayTimeout !== null ) {
			window.clearTimeout( tbDisplayTimeout );
			tbDisplayTimeout = null;
		}
		if(isFullscreen) {
			tbDisplayTimeout = window.setTimeout( hideToolbar, tbHideDelay );
		}
		if(this.adjustTitleOnShow) {
			this.adjustTitle();
			this.adjustTitleOnShow = false;
		}
	};

	this.registerActivity = function(action) {

	console.log( 'this.registerActivity !isFullscreen :', !isFullscreen  );
	console.log( 'this.registerActivity action :', action  );

		/*if( !isFullscreen) {
			return;
		}*/

		if(action) {
			//self.showToolbar();
		}
		else {
			if( navToolbar.is(':visible') ) {
				hideToolbar();
			} else {
				self.showToolbar();
			}
		}
	};

	this.toggleToolbar = function( e )
	{
//		console.log( 'navbar::toggleToolbar()' );
		if( typeof window.top.lastEvent !== 'undefined' && window.top.lastEvent !== null ) {
			if( window.top.lastEvent.toElement === e.originalEvent.toElement && window.top.lastEvent.x == e.originalEvent.x && window.top.lastEvent.y === e.originalEvent.y ) {
				if( window.top.lastEvent.cancelBubble === true ) {
					e.stopPropagation();
					e.cancelBubble = true;
					return;
				}
			}
			delete window.top.lastEvent;
		}
		if( e.target.nodeName === "IMG" ) {
			e.stopPropagation();
			e.cancelBubble = true;
			return;
		}
		if( !isFullscreen ) {
			return;
		}
		if( navToolbar.is(':visible') ) {
			hideToolbar();
		} else {
			self.showToolbar();
		}
	};

	this.canHideToolbar = function( allow )
	{
		//console.log( 'navbar::canHideToolbar() allow = ' + allow );
		canHideFullscreenToolbar = !!allow;
		if( isFullscreen ) {
			if( tbDisplayTimeout !== null ) {
				window.clearTimeout( tbDisplayTimeout );
				tbDisplayTimeout = null;
			}
			if( canHideFullscreenToolbar ) {
				tbDisplayTimeout = window.setTimeout( hideToolbar, tbHideDelay );
			} else if( !navToolbar.is(':visible') ) {
				self.showToolbar();
			}
		}
	};

	var leaveFullscreen = function()
	{
//		console.log( 'NavBar::leaveFullscreen' );
		isFullscreen = false;
		window.clearTimeout( tbDisplayTimeout );
		navToolbar.css( 'top', 0 ).show();
		self.fullscreenButton.onClick( function() { window.top.navBar.enterFullscreen( true ); } );
		$('body', window.top.document).off();
		if( typeof window.top.iPadContainerFrame !== 'undefined' && window.top.iPadContainerFrame !== null ) {
			$('body', window.top.iPadContainerFrame.document).off();
		}
		window.top.webApp.exitFullscreen();
	};

	this.enterFullscreen = function( saveFullscreenMode )
	{
//		console.log( 'NavBar::enterFullscreen', saveFullscreenMode );
		saveFullscreenMode = !!saveFullscreenMode;
		isFullscreen = true;
		if( saveFullscreenMode ) {
			window.top.webApp.setItem( 'fullscreen', isFullscreen );
		}
		window.top.webApp.enterFullscreen();
		self.fullscreenButton.onClick( self.exitFullscreen );
//		$('.toolbar_left .navbtn_back .back_btn').on( 'click', leaveFullscreen );
		$('#content', window.top.document).off().on( 'click', self.toggleToolbar );
		$('#ipad_container', window.top.document).off().on( 'click', function(e) { e.stopPropagation(); } );
		$('#viewerContainer', window.top.iPadContainerFrame.document).off().on( 'click', self.toggleToolbar );
		//var pdfviewer = window.top.iPadContainerFrame.frames[0];
		//if( typeof pdfviewer !== 'undefined' && pdfviewer !== null ) {
		//	$('#viewerContainer', pdfviewer.document).off().on( 'click', self.toggleToolbar );
		//}
		navToolbar.bind( 'click', function( event ) { event.stopPropagation(); } );
		$('#fileSelect').bind( 'click', function( event ) { event.stopPropagation(); } );
		self.showToolbar();
	};

	this.exitFullscreen = function()
	{
//		console.log( 'NavBar::exitFullscreen' );
		leaveFullscreen();
		$('#social_list_container, #social_transcript_tab, #liveFeed', window.top.document).removeClass( 'fullscreenShort fullscreenTall' ).addClass( 'classic' );
		window.top.webApp.setItem( 'fullscreen', false );
	};

	this.showLogo = function()
	{
		tbTitle.hide();
		tbLogo.show();
	};
	this.hideLogo = function()
	{
		tbLogo.hide();
	};

	this.showTitle = function( title )
	{
		tbLogo.hide();
		if( typeof title === 'string' && title !== null ) {
			tbTitle.text( title );
		}
		tbTitle.hide();
		tbTitle.width( $("td.toolbar_middle").width() );
		tbTitle.show();
	};
	this.hideTitle = function()
	{
		tbTitle.hide();
	};
	this.setTitle = function( title )
	{
		tbTitle.text( title );
	};
	this.resizeTitle = function _resizeTitle() {
		tbTitle.hide();
		tbTitle.width( $("td.toolbar_middle").width() );
		tbTitle.show();
	};

	this.showWelcome = function()
	{
		tbWelcome.show();
	};
	this.hideWelcome = function()
	{
		tbWelcome.hide();
	};

	this.backButton = new Button( $('.navbtn_back'), $('.back_btn') );
	allButtons.push( this.backButton );

	this.casesButton = new Button( $('.navbtn_cases'), $('.cases_btn'), function() {
		window.top.iPadContainerFrame.location.assign( '/cases' );
	} );
	allButtons.push( this.casesButton );

	this.logoutButton = new Button( $('.navbtn_logout'), $('.logout_btn'), function() {
		$('#dialogLogout').fadeIn( 200 );
	} );
	allButtons.push( this.logoutButton );

	this.joinButton = new Button( $('.navbtn_join'), $('.join_btn'), function() {
		showDialogJoin();
	} );
	allButtons.push( this.joinButton );

	this.fullscreenButton = new Button( $('.navbtn_fullscreen'), $('.fullscreen_btn'), function() {
		self.enterFullscreen( true );
	} );
	allButtons.push( this.fullscreenButton );

	this.presentationButton = new Button( $('.navbtn_presentation'), $('.presentation_btn'), function() {
		window.top.webApp.presentationPromptStart();
	} );
	allButtons.push( this.presentationButton );

	this.presentationPauseButton = new Button( $('.navbtn_pause'), $('.pause_btn'), function() {
		window.top.webApp.presentationPause();
	} );
	allButtons.push( this.presentationPauseButton );

	this.presentationPlayButton = new Button( $('.navbtn_play'), $('.play_btn'), function() {
		window.top.webApp.presentationResume();
	} );
	allButtons.push( this.presentationPlayButton );

	this.annotatorButton = new Button( $('.navbtn_annotator'), $('.annotator_btn'), function() {
		window.top.webApp.passPresenter();
	} );
	allButtons.push( this.annotatorButton );

	this.downloadButton = new Button( $('.navbtn_download'), $('.download_btn'), function() {
		window.top.webApp.download();
	} );
	allButtons.push( this.downloadButton );

	this.searchButton = new Button( $('.navbtn_search'), $('.search_btn'), function() {
		window.top.webApp.search();
	} );
	allButtons.push( this.searchButton );

	this.emailButton = new Button( $('.navbtn_send'), $('.email_btn'), function() {
		window.top.webApp.send();
	} );
	allButtons.push( this.emailButton );

	this.introduceButton = new Button( $('.navbtn_introduce') );
	allButtons.push( this.introduceButton );

	this.skipstampButton = new Button( $('.navbtn_skipstamp'), null, function() {
		console.log( 'skipstampButton::click!' );
	} );
	allButtons.push( this.skipstampButton );

	this.cancelButton = new Button( $('.navbtn_cancel'), null, function() {
		console.log( 'cancelButton::click!' );
	} );
	allButtons.push( this.cancelButton );

	this.saveButton = new Button( $('.navbtn_save'), null, function() {
		var userType = window.top.webApp.getS( 'user.userType' );
		if( typeof userType === 'undefined' || userType === null) {
			return;
		}
		if(window.top.webApp.isWitness()) {
			window.top.webApp.share();
		} else {
			window.top.webApp.saveAs();
		}
	} );
	allButtons.push( this.saveButton );

	this.sendButton = new Button( $('.navbtn_send'), null, function() {
		window.top.webApp.send();
	} );
	allButtons.push( this.sendButton );

	this.tutorialButton = new Button( $('.navbtn_tutorial'), $('.tutorial_btn') );
	allButtons.push( this.tutorialButton );

	this.allDisable = function()
	{
		for( var b in allButtons ) {
			var btn = allButtons[b];
			if( typeof btn !== 'undefined' && btn !== null && btn instanceof Button ) {
				btn.disable();
			}
		}
		this.hideLogo();
		this.hideWelcome();
		this.editorActions.hide();
	};

	this.getPathname = function _getPathname() {
		return (typeof self.pathname === "string" ? self.pathname.toLowerCase() : "");
	};

	this.standardSavePdf = function() {
		var userType = window.top.webApp.getS( 'user.userType' );
		if( typeof userType === 'undefined' || userType === null) {
			return;
		}
		if(window.top.webApp.isWitness()) {
			window.top.webApp.share();
		} else {
			window.top.webApp.saveAs();
		}
	};

	//editor actions
	this.arrowButton = new Button( $('.editor_actions .arrow') );
	this.colorArrowButton = new Button( $('.editor_actions .colorArrow') );
	this.textSelectButton = new Button( $('.editor_actions .textSelect') );
	this.colorSelectButton = new Button( $('.editor_actions .colorSelect') );
	this.drawingButton = new Button( $('.editor_actions .drawing') );
	this.colorDrawButton = new Button( $('.editor_actions .colorDraw') );
	this.noteButton = new Button( $('.editor_actions .note') );
	this.colorNoteButton = new Button( $('.editor_actions .colorNote') );
	this.eraseButton = new Button( $('.editor_actions .erase') );
	this.undoButton = new Button( $('.editor_actions .undo') );
	// this.calloutButton = new Button( $('.editor_actions .callout') );
	this.thumbModeButton = new Button( $('.editor_actions .thumbMode') );
	this.pageModeButton = new Button( $('.editor_actions .pageMode') );
	this.clearAllButton = new Button( $('.editor_actions .removeAll') );

	//defaults
	var retryCount = 0;
	this.updateDefaults = function() {
		$('.selected').removeClass('selected');
		if( typeof window.top.iPadContainerFrame === 'undefined' || window.top.iPadContainerFrame === null ) {
			self.pathname = window.top.location.pathname;
		} else {
			try {
				self.pathname = window.top.iPadContainerFrame.location.pathname;
			} catch( err ) {
				console.log( err );
				return;	// try again later -- IE workaround
			}
		}
		self.allDisable();
		self.editorActions.hide();
		leaveFullscreen();
		window.top.webApp.presentationStatus();
		console.log( 'NavBar::updateDefaults() -- pathname:', self.pathname, 'retryCount:', retryCount, " session.class:", window.top.webApp.getS( "deposition.class" ) );
		var isTrialBinder = (window.top.webApp.getS( "deposition.class" ) === "Trial Binder");
		switch( self.getPathname() ) {
			case '/members':
			case '/nonmembers':
			case '/guests':
			case '/witness':
			case '/reset':
				self.showLogo();
				self.backButton.onClick( function() { window.location.replace( '/signin' ); } ).enable();
				break;
			case '/cases':
				self.showLogo();
				self.showWelcome();
				self.logoutButton.enable();
				self.joinButton.enable();
				if( typeof window.top.tutorialVideos === 'object' && window.top.tutorialVideos !== null ) window.top.tutorialVideos.bind();
				break;
			case '/cases/browse':
				self.showLogo();
				var redirectTo = (typeof Datacore.s.isJoin !== 'undefined' && !!Datacore.s.isJoin) ? '/i?redirect=/cases' : '/users/logout?redirect=/members&ul=1';
				self.backButton.onClick( function() { window.location.replace( redirectTo ); } ).enable();
				break;
			case '/depositions':
				self.showLogo();
				self.showWelcome();
				self.logoutButton.enable();
				var userType = window.top.webApp.getS( 'user.userType' );
				if( typeof userType !== 'undefined' && userType !== null && userType === 'M' ) {
					self.casesButton.enable();
				}
				if( typeof window.top.tutorialVideos === 'object' && window.top.tutorialVideos !== null ) window.top.tutorialVideos.bind();
				break;
			case '/depositions/pdf':
				
				if( Datacore.getS( 'depositionStatus' ) === 'I' && window.top.webApp.userIsSpeaker() === true ) {
					var mFile = window.top.Datacore.getFile( window.top.webApp.getS( 'viewerFile.ID' ) );
					var mCaseFolder = window.top.Datacore.getFolder( window.top.webApp.getS( 'viewerFile.caseFolderID' ) );
					if( !isTrialBinder && (isset(mFile) && !mFile.isExhibit) || (isset(mCaseFolder) && mCaseFolder.class==="Exhibit" && mCaseFolder.caseID > 0) ) {
						var fn = window.top.webApp.introduce;
						if( window.top.webApp.getS( 'deposition.class' ) === 'WitnessPrep' || window.top.webApp.getS( 'deposition.class' ) === 'WPDemo' ) {
							fn = window.top.webApp.witnessPrepIntroduce;
						}
						self.introduceButton.onClick( fn ).removeClass('green').addClass( 'dark' ).enable();
					}
					self.presentationButton.onClick(window.top.webApp.presentationPromptStart).visible().enable();
				} else if(Datacore.getS( 'depositionStatus' ) === 'I' && window.top.webApp.userIsSpeaker() !== true) {
					if(window.top.Datacore.s.presentationAvailable) {
						self.presentationButton.onClick(window.top.webApp.presentationJoinSave).visible().enable().addClass('active');
					} else {
						self.presentationButton.onClick(jQuery.noop).visible().enable().addClass('notActive');
					}
				}
				
				self.backButton.onClick( window.top.webApp.backButton ).enable();
				self.fullscreenButton.enable();
				self.downloadButton.enable();
				self.searchButton.enable();
				self.searchUI.hide();
				self.saveButton.onClick(self.standardSavePdf);
				self.sendButton.onClick(window.top.webApp.send);
				self.downloadButton.onClick(window.top.webApp.download);

				self.arrowButton.onClick(window.top.webApp.arrowButton).enable();
				self.colorArrowButton.onClick(window.top.webApp.colorArrowButton).enable();
				self.textSelectButton.onClick(window.top.webApp.textSelectButton).enable();
				self.colorSelectButton.onClick(window.top.webApp.colorSelectButton).enable();
				self.drawingButton.onClick(window.top.webApp.drawingButton).enable();
				self.colorDrawButton.onClick(window.top.webApp.colorDrawButton).enable();
				self.noteButton.onClick(window.top.webApp.noteButton).enable();
				self.colorNoteButton.onClick(window.top.webApp.colorNoteButton).enable();
				self.eraseButton .onClick(window.top.webApp.eraseButton).enable();
				self.undoButton.onClick(window.top.webApp.undoButton).enable();
				// self.calloutButton.onClick(window.top.webApp.calloutButton).enable();
				self.thumbModeButton.onClick(window.top.webApp.thumbMode).enable();
				self.pageModeButton.onClick(window.top.webApp.pageMode);
				self.clearAllButton.onClick(window.top.webApp.clearAllButton).enable();

				var userType = window.top.webApp.getS( 'user.userType' );
				console.log( '>>>>>>>>>>>USER TYPE:', userType);
				if( userType !== 'WM' && userType !== 'W' ) {
					self.sendButton.enable();
					self.enterFullscreen( true );
					console.log( '>>>>>>>>>>>USER TYPE:', userType);
				} else {
					self.presentationButton.invisible();
					// self.calloutButton.disable();
					self.saveButton.addClass('dark');
					self.enterFullscreen( true );
				}
				self.editorActions.show();
				if( userType !== 'G' ) {
					self.saveButton.enable();
				}
				if( userType === 'W' ) {
					self.enterFullscreen( true );
					self.fullscreenButton.disable();
					self.sendButton.disable();
					self.downloadButton.disable();
					self.searchButton.disable();
					self.arrowButton.disable();
					self.colorArrowButton.disable();
					self.noteButton.disable();
					self.colorNoteButton.disable();
					self.eraseButton.disable();
					self.undoButton.disable();
					self.thumbModeButton.disable().addClass('restrict');
					self.pageModeButton.disable().addClass('restrict');
					self.clearAllButton.disable();
				} else {
					self.noteButton.enable();
					self.colorNoteButton.enable();
				}
				if( userType === 'W' || JSON.parse( window.top.webApp.getItem( 'fullscreen' ) ) === true ) {
					self.enterFullscreen( true );
					$('#social_list_container, #social_transcript_tab, #liveFeed', window.top.document).switchClass( 'classic fullscreenTall', 'fullscreenShort', {duration : tbAnimateDuration, easing : 'linear'} );
				} else {
					leaveFullscreen();
					$('#social_list_container, #social_transcript_tab, #liveFeed', window.top.document).switchClass( 'fullscreenShort fullscreenTall', 'classic', {duration : tbAnimateDuration, easing : 'linear'} );
				}
				if( ["M"].indexOf( userType ) >= 0 ) {
					if( typeof window.top.fileSelection !== "undefined" && window.top.fileSelection !== null ) {
						window.top.fileSelection.enableFileSelection();
					}
				}
				break;
			case '/depositions/introduce':
				leaveFullscreen();
				if( Datacore.getS( 'depositionStatus' ) === 'I' && window.top.webApp.userIsSpeaker() === true ) {
					var mFile = window.top.Datacore.getFile( window.top.webApp.getS( 'viewerFile.ID' ) );
					if( typeof mFile !== 'undefined' && mFile !== null && !mFile.isExhibit ) {
						//var fn = window.top.webApp.introduce;
						self.skipstampButton.enable();
						if( window.top.webApp.getS( 'deposition.class' ) === 'WitnessPrep' || window.top.webApp.getS( 'deposition.class' ) === 'WPDemo' ) {
							//fn = window.top.webApp.witnessPrepIntroduce;
							self.skipstampButton.disable();
						}
						self.introduceButton.removeClass('dark').enable(); // remove onclick
						self.cancelButton.enable();
					}
					// self.presentationButton.visible().enable(); // TODO check with ipad
				}
				self.arrowButton.disable();
				self.colorArrowButton.disable();
				self.textSelectButton.disable();
				self.colorSelectButton.disable();
				self.drawingButton.disable();
				self.colorDrawButton.disable();
				self.noteButton.disable();
				self.colorNoteButton.disable();
				self.eraseButton.disable();
				self.undoButton.disable();
				self.thumbModeButton.disable();
				self.pageModeButton.disable();
				self.clearAllButton.disable();
				self.backButton.onClick( window.top.webApp.backButton ).enable();
				self.fullscreenButton.disable();
				self.downloadButton.disable();
				self.searchButton.disable();
				// self.calloutButton.disable();
				self.backButton.onClick( window.top.webApp.backButton ).enable();
				self.searchUI.hide();
				self.showTitle();
				if( typeof window.top.fileSelection !== 'undefined' && window.top.fileSelection !== null ) {
					window.top.fileSelection.disableFileSelection();
				}
				break;
			case '/depositions/presentation':
				if( !isFullscreen ) {
					self.enterFullscreen( false );
				}
				if( window.top.webApp.isWitness() ) {
					self.backButton.disable();
				} else {
					self.backButton.onClick( window.top.webApp.backButton ).enable();
				}
				var pData = window.top.webApp.getS( 'joinPresentationData' );
				var pFile;
				if( typeof pData !== 'undefined' && pData !== null ) {
					if( typeof pData.title === 'string' && !window.top.webApp.userIsSpeaker() ) {
						self.setTitle( pData.title );
					}
					pFile = window.top.Datacore.getFile( pData.fileID );
				}
				if( window.top.webApp.userIsSpeaker() === true ) {
					self.setTitle( pFile ? pFile.name : '' );
					self.presentationButton.addClass( 'active' ).visible().enable().onClick(function(){window.top.webApp.presentationEnd();});
					if(!!window.top.webApp.getS("presentationPaused")) {
						self.presentationPlayButton.enable();
					} else {
						self.presentationPauseButton.enable();
					}
					self.annotatorButton.removeClass( 'revoke' ).enable();
					self.searchButton.enable();
					$('.search_ui_left').css('width',540);
					$('#ipad_container.portrait .search_ui_wrap > .search_ui_left').css('width',270);
					var introduceFn = (window.top.webApp.getS( 'deposition.class' ) === 'WitnessPrep' || window.top.webApp.getS( 'deposition.class' ) === 'WPDemo') ? window.top.webApp.witnessPrepIntroduce : window.top.webApp.introduce;
					self.introduceButton.onClick( introduceFn ).addClass( 'dark' );
					if( !isTrialBinder && typeof pFile !== 'undefined' && pFile !== null && !pFile.isExhibit ) {
						self.introduceButton.enable();
					} else {
						self.introduceButton.disable();
					}
					self.sendButton.onClick( window.top.webApp.send ).enable();
					self.saveButton.onClick( window.top.webApp.saveAs ).enable();
					self.arrowButton.onClick(window.top.webApp.arrowButton).enable();
					self.colorArrowButton.onClick(window.top.webApp.colorArrowButton).enable();
					self.textSelectButton.onClick(window.top.webApp.textSelectButton).enable();
					self.colorSelectButton.onClick(window.top.webApp.colorSelectButton).enable();
					self.drawingButton.onClick(window.top.webApp.drawingButton).enable();
					self.colorDrawButton.onClick(window.top.webApp.colorDrawButton).enable();
					//self.noteButton.onClick(window.top.webApp.noteButton).enable();
					self.colorNoteButton.onClick(window.top.webApp.colorNoteButton).enable();
					self.eraseButton .onClick(window.top.webApp.eraseButton).enable();
					self.undoButton.onClick(window.top.webApp.undoButton).enable();
					self.thumbModeButton.onClick(window.top.webApp.thumbMode).enable();
					self.pageModeButton.onClick(window.top.webApp.pageMode);
					self.clearAllButton.onClick(window.top.webApp.clearAllButton).enable();
					// self.calloutButton.onClick(window.top.webApp.calloutButton).enable();
					self.editorActions.show();
					self.noteButton.disable();
					self.colorNoteButton.disable();
					self.searchUI.hide();
				} else {
					// self.calloutButton.disable();
					if( typeof window.top.fileSelection !== 'undefined' && window.top.fileSelection !== null ) {
						window.top.fileSelection.disableFileSelection();
					}
				}
				self.showTitle();
				if( typeof pData !== 'undefined' && pData !== null ) {
					if( pData.annotatorID !== window.top.webApp.getS( 'user.ID' ) ) {
						self.presentationButton.invisible();
						self.introduceButton.disable();
						self.sendButton.disable();
						self.saveButton.disable();
					}
				}
				break;
			case '/depositions/video':
			case '/depositions/audio':
				// Check for race condition of the Multimedia object not being initialized
				if( typeof window.top.Multimedia === 'undefined' || window.top.Multimedia === null ) {
					console.log( 'NavBar::updateDefaults() - waiting for window.top.Multimedia to be initialized' );
					if ( ++retryCount < 50 ) {
						setTimeout( self.updateDefaults, 200 );
						return;
					}
					window.top.iPadContainerFrame.location.replace( '/depositions' );
					// window.top.Multimedia is still not defined
					return;
				}
				self.backButton.onClick( window.top.Multimedia.backButton ).enable();
				var introduceMode = !!window.top.Multimedia.isDragIntroduce;
				self.showTitle();
				if( introduceMode ) {
					window.top.navBar.introduceButton.onClick( window.top.Multimedia.introduce ).removeClass( 'dark' ).enable().click();
					window.top.navBar.skipstampButton.onClick( window.top.Multimedia.skipStamp ).enable();
					window.top.fileSelection.disableFileSelection();
				} else {
					self.downloadButton.onClick( window.top.Multimedia.downloadMultimediaFile ).enable();
					if( Datacore.getS( 'depositionStatus' ) === 'I' && window.top.webApp.userIsSpeaker() === true ) {
						var mFile = window.top.Datacore.getFile( window.top.Multimedia.fileID );
						console.log( "window.top.Datacore.getFile(" + window.top.Multimedia.fileID + ")" );
						if( typeof mFile !== 'undefined' && mFile !== null && !mFile.isExhibit ) {
							self.introduceButton.onClick( window.top.Multimedia.introduce ).addClass( 'dark' ).enable();
						}
					}
					if ( window.top.webApp.getS( 'user.userType' ) !== 'W' ) {
						self.downloadButton.onClick( window.top.Multimedia.downloadMultimediaFile ).enable();
					}
					if( window.top.webApp.getS( 'user.userType' ) !== 'WM' && window.top.webApp.getS( 'user.userType' ) !== 'W' ) {
						self.sendButton.onClick( window.top.Multimedia.sendMultimediaFile ).enable();
					}
					if(!window.top.webApp.isGuest() && !window.top.webApp.isWitness()) {
						self.saveButton.onClick( window.top.Multimedia.getSaveFilename ).enable();
					}
					if(!window.top.webApp.isWitness()) {
						window.top.webApp.toggleAttendPresentationBtn(!!window.top.Datacore.s.presentationAvailable);
					}
				}
				if( window.top.webApp.getS( 'deposition.class' ) === 'WitnessPrep' || window.top.webApp.getS( 'deposition.class' ) === 'WPDemo' ) {
					self.sendButton.disable();
					self.saveButton.onClick( window.top.Multimedia.getSaveFilename ).enable();
					self.skipstampButton.disable();
					self.introduceButton.addClass( 'dark' ).onClick( window.top.Multimedia.skipStamp );
				}
				break;
		}
		retryCount = 0;
	};
	this.updateDefaults();

	//listener
	$('iframe#ipad_container_frame').load( function() {
		console.log( 'Navbar::ipad_container_frame -- load:', window.top.iPadContainerFrame.location.pathname );
		self.updateDefaults();
	} );
}
<?php $this->Html->scriptEnd(); ?>
</script>
