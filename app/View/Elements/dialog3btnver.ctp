<div id="dialog3BtnVer">
	<div class="popupDialog">
		<div class="popupContents">
			<h4><!-- title --></h4>
			<div class="dialogMsg"><!-- description --></div>
		</div>
		<div class="dialogBtnWrap"></div>
	</div>
</div>
<script type="text/javascript">
function showDialog3BtnVer( title, description, buttons )
{
	$( '#dialog3BtnVer div.popupContents h4' ).html( (typeof title !== 'undefined' && title !== null ? title : '&nbsp;') );
	$( '#dialog3BtnVer div.dialogMsg' ).html( (typeof description !== 'undefined' && description !== null ? description : '&nbsp;') );

	if (typeof buttons === 'undefined' || buttons === null) {
		buttons = [];
	} else if (!$.isArray( buttons )){
		buttons = [buttons];
	}

	$( '#dialog3BtnVer div.dialogBtnWrap' ).empty();

	for (var b in buttons) {
		$( '#dialog3BtnVer div.dialogBtnWrap' ).append( prepareDialogButtonVer( buttons[b] ) );
	}

	$( '#dialog3BtnVer' ).show();
}

function dismissDialog3BtnVer() {
	$( '#dialog3BtnVer' ).fadeOut( 200 );
}

function prepareDialogButtonVer( btn ) {
	var btnId = (typeof btn.id !== 'undefined' && btn.id !== null ? ' id="'+btn.id+'"' : '');
	var btnClass = (typeof btn.class !== 'undefined' && btn.class !== null ? btn.class : 'app_bttn');
	var btnLabel = (typeof btn.label !== 'undefined' && btn.label !== null ? btn.label : '&nbsp;');
	var btnInactive = (typeof btn.inactive !== 'undefined' && Boolean( btn.inactive ) === true);
	var btnAction = '';
	if (btnInactive) {
		btnClass += ' inactive';
	} else {
		btnAction = ' onclick="'+(typeof btn.action !== 'undefined' && btn.action !== null ? btn.action : '')+'"';
	}
	if (btnClass.length > 0) {
		btnClass = ' class="'+btnClass+'"';
	}
	return '<div'+btnId+btnClass+btnAction+'><span>'+btnLabel+'</span></div>';
}
</script>
