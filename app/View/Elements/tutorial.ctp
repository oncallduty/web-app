<?php $tutorialVideos = $this->Session->check( 'User.tutorialVideos' ) ? $this->Session->read( 'User.tutorialVideos' ) : null; ?>

<div id="tutorialBackground" style="display:none;">
	<ul id="tutorialList"></ul>
</div>
<div id="tutorialContents" style="display:none;background-image: url('/img/ipad/000000-0.7.png');">
	<div id="tutorialHide" class="app_bttn"><span>Close</span></div>
	<div id="videoContainer" style="width:948px;height:711px;position:absolute;top:49px;left:34px;border:4px solid #000;border-radius:4px;margin:0;">
		<video id="tutorialVideo" width="100%" height="100%" controls preload="auto" style="display:block;">
			<source src="" type="video/mp4" />
		</video>
	</div>
</div>

<script type="text/javascript">
<?php $this->Html->scriptStart( ['inline'=>FALSE] ); ?>

function TutorialVideos( videos )
{
	var enabled = false;
	var videoList = [];
	var roles = [];
	if( typeof videos !== 'object' || videos === null || !jQuery.isArray( videos ) || videos.length <= 0 ) {
		console.log( 'no tutorial videos' );
	} else {
		enabled = true;
	}
	var tutorialWrap = $('#tutorialBackground');
	var tutorialList = $('#tutorialList');
	var tutorialContents = $('#tutorialContents');
	var tutorialVideo = $('#tutorialVideo');
	$('#tutorialHide > span').off().on( 'click', hidePlayer );

	function buildList() {
		roles = [];
		videoList = [];
		var cUser = top.webApp.getS( "user" );
		var userType = cUser.userType;
		switch( userType ) {
			case "M":
				roles.push( "Member" );
				if( top.webApp.userIsOwner() === true ) {
					roles.push( "Owner" );
				}
				if( cUser.clientTypeID === "CRT" ) {
					var isClientAdmin = (cUser.typeID === "C");
					if( isClientAdmin || cUser.typeID === "CU" ) {
						roles.push( "CourtOfficer" );
					}
					if( isClientAdmin || cUser.typeID === "RPT" ) {
						roles.push( "CourtReporter" );
					}
					if( isClientAdmin || cUser.typeID === "JDG" ) {
						roles.push( "Judge" );
					}
				}
				break;
			case 'G':
				roles.push( 'Guest' );
				break;
			case 'W':
				roles.push( 'Witness' );
				break;
			case 'WM':
				roles.push( 'TrustedWitness' );
				roles.push( 'Witness' );
				break;
		}
		for( v in videos ) {
			videoInfo = videos[v];
			for( r in roles ) {
				role = roles[r];
				if( videoInfo.roles.indexOf( role ) !== -1 ) {
					videoList.push( videoInfo );
					break;
				}
				delete r, role;
			}
			delete v, videoInfo;
		}
		videoList.sort( function( a, b ) {
			if( a.sort < b.sort ) {
				return -1;
			}
			if( a.sort > b.sort ) {
				return 1;
			}
			return 0;
		} );
		tutorialList.empty();
		for( v in videoList ) {
			videoInfo = videoList[v];
			li = $('<li></li>');
			label = $('<span></span>');
			label.append( videoInfo.label );
			li.append( label );
			li.on( 'click', {src:videoInfo.url}, function(event) {
				if (event && event.stopPropagation) {
					event.stopPropagation();
				}
				else if (window.event) {
					window.event.cancelBubble = true;
				}
				openPlayer( event.data.src );
			} );
			tutorialList.append( li );
			delete v, videoInfo, li, label;
		}
		enabled = ( videoList.length === 0 ) ? false : true;
	};

	function openPlayer( src )
	{
		hideMenu();
		$('source', tutorialVideo).attr( 'src', src );
		tutorialVideo.load();
		tutorialVideo.get( 0 ).play();
		tutorialVideo.off().on( 'ended', hidePlayer );
		tutorialContents.show();
	};

	function hidePlayer( event )
	{
		if(typeof event !== 'undefined') {
			if (event && event.stopPropagation) {
				event.stopPropagation();
			}
			else if (window.event) {
				window.event.cancelBubble = true;
			}
		}
		tutorialVideo.get( 0 ).pause();
		tutorialContents.fadeOut( 200 );
	};

	function showMenu()
	{
		tutorialWrap.show();
	};

	function hideMenu( event )
	{
		if(typeof event !== 'undefined') {
			if (event && event.stopPropagation) {
				event.stopPropagation();
			}
			else if (window.event) {
				window.event.cancelBubble = true;
			}
		}
		tutorialWrap.fadeOut( 200 );
	};

	this.bind = function()
	{
		buildList();
		if( enabled ) {
			tutorialWrap.off().on( 'click', hideMenu );
			top.navBar.tutorialButton.onClick( showMenu ).enable();
		}
	};
}
var tutorialVideos = new TutorialVideos( <?=json_encode( $tutorialVideos, JSON_UNESCAPED_SLASHES );?> );

<?php $this->Html->scriptEnd(); ?>
</script>
