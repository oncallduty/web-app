<div id="dialogSearch" data-tag="">
	<div class="popupDialog">
		<div class="loading"></div>
		<h2></h2>
		<div class="dialogMsg">
			<div class="dialogMsgInnerWrap"></div>
		</div>
		<div class="dialogOkBtnWrap">
			<button id="dialogSearchClose" onclick="cancelSearch()"></button>
		</div>
	</div>
</div>
<script type="text/javascript">
var cancelFunction = null;

function showSearchDialog( title, description, buttonLabel ) {
	$('#dialogSearch div.popupDialog h2').html( title );
	$('#dialogSearch div.dialogMsg div.dialogMsgInnerWrap').html( '<span>'+description+'</span>' );
	$('#dialogSearchClose').removeClass().addClass( 'app_bttn_new' ).html( '<span>'+buttonLabel+'</span>' );
	$('#dialogSearch').show();
	searchDialogCalculatePosition();
	$('#dialogSearchClose').focus();
}

function dismissSearchDialog() {
	$('#dialogSearch').fadeOut( 200 );
}

function cancelSearch() {
	cancelFunction();
	$('#dialogSearch').fadeOut( 200 );
}

function setCancelSearchFunction( action ) {
	cancelFunction = action;
}

function searchDialogCalculatePosition() {
	var halfWidth = $('#dialogSearch .popupDialog').width() / 2;
	$('#dialogSearch .popupDialog').css( 'margin', 'auto -'+halfWidth+'px' );
}
</script>