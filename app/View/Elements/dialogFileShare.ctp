<div id="dialogFileShare">
	<div class="popupDialog">
		<h2>Share Document</h2>
		<div class="dialogMsg">
			<div class="dialogMsgInnerWrap">
				<span>Please provide a name for the document you are sharing:</span>
			</div>
		</div>
		<div class="dialogForm">
			<input type="text" name="shareFileName" id="shareFileName" value="" />
		</div>
		<div class="dialogActions">
			<button id="dialogFileShareCancel" class="btn_app_cancel" onclick="cancelDialogFileShare()">Cancel</button>
			<button id="dialogFileShareSubmit" class="btn_app_normal" onclick="submitDialogFileShare()">Share</button>
		</div>
	</div>
</div>
<script type="text/javascript">
<!-- //
var fsSubmitFunction = null;

function showDialogFileShareBtn( requestType, title, description, objName ) {
	if (typeof requestType === 'undefined') {
		console.log( 'requestType is required in showDialogFileShareBtn()' );
		return;
	}

	$('#dialogFileShare div.dialogForm').hide();

	if (typeof title !== 'undefined' && title !== null && title.length > 0) {
		$('#dialogFileShare div.popupDialog h2').html( title );
	}

	if (typeof description !== 'undefined' && description !== null && description.length > 0) {
		$('#dialogFileShare div.dialogMsg div.dialogMsgInnerWrap').html( '<span>'+description+'</span>' );
	}

	if (requestType === 'file' && typeof objName !== 'undefined' && objName !== null && objName.length > 0) {
		$('#dialogFileShare div.dialogForm').show();
		$('#dialogFileShare div.dialogForm input').val( objName );
	}

	$('#dialogFileShare').fadeIn( 100 );
	$('#dialogFileShare div.dialogForm input').focus();
}

function cancelDialogFileShare() {
	clearDialogFileShareSubmitAction();
	$('#dialogFileShare').fadeOut( 200 );
}

function submitDialogFileShare() {
	if (typeof fsSubmitFunction === 'function') {
		fsSubmitFunction();
		clearDialogFileShareSubmitAction();
	}

	$('#dialogFileShare').fadeOut( 200 );
}

function setDialogFileShareSubmitAction( action ) {
	fsSubmitFunction = action;
}

function clearDialogFileShareSubmitAction() {
	fsSubmitFunction = null;
}
// -->
</script>