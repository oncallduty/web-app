<div id="colorpicker_background">
	<div id="colorpicker">
		<h2>Select a Color</h2>
		<div class="color-line">
			<div class="border">
				<div id="red" class="color-square" style="background-color: #FF0000"></div>
			</div>
			<div class="border">
				<div id="green" class="color-square" style="background-color: #00FF00"></div>
			</div>
			<div class="border">
				<div id="blue" class="color-square" style="background-color: #0080FF"></div>
			</div>
			<div class="border">
				<div id="magenta" class="color-square" style="background-color: #FF00FF"></div>
			</div>
		</div>
		<div style="clear: both;"></div>
		<div class="color-line">
			<div class="border">
				<div id="orange" class="color-square" style="background-color: #FCB643"></div>
			</div>
			<div class="border">
				<div id="yellow" class="color-square" style="background-color: #FFFF00"></div>
			</div>
			<div class="border">
				<div id="black" class="color-square" style="background-color: #000000"></div>
			</div>
			<div class="border">
				<div id="cyan" class="color-square" style="background-color: #00FFFF"></div>
			</div>
			<div class="border">
				<div id="indigo" class="color-square" style="background-color: #8000FF"></div>
			</div>
		</div>
		<div style="clear: both;"></div>
		<div id="button-container">
			<div id="default-color">Default</div>
		</div>
	</div>
</div>

<script type="text/javascript">
<?php $this->Html->scriptStart( array( 'inline' => false ) ); ?>

var currentColor = null;
var defaultColor = "#ff0000";
var closeFunction = null;

function showColorPicker(x, y, selectedColor, closeFunc, dfltColor, hide_black) {
	$('#colorpicker').css({"left": x, "top": y});
	$('.border').removeClass("selected");
	if (typeof hide_black === "boolean" && hide_black) {
		$('#colorpicker #black').parent().hide();
		$('#colorpicker #cyan').parent().show();
	} else {
		$('#colorpicker #black').parent().show();
		$('#colorpicker #cyan').parent().hide();
	}
	setColor(selectedColor);
	$('#colorpicker_background').show();
	defaultColor = dfltColor;
	closeFunction = closeFunc;
};
window.top.showColorPicker = showColorPicker;

function setColor(newColor) {
	$('.border').removeClass("selected");
	var colorDiv = $('<div>').css("background-color", newColor);
	$('.border div:first-child').each(function () {
		if ($(this).css("background-color") === colorDiv.css("background-color")) {
			$(this).parent().addClass("selected");
		}
	});
	currentColor = newColor;
}

function close_function() {
	if (typeof closeFunction === 'function') {
		closeFunction(currentColor);
		closeFunction = null;
	}
	$('#colorpicker_background').hide();
}

$(document).ready( function() {
	$('.border').on("click", function () {
		var c = $(this).children(".color-square").css("background-color");
		setColor(c);
		close_function();
	});

	$('#button-container').on("click", function() {
		$('.border').removeClass("selected");
		setColor(defaultColor);
		close_function();
	});

	$('#colorpicker_background').on("click", close_function);
});
<?php $this->Html->scriptEnd( array( 'inline' => false ) ); ?>
</script>
