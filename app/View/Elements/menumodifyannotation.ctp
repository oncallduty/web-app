<div id="menuModifyAnnotation">
	<div class="popupDialog">
		<div>
			<div id="copyText" class="modifyButton leftButton">Copy Text</div>
			<div id="color" class="modifyButton">Color...</div>
			<div id="thickness" class="modifyButton">Thickness...</div>
			<div id="opacity" class="modifyButton">Opacity...</div>
			<div id="delete" class="modifyButton rightButton">Delete</div>
		</div>
	</div>
</div>
<?php $this->Html->scriptStart( array( 'inline' => false ) ); ?>

var closeFunction = null;
var downFunction = null;
var modifyDrawObj = null;

function showMenuModifyAnnotation( drawObj, offset, closeFunc, downFunc ) {
	updateMenu( drawObj );
	$('#menuModifyAnnotation').fadeIn( 200 );
	$('#menuModifyAnnotation .modifyButton').on( "click", dismissMenuModifyAnnotation );
	$('#menuModifyAnnotation').on( 'mousedown', mousedownMenuModifyAnnotation );
	positionMenu( $('#menuModifyAnnotation .popupDialog'), drawObj, offset );
	closeFunction = closeFunc;
	downFunction = downFunc;
	modifyDrawObj = drawObj
}

function dismissMenuModifyAnnotation( event ) {
	$('#menuModifyAnnotation').off();
	$('#menuModifyAnnotation .modifyButton').off();
	$('#menuModifyAnnotation').fadeOut( 200 );
	if (typeof closeFunction === 'function') {
		closeFunction( event.target.id, modifyDrawObj );
		closeFunction = null;
		downFunction = null;
	}
}

function mousedownMenuModifyAnnotation( event ) {
	if (typeof downFunction === 'function') {
		var close = downFunction( modifyDrawObj, event );
		if (close) {
			closeFunction = null;
			downFunction = null;
			$('#menuModifyAnnotation').off();
			$('#menuModifyAnnotation .modifyButton').off();
			$('#menuModifyAnnotation').hide();
		}
	}
}

function updateMenu( drawObj ) {
	if (drawObj.drawType === 'highlight') {
		$('#copyText').hide();
		$('#thickness').hide();
		$('#color').addClass( 'leftButton' );
    } else {
		$('#copyText').hide();
		$('#thickness').show();
		$('#color').addClass( 'leftButton' );
    }
}

<?php $this->Html->scriptEnd( array( 'inline' => false ) ); ?>