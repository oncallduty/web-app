<div id="dialogSelectGroups">
	<div class="popupDialog">
		<div id="outerFrame">
			<div class="dialogGroup">
				<ul id="groupList">
				</ul>
			</div>
		</div>
	</div>
</div>
<?php $this->Html->scriptStart( array( 'inline' => false ) ); ?>
var gsSubmitFunction = null;

function showDialogSelectGroups( x, y, groups ) {
	$('#dialogSelectGroups .popupDialog').css({"left": x, "top": y, "margin": 'auto'});
	$('#groupList').empty();

	if (typeof groups === 'undefined' || groups === null) {
		console.log( 'No groups passed in to showDialogSelectGroups()' );
	}

	// Add groups to list and select All Attendees.
	for( var i = 0; i < groups.length; i++ ) {
		var html = '<li onclick="submitDialogSelectGroups(\'' + groups[i][1] + '\')">' + groups[i][0] + '</li>';
		$('#groupList').append( html );
	}
	$('#dialogSelectGroups').on( 'click', cancelDialogSelectGroups );

	$('#dialogSelectGroups').fadeIn( 100 );
}

function cancelDialogSelectGroups() {
	clearDialogSelectGroupsSubmitAction();
	$('#groupList').off();
	$('#dialogSelectGroups').off();
	$('#dialogSelectGroups').fadeOut( 200 );
}

function submitDialogSelectGroups( group ) {
	if (typeof gsSubmitFunction === 'function') {
		gsSubmitFunction( group );
		clearDialogSelectGroupsSubmitAction();
	}

	$('#groupList').off();
	$('#dialogSelectGroups').off();
	$('#dialogSelectGroups').fadeOut( 200 );
}

function setDialogSelectGroupsSubmitAction( action ) {
	gsSubmitFunction = action;
}

function clearDialogSelectGroupsSubmitAction() {
	gsSubmitFunction = null;
}

<?php $this->Html->scriptEnd( array( 'inline' => false ) ); ?>
