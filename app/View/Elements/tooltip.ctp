<div id="tooltip_wrap">
	<div id="tooltip">Insert witty phrase here</div>
</div>
<script type="text/javascript">
//tooltip
<?php $this->Html->scriptStart( ['inline'=>FALSE] ); ?>
function showTooltip( msg, autoFadeOut, fadeTime, delayTime )
{
	fadeTime = isset( fadeTime ) ? fadeTime : 200;
	delayTime = isset( delayTime ) ? delayTime : 2000;
	$('#tooltip').text( msg );
	//window.setTimeout( function() { $('#tooltip_wrap').animate( {opacity:0}, 200 ); }, 2000 );
	$('#tooltip_wrap').fadeIn( fadeTime );
	if (typeof autoFadeOut !== 'undefined' && autoFadeOut) {
		window.setTimeout( function() {
			dismissTooltip( fadeTime );
		}, delayTime );
	}
}

function dismissTooltip( fadeTime )
{
	fadeTime = isset( fadeTime ) ? fadeTime : 200;
	// $('#tooltip_wrap').animate( {opacity:0}, 200, function(){ $('#tooltip_wrap').attr( 'display', 'none' ); } );
	$('#tooltip_wrap').fadeOut( fadeTime );
}

<?php $this->Html->scriptEnd(); ?>
</script>
