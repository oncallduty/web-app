<div id="fullscreenToolBar" style="display: none;">
	<div class="background">
		<div id="ipad_toolbar">
			<div id="toolbar_left">
				<div id="navbar_container">
					<div id="ipad_navbar_back" class="toolbar_btn">
						<a href="javascript:;"><div id="ipad_navbtn_back"><p>Back</p></div></a>
					</div>
					<div id="ipad_navbar_normalscreen" class="toolbar_btn">
						<div title="Classic Screen" id="ipad_navbtn_normalscreen"><p>&nbsp;</p></div>
					</div>
					<div id="ipad_navbar_full_presentation" class="toolbar_btn">
						<div title="Start/End Presentation" id="ipad_navbtn_full_presentation"><p>&nbsp;</p></div>
					</div>
					<div id="ipad_navbar_full_pass_annotation" class="toolbar_btn">
						<div title="Pass/Revoke Annotation Role" id="ipad_navbtn_full_pass_annotation"><p>&nbsp;</p></div>
					</div>
					<div id="ipad_navbar_download" class="toolbar_btn">
						<div title="Download File" id="ipad_navbtn_download"><p>&nbsp;</p></div>
					</div>
				</div>
			</div>
			<div id="toolbar_middle">
				<div id="toolbar_title"></div>
			</div>
			<div id="toolbar_right">
				<div id="ipad_navbtn_introduce" class="btn_app_normal"><p>Introduce</p></div>
				<div id="ipad_navbtn_send" class="btn_app_normal"><p>Email</p></div>
				<div id="ipad_navbtn_save" class="btn_app_normal"><p>Save</p></div>
			</div>
		</div>
		<?=$this->element( 'navbar' );?>
		<div id="editor_actions">
			<div id="edit_actions">
				<div title="Arrow" id="arrow" class="edit_btn round_left">
					<div id="arrow_icon"></div>
				</div>
				<div title="Select Color" id="colorArrow" class="edit_btn round_right">
					<div id="color_icon"></div>
				</div>
				<div title="Highlight" id="textSelect" class="edit_btn round_left">
					<div id="select_icon"></div>
				</div>
				<div title="Select Color" id="colorSelect" class="edit_btn round_right">
					<div id="color_icon"></div>
				</div>
				<div title="Pencil" id="drawing" class="edit_btn round_left">
					<div id="drawing_icon"></div>
				</div>
				<div title="Select Color" id="colorDraw" class="edit_btn round_right">
					<div id="color_icon"></div>
				</div>
				<div title="Note" id="note" class="edit_btn round_left">
					<div id="note_icon">ABC</div>
				</div>
				<div title="Select Color" id="colorNote" class="edit_btn round_right">
					<div id="color_icon"></div>
				</div>
				<div title="Erase" id="erase" class="edit_btn">
					<div id="erase_icon"></div>
				</div>
				<div title="Undo" id="undo" class="edit_btn">
					<div id="undo_icon"></div>
				</div>
			</div>
			<div id="page_actions">
				<div title="Clear All Annotations" id="removeAll" class="edit_btn">Clear</div>
			</div>
		</div>
	</div>
</div>

<?php $this->Html->scriptStart( array( 'inline' => false ) ); ?>

var fullScreenToolBarHeight = 0;
var fullScreenToolBarCanHide = true;
var fullScreenToolBarAnimateTime = 600;
var fullScreenToolBarHideTime = 8000;
var fullScreenToolBarTimeoutID = null;

function showFullscreenToolBar( title ) {
	if (DC.s.userType === 'W') {
		$('#erase').hide();
		$('#undo').hide();
		$('#removeAll').hide();
		$('#ipad_navbar_download').hide();
		$('#ipad_navbtn_send').hide();
	}
	if (DC.s.userType === 'G') {
		$('#ipad_navbtn_save').hide();
	}
	if( DC.s.speakerID === DC.s.currentUserID ) {
		// Change the color of the presentation icon to show whether there is a presentation of not
		if( typeof DC.s.joinPresentationData !== 'undefined' && DC.s.joinPresentationData !== null ) {
			$('#ipad_navbtn_full_presentation').addClass( 'active' );
		}
		else {
			$('#ipad_navbtn_full_presentation').removeClass( 'active' );
		}
	}
	$('#toolbar_title').text(title);
	$('#fullscreenToolBar').show();
	$('#fullscreenToolBar').animate( {top: 0}, fullScreenToolBarAnimateTime );
	if (fullScreenToolBarTimeoutID !== null) {
		window.clearTimeout( fullScreenToolBarTimeoutID );
		fullScreenToolBarTimeoutID = null;
	}
	fullScreenToolBarTimeoutID = window.setTimeout( dismissFullscreenToolBar, fullScreenToolBarHideTime );
}

function dismissFullscreenToolBar( fast ) {
	if (fullScreenToolBarTimeoutID !== null) {
		window.clearTimeout( fullScreenToolBarTimeoutID );
		fullScreenToolBarTimeoutID = null;
	}
	if (!fullScreenToolBarCanHide) {
		return;
	}

	var animationTime = fullScreenToolBarAnimateTime;
	if (typeof fast !== 'undefined' && fast !== null) {
		animationTime = 1;
	}
	$('#fullscreenToolBar').animate( {top: '-' + fullScreenToolBarHeight + 'px'}, animationTime, "swing", function() {
		$('#fullscreenToolBar').hide();
	});
}

function isShowingFullscreenToolBar() {
	return 	$('#fullscreenToolBar').is( ':visible' );
}

function setCanHideFullscreenToolBar( canHide ) {
	fullScreenToolBarCanHide = canHide;
	if (canHide && isShowingFullscreenToolBar()) {
		fullScreenToolBarTimeoutID = window.setTimeout( dismissFullscreenToolBar, fullScreenToolBarHideTime );
	}
	if (!canHide && fullScreenToolBarTimeoutID !== null) {
		window.clearTimeout( fullScreenToolBarTimeoutID );
		fullScreenToolBarTimeoutID = null;
	}
}

function FullTitleActions () {
	this.btn_presentation_display = false;
}

FullTitleActions.prototype.showPresentation = function() {
	this.btn_presentation_display = true;
};

FullTitleActions.prototype.hidePresentation = function() {
	this.btn_presentation_display = false;
};

FullTitleActions.prototype.presentationActive = function( active ) {
	if (parent.isset( active ) && active ) {
		$('#ipad_navbtn_full_presentation').addClass( 'active' );
	} else {
		$('#ipad_navbtn_full_presentation').removeClass( 'active' );
	}
}

FullTitleActions.prototype.showAll = function() {
	this.showPresentation();
}

FullTitleActions.prototype.hideAll = function() {
	this.hidePresentation();
}
FullTitleActions.prototype.ready = function() {
	return (this.btn_logout_display || this.btn_cases_display || this.btn_back_display);
};

FullTitleActions.prototype.redraw = function() {
	if (this.btn_presentation_display) {
		$('#ipad_navbar_full_presentation').css( 'display', 'inline-block' );
	}
};

var _fullTitleActions = null;

$(document).ready( function() {
	_fullTitleActions = new FullTitleActions();
	fullScreenToolBarHeight = $('#fullscreenToolBar').height();
	$('#fullscreenToolBar').css( 'top', '-' + fullScreenToolBarHeight + 'px' );

	if (_fullTitleActions.ready()) {
		_fullTitleActions.redraw();
	}
});

<?php $this->Html->scriptEnd( array( 'inline' => false ) ); ?>