<div id="dialogLogout">
	<div class="popupDialog">
		<div class="popupContents">
			<h4>Logout</h4>
			<div class="dialogMsg">Are you sure you want to logout?</div>
		</div>
		<div class="dialogBtnWrap">
			<div id="btn_logout_cancel" class="app_bttn"><span>Cancel</span></div>
			<div id="btn_logout" class="app_bttn_new"><span>Yes</span></div>
		</div>
	</div>
</div>
<script type="text/javascript">
<?php $this->Html->scriptStart( array( 'inline' => false ) ); ?>
$('#btn_logout_cancel').click( function() {
	$('#dialogLogout').fadeOut( '200' );
} );

$('#btn_logout').click( function() {
	window.location.assign( '/users/logout?ul=1' );
} );

function showDialogLogout() {
	$('#dialogLogout').fadeIn( '200' );
}
<?php $this->Html->scriptEnd(); ?>
</script>