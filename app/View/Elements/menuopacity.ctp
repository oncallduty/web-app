<div id="menuOpacity" >
	<div class="popupDialog">
		<div class="title">Alpha</div>
		<div class="wrapper">
			<div class="selectBox">
				<div id="alpha25" class="border alpha"></div>
			</div>
			<div class="text25">25%</div>
		</div>
		<div class="wrapper">
			<div class="selectBox">
				<div id="alpha50" class="border alpha"></div>
			</div>
			<div class="text50">50%</div>
		</div>
		<div class="wrapper">
			<div class="selectBox">
				<div id="alpha75" class="border alpha"></div>
			</div>
			<div class="text75">75%</div>
		</div>
		<div class="wrapper">
			<div class="selectBox">
				<div id="alpha100" class="border alpha"></div>
			</div>
			<div class="text100">100%</div>
		</div>
	</div>
</div>
<?php $this->Html->scriptStart( array( 'inline' => false ) ); ?>

var opacityCloseFunction = null;
var opacityModifyDrawObj = null;

function showMenuOpacity( drawObj, offset, closeFunc ) {
	var opacity = 100;
	if( drawObj.drawType === 'marker' ) {
		$('#menuOpacity .text25').html( '20%' );
		$('#menuOpacity .text50').html( '35%' );
		$('#menuOpacity .text75').html( '50%' );
		$('#menuOpacity .text100').html( '65%' );
		opacity = (Math.round(((drawObj.opacity * 100) - 20) / 15) + 1 ) * 25;
	} else {
		$('#menuOpacity .text25').html( '25%' );
		$('#menuOpacity .text50').html( '50%' );
		$('#menuOpacity .text75').html( '75%' );
		$('#menuOpacity .text100').html( '100%' );
		opacity = Math.round( 4 * drawObj.opacity ) * 25;
	}
	$('#menuOpacity .selectBox').removeClass( 'selected' );
	$('#menuOpacity #alpha' + opacity).parent().addClass( 'selected' );
	$('#menuOpacity').fadeIn( 200 );
	positionMenu( $('#menuOpacity .popupDialog'), drawObj, offset );
	opacityCloseFunction = closeFunc;
	opacityModifyDrawObj = drawObj
}

function dismissMenuOpacity( event ) {
	$('#menuOpacity').fadeOut( 200 );
	if (typeof opacityCloseFunction === 'function') {
		var c = null;
		var clicked = $(this);
		if (clicked != null && clicked.attr( 'id' ).substr( 0, 5 ) === 'alpha' ) {
			c = parseInt( clicked.attr( 'id' ).substr( 5 ) );
			if ( isNaN( c ) ) {
				c = null;
			} else {
				c = Math.min( 100, Math.max( 25, c ) );
			}
		}
		opacityCloseFunction( c, opacityModifyDrawObj );
		opacityCloseFunction = null;
	}
}

$(document).ready(function() {
	$('#menuOpacity').on( 'click', dismissMenuOpacity );
	$('#menuOpacity .border').on( "click", dismissMenuOpacity );
});
<?php $this->Html->scriptEnd( array( 'inline' => false ) ); ?>