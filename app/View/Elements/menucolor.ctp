<div id="menuColor" >
	<div class="popupDialog">
		<div class="title">Color</div>
		<div>
			<div class="border">
				<div id="red" class="color-square" style="background-color: #FF0000"></div>
			</div>
			<div class="border">
				<div id="green" class="color-square" style="background-color: #00FF00"></div>
			</div>
			<div class="border">
				<div id="blue" class="color-square" style="background-color: #0080FF"></div>
			</div>
			<div class="border">
				<div id="magenta" class="color-square" style="background-color: #FF00FF"></div>
			</div>
			<div class="border">
				<div id="orange" class="color-square" style="background-color: #FCB643"></div>
			</div>
			<div class="border">
				<div id="yellow" class="color-square" style="background-color: #FFFF00"></div>
			</div>
			<div class="border">
				<div id="black" class="color-square" style="background-color: #000000"></div>
			</div>
			<div class="border">
				<div id="cyan" class="color-square" style="background-color: #00FFFF"></div>
			</div>
			<div class="border">
				<div id="indigo" class="color-square" style="background-color: #8000FF"></div>
			</div>
		</div>
	</div>
</div>
<?php $this->Html->scriptStart( array( 'inline' => false ) ); ?>

var colorCloseFunction = null;
var colorModifyDrawObj = null;

function showMenuColor( drawObj, offset, closeFunc ) {
	if (drawObj.drawType === 'highlight') {
		$('#black').parent().hide();
		$('#cyan').parent().show();
	} else {
		$('#black').parent().show();
		$('#cyan').parent().hide();
	}
	$('#menuColor').fadeIn( 200 );
	positionMenu( $('#menuColor .popupDialog'), drawObj, offset );
	colorCloseFunction = closeFunc;
	colorModifyDrawObj = drawObj
}

function dismissMenuColor( event ) {
	$('#menuColor').fadeOut( 200 );
	if (typeof colorCloseFunction === 'function') {
		var c = $(this).children(".color-square").css("background-color");
		colorCloseFunction( c, colorModifyDrawObj );
		colorCloseFunction = null;
	}
}

$(document).ready(function() {
	$('#menuColor').on( 'click', dismissMenuColor );
	$('#menuColor .border').on( "click", dismissMenuColor );
});
<?php $this->Html->scriptEnd( array( 'inline' => false ) ); ?>
