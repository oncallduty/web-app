<script type="text/javascript">
<!-- //
function showLoadingOverlay( title ) {
	if( !loadingOverlayIsVisible() ){
		loadingOverlayChangeTitle( title );
		$('#loading').fadeIn( 200 );
	}
}

function dismissLoadingOverlay() {
	if( loadingOverlayIsVisible() ){
		$('#loading').fadeOut( 200, function(){
			$('#loading .spinner div').html( 'Loading...' );
		});
	}
}

function loadingOverlayIsVisible() {
	return $('#loading').is(":visible");
}

function loadingOverlayChangeTitle(title) {
	$('#loading .spinner div').html( title );
}
// -->
</script>