<div id="dialogTextInput">
	<div class="popupDialog">
		<h2></h2>
		<div class="dialogMsg">
			<input type="text" id="dialogInput">
		</div>
		<div class="dialogOkBtnWrap">
			<button id="dialogTextInputCancel" class="btn_app_cancel" onclick="dismissDialogTextInputCancel();"><span>Cancel</span></button>
			<button id="dialogTextInputClose" class="btn_app_normal" onclick="dismissDialogTextInput()" onkeyup="dismissDialogTextInput()"></button>
		</div>
	</div>
</div>
<script type="text/javascript">
<!-- //
var cancelFunction = null;
var closeFunction = null;
var textInput = '';

function showDialogTextInput( title, buttonLabel, defaultText ) {
	$('#dialogTextInput div.popupDialog h2').html( title );
	$('#dialogTextInput #dialogInput').val(defaultText);
	$('#dialogTextInputClose').html( '<span>'+buttonLabel+'</span>' );
	$('#dialogTextInput').show();
	// Focus needs to be after the show.
	$('#dialogTextInput #dialogInput').focus();
}

function dismissDialogTextInput() {
	cancelFunction = null;

	if (typeof closeFunction === 'function') {
		closeFunction($('#dialogTextInput #dialogInput').val());
		closeFunction = null;
	}

	$('#dialogTextInput').fadeOut( 200 );
}

function setDialogTextInputCloseAction( action ) {
	closeFunction = action;
}

function setDialogTextInputCancelAction( action ) {
	cancelFunction = action;
}

function dismissDialogTextInputCancel() {
	closeFunction = null;

	if (typeof cancelFunction === 'function') {
		cancelFunction();
		cancelFunction = null;
	}

	$('#dialogTextInput').fadeOut( 200 );
};

function setDialogTextInputCloseAddClass(className) {
	$('#dialogTextInputClose').addClass(className);
};

function setDialogTextInputCloseRemoveClass(className) {
	$('#dialogTextInputClose').removeClass(className);
};
// -->
</script>