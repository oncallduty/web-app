<div id="depo_files_send_dlg">
	<div id="depo_files_send">
		<section id="sender_actions">
			<div class="title"></div>
			<div class="port">
				<button class="cancel_button" id="dlgActionCancel">Cancel</button>
			</div>
			<div class="steer">
				<button id="dlgActionSend">Send</button>
			</div>
		</section>
		<section id="sender_directions">
			<fieldset>
				<form id="emailForm">
					<div><label>To:</label><input name="sendTo" id="sendTo" type="text" required="required" /></div>
					<div><label>Cc:</label><input name="sendCc" id="sendCc" type="text" /></div>
					<div><label>Bcc:</label><input name="sendBcc" id="sendBcc" type="text" /></div>
					<div><label>Subject:</label><input name="sendSubject" id="sendSubject" type="text" required="required" /></div>
				</form>
			</fieldset>
		</section>
		<section id="sender_contents">
			<div>
				<textarea name="sendContents" id="sendContents"></textarea>
			</div>
			<div>
				<ul id="sender_attachments"></ul>
			</div>
		</section>
	</div>
</div>

<script type="text/javascript">
<?php $this->Html->scriptStart( ['inline' => false] ); ?>
var sendDialog = window.top.sendDialog = {
	folderID: null,
	fileID: null,
	fileIDs: null,
	annotations: null,
	sendFileIDs: null,
	finallyFn: null,
	setFinallyFn: function setFinallyFn( finFn ) {
		if(typeof finFn === "function") {
			this.finallyFn = finFn;
		}
	},
	runFinallyFn: function runFinallyFn() {
		if(typeof this.finallyFn === "function") {
			this.finallyFn();
		}
	},
	setFolderID: function( folderID, selectedFileIDs ) {
		var selectedFolder = Datacore.getFolder( folderID );
		$('#sender_actions div.title').text( selectedFolder.name );
		$('input#sendSubject').val( selectedFolder.name );
		var files = selectedFolder.getFiles();
		this.sendFileIDs = [];
		for( var f in files ) {
			var _file = files[ f ];
			if( typeof selectedFileIDs === 'object' && selectedFileIDs !== null && selectedFileIDs.length > 0 ) {
				for( var i in selectedFileIDs ) {
					if( selectedFileIDs[ i ] === _file.ID ) {
						this.sendFileIDs.push( _file.ID );
						this.addAttachmentFilename( _file.name, _file.mimeType );
						break;
					}
				}
			} else {
				this.addAttachmentFilename( files[f].name, files[f].mimeType );
			}
		}
		this.folderID = selectedFolder.ID;
	},
	setFileID: function( fileID, annotations ) {
		//console.log( "sendDialog.setFileID; fileID:", fileID, annotations );
		var selectedFile = Datacore.getFile( fileID );
		$('#sender_actions div.title').text( selectedFile.name );
		$('input#sendSubject').val( selectedFile.name );
		this.addAttachmentFilename( selectedFile.name, selectedFile.mimeType );
		this.fileID = selectedFile.ID;
		this.sendFileIDs = [];
		if( typeof annotations !== "undefined" && annotations !== null ) {
			this.annotations = annotations;
		} else {
			this.annotations = null;
		}
	},
	setFileIDs: function( selectedFileIDs ) {
		$('#sender_actions div.title').text( 'Searchable' );
		$('input#sendSubject').val( 'Searchable' );
		var files = Datacore.getFiles( selectedFileIDs );
		this.sendFileIDs = [];
		for (var f in files) {
			var _file = files[f];
			for( var i in selectedFileIDs ) {
				if( selectedFileIDs[i] === _file.ID ) {
					this.sendFileIDs.push( _file.ID );
					this.addAttachmentFilename( _file.name, _file.mimeType );
					break;
				}
			}
		}
		this.fileIDs = selectedFileIDs;
	},
	setValue: function( identity, value) {
		$(identity).val( value );
	},
	addAttachmentFilename: function( filename, mimetype ) {
		var typeclass = null;

		switch (mimetype) {
			case 'application/pdf':
				typeclass = 'type_pdf';
				break;
			default:
				typeclass = 'type_txt';
		}

		$('#sender_contents div ul').append( '<li class="'+typeclass+'">'+filename+'</li>' );
	},
	show: function() {
		if (this.folderID === null && this.fileID === null && this.fileIDs === null) {
			console.log( ' -- No folder or file selected to send.' );
			return;
		}
		$('#depo_files_send_dlg').fadeIn( 500 );
	},
	hide: function () {
		$('#depo_files_send_dlg').fadeOut( 500 );
		// flush the form fields
		this.folderID = null;
		this.fileID = null;
		this.fileIDs = null;
		$('#sender_attachments').empty();
		$('#sender_directions input').val( '' );
		$('#sendContents').val(''); // textarea
	},
	cancel: function() {
		this.hide();
	},
	sendFolder: function() {
		var data = {};
		var sendItems = [ 'sendTo', 'sendCc', 'sendBcc', 'sendSubject', 'sendContents' ];
		for( var si in sendItems ) {
			data[ sendItems[ si ] ] = $( '#' + sendItems[ si ] ).val();
		}
		if( this.folderID !== null || this.sendFileIDs.length > 1 ) {
			data['folderID'] = this.folderID;
			data['selectedFileIDs'] = this.sendFileIDs;
			$.post( '/depositions/sendFolder', data )
			.done( function( response ) {
				if (typeof response === 'string') {
					response = $.parseJSON( response );
				}
				if (response === null) {
					alert ('An error occurred while attempting to email the folder.');
					return;
				} else if (typeof response.status === 'undefined') {
					alert( (typeof response.message !== 'undefined' ? response.message : 'An error occurred while attempting to email the folder.' ) );
					return;
				}
				$('#dlgActionCancel').trigger( 'click' );
				sendDialog.runFinallyFn();
			} );
		}
		if (this.fileID !== null) {
			var url = '/depositions/sendFile';
			data['fileID'] = this.fileID;
			if (this.annotations !== null ) {
				data['annotations'] = this.annotations;
				data['sourceFileID'] = this.fileID;
				url = 'depositions/sendFileWithAnnotations';
			}
			$.post( url, data )
			.done( function( response ) {
				if (typeof response === 'string') {
					response = $.parseJSON( response );
				}
				if (response === null) {
					alert ('An error occurred while attempting to email the file.');
					return;
				} else if (typeof response.status === 'undefined') {
					alert( (typeof response.message !== 'undefined' ? response.message : 'An error occurred while attempting to email the document.' ) );
					return;
				}
				$('#dlgActionCancel').trigger( 'click' );
				sendDialog.runFinallyFn();
			} );
		}
	}
};
$('#dlgActionCancel').click( function() {
	sendDialog.cancel();
} );
$('#dlgActionSend').click( function() {
	sendDialog.sendFolder();
} );
<?php $this->Html->scriptEnd(); ?>
</script>
