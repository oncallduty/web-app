<div id="dialogNoteEntry">
	<div class="popupDialog">
		<div class="dialogUsername"></div>
		<div class="dialogDate"></div>
		<textarea id="dialogNote"></textarea>
		<div class="dialogOkBtnWrap">
			<button id="dialogNoteEntrySave" onclick="dismissDialogNoteEntry()" onkeyup="dismissDialogNoteEntry()"></button>
			<button id="dialogNoteEntryTrash" onclick="trashDialogNoteEntry()" onkeyup="trashDialogNoteEntry()"></button>
		</div>
	</div>
</div>
<script type="text/javascript">
<!-- //
var closeFunction = null;

function showDialogNoteEntry( username, date, note ) {
//	$('#dialogNoteEntry div.dialogUsername').text( username );
	$('#dialogNoteEntry div.dialogDate').text( date );
	$('#dialogNoteEntry #dialogNote').val( note );
	if (note.length === 0) {
		$('#dialogNoteEntry #dialogNoteEntryTrash').hide();
	} else {
		$('#dialogNoteEntry #dialogNoteEntryTrash').show();
	}
	$('#dialogNoteEntry').show();
	$('#dialogNoteEntry #dialogNote').focus();
	$('#dialogNoteEntry').on('click', dismissDialogNoteEntry);
	$('#dialogNoteEntry .popupDialog').on('click', function(e) {
		$('#dialogNoteEntry #dialogNote').focus();
		e.stopPropagation();
	});
}

function dismissDialogNoteEntry() {
	if (typeof closeFunction === 'function') {
		closeFunction($('#dialogNoteEntry #dialogNote').val(), false);
		closeFunction = null;
	}

	$('#dialogNoteEntry').off('click', dismissDialogNoteEntry);
	$('#dialogNoteEntry #dialogNote').off();
	$('#dialogNoteEntry').fadeOut( 200 );
}

function trashDialogNoteEntry() {
	if (typeof closeFunction === 'function') {
		closeFunction($('#dialogNoteEntry #dialogNote').val(), true);
		closeFunction = null;
	}
	dismissDialogNoteEntry();
}

// Two paramaters passed to the close function: The current contents of the note and if the note should be trashed.
function setDialogNoteEntryCloseAction( action ) {
	closeFunction = action;
}

function clearDialogNoteEntryCloseAction() {
	closeFunction = null;
}
// -->
</script>