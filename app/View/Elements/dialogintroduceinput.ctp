<div id="dialogIntroduceInput">
	<div id="prompt">Click outside of the box<br >to exit edit mode.</div>
	<div class="popupDialog">
		<div class="dialogIntroduce">
			<input type="text" id="dialogTitle">
			<div id="pad"></div>
			<input type="text" id="dialogSubtitle">
			<div id="bar"></div>
			<span id="date"></span>
		</div>
		<div class="dialogOkBtnWrap">
			<button id="dialogIntroduceInputUndo" onclick="dialogIntroduceInputToggle()" onkeyup="dismissDialogIntroduceInput()"></button>
			<button id="dialogIntroduceInputRedo" onclick="dialogIntroduceInputToggle()" onkeyup="dismissDialogIntroduceInput()"></button>
		</div>
	</div>
</div>
<script type="text/javascript">
<?php // insert view js into buffered codeblock ?>
<?php $this->Html->scriptStart( array( 'inline' => false ) ); ?>
var closeFunction = null;
//var ipad_half_width = 0;
var savedText = {newTitle:"", newSubtitle:"", oldTitle:"", oldSubtitle:"", editNew: true};
var currentFocus = null;

function showDialogIntroduceInput( defaultTitle, defaultSubTitle, displayDate, xPos, yPos ) {
	$('#dialogIntroduceInput #dialogTitle').val(defaultTitle);
	$('#dialogIntroduceInput #dialogSubtitle').val(defaultSubTitle);
	$('#dialogIntroduceInput #date').text(displayDate);
	$('#dialogIntroduceInput .popupDialog').css('left', xPos);
	$('#dialogIntroduceInput .popupDialog').css('top', yPos);
	$('#dialogIntroduceInput .popupDialog').css('margin', 'auto');
	$('#dialogIntroduceInput').show();
	// Stuff for after the show.
	resize($('#dialogIntroduceInput #dialogTitle')[0]);
	resize($('#dialogIntroduceInput #dialogSubtitle')[0]);
	$('#dialogIntroduceInput #dialogTitle').focus();
	savedText.newTitle = savedText.oldTitle = defaultTitle;
	savedText.newSubtitle = savedText.oldSubtitle = defaultSubTitle;
	savedText.editNew = true;
	$('#dialogIntroduceInput').click(clickDialogIntroduceInput);
	$('#dialogIntroduceInput')[0].addEventListener("touchend", clickDialogIntroduceInput, false);
}

function dialogIntroduceInputToggle() {
	// Save focus
	if ($('#dialogIntroduceInput #dialogTitle').is(':focus')) {
		currentFocus = $('#dialogIntroduceInput #dialogTitle');
	} else if ($('#dialogIntroduceInput #dialogSubtitle').is(':focus')) {
		currentFocus = $('#dialogIntroduceInput #dialogSubtitle');
	}

	if (savedText.editNew) {
		savedText.newTitle = $('#dialogIntroduceInput #dialogTitle').val();
		savedText.newSubtitle = $('#dialogIntroduceInput #dialogSubtitle').val();
		$('#dialogIntroduceInput #dialogTitle').val(savedText.oldTitle);
		$('#dialogIntroduceInput #dialogSubtitle').val(savedText.oldSubtitle);
		$('#dialogIntroduceInput #dialogIntroduceInputUndo').hide();
		$('#dialogIntroduceInput #dialogIntroduceInputRedo').show();
	} else {
		savedText.oldTitle = $('#dialogIntroduceInput #dialogTitle').val();
		savedText.oldSubtitle = $('#dialogIntroduceInput #dialogSubtitle').val();
		$('#dialogIntroduceInput #dialogTitle').val(savedText.newTitle);
		$('#dialogIntroduceInput #dialogSubtitle').val(savedText.newSubtitle);
		$('#dialogIntroduceInput #dialogIntroduceInputUndo').show();
		$('#dialogIntroduceInput #dialogIntroduceInputRedo').hide();
	}
	savedText.editNew = !savedText.editNew;
	resize($('#dialogIntroduceInput #dialogTitle')[0]);
	resize($('#dialogIntroduceInput #dialogSubtitle')[0]);
	if (currentFocus !== null) {
		currentFocus.focus();
	}
}

function clickDialogIntroduceInput(event) {
	if (event.target === $('#dialogIntroduceInput')[0]) {
		dismissDialogIntroduceInput();
	}
}

function dismissDialogIntroduceInput() {
	if (typeof closeFunction === 'function') {
		if ($('#dialogIntroduceInput #dialogTitle').val().length === 0) {
			$('#dialogIntroduceInput #dialogTitle').val(savedText.oldTitle);
		}
		closeFunction($('#dialogIntroduceInput #dialogTitle').val(), $('#dialogIntroduceInput #dialogSubtitle').val());
		closeFunction = null;
	}

	$('#dialogIntroduceInput').fadeOut( 200 );
}

function setDialogIntroduceInputCloseAction( action ) {
	closeFunction = action;
}

function clearDialogIntroduceInputCloseAction() {
	closeFunction = null;
}

function focusDialogIntroduceInput(event) {
	currentFocus = $(this);
}

function resize(target, which) {
	var noCharCodes = [33, 34, 35, 36,37, 38, 39, 40, 45 ];

	//console.log('which: ' + which + ' string: ' + String.fromCharCode( which ));
	var popup = $('#dialogIntroduceInput .popupDialog');
    var text = target.value;
	if (isset( which )) {
		if (which === 8 || which === 46) {
			// User pressed backspace or delete, we guess they are removing the last character for calculating width.
			text = text.slice( 0, -1 );
			text += "'";
		} else if (noCharCodes.indexOf( which ) != -1) {
			// These are cursor codes and do not change the length of the text.
			return;
		} else {
			text += String.fromCharCode( which );
			text += "'";	// Add so spaces at end are are counted.
		}
	}
	var dateSpan = $('#dialogIntroduceInput #date').detach();
    var textDiv = $('<span id="textsize">'+text+'</span>');
    $('.dialogIntroduce').append(textDiv);
	// See if we hit the limit on the right side.
	var maxWidth = $('#dialogIntroduceInput')[0].offsetWidth;
	var spanMaxWidth = parseInt( textDiv.css( 'max-width' ).slice( 0, -2 ) );
    var textSize = Math.min( textDiv[0].offsetWidth,  spanMaxWidth );
	if (isset( which ) && ( popup[0].offsetLeft + popup[0].offsetWidth >= $('#dialogIntroduceInput')[0].offsetWidth ) && ( textSize < spanMaxWidth )) {
		//Move the dialog to the left.  We add the char to "A" to account only for the new char and ignore any padding.
		textDiv.text( "A" );
		var diff = textDiv[0].offsetWidth;
		textDiv.text( "A" + String.fromCharCode( which ) );
		diff = textDiv[0].offsetWidth - diff;
		popup.css( 'left', Math.max( 0, popup[0].offsetLeft - diff ) + 'px' );
		textDiv.text( text );
	}
    $(textDiv).remove();
    $('.dialogIntroduce').append(dateSpan);
    target.style.width = textSize + 'px';

	//Reposition prompt text.
	var prompt = $('#dialogIntroduceInput #prompt');
	var middle = popup[0].offsetLeft + popup[0].offsetWidth / 2;
	prompt.css( 'left', ( middle - prompt[0].offsetWidth / 2 ) + 'px' );
	prompt.css( 'top', ( popup[0].offsetTop - prompt[0].offsetHeight - 20 ) + 'px' );
}

$(document).ready(function() {
	$('#dialogIntroduceInput #dialogTitle').keydown(function(e) {
		if ( e.which == 13 ) {
			$('#dialogIntroduceInput #dialogSubtitle').focus();
		} else {
			resize(e.target, e.which);
		}
	}).focus(focusDialogIntroduceInput);
	$('#dialogIntroduceInput #dialogSubtitle').keydown(function(e) {
		if ( e.which == 13 ) {
			dismissDialogIntroduceInput();
		} else {
			resize(e.target, e.which);
		}
	}).focus(focusDialogIntroduceInput);
	//ipad_half_width = $('#ipad_container')[0].offsetWidth / 2;


});


<?php $this->Html->scriptEnd( array( 'inline' => false ) ); ?>
</script>