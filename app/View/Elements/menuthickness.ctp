<div id="menuThickness" >
	<div class="popupDialog">
		<div class="title">Thickness</div>
		<div class="border">
			<div id="line1" class="line"></div>
		</div>
		<div class="border">
			<div id="line2" class="line"></div>
		</div>
		<div class="border">
			<div id="line3" class="line"></div>
		</div>
		<div class="border">
			<div id="line4" class="line"></div>
		</div>
	</div>
</div>
<?php $this->Html->scriptStart( array( 'inline' => false ) ); ?>

var thicknessCloseFunction = null;
var thicknessModifyDrawObj = null;

function showMenuThickness( drawObj, offset, closeFunc ) {
	$('#menuThickness .border').removeClass( 'selected' );
	$('#menuThickness #line' + drawObj.lineWidth).parent().addClass( 'selected' );
	$('#menuThickness').fadeIn( 200 );
	positionMenu( $('#menuThickness .popupDialog'), drawObj, offset );
	thicknessCloseFunction = closeFunc;
	thicknessModifyDrawObj = drawObj
}

function dismissMenuThickness( event ) {
	$('#menuThickness').fadeOut( 200 );
	if (typeof thicknessCloseFunction === 'function') {
		var c = null;
		var clicked = $(this).children(".line");
		if (clicked.length === 1 && clicked.attr( 'id' ).substr( 0, 4 ) === 'line' ) {
			c = parseInt( clicked.attr( 'id' ).substr( 4 ) );
			if ( isNaN( c ) ) {
				c = null;
			} else {
				c = Math.min( 4, Math.max( 1, c ) );
			}
		}
		thicknessCloseFunction( c, thicknessModifyDrawObj );
		thicknessCloseFunction = null;
	}
}

$(document).ready(function() {
	$('#menuThickness').on( 'click', dismissMenuThickness );
	$('#menuThickness .border').on( "click", dismissMenuThickness );
});
<?php $this->Html->scriptEnd( array( 'inline' => false ) ); ?>