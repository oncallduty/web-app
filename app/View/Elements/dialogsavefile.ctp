<div id="dialogSaveFile">
	<div class="popupDialog">
		<div id="title"></div>
		<div class="shadowLine"></div>
		<div class="dialogMsg">
			<input type="text" id="dialogInput" onkeyup="dialogSaveFileKeyUp();">
		</div>
		<div class="folderInfo">
			<div class="folderIcon"></div>
			<div id="folderList">
				<div class="anchor">
					<ul></ul>
				</div>
			</div>
		</div>
		<div class="dialogOkBtnWrap">
			<button id="dialogSaveFileCancel" class="btn_app_cancel" onclick="clearDialogSaveFileCloseAction(); dismissDialogSaveFile();"><span>Cancel</span></button>
			<button id="dialogSaveFileClose" class="btn_app_normal" onclick="dismissDialogSaveFile()" onkeyup="dismissDialogSaveFile()"></button>
		</div>
	</div>
</div>
<script type="text/javascript">
//<![CDATA[
var closeFunction = null;
var textInput = '';
var dialogSaveFileSelectedFolderID = -1;
var dialogSaveFileHide = true;

function showDialogSaveFile( title, buttonLabel, defaultText ) {
	$('#dialogSaveFile #title').html( title );
	$('#dialogSaveFile #dialogInput').val(defaultText);
	$('#dialogSaveFileClose').html( '<span>'+buttonLabel+'</span>' );
	dialogSaveFileAddFolders();
	$('#dialogSaveFile').show();
	// Focus needs to be after the show.
	$('#dialogSaveFile #dialogInput').focus();
	if ($('#ipad_container').hasClass('fullscreen')) {
		dialogSaveFileCalculatePosition();
	}
	$('#dialogSaveFile #dialogInput').off().on( 'keyup', dialogSaveFileKeyUp );
}

function dismissDialogSaveFile() {
	if (typeof closeFunction === 'function') {
		closeFunction($('#dialogSaveFile #dialogInput').val(), dialogSaveFileSelectedFolderID);
		closeFunction = null;
	}

	$('#dialogSaveFile').fadeOut( 200, function() {
		$('#dialogSaveFile .popupDialog').css( 'left', '');
	});
}

function setDialogSaveFileCloseAction( action ) {
	closeFunction = action;
}

function clearDialogSaveFileCloseAction() {
	closeFunction = null;
}

function dialogSaveFileResetFolderID() {
	dialogSaveFileSelectedFolderID = -1;
}

function dialogSaveFileAddFolders() {
	var folders = Datacore.getFolders();
	var f;
	var list = $('#dialogSaveFile #folderList ul');
	var userType = top.webApp.getS( 'user.userType' );

	$(list).empty();
	// See if we need to reset selected folderID
	if (dialogSaveFileSelectedFolderID !== -1) {
		var found = false;
		for (f in folders) {
			if (dialogSaveFileSelectedFolderID === parseInt( folders[f].ID )) {
				found = true;
				break;
			}
		}
		if (found === false) {
			dialogSaveFileSelectedFolderID = -1;
		}
	}
	for (f in folders) {
		if (dialogSaveFileSelectedFolderID === -1 && (folders[f].class === 'Personal' || (userType === 'WM' && folders[f].class === 'WitnessAnnotations'))) {
			dialogSaveFileSelectedFolderID = parseInt( folders[f].ID );
		}
		if( userType === 'WM' ) {
			if (folders[f].class === 'WitnessAnnotations') {
				var start = '<li id="folder_' + folders[f].ID + '"';
				if (dialogSaveFileSelectedFolderID === parseInt( folders[f].ID )) {
					start += ' class="select"';
				}
				start += ' onclick="dialogSaveFileUpdate(' + folders[f].ID + ')">';
				$(list).append(start + folders[f].name + '</li>');
			}
		} else {
			if (folders[f].class === 'Personal' || folders[f].class === 'Folder' || folders[f].class === 'Trusted') {
				var start = '<li id="folder_' + folders[f].ID + '"';
				if (dialogSaveFileSelectedFolderID === parseInt( folders[f].ID )) {
					start += ' class="select"';
				}
				start += ' onclick="dialogSaveFileUpdate(' + folders[f].ID + ')">';
				$(list).append(start + folders[f].name + '</li>');
			}
		}
	}
	$('#dialogSaveFile #folderList li').not('.select').hide();
	dialogSaveFileHide = true;
}

function dialogSaveFileUpdate( id ) {
	$('#dialogSaveFile #folderList .select').removeClass('select');
	$('#dialogSaveFile #folderList #folder_' + id).addClass('select');
	dialogSaveFileSelectedFolderID = id;
	if (dialogSaveFileHide) {
		$('#dialogSaveFile #folderList li').not('.select').slideDown( 200 );
	} else {
		$('#dialogSaveFile #folderList li').not('.select').slideUp( 200 );
	}
	dialogSaveFileHide = !dialogSaveFileHide;
}

function dialogSaveFileCalculatePosition() {
	var halfWidth = $('#dialogSaveFile .popupDialog').width() / 2;
	var screenMiddle = $('#dialogSaveFile').width() / 2;
	$('#dialogSaveFile .popupDialog').css( 'left', (screenMiddle - halfWidth) + 'px' );
}

function dialogSaveFileKeyUp( e )
{
	if( typeof e !== 'undefined' && e !== null && typeof e.keyCode !== 'undefined' && e.keyCode === 13 ) {
		dismissDialogSaveFile();
	}
}

function DialogSaveFile( dialogTitle, saveBtnLabel, fileID, callback ) {
	console.log( "DialogSaveFile;", arguments );
	var self = this;

	var defaultValue = window.top.webApp.isTempFile() ? window.top.Datacore.s.tempFile.originalFilename : '';
	if(window.top.webApp && typeof window.top.webApp.getS === "function") {
		defaultValue = window.top.webApp.getS( 'viewerFilename' );
	}

	var defaultFolder = 0;
	if(typeof fileID === "number" || (typeof fileID === "string" && parseInt(fileID) > 0)) {
		dcFile = window.top.Datacore.getFile( fileID );
		if( typeof dcFile !== 'undefined' && dcFile !== null ) {
			var extIdx = dcFile.name.lastIndexOf( "." );
			defaultValue = dcFile.name.substr( 0, extIdx );
			defaultFolder = defaultFolder ? defaultFolder : parseInt(dcFile.folderID);
		}
	}
	else if(typeof fileID === "string" && fileID !== "") {
		defaultValue = fileID;
	}

	if(window.top.webApp.isTempFile()) {
		defaultFolder = (window.top.Datacore.s.tempFile.originalFolderID > 0) ? window.top.Datacore.s.tempFile.originalFolderID : defaultFolder;
		defaultValue = (window.top.Datacore.s.tempFile.originalFilename > 0) ? window.top.Datacore.s.tempFile.originalFilename : defaultValue;
	}

	this.dialogTitle = (typeof dialogTitle === "string" && dialogTitle.length > 0) ? dialogTitle : "Save Document";
	this.saveBtnLabel = (typeof saveBtnLabel === "string" && saveBtnLabel.length > 0) ? saveBtnLabel : "Save";
	this.defaultValue = (typeof defaultValue === "string") ? defaultValue : "";
	this.selectedFolderID = (typeof defaultFolder === "number") ? defaultFolder : 0;
	this.callback = callback;


	var dialog = $('#dialogSaveFile');
	if( !dialog ) {
		console.warn( "DialogSaveFile; Unable to locate dialog element" );
		return;
	}
	var titleEl = $('#title', dialog);
	var inputEl = $('#dialogInput', dialog);
	var saveBtn = $('#dialogSaveFileClose', dialog);
	var folderList = $('#folderList ul', dialog);
	if( !titleEl || !inputEl || !saveBtn || !folderList ) {
		console.warn( "DialogSaveFile; Unable to locate required element(s)" );
		return;
	}
	var folderSelectMode = false;
	var folders = window.top.Datacore.getFolders();
	var userType = window.top.webApp.getS( 'user.userType' );
	var saveFilename = "";
	var isOverwrite = false;

	this.show = function() {
		titleEl.html( self.dialogTitle );
		inputEl.val( self.defaultValue );
		saveBtn.html( '<span>' + self.saveBtnLabel + '</span>' );
		// onclick="dismissDialogSaveFile()" onkeyup="dismissDialogSaveFile()"
		saveBtn.off('click').on('click', saveFile);
		saveBtn.off('keyup').on('keyup', saveFile);
		populateFolderList();
		dialog.show();
	};

	function selectFolder( folderID ) {
		if( folderSelectMode && folderID > 0 ) {
			$('.select', folderList).removeClass( 'select' );
			$('#folder_' + folderID, folderList).addClass( 'select' );
			$('li', folderList).not( '.select' ).slideUp( 150 );
			folderSelectMode = false;
			for( var f in folders ) {
				if( folderID === parseInt( folders[f].ID ) ) {
					self.selectedFolderID = folderID;
					return;
				}
			}
			self.selectedFolderID = 0;
		} else {
			$('li', folderList).not( '.select' ).slideDown( 150 );
			folderSelectMode = true;
		}
	}

	function populateFolderList() {
		self.selectedFolderID = (typeof defaultFolder === "number") ? defaultFolder : 0;
		var f;
		folderList.empty();

		if( self.selectedFolderID > 0 ) {
			var dcFolder = window.top.Datacore.getFolder(self.selectedFolderID);
			if(dcFolder && dcFolder.ID && parseInt(dcFolder.ID) > 0) {
				if(['Personal','Folder','Trusted'].indexOf(dcFolder.class) < 0)
					self.selectedFolderID = window.top.webApp.getPersonalFolderID();
			}

			var isValid = false;
			for( f in folders ) {
				if( self.selectedFolderID === parseInt( folders[f].ID ) ) {
					isValid = true;
					break;
				}
			}
			if( !isValid ) {
				self.selectedFolderID = 0;
			}
		}

		for( f in folders ) {
			var folder = folders[f];
			if( self.selectedFolderID === 0 && (folder.class === 'Personal' || (userType === 'WM' && folder.class === 'WitnessAnnotations')) ) {
				self.selectedFolderID = parseInt( folder.ID );
			}
			if( (userType === 'WM' && folder.class === 'WitnessAnnotations') || ['Personal','Folder','Trusted'].indexOf( folder.class ) >= 0 ) {
				var li = $('<li id="folder_' + folder.ID + '"></li>');
				li.addClass( "noselect" );
				if( self.selectedFolderID === parseInt( folder.ID ) ) {
					li.addClass( "select" );
				}
				li.off().on( 'click', {folderID:folder.ID}, function(e) {
					selectFolder( e.data.folderID );
				} );
				li.append( folder.name );
				folderList.append( li );
			}
		}
		$('li', folderList).not( '.select' ).hide();
		folderSelect = false;
	};

	function close() {
		$('#dialogSaveFile').fadeOut( 200, function() {
			console.log('DialogSaveFile::close()');
			$('#dialogSaveFile .popupDialog').css( 'left', '');
		});
	};

	function saveFile() {
		console.log('DialogSaveFile::saveFile()');
		inputEl = $('#dialogInput', dialog).val();
		close();
		callback(inputEl, self.selectedFolderID);
	};
}

//]]>
</script>
