<?php $authUser = $this->Session->read( 'Auth.User' ); ?>

<div id="ipad_titlebar" class="hideSocialList" style="display:none;">
	<div id="ipad_toolbar">
		<div id="toolbar_left">
			<div id="navbar_container">
				<div id="ipad_navbar_back" class="toolbar_btn">
					<a href="javascript:history.go(-1);"><div id="ipad_navbtn_back"><p>Back</p></div></a>
				</div>
				<div id="ipad_navbar_cases" class="toolbar_btn">
					<div id="ipad_navbtn_cases"><p>Cases</p></div>
				</div>
				<div id="ipad_navbar_logout" class="toolbar_btn">
					<div id="ipad_navbtn_logout"><p>Logout</p></div>
				</div>
				<div id="ipad_navbar_join" class="toolbar_btn">
					<div id="ipad_navbtn_join"><p>Join</p></div>
				</div>
				<div id="ipad_navbar_fullscreen" class="toolbar_btn">
					<div title="Full Screen" id="ipad_navbtn_fullscreen"><p>&nbsp;</p></div>
				</div>
				<div id="ipad_navbar_presentation" class="toolbar_btn">
					<div title="Start/End Presentation" id="ipad_navbtn_presentation"><p>&nbsp;</p></div>
				</div>
				<div id="ipad_navbar_pass_annotation" class="toolbar_btn">
					<div title="Pass/Revoke Annotation Role" id="ipad_navbtn_pass_annotation"><p>&nbsp;</p></div>
				</div>
				<div id="ipad_navbar_download" class="toolbar_btn">
					<div title="Download File" id="ipad_navbtn_download"><p>&nbsp;</p></div>
				</div>
			</div>
		</div>

		<div id="toolbar_middle">
			<div id="toolbar_title"></div>
		</div>
		<div id="toolbar_right">
			<?php if ($authUser !== null) : ?>
			<div id="welcome_user"><span>Welcome, <?="{$authUser['firstName']} {$authUser['lastName']}"?></span></div>
			<?php endif; ?>
			<div id="ipad_navbtn_skip_stamp" class="btn_app_normal"><p>Skip Stamp</p></div>
			<div id="ipad_navbtn_introduce" class="btn_app_normal"><p>Introduce</p></div>
			<div id="ipad_navbtn_send" class="btn_app_normal"><p>Email</p></div>
			<div id="ipad_navbtn_save" class="btn_app_normal"><p>Save</p></div>
		</div>
	</div>
</div>
<script>
<?php $this->Html->scriptStart( array( 'inline' => false ) ); ?>
function TitleActions () {
	this.btn_back_display = false;
	this.btn_cases_display = false;
	this.btn_logout_display = false;
	this.btn_join_display = false;
	this.btn_download_display = false;
	this.btn_save_display = false;
	this.btn_introduce_display = false;
	this.btn_send_display = false;
	this.btn_fullscreen_display = false;
	this.btn_presentation_display = false;
	this.btn_pass_annotation_display = false;

	this.show_introduce_display = false;
	this.show_viewer_display = false;
	this.show_presentation_revoke = false;

	this.casesAction = '/cases';
}

TitleActions.prototype.setBackAction = function( action ) {
	if (typeof action !== 'undefined' && action !== null) {
		var actionAsInt = parseInt( action );
		$('#ipad_navbar_back a').attr( 'href', (isNaN( actionAsInt ) ? action : 'javascript:history.go('+actionAsInt+');') );
	}
};

TitleActions.prototype.resetAction = function() {
	$('#ipad_navbar_back a').attr( 'href', 'javascript:history.go(-1);' );
};

TitleActions.prototype.setCasesAction = function( action ) {
	if (typeof action !== 'undefined' && action !== null) {
		this.casesAction = action;
	}
};

TitleActions.prototype.showBack = function( action ) {
	this.setBackAction( action );
	this.btn_back_display = true;
};

TitleActions.prototype.hideBack = function() {
	this.btn_back_display = false;
};

TitleActions.prototype.showCases = function() {
	this.btn_cases_display = true;
};

TitleActions.prototype.hideCases = function() {
	this.btn_cases_display = false;
};

TitleActions.prototype.showLogout = function() {
	this.btn_logout_display = true;
};

TitleActions.prototype.hideLogout = function() {
	this.btn_logout_display = false;
};

TitleActions.prototype.showJoin = function() {
	this.btn_join_display = true;
};

TitleActions.prototype.hideJoin = function() {
	this.btn_join_display = false;
};

TitleActions.prototype.showDownload = function() {
	this.btn_download_display = true;
};

TitleActions.prototype.hideDownload = function() {
	this.btn_download_display = false;
};

TitleActions.prototype.showIntroduce = function() {
	this.btn_introduce_display = true;
};

TitleActions.prototype.hideIntroduce = function() {
	this.btn_introduce_display = false;
};

TitleActions.prototype.showSave = function() {
	this.btn_save_display = true;
};

TitleActions.prototype.hideSave = function() {
	this.btn_save_display = false;
};

TitleActions.prototype.showSend = function() {
	this.btn_send_display = true;
};

TitleActions.prototype.hideSend = function() {
	this.btn_send_display = false;
};

TitleActions.prototype.showIntroduceViewer = function() {
	this.show_introduce_display = true;
	this.btn_introduce_display = true;
};

TitleActions.prototype.hideIntroduceViewer = function() {
	this.show_introduce_display = false;
};

TitleActions.prototype.showViewer = function() {
	this.show_viewer_display = true;
};

TitleActions.prototype.hideViewer = function() {
	this.show_viewer_display = false;
};

TitleActions.prototype.showFullscreen = function() {
	this.btn_fullscreen_display = true;
};

TitleActions.prototype.hideFullscreen = function() {
	this.btn_fullscreen_display = false;
};

TitleActions.prototype.showPresentation = function() {
	this.btn_presentation_display = true;
};

TitleActions.prototype.hidePresentation = function() {
	this.btn_presentation_display = false;
};

TitleActions.prototype.showPassAnnotation = function() {
	this.btn_pass_annotation_display = true;
};

TitleActions.prototype.hidePassAnnotation = function() {
	this.btn_pass_annotation_display = false;
};

TitleActions.prototype.presentationActive = function( active ) {
	if (isset( active ) && active ) {
		$('#ipad_navbtn_presentation').addClass( 'active' );
	} else {
		$('#ipad_navbtn_presentation').removeClass( 'active' );
	}
}

TitleActions.prototype.passAnnotationActive = function( active ) {
	if (isset( active ) && active ) {
		$('#ipad_navbtn_pass_annotation').addClass( 'active' );
	} else {
		$('#ipad_navbtn_pass_annotation').removeClass( 'active' );
	}
}

TitleActions.prototype.passAnnotationRevoke = function( show ) {
	if (isset( show ) && show ) {
		$('#ipad_navbtn_pass_annotation').addClass( 'revoke' );
		this.show_presentation_revoke = true;
	} else {
		$('#ipad_navbtn_pass_annotation').removeClass( 'revoke' );
		this.show_presentation_revoke = false;
	}
}

TitleActions.prototype.showAll = function() {
	this.showBack();
	this.showCases();
	this.showLogout();
	this.showJoin();
	this.showDownload();
	this.showIntroduce();
	this.showSave();
	this.showSend();
	this.showIntroduceViewer();
	this.showViewer();
	this.showFullscreen();
	this.showPresentation();
	this.showPassAnnotation();
};

TitleActions.prototype.hideAll = function() {
	this.hideBack();
	this.hideCases();
	this.hideLogout();
	this.hideJoin();
	this.hideDownload();
	this.hideIntroduce();
	this.hideSave();
	this.hideSend();
	this.hideIntroduceViewer();
	this.hideViewer();
	this.hideFullscreen();
	this.hidePresentation();
	this.hidePassAnnotation();
};

TitleActions.prototype.setTitle = function(title) {
	$('#toolbar_title').html( title );
	$('#toolbar_title').prop( 'title', title);
}

TitleActions.prototype.ready = function() {
	return (this.btn_logout_display || this.btn_cases_display || this.btn_back_display);
};

TitleActions.prototype.redraw = function() {
	$('#navbar_container').hide();
	$('#toolbar_right').hide();
	$('#toolbar_left div.toolbar_btn').css( 'display', 'none' );
	$('#toolbar_right div.btn_app_normal').css( 'display', 'none' );

	if (this.btn_back_display) {
		$('#ipad_navbar_back').css( 'display', 'inline-block' );
	}

	if (this.btn_cases_display) {
		$('#ipad_navbar_cases').css( 'display', 'inline-block' );
	}

	if (this.btn_logout_display) {
		$('#ipad_navbar_logout').css( 'display', 'inline-block' );
	}

	if (this.btn_join_display) {
		$('#ipad_navbar_join').css( 'display', 'inline-block' );
	}

	if (this.btn_download_display) {
		$('#ipad_navbar_download').css( 'display', 'inline-block' );
	}

	if (this.btn_introduce_display) {
		$('#ipad_navbtn_introduce').css( 'display', 'inline-block' );
	}

	if (this.btn_save_display) {
		$('#ipad_navbtn_save').css( 'display', 'inline-block' );
	}

	if (this.btn_send_display) {
		$('#ipad_navbtn_send').css( 'display', 'inline-block' );
	}

	if (this.btn_fullscreen_display) {
		$('#ipad_navbar_fullscreen').css( 'display', 'inline-block' );
	}

	if (this.btn_presentation_display) {
		$('#ipad_navbar_presentation').css( 'display', 'inline-block' );
	}

	if (this.btn_pass_annotation_display) {
		$('#ipad_navbar_pass_annotation').css( 'display', 'inline-block' );
	}

	if (this.show_introduce_display) {
		$('#toolbar_logo').hide();
		$('#welcome_user').hide();
		$('#toolbar_title').show();
		$('#ipad_navbtn_skip_stamp').css( 'display', 'inline-block' );
		$('#ipad_navbar_download').hide();
		$('#ipad_navbar_fullscreen').hide();
	} else if (this.show_viewer_display) {
		$('#toolbar_logo').hide();
		$('#welcome_user').hide();
		$('#toolbar_title').show();
		$('#ipad_navbtn_skip_stamp').hide();
	} else if (this.show_presentation_revoke) {
		$('#toolbar_logo').hide();
		$('#welcome_user').hide();
		$('#toolbar_title').show();
		$('#ipad_navbtn_skip_stamp').hide();
		$('#ipad_navbtn_introduce').hide();
		$('#ipad_navbtn_send').hide();
		$('#ipad_navbtn_save').hide();
		$('#ipad_navbar_download').hide();
		$('#ipad_navbar_fullscreen').hide();
	} else {
		$('#toolbar_logo').show();
		$('#welcome_user').show();
		$('#toolbar_title').hide();
		$('#header').hide();
		$('#ipad_navbtn_skip_stamp').hide();
		$('#ipad_navbtn_introduce').hide();
		$('#ipad_navbtn_send').hide();
		$('#ipad_navbtn_save').hide();
		$('#ipad_navbar_download').hide();
		$('#ipad_navbar_fullscreen').hide();
	}

	$('#navbar_container').show();
	$('#toolbar_right').show();
};

$('#ipad_navbtn_logout').click( function() {
	showDialogLogout();
} );

$('#ipad_navbtn_join').click( function() {
	showDialogJoin();
} );

var _titleActions = null;

$(document).ready( function() {
	_titleActions = new TitleActions();
	<?php if (isset( $goBack )) : ?>_titleActions.showBack( "<?= $goBack ?>" );<?php endif; ?>
	<?php if (isset( $goCases ) && $goCases) : ?>_titleActions.showCases();<?php endif; ?>
	<?php if ($authUser !== null) : ?>_titleActions.showLogout();<?php endif; ?>
	<?php if (!isset( $showOnLoad ) || $showOnLoad) : ?>

	$('#ipad_navbtn_cases').click( function() {
		$('#ipad_container_frame').attr( 'src', _titleActions.casesAction );
	});
	if (_titleActions.ready()) {
		_titleActions.redraw();
	}
	<?php endif; ?>
} );
<?php $this->Html->scriptEnd( array( 'inline' => false ) ); ?>
</script>
