<?php $this->Html->css( 'social', null, array( 'inline'=>FALSE ) ); ?>
<?php $this->Html->script( 'social', ['inline'=>FALSE] ); ?>

<div id="attendee_list_container">
	<div id="social_list_container" class="hideSocialList">
		<div id="social_chat_tab" class="social_titlebar">Chat</div>
		<div id="social_list_titlebar">
			<div id="users_titlebar">
				<div id="users_group_edit_cancel"><p>Back</p></div>
				<div title="Create Group" id="users_group_add"></div>
				<div class="social_list_nav_title">Attendees</div>
			</div>
		</div>
		<div id="social_attendee_container">
			<div id="social_attendee_list"></div>
			<div id="social_group_edit">
				<div id="group_form">
					<div id="group_form_name">
						<div id="group_name_edit_container">
							<p>Enter Unique Group Name</p>
							<input name="group_name_edit" id="group_name_edit" class="transparent" />
							<span id="clear-input"></span>
						</div>
					</div>
					<div id="group_form_action">
						<div id="group_form_action_delete"><p>Delete</p></div>
						<div id="group_form_action_save"><p>Save</p></div>
					</div>
				</div>
				<div id="group_attendee_list" class="inactive"></div>
			</div>
		</div>
		<div id="social_chat_window">
			<div id="chat_title">
				<p>Name of Chat Room</p>
				<a href="javascript:void(0);" onclick="Social.hideChat();">
					<span id="close_chat"></span>
				</a>
				<div id="chat_action">
					<div id="role_button">Pass the Role</div>
					<div id="chat_action_text">You can make this person the Session Leader</div>
				</div>
			</div>
			<div id="chat_contents">
			</div>
			<div id="chat_input">
				<input name="chatentry" id="chatentry" />
				<p>Chat user is offline</p>
			</div>
		</div>
		<div id="social_transcript_tab">
			<div id="transcript" class="social_titlebar">Transcript</div>
			<div id="transcript_contents">
				<div id="rcAuth">
					<div id="rcLogin">
						<div class="summary">
							<p>Enter an Event ID and Access Key</p>
							<p>provided by the Court Reporter to</p>
							<p>receive the live transcript feed.</p>
						</div>
						<form id="rcLoginForm">
							<input id="rcEventID" name="eventID" value="" placeholder="Event ID" />
							<input id="rcAccessKey" name="accessKey" value="" placeholder="Access Key" />
						</form>
						<div class="actions">
							<div class="btnCancel"><p>Cancel</p></div>
							<div class="btnOK disabled"><p>Subscribe</p></div>
							<div class="clear"></div>
						</div>
					</div>
					<div id="rcError" style="display:none;">
						<h3 class="heading">Authentication Failed</h3>
						<div class="errMsg">The Event ID or Access Key were incorrect.</div>
						<div class="actions">
							<div class="btnCancel"><p>Cancel</p></div>
							<div class="btnOK"><p>Retry</p></div>
							<div class="clear"></div>
						</div>
					</div>
					<div id="rcLoading" style="display:none;">
						<div class="loading"></div>
					</div>
				</div>
				<div id="liveFeed" style="display:none;"></div>
				<div id="transcript_icon"></div>
				<div id="rcResetWrap" style="display:none;">
					<div id="rcResetDialog">
						<h3 class="heading">End Transcript?</h3>
						<div class="errMsg">
							<p>End the current transcript for all</p>
							<p>attendees?</p>
						</div>
						<div class="actions">
							<div class="btnCancel"><p>Cancel</p></div>
							<div class="btnOK"><p>Continue</p></div>
							<div class="clear"></div>
						</div>
					</div>
				</div>
			</div>
			<div id="rcPoweredBy">
				<div class="txReset" style="display:none;">Reset</div>
				<div class="rcLogo"></div>
			</div>
		</div>
	</div>
	<div id="attendee_list_toggle_wrap" class="hideSocialList">
		<div id="attendee_list_toggle"></div>
		<div id="attendee_chat_unread"></div>
	</div>
</div>