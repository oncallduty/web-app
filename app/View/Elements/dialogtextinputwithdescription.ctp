<div id="dialogTextInputWithDescription">
	<div class="popupDialog">
		<h2></h2>
		<div class="dialogMsg">
			<div class="dialogMsgInnerWrap">
				<span>Please provide a name for the document you are sharing:</span>
			</div>
		</div>
		<input type="text" id="dialogInput">
		<div class="dialogOkBtnWrap">
			<button id="dialogTextInputWithDescriptionCancel" class="btn_app_cancel" onclick="dismissDialogTextInputWithDescriptionCancel();"><span>Cancel</span></button>
			<button id="dialogTextInputWithDescriptionClose" class="btn_app_normal" onclick="dismissDialogTextInputWithDescription()" onkeyup="dismissDialogTextInputWithDescription()"></button>
		</div>
	</div>
</div>
<script type="text/javascript">
<!-- //
var cancelFunction = null;
var closeFunction = null;
var textInput = '';

function showDialogTextInputWithDescription( title, description, defaultText, buttonLabel ) {
	$('#dialogTextInputWithDescription div.popupDialog h2').html( title );
	$('#dialogTextInputWithDescription div.dialogMsg div.dialogMsgInnerWrap').html( '<p>'+description+'</p>' );
	$('#dialogTextInputWithDescription #dialogInput').val(defaultText);
	$('#dialogTextInputWithDescriptionClose').html( '<span>'+buttonLabel+'</span>' );
	$('#dialogTextInputWithDescription').show();
	$('#dialogTextInputWithDescription #dialogInput').focus();
}

function setDialogTextInputWithDescriptionCloseAction( action ) {
	closeFunction = action;
}

function setDialogTextInputWithDescriptionCancelAction( action ) {
	cancelFunction = action;
}

function dismissDialogTextInputWithDescription() {
	cancelFunction = null;

	if (typeof closeFunction === 'function') {
		closeFunction($('#dialogTextInputWithDescription #dialogInput').val());
		closeFunction = null;
	}

	$('#dialogTextInputWithDescription').fadeOut( 200 );
}

function dismissDialogTextInputWithDescriptionCancel() {
	closeFunction = null;

	if (typeof cancelFunction === 'function') {
		cancelFunction();
		cancelFunction = null;
	}

	$('#dialogTextInputWithDescription').fadeOut( 200 );
}
// -->
</script>