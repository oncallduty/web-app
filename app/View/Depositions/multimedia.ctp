<?= $this->Html->css( 'depositions', null, array( 'inline'=>false ) ); ?>
<?= $this->Html->css( 'introduce', null, array( 'inline'=>false ) ); ?>
<?= $this->Html->css( "brands/{$brand}/branding.css" ); ?>
<?= $this->Html->script( 'multimedia', array( 'inline' => false ) ); ?>
<div id="file_contents" class="<?=$fileType;?>">
	<div id="stamp" class="unselectable multimedia">
		<div class="dialogIntroduce" class="unselectable">
			<span type="text" id="dialogTitle" class="unselectable"></span>
			<div id="pad"></div>
			<span type="text" id="dialogSubtitle" class="unselectable"></span>
			<div id="bar"></div>
			<span id="date" class="unselectable"></span>
		</div>
	</div>

	<div id="spinner"></div>
	<div id="editor_contents">

		<div id="multimediaFileContainer">
			<?php if($fileType == 'audio') :?>
			<audio id="multimediaFileplayer" controls="controls" preload="auto" >
				<source src="<?= str_replace(['"','http://'], ['%22','https://'], $fileURL) ?>&ac=<?=time();?>" type="audio/mp3" />
				Your browser does not support audio playback.
			</audio>
			<?php else :?>
			<video id="multimediaFileplayer" class="video-js vjs-default-skin" controls="controls" preload="auto" allowFullScreen="true" webkitallowfullscreen="true" mozallowfullscreen="true">
				<source src="<?= str_replace('"', '%22', $fileURL) ?>&ac=<?=time();?>" type="video/mp4" />
				Your browser does not support video playback.
			</video>
			<?php endif; ?>

		</div>

	</div>
	<div id="depositions_footer">
		<div id="powered_by_logo_footer" ></div>
	</div>
</div>

<script type="text/javascript">
<?php // insert view js into buffered codeblock ?>
<?php $this->Html->scriptStart( array( 'inline' => false ) ); ?>
//window.document.domain = "edepoze.com";
//window.document.domain = "armyargentina.com";
window.document.domain = "<?=Configure::read('SiteConfig')['domain'];?>";

var DC = parent.Datacore;

var requestedFileID = parseInt( <?=$fileID;?> );
DC.tempFiles = JSON.parse( '<?=$tempFiles;?>' );

<?php if (isset( $datacore )) : ?>
	DC.s.depositionID = parseInt( <?= $depositionID ?> );
	<?php foreach ($datacore as $propname => $propval) : ?>
	DC.s['<?= $propname ?>'] = <?= json_encode( $propval ); ?>;
	<?php endforeach; ?>
<?php endif; ?>
<?php if( isset( $deposition ) ): ?>
top.Datacore.addDeposition( <?=json_encode( $deposition );?> );
top.webApp.setS( 'deposition', top.Datacore.getDeposition( <?=$depositionID;?> ) );
<?php endif; ?>
var GROWTH_LIMIT = 1000;
var originalWidth = parseInt( $('body').css( 'width').slice( 0, -2 ) );

$(window).resize( function() {
	var newWidth = parseInt( $('body').css( 'width').slice( 0, -2 ) );

	if ( (originalWidth < GROWTH_LIMIT && newWidth >= GROWTH_LIMIT ) || ( originalWidth > GROWTH_LIMIT && newWidth <= GROWTH_LIMIT )) {
		Multimedia.resize( newWidth >= GROWTH_LIMIT );
	}
	originalWidth = newWidth;
} );

window.parent.redirect = function() {
	top.document.getElementById( 'ipad_container_frame' ).setAttribute( 'src', '/depositions' );
};
window.parent.resetredirect = function() {
	$('#ipad_container', window.parent.document).removeClass('viewer');
	top.document.getElementById( 'ipad_container_frame' ).setAttribute( 'src', '/cases' );
};

function showSpinner() {
	var div = document.getElementById( 'editor_contents' );
	var width = div.offsetWidth - 20;
	$('#spinner').show();
	$('#spinner').width( width );
	$('#spinner').css( 'top', window.pageYOffset + 'px' );
}

function hideSpinner() {
	$('#spinner').css( 'top', '0px' );
	$('#spinner').hide();
}

function multimediaReady()
{
	if(window.top.Datacore.s.inPresentation && window.top.webApp.userIsSpeaker()) {
		window.top.webApp.presentationEnd();
	}

	// Reload file info in Datacore
	top.webApp.setS( 'inDeposition', parseInt( <?=$inDeposition;?> ) );
	var fileData = top.Datacore.getFile( requestedFileID );
	if( typeof fileData === 'undefined' || fileData === null ) {
		var _file = window.top.webApp.getItem( 'viewFile' );
		if( typeof _file !== 'undefined' && _file !== null ) {
			fileData = JSON.parse( _file );
			top.Datacore.addFile( fileData );
		}
	}
	if( typeof fileData !== 'undefined' && fileData !== null ) {
		window.top.webApp.setS( 'viewerFile', fileData );
	}

	$('#multimediaFileplayer').oncanplay = hideSpinner();

	if (DC.s.depositionStatus !== 'F') {
		// Check to see if we have an unknown user type or a blocked witness.
		var currentUser = DC.getUser(DC.s.currentUserID);
		if (typeof currentUser === 'undefined' || currentUser === null || (( currentUser.userType === 'W' || currentUser.userType === 'WM') && currentUser.blockExhibits )) {
			parent.redirect();
		}
	}
//	if (originalWidth >= GROWTH_LIMIT) {
//		$('#ipad_navbar', window.parent.document).addClass('hideSocialList').removeClass('showSocialList');
//	} else {
//		$('#ipad_navbar', window.parent.document).addClass('showSocialList').removeClass('hideSocialList');
//	}
	$('#ipad_navbar', window.parent.document).addClass('viewer');
	$('#ipad_container', window.parent.document).addClass('viewer');
	$('#social_list_container, #social_transcript_tab, #liveFeed', window.top.document).removeClass( 'classic fullscreenShort fullscreenTall', {duration : 200, easing : 'linear'});

	window.parent.requestedFileID = requestedFileID;

	// setup attendee list
	if (DC.s.depositionStatus === '<?= EDCase::STATUS_INPROGRESS ?>') {
		$('#attendee_list_container', top.document).show();
	}
	if (!DC.s.connectedToDeposition && DC.s.depositionStatus != 'F') {
		top.connect_to_deposition();
	}

	if( window.top.Datacore.s.userType === 'M' && (typeof window.top.fileSelection === 'undefined' || window.top.fileSelection === null) ) {
		window.top.fileSelection = new window.top.FileSelection();
		window.top.fileSelection.enableFileSelection();
	}

	var date = new Date ();
	DC.s.exhibitDate = (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear().toString().substr(2);
	DC.s.oldExhibitText = {title: DC.s.exhibitTitle, subTitle: DC.s.exhibitSubTitle};

	Multimedia.init(<?= $beginIntroduce ?>);
	window.top.Datacore.syncAllFolders();

	window.onbeforeunload = function()
	{
		window.top.webApp.toggleAttendPresentationBtn(false, false);
		delete window.top.Multimedia;
	};
}
top.webApp.onReady( multimediaReady );

window.top.introduceRedirect = function(newFileID, toDeposition) {
	if( !top.Multimedia.isDragIntroduce  && typeof newFileID !== undefined && newFileID !== null && newFileID > 0) {
		requestedFileID = newFileID;
		console.log('redirecting to ' + window.location.pathname + '?ID=' + requestedFileID);
		top.document.getElementById( 'ipad_container_frame' ).setAttribute( 'src', window.location.pathname + '?ID='+requestedFileID );
	} else {
		console.log('redirecting to /depositions');
		top.document.getElementById( 'ipad_container_frame' ).setAttribute( 'src', '/depositions' );
	}
};
<?php $this->Html->scriptEnd( array( 'inline' => false ) ); ?>
</script>
