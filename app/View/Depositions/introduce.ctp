<?php $this->Html->css( 'depositions', null, ['inline'=>FALSE] ); ?>
<?php $this->Html->css( 'introduce', null, array( 'inline'=>false ) ); ?>
<?=$this->Html->css( "brands/{$brand}/branding.css" );?>
<div id="spinner" style="display:none;"></div>
<div id="file_contents" class="introduce">
	<div id="editor_contents">
		<div id="doc_viewer" class="introduce">
			<iframe id="doc_viewer_frame" name="doc_viewer_frame" src=""></iframe>
		</div>
	</div>
</div>
<div id="depositions_footer">
	<div id="powered_by_logo_footer" ></div>
</div>


<script type="text/javascript">
<?php $this->Html->scriptStart( ['inline'=>FALSE] ); ?>
//window.document.domain = "edepoze.com";
//window.document.domain = "armyargentina.com";
window.document.domain = "<?=Configure::read('SiteConfig')['domain'];?>";

var requestedFileID = parseInt( <?=$fileID;?> );
var fromView = parseInt( <?=$fromView;?> );
var introduceFilename = null;
window.top.Datacore.tempFiles = JSON.parse( '<?=$tempFiles;?>' );

<?php if (isset( $datacore )) : ?>
window.top.Datacore.s.depositionID = parseInt( <?= $depositionID ?> );
<?php foreach ($datacore as $propname => $propval) : ?>
window.top.Datacore.s['<?= $propname ?>'] = <?= json_encode( $propval ); ?>;
<?php endforeach; ?>
<?php endif; ?>
<?php if( isset( $deposition ) ): ?>
window.top.Datacore.addDeposition( <?=json_encode( $deposition );?> );
window.top.webApp.setS( 'deposition', window.top.Datacore.getDeposition( <?=$depositionID;?> ) );
<?php endif; ?>

var GROWTH_LIMIT = 1000;
var originalWidth = parseInt( $('body').css( 'width').slice( 0, -2 ) );

$(window).resize( function() {
	var newWidth = parseInt( $('body').css( 'width').slice( 0, -2 ) );
	if (originalWidth < GROWTH_LIMIT && newWidth >= GROWTH_LIMIT)
		frameGrown();
	if (originalWidth > GROWTH_LIMIT && newWidth <= GROWTH_LIMIT)
		frameShrunk();
	originalWidth = newWidth;
} );

function frameShrunk() {
}

function frameGrown() {
}

function showFileViewer( fileID ) {
	console.log( 'introduce::showFileViewer()', fileID );
	var fileData = window.top.Datacore.getFile( fileID );
	if( typeof fileData === 'undefined' || fileData === null ) {
		var _file = window.top.webApp.getItem( 'viewFile' );
		if( typeof _file !== 'undefined' && _file !== null ) {
			fileData = JSON.parse( _file );
			window.top.Datacore.addFile( fileData );
		}
	}
//	console.log( 'showFileViewer -- fileData:', fileData );
	if( typeof fileData !== 'undefined' && fileData !== null ) {
//		window.top._titleActions.setTitle( fileData.name );
		window.top.webApp.setS( 'viewerFile', fileData );
		window.top.webApp.setS( 'viewerFilename', fileData.name );
		window.top.navBar.setTitle( fileData.name );
		window.top.navBar.showTitle();
		window.top.navBar.updateDefaults();
		$('#saveFile').parent().attr( 'download', fileData.name );
	}
	//var File = window.top.Datacore.getFile( fileID );
	<?php /*
	// File.url = '<?= $fileURL ?>';
	*/ ?>

	// set the file title
	if (typeof window.top.Datacore.s.tempFile !== 'undefined' && window.top.Datacore.s.tempFile !== null && window.top.Datacore.s.tempFile.fileID === fileID) {
		window.top.navBar.setTitle( window.top.Datacore.s.tempFile.originalFilename + '.pdf' );
		$('#saveFile').parent().attr( 'download', window.top.Datacore.s.tempFile.originalFilename + '.pdf' );
	}

	// load the iframe with the document viewer
	window.top.showLoadingOverlay( 'Introducing...' );
	var viewer = "<?=$pdfViewerURL;?>" + fileID + "/viewer/" + window.top.Datacore.s.sKey;
	console.log( viewer );
//	$('#doc_viewer').html( '<iframe src="' + viewer + '"></iframe>' );
	$("#doc_viewer_frame").attr( "src", viewer );

//	var viewer = '/depositions/introduceviewer';
//	$('#doc_viewer').html( '<iframe src="'+viewer+'?ID='+fileID+'"></iframe>' );

	if ($('#dlgActionCancel', window.top.document).length > 0) {
		$('#dlgActionCancel', window.top.document).off().on( 'click', function() {
			$('#doc_viewer').show();
		} );
	}
}

function view_file( fileID ) {
	window.location.href='/depositions/pdf?ID='+fileID;
}

window.top.introduceRedirect = function(newFileID, toDeposition) {
	toDeposition = (typeof toDeposition !== 'undefined' && toDeposition !== null) ? toDeposition : false;
	if( fromView && !toDeposition ) {
		if( typeof newFileID !== undefined && newFileID !== null && newFileID > 0 ) {
			requestedFileID = newFileID;
		}
		console.log('redirecting to /depositions/pdf?ID='+requestedFileID);
		window.top.document.getElementById( 'ipad_container_frame' ).setAttribute( 'src', '/depositions/pdf?ID='+requestedFileID );
	} else {
		console.log('redirecting to /depositions');
		window.top.document.getElementById( 'ipad_container_frame' ).setAttribute( 'src', '/depositions' );
	}
};

$(document).ready( function() {
	console.log( 'introduce -- document ready', requestedFileID );
	$('#ipad_container', window.top.document).removeClass('fullscreen');

//	if (originalWidth >= GROWTH_LIMIT) {
//		$('#ipad_navbar', window.top.document).addClass('hideSocialList').removeClass('showSocialList');
//	} else {
//		$('#ipad_navbar', window.top.document).addClass('showSocialList').removeClass('hideSocialList');
//	}
	$('#ipad_navbar', window.top.document).addClass('viewer');
	$('#ipad_navbar, #ipad_container', window.top.document).addClass('viewer');
//	$('#social_list_container, #social_transcript_tab, #liveFeed', window.top.document).switchClass( 'fullscreenShort fullscreenTall', 'classic', {duration : 200, easing : 'linear'} );

	// setup attendee list
	if( window.top.Datacore.s.depositionStatus === '<?= EDCase::STATUS_INPROGRESS ?>' ) {
		$('#attendee_list_container', window.top.document).show();
	}

	showFileViewer( requestedFileID );

	if( !window.top.Datacore.s.connectedToDeposition && window.top.Datacore.s.depositionStatus !== 'F' ) {
		parent.connect_to_deposition();
	}

} );

var showSpinner = window.top.showSpinner = function() {
	var div = document.getElementById('file_contents');
	var width = div.offsetWidth - 10;
	$('#spinner').show();
	$('#spinner').width(width);
	$('#spinner').css('top', window.pageYOffset + 'px');
};

var hideSpinner = window.top.hideSpinner = function() {
	$('#spinner').css('top', '0px');
	$('#spinner').hide();
};

function introduceReady()
{
	console.log( 'introduce::onReady' );
	window.top.webApp.setS( 'inDeposition', parseInt( <?=$inDeposition;?> ) );
	window.top.webApp.setS( 'deposition', window.top.Datacore.getDeposition( <?=$depositionID;?> ) );
	// Check to see that we are still the current speaker.
	if( window.top.webApp.userIsSpeaker() !== true ) {
//		window.top.introduceRedirect( requestedFileID, true );
		alert('User is not speaker, redirecting');
		window.top.iPadContainerFrame.location.replace( '/depositions' );
	}
//	var fileData;
//	var _file = sessionStorage.getItem( 'viewFile' );
//	if( typeof _file !== 'undefined' && _file !== null ) {
//		fileData = JSON.parse( _file );
//		window.top.Datacore.addFile( fileData );
//	}
//	if( typeof fileData !== 'undefined' && fileData !== null ) {
//		window.top.navBar.setTitle( fileData.name );
//		window.top.navBar.showTitle();
//	}
//	window.onbeforeunload = function( e )
//	{
//		delete window.top.Introduce;
//	};
	window.top.webApp.setS('beginIntroduce', 'yes');
	window.top.webApp.setS('fromView', fromView);
	setTimeout( function() { window.top.Datacore.syncAllFolders(); }, 200 );
}
window.top.webApp.onReady( introduceReady );
<?php $this->Html->scriptEnd(); ?>
</script>
