<?php $this->Html->css( 'depositions', NULL, ['inline'=>FALSE] ); ?>
<?php $this->Html->css( "brands/{$brand}/branding.css", NULL, ['inline'=>FALSE] ); ?>
<div id="file_contents" class="<?=(AppHelper::appIsFullscreen() ? 'fullscreen' : '');?>">
	<div id="editor_contents">
		<div id="doc_viewer">
			<iframe id="doc_viewer_frame" name="doc_viewer_frame" src=""></iframe>
		</div>
	</div>
	<!--div id="depositions_footer">
		<div id="powered_by_logo_footer" ></div>
	</div-->
</div>

<script type="text/javascript">
<?php $this->Html->scriptStart( ['inline'=>FALSE] ); ?>
//window.document.domain = "edepoze.com";
//window.document.domain = "armyargentina.com";
window.document.domain = "<?=Configure::read('SiteConfig')['domain'];?>";


var requestedFileID = parseInt( <?=$fileID;?> );
window.top.Datacore.tempFiles = JSON.parse( '<?=$tempFiles;?>' );
window.top.contentSearch = null;

<?php if( isset( $datacore ) ): ?>
window.top.Datacore.s.depositionID = parseInt( <?=$depositionID;?> );
<?php foreach ($datacore as $propname => $propval): ?>
window.top.Datacore.s['<?= $propname ?>'] = <?=json_encode( $propval );?>;
<?php endforeach; ?>
<?php endif; ?>
<?php if( isset( $deposition ) ): ?>
window.top.Datacore.addDeposition( <?=json_encode( $deposition );?> );
window.top.webApp.setS( 'deposition', window.top.Datacore.getDeposition( <?=$depositionID;?> ) );
<?php endif; ?>

var GROWTH_LIMIT = 1000;
var originalWidth = parseInt( $('body').css( 'width').slice( 0, -2 ) );

$(window).resize( function() {
	var newWidth = parseInt( $('body').css( 'width').slice( 0, -2 ) );
	if (originalWidth < GROWTH_LIMIT && newWidth >= GROWTH_LIMIT)
		frameGrown();
	if (originalWidth > GROWTH_LIMIT && newWidth <= GROWTH_LIMIT)
		frameShrunk();
	originalWidth = newWidth;
} );

function frameShrunk() {
}

function frameGrown() {
}

function showFileViewer( fileID )
{
	window.top.webApp.setS( 'useListScrollTop', true );
	console.log( 'PDF::showFileViewer()', fileID );
	var fileData = window.top.Datacore.getFile( fileID );
	if( typeof fileData === 'undefined' || fileData === null ) {
		var _file = window.top.webApp.getItem( 'viewFile' );
		if( typeof _file !== 'undefined' && _file !== null ) {
			fileData = JSON.parse( _file );
			window.top.Datacore.addFile( fileData );
		}
	}
//	console.log( 'showFileViewer -- fileData:', fileData );
	if( typeof fileData !== 'undefined' && fileData !== null ) {
//		window.top._titleActions.setTitle( fileData.name );
		window.top.webApp.setS( 'viewerFile', fileData );
		window.top.webApp.setS( 'viewerFilename', fileData.name );
		window.top.navBar.setTitle( fileData.name );
		window.top.navBar.showTitle();
		window.top.navBar.updateDefaults();
		$('#saveFile').parent().attr( 'download', fileData.name );
	}

	// set the file title
	if (typeof window.top.Datacore.s.tempFile !== 'undefined' && window.top.Datacore.s.tempFile !== null && window.top.Datacore.s.tempFile.fileID === fileID) {
		window.top.navBar.setTitle( window.top.Datacore.s.tempFile.originalFilename + '.pdf' );
		$('#saveFile').parent().attr( 'download', window.top.Datacore.s.tempFile.originalFilename + '.pdf' );
	}
	// load the iframe with the document viewer
//	var viewer = '/depositions/pdfviewer';
//	$('#doc_viewer').html( '<iframe src="'+viewer+'?ID='+fileID+'"></iframe>' );
	window.top.showLoadingOverlay( 'Opening...' );
	var viewer = "<?=$pdfViewerURL;?>" + fileID + "/viewer/" + window.top.Datacore.s.sKey;
	console.log( viewer );
//	$('#doc_viewer').html( '<iframe id="doc_viewer_frame" name="doc_viewer_frame" src="' + viewer + '"></iframe>' );
	$("#doc_viewer_frame").attr( "src", viewer );

	if ($('#dlgActionCancel', window.top.document).length > 0) {
		$('#dlgActionCancel', window.top.document).off().on( 'click', function() {
			$('#doc_viewer').show();
		} );
	}
	if( window.top.Datacore.s.userType === 'M' && (typeof window.top.fileSelection === 'undefined' || window.top.fileSelection === null) ) {
		window.top.fileSelection = new window.top.FileSelection();
		// contentSearch must be initialized before the call to enableFileSelection()
		window.top.contentSearch = new window.top.ContentSearch( 'Slideout', $('.content_search_container', window.top.document), $('.fsTablesPanel', window.top.document), $('#fsFilesList ul', window.top.document), window.top.fileSelection );
		window.top.fileSelection.enableFileSelection();
	}
}

function view_file( fileID ) {
	window.location.href='/depositions/pdf?ID='+fileID;
}

$(document).ready( function() {
//	if (originalWidth >= GROWTH_LIMIT) {
//		$('#ipad_navbar', window.top.document).removeClass( 'showSocialList' ).addClass( 'hideSocialList' );
//	} else {
//		$('#ipad_navbar', window.top.document).removeClass( 'hideSocialList' ).addClass( 'showSocialList' );
//	}
	$('#ipad_navbar, #ipad_container', window.top.document).addClass( 'viewer' );

	// initialize the titlebar
//	parent._titleActions.hideAll();
//	parent._titleActions.showBack();
//	parent._titleActions.showFullscreen();
//	parent._titleActions.showViewer();
//	parent._titleActions.setBackAction( 'javascript:;' ); // The annotation code will invoke window.parent.redirect()
//	parent._titleActions.setTitle( 'Loading...' );
	if( window.top.webApp.userIsSpeaker() === true ) {
//		parent._titleActions.showPresentation();
		window.top.navBar.presentationButton.enable();
	}
//	parent._titleActions.redraw();

	if (window.top.Datacore.s.userType === 'W') {
		$('#content #arrow').hide();
		$('#content #colorArrow').hide();
		$('#content #note').hide();
		$('#content #colorNote').hide();
		$('#content #erase').hide();
		$('#content #undo').hide();
		$('#content #removeAll').hide();
	}
	// setup attendee list
	if (window.top.Datacore.s.depositionStatus === '<?= EDCase::STATUS_INPROGRESS ?>' && window.top.Datacore.s.userType !== 'W' && window.top.Datacore.s.userType !== 'TW') {
		$('#attendee_list_container', window.top.document).show();
	}

//	console.log( 'requestedFileID', requestedFileID );
	var pData = window.top.webApp.getS( 'joinPresentationData' );
	if( window.top.Datacore.hasFile( requestedFileID ) || ( typeof pData !== 'undefined' && pData !== null && !window.top.webApp.userIsSpeaker()) ) {
		window.parent.requestedFileID = null;
		showFileViewer( requestedFileID );
	} else {
		var viewFile = window.top.webApp.getItem( 'viewFile' );
		if( typeof viewFile !== 'undefined' && viewFile !== null ) {
			var _file = JSON.parse( viewFile );
			window.top.navBar.setTitle( _file.name );
//			console.log( 'PDF -- sync folders' );
		}
		showFileViewer( requestedFileID );
	}

	if( !window.top.Datacore.s.connectedToDeposition && window.top.Datacore.s.depositionStatus !== 'F' ) {
		parent.connect_to_deposition();
	}

	window.onbeforeunload = function( e )
	{
		if( window.top.isset( window.top.contentSearch ) && window.top.contentSearch.getInSearch() ) {
			window.top.contentSearch.storeSearchValues();
		}
		delete window.top.contentSearch;
	};
	$('body').on( 'touchmove', function( event ) { window.top.webApp.touchHandlerHelper( event ); } );
} );

var showSpinner = window.top.showSpinner = function() {
	var div = document.getElementById('file_contents');
	var width = div.offsetWidth - 10;
	$('#spinner').show();
	$('#spinner').width(width);
	$('#spinner').css('top', window.pageYOffset + 'px');
};

var hideSpinner = window.top.hideSpinner = function() {
	$('#spinner').css('top', '0px');
	$('#spinner').hide();
};

function pdfReady()
{
//	console.log( 'pdf::onReady' );
	window.top.webApp.setS( 'inDeposition', parseInt( <?=$inDeposition;?> ) );
	window.top.webApp.setS('beginIntroduce', 'no');
	window.top.webApp.setS('presentation', 'no');
	var trialTypes = ['Trial', 'Demo Trial', 'Arbitration', 'Mediation', 'Hearing'];
	trialTypes.indexOf(window.top.webApp.getS( 'deposition.class' )) > -1 ? window.top.navBar.introduceButton.label( 'Mark' ) : window.top.navBar.introduceButton.label( 'Introduce' );
	setTimeout( function() { window.top.Datacore.syncAllFolders(); }, 200 );
}
window.top.webApp.onReady( pdfReady );
<?php $this->Html->scriptEnd(); ?>
</script>
