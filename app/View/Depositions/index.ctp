<?php
	$this->Html->css( 'usersort', NULL, ['inline'=>FALSE] );
	$this->Html->css( 'contentsearch', NULL, ['inline'=>FALSE] );
	$this->Html->css( 'depositions', NULL, ['inline'=>FALSE] );
	$this->Html->css( "brands/{$brand}/branding.css", NULL, ['inline'=>FALSE] );
?>
<head>


 

	<link href="../asset/css/custom-admin.css" rel="stylesheet" type="text/css">
	<link href="../asset/css/AdminLTE.min.css" rel="stylesheet" type="text/css">
	<?=$this->Html->css( 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' );?>
			
	<?=$this->Html->css( 'https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css' );?>
	<?=$this->Html->css( 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic' );?>
	<?=$this->Html->css( 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );?>

  


 
</head>
<div id="deposition_content">
	
	<div class="content_search_container"></div>
	

		<div class="tablesPanel">			
			<div id="depositions_header">
					<div  title="Join Presentation" id="attend_presentation" class="notActive"></div>
					<div id="deposition_info" class="hidePresentationBtn">
						<p class="deposition_name"></p>
						<p class="deposition_number"></p>
						<p class="deposition_passcodetitle">Passcode:</p><p title="Deposition Passcode" class="deposition_passcode"></p>
						<div id="btn_start_deposition" class="btn_start">Begin Session</div>
						<div id="btn_finish_deposition" class="btn_finish">Finish Session</div>
					</div>
					
			</div>
			
			<div class="foldersPanel">
				<div class="legend">Folders (0)</div>
				<div id="sortFolders" class="sortHeader">
				</div>
				<div id="foldersList">
					<ul></ul>
				</div>
			</div>
			<div class="filesPanel">
				
				<div class="legend"><?=$userType=='W'?'Exhibits':'Documents';?> (0)</div>
				<div id="sortFiles" class="sortHeader"></div>
				<div id="sortCSFiles" class="sortHeader"></div>
				<div id="filesList">
					<ul></ul>
					<div class="folderActions">
						<div class="actionBtns">
							<div class="cancelBtn">Cancel</div>
							<div class="okayBtn">Email</div>
						</div>
						<div class="downloadBtn" title="Download"></div>
						<div class="emailBtn" title="Email"></div>
						<div class="sendPortalInviteBtn" title="Send Portal Invite"></div>
					</div>
					<div id="witnessBlock" style="display:none;">Blocked</div>
				</div>
			</div>
		</div>
		
</div>

<script type="text/javascript">
<?php $this->Html->scriptStart( ['inline'=>FALSE] ); ?>
//window.document.domain = "edepoze.com";
//window.document.domain = "armyargentina.com";
window.document.domain = "<?=Configure::read('SiteConfig')['domain'];?>";

if( parent === window ) {
	window.top.location.replace( '/i' );
}

//window.top.Datacore.clean();

<?php if (isset( $datacore )) : ?>
	window.top.Datacore.s.depositionID = parseInt( <?= $depositionID ?> );
<?php foreach( $datacore as $propname => $propval ): ?>
	window.top.Datacore.s['<?= $propname ?>'] = <?= json_encode( $propval ); ?>;
<?php endforeach; ?>
<?php endif; ?>

window.top.Datacore.addDeposition( <?=json_encode( $deposition );?> );
window.top.Datacore.loadExhibitHistory( <?= $exhibitHistory ?> );

window.top.userSortFolders = null;
window.top.userSortFiles = null;
window.top.contentSearch = null;
window.top.tablesPanel = null;

window.top.webApp.removeItem( 'viewFile' );

function documentReady()
{
	if( !window.top.loadingOverlayIsVisible() ){
		window.top.showLoadingOverlay( 'Loading...' );
	}

//	console.log( 'index::documentReady()' );
	$('#ipad_navbar, #ipad_container', window.top.document).removeClass( 'viewer' );
	window.top.webApp.setAppOrientation( {"orientation": "landscape"} );
	window.top.webApp.setS( 'inDeposition', parseInt( <?=$inDeposition;?> ) );
	window.top.webApp.setS( 'deposition', window.top.Datacore.getDeposition( <?=$depositionID;?> ) );
	window.top.tablesPanel = new TablesPanel( $('.tablesPanel') );
	window.top.userSortFolders = new window.top.UserSort( 'Folders', $('#sortFolders'), window.top.tablesPanel.sortFolders, window.top.tablesPanel );
	window.top.userSortFiles = new window.top.UserSort( 'Files', $('#sortFiles'), window.top.tablesPanel.sortFiles, window.top.tablesPanel );
	window.top.contentSearch = new window.top.ContentSearch( 'Session', $('.content_search_container'), $('.tablesPanel'), $('#filesList ul'), window.top.tablesPanel );
	if( window.top.webApp.getS( 'user.userType' ) === 'M' && (window.top.webApp.getS( 'deposition.class' ) !== 'WitnessPrep' && window.top.webApp.getS( 'deposition.class' ) !== 'WPDemo') ) {
		if( typeof window.top.liveTranscript !== 'undefined' && window.top.liveTranscript !== null ) {
			if( window.top.liveTranscript.depositionID !== window.top.webApp.getS( 'inDeposition' ) ) {
				window.top.liveTranscript = new window.top.LiveTranscripts( window.top.webApp.getS( 'inDeposition' ) );
			}
		} else {
			window.top.liveTranscript = new window.top.LiveTranscripts( window.top.webApp.getS( 'inDeposition' ) );
		}
	} else {
		delete window.top.liveTranscript;
	}

	var userType = window.top.webApp.getS( 'user.userType' );
	if( userType === 'W' ) {
		$('#content').addClass( 'witness' );
	}

	$('#ipad_container', parent.document).removeClass('fullscreen');
	if(isset( window.top.Datacore.s.tempFile )) {
		delete window.top.Datacore.s.tempFile;
	}
	$('#ipad_navbar', window.top.document).removeClass( 'viewer' );
	$('#ipad_container', window.top.document).removeClass( 'viewer' );
	$('#social_list_container, #social_transcript_tab, #liveFeed', window.top.document).removeClass( 'classic fullscreenShort fullscreenTall', {duration : 200, easing : 'linear'});

	// setup titlebar
//	window.top._titleActions.hideAll();
	window.top.navBar.allDisable();
	window.top.navBar.showLogo();
	window.top.navBar.showWelcome();
//	window.top._titleActions.showLogout();
	window.top.navBar.logoutButton.enable();
	window.top.navBar.canHideToolbar( false );

	if( userType === 'M' ) {
//		window.top._titleActions.showCases();
		window.top.navBar.casesButton.enable();
	}
//	window.top._titleActions.presentationActive( window.top.Datacore.s.presentationAvailable );
	if( typeof window.top.tutorialVideos === 'object' && window.top.tutorialVideos !== null ) window.top.tutorialVideos.bind();

	// setup attendee list
	if (window.top.Datacore.s.depositionStatus === '<?= EDCase::STATUS_INPROGRESS ?>' && userType !== 'W' && userType !== 'TW') {
		$('#attendee_list_container', window.parent.document).show();
	} else {
		$('#attendee_list_container', window.parent.document).hide();
	}

	if (isset( window.top.Datacore.s.started )) {
		window.top.Datacore.s.started = convertDate( window.top.Datacore.s.started );
	}

	// set page elements
	$('#depositions_header .deposition_name').html( '<img src="/asset/images/title.png">&nbsp;' + window.top.Datacore.s.depositionOf );

	$('#btn_start_deposition').hide();
	$('#btn_finish_deposition').hide();
	if (window.top.Datacore.s.currentUserID == window.top.Datacore.s.depositionOwnerID && window.top.Datacore.getS('parentID') === 0) {
		$('#depositions_header .deposition_number').text( 'SessionID: '+window.top.Datacore.s.depositionID );
		// setup depostion buttons
		if (window.top.Datacore.s.depositionStatus === '<?= EDCase::STATUS_NOTSTARTED ?>') {
			$('#btn_start_deposition').show();
		} else if (window.top.Datacore.s.depositionStatus === '<?= EDCase::STATUS_INPROGRESS ?>') {
			$('#btn_finish_deposition').show();
			$('.deposition_passcode').css( 'display', 'inline-block' );
			$('.deposition_passcodetitle').css( 'display', 'inline-block' );
		}
	} else {
		$('#depositions_header .deposition_passcode').hide();
		$('#depositions_header .deposition_passcodetitle').hide();
		
	}

	if(typeof window.top.webApp !== 'undefined') {
		window.top.webApp.toggleAttendPresentationBtn(false, false);
	}
	if ( window.top.webApp.isWitness() ) {
		$('#attend_presentation').hide();
	}

	if( window.top.Datacore.s.userType === 'G' || window.top.Datacore.s.userType === 'WM' && $('#users_group_add', window.top.document).length > 0) {
		$('#users_group_add', window.top.document).remove();
	}

	if( typeof window.top.fileSelection !== 'undefined' && window.top.fileSelection !== null ) {
		window.top.fileSelection.disableFileSelection();
		delete window.top.fileSelection;
	}

//	if ($('#attendee_list_toggle_wrap', window.parent.document).hasClass( 'showSocialList' )) {
//		$('.tablesPanel #files').addClass( 'showSocialList' );
//	}

	// Sync all of the folders.
	window.top.Datacore.syncAllFolders( function() {
//		console.log( 'index::window.top.Datacode.syncAllFolders()' );
		var folders = window.top.userSortFolders.sort( window.top.Datacore.getFolders() );
		window.top.tablesPanel.sortFolders();
		var selectedFolderID = parseInt( window.top.webApp.getItem( 'selectedFolderID' ) );
		for( var f in folders ) {
			if( folders[f].ID === selectedFolderID ) {
//				window.top.Datacore.syncFolder( selectedFolderID, function() { window.top.tablesPanel.selectFolder( selectedFolderID ); } );
				window.top.tablesPanel.selectFolder( selectedFolderID );
				window.top.dismissLoadingOverlay();
				return;
			}
		}
		if( folders.length > 0 ) {
			var folderID = folders[0].ID;
			window.top.webApp.setItem( 'selectedFolderID', folderID );
//			$('#foldersList ul li').removeClass( 'selected' );
//			$('#folder_item_' + folders[0].ID).addClass( 'selected' );
			window.top.tablesPanel.selectFolder( folderID );
//			window.top.Datacore.syncFolder( folderID, function() { window.top.tablesPanel.sortFolders(); } );
			window.top.tablesPanel.sortFolders();
		}
		window.top.dismissLoadingOverlay();
	} );

	if( !window.top.Datacore.s.connectedToDeposition && window.top.Datacore.s.depositionStatus !== 'F' ) {
		window.top.connect_to_deposition();
	}

//	window.top.checkPresentation();	//connect_to_deposition calls checkPresentation, this ends up as a duplicate call
//	var depositionID = ( window.top.Datacore.s.parentID !== 0 ) ? window.top.Datacore.s.parentID : window.top.Datacore.s.depositionID;
//	var depositionID = window.top.Datacore.getRootDepositionID();	//unused

	window.onbeforeunload = function( e )
	{
		if( window.top.isset( window.top.contentSearch ) && window.top.contentSearch.getInSearch() ) {
			window.top.contentSearch.storeSearchValues();
		}
		delete window.top.tablesPanel;
		delete window.top.permissions;
		delete window.top.sendDialog;
		delete window.top.userSortFolders;
		delete window.top.userSortFiles;
		delete window.top.contentSearch;
		delete window.top.resetredirect;
		if( typeof window.top.fileSelection !== 'undefined' && window.top.fileSelection !== null ) {
			window.top.fileSelection.disableFileSelection();
			delete window.top.fileSelection;
		}
	};
	$('body').on( 'touchmove', function( event ) { window.top.webApp.touchHandlerHelper( event ); } );
}
window.top.webApp.onReady( documentReady );

function isset( v ) {
	return (typeof v !== 'undefined' && v !== null);
}

function view_file( fileID, mimeType )
{
	window.top.webApp.setItem( 'selectedFileID', fileID );
	if( mimeType.indexOf( "audio" ) > -1 ) {
		window.location.href = '/depositions/audio?ID=' + fileID;
	}
	else if( mimeType.indexOf( "video" ) > -1 ) {
		window.location.href = '/depositions/video?ID=' + fileID;
	}
	else {
		window.location.href = '/depositions/pdf?ID=' + fileID;
	}
}

function on_drag_start( event, dcObjType, dcObjID ) {
	if(!!event.impersonated) {
		dcObjType = event.data.dcObjType;
		dcObjID = event.data.dcObjID;
	}
	var dragType = (dcObjType === 'folder') ? 'folder' : 'document';
	window.top.showTooltip( "Drag and Drop " + dragType + " to Group or Specific Individual", false );
	var target = event.target;
	if (!$(target).hasClass( 'file_info' )) {
		if ($(target).parents('.file_info').length === 1) {
			target = $(target).parents('.file_info');
		}
	}
	$('#drag_image').remove();
	var drag_image = $( target ).clone();
	drag_image.attr( 'id', 'drag_image' );
	drag_image.removeAttr( 'class' );
	drag_image.removeAttr( 'style' );
	drag_image.removeAttr( 'draggable' );
	$('#foldersList').append( drag_image );
	$('#drag_image .name').remove();
	$('#drag_image .date').remove();
	$('#drag_image .exhibitHistory').remove();
	$('#drag_image .dragBox').remove();
	window.top.Datacore.s.dragging = dcObjType;
	if(!!event.impersonated) {
		event.dataTransfer.helper.css({position:'fixed','left':event.clientX-10,'top':event.clientY-25,'z-index':100});
		event.dataTransfer.helper.text(event.data.name);
		$('#foldersList').append(event.dataTransfer.helper);
	} else {
		event.dataTransfer.effectAllowed = 'copyMove';
		event.dataTransfer.setData( 'text', JSON.stringify( {type:dcObjType,id:dcObjID} ) );
		if (typeof event.dataTransfer.setDragImage === 'function') {
			event.dataTransfer.setDragImage( drag_image.get(0), 10, 25 );
		}
	}
}

function on_drag( event) {
	if(!!event.impersonated) {
		var helper = event.dataTransfer.helper;
		helper.css({'left':event.clientX-event.dataTransfer.offsetX,'top':event.clientY-event.dataTransfer.offsetY});
		window.top.webApp.draggableMove(event.clientX, event.clientY);
	}
}

function on_drag_end( event ) {
	window.top.dismissTooltip();
	window.top.Datacore.s.dragging = null;
	$('#drag_image').remove();
	$('.drag_over', window.top.document).each( function() {
		$( this ).removeClass( 'drag_over' );
	} );
	if(!!event.impersonated) {
		var dcObjType = event.data.dcObjType;
		var dcObjID = event.data.dcObjID;
		event.dataTransfer.setData( 'text', JSON.stringify( {type:dcObjType,id:dcObjID} ) );
		$('#file_drag_helper').remove();
		window.top.webApp.draggableDrop(event, event.clientX, event.clientY);
	}
}

var touchDragHandlerInProcess = false;
var touchDragHandlerStartTracking = null;
function touchDragHandler( event, data ) {
	var jEvent = event;
	event = event.originalEvent;
    var touches = event.changedTouches,
        first = touches[0],
        type = "";
    switch(event.type) {
        case "touchstart":
			type = "mousedown";
			touchDragHandlerStartTracking = first;
			touchDragHandlerInProcess = false;
			setTimeout(function(){
				if(touchDragHandlerStartTracking !== null) {
					touchDragHandlerStartTracking = null;
				}
			}, 2000);
			break;
        case "touchmove":
			if(touchDragHandlerStartTracking !== null
				&& (Math.abs(first.screenX-touchDragHandlerStartTracking.screenX) > 5
					|| Math.abs(first.screenY-touchDragHandlerStartTracking.screenY) > 5)) {
				touchDragHandlerInProcess = true;
				type = "dragstart";
				break;
			}
			if(!touchDragHandlerInProcess) {
				event.preventDefault();
				return;
			} else {
				type = "drag";
			}
			break;
        case "touchend":
			if(touchDragHandlerInProcess) {
				type = "dragend";
			} else {
				type = "click";
			}
			touchDragHandlerInProcess = false;
			touchDragHandlerStartTracking = null;
			break;
        case "touchcancel":
			if(touchDragHandlerInProcess) {
				type = "dragend";
			} else {
				type = "click";
			}
			touchDragHandlerInProcess = false;
			touchDragHandlerStartTracking = null;
			break;
        default:           return;
    }
    var simulatedEvent;
	if (document.createEvent) {
		simulatedEvent = document.createEvent("HTMLEvents");
		simulatedEvent.initEvent(type, true, true);
	} else {
		simulatedEvent = document.createEventObject();
		simulatedEvent.eventType = type;
	}
	simulatedEvent.eventName = type;
	simulatedEvent.view = window;
	simulatedEvent.detail = 1;
	simulatedEvent.screenX = first.screenX;
	simulatedEvent.screenY = first.screenY;
	simulatedEvent.clientX = first.clientX;
	simulatedEvent.clientY = first.clientY;
	simulatedEvent.ctrlKey = false;
	simulatedEvent.altKey = false;
	simulatedEvent.shiftKey = false;
	simulatedEvent.metaKey = false;
	simulatedEvent.button = 0;
	simulatedEvent.relatedTarget = null;
	simulatedEvent.impersonated = true;
	simulatedEvent.data = jEvent.data;
	simulatedEvent = $.extend(simulatedEvent, data);
	if (document.createEvent) {
		event.target.dispatchEvent(simulatedEvent);
	} else {
		event.target.fireEvent("on" + simulatedEvent.eventType, simulatedEvent);
	}
	event.preventDefault();
}


$('#btn_start_deposition').click( function() {
	var id = window.top.Datacore.s.depositionID;
	$.getJSON( 'depositions/startDeposition', null, function(data, textStatus, xhr) {
		if (textStatus === 'success') {
			if (typeof data.error === 'undefined') {
				$('#btn_start_deposition').hide();
				$('#btn_finish_deposition').show();
				$('.deposition_passcode').css( 'display', 'inline-block' );
				$('#attendee_list_container', window.top.document).show();
				var lDate = new Date();
				var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
				var dateString = months[lDate.getMonth()] + ' ' + lDate.getDate() + ', ' + lDate.getFullYear();
				var _started = lDate.getUTCFullYear() + '-' + (lDate.getUTCMonth()+1) + '-' + lDate.getUTCDate() + ' ' + lDate.getUTCHours() + ':' + lDate.getUTCMinutes() + ':' + lDate.getUTCSeconds();
				window.top.Datacore.s.started = convertDate( _started );
				window.top.showDialog1Btn( 'Session Started', 'Session ID: '+id+'<br /> Passcode: '+$('<div/>').text(data.EDDeposition.key).html()+'<br />Start Date: '+dateString, 'OK ' );
			} else {
				$('#btn_finish_deposition').hide();
				$('#btn_start_deposition').show();
				var errTitle = (typeof data.title !== 'undefined' && data.title !== null && data.title.length > 0) ? data.title : 'Error';
				var errText = (typeof data.message !== 'undefined' && data.message !== null && data.message.length > 0) ? data.message : 'Session failed to start.';
				parent.showPlainDialog1Btn( errTitle, errText, 'Close' );
			}
		}
	} ).fail( start_failed );
} );

function start_failed() {
	$('#btn_finish_deposition').hide();
	$('#btn_start_deposition').show();
	parent.showPlainDialog1Btn( 'Error', 'Session failed to start.', 'Close' );
}

$('#btn_finish_deposition').click( function() {
	var dialogButtons = [];
	dialogButtons.push( {label: 'Cancel', action : 'dismissDialog3Btn();', class : 'app_btn_finish_cancel'} );
	dialogButtons.push( {label: 'Finish', action : function(){window.top.finishDeposition();}, class : 'app_btn_finish'} );
	var confirmFinish = 'Are you sure you want to finish the session? It will be closed and all users should save their work before exiting.';
	parent.showDialog3Btn( 'Finish the Session?', confirmFinish, dialogButtons );
} );

window.top.finishDeposition = function _finishDeposition()  {
	window.top.dismissDialog3Btn();
	$.getJSON("depositions/finishDeposition", null, function(data, textStatus, xhr) {
		if (textStatus === "success" ) {
			if (data === null) {
				window.top.showPlainDialog1Btn( 'Error', 'Session failed to finish.', 'Close' );
			}
		}
	}).fail(function(xhr, textStatus, error) {
//		console.log( 'index::finishDeposition() -- failed: ' + textStatus + ', ' + error );
		window.top.showPlainDialog1Btn( 'Error', 'Session failed to finish.', 'Close' );
	});
	$('#btn_finish_deposition').hide();
	$('#depositions_header .deposition_passcode').hide();
	$('#depositions_header .deposition_passcodetitle').hide();
	
}

$('.deposition_passcode').click( function() {
	$.getJSON("depositions/getKey", null, function(data, textStatus, xhr) {
		if (textStatus === "success" ) {
			if (data !== null) {
				parent.showDialogDepositionPasscode( window.top.Datacore.s.started, data.EDDeposition.key,  $('.deposition_passcode').offset().left + $('.deposition_passcode').width() );
			}
		}
	});
} );

function convertDate( stringDateTime ) {
	var s = stringDateTime.split( /[^0-9]/ );
	var started = new Date( s[0], s[1]-1, s[2], s[3], s[4], s[5] );
	var clientTimezoneOffset = new Date().getTimezoneOffset();
	started.setMinutes(started.getMinutes() - clientTimezoneOffset);

	var result = ( '0' + ( started.getMonth() + 1 ) ).slice(-2) + '/' +
				 ( '0' + started.getDate() ).slice(-2) + '/' +
				 started.getFullYear() + ' ' +
				 started.getHours() + ':' +
				 ( '0' + started.getMinutes() ).slice(-2) + ':' +
				 ( '0' + started.getSeconds() ).slice(-2);
	return result;
}

window.top.resetredirect = function()
{
	if (window.top.Datacore.s.userType === 'G' || window.top.Datacore.s.userType === 'W' || window.top.Datacore.s.userType === 'WM' || window.top.Datacore.s.userType === 'R') {
		$('#ipad_container_frame', window.top.document).attr( 'src', '/logout?ul=4' );
	} else {
		$('#ipad_container_frame', window.top.document).attr( 'src', '/cases' );
	}
};

function TablesPanel( panel )
{
//	console.log( 'index::TablesPanel()' );
	var folders = null;
	var folderID = 0;
	var folder = null;
	var files = null;
	var selectedFiles = {};
	var inEmailSelect = false;
	var inCustomSort = null;
	if( !panel || typeof panel !== 'object' || !panel.jquery ) {
		return null;
	}
	var foldersList = $('#foldersList ul', panel);
	var filesList = $('#filesList ul', panel);
	var user = window.top.webApp.getS( 'user' );
	if( typeof user === 'undefined' || user === null || !(user instanceof window.top.DcUser) ) {
		return null;
	}
	var deposition = window.top.webApp.getS( 'deposition' );
	var bind = this;

	this.selectFolder = function( ID )
	{
//		console.log( 'index::TablesPanel.selectFolder()', ID );
		var _folderID = parseInt( ID );
		var _folder = window.top.Datacore.getFolder( _folderID );
		if( _folder !== null && _folder instanceof window.top.DcFolder ) {
			folderID = _folderID;
			folder = _folder;
		} else {
			window.top.Datacore.syncFolder( _folderID );
			return;
		}
		$('li', foldersList).removeClass( 'selected' );
		$('#folder_item_' + folderID).addClass( 'selected' );
		files = window.top.userSortFiles.sort( folder.getFiles() );
//		window.top.webApp.setS( 'selectedFolderID', folderID );
		window.top.webApp.setItem( 'selectedFolderID', folderID );
		buildFileList();
		scrollToSelectedFile();
	};

	this.sortFolders = function()
	{
//		console.log( 'index::TablesPanel.sortFolders()' );
		if( folders === null ) {
			if( deposition ) {
				folders = deposition.getFolders();
			}
		}
		folders = window.top.userSortFolders.sort( folders );
		buildFolderList();
	};

	this.sortFiles = function()
	{
//		console.log( 'index::TablesPanel.sortFiles()' );
		files = window.top.userSortFiles.sort( files );
		buildFileList();
	};

	this.startCustomSort = function( type )
	{
//		console.log( 'index::TablesPanel.startCustomSort() type = ' + type );
		if( inEmailSelect ) {
			window.top.tablesPanel.abortSelectFiles();
		}
		if( inCustomSort ) {
			this.endCustomSort();
			$('.doneButton', panel).trigger( 'abortCustomSort' );
		}
		var options = {items:'.sortable', containment:'parent', helper:'clone', tolerance: 'pointer'};
		switch( type ) {
			case 'Files':
				inCustomSort = filesList;
				break;
			case 'Folders':
				inCustomSort = foldersList;
				break;
			default:
				return;
		}
		inCustomSort.addClass( 'CustomSort' );
		inCustomSort.children().off();
		inCustomSort.sortable( options );
	};

	this.endCustomSort = function()
	{
//		console.log( 'index::TablesPanel.endCustomSort()' );
		if( inCustomSort === null ) {
			return;
		}
		inCustomSort.removeClass( 'CustomSort' );
		if( inCustomSort.hasClass( 'ui-sortable' ) ) {
			inCustomSort.sortable( 'destroy' );
		}
		if( inCustomSort === filesList ) {
			buildFileList();
		} else if( inCustomSort === foldersList ) {
			buildFolderList();
		}
		inCustomSort = null;
	};

	this.saveCustomSort = function( data )
	{
//		console.log( 'index::TablesPanel.saveCustomSort() data = ' + data );
		var sortElement;
		switch( data.sortObject ) {
			case 'Files':
				sortElement = filesList;
				break;
			case 'Folders':
				sortElement = foldersList;
				break;
			default:
				return;
		}
		if( !sortElement.hasClass( 'ui-sortable') ) {
			return;
		}
		var list = sortElement.sortable( 'toArray', {"attribute":"data-id"} );
		var sortPosMap = [];
		var listMap = {};
		var sortPositions = [];
		if( inCustomSort === filesList ) {
			for( var f in files ) {
				var _file = files[f];
				sortPosMap[f] = _file.sortPos;
				listMap[_file.ID] = _file;
			}
			for( var i in list ) {
				var _fileID = parseInt( list[i] );
				var _file = listMap[_fileID];
				var sortPos = sortPosMap[i];
//				console.log( _file.name, _file.ID, _file.sortPos, sortPos );
				_file.sortPos = sortPos;
				var customSort = {};
				customSort[_file.ID] = _file.sortPos;
				sortPositions.push( customSort );
			}
			files = window.top.userSortFiles.sort( files );
		} else if( inCustomSort === foldersList ) {
			for( var f in folders ) {
				var _folder = folders[f];
				sortPosMap[f] = _folder.sortPos;
				listMap[_folder.ID] = _folder;
			}
			for( var i in list ) {
				var _folderID = parseInt( list[i] );
				var _folder = listMap[_folderID];
				var sortPos = sortPosMap[i];
//				console.log( _folder.name, _folder.ID, _folder.sortPos, sortPos );
				_folder.sortPos = sortPos;
				var customSort = {};
				customSort[_folder.ID] = _folder.sortPos;
				sortPositions.push( customSort );
			}
			folders = window.top.userSortFolders.sort( folders );
		}

		var postData = {"sortObject":data.sortObject, "list":sortPositions, "sessionID":window.top.webApp.getS( 'deposition.ID' )};

		$.post( '/users/setCustomSort', postData, function( data, textStatus ) {
			if( textStatus === "success" ) {
				if( data !== null ) {
//					console.log( 'index::TablesPanel.saveCustomSort() -- success' );
					return;
				}
			}
//			console.log( 'index::TablesPanel.saveCustomSort() -- failure' );
		}, 'json' );
	};

	var buildFolderList = function()
	{
//		console.log( 'index::buildFolderList() ');
		var foldersListMinLength = 10;
		var folders = window.top.userSortFolders.sort( deposition.getFolders() );

		foldersList.empty();

		var userType = window.top.webApp.getS( 'user.userType' );
		var blockExhibits = !!window.top.webApp.getS( 'user.blockExhibits' );
		var activeFolderID = parseInt( window.top.webApp.getItem( 'selectedFolderID' ) );
		if( userType === 'WM' && blockExhibits ) {
			$('#sortFolders').hide();
		} else {
			$('#sortFolders').show();
			var currentUser = window.top.Datacore.getS( 'currentUser' );
			var userIsSpeaker = window.top.webApp.userIsSpeaker();
			var isDraggable = false;
			var elementHTML = '<li></li>';
			var folderLabelHTML = '<div class="folderLabel"></div>';
			var blockLabelHTML = '<p class="blockLabel">Blocked folder</p>';

			for( var f in folders ) {
				var folder = folders[f];
				if( typeof folder === 'undefined' || folder === null ) {
//					console.log( folder );
					continue;
				}
				var li = $( elementHTML );
				li.attr( 'id', 'folder_item_' + folder.ID );
				li.addClass( 'sortable' );
				if( folder.caseID !== null ) {
					li.addClass( 'caseFolder' );
				}
				li.attr( 'data-id', folder.ID );

				li.on( 'click', {folderID:folder.ID,dcFolder:folder}, function( event ) {
					var dcFolder = event.data.dcFolder;
					$('li', foldersList).removeClass( 'selected' );
					$('#folder_item_' + event.data.folderID).addClass( 'selected' );
					window.top.tablesPanel.selectFolder( event.data.folderID );
					if(dcFolder.caseID !== null) {
						//console.log("CaseExhibit::Click(true)");
						window.top.webApp.setS("caseExhibit", true);
					} else {
						//console.log("CaseExhibit::Click(false)");
						window.top.webApp.setS("caseExhibit", false);
					}
				} );
				var dragHandle = $( '<div class="dragBox"><div class="handle"></div></div>' );
				li.append( dragHandle );
				var folderLabel = $( folderLabelHTML );
				folderLabel.append( '<p class="folderName">' + folder.name + '</p>' );
				isDraggable = (userIsSpeaker && folder.class !== 'Exhibit');
				if( typeof currentUser !== 'undefined' && currentUser !== null && (currentUser.userType === 'W' || currentUser.userType === 'WM') && currentUser.blockExhibits ) {
					isDraggable = false;
					folderLabel.append( blockLabelHTML );
				}
				li.append( folderLabel );
				if( isDraggable ) {
					var data = {name: folder.name, fileID: File.ID, dcObjType: 'folder', dcObjID: folder.ID};
					var dataTransfer = {
						impersonated: true,
						dropEffect: "copy",
						effectAllowed: "copyMove",
						helper: $('<div id="file_drag_helper">' + File.name + '</div>'),
						offsetX: 10,
						offsetY: 25,
						myDataStore: {},
						setData: function(format, data) { this.myDataStore[format] = data; },
						getData: function(format) { return this.myDataStore[format]; },
						setDragImage: function(element, x, y) {
							this.offsetX = x;
							this.offsetY = y;
						}
					};
					//li.draggable();
					li.attr( 'draggable', 'true' );
					li.on( 'dragstart', {type:'folder',folderID:folder.ID}, function( event ) { on_drag_start( event.originalEvent, event.data.type, event.data.folderID ); } );
					li.on( 'drag', function( event ) { on_drag( event.originalEvent ); } );
					li.on( 'dragend', function( event ) { on_drag_end( event.originalEvent ); } );
					li.on( 'touchstart', data, function( event ) { touchDragHandler( event, { dataTransfer : dataTransfer } ); } );
					li.on( 'touchmove', data, function( event ) { touchDragHandler( event, { dataTransfer : dataTransfer } ); } );
					li.on( 'touchend', data, function( event ) { touchDragHandler( event, { dataTransfer : dataTransfer } ); } );
					li.on( 'touchcancel', data, function( event ) { touchDragHandler( event, { dataTransfer : dataTransfer } ); } );
				}
				foldersList.append( li );
			}

			$('.foldersPanel .legend').html( '<img src="/asset/images/f1.png"> Folders (' + folders.length + ')' );

			if( foldersList.length < foldersListMinLength ) {
				for( var i = (foldersListMinLength - folders.length); i > 0; --i ) {
					foldersList.append( elementHTML );
				}
			}
			var activeFolder = null;
			if( !isNaN( activeFolderID ) ) {
				activeFolder = window.top.Datacore.getFolder( activeFolderID );
			}
			if( typeof activeFolder !== 'undefined' && activeFolder !== null ) {
				bind.selectFolder( activeFolderID );
			} else {
				var firstFolderID = folders[0].ID;
				bind.selectFolder( firstFolderID );
			}
		}

		if( isset( activeFolderID ) && isset( $( '#folder_item_' + activeFolderID ) ) ) {
			var position = $('#folder_item_' + activeFolderID).position();
			if(isset(position)) {
				$('#foldersList ul').scrollTop( position.top );
			}
		}
	};

	var buildFileList = function()
	{
		var searchValues = window.top.searchValues;
		if( searchValues !== null && typeof searchValues !== 'undefined' ) {
			window.top.contentSearch.buildFileList();
			return;
		}
//		console.log( 'index::TablesPanel.buildFileList()' );
		var filesListMinLength = 10;
		window.top.tablesPanel.abortSelectFiles( true );
		filesList.empty();
		var userType = window.top.webApp.getS( 'user.userType' );
		var blockExhibits = !!window.top.webApp.getS( 'user.blockExhibits' );
		var legendText = (userType === 'W') ? 'Exhibits' : 'Documents';
		$('.filesPanel .legend', panel).text( legendText + ' (0)' );

		filesList.addClass( 'loading' );

		if( folder.numFiles !== folder.getFileIDs().length ) {
			window.top.Datacore.syncFolder( folderID, function() { bind.selectFolder( folderID ); } );
			return;
		}

		if( files.length >= 1 ) {
			$('#sendFolder, #saveFolder').removeClass( 'inactive' );
		} else {
			$('#sendFolder, #saveFolder').addClass( 'inactive' );
		}

		if( !blockExhibits ) {
			var userIsSpeaker = window.top.webApp.userIsSpeaker();
			var isDraggable = false;
			var isTrialBinder = (window.top.webApp.getS( "deposition.class" ) === "Trial Binder");

			var clientTimezoneOffset = new Date().getTimezoneOffset();
			for( var f in files ) {
				var File = files[f];
				isDraggable = (userIsSpeaker && !isTrialBinder && (folder.class !== 'Exhibit' || (folder.class === 'Exhibit' && folder.caseID)));

				// Custom second line for members and the folder is not exhibit.
				var secondLine;
				if( (user.userType === 'M' || user.userType === 'WM') && folder.class !== 'Exhibit' ) {
					var exhibitHistoryList = window.top.Datacore.getExhibitsBySourceFileID( File.ID );
					if( exhibitHistoryList.length === 0 ) {
						secondLine = '<p class="name">&nbsp;</p>';
					} else {
						exhibitHistoryList = exhibitHistoryList.reverse();
						var first = true;
						secondLine = '<p class="exhibitHistory">';
						for( var i in exhibitHistoryList ) {
							var exHistory = exhibitHistoryList[i];
							var srcFile = window.top.Datacore.getFile( exHistory.exhibitFileID );
							if( typeof srcFile !== 'undefined' && srcFile !== null ) {
								if( first ) {
									first = false;
									secondLine += 'As Exhibit: ';
								} else {
									secondLine += ', ';
								}
								secondLine += srcFile.name;
							}
						}
						secondLine += '&nbsp;</p>';
					}
				} else if( user.userType === 'G' ) {
					secondLine = '<p class="name">&nbsp;</p>';
				} else {
					var exhibitHistory = window.top.Datacore.getExhibitByExhibitFileID( File.ID );
					var localTime;
					var name;
					if( exhibitHistory === null ) {
						secondLine = '<p class="name">&nbsp;</p>';
					} else {
						localTime = new Date( exhibitHistory.introducedDate );
						name = exhibitHistory.introducedBy;
						localTime.setMinutes( localTime.getMinutes() - clientTimezoneOffset );
						var hours = localTime.getHours();
						var ampm = "AM";
						if( hours >= 12 ) {
							hours -= 12;
							ampm = "PM";
						}
						if( hours === 0 ) {
							hours = 12;
						}
						var minutes = ('0' + localTime.getMinutes()).slice( -2 );
						var seconds = ('0' + localTime.getSeconds()).slice( -2 );

						secondLine = '<p class="exhibitHistory">'+((localTime.getMonth()+1) + '/' + localTime.getDate() + '/' + localTime.getFullYear()) + ' ' + hours + ':' + minutes + ':' + seconds + ' ' + ampm + ' By: '+ name + '</p>';
					}
				}
				//var pdfviewerURL = 'https://pdf-staging2.edepoze.com/webapp/' + File.ID + '/image/0/zoom4/img.jpg';
				//console.log( 'pdfViewerURL =============' + <?=$pdfViewerURL;?> );
				//console.log( 'Window Data Core ==========' + window.top.Datacore.s.sKey );

				var file_info = $( '<div class="file_info"><p class="fileName">' + File.name + '</p>' + secondLine + '</div>' );

				if( isDraggable ) {
					var data = {name: File.name, fileID: File.ID, dcObjType: 'file', dcObjID: File.ID};
					var dataTransfer = {
						impersonated: true,
						dropEffect: "copy",
						effectAllowed: "copyMove",
						helper: $('<div id="file_drag_helper">' + File.name + '</div>'),
						offsetX: 10,
						offsetY: 25,
						myDataStore: {},
						setData: function(format, data) { this.myDataStore[format] = data; },
						getData: function(format) { return this.myDataStore[format]; },
						setDragImage: function(element, x, y) {
							this.offsetX = x;
							this.offsetY = y;
						}
					};
					file_info.attr( 'draggable', 'true' );
					file_info.on( 'dragstart', {fileID:File.ID}, function( event ) { on_drag_start( event.originalEvent, 'file', event.data.fileID ); } );
					file_info.on( 'drag', function( event ) { on_drag( event.originalEvent ); } );
					file_info.on( 'dragend', function( event ) { on_drag_end( event.originalEvent ); } );
					file_info.on( 'touchstart', data, function( event ) { touchDragHandler( event, { dataTransfer : dataTransfer } ); } );
					file_info.on( 'touchmove', data, function( event ) { touchDragHandler( event, { dataTransfer : dataTransfer } ); } );
					file_info.on( 'touchend', data, function( event ) { touchDragHandler( event, { dataTransfer : dataTransfer } ); } );
					file_info.on( 'touchcancel', data, function( event ) { touchDragHandler( event, { dataTransfer : dataTransfer } ); } );
				}
				var li = $( '<li id="file_item_'+File.ID+'" class="sortable"></li>' );
				li.attr( 'data-id', File.ID );
				li.attr( 'data-mimetype', File.mimeType );
				li.on( 'click', {file:File}, function( event ) {
					window.top.webApp.setItem( 'viewFile', JSON.stringify( event.data.file ) );
					view_file( event.data.file.ID, event.data.file.mimeType );
				} );
				var dragHandle = $( '<div class="dragBox"><div class="handle"></div></div>' );
				var selectDiv = $( '<div class="fileSelect"><div class="checkBtn"></div></div>' );
				var dateInfo = $('<div class="fileCreated">' + ((File.created.getMonth() + 1) + '/' + File.created.getDate() + '/' + File.created.getFullYear()) + '</div>');
				li.append( dragHandle );
				li.append( selectDiv );
				li.append( file_info );
				li.append( dateInfo );
				filesList.append( li );
			}

			var listLength = $('li', filesList).length;
			$('.filesPanel .legend', panel).html('<img src="/asset/images/f2.png">' + legendText + ' (' + listLength + ')' );



			if( listLength < filesListMinLength ) {
				for( var i = (filesListMinLength - listLength); i > 0; --i ) {
					filesList.append( '<li></li>' );
				}
			}
		}
		filesList.removeClass( 'loading' );
		var emailBtn = $('.folderActions .emailBtn');
		emailBtn.off();
		var okayBtn = $('.folderActions .okayBtn');
		okayBtn.off();
		okayBtn.on( 'click', window.top.tablesPanel.sendFiles );
		var cancelBtn = $('.folderActions .cancelBtn');
		cancelBtn.off();
		cancelBtn.on( 'click', function() { window.top.tablesPanel.abortSelectFiles(); } );
		var downloadBtn = $('.folderActions .downloadBtn');
		downloadBtn.off();
		if( files.length > 0 && userType !== 'W' ) {
			emailBtn.removeClass('disabled');
			emailBtn.on( 'click', {folderID:folderID}, function( event ) { window.top.tablesPanel.emailSelectFiles( event.data.folderID ); } );
			downloadBtn.removeClass('disabled');
			downloadBtn.on( 'click', window.top.tablesPanel.downloadFolder );
		} else {
			emailBtn.addClass('disabled');
			downloadBtn.addClass('disabled');
		}
		if( userType === 'W' || userType === 'WM' ) {
			emailBtn.off().hide();
			if ( blockExhibits ) {
				downloadBtn.off().hide();
				$('#witnessBlock').show();
			} else {
				downloadBtn.off().show();
				$('#witnessBlock').hide();
			}
		}
		var sendPortalInviteBtn = $('.folderActions .sendPortalInviteBtn');
		sendPortalInviteBtn.off();
		if( user.typeID === 'RPT' ) {
			sendPortalInviteBtn.show();
			if ( folder.class === 'Exhibit' ) {
				sendPortalInviteBtn.removeClass('disabled');
				sendPortalInviteBtn.on( 'click', bind.showPortalInvite );
			} else {
				sendPortalInviteBtn.addClass('disabled');
			}
		} else {
			sendPortalInviteBtn.hide();
		}

		if( userType === 'W' ) {
			$('#sortFiles').hide();
		} else {
			$('#sortFiles').show();
		}
	};

	var scrollToSelectedFile = function() {
		var selectedFileID = window.top.webApp.getItem( 'selectedFileID' );
		if( selectedFileID && isset( $('#file_item_' + selectedFileID).position() ) ){
			var filesTop = $('#file_item_' + selectedFileID).position().top;
			if( isset( selectedFileID ) ) {
				$('#filesList ul li').removeClass('selected');
				$('#filesList ul #file_item_' + selectedFileID).addClass('selected');
				if( filesTop > 0 ) {
					$('#filesList ul').scrollTop( filesTop );
				}
			}
		}
		else {
			window.top.webApp.setItem( 'selectedFileID', null );
		}
	}

	this.abortSelectFiles = function( skipBuild )
	{
//		console.log( 'index::TablesPanel.abortSelectFiles()' );
		skipBuild = !!skipBuild;
		inEmailSelect = false;
		$('.folderActions .actionBtns').hide();
		filesList.removeClass( 'selectEdit' );
		if( !skipBuild ) {
			buildFileList();
		}
	};

	this.emailSelectFiles = function()
	{
//		console.log( 'index::TablesPanel.emailSelectFiles() folderID = ' + folderID );
		if( inCustomSort ) {
			$('.doneButton').trigger( 'abortCustomSort' );
			bind.endCustomSort();
		}
		inEmailSelect = true;
		filesList.addClass( 'selectEdit' );
		window.top.tablesPanel.addSelectFileClickToRows();
		selectedFiles = {};
		$('#filesList ul li.sortable').each(function() {
			var fileID = $(this).attr('data-id');
			selectedFiles[fileID] = parseInt(fileID);
		});
		$('.fileSelect', filesList).addClass( 'selected' );
		$('.folderActions .actionBtns').show();
	};

	var selectFile = function( event )
	{
		var target = $( event.currentTarget );
		var fileID = parseInt( target.attr( 'data-id' ) );
		if( isNaN( fileID ) ) {
			return;
		}
		var hasFile = false;
		for( var f in files ) {
			if( files[f].ID === fileID ) {
				hasFile = true;
				break;
			}
		}
		if( !hasFile ) {
			return;
		}
		if( typeof selectedFiles[fileID] !== 'undefined' ) {
			delete selectedFiles[fileID];
			$('.fileSelect', target).removeClass( 'selected' );
		} else {
			selectedFiles[fileID] = fileID;
			$('.fileSelect', target).addClass( 'selected' );
		}
	};

	this.downloadFolder = function()
	{
		if( !folderID ) {
			window.top.showTooltip( 'Please select a folder.', true );
			return;
		}
		if( files.length <= 0 ) {
			//window.top.showDialog1Btn( 'Error', 'Unable to download an empty folder', 'OK' );
			return;
		}
		$('iframe.folder-download').remove();
		var iframe = $('<iframe class="folder-download" style="display:none;width:0;height:0;"></iframe>');
		iframe.attr( 'src', '/depositions/saveFolder?folderID=' + folderID + '&folderName=' + folder.name );
		$('body').append(  iframe );
	};

	this.sendFiles = function()
	{
		if( !folderID ) {
			window.top.showTooltip( 'Please select a folder.', true );
			return;
		}
		if( files.length <= 0 ) {
			//window.top.showDialog1Btn( 'Error', 'Unable to email an empty folder', 'OK' );
			return;
		}
		var selectedFileIDs = [];
		for( var fID in selectedFiles ) {
			selectedFileIDs.push( selectedFiles[fID] );
		};
		window.top.sendDialog.setFolderID( folderID, selectedFileIDs );
		window.top.sendDialog.show();
		window.top.tablesPanel.abortSelectFiles();
	};

	this.showPortalInvite = function(event)	{
		window.top.setDialogTextInputWithDescriptionCloseAction( sendPortalInvite );
		window.top.showDialogTextInputWithDescription("Send Exhibits Invitation", "Enter the email address of a proofreader or production department to give them access to download the exhibits.", "", "Send");
	};

	var sendPortalInvite = function( emailAddress )
	{
		$.post( '/depositions/sendPortalInvite', {"sessionID":window.top.Datacore.s.depositionID, "emailAddress":emailAddress}, function( data, textStatus ) {
			if( textStatus === "success" && data !== null && data.EDDeposition.sent) {
				window.top.showPlainDialog1Btn( 'Email Sent', 'The invitation email has been sent.', 'Close' );
			} else {
				window.top.showPlainDialog1Btn( 'Email Failed', 'The invitation email failed to send.', 'Close' );
			}
		}, 'json' );
	};

	this.getInCustomSort = function()
	{
		return inCustomSort;
	};

	this.addSelectFileClickToRows =  function()
	{
		var items = $('li', filesList);
		items.off();
		$('.file_info', items).removeAttr( 'draggable' );
		items.on( 'click', selectFile );
	};
}
<?php $this->Html->scriptEnd(); ?>
</script>
