<?php $this->Html->script('annotate', ['inline' => FALSE]); ?>

<div id="spinner"></div>
<div id="outerContainer" class="loadingInProgress">
	<div id="sidebarContainer">
		<div id="toolbarSidebar">
			<div class="splitToolbarButton toggled">
				<button id="viewThumbnail" class="toolbarButton group toggled" title="Show Thumbnails" tabindex="2" data-l10n-id="thumbs" style="display:none;">
					<span data-l10n-id="thumbs_label">Thumbnails</span>
				</button>
				<button id="viewOutline" class="toolbarButton group" title="Show Document Outline" tabindex="3" data-l10n-id="outline" disabled="" style="display:none;">
					<span data-l10n-id="outline_label">Document Outline</span>
				</button>
			</div>
		</div>
		<div id="sidebarContent">
			<div id="thumbnailView"></div>
			<div id="outlineView" class="hidden"></div>
		</div>
	</div>  <!-- sidebarContainer -->

	<div id="mainContainer">
		<div class="findbar hidden doorHanger hiddenSmallView" id="findbar">
			<label for="findInput" class="toolbarLabel" data-l10n-id="find_label">Find:</label>
			<input id="findInput" class="toolbarField" tabindex="41">
			<div class="splitToolbarButton">
				<button class="toolbarButton findPrevious" title="" id="findPrevious" tabindex="42" data-l10n-id="find_previous">
					<span data-l10n-id="find_previous_label">Previous</span>
				</button>
				<div class="splitToolbarButtonSeparator"></div>
				<button class="toolbarButton findNext" title="" id="findNext" tabindex="43" data-l10n-id="find_next">
					<span data-l10n-id="find_next_label">Next</span>
				</button>
			</div>
			<input type="checkbox" id="findHighlightAll" class="toolbarField">
			<label for="findHighlightAll" class="toolbarLabel" tabindex="44" data-l10n-id="find_highlight">Highlight all</label>
			<input type="checkbox" id="findMatchCase" class="toolbarField">
			<label for="findMatchCase" class="toolbarLabel" tabindex="45" data-l10n-id="find_match_case_label">Match case</label>
			<span id="findMsg" class="toolbarLabel"></span>
		</div>  <!-- findbar -->

		<div id="secondaryToolbar" class="secondaryToolbar hidden doorHangerRight">
			<div id="secondaryToolbarButtonContainer">
			</div>
		</div>  <!-- secondaryToolbar -->

		<div class="toolbar">
			<div id="toolbarContainer">
				<div id="toolbarViewer">
					<div id="toolbarViewerLeft">
						<button id="sidebarToggle" class="toolbarButton" title="Toggle Sidebar" tabindex="4" data-l10n-id="toggle_sidebar">
							<span data-l10n-id="toggle_sidebar_label">Toggle Sidebar</span>
						</button>
						<div class="toolbarButtonSpacer"></div>
						<button id="viewFind" class="toolbarButton group hiddenSmallView" title="Find in Document" tabindex="5" data-l10n-id="findbar">
							<span data-l10n-id="findbar_label">Find</span>
						</button>
						<div class="splitToolbarButton">
							<button class="toolbarButton pageUp" title="Previous Page" id="previous" tabindex="6" data-l10n-id="previous">
								<span data-l10n-id="previous_label">Previous</span>
							</button>
							<div class="splitToolbarButtonSeparator"></div>
							<button class="toolbarButton pageDown" title="Next Page" id="next" tabindex="7" data-l10n-id="next">
								<span data-l10n-id="next_label">Next</span>
							</button>
						</div>
						<label id="pageNumberLabel" class="toolbarLabel" for="pageNumber" data-l10n-id="page_label">Page: </label>
						<input type="number" id="pageNumber" class="toolbarField pageNumber" value="1" size="4" min="1" tabindex="8">
						</input>
						<span id="numPages" class="toolbarLabel"></span>
					</div>
					<div id="toolbarViewerRight">
						<button id="firstPage" class="toolbarButton firstPage" title="Go to First Page" tabindex="22" data-l10n-id="first_page">
							<span data-l10n-id="first_page_label">Go to First Page</span>
						</button>
						<button id="lastPage" class="toolbarButton lastPage" title="Go to Last Page" tabindex="23" data-l10n-id="last_page">
							<span data-l10n-id="last_page_label">Go to Last Page</span>
						</button>
						<div class="verticalToolbarSeparator hiddenSmallView"></div>
						<button id="pageRotateCw" class="toolbarButton rotateCw" title="Rotate Clockwise" tabindex="24" data-l10n-id="page_rotate_cw">
							<span data-l10n-id="page_rotate_cw_label">Rotate Clockwise</span>
						</button>
						<button id="pageRotateCcw" class="toolbarButton rotateCcw" title="Rotate Counterclockwise" tabindex="25" data-l10n-id="page_rotate_ccw">
							<span data-l10n-id="page_rotate_ccw_label">Rotate Counterclockwise</span>
						</button>
					</div>
					<div class="outerCenter">
						<div class="innerCenter" id="toolbarViewerMiddle">
							<div class="splitToolbarButton">
								<button id="zoomOut" class="toolbarButton zoomOut" title="Zoom Out" tabindex="9" data-l10n-id="zoom_out">
									<span data-l10n-id="zoom_out_label">Zoom Out</span>
								</button>
								<div class="splitToolbarButtonSeparator"></div>
								<button id="zoomIn" class="toolbarButton zoomIn" title="Zoom In" tabindex="10" data-l10n-id="zoom_in">
									<span data-l10n-id="zoom_in_label">Zoom In</span>
								</button>
							</div>
							<span id="scaleSelectContainer" class="dropdownToolbarButton">
								<select id="scaleSelect" title="Zoom" tabindex="11" data-l10n-id="zoom">
									<!-- option id="pageAutoOption" value="auto" selected="selected" data-l10n-id="page_scale_auto" style="display:none;">Automatic Zoom</option -->
									<!-- option id="pageActualOption" value="page-actual" data-l10n-id="page_scale_actual" style="display:none;">Actual Size</option -->
									<option id="pageFitOption" value="page-fit" data-l10n-id="page_scale_fit">Fit Page</option>
									<option id="pageWidthOption" value="page-width" data-l10n-id="page_scale_width">Full Width</option>
									<!-- option id="customScaleOption" value="custom" style="display:none;"></option -->
									<option value="0.5" style="display:none;">50%</option>
									<option value="0.75" style="display:none;">75%</option>
									<option value="1">100%</option>
									<option value="1.25">125%</option>
									<option value="1.5">150%</option>
									<option value="1.75" style="display:none;">175%</option>
									<option value="2">200%</option>
									<option value="2.25" style="display:none;">225%</option>
									<option value="2.5" style="display:none;">250%</option>
									<option value="2.75" style="display:none;">275%</option>
									<option value="3">300%</option>
									<option value="3.25" style="display:none;">325%</option>
									<option value="3.5" style="display:none;">350%</option>
									<option value="3.75" style="display:none;">375%</option>
									<option value="4">400%</option>
								</select>
							</span>
						</div>
					</div>
				</div>
				<div id="loadingBar">
					<div class="progress">
						<div class="glimmer"></div>
					</div>
				</div>
			</div>
		</div>

		<!-- Attempting to remove this context menu and applying events directly to buttons caused Rotate to vary from 90 degrees to 180 -->
		<menu type="context" id="viewerContextMenu">
			<menuitem id="contextPageRotateCw" label="Rotate Clockwise" data-l10n-id="page_rotate_cw"></menuitem>
			<menuitem id="contextPageRotateCcw" label="Rotate Counter-Clockwise" data-l10n-id="page_rotate_ccw"></menuitem>
		</menu>

		<div id="viewerContainer" class="loading" tabindex="0">
			<div id="viewer" style="padding-right:19px; border-right:19px solid transparent;"></div>
		</div>

		<div id="errorWrapper" hidden='true'>
			<div id="errorMessageLeft">
				<span id="errorMessage"></span>
				<button id="errorShowMore" data-l10n-id="error_more_info">
					More Information
				</button>
				<button id="errorShowLess" data-l10n-id="error_less_info" hidden='true'>
					Less Information
				</button>
			</div>
			<div id="errorMessageRight">
				<button id="errorClose" data-l10n-id="error_close">
					Close
				</button>
			</div>
			<div class="clearBoth"></div>
			<textarea id="errorMoreInfo" hidden='true' readonly="readonly"></textarea>
		</div>
	</div> <!-- mainContainer -->
</div> <!-- outerContainer -->

<div id="printContainer"></div>
<img id="dragIcon" src="/img/ipad/icons/selected-annotation.png" style="display: none;" width="14" height="14" />

<script type="text/javascript">
//window.document.domain = "edepoze.com";
//window.document.domain = "armyargentina.com";
window.document.domain = "<?=Configure::read('SiteConfig')['domain'];?>";


	var lastPage = null;
	var lastScrollPos = 0;
	var animating = false;

	$(document).ready(function () {
		PDFView.initialize();

		PDFJS.disableFontFace = true;
		var prefs = new Preferences();
		prefs.initializedPromise.then(function () {
			prefs.set('showPreviousViewOnLoad', false);
		});

		// Special debugging flags in the hash section of the URL.
		var hash = document.location.hash.substring(1);
		var hashParams = PDFView.parseQueryString(hash);

		if ('disableWorker' in hashParams) {
			PDFJS.disableWorker = (hashParams['disableWorker'] === 'true');
		}
		if ('disableRange' in hashParams) {
			PDFJS.disableRange = (hashParams['disableRange'] === 'true');
		}
		if ('disableAutoFetch' in hashParams) {
			PDFJS.disableAutoFetch = (hashParams['disableAutoFetch'] === 'true');
		}
		if ('disableFontFace' in hashParams) {
			PDFJS.disableFontFace = (hashParams['disableFontFace'] === 'true');
		}
		if ('disableHistory' in hashParams) {
			PDFJS.disableHistory = (hashParams['disableHistory'] === 'true');
		}

		var locale = PDFJS.locale || navigator.language;
		if( 'locale' in hashParams ) {
		  locale = hashParams['locale'];
		}
		mozL10n.setLanguage( locale );

		if ('textLayer' in hashParams) {
			switch (hashParams['textLayer']) {
				case 'off':
					PDFJS.disableTextLayer = true;
					break;
				case 'visible':
				case 'shadow':
				case 'hover':
					var viewer = document.getElementById('viewer');
					viewer.classList.add('textLayer-' + hashParams['textLayer']);
					break;
			}
		}

		if ('pdfBug' in hashParams) {
			PDFJS.pdfBug = true;
			var pdfBug = hashParams['pdfBug'];
			var enabled = pdfBug.split(',');
			PDFBug.enable(enabled);
			PDFBug.init();
		}

		if (PDFView.supportsIntegratedFind) {
			document.getElementById('viewFind').classList.add('hidden');
		}

		// Listen for warnings to trigger the fallback UI.  Errors should be caught and call PDFView.error() so we don't need to listen for those.
//		PDFJS.LogManager.addLogger( {warn : function() { PDFView.fallback(); }} );

		// Suppress context menus for some controls
		document.getElementById('scaleSelect').oncontextmenu = noContextMenuHandler;

		var mainContainer = document.getElementById('mainContainer');
		var outerContainer = document.getElementById('outerContainer');
		mainContainer.addEventListener('transitionend', function (e) {
			if (e.target == mainContainer) {
				var event = document.createEvent('UIEvents');
				event.initUIEvent('resize', false, false, window, 0);
				window.dispatchEvent(event);
				outerContainer.classList.remove('sidebarMoving');
			}
		}, true);

		document.getElementById('sidebarToggle').addEventListener('click', function () {
			this.classList.toggle('toggled');
			outerContainer.classList.add('sidebarMoving');
			outerContainer.classList.toggle('sidebarOpen');
			PDFView.sidebarOpen = outerContainer.classList.contains('sidebarOpen');
			PDFView.renderHighestPriority();
		});

		// document.getElementById('viewThumbnail').addEventListener( 'click', function() { PDFView.switchSidebarView('thumbs'); } );
		document.getElementById('previous').addEventListener('click', function () {
			PDFView.forceToPageTop = true;
			PDFView.page--;
		});
		document.getElementById('next').addEventListener('click', function () {
			PDFView.forceToPageTop = true;
			PDFView.page++;
		});
		document.getElementById('pageNumber').addEventListener('click', function () {
			this.select();
		});
		document.getElementById('pageNumber').addEventListener('change', function () {
			// Handle the user inputting a floating point number.
			PDFView.page = (this.value | 0);
			if (this.value !== (this.value | 0).toString()) {
				this.value = PDFView.page;
			}
		});

		document.getElementById('zoomIn').addEventListener('click', function () {
			PDFView.zoomIn();
		});
		document.getElementById('zoomOut').addEventListener('click', function () {
			PDFView.zoomOut();
		});
		document.getElementById('scaleSelect').addEventListener('change', function () {
			PDFView.setPageScale(this.value);
		});

		document.getElementById('firstPage').addEventListener('click', SecondaryToolbar.firstPageClick.bind(SecondaryToolbar));
		document.getElementById('lastPage').addEventListener('click', SecondaryToolbar.lastPageClick.bind(SecondaryToolbar));
		document.getElementById('pageRotateCw').addEventListener('click', SecondaryToolbar.pageRotateCwClick.bind(SecondaryToolbar));
		document.getElementById('pageRotateCcw').addEventListener('click', SecondaryToolbar.pageRotateCcwClick.bind(SecondaryToolbar));

		var presentationClient = 0;
		var pData = window.top.webApp.getS( 'joinPresentationData' );
		if( typeof pData !== 'undefined' && pData !== null && window.top.webApp.userIsSpeaker() === true ) {
			presentationClient = 1;
		}

		// Add current date to end of URL because IE will check if the file has been updated and returned a cached copy.
		PDFView.open('https://<?= $_SERVER['HTTP_HOST'] ?>/depositions/file?fileID=<?= $fileID ?>&presentationClient=' + presentationClient + '&x=' + Date.now(), 0);

		Annotate.init();
		$(window).bind('pagerender', Annotate.canvasSetup);
		$(window).bind('pagechange', Annotate.pageChange);

		$('#viewerContainer').scroll(function (event) {
			if (!Annotate.presentationHost && !Annotate.iAmPresentor) {
				return;
			}
			if (animating) {
				return;
			}
			// Do a sticky scroll.
			var scrollPos = event.target.scrollTop;
			var visible = PDFView.getVisiblePages();
			var visiblePages = visible.views;
			if (visiblePages.length > 1) {
				var page = null;
				var newScroll = 0;
				if (lastScrollPos < scrollPos) {
					// Scrolling down.
					var index = visiblePages[0].id < visiblePages[1].id ? 1 : 0;
					page = visiblePages[index];
					if (page.percent >= 11) {
						animating = true;
						newScroll = page.view.el.offsetTop + page.view.el.clientTop;
						var step = (newScroll - $('#viewerContainer').scrollTop()) / 10;
						var intervalID = setInterval(function () {
							$('#viewerContainer').scrollTop($('#viewerContainer').scrollTop() + step);
							visible = PDFView.getVisiblePages();
							if (visible.views.length === 1) {
								clearInterval(intervalID);
								animating = false;
								lastScrollPos = $('#viewerContainer').scrollTop();
							}
						}, 50);
					}
				} else if (lastScrollPos > scrollPos) {
					// going up.
					var index = visiblePages[0].id < visiblePages[1].id ? 0 : 1;
					page = visiblePages[index];
					var scale = PDFView.currentScale;
					if (page.percent >= 11) {
						animating = true;
						newScroll = page.view.el.offsetTop + page.view.el.clientTop + page.view.canvas.clientHeight - event.target.clientHeight;
						var step = (newScroll - $('#viewerContainer').scrollTop()) / 10;
						var intervalID = setInterval(function () {
							$('#viewerContainer').scrollTop($('#viewerContainer').scrollTop() + step);
							visible = PDFView.getVisiblePages();
							if (visible.views.length === 1) {
								clearInterval(intervalID);
								animating = false;
								lastScrollPos = $('#viewerContainer').scrollTop();
							}
							if (scale !== PDFView.currentScale) {
								page.view.scrollIntoView();
								if (typeof page.view.canvas.clientHeight === 'undefined') {
									console.log('page.view.canvas.clientHeight');
								}
								if (typeof event.target.clientHeight === 'undefined') {
									console.log('event.target.clientHeight');
								}
								newScroll = page.view.el.offsetTop + page.view.el.clientTop + page.view.canvas.clientHeight - event.target.clientHeight;
								clearInterval(intervalID);
								animating = false;
								$('#viewerContainer').scrollTop(newScroll);
								lastScrollPos = newScroll;
							}
						}, 50);
//						$('#viewerContainer').animate({scrollTop: newScroll}, 500, function() {
//							animating = false;
//							lastScrollPos = newScroll;
//						});
					}
				}
			}
			lastScrollPos = scrollPos;
		});
	});

	function showSpinner() {
		var div = document.getElementById('outerContainer');
		var width = div.offsetWidth - SCROLLBAR_PADDING;
		$('#spinner').show();
		$('#spinner').width(width);
		$('#spinner').css('top', window.pageYOffset + 'px');
	}

	function hideSpinner() {
		$('#spinner').css('top', '0px');
		$('#spinner').hide();
	}

function pdfViewerReady()
{
	console.log( '/depositions/pdfviewer; pdfViewerReady' );

	window.onbeforeunload = function()
	{
		delete window.top.Annotate;
		delete window.top.contentSearch;
	};

}
window.top.webApp.onReady( pdfViewerReady );
</script>
