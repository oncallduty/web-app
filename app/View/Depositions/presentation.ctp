<?php $this->Html->css( 'depositions', NULL, ['inline'=>FALSE] ); ?>
<?php $this->Html->css( "brands/{$brand}/branding.css", NULL, ['inline'=>FALSE] ); ?>
<div id="file_contents">
	<div id="editor_contents">
		<div id="doc_viewer">
			<iframe id="doc_viewer_frame" name="doc_viewer_frame" src=""></iframe>
		</div>
	</div>
	<div id="depositions_footer">
		<div id="powered_by_logo_footer" ></div>
	</div>
</div>

<script type="text/javascript">
<?php $this->Html->scriptStart( ['inline'=>FALSE] ); ?>
//window.document.domain = "edepoze.com";
//window.document.domain = "armyargentina.com";
window.document.domain = "<?=Configure::read('SiteConfig')['domain'];?>";

<?php if( isset( $datacore ) ): ?>
window.top.Datacore.s.depositionID = parseInt( <?=$depositionID;?> );
<?php foreach ($datacore as $propname => $propval): ?>
window.top.Datacore.s['<?= $propname ?>'] = <?=json_encode( $propval );?>;
<?php endforeach; ?>
<?php endif; ?>
<?php if( isset( $deposition ) ): ?>
window.top.Datacore.addDeposition( <?=json_encode( $deposition );?> );
window.top.webApp.setS( 'deposition', window.top.Datacore.getDeposition( <?=$depositionID;?> ) );
<?php endif; ?>

var GROWTH_LIMIT = 1000;
var originalWidth = parseInt( $('body').css( 'width').slice( 0, -2 ) );

$(window).resize( function() {
	var newWidth = parseInt( $('body').css( 'width').slice( 0, -2 ) );
	if (originalWidth < GROWTH_LIMIT && newWidth >= GROWTH_LIMIT)
		frameGrown();
	if (originalWidth > GROWTH_LIMIT && newWidth <= GROWTH_LIMIT)
		frameShrunk();
	originalWidth = newWidth;
} );
function frameShrunk() {
}
function frameGrown() {
}
function showFileViewer( sessionID ) {
	var fileData = window.top.Datacore.getFile( sessionID );
	if( typeof fileData === 'undefined' || fileData === null ) {
		var _file = window.top.webApp.getItem( 'viewFile' );
		if( typeof _file !== 'undefined' && _file !== null ) {
			fileData = JSON.parse( _file );
			window.top.Datacore.addFile( fileData );
		}
	}
	if( typeof fileData !== 'undefined' && fileData !== null ) {
		window.top.webApp.setS( 'viewerFile', fileData );
		window.top.webApp.setS( 'viewerFilename', fileData.name );
		window.top.navBar.setTitle( fileData.name );
		window.top.navBar.showTitle();
		window.top.navBar.updateDefaults();
		$('#saveFile').parent().attr( 'download', fileData.name );
	}
	var presentationPage = "presentation/";
	if(window.top.webApp.userIsSpeaker() === true) {
		presentationPage = "presenter/";
	}
	window.top.showLoadingOverlay( 'Loading...' );
	var fileID = 0;
	var pData = window.top.webApp.getS("joinPresentationData");
	if( pData && typeof pData.orientation === "string" ) {
		window.top.webApp.setAppOrientation( {"orientation": pData.orientation} );
	}
	if(window.top.webApp.userIsSpeaker() === true && window.top.webApp.getS("presentationPaused")) {
//		viewer += "?fileID="+pData.fileID;
		fileID = pData.fileID;
	}
	var viewer = "<?=$pdfViewerURL;?>" + fileID + "/" + presentationPage + sessionID + "/" + window.top.Datacore.s.sKey;
	console.log( viewer );
//	$('#doc_viewer').html( '<iframe src="' + viewer + '"></iframe>' );
	$("#doc_viewer_frame").attr( "src", viewer );
	if ($('#dlgActionCancel', window.top.document).length > 0) {
		$('#dlgActionCancel', window.top.document).off().on( 'click', function() {
			$('#doc_viewer').show();
		} );
	}
	if( window.top.Datacore.s.userType === 'M' && (typeof window.top.fileSelection === 'undefined' || window.top.fileSelection === null) ) {
		window.top.fileSelection = new window.top.FileSelection();
		window.top.fileSelection.enableFileSelection();
	}
}

function view_file( fileID ) {
	window.location.href='/depositions/presentation';
}

$(document).ready( function() {
	window.top.Datacore.syncAllFolders();
	if( !window.top.Datacore.s.connectedToDeposition && window.top.Datacore.s.depositionStatus !== 'F' ) {
		parent.connect_to_deposition();
	}
} );

var showSpinner = window.top.showSpinner = function() {
	var div = document.getElementById('file_contents');
	var width = div.offsetWidth - 10;
	$('#spinner').show();
	$('#spinner').width(width);
	$('#spinner').css('top', window.pageYOffset + 'px');
};

var hideSpinner = window.top.hideSpinner = function() {
	$('#spinner').css('top', '0px');
	$('#spinner').hide();
};

function presentationReady() {
	console.log('Presentation::presentationReady()');
	window.top.webApp.setS( 'inDeposition', parseInt( <?=$inDeposition;?> ) );
	if( top.webApp.userIsSpeaker() === true ) {
		$('#outerContainer').addClass( 'speaker' );
		$('#toolbar').show();
		if( typeof window.top.fileSelection === 'undefined' || window.top.fileSelection === null ) {
			window.top.fileSelection = new window.top.FileSelection();
		}
	} else {
		$('#outerContainer').removeClass( 'speaker' );
		$('#toolbar').hide();
	}
	window.top.navBar.enterFullscreen( false );
	if( !window.top.Datacore.s.connectedToDeposition ) {
		window.top.selectDeposition( function() {
			window.top.checkPresentationStatus( null, presentationInit );
		} );
		//return;
	}
	presentationInit();
	window.onbeforeunload = function( e ) {
		$('#ipad_container', window.top.document).removeClass( 'annotator' );
//		if( typeof window.top.fileSelection !== 'undefined' && window.top.fileSelection !== null ) {
//			window.top.fileSelection.disableFileSelection();	//disable not delete, fileSelection persists
//		}
	};
}
window.top.webApp.onReady( presentationReady );

function presentationInit( pData ) {
	if( typeof pData === 'undefined' || pData === null ) {
		pData = window.top.webApp.getS( 'joinPresentationData' );
	}
//	if( typeof pData === 'undefined' || pData === null ) {
//		//return;
//	}
	console.log('Presentation::presentationInit()', 'pData', pData);
	window.top.Datacore.s.inPresentation = true;
	window.top.webApp.setItem( 'inPresentation', 'true' );
	if( window.top.webApp.userIsSpeaker() === true ) {
		window.top.webApp.presentationHost = true;
		$('#ipad_container', window.top.document).addClass( 'annotator' );
		$('#toolbarContainer').show();
	}
	showFileViewer(window.top.webApp.getS('inDeposition'));
	window.top.checkPresentationStatus( null, function( pData ) {
		if( typeof pData !== 'undefined' && pData !== null ) {
			if(!window.top.webApp.userIsSpeaker()) {
				window.top.navBar.setTitle( pData.title );
			}
			var pFile = window.top.Datacore.getFile( pData.fileID );
			if( typeof pFile !== 'undefined' && pFile !== null && !pFile.isExhibit && window.top.webApp.userIsSpeaker() === true ) {
				window.top.navBar.introduceButton.enable();
			} else {
				window.top.navBar.introduceButton.disable();
			}
			window.top.webApp.presentationPageAdd( pData );
		}
	} );
	window.top.Datacore.syncAllFolders( function() {
		if( window.top.webApp.userIsSpeaker() === true ) {
			var pData = window.top.webApp.getS( 'joinPresentationData' );
			if( typeof pData !== 'undefined' && pData !== null ) {
				var pFile = window.top.Datacore.getFile( pData.fileID );
				if( typeof pFile !== 'undefined' && pFile !== null && !pFile.isExhibit ) {
					window.top.navBar.introduceButton.enable();
				} else {
					window.top.navBar.introduceButton.disable();
				}
				if( typeof window.top.fileSelection !== 'undefined' && window.top.fileSelection !== null ) {
					if( pData.annotatorID === window.top.webApp.getS( 'user.ID' ) ) {
						window.top.fileSelection.enableFileSelection();
					} else {
						window.top.fileSelection.disableFileSelection();
					}
				}

			}
		}
	} );
	if ( !window.top.webApp.presentationHost ) {
	}
	window.top.webApp.setS('presentation', 'yes');
	window.top.webApp.setS('beginIntroduce', 'no');
}
<?php $this->Html->scriptEnd(); ?>
</script>
