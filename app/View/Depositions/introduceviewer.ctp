<?php $this->Html->css( 'introduce', null, array( 'inline'=>false ) ); ?>
<?php $this->Html->script( 'introduce', ['inline' => false] ); ?>

<div id="stamp" class="unselectable">
	<div class="dialogIntroduce" class="unselectable">
		<span type="text" id="dialogTitle" class="unselectable"></span>
		<div id="pad"></div>
		<span type="text" id="dialogSubtitle" class="unselectable"></span>
		<div id="bar"></div>
		<span id="date" class="unselectable"></span>
	</div>
	<div id="drag_arrows"></div>
</div>
<div id="spinner"></div>
<div id="outerContainer">
	<canvas id="page-1"></canvas>
</div> <!-- outerContainer -->

<script type="text/javascript">
//window.document.domain = "edepoze.com";
//window.document.domain = "armyargentina.com";
window.document.domain = "<?=Configure::read('SiteConfig')['domain'];?>";

var SCROLLBAR_PADDING = 20;
var introducePage = null;
var rendering = false;
var resizeTimer = null;

function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}

function incrementText(title) {
	var digits = '0123456789';
	var index = title.length - 1;

	//Look for any digits at the end of the string.
	while (index >= 0) {
		var char = title.charAt(index);
		if (digits.indexOf(char) >= 0) {
			index--;
		} else {
			break;
		}
	}

	if (index === title.length - 1) {
		// No digits at the end of the string, so do nothing to the string.
		return title;
	}

	var count;
	if (index === -1) {
		// The whole text is a number.
		count = parseInt(title, 10);
		count++;
		title = pad(count, title.length);
	} else {
		count = parseInt(title.substr(index + 1), 10);
		count++;
		title = title.substr(0, index + 1) + pad(count, title.length - index - 1);
	}

	return title;
}

function updateExhibitCounter(exhibitText) {
	if (typeof exhibitText.title === 'undefined' || exhibitText.title === null) {
		exhibitText.title = window.top.Datacore.s.depositionOf;
	}
	if (typeof exhibitText.subTitle === 'undefined' || exhibitText.subTitle === null) {
		exhibitText.subTitle = 'Exhibit_0';
	}
	if (exhibitText.subTitle.length > 0) {
		//Get the last subtitle value and add one to it.
		exhibitText.subTitle = incrementText(exhibitText.subTitle);
	} else {
		//Update the last value in the title.
		exhibitText.title = incrementText(exhibitText.title);
	}
}


function renderPage() {
	var scale = 1.0;

	if (resizeTimer !== null) {
		clearTimeout(resizeTimer);
		resizeTimer = null;
	}
	// Don't start rendering if we already have started.
	if (rendering) {
		return;
	}
	rendering = true;

	// Prepare canvas using PDF page dimensions and fit width
	var canvas = document.getElementById( 'page-1' );
	var context = canvas.getContext( '2d' );
	var div = document.getElementById( 'outerContainer' );
	var view = introducePage.view;
	var width = div.offsetWidth - SCROLLBAR_PADDING;
	canvas.width = width;
	if (introducePage.rotate === 90 || introducePage.rotate === 270) {
		scale = width / view[3];
		canvas.height = view[2] * scale;
	} else {
		scale = width / view[2];
		canvas.height = view[3] * scale;
	}
	var viewport = introducePage.getViewport( scale );

	// Render PDF page into canvas context
	var renderTask = introducePage.render( {canvasContext: context, viewport: viewport} );
	renderTask.promise.then( renderDone );
	Introduce.renderStart();
	showSpinner();
}

function renderDone() {
	rendering = false;
	hideSpinner();
	Introduce.renderDone();
}

function showSpinner() {
	var div = document.getElementById( 'outerContainer' );
	var width = div.offsetWidth - SCROLLBAR_PADDING;
	$('#spinner').show();
	$('#spinner').width( width );
	$('#spinner').css( 'top', window.pageYOffset + 'px' );
}

function hideSpinner() {
	$('#spinner').css( 'top', '0px' );
	$('#spinner').hide();
}

$(window).resize( function(e) {
	//Set a timer to wait until the resize is done.
	if (resizeTimer !== null) {
		clearTimeout(resizeTimer);
	}
	resizeTimer = setTimeout( renderPage, 100 );
});

$(document).ready( function() {
	var url = '/depositions/file?fileID=<?=$fileID;?>';
	PDFJS.getDocument(url).then(function getPdf(pdf) {
		// Fetch the first page only
		pdf.getPage(1).then(function getPage(page) {
			introducePage = page;
			renderPage();
		});
	});

	var date = new Date ();
	window.top.Datacore.s.exhibitDate = (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear().toString().substr(2);
	var exhibitText = {title: window.top.Datacore.s.exhibitTitle, subTitle: window.top.Datacore.s.exhibitSubTitle};
	window.top.Datacore.s.oldExhibitText = {title: window.top.Datacore.s.exhibitTitle, subTitle: window.top.Datacore.s.exhibitSubTitle};
	updateExhibitCounter(exhibitText);

	// Test cases.
//	console.log(incrementText("000123"));
//	console.log(incrementText("   123"));
//	console.log(incrementText("Exhibit/0003"));
//	console.log(incrementText("Exhibit-123"));
//	console.log(incrementText("Exhibit123"));
//	console.log(incrementText(""));
//	console.log(incrementText("Docs 99"));
//	console.log(incrementText("99 Docs"));
//	console.log(incrementText("Some 99 Docs"));

	Introduce.exhibitXOrigin = window.top.Datacore.s.exhibitXOrigin;
	Introduce.exhibitYOrigin = window.top.Datacore.s.exhibitYOrigin;
	Introduce.fileID = parseInt( <?= $fileID ?> );
	if (Introduce.firstExhibit) {
		parent.parent.parent.showDialogIntroduceInput(exhibitText.title, exhibitText.subTitle, window.top.Datacore.s.exhibitDate, '380px', '260px');
		parent.parent.parent.setDialogIntroduceInputCloseAction(Introduce.finishEdit);
	} else {
		Introduce.showStamp(exhibitText.title, exhibitText.subTitle);
		Introduce.stamp.hide();
	}

});
</script>
