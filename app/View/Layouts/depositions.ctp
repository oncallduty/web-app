<!DOCTYPE html>
<html>
<head>
	<?=$this->Html->charset();?>
	<title><?=$title_for_layout;?></title>
	<?=$this->Html->meta( 'icon', '/img/edepoze/icon.png' ); ?>
	<?=$this->fetch( 'meta' ); ?>
	<link href="/css/fonts.css" rel="stylesheet" type="text/css">
	<?=$this->Html->css( 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css' );?>
	<?=$this->Html->css( 'frame' ); ?>
	<?=$this->Html->css( 'jquery-ui-1.11.4.min' ); ?>
	<?=$this->fetch( 'css' ); ?>
	<link href="../asset/css/custom-admin.css" rel="stylesheet" type="text/css">
	<link href="../asset/css/AdminLTE.min.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div id="content" class="<?=AppHelper::getContainerClasses();?>">
		<?=$this->fetch( 'content' );?>
	</div>
	<div id="sessionflash">
		<?=$this->Session->flash();?>
	</div>
	<div id="debuglog">
		<?=$this->element( 'sql_dump' );?>
	</div>
	<?=$this->Html->script( 'jquery-2.2.4.min' );?>
	<?=$this->Html->script( 'jquery-ui-1.11.4.min' );?>
	<?=$this->Html->script( 'jquery-ui-touchpunch.0.2.3' );?>
	<?=$this->fetch( 'script' ); ?>
	<?= $this->Js->writeBuffer(); ?>
	<?=$this->element( 'webfonts' );?>
</body>
</html>
