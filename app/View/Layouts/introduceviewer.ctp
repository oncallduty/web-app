<!DOCTYPE html>
<html dir="ltr" mozdisallowselectionprint moznomarginboxes>
	<head>
		<?=$this->Html->charset();?>
		<title><?=$title_for_layout;?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="google" content="notranslate">
		<?=$this->Html->meta( 'icon', '/img/edepoze/icon.png' ); ?>
		<?=$this->fetch( 'meta' ); ?>
		<link href="/css/fonts.css" rel="stylesheet" type="text/css">
		<?=$this->Html->css( 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css' );?>
		<?=$this->Html->css( 'jquery-ui-1.11.4.min' ); ?>
		<?=$this->fetch( 'css' ); ?>
		<?=$this->Html->script( 'jquery-2.2.4.min' ); ?>
		<?=$this->Html->script( 'jquery-ui-1.11.4.min' ); ?>
		<?=$this->Html->script( 'jquery-ui-touchpunch.0.2.3' );?>
		<script type="text/javascript" src="/js/pdfjs/pdf.js"></script>
		<?=$this->fetch( 'script' ); ?>
	</head>

	<body tabindex="1" style="background-color: #404040;">
		<?=$this->fetch( 'content' );?>
		<?php /*
		<div id="sessionflash">
			<?=$this->Session->flash();?>
		</div>
		<div id="debuglog">
			<?=$this->element( 'sql_dump' );?>
		</div>

		<?= $this->Js->writeBuffer(); ?>
		 */ ?>

		<?=$this->element( 'webfonts' );?>
	</body>
</html>
