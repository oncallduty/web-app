<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript">
			// break out of any frames
			if (window.self !== window.top) {
				window.parent.location = self.location;
			}
		</script>
		<?= $this->Html->charset(); ?>
		<title><?=$title_for_layout;?></title>
		<?= $this->Html->meta( 'icon', "/img/brands/{$brand}/favicon.ico" ); ?>
		<?= $this->fetch( 'meta' ); ?>
		<link rel="icon" type="image/png" href="/img/brands/<?=$brand;?>/favicon-64x64.png" sizes="64x64" />
		<link href="/css/fonts.css" rel="stylesheet" type="text/css">
		<?=$this->Html->css( 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css' );?>
		<?= $this->Html->css( 'signin' ); ?>
		<?= $this->Html->css( "brands/{$brand}/branding.css" ); ?>
		<?php // echo $this->Html->css( 'jquery-ui-1.11.4.min' ); ?>
		<?= $this->fetch( 'css' ); ?>



  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

  <!-- Theme style -->
  <link rel="stylesheet" href="asset/css/AdminLTE.min.css">
  <link rel="stylesheet" href="asset/css/custom-admin.css" type="text/css" media="screen"/>


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  
	</head>
	<body hold-transition skin-blue sidebar-mini>
		<!-- <? if ($last_error){ ?>
			<div style="position:absolute;top:0px;right:0px;font-size:12px;">
			Error code: <?=$last_error;?>&nbsp;
			</div>
		<? } ?> -->
		<div class="wrapper">
		  <header class="front-hd text-center">
    <!-- Logo -->
   <a href="dashboard.html" class="centerlogo">
     <img src="asset/images/logo.png" >
    </a> 

  </header>
		<div class="center">
		<div id="container">
			
				<?= $this->fetch( 'content' ); ?>
			
		</div>
		<div id="footer">
		</div>
		<div id="sessionflash">
			<?= $this->Session->flash(); ?>
		</div>
		</div>
		</div>
		<div id="debuglog">
			<?= $this->element( 'sql_dump' ); ?>
		</div>
		<?= $this->Html->script( 'jquery-2.2.4.min' ); ?>
		<?php // echo $this->Html->script( 'jquery-ui-1.11.4.min' ); ?>
		<?= $this->fetch( 'script' ); ?>
		<?= $this->Js->writeBuffer(); ?>
		<?=$this->element( 'webfonts' );?>
	</body>
	<footer class="front-footer text-center">
			<section class="ts-copy-right">
			    <div class="container">
				<div class="row">
				    <div class="col-lg-4 col-md-4 col-sm-4">
					<p class="copyright">Copyright &copy; <?=date('Y')?> eDepoze,LLC. </p>            </div>
				    <div class="col-lg-8 col-md-8 col-sm-8">
				      <div id="footer-logo" class="footer-logo" style="display:block;"></div>
				    </div>
				</div>
			    </div>
			</section>			   
</footer>
</html>
