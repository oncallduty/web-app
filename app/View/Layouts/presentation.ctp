<!DOCTYPE html>
<html dir="ltr" mozdisallowselectionprint moznomarginboxes>
	<head>
		<?=$this->Html->charset();?>
		<title><?=$title_for_layout;?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
		<meta name="google" content="notranslate"/>
		<?=$this->Html->meta( 'icon', '/img/edepoze/icon.png' );?>
		<?=$this->fetch( 'meta' );?>
		<link href="/css/fonts.css" rel="stylesheet" type="text/css">
		<?=$this->Html->css( 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css' );?>
		<link rel="stylesheet" href="/css/presentation.css"/>
		<?=$this->Html->css( 'pdfviewer' );?>
		<?=$this->fetch( 'css' );?>
		<?=$this->Html->script( 'jquery-2.2.4.min' );?>
	</head>
	<body tabindex="1">
		<div id="content" class="<?=AppHelper::getContainerClasses();?>">
			<?=$this->element( 'menumodifyannotation' );?>
			<?=$this->element( 'menucolor' );?>
			<?=$this->element( 'menuthickness' );?>
			<?=$this->element( 'menuopacity' );?>
			<?=$this->fetch( 'content' );?>
		</div>
		<?=$this->fetch( 'script' );?>
		<?=$this->Js->writeBuffer();?>
		<?=$this->element( 'webfonts' );?>
	</body>
</html>
