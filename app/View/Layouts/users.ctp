<!DOCTYPE html>
<html>
<head>
	<?=$this->Html->charset();?>
	<title><?=$title_for_layout;?></title>
	<?=$this->Html->meta( 'icon', "/img/brands/{$brand}/favicon.ico" );?>
	<?=$this->fetch( 'meta' );?>
	<link rel="icon" type="image/png" href="/img/brands/<?=$brand;?>/favicon-64x64.png" sizes="64x64" />
	<link href="/css/fonts.css" rel="stylesheet" type="text/css">
	<?=$this->Html->css( 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css' );?>
	<?=$this->Html->css( 'app' );?>
	<?=$this->Html->css( 'usersort' );?>
	<?=$this->Html->css( 'contentsearch' );?>
	<?=$this->Html->css( "brands/{$brand}/branding.css" );?>
	<?=$this->Html->css( 'jquery-ui-1.11.4.min' );?>
	<?=$this->fetch( 'css' ); ?>
	
		<link href="../asset/css/custom-admin.css" rel="stylesheet" type="text/css">
		<link href="../asset/css/AdminLTE.min.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		
		
  

<body>
	<script type="text/javascript">
	//window.document.domain = "edepoze.com";
	//window.document.domain = "armyargentina.com";
	window.document.domain = "<?=Configure::read('SiteConfig')['domain'];?>";

	</script>
	<div id="container">
		
		<div id="content">
			<div id="ipad_container" class="<?=AppHelper::getContainerClasses();?>">
				<?=$this->element( 'dialog1btn' );?>
				<?=$this->element( 'dialog3btn' );?>
				<?=$this->element( 'dialog3btnver' );?>
				<?=$this->element( 'dialogtooltip' );?>
				<?=$this->element( 'dialoglogout' );?>
				<?=$this->element( 'dialogjoin' );?>
				<?=$this->element( 'dialogsearch' );?>
				<?=$this->element( 'tooltip' );?>
				<?=$this->element( 'colorpicker' );?>
				<?=$this->element( 'dialogtextinput' );?>
				<?=$this->element( 'dialogtextinputwithdescription' );?>
				<?=$this->element( 'dialogintroduceinput' );?>
				<?=$this->element( 'dialogdepositionpasscode' );?>
				<?=$this->element( 'dialogsavefile' );?>
				<?=$this->element( 'dialognoteentry' );?>
				<?=$this->element( 'fileselection' );?>
				<?=$this->fetch( 'content' );?>
			</div>
		</div>
		
				
	</div>
	<div id="debuglog">
		<?=$this->element( 'sql_dump' );?>
	</div>
	<?=$this->Html->script( 'jquery-2.2.4.min' );?>
	<?=$this->Html->script( 'jquery-ui-1.11.4.min' );?>
	<?=$this->Html->script( 'jquery-ui-touchpunch.0.2.3' );?>
	<?=$this->Html->script( 'webapp' );?>
	<?=$this->fetch( 'script' ); ?>
	<?= $this->Js->writeBuffer(); ?>
	<?=$this->element( 'webfonts' );?>
</body>
<footer class="front-footer text-center">
			<section class="ts-copy-right">
			    <div class="container">
				<div class="row">
				    <div class="col-lg-4 col-md-4 col-sm-4">
    
					<p class="copyright">Copyright &copy; <?=date('Y')?> eDepoze,LLC. </p></div>
				    <div class="col-lg-8 col-md-8 col-sm-8">
				      <div id="footer-logo" class="footer-logo" style="display:block;"></div>
				    </div>
				</div>
			    </div>
			</section>			   
</footer>
</html>
