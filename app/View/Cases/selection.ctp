<?php $this->Html->css( 'case-selection', null, array('inline' => false) ); ?>

<div id="loading">
	<div class="spinner">
		<img src="/img/ipad/loading.gif" width="48" height="48" border="0" />
		<div>Loading ...</div>
	</div>
</div>
<?=$this->element( 'titlebar' );?>
<section id="content_header">
	<div id="heading-wrapper">
		<h1>Select a Location:</h1>
	</div>
	<div class="selection_instructions">
		<p>Please select a Deposition from the list:</p>
	</div>
</section>
<section id="show_depositions">
	<div id="list_cases">
		<ul></ul>
	</div>
	<div id="list_depositions">
		<ul></ul>
	</div>
</section>

<?=$this->Html->script( 'datacore', array( 'inline' => false ) ); ?>
<?php // insert view js into buffered codeblock ?>
<script type="text/javascript">
<?php $this->Html->scriptStart( array( 'inline' => false ) ); ?>
function init_cases() {
	<?php if (isset( $api_data_class ) && count( $api_data_class ) > 0): ?>
	Datacore.loadCases( <?= json_encode( $api_data_class ) ?> );
	<?php endif; ?>
}

function toggle_case( caseID ) {
	$('#list_cases ul li').removeClass( 'toggled' );

	$('#list_depositions ul').empty();

	$('#case_item_'+caseID).addClass( 'toggled' );

	// set option to create new deposition
	$('#list_depositions ul').append(
		'<li class="add" onclick="verifyLink('+caseID+', null);">\
			<div class="depo_name"><div><p><strong>Select here to add the Session to this Case</strong></p><p></p></div></div>\
			<div class="depo_info"><p>&nbsp;</p></div>\
			<div class="depo_info"><p>&nbsp;</p></div>\
			<div class="depo_actions"><p>&nbsp;</p></div>\
		</li>'
	);

	// load the depositions for this caseID
	var activecase = Datacore.getCase( caseID );
	var depositions = activecase.getDepositions();
	var depositionClass = '<?= $depositionClass ?>';
	for (var d=0; d<depositions.length; ++d) {
		var depo = depositions[d];

		if( depo.class !== depositionClass ){
			continue;
		}

		$('#list_depositions ul').append(
			'<li onclick="verifyLink('+caseID+', '+depo.ID+');">\
				<div class="depo_name"><div><p><strong>Deposition of '+depo.depositionOf+'</strong></p><p>Title/Affiliation: '+depo.title+'</p></div></div>\
				<div class="depo_info"><p><em>Volume: '+depo.volume+'</em></p></div>\
				<div class="depo_info"><p><strong>'+(
					(depo.created.getMonth()+1) + '/' + depo.created.getDate() + '/' + depo.created.getFullYear()
				)+'</strong></p></div>\
				<div class="depo_actions"><p class="depo_go"></p></div>\
			</li>'
		);
	}
	var min_depo_length = 9;
	if (depositions.length < min_depo_length) {
		for (var i=(min_depo_length-depositions.length); i>0; --i) {
			$('#list_depositions ul').append( '<li>&nbsp;</li>' );
		}
	}
}

function verifyLink( caseID, depoID ) {
	var dialogButtons = [];
	dialogButtons.push( {label: 'Cancel', action : 'dismissDialog3Btn();', class : 'app_bttn_cancel'} );
	dialogButtons.push( {label: 'Save', action : 'toggle_depo('+caseID+', '+depoID+');', class : 'app_bttn_new'} );
	var confirmLocation = 'All files from this Session will be saved to this folder.';

	showDialog3Btn( 'Location confirmed!', confirmLocation, dialogButtons, true );
}

function toggle_depo( caseID, depoID ) {
	dismissDialog3Btn();
	console.log( '-- creating new Deposition Link');
	$("#loading").fadeIn( 200 );

	var url = '/cases/selection?cID='+caseID+'&dID='+(typeof depoID !== 'undefined' && depoID ? depoID : '0');
	$.ajax( url ).done( function( data, status ) {
		$("#loading").fadeOut( 200 );

		try {
			var response = jQuery.parseJSON( data );
		} catch( err ) {
			console.log( '-- [ERROR] ' + err.message );
			if( typeof( showDialog1Btn ) === 'function' ) {
				showDialog1Btn( 'Error', 'Unexpected error: "' + err.message + '"', 'OK' );
			} else {
				alert( err.message );
			}
		}

		if (typeof( response ) === 'undefined' || response === null) {
			console.log( '-- [ERROR] Invalid response data' );
			return;
		}

		if (response.hasOwnProperty( 'redirect' )) {
			console.log( '-- redirecting to: ' + response.redirect );
			window.location.assign( response.redirect );
			return;
		}

		if( response.hasOwnProperty( 'success' ) ) {
			if( response.success !== true ) {
				if( response.hasOwnProperty( 'error' ) ) {
					console.log( '-- [ERROR] ' + response.error );
					if( typeof( showDialog1Btn ) === 'function' ) {
						showDialog1Btn( 'Error', response.error, 'OK' );
					} else {
						alert( response.error );
					}
				}
				return;
			} else {
				console.log( '-- success! Okay to go.' );
			}
		}
	} );
}

function redirect() {
	window.location = '/users/logout?redirect=/members&ul=1';
}

$(document).ready( function() {
	if ($('#past_depo_date').length > 0) {
		$('#past_depo_date').datepicker();
	}

	parent._titleActions.setBackAction( 'javascript:redirect();' );
	parent._titleActions.showBack();
	parent._titleActions.redraw();

	init_cases();

	var cases = Datacore.getCases();
	for (var c=0; c < cases.length; ++c) {
		$('#list_cases ul').append(
			'<li id="case_item_'+cases[c].ID+'" onclick="toggle_case('+cases[c].ID+');">'
			+ '<div><p>'+cases[c].name+'</p><p class="case_number">Case #: '+(cases[c].number)+'</p></div></li>'
		);
	}
	var min_case_length = 9;
	if (cases.length < min_case_length) {
		for (var i=(min_case_length-cases.length); i>0; --i) {
			$('#list_cases ul').append( '<li><div>&nbsp;</div></li>' );
		}
	}
} );
<?php $this->Html->scriptEnd( array( 'inline' => false ) ); ?>
</script>
