<?php $this->Html->css( 'case-browse', null, ['inline'=>FALSE] ); ?>
<?=$this->Html->script( 'datacore', ['inline'=>FALSE] );?>
<?=$this->Html->script( 'webapp', ['inline'=>FALSE] );?>

<div id="loading">
	<div class="spinner">
		<img src="/img/ipad/loading.gif" width="48" height="48" border="0" />
		<div>Loading ...</div>
	</div>
</div>
<?=$this->element( 'navbar' );?>
<section id="content_header">
	<div id="heading-wrapper">
		<h1>Select a Location:</h1>
	</div>
	<div class="browse_instructions">
		<p>Please select a Deposition from the list:</p>
	</div>
</section>
<section id="browsePanel">
	<div class="casesPanel">
		<div id="casesList">
			<ul></ul>
		</div>
	</div>
	<div class="deposPanel">
		<div id="deposList">
			<ul></ul>
		</div>
	</div>
</section>
<script type="text/javascript">
<?php $this->Html->scriptStart( ['inline'=>FALSE] ); ?>
//window.document.domain = "edepoze.com";
//window.document.domain = "armyargentina.com";
window.document.domain = "<?=Configure::read('SiteConfig')['domain'];?>";

top.Datacore.s.isJoin = <?=json_encode( $isJoin );?>;

$(document).ready( function() {
	<?php if( isset( $api_data_class ) && count( $api_data_class ) ): ?>
	top.Datacore.loadCases( <?= json_encode( $api_data_class ) ?> );
	<?php endif; ?>
	top.browseView = new BrowseView( '<?=$depositionClass;?>' );
} );

var BrowseView = function( depoClass )
{
	var cases = [];
	var depositions = [];
	var caseID = null;
	var _cases = top.Datacore.getCases();
	for( var c in _cases ) {
		var _case = _cases[c];
		switch( depoClass ) {
			case 'Arbitration':
			case 'Deposition':
			case 'Hearing':
			case 'Mediation':
			case 'Trial':
			case 'WitnessPrep':
				if( _case.class === 'Case' ) {
					cases.push( _case );
				}
				break;
			case 'Demo':
			case 'Demo Trial':
			case 'WPDemo':
				if( _case.class === 'Demo' ) {
					cases.push( _case );
				}
				break;
		}
	}
	delete _cases;
	var casesList = $('#casesList ul');
	var deposList = $('#deposList ul');

	var sortByName = function( a, b ) {
		if( a.name > b.name ) {
			return 1;
		} else if( a.name < b.name ) {
			return -1;
		} else {
			if( a.ID > b.ID ) {
				return 1;
			} else if( a.ID < b.ID ) {
				return -1;
			}
			return 0;
		}
	};

	var sortCases = function()
	{
		cases = cases.sort( sortByName );
		buildCasesList();
	};

	var sortDepos = function()
	{
		if( !caseID ) {
			return;
		}
		var selectedCase = null;
		for( var c in cases ) {
			if( cases[c].ID === caseID ) {
				selectedCase = cases[c];
			}
		}
		if( !selectedCase ) {
			return;
		}
		var _depos = selectedCase.getDepositions();
		depositions = [];
		for( var d in _depos ) {
			var _depo = _depos[d];
			if( _depo.class === depoClass  && _depo.statusID === 'N' ) {
				depositions.push( _depo );
			}
		}
		delete _depos;
		depositions = depositions.sort( sortByName );
		buildDeposList();
	};

	var buildCasesList = function()
	{
		casesList.empty();
		var elementHTML = '<li></li>';
		var labelHTML = '<div class="caseLabel"></div>';
		for( var c in cases ) {
			var _case = cases[c];
			var li = $( elementHTML );
			li.attr( 'data-id', _case.ID );
			li.addClass( 'case' );
			if( caseID === _case.ID ) {
				li.addClass( 'selected' );
			}
			var label = $( labelHTML );
			label.append( _case.name );
			li.append( label );
			li.on( 'click', {"caseID":_case.ID}, function( event ) {
				selectCase( event.data.caseID );
			} );
			casesList.append( li );
		}
		var listMinLength = 11;
		if( casesList.length < listMinLength ) {
			for( var i = (listMinLength - cases.length); i > 0; --i ) {
				casesList.append( elementHTML );
			}
		}
	};

	var buildDeposList = function()
	{
		deposList.empty();
		var elementHTML = '<li></li>';
		var depoNameHTML = '<div class="depoName"></div>';
		var witnessHTML = '<div class="witness"></div>';
		var volumeHTML = '<div class="volume"></div>';
		var scheduledHTML = '<div class="depoDate"></div>';

		var depoPH = new DcDeposition( {"ID":0, "depositionOf":"Select here to add the Session to this Case"} );
		depositions.unshift( depoPH );

		for( var d in depositions ) {
			var _depo = depositions[d];
			var li = $( elementHTML );
			li.attr( 'data-id', _depo.ID );
			li.addClass( 'deposition' );
			if( _depo.ID === 0 ) {
				li.addClass( 'add' );
			}
			var depoName = $( depoNameHTML );
			var witness = $( witnessHTML );
			witness.append( _depo.depositionOf );
			depoName.append( witness );
			li.append( depoName );
			if( _depo.volume ) {
				var volume = $( volumeHTML );
				volume.append( 'Volume: ' + _depo.volume );
				li.append( volume );
			}
			if( _depo.openDateTime ) {
				var scheduled = $( scheduledHTML );
				var scheduledDate = (_depo.openDateTime.getMonth() + 1 ) + '/' + _depo.openDateTime.getDate() + '/' + _depo.openDateTime.getFullYear();
				scheduled.append( scheduledDate );
				li.append( scheduled );
			}
			li.on( 'click', {"caseID":caseID,"depoID":_depo.ID}, function( event ) {
				verifyLink( event.data.caseID, event.data.depoID );
			} );
			deposList.append( li );
		}

		var listMinLength = 11;
		if( deposList.length < listMinLength ) {
			for( var i = (listMinLength - depositions.length); i > 0; --i ) {
				deposList.append( elementHTML );
			}
		}
	};

	var selectCase = function( ID )
	{
		var _caseID = parseInt( ID );
		if( isNaN( _caseID ) || _caseID <= 0 ) {
			return;
		}
		caseID = _caseID;
		buildCasesList();
		sortDepos();
	};

	var verifyLink = function( caseID, depoID )
	{
		var dialogButtons = [];
		dialogButtons.push( {label:'Cancel', action:'dismissDialog3Btn();', class:'app_bttn_cancel'} );
		dialogButtons.push( {label:'Save', action:'top.browseView.selectDeposition(' + caseID + ',' + depoID + ');', class:'app_bttn_new'} );
		var confirmLocation = 'All files from this Deposition will be saved to this folder.';
		showDialog3Btn( 'Location confirmed!', confirmLocation, dialogButtons, true );
	};

	this.selectDeposition = function( caseID, depoID )
	{
		dismissDialog3Btn();
		$('#loading').fadeIn( 200 );
		var url = '/cases/browse?cID=' + caseID + '&dID=' + (typeof depoID !== 'undefined' && depoID ? depoID : '0');
		$.ajax( url ).done( function( data, status ) {
			$("#loading").fadeOut( 200 );
			try {
				var response = JSON.parse( data );
			} catch( err ) {
				console.log( '-- [ERROR] ' + err.message );
				if( typeof( showDialog1Btn ) === 'function' ) {
					showDialog1Btn( 'Error', 'Unexpected error: "' + err.message + '"', 'OK' );
				} else {
					alert( err.message );
				}
			}
			if( typeof response === 'undefined' || response === null ) {
				console.log( '-- [ERROR] Invalid response data' );
				return;
			}
			if( response.hasOwnProperty( 'redirect' ) ) {
				console.log( '-- redirecting to: ' + response.redirect );
				window.location.assign( response.redirect );
				return;
			}
			if( response.hasOwnProperty( 'success' ) ) {
				if( response.success !== true ) {
					if( response.hasOwnProperty( 'error' ) ) {
						console.log( '-- [ERROR] ' + response.error );
						if( typeof( showDialog1Btn ) === 'function' ) {
							showDialog1Btn( 'Error', response.error, 'OK' );
						} else {
							alert( response.error );
						}
					}
					return;
				} else {
					console.log( '-- success! Okay to go.' );
				}
			}
		} );
	};

	sortCases();
	if( cases.length > 0 ) {
		selectCase( cases[0].ID );
	}
};
<?php $this->Html->scriptEnd(); ?>
</script>
