<?php
	$page_instructions_new = 'View new sessions below. To begin, select the case file the session is associated with. To view past sessions, click the "past sessions" button above.';
	$page_instructions_past = 'View past sessions below. To begin, select the case file the session is associated with.';

	$this->Html->css( 'usersort', NULL, ['inline'=>FALSE] );
	$this->Html->css( 'cases', NULL, ['inline'=>FALSE] );
?>
<head>


  

	<link href="../asset/css/custom-admin.css" rel="stylesheet" type="text/css">
	<link href="../asset/css/AdminLTE.min.css" rel="stylesheet" type="text/css">
	<?=$this->Html->css( 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' );?>
			
	<?=$this->Html->css( 'https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css' );?>
	<?=$this->Html->css( 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic' );?>
	<?=$this->Html->css( 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );?>

  



    
</head>

<section class="tablesPanel" id="tablesPanel">
	<div class="casesPanel">
		<div class="legend"><img src="/asset/images/f1.png">&nbsp;Cases</div>
		<div id="sortCases" class="sortHeader"></div>
		<div id="casesList">
			<ul></ul>
		</div>
		</div>

		<div class="deposPanel">
			<div class="legend"><img src="/asset/images/f2.png">&nbsp;Sessions</div>
			<div id="sortDepos" class="sortHeader" style="display:none;"></div>
			<div id="deposList">
				<div id="caseMessage">Select the Case you wish to view</div>
				<ul></ul>
			</div>
		</div>

</section>
<script type="text/javascript">
//<![CDATA[
//window.document.domain = "edepoze.com";
//window.document.domain = "armyargentina.com";
window.document.domain = "<?=Configure::read('SiteConfig')['domain'];?>";

window.top.Datacore.clean();
$('#ipad_container', window.top.document).removeClass( 'viewer' ).removeClass( 'fullscreen' );

if (typeof window.top.Datacore.s.connectedToDeposition !== 'undefined' && window.top.Datacore.s.connectedToDeposition) {
	window.top.disconnect_from_deposition();
}

var selectedCaseID = (window.top.webApp.hasItem('selectedCaseID')) ? parseInt( window.top.webApp.getItem( 'selectedCaseID' ) ) : 0;

function padItemList( list_id, min_length, list_contents ) {
	if (typeof list_id === 'undefined' || list_id === null) {
		list_id = '';
	}
	if (typeof min_length === 'undefined' || min_length === null) {
		min_length = 0;
	}
	if (typeof list_contents === 'undefined' || list_contents === null || list_contents.length < 1) {
		list_contents = '&nbsp;';
	}
	if ($(list_id).length < 1) {
		return;
	}
	var current_length = $(list_id+' li').length;
	if (current_length < min_length) {
		for (var i=(min_length-current_length); i>0; --i) {
			$(list_id).append( '<li>'+list_contents+'</li>' );
		}
	}
}

function draw_page_lists( caseID ) {
	draw_list_cases();

	if (typeof caseID !== 'undefined' && caseID !== null) {
		draw_list_depositions( caseID );
	}
}

function draw_list_cases() {
	console.log( 'draw_list_cases -- deprecated' );
	window.top.casesView.sortCases();
}

function draw_list_depositions( caseID )
{
	console.log( 'draw_list_depositions -- deprecated' );
	window.top.casesView.sortDepositions();
}

function select_deposition( caseID, depoID )
{
	var selectedDeposition = window.top.Datacore.getDeposition( depoID );

	if( typeof selectedDeposition.banned !== 'undefined' && selectedDeposition.banned !== null ) {
		window.top.showDialog1Btn( 'Access Denied', 'You do not have access to this deposition.', 'OK' );
		return;
	} else if ( selectedDeposition.statusID === 'F' ) {
//		window.top.Datacore.clean();
		window.location.href='/cases/deposition?ID='+depoID+'&caseID='+caseID;
		return;
	}

	$.getJSON( '/cases/attendDeposition?ID='+depoID, null, function(data, textStatus, xhr) {
		if (textStatus === 'success') {
			if (typeof data.error === 'undefined') {
//				window.top.Datacore.clean();
				window.top.showLoadingOverlay( 'Joining...' );
				window.location.href='/cases/deposition?ID='+depoID+'&caseID='+caseID;
			} else if(data.error.code === 106 && data.error.description !== '') {
				window.top.showPlainDialog1Btn( 'Notice!', data.error.description, 'OK' );
			} else {
				window.top.showPlainDialog1Btn( 'Notice!', 'The Session has not started yet', 'OK' );
			}
		} else {
			window.top.showPlainDialog1Btn( 'Error', 'Could not attend the session.', 'OK' );
		}
	} );
}
//]]>
</script>
<script type="text/javascript">
<?php $this->Html->scriptStart( ['inline'=>FALSE] ); ?>
window.top.webApp.removeItem( 'selectedFolderID' );
function CasesView()
{
	var bind = this;
	var contentBox = $('#content');
	var tablesPanel = $('.tablesPanel');
	var casesList = $('#casesList ul', tablesPanel);
	var deposList = $('#deposList ul', tablesPanel);
	var cases = null;
	var fCases = [];
	var depositions = null;
	var fDepositions = [];
	var caseID = null;
	var depoFilter = 'all';	//all | scheduled | completed
	var inCustomSort = null;

	if( window.top.webApp.hasItem( 'selectedCaseID' ) ) {
		var _sCaseID = parseInt( window.top.webApp.getItem( 'selectedCaseID' ) );
		if( !isNaN( _sCaseID ) && _sCaseID > 0 ) {
			caseID = _sCaseID;
		}
	}

	this.sortCases = function()
	{
//		console.log( 'sortCases' );
		if( !cases ) {
			cases = window.top.Datacore.getCases();
		}
		fCases = [];
		if( caseID ) {
			var hasCase = false;
			for( var c in cases ) {
				if( cases[c].ID === caseID ) {
					hasCase = true;
					break;
				}
			}
			if( !hasCase ) {
				caseID = null;
			}
			depositions = fDepositions = [];
			buildDeposList();
		}
		fCases = window.top.userSortCases.sort( cases );
		if( !caseID && fCases.length > 0 ) {
			caseID = cases[0].ID;
			window.top.webApp.setItem( 'selectedCaseID', caseID );
		}
		buildCasesList();
		if( caseID ) {
			bind.selectCase( caseID );
		}
	};

	this.sortDepositions = function()
	{
//		console.log( 'sortDepositions', depoFilter );
		if( !caseID ) {
			return;
		}
		if( !depositions ) {
			var _case = window.top.Datacore.getCase( caseID );
			depositions = _case.getDepositions();
		}
		fDepositions = [];
		for( var d in depositions ) {
			var _depo = depositions[d];
			if( depoFilter === 'all' || (depoFilter === 'scheduled' && (_depo.statusID === 'N' || _depo.statusID === 'I')) || (depoFilter === 'completed' && _depo.statusID === 'F') ) {
				fDepositions.push( _depo );
			}
		}
		fDepositions = window.top.userSortDepos.sort( fDepositions );
		buildDeposList();
	};

	this.selectCase = function( ID )
	{
		ID = parseInt( ID );
		if( isNaN( ID ) || ID <= 0 ) {
			return;
		}
		if( !cases ) {
			cases = window.top.userSortCases.sort( window.top.Datacore.getCases() );
		}
		var _caseID = null;
		for( var i in cases ) {
			if( cases[i].ID === ID ) {
				_caseID = ID;
				break;
			}
		}
		if( !_caseID ) {
			return;
		}
		var _case = window.top.Datacore.getCase( _caseID );
		if( typeof _case !== 'object' || _case === null ) {
			return;
		}
		if( inCustomSort ) {
			bind.endCustomSort();
		}
		if( _caseID ) {
			var dSortHeader = $('#sortDepos.sortHeader');
			dSortHeader.show();
			$('#caseMessage').hide();
		}
		caseID = _caseID;
		window.top.webApp.setItem( 'selectedCaseID', caseID );
		$('li', casesList).removeClass( 'selected' );
		$('#case_item_' + caseID).addClass( 'selected' );
		depositions = _case.getDepositions();
		this.sortDepositions();
	};

	this.selectDepoFilter = function( mode )
	{
		mode = mode.toLowerCase();
//		console.log( 'selectDepoFilter' );
		if( inCustomSort ) {
			bind.endCustomSort();
		}
		switch( mode ) {
			case 'all':
			case 'scheduled':
			case 'completed':
				depoFilter = mode;
				break;
		}
		this.sortDepositions();
		buildDeposList();
	};

	var buildCasesList = function()
	{
//		console.log( 'buildCasesList' );
		var elementHTML = '<li></li>';
		var caseLabelHTML = '<div class="caseLabel"></div>';
		var caseNumHTML = '<div class="caseNumber"><span>Case #:&nbsp;</span></div>';

		casesList.empty();

		for( var c in fCases ) {
			var _case = fCases[c];
			var li = $( elementHTML );
			li.attr( 'id', 'case_item_' + _case.ID );
			li.attr( 'data-id', _case.ID );
			li.addClass( 'sortable' );
			if( caseID === _case.ID ) {
				li.addClass( 'selected' );
			}
			var dragHandle = $( '<div class="dragBox"><div class="handle"></div></div>' );
			li.append( dragHandle );
			var label = $( caseLabelHTML );
			label.append( _case.name );
			li.append( label );
			var caseNum = $( caseNumHTML );
			caseNum.append( _case.number );
			li.append( caseNum );
			li.on( 'click', {caseID:_case.ID}, function( event ) {
				bind.selectCase( event.data.caseID );
			} );
			casesList.append( li );
		}

		var casesMinLength = 10;
		if( casesList.length < casesMinLength ) {
			for( var i = (casesMinLength - fCases.length); i > 0; --i ) {
				casesList.append( elementHTML );
			}
		}
	};

	var buildDeposList = function()
	{
//		console.log( 'buildDeposList' );
		deposList.empty();
		var sortBy = window.top.userSortDepos.getSortByField();
		var sortOrder = window.top.userSortDepos.getSortOrder();
		var elementHTML = '<li></li>';
		var statusIconHTML = '<div class="status"><div class="statusIcon"></div></div>';
		var depoNameHTML = '<div class="depoName"></div>';
		var witnessHTML = '<div class="witness"></div>';
		var typeHTML = '<div class="depoType"><span>Type:</span></div>';
		var depoStatusHTML = '<div class="depoStatus"></div>';
		var volDateHTML = '<div class="depoVolDate"></div>';
		var volumeHTML = '<div class="depoVolume"></div>';
		var dateHTML = '<div class="depoDate"></div>';
		var actionHTML = '<div class="depoActions"><div class="depoGo">&nbsp;</div></div>';
		var sectionHTML = '<div class="section"></div>';

		var buildDepoLineItem = function()
		{
			var dateField = '';
			switch (_depo.statusID) {
				case 'N':
					dateField = 'openDateTime';
					break;
				case 'I':
					dateField = 'started';
					break;
				case 'F':
					dateField = 'finished';
					break;
			}
			var depo_date = '';
			if( typeof _depo[dateField] !== 'undefined' && _depo[dateField] !== null ) {
				var _date = _depo[dateField];
				depo_date = (_date.getMonth() + 1 ) + '/' + _date.getDate() + '/' + _date.getFullYear();
			}
			var li = $( elementHTML );
			li.attr( 'depo_item_' + _depo.ID );
			li.attr( 'data-id', _depo.ID );
			li.addClass( 'sortable' );
			li.on( 'click', {"caseID":caseID, "depositionID":_depo.ID}, function( event ) {
				select_deposition( event.data.caseID, event.data.depositionID );
			} );
			var dragHandle = $( '<div class="dragBox"><div class="handle"></div></div>' );
			li.append( dragHandle );
			var statusIcon = $( statusIconHTML );
			statusIcon.addClass( 'depoStatus' + _depo.statusID );
			li.append( statusIcon );
			var depoName = $( depoNameHTML );
			var witness = $( witnessHTML );
			witness.append( _depo.depositionOf );
			var type = $( typeHTML );
			switch( _depo.class ) {
				case 'WitnessPrep':
					type.append( 'Witness Prep' );
					break;
				case 'WPDemo':
					type.append( 'Demo Witness Prep' );
					break;
				case 'Demo':
					type.append( 'Demo Deposition' );
					break;
				case 'Trial':
					type.append( 'Trial' );
					break;
				case 'Demo Trial':
					type.append( 'Demo Trial' );
					break;
				case 'Arbitration':
					type.append( 'Arbitration' );
					break;
				case 'Mediation':
					type.append( 'Mediation' );
					break;
				case 'Hearing':
					type.append( 'Hearing' );
					break;
				case 'Trial Binder':
				default:
					type.append( _depo.class );
					break;
			}
			depoName.append( witness );
			depoName.append( type );
			li.append( depoName );
			var depoStatus = $( depoStatusHTML );
			var statusLabel;
			switch( _depo.statusID ) {
				case 'N':
					statusLabel = 'Scheduled';
					break;
				case 'I':
					statusLabel = 'Active';
					break;
				case 'F':
					statusLabel = 'Completed';
					break;
			}
			depoStatus.append( '<p>' + statusLabel + '</p>' );
			li.append( depoStatus );
			var volDate = $( volDateHTML );
			var volume = $( volumeHTML );
			volume.attr( 'title', 'Volume: ' + _depo.volume);
			volume.append( 'Volume: ' + ((_depo.volume.length > 7) ? _depo.volume.substring(0,5)+'...' : _depo.volume) );
			var depoDate = $( dateHTML );
			depoDate.append( depo_date );
			volDate.append( volume );
			volDate.append( depoDate );
			li.append( volDate );
			li.append( actionHTML );
			return li;
		};

		var depos = [];
		var preps = [];
		var trials = [];
		var arbitrations = [];
		var mediations = [];
		var hearings = [];
		var trialBinders = [];
		var loadDeposToList = function()
		{
			deposList.append( dSecLI );
			for( var i in depos ) {
				deposList.append( depos[i] );
			}
		};
		var loadPrepsToList = function()
		{
			deposList.append( wpSecLI );
			for( var i in preps ) {
				deposList.append( preps[i] );
			}
		};
		var loadTrialsToList = function()
		{
			deposList.append( tSecLI );
			for( var i in trials ) {
				deposList.append( trials[i] );
			}
		};
		var loadTrialBindersToList = function()
		{
			deposList.append( tbSecLI );
			for( var i in trialBinders ) {
				deposList.append( trialBinders[i] );
			}
		};
		var loadArbitrationsToList = function()
		{
			deposList.append( aSecLI );
			for( var i in arbitrations ) {
				deposList.append( arbitrations[i] );
			}
		};
		var loadMediationsToList = function()
		{
			deposList.append( mSecLI );
			for( var i in mediations ) {
				deposList.append( mediations[i] );
			}
		};
		var loadHearingsToList = function()
		{
			deposList.append( hSecLI );
			for( var i in hearings ) {
				deposList.append( hearings[i] );
			}
		};

		for( var d in fDepositions ) {
			var _depo = fDepositions[d];
			var li = buildDepoLineItem();
			if( sortBy !== 'class' ) {
				deposList.append( li );
			} else {
				if( _depo.class === 'WitnessPrep' || _depo.class === 'WPDemo' ) {
					preps.push( li );
				} else if ( _depo.class === 'Trial' || _depo.class === 'Demo Trial' ) {
					trials.push( li );
				} else if ( _depo.class === 'Arbitration' ) {
					arbitrations.push( li );
				} else if ( _depo.class === 'Mediation' ) {
					mediations.push( li );
				} else if ( _depo.class === 'Hearing' ) {
					hearings.push( li );
				} else if ( _depo.class === 'Trial Binder' ) {
					trialBinders.push( li );
				} else {
					depos.push( li );
				}
			}
		}

		if( sortBy === 'class' ) {
			var dSecLI = $( elementHTML );
			var wpSecLI = $( elementHTML );
			var tSecLI = $( elementHTML );
			var tbSecLI = $( elementHTML );
			var aSecLI = $( elementHTML );
			var mSecLI = $( elementHTML );
			var hSecLI = $( elementHTML );
			dSecLI.addClass( 'section' );
			wpSecLI.addClass( 'section' );
			tSecLI.addClass( 'section' );
			tbSecLI.addClass( 'section' );
			aSecLI.addClass( 'section' );
			mSecLI.addClass( 'section' );
			hSecLI.addClass( 'section' );
			var depoSection = $( sectionHTML );
			depoSection.append( 'Deposition' );
			var wpSection = $( sectionHTML );
			wpSection.append( 'Witness Prep' );
			var trialSection = $( sectionHTML );
			trialSection.append( 'Trial' );
			var trialBinderSection = $( sectionHTML );
			trialBinderSection.append( 'Trial Binder' );
			var arSection = $( sectionHTML );
			arSection.append( 'Arbitration' );
			var meSection = $( sectionHTML );
			meSection.append( 'Mediation' );
			var heSection = $( sectionHTML );
			heSection.append( 'Hearing' );
			dSecLI.append( depoSection );
			wpSecLI.append( wpSection );
			tSecLI.append( trialSection );
			tbSecLI.append( trialBinderSection );
			aSecLI.append( arSection );
			mSecLI.append( meSection );
			hSecLI.append( heSection );

			if( sortOrder === 'Asc' ) {
				loadArbitrationsToList();
				loadDeposToList();
				loadHearingsToList();
				loadMediationsToList();
				loadTrialsToList();
				loadTrialBindersToList();
				loadPrepsToList();
			} else {
				loadPrepsToList();
				loadTrialBindersToList();
				loadTrialsToList();
				loadMediationsToList();
				loadHearingsToList();
				loadDeposToList();
				loadArbitrationsToList();
			}
		}

		var deposMinLength = 10;
		if( deposList.length < deposMinLength ) {
			for( var i = (deposMinLength - fDepositions.length); i > 0; --i ) {
				deposList.append( elementHTML );
			}
		}
	};

	this.startCustomSort = function( type )
	{
//		console.log( 'startCustomSort', type );
		if( inCustomSort ) {
			this.endCustomSort();
			$('.doneButton', contentBox).trigger( 'abortCustomSort' );
		}
		var options = {items:'.sortable', containment:'parent'};
		switch( type ) {
			case 'Depositions':
				inCustomSort = deposList;
				break;
			case 'Cases':
				inCustomSort = casesList;
				break;
			default:
				return;
		}
		inCustomSort.addClass( 'CustomSort' );
		inCustomSort.off();
		inCustomSort.sortable( options );
	};

	this.endCustomSort = function()
	{
//		console.log( 'endCustomSort' );
		$('.doneButton', contentBox).hide();
		if( inCustomSort === null ) {
			return;
		}
		inCustomSort.removeClass( 'CustomSort' );
		if( inCustomSort.hasClass( 'ui-sortable' ) ) {
			inCustomSort.sortable( 'destroy' );
		}
		if( inCustomSort === deposList ) {
			buildDeposList();
		} else if( inCustomSort === casesList ) {
			buildCasesList();
		}
		inCustomSort = null;
	};

	this.saveCustomSort = function( data )
	{
//		console.log( 'saveCustomSort', data );
		var sortElement;
		switch( data.sortObject ) {
			case 'Depositions':
				sortElement = deposList;
				break;
			case 'Cases':
				sortElement = casesList;
				break;
			default:
				return;
		}
		if( !sortElement.hasClass( 'ui-sortable') ) {
			return;
		}
		var list = sortElement.sortable( 'toArray', {"attribute":"data-id"} );
		var sortPosMap = [];
		var listMap = {};
		var sortPositions = [];
		if( inCustomSort === deposList ) {
			for( var d in fDepositions ) {
				var _depo = fDepositions[d];
				sortPosMap[d] = _depo.sortPos;
				listMap[_depo.ID] = _depo;
			}
			for( var i in list ) {
				var _depoID = parseInt( list[i] );
				var _depo = listMap[_depoID];
				var sortPos = sortPosMap[i];
//				console.log( _file.name, _file.ID, _file.sortPos, sortPos );
				_depo.sortPos = sortPos;
				var customSort = {};
				customSort[_depo.ID] = _depo.sortPos;
				sortPositions.push( customSort );
//				window.top.Datacore.addDeposition( _depo );
			}
			fDepositions = window.top.userSortDepos.sort( fDepositions );
		} else if( inCustomSort === casesList ) {
			for( var c in fCases ) {
				var _case = fCases[c];
				sortPosMap[c] = _case.sortPos;
				listMap[_case.ID] = _case;
			}
			for( var i in list ) {
				var _caseID = parseInt( list[i] );
				var _case = listMap[_caseID];
				var sortPos = sortPosMap[i];
//				console.log( _folder.name, _folder.ID, _folder.sortPos, sortPos );
				_case.sortPos = sortPos;
				var customSort = {};
				customSort[_case.ID] = _case.sortPos;
				sortPositions.push( customSort );
//				window.top.Datacore.addCase( _case );
			}
			fCases = window.top.userSortCases.sort( fCases );
		}

		$.post( '/users/setCustomSort', {"sortObject":data.sortObject, "list":sortPositions}, function( data, textStatus ) {
			if( textStatus === "success" ) {
				if( data !== null ) {
					console.log( 'setCusomSort -- success' );
					return;
				}
			}
			console.log( 'setCusomSort -- failure' );
		}, 'json' );
	};

	var userPrefs = window.top.webApp.getS( 'userPreferences' );
	if( typeof userPrefs !== 'undefined' && userPrefs !== null ) {
		var depoFilterOptions = ["All", "Scheduled", "Completed"];
		for( var i in userPrefs ) {
			var userPref = userPrefs[i];
			if( userPref.hasOwnProperty( 'prefKey' ) ) {
				if( userPref.prefKey === 'DepositionsFilter' ) {
					var depoFilterIdx = userPref.value;
					depoFilter = ((typeof depoFilterOptions[depoFilterIdx] !== 'undefined') ? depoFilterOptions[depoFilterIdx].toLowerCase() : "all");
				}
			}
		}
		delete depoFilterOptions;
	}
	delete userPrefs;
};

function documentReady()
{
	window.top.webApp.setS( 'inDeposition', null );
	if( typeof window.top.Social !== 'undefined' && window.top.Social !== null ) {
		window.top.Social.reset();
	}
	window.top.casesView = new CasesView();
	window.top.userSortCases = new window.top.UserSort( 'Cases', $('#sortCases'), window.top.casesView.sortCases, window.top.casesView );
	window.top.userSortDepos = new window.top.UserSort( 'Depositions', $('#sortDepos'), window.top.casesView.sortDepositions, window.top.casesView );
	$('#status_view_new').on( 'click', function() { window.top.casesView.selectDepoFilter( 'scheduled' ); } );
	$('#status_view_past').on( 'click', function() { window.top.casesView.selectDepoFilter( 'completed' ); } );

	window.top.navBar.allDisable();
	window.top.navBar.logoutButton.enable();
	window.top.navBar.joinButton.enable();
	window.top.navBar.showLogo();
	window.top.navBar.showWelcome();
	if( typeof window.top.tutorialVideos === 'object' && window.top.tutorialVideos !== null ) window.top.tutorialVideos.bind();

	<?php if( isset( $api_data_class ) && count( $api_data_class ) > 0 ): ?>
	window.top.Datacore.loadCases( <?= json_encode( $api_data_class ) ?> );
	<?php endif; ?>

	window.top.casesView.sortCases();

	window.onbeforeunload = function( e )
	{
		delete window.top.casesView;
		delete window.top.userSortCases;
		delete window.top.userSortDepos;
	};
	$('body').on( 'touchmove', function( event ) { window.top.webApp.touchHandlerHelper( event ); } );
}
window.top.webApp.onReady( documentReady );


<?php $this->Html->scriptEnd(); ?>
</script>


