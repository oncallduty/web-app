<?php
$this->Html->css( 'login', null, array( 'inline'=>false ) );
$this->Html->script( 'login', array( 'inline'=>false ) );

$data = $this->Js->get( '#UserWitnessForm' )->serializeForm( ['isForm'=>TRUE, 'inline'=>TRUE] );
$options = array(
	'data'=>$data,
	'async'=>TRUE,
	'dataExpression'=>TRUE,
	'method'=>'POST',
	'before'=>'$("#loading").fadeIn( 200 );',
	'complete'=>'console.log("complete WITNESS");$("#loading").hide();',
	'success'=>'witnessLoginSuccess(data,textStatus);',
	'error'=>'loginError(data,textStatus);',
);
$this->Js->get( '#UserWitnessForm' )->event( 'submit', $this->Js->request( ['action'=>'witness', 'controller'=>'users'], $options ) );

$data = $this->Js->get( '#UserGuestsForm' )->serializeForm( array( 'isForm'=>TRUE, 'inline'=>TRUE ) );
$options = array(
	'data'=>$data,
	'async'=>TRUE,
	'dataExpression'=>TRUE,
	'method'=>'POST',
	'before'=>'console.log("loading");$("#loading").fadeIn( 200 );',
	'complete'=>'console.log("complete GUEST");$("#loading").hide();',
	'success'=>'loginSuccess(data,textStatus);',
	'error'=>'loginError(data,textStatus);',
);
$this->Js->get( '#UserGuestsForm' )->event( 'submit', $this->Js->request( array( 'action'=>'guests', 'controller'=>'users' ), $options ) );

?>


<?=$this->element( 'dialog1btn' );?>
<?=$this->element( 'dialog3btn' );?>


<div id="loading">
	<div class="spinner">
		<img src="/img/ipad/loading.gif" width="48" height="48" border="0" />
		<div>Loading ...</div>
	</div>
</div>

<?=$this->element( 'navbar' );?>

<div class="wrapper">
<div class="center">
	<div class="container">
			<div class="centerpad1">
				<div>
					<div class="row">
						<div class="col-sm-6">
							<div class="lform-nonmembers">
							<div class="formhd-guest">Welcome Guest! </div>
							<div class="form-body-nonmembers">
							<div class="form-txt">Please login with the Session ID, Session passcode, your name and email account information.</div>
							<form action="/guests" id="UserGuestsForm" onsubmit="event.returnValue = false; return false;" method="post" accept-charset="utf-8">
							<div class="form-group" style="display:none;">
							<input class="form-control" type="hidden" name="_method" value="POST"></div>
							<div class="form-group">
							<label for="EDUserDepositionID">*Session ID:</label>
							<input class="form-control" placeholder="Insert Your Session ID" name="data[EDUser][depositionID]" required="required" oninvalid="setCustomValidity('Deposition ID is required.')" onchange="try{setCustomValidity('')}catch(e){}" type="text" id="EDUserDepositionID"></div>
							<div class="form-group">
								<label for="EDUserDepositionPasscode">*Session Passcode:</label>
								<input class="form-control" placeholder="Insert Your Session Passcode" name="data[EDUser][depositionPasscode]" required="required" oninvalid="setCustomValidity('Deposition Passcode is required.')" onchange="try{setCustomValidity('')}catch(e){}" type="password" id="EDUserDepositionPasscode"></div>
								<div class="form-group">
									<label for="EDUserName">*Name:</label>
									<input class="form-control" placeholder="Type your Name" name="data[EDUser][name]" required="required" oninvalid="setCustomValidity('Please enter your name. Blank names are not allowed.')" onchange="try{setCustomValidity('')}catch(e){}" pattern="^.*[\S]+.*$" type="text" id="EDUserName"></div>
									<div class="form-group">
										<label for="EDUserEmail">*Email:</label>
										<input class="form-control" placeholder="Type your Email" name="data[EDUser][email]" required="required" oninvalid="setCustomValidity('Please enter your email address.')" onchange="try{setCustomValidity('')}catch(e){}" type="email" id="EDUserEmail"></div>
										<div class="lformbtn_nonmember">
											<a href="/guests">
												<button class="btn nonmember-btn-log" type="submit">Login</button>
											</a>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="jform-nonmembers">
								<div class="formhd-witness">Welcome Witness! </div>
								<div class="form-body-nonmembers">
									<div class="form-txt">Please login with the Session ID.</div>
									<form action="/witness" id="UserWitnessForm" onsubmit="event.returnValue = false; return false;" method="post" accept-charset="utf-8">
										<div class="form-group" style="display:none;">
											<input class="form-control" type="hidden" name="_method" value="POST"></div>
											<div class="form-group">
												<label for="EDUserDepositionID">*Session ID:</label>
												<input class="form-control" placeholder="Insert Your Session ID" name="data[EDUser][depositionID]" required="required" oninvalid="setCustomValidity('Session ID is required.')" onchange="try{setCustomValidity('')}catch(e){}" type="text" value="" id="EDUserDepositionID"></div>
												<div class="form-group">
													<label for="witnessName">*Name:</label>
													<input class="form-control" placeholder="Type your Name" name="data[EDUser][name]" id="witnessName" required="required" oninvalid="setCustomValidity('Please enter your name.')" onchange="try{setCustomValidity('')}catch(e){}" pattern="^.*[\S]+.*$" type="text"></div>
													<div class="jformbtn_nonmember">
														<a href="/witness">
															<button class="btn nonmember-btn-log" type="submit">Login</button>
														</a>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
						</div>
						</div>
					</div>
				</div>
						
						
			</div>
	</div>
<div>

<div class="passcodeLogins">	<form id="passcodeLogin" name="passcodeLogin" onsubmit="event.returnValue = false; return false;">
		<input id="depositionID" name="data[EDUser][depositionID]" type="hidden"/>
		<input id="loginClass" name="data[EDUser][loginClass]" type="hidden"/>
		<input id="skipPasscode" name="data[EDUser][skipPasscode]" type="hidden"/>

		<div id="witnessPrepLoginDialog" class="appDialog" style="display:none;">
			<div class="popupDialog small" style="top:188px;height:300px;background-size:100% 100%;">
				<div class="popupContents">
					<h4 style="padding-top:0;">Enter Passcode</h4>
					<div class="dialogMsg">
						<p>Enter the passcode to enter as a trusted witness</p>
						<p>with access to all documents.</p>
						<p>Select Skip to enter as a normal witness.</p>
						<div style="margin-top:20px;">
							<input id="wpPasscode" class="passcodeInput" name="data[EDUser][wpPasscode]" type="password" oninput="updateWPLoginButtons()" onkeypress="updateWPLoginButtons()" onkeyup="updateWPLoginButtons()"/>
						</div>
					</div>
				</div>
				<div class="dialogBtnWrap">
					<div class="app_bttn" onclick="dismissWPLoginDialog()"><span>Cancel</span></div>
					<div id="wpLoginBtnSkip" class="app_bttn_new" onclick="$('#passcodeLogin').submit();"><span>Skip</span></div>
					<div id="wpLoginBtnContinue" class="app_bttn_new" onclick="$('#passcodeLogin').submit();" style="display:none;"><span>Continue</span></div>
				</div>
			</div>
		</div>
		<div id="trialBinderLoginDialog" class="appDialog" style="display:none;">
			<div class="popupDialog small" style="top:188px;height:300px;background-size:100% 100%;">
				<div class="popupContents">
					<h4 style="padding-top:0;">Session Passcode</h4>
					<div class="dialogMsg">
						<p>Enter the Session Passcode to enter this session.</p>
						<p>Or select Skip if you do not have a passcode.</p>
						<div style="margin-top:20px;">
							<input id="tbPasscode" class="passcodeInput" name="data[EDUser][tbPasscode]" type="password" oninput="updateTBLoginButtons()" onkeypress="updateTBLoginButtons()" onkeyup="updateTBLoginButtons()"/>
						</div>
					</div>
				</div>
				<div class="dialogBtnWrap">
					<div class="app_bttn" onclick="dismissTBLoginDialog()"><span>Cancel</span></div>
					<div id="tbLoginBtnSkip" class="app_bttn_new" onclick="$('#passcodeLogin').submit();"><span>Skip</span></div>
					<div id="tbLoginBtnContinue" class="app_bttn_new" onclick="$('#passcodeLogin').submit();" style="display:none;"><span>Continue</span></div>
				</div>
			</div>
		</div>
	</form>
</div>

<div class="clear"></div>
<div id="member_messaging"></div>

<footer class="front-footer text-center">
	<section class="ts-copy-right">
	    <div class="container">
		<div class="row">
		    <div class="col-lg-4 col-md-4 col-sm-4">
			<p class="copyright">Copyright &copy; <?=date('Y')?> eDepoze,LLC. </p>           </div>
		    <div class="col-lg-8 col-md-8 col-sm-8">
		      <div id="footer-logo" class="footer-logo" style="display:block;"></div>
		    </div>
		</div>
	    </div>
	</section>			   
</footer>

<?php
$this->Html->script( 'datacore', ['inline'=>FALSE] );
$this->Html->script( "{$nodeJS}socket.io/socket.io.js", ['inline'=>FALSE] );
$this->Html->script( 'sockets', ['inline'=>FALSE] );
?>
<script type="text/javascript">
//witness
<?php $this->Html->scriptStart( ['inline'=>FALSE] ); ?>
window.top.webApp.removeAllItems();
window.top.nodeURL = '<?=$nodeJS;?>';
function dismissWPLoginDialog() {
	$('#witnessPrepLoginDialog').fadeOut( 200, function() {
		$('#wpPasscode').val('');
		updateWPLoginButtons();
	} );
}
function updateWPLoginButtons() {
	if( $('#wpPasscode').val().length > 0 ) {
		if( $('#wpLoginBtnSkip').is(':visible') ) {
			$('#wpLoginBtnSkip').hide();
			$('#wpLoginBtnContinue').show();
		}
	} else {
		$('#wpLoginBtnSkip').show();
		$('#wpLoginBtnContinue').hide();
	}
}
function dismissTBLoginDialog() {
	$('#trialBinderLoginDialog').fadeOut( 200, function() {
		$('#tbPasscode').val('');
		updateTBLoginButtons();
	} );
}
function updateTBLoginButtons() {
	if( $('#tbPasscode').val().length > 0 ) {
		if( $('#tbLoginBtnSkip').is(':visible') ) {
			$('#tbLoginBtnSkip').hide();
			$('#tbLoginBtnContinue').show();
		}
	} else {
		$('#tbLoginBtnSkip').show();
		$('#tbLoginBtnContinue').hide();
	}
}
function witnessLoginReady()
{
//	console.log( 'witnessLogin::onReady' );
	$("#UserWitnessForm input[name='data[EDUser][depositionID]']").focus();
	$("#passcodeLogin").bind( "submit", function( event ) {
		$("#depositionID").val( $("#EDUserDepositionID").val() );
		var wpPasscode = $("#wpPasscode").val();
		var tbPasscode = $("#tbPasscode").val();
		var skipPasscode;
		if( $("#loginClass").val() === "Witness Prep" ) {
			skipPasscode = (wpPasscode.length > 0 ? 0 : 1);
		} else {
			skipPasscode = (tbPasscode.length > 0 ? 0 : 1);
		}
		$("#skipPasscode").val( skipPasscode );
		$("#loading").fadeIn( 200 );
		$('#witnessPrepLoginDialog').hide();
		$('#trialBinderLoginDialog').hide();
		$.ajax( {
			async:true,
			complete:function( XMLHttpRequest, textStatus ) { $("#loading").hide(); },
			data:$("#passcodeLogin").serialize(),
			error:function( XMLHttpRequest, textStatus, errorThrown ) { console.log( 'error', XMLHttpRequest, textStatus, errorThrown ); },
			success:function( data, textStatus ) { witnessLoginSuccess( data, textStatus ); },
			type:"POST",
			url:"/witness"
		} );
		return false;
	} );
}
top.webApp.onReady( witnessLoginReady );
<?php $this->Html->scriptEnd(); ?>
</script>