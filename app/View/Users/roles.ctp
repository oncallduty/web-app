
<script>window.currentDomain = '<?=Configure::read('SiteConfig')['domain']?>';</script>

<?=$this->Html->script( 'jquery-2.2.4.min' ); ?>
<?=$this->Html->script( 'jquery.mb.browser' ); ?>
<?=$this->Html->script( 'webapp.js' ); ?>
<?=$this->Html->css( 'app' ); ?>
<?=$this->fetch( 'css' ); ?>
<?=$this->element( 'dialog1btn' );?>




		<div class="centerpad">
			<div class="col-sm-8">
				<div class="row">
					<div class="col-sm-6">
						<div class="lbox">
							<img src="asset/images/login.png" />
							<div class="nmtxt">
								If you have an eDepoze account, click below to go to the login page.
							</div>
							<a href="/members"><button class="btn btn-login" type="button">Login</button></a>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="jbox">
							<img src="asset/images/join.png" />
							<div class="nmtxt">
								If you don't have an eDepoze account and need to join a session as a guest or witness, click here to join
							</div>
							<a href="/nonmembers"><button class="btn btn-join" type="button">Join</button></a>
						</div>
					</div>
				</div>
			</div>
		</div>

	<div id="signin_instructions"></div>
<script type="text/javascript">
//<![CDATA[
window.top.webApp.removeAllItems();

$(document).ready( function() {
	var isCompat = false;
	var isLatest = false;
	if( jQuery.browser.chrome ) {
		isCompat = (jQuery.browser.majorVersion >= 30);
		isLatest = (jQuery.browser.majorVersion >= 40);
	} else if( jQuery.browser.mozilla ) {
		isCompat = (jQuery.browser.majorVersion >= 30);
		isLatest = (jQuery.browser.majorVersion >= 38);
	} else if( jQuery.browser.safari ) {
		isLatest = (jQuery.browser.majorVersion >= 9);
		isCompat = (jQuery.browser.majorVersion >= 6);
	} else if( jQuery.browser.msie ) {
		isCompat = (jQuery.browser.majorVersion >= 10);
		isLatest = jQuery.browser.webkit ? (jQuery.browser.majorVersion >= 12) : (jQuery.browser.majorVersion >= 11);
	}
	if( !isLatest && isCompat ) {
		showWarningDialog();
	} else if( !isCompat ) {
		$('#signin_options a').attr('href','javascript:showErrorDialog();');
		showErrorDialog();
	}
} );

function showWarningDialog() {
	var msg = 'You are currently using ' + jQuery.browser.name + ' version ' + jQuery.browser.majorVersion + '. It is recommeded to upgrade to a newer version.<br/>Please consider updating your browser.';
	if( typeof console !== 'undefined' ) {
		console.log( msg );
	}
	showDialog1Btn( 'WARNING', msg, 'Continue' );
}

function showErrorDialog() {
	var msg = 'You are currently using ' + jQuery.browser.name + ' version ' + jQuery.browser.majorVersion + '. It is required to upgrade to a newer version.<br/>You must update your browser to continue.';
	if( typeof console !== 'undefined' ) {
		console.log( msg );
	}
	showDialog1Btn( 'ERROR', msg, 'Close' );
}
//]]>
</script>


