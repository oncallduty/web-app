<?php
$this->Html->css( 'login', NULL, ['inline'=>FALSE] );
$this->Html->script( 'login', ['inline'=>FALSE] );

$data = $this->Js->get( '#UserWitnessForm' )->serializeForm( ['isForm'=>TRUE, 'inline'=>TRUE] );
$options = array(
	'data'=>$data,
	'async'=>TRUE,
	'dataExpression'=>TRUE,
	'method'=>'POST',
	'before'=>'$("#loading").fadeIn( 200 );',
	'complete'=>'$("#loading").hide();',
	'success'=>'witnessLoginSuccess(data,textStatus);',
	'error'=>'loginError(data,textStatus);',
);
$this->Js->get( '#UserWitnessForm' )->event( 'submit', $this->Js->request( ['action'=>'witness', 'controller'=>'users'], $options ) );
?>
<div id="loading">
	<div class="spinner">
		<img src="/img/ipad/loading.gif" width="48" height="48" border="0" />
		<div>Loading ...</div>
	</div>
</div>
<?=$this->element( 'navbar' );?>
<div id="login_welcome_wrap">
	<h1>Welcome Witness!</h1>
	<div>
		Please login with the Session ID.
	</div>
</div>
<div id="login_form_wrap">
	<?=$this->Form->create( 'User', ['default'=>false] );?>
	<?=$this->Form->input( 'EDUser.depositionID', [
		'label'=>'*Session ID:',
		'default'=>$sessionID,
		'required',
		'oninvalid'=>"setCustomValidity('Session ID is required.')",
		'onchange'=>"try{setCustomValidity('')}catch(e){}"
	] );?>
	<?= $this->Form->input( 'EDUser.name', [
		'label'=>'*Name:',
		'id'=>'witnessName',
		'required',
		'oninvalid'=>"setCustomValidity('Please enter your name.')",
		'onchange'=>"try{setCustomValidity('')}catch(e){}",
		'pattern'=>'^.*[\S]+.*$',
	] ); ?>
	<?=$this->Form->end( 'Login' );?>
</div>
<div class="clear"></div>
<div id="ipad_footer">
	<?=$this->element( 'copyright' );?>
</div>
<div class="passcodeLogins">
	<form id="passcodeLogin" name="passcodeLogin" onsubmit="event.returnValue = false; return false;">
		<input id="depositionID" name="data[EDUser][depositionID]" type="hidden"/>
		<input id="loginClass" name="data[EDUser][loginClass]" type="hidden"/>
		<input id="skipPasscode" name="data[EDUser][skipPasscode]" type="hidden"/>

		<div id="witnessPrepLoginDialog" class="appDialog" style="display:none;">
			<div class="popupDialog small" style="top:188px;height:300px;background-size:100% 100%;">
				<div class="popupContents">
					<h4 style="padding-top:0;">Enter Passcode</h4>
					<div class="dialogMsg">
						<p>Enter the passcode to enter as a trusted witness</p>
						<p>with access to all documents.</p>
						<p>Select Skip to enter as a normal witness.</p>
						<div style="margin-top:20px;">
							<input id="wpPasscode" class="passcodeInput" name="data[EDUser][wpPasscode]" type="password" oninput="updateWPLoginButtons()" onkeypress="updateWPLoginButtons()" onkeyup="updateWPLoginButtons()"/>
						</div>
					</div>
				</div>
				<div class="dialogBtnWrap">
					<div class="app_bttn" onclick="dismissWPLoginDialog()"><span>Cancel</span></div>
					<div id="wpLoginBtnSkip" class="app_bttn_new" onclick="$('#passcodeLogin').submit();"><span>Skip</span></div>
					<div id="wpLoginBtnContinue" class="app_bttn_new" onclick="$('#passcodeLogin').submit();" style="display:none;"><span>Continue</span></div>
				</div>
			</div>
		</div>
		<div id="trialBinderLoginDialog" class="appDialog" style="display:none;">
			<div class="popupDialog small" style="top:188px;height:300px;background-size:100% 100%;">
				<div class="popupContents">
					<h4 style="padding-top:0;">Session Passcode</h4>
					<div class="dialogMsg">
						<p>Enter the Session Passcode to enter this session.</p>
						<p>Or select Skip if you do not have a passcode.</p>
						<div style="margin-top:20px;">
							<input id="tbPasscode" class="passcodeInput" name="data[EDUser][tbPasscode]" type="password" oninput="updateTBLoginButtons()" onkeypress="updateTBLoginButtons()" onkeyup="updateTBLoginButtons()"/>
						</div>
					</div>
				</div>
				<div class="dialogBtnWrap">
					<div class="app_bttn" onclick="dismissTBLoginDialog()"><span>Cancel</span></div>
					<div id="tbLoginBtnSkip" class="app_bttn_new" onclick="$('#passcodeLogin').submit();"><span>Skip</span></div>
					<div id="tbLoginBtnContinue" class="app_bttn_new" onclick="$('#passcodeLogin').submit();" style="display:none;"><span>Continue</span></div>
				</div>
			</div>
		</div>
	</form>
</div>
<?php
$this->Html->script( 'datacore', ['inline'=>FALSE] );
$this->Html->script( "{$nodeJS}socket.io/socket.io.js", ['inline'=>FALSE] );
$this->Html->script( 'sockets', ['inline'=>FALSE] );
?>
<script type="text/javascript">
//witness
<?php $this->Html->scriptStart( ['inline'=>FALSE] ); ?>
window.top.webApp.removeAllItems();
window.top.nodeURL = '<?=$nodeJS;?>';
function dismissWPLoginDialog() {
	$('#witnessPrepLoginDialog').fadeOut( 200, function() {
		$('#wpPasscode').val('');
		updateWPLoginButtons();
	} );
}
function updateWPLoginButtons() {
	if( $('#wpPasscode').val().length > 0 ) {
		if( $('#wpLoginBtnSkip').is(':visible') ) {
			$('#wpLoginBtnSkip').hide();
			$('#wpLoginBtnContinue').show();
		}
	} else {
		$('#wpLoginBtnSkip').show();
		$('#wpLoginBtnContinue').hide();
	}
}
function dismissTBLoginDialog() {
	$('#trialBinderLoginDialog').fadeOut( 200, function() {
		$('#tbPasscode').val('');
		updateTBLoginButtons();
	} );
}
function updateTBLoginButtons() {
	if( $('#tbPasscode').val().length > 0 ) {
		if( $('#tbLoginBtnSkip').is(':visible') ) {
			$('#tbLoginBtnSkip').hide();
			$('#tbLoginBtnContinue').show();
		}
	} else {
		$('#tbLoginBtnSkip').show();
		$('#tbLoginBtnContinue').hide();
	}
}
function witnessLoginReady()
{
//	console.log( 'witnessLogin::onReady' );
	$("#UserWitnessForm input[name='data[EDUser][depositionID]']").focus();
	$("#passcodeLogin").bind( "submit", function( event ) {
		$("#depositionID").val( $("#EDUserDepositionID").val() );
		var wpPasscode = $("#wpPasscode").val();
		var tbPasscode = $("#tbPasscode").val();
		var skipPasscode;
		if( $("#loginClass").val() === "Witness Prep" ) {
			skipPasscode = (wpPasscode.length > 0 ? 0 : 1);
		} else {
			skipPasscode = (tbPasscode.length > 0 ? 0 : 1);
		}
		$("#skipPasscode").val( skipPasscode );
		$("#loading").fadeIn( 200 );
		$('#witnessPrepLoginDialog').hide();
		$('#trialBinderLoginDialog').hide();
		$.ajax( {
			async:true,
			complete:function( XMLHttpRequest, textStatus ) { $("#loading").hide(); },
			data:$("#passcodeLogin").serialize(),
			error:function( XMLHttpRequest, textStatus, errorThrown ) { console.log( 'error', XMLHttpRequest, textStatus, errorThrown ); },
			success:function( data, textStatus ) { witnessLoginSuccess( data, textStatus ); },
			type:"POST",
			url:"/witness"
		} );
		return false;
	} );
}
top.webApp.onReady( witnessLoginReady );
<?php $this->Html->scriptEnd(); ?>
</script>
