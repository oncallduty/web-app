<?=$this->element( 'loadingOverlay' );?>
<?=$this->element( 'dialogFileShare' );?>
<?=$this->element( 'navbar' );?>
<?=$this->element( 'tutorial' );?>
<?=$this->element( 'sendFileForm' );?>
<?php if( $userType !== 'W' ): echo $this->element( 'social' ); endif; ?>

<div>
	<iframe name="ipad_container_frame" id="ipad_container_frame" src="" scrolling="no" class="hideSocialList <?=AppHelper::getContainerClasses();?>"></iframe>
</div>

<script>
var authUserLog = '<?=$authUserLog;?>';
</script>
<?php $this->Html->script( 'datacore', ['inline'=>FALSE] ); ?>
<?php $this->Html->script( "{$nodeJS}socket.io/socket.io.js", ['inline'=>FALSE] ); ?>
<?php $this->Html->script( 'sockets', ['inline'=>FALSE] ); ?>
<?php $this->Html->script( 'usersort', ['inline'=>FALSE] ); ?>
<?php $this->Html->script( 'contentsearch', ['inline'=>FALSE] ); ?>
<?php $this->Html->script( 'mark.min', ['inline'=>FALSE] ); ?>
<?php $this->Html->script( 'livetranscript', ['inline'=>FALSE] ); ?>

<script type="text/javascript">
<?php $this->Html->scriptStart( ['inline' => FALSE] ); ?>
//window.document.domain = "edepoze.com";
//window.document.domain = "armyargentina.com";
window.document.domain = "<?=Configure::read('SiteConfig')['domain'];?>";

Datacore.s.nodeURL = '<?=$nodeJS;?>';
Datacore.s.sKey = '<?=$sKey;?>';
Datacore.s.userType = '<?=$userType;?>';
top.webApp.setS( 'sortPreferences', <?=json_encode( $sortPreferences );?> );
top.webApp.setS( 'userPreferences', <?=json_encode( $userPreferences );?> );
Datacore.s.currentUserID = parseInt( <?=intval( $userID );?> );
Datacore.addUser( <?=json_encode( $authUser );?> );

var requestedFileID = null;

function framesetReady()
{
//	console.log( 'frameset::onReady' );
	$('#ipad_container_frame').attr( 'src', '<?=$frameSrc;?>' );
	top.webApp.setS( 'user', top.Datacore.getUser( <?=intval( $userID );?> ) );
}
top.webApp.onReady( framesetReady );

var redirect = function()
{
	$('#ipad_container_frame').attr( 'src', '/depositions' );
};

var resetredirect = function() {
	$('#ipad_container').removeClass( 'viewer' );
	$('#ipad_container_frame').attr( 'src', '/cases' );
};
<?php $this->Html->scriptEnd(); ?>
</script>
