<?php
$this->Html->css( ['login'], NULL, ['inline'=>FALSE] );
$this->Html->script( 'login', ['inline'=>FALSE] );

$this->Js->get( '#PasswordResetForm' )->event( 'submit', $this->Js->request( ['controller'=>'users', 'action'=>'reset'], [
	'data' => $this->Js->get( '#PasswordResetForm' )->serializeForm( ['isForm'=>TRUE, 'inline'=>TRUE] ),
	'async' => TRUE,
	'dataExpression' => TRUE,
	'method' => 'POST',
	'before' => '$("#loading").fadeIn( 200 );',
	'complete' => '$("#loading").hide();',
	'success' => 'resetSuccess(data,textStatus);',
	'error' => 'resetError(data,textStatus);',
] ) );
?>

<head>
<link href="../asset/css/custom-admin.css" rel="stylesheet" type="text/css">
<link href="../asset/css/AdminLTE.min.css" rel="stylesheet" type="text/css">

</head>


<div id="loading">
	<div class="spinner">
		<img src="/img/ipad/loading.gif" width="48" height="48" border="0" />
		<div>Loading ...</div>
	</div>
</div>

<?=$this->element( 'navbar' );?>

<div class="wrapper">

  
  <div class="center">
  
	  <div class="container">
		<div class="centerpad4">
			<div class="forgot-lform">
				<div class="formhd">Password</div>
				<div class="form-body-members">
					<div class="form-group">
						Enter your username below and a temporary password will be sent to you shortly.
					</div>
					
						<form action="/reset" id="PasswordResetForm" onsubmit="event.returnValue = false; return false;" method="post" accept-charset="utf-8">
						<div style="display:none;">
						<input class="form-control" type="hidden" name="_method" value="POST">
						</div>
						<div class="form-group"><label for="EDUserUsername">Username:</label>

						

						<input class="form-control" placeholder="Type your Username" name="data[EDUser][username]" required="required" oninvalid="setCustomValidity('Username is required.')" onchange="try{setCustomValidity('')}catch(e){}" type="text" id="EDUserUsername">
						
						</div>	
						

						<div class="lformbtn_uname">
						<a href="/reset"><button class="btn btn-frgt-uname" type="button">Submit</button></a>
						</div>
						</form>
					
				</div>


				
			</div>
		</div>
	  </div>
  </div>


</div>
<div class="clear"></div>
<footer class="front-footer text-center">
			<section class="ts-copy-right">
			    <div class="container">
				<div class="row">
				    <div class="col-lg-4 col-md-4 col-sm-4">
					<p>Copyright 2020 eDepoze, LLC.</p>            </div>
				    <div class="col-lg-8 col-md-8 col-sm-8">
				      <div id="footer-logo" class="footer-logo" style="display:block;"></div>
				    </div>
				</div>
			    </div>
			</section>			   
</footer>
<!--div id="ipad_footer">
	<?=$this->element( 'copyright' );?>
</div-->



