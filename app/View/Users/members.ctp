<?php
$this->Html->css( 'login', null, array( 'inline'=>false ) );
$this->Html->script( 'login', array( 'inline'=>false ) );
$this->Html->script( 'sha1', array( 'inline'=>false ) );

$data = $this->Js->get( '#UserMembersForm' )->serializeForm( array( 'isForm'=>TRUE, 'inline'=>TRUE ) );
$options = array(
	'data'=>$data,
	'async'=>TRUE,
	'dataExpression'=>TRUE,
	'method'=>'POST',
	'before'=>'console.log("loading");$("#loading").fadeIn( 200 );',
	'complete'=>'console.log("complete");$("#loading").hide();',
	'success'=>'loginSuccess(data,textStatus);',
	'error'=>'loginError(data,textStatus);',
);
$this->Js->get( '#UserMembersForm' )->event( 'submit', $this->Js->request( array( 'action'=>'members', 'controller'=>'users' ), $options ) );
?>
<?=$this->element( 'dialog1btn' );?>
<?=$this->element( 'dialog3btn' );?>

<div id="loading">
	<div class="spinner">
		<img src="/img/ipad/loading.gif" width="48" height="48" border="0" />
		<div>Loading ...</div>
	</div>
</div>
<?=$this->element( 'navbar' );?>

<div class="wrapper">  
  <div class="center">  
	  <div class="container">
		<div class="centerpad">
			<div class="lform">
				<div class="formhd">Welcome Member! </div>
				<div class="form-body-members">
					<div class="form-txt">
						Please login with your Username and Password to access your account.
					</div>
					<form action="/members" id="UserMembersForm" onsubmit="event.returnValue = false; return false;" method="post" accept-charset="utf-8">
					<div style="display:none;">
					<input type="hidden" name="_method" value="POST"></div>

					<div class="form-group"><label for="EDUserUsername">*Username:</label><input class="form-control" placeholder="Type your Username" name="data[EDUser][username]" required="required" oninvalid="setCustomValidity('Username is required.')" onchange="try{setCustomValidity('')}catch(e){}" type="text" id="EDUserUsername"></div>	

					<div class="form-group"><label for="EDUserPasscode">*Password:</label><input class="form-control" placeholder="Type your Password" name="data[EDUser][passcode]" required="required" oninvalid="setCustomValidity('Please enter your password.')" onchange="try{setCustomValidity('')}catch(e){}" type="password" id="EDUserPasscode"></div>
					<div class="separator">Login to Active session</div>
					<div class="form-txt">
						Use the Session ID and Passcode to enter an active session.
					</div>					
					<div class="form-group"><label for="EDUserDepositionID">Session ID:</label><input class="form-control" placeholder="Insert Your Session ID" name="data[EDUser][depositionID]" type="text" id="EDUserDepositionID"></div>

					<div class="form-group"><label for="EDUserDepositionPasscode">Session Passcode:</label><input class="form-control" placeholder="Insert Your Session Passcode" name="data[EDUser][depositionPasscode]" type="password" id="EDUserDepositionPasscode"></div>	

					

					<input type="hidden" name="data[EDUser][password]" id="EDUserPassword">

					<div class="lformbtn">
						<a href="/reset"><button class="btn btn-frgt" type="button">Forgot Password?</button></a>
						<a href="/members"><button class="btn member-btn-log" type="submit">Login</button></a>
					</div>
					
					</form>
				</div>


				
			</div>
		</div>
	  </div>
  
  




  </div>

<div class="clear"></div>
<div id="member_messaging"><?= $this->Session->flash( 'memberMsg' ) ?></div>

</div>

<script type="text/javascript">
//members
<?php $this->Html->scriptStart( ['inline'=>FALSE] ); ?>
window.top.webApp.removeAllItems();

$('#UserMembersForm').submit( function() {
	$('#EDUserPassword').val( sha1( $('#EDUserPasscode').val() ) );
	$('#EDUserPasscode').val( '' );
} );

$(document).ready( function() {
	var messenger = $('#member_messaging');
	if (messenger.html().length > 0) {
		messenger.fadeIn(500).delay(3600).fadeOut(500);
	}
} );
<?php $this->Html->scriptEnd(); ?>
</script>