<?php
$this->Html->css( 'login', null, array( 'inline'=>false ) );
$this->Html->script( 'login', array( 'inline'=>false ) );

$data = $this->Js->get( '#UserGuestsForm' )->serializeForm( array( 'isForm'=>TRUE, 'inline'=>TRUE ) );
$options = array(
	'data'=>$data,
	'async'=>TRUE,
	'dataExpression'=>TRUE,
	'method'=>'POST',
	'before'=>'console.log("loading");$("#loading").fadeIn( 200 );',
	'complete'=>'console.log("complete");$("#loading").hide();',
	'success'=>'loginSuccess(data,textStatus);',
	'error'=>'loginError(data,textStatus);',
);
$this->Js->get( '#UserGuestsForm' )->event( 'submit', $this->Js->request( array( 'action'=>'guests', 'controller'=>'users' ), $options ) );
?>
<div id="loading">
	<div class="spinner">
		<img src="/img/ipad/loading.gif" width="48" height="48" border="0" />
		<div>Loading ...</div>
	</div>
</div>
<?=$this->element( 'navbar' );?>
<div id="login_welcome_wrap">
	<h1>Welcome Guest!</h1>
	<div>
		Please login with the Session ID, Session passcode, your name and email account information.
	</div>
</div>
<div id="login_form_wrap">
	<?= $this->Form->create( 'User', ['default'=>false] ); ?>
	<?= $this->Form->input( 'EDUser.depositionID', [
		'label'=>'*Session ID:',
		'required',
		'oninvalid'=>"setCustomValidity('Deposition ID is required.')",
		'onchange'=>"try{setCustomValidity('')}catch(e){}"
	] ); ?>
	<?= $this->Form->input( 'EDUser.depositionPasscode', [
		'label'=>'*Session Passcode:',
		'type'=>'password',
		'required',
		'oninvalid'=>"setCustomValidity('Deposition Passcode is required.')",
		'onchange'=>"try{setCustomValidity('')}catch(e){}"
	] ); ?>
	<?= $this->Form->input( 'EDUser.name', [
		'label'=>'*Name:',
		'required',
		'oninvalid'=>"setCustomValidity('Please enter your name. Blank names are not allowed.')",
		'onchange'=>"try{setCustomValidity('')}catch(e){}",
		'pattern'=>'^.*[\S]+.*$',
	] ); ?>
	<?= $this->Form->input( 'EDUser.email', [
		'label'=>'*Email:',
		'required',
		'oninvalid'=>"setCustomValidity('Please enter your email address.')",
		'onchange'=>"try{setCustomValidity('')}catch(e){}"
	] ); ?>
	<?= $this->Form->end( 'Login' ); ?>
</div>
<div class="clear"></div>
<div id="ipad_footer">
	<?=$this->element( 'copyright' );?>
</div>
<script type="text/javascript">
//guests
<?php $this->Html->scriptStart( ['inline'=>FALSE] ); ?>
window.top.webApp.removeAllItems();
<?php $this->Html->scriptEnd(); ?>
</script>