/* global Datacore */

var ATTENDEE_IDENTIFIER = 'attendee_';

function getAllAttendeesID() {
	return Datacore.getPrimaryDepositionID()+'.deposition';
}

function getPrivateUnreadMessageCount( groupID ) {
	var chatroomUnreadTally = 0;

	if (Datacore.hasRoom( groupID )) {
		var group = Datacore.getRoom( groupID );
		if (group) {
			var memberIDs = group.getMemberIDs();
			for (var mid in memberIDs) {
				if (memberIDs[mid] === Datacore.getS('currentUserID')) {
					continue;
				}
				var roomName = (memberIDs[mid] > Datacore.getS('currentUserID')) ? Datacore.getS('currentUserID')+'.'+memberIDs[mid] : memberIDs[mid]+'.'+Datacore.getS('currentUserID');
				var chatroom = Datacore.getRoom( Datacore.getPrimaryDepositionID()+'.private.'+roomName );
				chatroomUnreadTally += (isset( chatroom ) ? chatroom.unreadMessageCount : 0);
			}
		}
	}

	return chatroomUnreadTally;
}

function selectGroupMember() {
	var listitem = $(this).parent();
	if (listitem.hasClass( 'selected' )) {
		listitem.removeClass( 'selected' );
	} else {
		listitem.addClass( 'selected' );
	}
}

var Social = {
	currentGroup : null,
	currentRoom : null,
	chatHistoryHeightBig : null,
	chatHistoryHeightSmall : null,
	mouseDownTimerID: null,
	transcriptCount : 1,
	transcriptAtBottom : true,
	chatScrollPos: 0,

	showAttendeeList : function() {
//		$('#ipad_container').addClass( 'showSocial' );
		$('#social_list_titlebar #users_group_edit_cancel').hide();
		$('#social_list_titlebar #users_group_add').show();
		$('#social_list_titlebar .social_list_nav_title').text( 'Attendees' );

		Datacore.s.editorGroupID = null;
		$('#social_group_edit #group_name_edit').val( '' );

		Social.drawAttendeeList();

		$('#social_group_edit').hide();
		$('#social_attendee_list').show();
	},

	showGroupEdit : function( groupID ) {
		// show the Group Creator if groupID is not defined, otherwise show the Group Editor
		Datacore.s.editorGroupID = (typeof groupID !== 'undefined' && Datacore.hasRoom( groupID )) ? groupID : null;

		// collapse any open groups and any open chats
		$('#social_attendee_list .toggle').addClass( 'collapsed' );
		$('#social_attendee_list ul').hide();
		Social.hideChat();

		$('#social_attendee_list').hide();

		var groupMemberIDs = [];

		if ( Datacore.hasRoom( Datacore.getS('editorGroupID') ) ) {
			var Group = Datacore.getRoom( Datacore.getS('editorGroupID') );

			var titleText = 'Edit Group';

			$('#group_name_edit').val( Group.title );
			if ($('#group_name_edit_container #group_name_edit').hasClass( 'transparent' )) {
				$('#group_name_edit_container #group_name_edit').removeClass( 'transparent' );
			}
			$('#group_name_edit_container #clear-input').show();

			if ($('#group_attendee_list').hasClass( 'inactive' )) {
				$('#group_attendee_list').removeClass( 'inactive' );
			}

			if ($('#group_form_action #group_form_action_save').hasClass( 'inactive' )) {
				Social.enableGroupSaveButton();
			}

			if ($('#group_form_action #group_form_action_delete').hasClass( 'inactive')) {
				Social.enableGroupDeleteButton();
			}
			$('#group_form_action #group_form_action_delete').show();

			groupMemberIDs = Group.getMemberIDs();
		} else {
			titleText = 'Create Group';

			$('#social_list_titlebar #users_group_add').hide();

			if (!$('#group_attendee_list').hasClass( 'inactive' )) {
				$('#group_attendee_list').addClass( 'inactive' );
			}

			if (!$('#group_form_action #group_form_action_save').hasClass( 'inactive' )) {
				Social.disableGroupSaveButton();
			}

			Social.disableGroupDeleteButton();
			$('#group_form_action #group_form_action_delete').hide();
		}

		$('#group_attendee_list').empty();

		// generate list of Attendees to select
		var attendees_list = '';

		var allAttRoom = Datacore.getRoom( webApp.getS( 'inDeposition' ) + '.deposition' );
		if( typeof allAttRoom === 'undefined' || allAttRoom === null ) {
			return;
		}
		var depositionLeaderID = window.top.webApp.getSpeakerID();
		var users = Datacore.getUsers( allAttRoom.getMemberIDs() );
		users.sort( function( a, b ) {
//			return (a.name > b.name ? 1 : (a.name < b.name ? -1 : 0));
			if( a === null || b === null ) {
				return 0;
			}
			if( parseInt( a.ID ) === depositionLeaderID ) {
				return -1;
			}
			if( parseInt( b.ID ) === depositionLeaderID ) {
				return 1;
			}
			if( a.userType === 'W' || a.userType === 'WM' ) {
				return -1;
			}
			if( b.userType === 'W' || b.userType === 'WM' ) {
				return 1;
			}
			return a.name.trim().toLowerCase().localeCompare( b.name.trim().toLowerCase() );
		} );
		for (var u in users) {
			var User = users[u];
			if (parseInt( User.ID ) === Datacore.getS('currentUserID') || User.userType === 'W' || User.userType === 'WM') {
				continue;
			}
			var li_classes = [];
			if (groupMemberIDs.indexOf( parseInt( User.ID ) ) >= 0) {
				li_classes.push( 'selected' );
			}
			/** 13/11/14 - Determined not to do show inactive users as dimmed, mhill
			 *  if (!User.active) {
				li_classes.push( 'disconnected' );
			} */
			attendees_list += '<li id="'+ATTENDEE_IDENTIFIER+User.ID+'"'+(li_classes.length > 0 ? ' class="'+li_classes.join( ' ' )+'"' : '')+'>'
				+ '<span class="status_icon"></span>'+User.name+'</li>';
		}
		attendees_list = '<ul>'+attendees_list+'</ul>';

		$('#group_attendee_list').html( attendees_list );

		$('#group_attendee_list ul li span').click( selectGroupMember );

		$('#social_list_titlebar .social_list_nav_title').text( titleText );

		$('#social_list_titlebar #users_group_edit_cancel').show();

		$('#social_group_edit').show();

		$('#group_name_edit_container #group_name_edit').focus();
	},

	updateGroupEdit : function() {
		var users = Datacore.getUsers();
		var indexUsers = 0;
		var indexListItems = 0;
		var found = false;
		var newUser = '';
		var list = $('#group_attendee_list ul');
		var listChildren = $(list).first().children('li');
		var lastChild = $(listChildren).last();

		// Loop through all of the displayed users and see if we need to insert a new one.
		//console.log("Social::updateGroupEdit();");
		for(indexUsers in users) {
			found = false;
			var User = users[indexUsers];
			for(indexListItems = 0; indexListItems < listChildren.length; ++indexListItems) {
				// Skip ourself and witnesses.
				if (parseInt( User.ID ) === Datacore.getS('currentUserID') || User.userType === 'W' || User.userType === 'WM') {
					found = true;
					break;
				}
				var listID = parseInt( listChildren[indexListItems].id.slice( ATTENDEE_IDENTIFIER.length ) );
				var userID = parseInt( User.ID );
				if (listID === userID) {
					found = true;
					break;
				}
			}
			if(!found) {
				newUser = '<li id="'+ATTENDEE_IDENTIFIER+User.ID+'" >'+'<span class="status_icon"></span>'+User.name+'</li>';console.log("different:", newUser);
				$(lastChild).after( newUser );
			}
		}
		$('#group_attendee_list ul li span').off();
		$('#group_attendee_list ul li span').click( selectGroupMember );
	},

	addChatMessage : function( params ) {
		$('#chat_contents').append( Social.prepareChatMessage( params ) ).animate( {
			scrollTop : $('#chat_contents')[0].scrollHeight
		}, 300 );
	},

	createRoom : function( type, userIDs, roomTitle ) {
		console.log( '--- createRoom; type: ' + type + '; userIDs: ' + JSON.stringify( userIDs ) + '; roomTitle: "'+roomTitle+'"' );
		userIDs.sort(function(a,b){return a-b;});

		var privateUserID = null;
		var inviteIDs = [];

		var Room = {
			id : '',
			title : '',
			users : [Datacore.s.currentUserID],
			creator : dcUsers[Datacore.s.currentUserID],
			private : false,
			isChatEnabled : false
		};

		try {
			switch( type ) {
				case 'private':
					privateUserID = (userIDs[0] === Datacore.getS('currentUserID')) ? userIDs[1] : userIDs[0];
					Room.id = Datacore.getPrimaryDepositionID() + '.private.' + userIDs[0] + '.' + userIDs[1];
					Room.title = dcUsers[privateUserID].name;
					Room.private = true;
					Room.isChatEnabled = true;
					break;
				case 'group':
					if (!isset( roomTitle )) {
						throw 'Must define title of group';
					}
					var timestamp = Math.round( (Date.now() || new Date().getTime())/1000 );
					Room.id = Datacore.getPrimaryDepositionID() + '.group.' + Room.creator.ID + '.' + timestamp;
					Room.title = roomTitle;
					Room.isChatEnabled = true;
					break;
				default:
					throw 'Unsupported type ('+type+')';
			}

			for (var u in userIDs) {
				if (userIDs[u] === Datacore.getS('currentUserID')) {
					continue;
				}
				if (userIDs.hasOwnProperty( u )) {
					inviteIDs.push( userIDs[u] );
					Room.users.push( dcUsers[userIDs[u]] );
				}
			}

			// console.log( Room );
			Datacore.addRoom( Room );

			if (Room.id.length < 1 || !dcRooms[Room.id]) {
				throw 'Room was not saved';
			}

			sio.emit( 'join_to_room', {
				id : Room.id,
				title : Room.title,
				creatorID : Room.creator.ID,
				private : Room.private
			}, function(d){
				console.log( ' -- room creator joined to room' );
				console.log(d);
				sio.emit( 'inviteUsersToRoom', {
					roomID : Room.id,
					users : inviteIDs
				}, function(d){
					console.log( ' -- invited users to room on save' );
					console.log(d);
				} );
				Social.drawAttendeeList();
				if (privateUserID !== null) {
					Social.showChat( Room.id );
					$('#social_chat_window #chatentry').css( 'display', (dcUsers[privateUserID].active ? 'block' : 'none') );
					Social.updateChatAction( privateUserID );
				}
			} );
		} catch (e) {
			console.log( ' -- Error creating room: '+e );
			// Social.showChat( _room.id );
			showTooltip( 'An error occurred creating the room.', true );
			return;
		}
	},

	updateRoom : function( roomID, userIDs, roomTitle) {
		var Room = Datacore.getRoom( roomID );
		try {
			if (!Room) {
				throw 'Could not find room for ID ['+(typeof roomID)+'] "'+roomID+'"';
			}
			// perform all room member maintenance before rename_room
			var memberIDs = Room.getMemberIDs();
			// find any users not yet added to room
			var inviteUserIDs = [];
			for (var ui in userIDs) {
				if (memberIDs.indexOf( userIDs[ui] ) < 0) {
					inviteUserIDs.push( userIDs[ui] );
				}
			}
			if (inviteUserIDs.length > 0) {
				console.log( 'Adding users to room: '+inviteUserIDs.join( ', ' ) );
				sio.emit( 'inviteUsersToRoom', {
					roomID : Room.ID,
					users : inviteUserIDs
				}, function(d) {
						console.log( ' -- invited users to room on update : ' );
						console.log(d);
						if (isset( d.success ) && d.success) {
							var room = Datacore.getRoom( Room.ID);
							if (isset( room )) {
								for(var ui in inviteUserIDs) {
									room.addMemberID( inviteUserIDs[ui] );
								}
								Social.drawAttendeeList();
							}
						}
				   } );
			}
			// find any users to be removed from room
			var removeUserIDs = [];
			for (var mi in memberIDs) {
				if (memberIDs[mi] !== Datacore.getS('currentUserID') && userIDs.indexOf( memberIDs[mi] ) < 0) {
					removeUserIDs.push( memberIDs[mi] );
				}
			}
			if (removeUserIDs.length > 0) {
				console.log( 'Removing users from room: '+removeUserIDs.join( ', ' ) );
				sio.emit( 'remove_users_from_room', {
					id : Room.ID,
					users : removeUserIDs
				}, function(d) { console.log( ' -- removed users from room on update : ' ); console.log(d) } );
			}
			// update the room title, if changed
			if (Room.title !== roomTitle) {
				Room.title = roomTitle;
			}
			// update the room
			sio.emit( 'rename_room', {
				id : Room.ID,
				title : Room.title
			}, function(d) { console.log( ' -- Renamed Room: ' ); console.log(d); } );
		} catch (e) {
			console.log( ' -- Error updating room: '+e );
			showTooltip( 'An error occurred updating the room.', true );
			return;
		}
	},

	drawAttendeeList : function() {
		$('#social_attendee_list').empty();
		var user = Datacore.getS('currentUser');
		if( typeof user === 'undefined' || user === null || !(user instanceof window.top.DcUser) ) {
			console.error( 'missing current user:', user );
			return;
		}
//		console.log( 'social::drawAttendeeList(); userType:', user.userType );
		var isWitnessMember = ( user !== null && user.userType === 'WM' );
		var isDepositionOwner = ( Datacore.getS('parentID') === 0 && Datacore.getS('conductorID') === Datacore.getS('currentUserID') );
		var publicRooms = Datacore.getPublicRooms();
		publicRooms.sort( function( a, b ) {
			return (a.title > b.title ? 1 : (a.title < b.title ? -1 : 0));
		} );
		for (var pr in publicRooms) {
			var publicRoom = publicRooms[pr];
			var roomID = 'group_'+publicRoom.ID.replace( /[^a-zA-Z0-9]/g, '_' );
			var isRoomCollapsed = true;
			if (Social.currentGroup === null) {
				if (parseInt( pr ) === 0) {
					isRoomCollapsed = false;
				}
			} else if (Social.currentGroup === roomID){
				isRoomCollapsed = false;
			}
			var showChatElement = '';
			var noClickClass = '';
			var publicRoomInfo = publicRoom.ID.split( '.' );
			var publicRoomType = publicRoomInfo[1];
			if (publicRoomType === 'deposition') {
				noClickClass = ' no_click';
			} else {
				showChatElement = '<a href="javascript:void(0);" onclick="Social.showChat( \''+publicRoom.ID+'\' );">';
			}
			var group_edit_element = '';
			if (!publicRoom.readOnly && parseInt( publicRoom.creatorUserID ) === Datacore.getS('currentUserID')) {
				group_edit_element = '<div class="edit" onclick="Social.showGroupEdit(\''+publicRoom.ID+'\');"></div>';
			}
			var roomItem = '<div id="'+roomID+'" class="group_header">'
				+ showChatElement
				+ '<div class="list_item_title'+noClickClass+'">'
				+ '<span class="list_item_title_name">'+publicRoom.title+'</span>'
				+ '<span class="list_item_title_count">'+(parseInt( publicRoom.getMemberCount() )-1)+'</span>'
				+ '</div>'
				+ '<div id="unread_msg_'+roomID+'" class="group_chat_unread'+(group_edit_element.length > 0 ? ' editable' : '')+'">'
				+ '</div></a>'
				+ '<div class="toggle'+(isRoomCollapsed ? ' collapsed' : '')+'" onclick="Social.toggleGroup( this );"></div>'
				+ group_edit_element
				+ '</div>';
			roomItem += '<ul'+(isRoomCollapsed ? ' style="display:none;"' : '')+'>';
			var depositionLeaderID = window.top.webApp.getSpeakerID();
			var users = Datacore.getUsers( publicRoom.getMemberIDs() );
			users.sort( function( a, b ) {
//				return (a.name > b.name ? 1 : (a.name < b.name ? -1 : 0));
				if( a === null || b === null ) {
					return 0;
				}
				if( parseInt( a.ID ) === depositionLeaderID ) {
					return -1;
				}
				if( parseInt( b.ID ) === depositionLeaderID ) {
					return 1;
				}
				if( a.userType === 'W' || a.userType === 'WM' ) {
					return -1;
				}
				if( b.userType === 'W' || b.userType === 'WM' ) {
					return 1;
				}
				return a.name.trim().toLowerCase().localeCompare( b.name.trim().toLowerCase() );
			} );
			for( var u=0; u<users.length; ++u ) {
				if( typeof users[u] === 'undefined' || users[u] === null || typeof users[u].ID === 'undefined' || user.ID === users[u].ID ) {
					continue;
				}
				var userID = parseInt( users[u].ID );
				var roleClass = '';
				var witnessDiv = '';
				if (users[u].userType === 'W' || users[u].userType === 'WM') {
					roleClass = ' witness';
					if (depositionLeaderID === Datacore.getS('currentUserID')) {
						witnessDiv = '<div class="block_icon block_'+userID
								+ (users[u].blockExhibits ? ' block_exhibits' : '')
								+'" onclick="Social.toggleWitnessBlock('+userID+');"></div>';
					}
				} else if (userID === depositionLeaderID) {
					roleClass = ' leader';
				} else if ( Datacore.hasPresentationUser(userID) && depositionLeaderID === Datacore.getS('currentUserID') ) {
					roleClass = ' in_presentation';
				}
				roomItem +='<li class="'+(users[u].active ? 'active' : 'inactive')+roleClass+'">'
					+ '<div id="userid_'+userID+'" class="attendee_name" onclick="Social.showPrivateChat('+userID+');">'
					+ '<div class="attendee_role'+roleClass+'"></div>'
					+ '<div class="unread_msg_attendee_'+userID+' attendee_unread_messages"></div>'
					+ '<div class="list_item">'+users[u].name+'</div>'
					+ '</div>'
					+ witnessDiv
					+ '</li>';
			}
			roomItem += '</ul>';
			$('#social_attendee_list').append( '<div class="social_list_header">'+roomItem+'</div>' );
		}
		if (isDepositionOwner)  {
			$('#social_attendee_list .attendee_name').each(Social.setAttendeeLongPress);
		}
		if (depositionLeaderID === Datacore.getS('currentUserID')) {
			$('#social_attendee_list .group_header').each(Social.setDroppable);
			$('#social_attendee_list .active .attendee_name').each(Social.setDroppable);
		}
		Social.reportUnreadMessages();
		//check if chat window should disable messaging for offline user
		if (Social.currentRoom && $('#social_chat_window').is( ':visible' )) {
			var chatRoom = Datacore.getRoom( Social.currentRoom );
			if (chatRoom && !chatRoom.private) {
				$('#chat_title').addClass( 'group' );
				// TODO - only subtract one if Datacore.s.currentUserID is NOT the room creator
				$('#chat_title p').text( chatRoom.title+' ('+(chatRoom.getMemberCount()-1)+')' );
			}
			var roomInfo = Social.currentRoom.split( '.' );
			var roomType = roomInfo[1];
			if (roomType === 'private') {
				var user1 = dcUsers[roomInfo[2]];
				var user1Active = false;
				if (isset( user1 )) {
					user1Active = user1.active;
				}
				var user2 = dcUsers[roomInfo[3]];
				var user2Active = false;
				if (isset( user2 )) {
					user2Active = user2.active;
				}
				var cssDisplay = (user1Active && user2Active) ? 'block' : 'none';
				if ($('#social_chat_window #chatentry').length > 0) {
					$('#social_chat_window #chatentry').css( 'display', cssDisplay );
				}
				if (isset( user1 ) && isset( user2 )) {
					if (parseInt( user1.ID ) === Datacore.getS('currentUserID')) {
						Social.updateChatAction( user2.ID );
					} else {
						Social.updateChatAction( user1.ID );
					}
				}
			} else if (roomType === 'group') {
				if (!chatRoom) {
					Social.hideChat();
				}
			}
		}
		if ($('#social_group_edit').is( ':visible' )) {
			Social.updateGroupEdit();
		}
	},

	drawToggleBadge : function() {
		if( $('#social_list_container').is( ':visible' ) ) {
			$('#attendee_chat_unread').html( '' );
		} else {
			var tallyUnreadMessages = 0;
			var rooms = Datacore.getRooms();
			for (var r in rooms) {
				tallyUnreadMessages += rooms[r].unreadMessageCount;
			}
			$('#attendee_chat_unread').html( Social.prepareBadgeMarkup( tallyUnreadMessages ) );
		}
	},

	hideChat : function() {
		Social.currentRoom = null;
		$('#social_chat_window').hide( 200, 'easeOutQuad' );
		if ($('#social_attendee_container').hasClass( 'collapsed' )) {
			$('#social_attendee_container').removeClass( 'collapsed', 200, 'easeInQuad' );
		}
	},

	isCurrentRoomVisible : function() {
		if (Social.currentRoom && $('#social_chat_window').is( ':visible' ) && $('#social_list_container').is( ':visible' )) {
			return true;
		}
		return false;
	},

	prepareBadgeMarkup : function( count ) {
		return (count > 0 ? '<span class="sigil-red">'+count+'</span>' : '');
	},

	prepareChatMessage : function( args ) {
		var d = new Date( args.timestamp*1000 );
		var h = d.getHours();
		var m = d.getMinutes();
		return '<div class="chat_message">'
		+ '<div class="message_text"><p><span class="author">'+args.userName+'</span>'+args.text+'</p></div>'
		+ '<div class="message_timestamp"><p>'+(h%12 > 0 ? h%12 : 12)+':'+(m > 10 ? m : '0'+m)+' '+(h >= 12 ? 'pm' : 'am')+'</p></div>'
		+ '</div>';
	},

	reportUnreadMessages : function() {
		var rooms = Datacore.getRooms();
		for (var r in rooms) {
			var badgeTally = rooms[r].unreadMessageCount;
			if (rooms[r].private) {
				var memberIDs = rooms[r].getMemberIDs();
				var badgeElement = 'li div.unread_msg_attendee_'
					+ (Datacore.getS('currentUserID') !== parseInt( memberIDs[0] ) ? memberIDs[0] : memberIDs[1]);
			} else {
				badgeElement = '#unread_msg_group_'+rooms[r].ID.replace( /[^a-zA-Z0-9]/g, '_' );
				badgeTally += rooms[r].unreadChildMessageCount;
				if (rooms[r].ID === getAllAttendeesID()) {
					var visible_group = $('#social_attendee_list .toggle:not(.collapsed)');
					if (visible_group.length > 0) {
						badgeTally -= getPrivateUnreadMessageCount( visible_group.parent().attr( 'id' ).slice( 6 ).replace( /_/g, '.' ) );
					}
				}
			}
			$(badgeElement).html( Social.prepareBadgeMarkup( badgeTally ) );
		}
		Social.drawToggleBadge();
	},

	showChat : function( roomID ) {
		if (!Datacore.hasRoom( roomID )) {
			showTooltip( 'Could not load the requested room.', true );
			return;
		}
		var reqRoom = Datacore.getRoom( roomID );
		if (reqRoom.getUserCount() === 1) {
			return;
		}
		Social.currentRoom = reqRoom.ID;
		if (!reqRoom.private) {
			Social.toggleGroup( '#group_'+reqRoom.ID.replace( /[^a-zA-Z0-9]/g, '_' )+' > div.toggle', true );
			$('#chat_title').addClass( 'group' );
			$('#chat_title p').text( reqRoom.title+' ('+(reqRoom.getMemberCount()-1)+')' );
		} else {
			Datacore.getRoom( getAllAttendeesID() ).unreadChildMessageCount -= reqRoom.unreadMessageCount;
			$('#chat_title').removeClass( 'group' );
			$('#chat_title p').text( reqRoom.title );
		}
		reqRoom.unreadMessageCount = 0;
		$('#chat_contents .chat_message').remove();
		var messages = reqRoom.getMessages();
		for (var m in messages) {
			$('#chat_contents').append( Social.prepareChatMessage( messages[m] ) );
		}
		Social.reportUnreadMessages();
		$('#chat_action').hide();
		$('#chat_contents').height(Social.chatHistoryHeightBig);
		$('#social_chat_window #chatentry').css( 'display', 'block' );
		$('#social_chat_window').show( 200, 'easeInQuad' );
		$('#social_attendee_container').addClass( 'collapsed', 200, 'easeInQuad' );
		$('#chat_contents').animate( {
			scrollTop : $('#chat_contents')[0].scrollHeight
		}, 200 );
		$('#social_chat_window #chatentry').focus();
	},

	showDepositionChat : function() {
		Social.showChat( getAllAttendeesID() );
	},

	showPrivateChat : function( userID ) {
		var user = Datacore.getUser( userID );
		if (user.userType === 'W') {
			showPlainDialog1Btn( 'Can\'t chat with person', 'You can\'t chat with "'+user.name+'" because this person is a witness.', 'Close' );
			return;
		}
		var keyHeader = Datacore.getPrimaryDepositionID()+'.private.';
		var roomName = (userID > Datacore.getS('currentUserID') ? Datacore.getS('currentUserID')+'.'+userID : userID+'.'+Datacore.getS('currentUserID'));
		if (Datacore.hasRoom( keyHeader+roomName )) {
			Social.showChat( keyHeader+roomName );
			$('#social_chat_window #chatentry').css( 'display', (user.active ? 'block' : 'none') );
		} else {
			Social.createRoom( 'private', [Datacore.getS('currentUserID'),userID] );
		}
		Social.updateChatAction( userID );
	},

	updateChatAction : function( userID ) {
		console.log( 'updateChatAction(): userID = ' + userID );
		if (Social.currentRoom) {
			var chatRoom = Datacore.getRoom( Social.currentRoom );
			var showChatAction = false;
			if (isNaN( userID )) {
				return;
			}
			userID = parseInt( userID );
			var user = Datacore.getUser( userID );
			if (user === null) {
				console.log("User is null for userID: " + userID);
				return;
			}
			if (user.userType === 'G' || user.userType === 'R' || user.clientTypeID === 'CRT') {
				return;
			}
			if (chatRoom.private && chatRoom.hasMember( userID ) ) {
				var depositionLeaderID = ( Datacore.getS( 'speakerID' ) !== 0 ) ? depositionLeaderID = Datacore.getS( 'speakerID' ) : Datacore.getS( 'conductorID' );
				var isDepositionOwner = ( Datacore.getS( 'parentID' ) === 0 && Datacore.getS( 'conductorID' ) === Datacore.getS( 'currentUserID' ) );
				if (isDepositionOwner) {
					if ( depositionLeaderID === Datacore.getS( 'currentUserID') && user.active && user.userType !== 'WM' ) {
						showChatAction = !window.top.Datacore.s.inPresentation;
						$('#role_button').text('Pass the Role');
						$('#chat_action_text').text('You can make this person the Session Leader');
					} else if ( userID === Datacore.getS( 'speakerID' ) ) {
						showChatAction = true;
						$('#role_button').text('Revoke the Role');
						$('#chat_action_text').text('You can make yourself the Session Leader');
					}
				} else if ( depositionLeaderID === Datacore.getS('currentUserID') && user.active && userID === Datacore.getS( 'conductorID' ) ) {
					showChatAction = !window.top.Datacore.s.inPresentation;
					$('#role_button').text('Return the Role');
					$('#chat_action_text').text('You can return the Session Leader role');
				}
			}
			console.log("isDepositionOwner: "+isDepositionOwner+" depositionLeaderID:"+depositionLeaderID+" currentUserID: "+Datacore.getS('currentUserID')+" conductorID: "+Datacore.getS('conductorID'));
			// Adjust chat history height.
			if ($('#chat_action').is(':visible') && !showChatAction) {
				// Hide chat_action.
				Social.hideChatAction();
			} else if (!$('#chat_action').is(':visible') && showChatAction) {
				// Show chat_action.
				Social.showChatAction();
				//Scroll down to the bottom.
				$('#chat_contents').scrollTop( $('#chat_contents')[0].scrollHeight );
			}
		}
	},

	hideChatAction : function() {
		$('#chat_action').hide();
		$('#chat_contents').height( Social.chatHistoryHeightBig );
	},

	showChatAction : function() {
		$('#chat_action').show();
		$('#chat_contents').height( Social.chatHistoryHeightSmall );
	},

	isVisible : function() {
		if( $('#social_list_container').is( ':visible' ) ) {
			return true;
		} else {
			return false;
		}
	},

	hideAttendeeList : function() {
		if(window.top.Social.isVisible()) {
			window.top.Social.toggleAttendeeList(false);
		}
	},

	toggleAttendeeList : function( skipAnimation ) {
		skipAnimation = (typeof skipAnimation !== "undefined" && skipAnimation === true);
		var effect = {
			"duration": (!skipAnimation ? 200 : 0),
			"easing": (!skipAnimation ? "easeInOutQuad" : "swing")
		};
		var hiddenClass = "hideSocialList";
		var shownClass = "showSocialList";
		var drawChatRoom = function() {
			if (Social.isCurrentRoomVisible()) {
				var reqRoom = Datacore.getRoom( Social.currentRoom );
				Datacore.getRoom( getAllAttendeesID() ).unreadChildMessageCount -= reqRoom.unreadMessageCount;
				if( reqRoom.unreadMessageCount > 0 ) {
					reqRoom.unreadMessageCount = 0;
					$('#chat_contents .chat_message').remove();
					var messages = reqRoom.getMessages();
					for (var m in messages) {
						$('#chat_contents').append( Social.prepareChatMessage( messages[m] ) );
					}
					Social.chatScrollPos = $('#chat_contents')[0].scrollHeight;
				}
				$('#chat_contents')[0].scrollTop = Social.chatScrollPos;
				Social.reportUnreadMessages();
				$('#social_chat_window #chatentry').focus();
			}
			Social.drawToggleBadge;
		};
		if( $('#social_list_container').is( ':visible' ) ) {
			Social.chatScrollPos = $('#chat_contents')[0].scrollTop;
			$('#social_list_container').hide( {"effect": "slide", "direction": "right", "duration": effect.duration, "easing": effect.easing} );
			$('#social_list_container, #attendee_list_toggle_wrap').switchClass( shownClass, hiddenClass, effect.duration, effect.easing, Social.drawToggleBadge );
			// Whenever the Attendee list is toggled, "All Attendees" should be expanded
			Social.toggleGroup( '#group_'+Datacore.getPrimaryDepositionID()+'_deposition > div.toggle', true );
		} else {
			$('#social_list_container').show( {"effect": 'slide', "direction": "right", "duration": effect.duration, "easing": effect.easing } );
			$('#social_list_container, #attendee_list_toggle_wrap').switchClass( hiddenClass, shownClass, effect.duration, effect.easing, drawChatRoom );
		}
		if (Social.chatHistoryHeightBig === null) {
			Social.chatHistoryHeightBig = $('#chat_contents').height();
			Social.chatHistoryHeightSmall = $('#chat_contents').height() - $('#chat_action').height();
		}
	},

	toggleAttendPresentationBtn : function( showBtn, withAnimation ) {
		var duration = withAnimation ? 300 : 0;
		if (showBtn) {
			$('#ipad_container_frame').contents().find('#attend_presentation').fadeIn(duration).switchClass('notActive', 'available', duration, 'linear');
			$('#ipad_container_frame').contents().find('#deposition_info').switchClass( 'hidePresentationBtn', 'showPresentationBtn', duration, 'linear' );
		} else {
			$('#ipad_container_frame').contents().find('#attend_presentation').fadeOut(duration).switchClass('available', 'notActive', duration, 'linear');
			$('#ipad_container_frame').contents().find('#deposition_info').switchClass( 'showPresentationBtn', 'hidePresentationBtn', duration, 'linear' );
		}
	},

	toggleGroup : function( obj, noCollapse ) {
		if ($(obj).length < 1) {
			return;
		}
		noCollapse = (typeof noCollapse !== 'undefined' && noCollapse);
		var wasCollapsed = $(obj).hasClass( 'collapsed' );
		$('#social_attendee_list .toggle').addClass( 'collapsed' );
		$('#social_attendee_list ul').hide();

		var allAttendees = Datacore.getRoom( getAllAttendeesID() );
		var updateTally = allAttendees.unreadMessageCount + allAttendees.unreadChildMessageCount;

		if (noCollapse || wasCollapsed) {
			Social.currentGroup = $(obj).parent().attr( 'id' );
			updateTally -= getPrivateUnreadMessageCount( $(obj).parent().attr( 'id' ).slice( 6 ).replace( /_/g, '.' ) );
			$(obj).removeClass( 'collapsed' );
			$(obj).parent().siblings( 'ul' ).show();
		}

		$('#unread_msg_group_'+Datacore.getPrimaryDepositionID()+'_deposition').html( Social.prepareBadgeMarkup( updateTally ) );
	},

	setGroupAttendeesActive : function() {
		if ($('#group_name_edit_container #group_name_edit').hasClass( 'transparent' )) {
			$('#group_name_edit_container #group_name_edit').removeClass( 'transparent' );
		}
		if ($('#group_attendee_list').hasClass( 'inactive' )) {
			$('#group_attendee_list').removeClass( 'inactive' );
		}
		if ($('#group_form_action_save').hasClass( 'inactive' )) {
			Social.enableGroupSaveButton();
		}
		$('#group_name_edit_container #clear-input').show();
	},

	setGroupAttendeesInactive : function() {
		if (!$('#group_name_edit_container #group_name_edit').hasClass( 'transparent' )) {
			$('#group_name_edit_container #group_name_edit').addClass( 'transparent' );
		}
		if (!$('#group_attendee_list').hasClass( 'inactive' )) {
			$('#group_attendee_list').addClass( 'inactive' );
		}
		if (!$('#group_form_action_save').hasClass( 'inactive' )) {
			Social.disableGroupSaveButton();
		}
		$('#group_name_edit_container #clear-input').hide();
	},

	enableGroupSaveButton : function() {
		$('#group_form_action_save').removeClass( 'inactive' );
		$('#group_form_action_save').off().click( function() {
			var groupName = $('#group_name_edit').val();

			// determine the Attendees currently selected as group members
			var activeAttendeeIDs = [];
			$('#group_attendee_list ul li.selected').each( function() {
				activeAttendeeIDs.push( parseInt( this.id.slice( ATTENDEE_IDENTIFIER.length ) ) );
			} );

			if ( !Datacore.hasRoom( Datacore.getS('editorGroupID') )) {
				console.log( 'Saving new Group' );
				Social.createRoom( 'group', activeAttendeeIDs, groupName );
			} else {
				console.log( 'Updating Group by ID '+Datacore.getS('editorGroupID') );
				Social.updateRoom( Datacore.getS('editorGroupID'), activeAttendeeIDs, groupName );
			}
			Social.showAttendeeList();
		} );
	},

	disableGroupSaveButton : function() {
		$('#group_form_action_save').addClass( 'inactive' );
		$('#group_form_action_save').off();
	},

	enableGroupDeleteButton : function() {
		$('#group_form_action_delete').removeClass( 'inactive' );
		$('#group_form_action_delete').off().click( function() {
			sio.emit( 'close_room', {id : Datacore.getS('editorGroupID')} );
			Datacore.dropRoom( Datacore.getS('editorGroupID') );
			Social.showAttendeeList();
		} );
	},

	disableGroupDeleteButton : function() {
		$('#group_form_action_delete').addClass( 'inactive' );
		$('#group_form_action_delete').off();
	},

	changeRole : function() {
		var depositionLeaderID = ( Datacore.getS( 'speakerID' ) !== 0 ) ? Datacore.getS( 'speakerID' ) : Datacore.getS( 'conductorID' );
		var isDepositionOwner = ( Datacore.getS('parentID') === 0 && Datacore.getS('conductorID') === Datacore.getS('currentUserID') );
		if (isDepositionOwner) {
			if (depositionLeaderID != Datacore.getS('currentUserID')) {
				// Take back the role.
				$.getJSON("depositions/setSpeaker", null)
				.fail(function() {
					showPlainDialog1Btn( 'Error', 'Failed to take back the role.', 'Close' );
				});
			} else {
				// Pass the role on to the other person in the room.
				var chatRoom = Datacore.getRoom( Social.currentRoom );
				if (chatRoom && chatRoom.private) {
					var memberIDs = chatRoom.getMemberIDs();
					var userID = memberIDs[0] !== Datacore.getS('currentUserID') ? memberIDs[0] : memberIDs[1];
					var user = Datacore.getUser(userID);
					if (user === null) {
						return;
					}
					var userName = user.name;
					if (user.name.length >= 25) {
						userName = userName.substr(0, 25) + ' ' + userName.substr(25);
						if (user.name.length >= 50) {
							userName = userName.substr(0, 50) + '...';
						}
					}
					var dialogButtons = [];
					dialogButtons.push( {label: 'Cancel', action : 'dismissDialog3Btn();', class : 'app_bttn_cancel'} );
					dialogButtons.push( {label: 'Pass the Role', action : 'Social.passRole('+userID+');', class : 'app_bttn_new'} );
					var confirmPass = 'Would you like to pass the Leader Role to '+userName+'?';
					showDialog3Btn( 'Pass the Leader Role?', confirmPass, dialogButtons, true );
				}
			}
		} else {
			Social.passRole();
		}
	},

	toggleWitnessBlock : function( userID ) {
		var user = Datacore.getUser( userID );
		var depositionID = Datacore.getPrimaryDepositionID();
		var block_class = '.block_icon.block_'+userID;
		if (user.blockExhibits === true) {
			$(block_class).removeClass( 'block_exhibits' );
			user.blockExhibits = false;
		} else {
			if (!$(block_class).hasClass( 'block_exhibits' )) {
				$(block_class).addClass( 'block_exhibits' );
			}
			user.blockExhibits = true;
		}
		sendBlockExhibits(depositionID, userID, user.blockExhibits);
	},

	passRole : function( userID )  {
		dismissDialog3Btn();
		if (typeof userID === 'undefined' || userID === null) {
			userID = '';
		}
		$.getJSON("depositions/setSpeaker?userID="+userID, function(data, textStatus, xhr) {
			if (textStatus !== "success") {
				showPlainDialog1Btn( 'Error', 'Failed to pass the role.', 'Close' );
			}
		})
		.fail(function() {
			showPlainDialog1Btn( 'Error', 'Failed to pass the role.', 'Close' );
		});
	},

	setAttendeeLongPress : function() {
		$( this ).mousedown(function() {
			if (Social.mouseDownTimerID !== null) {
				return;
			}
			Social.mouseDownTimerID = window.setTimeout(Social.timeoutLongPress.bind( this ), 500);
		}).mouseup( Social.cancelLongPress ).hover( null, Social.cancelLongPress );
	},

	timeoutLongPress : function() {
		if (Social.mouseDownTimerID !== null) {
			Social.mouseDownTimerID = null;
			var idSplit = this.id.split( '_' );
			if (idSplit.length > 1) {
				getSelection().removeAllRanges();
				Social.showBootUserDialog( parseInt( idSplit[1] ) );
			}
		}
	},

	cancelLongPress : function() {
		getSelection().removeAllRanges();
		if (Social.mouseDownTimerID !== null) {
			window.clearTimeout( Social.mouseDownTimerID );
			Social.mouseDownTimerID = null;
		}
	},

	showBootUserDialog : function( userID ) {
		userID = parseInt( userID );
		if (isNaN( userID )) {
			return;
		}
		var user = Datacore.getUser( userID );
		if (user === null || !user.active) {
			return;
		}
		var dialogButtons = [];
		dialogButtons.push( {label: 'Allow to Rejoin', action : 'Social.bootUser('+userID+', false);', class : 'btn_app_normal'} );
		dialogButtons.push( {label: 'Boot Permanently', action : 'Social.confirmPermanentBoot('+userID+');', class : 'btn_app_extra'} );
		dialogButtons.push( {label: 'Cancel', action : 'dismissDialog3BtnVer();', class : 'btn_app_cancel'} );
		var confirmMsg = 'Are you sure you want to boot '+user.name+' from this session?';
		showDialog3BtnVer( 'Boot User?', confirmMsg, dialogButtons );
	},

	confirmPermanentBoot : function( userID ) {
		dismissDialog3BtnVer();
		userID = parseInt( userID );
		if (isNaN( userID )) {
			return;
		}
		var user = Datacore.getUser( userID );
		if (user === null) {
			return;
		}
		var dialogButtons = [];
		dialogButtons.push( {label: 'Cancel', action : 'dismissDialog3Btn();', class : 'app_bttn_cancel'} );
		dialogButtons.push( {label: 'Boot', action : 'Social.bootUser('+userID+', true);', class : 'app_bttn_new'} );
		var confirmMsg = user.name+' will be permanently booted from this session. The user will no longer have access to the session and will not be able to log back in.';
		showDialog3Btn( 'Permanently Boot User?', confirmMsg, dialogButtons );
	},

	bootUser : function( userID, ban ) {
		dismissDialog3BtnVer();
		dismissDialog3Btn();
		userID = parseInt( userID );
		if (isNaN( userID )) {
			return;
		}
		var user = Datacore.getUser( userID );
		if (user === null) {
			return;
		}
		var banUserID = !user.guest ? userID : '';
		var banGuestID = user.guest ? userID : '';
		$.getJSON("depositions/kickAttendee?userID="+banUserID+'&guestID='+banGuestID+'&ban='+ban, null, function(data, textStatus, xhr) {
			if (textStatus === "success" && data !== null) {
				console.log( 'Kicked userID:', userID );
				Datacore.dropUser( userID );
				var keyHeader = Datacore.getPrimaryDepositionID()+'.private.';
				var roomName = keyHeader + ( (userID > Datacore.getS('currentUserID') ? Datacore.getS('currentUserID')+'.'+userID : userID+'.'+Datacore.getS('currentUserID')) );
				if (Datacore.hasRoom( roomName )) {
					Datacore.dropRoom( roomName );
					if (Social.currentRoom === roomName) {
						Social.hideChat();
					}
				}
			}
		})
		.fail(function() {
			showPlainDialog1Btn( 'Error', 'Failed to boot the user.', 'Close' );
		});
	},

	setDroppable : function () {
		var resolveCurrentTarget = function( objstring ) {
			return {
				type : objstring.slice( 0, objstring.indexOf( '_' ) ),
				id : objstring.slice( objstring.indexOf( '_' )+1 ).replace( /_/g, '.' )
			};
		};

		var tag = this.id.split('_');
		if (tag[0] === 'userid') {
			var userID = parseInt( tag[1] );
			var user = Datacore.getUser( userID );
			if (user !== null && (user.userType === 'W' || user.userType === 'WM' || user.userType === 'G' || user.userType === 'R')) {
				return;
			}
		}

		var highlight = ($(this).hasClass( 'group_header' ) ? $(this) : $(this).parent());

		$(this).addClass('ui-droppable');

		$(this).on('dragover', function(event) {
			// disallow dragging onto (All Attendees)
			var domObj = resolveCurrentTarget( event.currentTarget.id );
			if( Datacore.getS( 'dragging' ) === 'folder' && domObj.type === 'group' ) {
				var room = Datacore.getRoom( domObj.id );
				if( room !== null && !room.isChatEnabled ) {
					return;
				}
			}

			event.preventDefault();

			if (!highlight.hasClass( 'drag_over' )) {
				highlight.addClass( 'drag_over' );
			}
		}).on('dragenter', function( event ) {
			// disallow dragging onto (All Attendees)
			var domObj = resolveCurrentTarget( event.currentTarget.id );
			if( Datacore.getS( 'dragging' ) === 'folder' && domObj.type === 'group' ) {
				var room = Datacore.getRoom( domObj.id );
				if( room !== null && !room.isChatEnabled ) {
					return;
				}
			}

			if (!highlight.hasClass( 'drag_over' )) {
				highlight.addClass( 'drag_over' );
			}
		}).on('dragleave', function() {
			if (highlight.hasClass( 'drag_over' )) {
				highlight.removeClass( 'drag_over' );
			}
		}).on('drop', function(event) {
			if((typeof event.originalEvent === "undefined" || event.originalEvent === null)
					&& (arguments[1] && typeof arguments[1].dataTransfer !== "undefined" && arguments[1].dataTransfer !== null)) {
				event.impersonated = true;
				event.originalEvent = arguments[1];
			}
			event.preventDefault();
			var dcobj = JSON.parse( event.originalEvent.dataTransfer.getData( 'text' ) );
			if (dcobj.type !== 'folder' && dcobj.type !== 'file') {
				return;
			}
			var domObj = resolveCurrentTarget( event.currentTarget.id );

			// prepare action arguments for group vs user
			if (domObj.type === 'userid') {
				var user = Datacore.getUser( domObj.id );
				var shareWith = user.name;
				var userRequest = parseInt( user.ID );
			} else if (domObj.type === 'group') {
				var room = Datacore.getRoom( domObj.id );

				if ( typeof room !== 'undefined' && room !== null && !room.isChatEnabled && room.readOnly && dcobj.type === 'file' ) {
					//Verify if they want to introduce.
					var dialogButtons = [];
					dialogButtons.push( {label: 'Cancel', action : 'dismissDialog3Btn();', class : 'btn_app_cancel'} );
					dialogButtons.push( {label: 'Introduce', action : 'Social.confirmIntroduce('+dcobj.id+');', class : 'btn_app_normal'} );
					var confirmMsg = 'Introduce Document as an official exhibit by sharing with "All Attendees"?';
					showDialog3Btn( 'Introduce Exhibit', confirmMsg, dialogButtons, true );

					return;
				}

				var memberIDs = room.getMemberIDs();

				// if group has no members, show 'Notice' dialog and return.
				var hasMember = false;
				for (var mi in memberIDs) {
					user = Datacore.getUser( memberIDs[mi] );

					if (user.ID !== Datacore.getS('currentUserID') && !user.guest) {
						hasMember = true;
					}
				}
				if (!hasMember) {
					showPlainDialog1Btn( 'Notice!', 'Sharing operation cannot be completed, the group does not contain any members.', 'OK' );

					return;
				}

				// get the room members and exclude the current user
				var userIDs = [];
				for (var mi in memberIDs) {
					if (memberIDs[mi] !== Datacore.getS('currentUserID')) {
						userIDs.push( memberIDs[mi] );
					}
				}
				userRequest = userIDs.join( ',' );

				shareWith = room.title;
			}

			// determine appropriate action for folder vs file
			var fileExt = '';
			if (dcobj.type === 'folder') {
				var requestUri = 'depositions/shareFolder?id='+dcobj.id+'&userIDs='+userRequest+'&folderName='+encodeURIComponent( Datacore.getFolder( dcobj.id ).name );
				var dialog = {
					title : 'Share Folder',
					text : 'Would you like to share this Folder with '+shareWith+'?',
					name : Datacore.getFolder( dcobj.id ).name
				};

				// prepare the action to be run once the file name "form" is submitted
				setDialogFileShareSubmitAction( function() {
					if (dcobj.type === 'file') {
						var newName = $('#dialogFileShare .popupDialog .dialogForm #shareFileName').val() + fileExt;
						requestUri += '&customName='+encodeURIComponent( newName );
					}
					console.log( 'social::setDialogFileShareSubmitAction() callback - requestURI = ', requestUri );
					$.getJSON( requestUri, null, function(data, textStatus, xhr) {
						console.log( ' -- File share returned '+textStatus );
						console.log( data );
						console.log( xhr );
						if (textStatus !== 'success' || data === null) {
							console.log( ' -- Error sharing object' );
						}
						if ( isset( data.EDDeposition ) && isset( data.EDDeposition.result ) ) {
							if ( data.EDDeposition.result.shared ) {
								showPlainDialog1Btn( 'Sharing Completed', 'The folder sharing operation has completed.', 'Close' );
							} else if ( !data.EDDeposition.result.shared && data.EDDeposition.result.skipped ) {
								showPlainDialog1Btn( 'Already Shared', 'The user already has this folder.', 'Close' );
							}
						}
					} );
				} );
			} else {
				// ASSUMPTION: files in eDepoze will always have an extension and we cannot let the user change them
				var fileName = Datacore.getFile( dcobj.id ).name;
				var extIndex = fileName.lastIndexOf( '.' );
				fileExt = fileName.substring( extIndex );
				fileName = fileName.substring( 0, extIndex );
				requestUri = 'depositions/shareFile?id='+dcobj.id+'&userIDs='+userRequest;
				dialog = {
					title : 'Share Document',
					text : 'Please provide a name for the document to share with '+shareWith+':',
					name : fileName
				};

				// prepare the action to be run once the file name "form" is submitted
				setDialogFileShareSubmitAction( function() {
					if (dcobj.type === 'file') {
						var newName = $('#dialogFileShare .popupDialog .dialogForm #shareFileName').val() + fileExt;
						requestUri += '&customName='+encodeURIComponent( newName );
					}
					console.log( 'social::setDialogFileShareSubmitAction() callback - requestURI = ', requestUri );
					$.getJSON( requestUri, null, function(data, textStatus, xhr) {
						console.log( ' -- File share returned '+textStatus );
						console.log( data );
						console.log( xhr );
						if (textStatus !== 'success' || data === null) {
							console.log( ' -- Error sharing object' );
						}
						if ( isset( data.EDDeposition ) && isset( data.EDDeposition.result ) ) {
							if ( data.EDDeposition.result.shared ) {
								showPlainDialog1Btn( 'Sharing Completed', 'The file sharing operation has completed.', 'Close' );
							}
						}
					} );
				} );
			}
			showDialogFileShareBtn( dcobj.type, dialog.title, dialog.text, dialog.name );
		} );
	},

	confirmIntroduce : function(fileID) {
		dismissDialog3Btn();
		var src = '/depositions/introduce?ID=';
		var file = Datacore.getFile( fileID );
		if( file.mimeType.indexOf("audio") > -1 ) {
			src = '/depositions/audio?beginIntroduce=1&ID=';
		}
		else if( file.mimeType.indexOf("video") > -1 ) {
			src = '/depositions/video?beginIntroduce=1&ID=';
		}
		else if (Datacore.getS('class') === 'WitnessPrep' || Datacore.getS('class') === 'WPDemo') {
			src = '/depositions/introduce?ID=';
		}
		window.top.document.getElementById( 'ipad_container_frame' ).setAttribute( 'src', src+fileID+"&fromView=3" );
		Social.toggleAttendeeList();
	},

	presentationActive : function()
	{
		webApp.toggleAttendPresentationBtn(true, true);
		$('#ipad_container_frame').contents().find('#attend_presentation').on('click',window.top.webApp.presentationJoin);
		Datacore.s.presentationAvailable = true;
		if( window.top.webApp.userIsSpeaker() !== true ) {
			if( window.top.iPadContainerFrame.location.pathname === '/depositions/presentation' ) {
				window.top.presentationJoin( window.top.webApp.getS( 'inDeposition' ) );
			} else {
				Social.presentationConfirmJoin();
			}
		}
	},

	presentationJoinable : function()
	{
		if(Datacore.s.presentationAvailable) {
			webApp.toggleAttendPresentationBtn(true, true);
			$('#ipad_container_frame').contents().find('#attend_presentation').on('click',window.top.webApp.presentationJoin);
		}
	},

	presentationStart: function( args )
	{
		$('#ipad_container_frame').contents().find('#attend_presentation').switchClass( 'notActive', 'available', 0 );
		if( Datacore.getS( 'presentationAvailable' ) === true ) {
			Social.activePresentation = args.roomID;
			if( window.top.webApp.userIsSpeaker() === true ) {
//				_titleActions.presentationActive( true );
//				_titleActions.showPassAnnotation();
//				_titleActions.redraw();
//				window.top.navBar.annotatorButton.enable();
//				var _fullTitleActions = window.top.frames["ipad_container_frame"]._fullTitleActions;
//				if( typeof _fullTitleActions !== 'undefined' && _fullTitleActions !== null ) {
//					_fullTitleActions.presentationActive( true );
					//_fullTitleActions.showPassAnnotation();
//					_fullTitleActions.redraw();
//				}
				if (typeof args.presentationInfo !== 'undefined') {
					window.top.webApp.setS( 'joinPresentationData', args.presentationInfo );
				} else {
					window.top.webApp.setS( 'joinPresentationData', args );
				}
//				window.top.iPadContainerFrame.view_file( Datacore.getVal( Datacore.s.joinPresentationData, 'fileID' ), 'application/pdf' );
//				if( window.top.iPadContainerFrame.location.pathname !== '/depositions/presentation' ) {
//					window.top.iPadContainerFrame.location.replace( '/depositions/presentation' );
//				} else {
//				}
			} else {
				Social.presentationConfirmJoin();
			}
		}
	},

	presentationEnd : function() {
		$('#ipad_container_frame').contents().find('#attend_presentation').switchClass( 'available', 'notActive', 0 );
		$('#attendee_list_toggle_wrap').show();
		window.top.navBar.presentationButton.removeClass( 'active' );
		if(!window.top.webApp.userIsSpeaker()) {
			window.top.navBar.presentationButton.addClass( 'notActive' ).onClick(jQuery.noop);
		}
		window.top.navBar.annotatorButton.disable();
		Datacore.removeAllPresentationUsers();
		Social.drawAttendeeList();
	},

	presentationConfirmJoin : function()
	{
		if( $('#dialog1Btn').attr( 'data-tag' ) === 'PresentationEnded' ) {
			dismissDialog1Btn();
		}
		webApp.toggleAttendPresentationBtn(true, true);
		$('#ipad_container_frame').contents().find('#attend_presentation').on('click',window.top.webApp.presentationJoin);
		if(!window.top.webApp.isDialogUp()) {
			if(window.top.webApp.isGuest() && window.top.webApp.viewerHasChanges()) {
				window.top.webApp.presentationJoinSave();
			} else {
				var dialogButtons = [];
				dialogButtons.push( {label: 'Cancel', action : 'dismissDialog3Btn();', class : 'app_bttn_cancel'} );
				dialogButtons.push( {label: 'Join', action : window.top.webApp.presentationJoinSave, class : 'app_bttn_new'} );
				var confirmPass = 'Would you like to join the Presentation?';
				showDialog3Btn( 'Presentation Started!', confirmPass, dialogButtons, true, null, 'JoinPresentationStarted' );
			}
		} else {
			window.top.showTooltip( 'Presentation Started!', true );
		}
	},

	presentationJoin : function() {
		dismissDialog3Btn();
		var depositionID = Datacore.getRootDepositionID();
		presentationJoin( depositionID, true );
	},

	presentationLeave : function() {
		$('#attendee_list_toggle_wrap').show();
	},

	transcriptClose: function() {
		if ($('#transcript_contents').hasClass('open')) {
			savedTranscriptPos = $('#liveFeed')[0].scrollTop;
			$('#transcript_contents').switchClass( 'open', '', 300, 'easeInOutQuad' );
			$('#social_transcript_tab').switchClass( 'open', '', 300, 'easeInOutQuad' );
		}
		$('#transcript_icon').hide();
	},

	reset: function() {
		//console.log( 'Social.reset' );
		$('#ipad_container_frame, #attendee_list_toggle_wrap, #ipad_navbar, #social_list_container').removeClass( 'showSocialList' ).addClass( 'hideSocialList' );
		$('#social_list_container, #social_transcript_tab, #liveFeed', window.top.document).removeClass( 'open classic fullscreenShort fullscreenTall' );
		$('#attendee_list_toggle_wrap').hide();
		$('#social_list_container').hide();
		$('#social_chat_window').hide();
		$('#social_attendee_container').removeClass( 'collapsed' );
		$('#chat_contents').removeAttr( 'style' );
		//$('#transcript_icon').hide();
		Social.transcriptClose();
		Social.currentGroup = null;
		Social.currentRoom = null;
		Social.chatHistoryHeightBig = null;
		Social.chatHistoryHeightSmall = null;
		window.clearTimeout( Social.mouseDownTimerID );
		Social.mouseDownTimerID = null;
		Social.transcriptCount = 1;
		Social.transcriptAtBottom = true;
	}
};

if ($('#attendee_list_toggle').length > 0) {
	$('#attendee_list_toggle_wrap').click( function() {
		Social.toggleAttendeeList();
	} );
}

if ($('#chatentry').length > 0) {
	$('#chatentry').keyup( function( e ) {
		if (e.keyCode === 13 && e.shiftKey === false) {
			var msgToSend = $('#chatentry').val().scaTrim();
			if (msgToSend.length > 0) {
				sendChatMessage( msgToSend, Social.currentRoom );
				$('#chatentry').val( '' );
			}
		}
	} );
}

if ($('#users_group_add').length > 0) {
	$('#users_group_add').click( function() { Social.showGroupEdit(); } );
}

if ($('#users_group_edit_cancel').length > 0) {
	$('#users_group_edit_cancel').click( function() {
		Social.showAttendeeList();
	} );
}

if ($('#role_button').length > 0) {
	$('#role_button').click( function() { Social.changeRole(); } );
}

if ($('#group_name_edit_container').length > 0) {
	$('#group_name_edit_container #group_name_edit').keyup( function( e ) {
		var editor_group_name = $('#group_name_edit_container #group_name_edit').val();
		var Group = Datacore.getRoom( Datacore.getS('editorGroupID') );
		if (Group) {
			if (editor_group_name.scaTrim() === Group.title) {
				if ($('#group_form_action #group_form_action_delete').hasClass( 'inactive')) {
					Social.enableGroupDeleteButton();
				}
			} else {
				if (!$('#group_form_action #group_form_action_delete').hasClass( 'inactive')) {
					Social.disableGroupDeleteButton();
				}
			}
		}

		if (editor_group_name.scaTrim().length > 0) {
			Social.setGroupAttendeesActive();
		} else {
			Social.setGroupAttendeesInactive();
		}
		$('#group_name_edit_container #group_name_edit').val( editor_group_name.scaLTrim() );
	} );

	$('#group_name_edit_container #clear-input').click( function() {
		Social.setGroupAttendeesInactive();
		Social.disableGroupDeleteButton();
		$('#group_name_edit_container #group_name_edit').val( '' );
	} );
}

if ($('#group_form_action').length > 0) {
	if ( !$('#group_form_action_save').hasClass('inactive') ) {
		Social.enableGroupSaveButton();
	}
	if ( !$('#group_form_action_delete').hasClass('inactive') ) {
		Social.enableGroupDeleteButton();
	}
}
var attendPresentationBtn = $('#ipad_container_frame').contents().find('#attend_presentation');
if (attendPresentationBtn.length > 0) {
	attendPresentationBtn.click( function() {
		if (Datacore.getS('presentationAvailable')) {
			Social.presentationJoin();
		}
	} );
}

if ($('#transcript_icon').length > 0) {
	$('#transcript_icon').click( function() {
		//Scroll to the bottom of the scroll window.
		$('#liveFeed').animate( {scrollTop: $('#liveFeed').get( 0 ).scrollHeight}, 100 );
//		$('#transcript_icon').hide();
	} );
}

var savedTranscriptPos = 0;
if ($('#social_chat_tab').length > 0) {
	$('#social_chat_tab').click( function() {
		if ($('#transcript_contents').hasClass('open')) {
			savedTranscriptPos = $('#liveFeed')[0].scrollTop;
			$('#transcript_contents').switchClass( 'open', '', 300, 'easeInOutQuad' );
			$('#social_transcript_tab').switchClass( 'open', '', 300, 'easeInOutQuad' );
		}
		$('#transcript_icon').hide();
	});
}

if ($('#transcript').length > 0) {
	$('#transcript').click( function() {
		var onComplete = function() {
			var rcEventID = $('#rcEventID');
			if( rcEventID.is(':visible') ) {
				rcEventID.focus();
			}
		};
		if (!$('#transcript_contents').hasClass('open')) {
			$('#liveFeed')[0].scrollTop = savedTranscriptPos;
			$('#transcript_contents').addClass( 'open', 300, 'easeInOutQuad', onComplete );
			$('#social_transcript_tab').addClass( 'open', 300, 'easeInOutQuad' );

			if( $('#liveFeed').is( ':visible' ) ) {
				// See if the user is not at the bottom of the scroll area.
				var contents = $('#liveFeed')[0];
				if (savedTranscriptPos !== 0 && contents.scrollHeight - contents.scrollTop !== contents.clientHeight) {
					$('#transcript_icon').show();
				}
			}
		}
	});
}
