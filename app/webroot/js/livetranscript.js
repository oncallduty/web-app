var LiveTranscripts = function( forDepositionID )
{
	console.log( 'LiveTranscripts', forDepositionID );

	var _history = [];
	var _pageNum = 1;
	var _lineNum = 1;
	var _bufferMessages = true;
	var _messageQueue = [];
	var _atBottom = true;
	var _autoScroll = true;
	var _isScrolling = false;
	var _scrollTimeout = null;
	var _deposition = null;
	var _userID = null;
	var _isStreaming = false;
	var liveFeed = $('#liveFeed');
	var txWrap;
	var txScrollIcon = $('#transcript_icon');
	var inEventID = $('#rcEventID');
	var inAccessKey = $('#rcAccessKey');
	var btnCancel = $('#rcLogin .btnCancel');
	var btnOK = $('#rcLogin .btnOK');
	var btnReset = $('#rcPoweredBy .txReset');

	var tx = this;

	_deposition = window.top.webApp.getS( 'deposition' );
	if( typeof _deposition === 'undefined' || _deposition === null ) {
		console.error( 'Missing deposition' );
		delete window.top.liveTranscript;
		return;
	}

	_userID = window.top.webApp.getS( 'user.ID' );
	if( typeof _userID === 'undefined' || _userID === null ) {
		console.error( 'Missing user' );
		delete window.top.liveTranscript;
		return;
	}

	if( _deposition.class === 'WitnessPrep' || _deposition.class === 'WPDemo' ) {
		console.error( 'WitnessPrep does not have live transcripts' );
		delete window.top.liveTranscript;
		return;
	}

	var resolveBackspaces = function( inString )
	{
		var i = 0;
		while( inString.indexOf( "\x08" ) !== -1 ) {
			inString = inString.replace( /.?\x08/, "" );
			++i;
			if( i >= 500 ) {
				inString.replace( /\x08/g, "" );
				break;
			}
		}
		return inString;
	};

	var checkAutoScroll = function()
	{
//		console.log( '_atBottom:', _atBottom );
		_autoScroll = (_atBottom);
		if( !_atBottom ) {
			txScrollIcon.show();
		}
	};

	var liveFeedOnScroll = function()
	{
//		console.log( this.scrollHeight, this.scrollTop, (this.scrollHeight - this.scrollTop), this.clientHeight, ((this.scrollHeight - this.scrollTop) === this.clientHeight) );
		if( _isScrolling === true ) {
			return;
		}
		if( (this.scrollHeight - this.scrollTop) === this.clientHeight ) {
			txScrollIcon.hide();
			_atBottom = true;
		} else {
			_atBottom = false;
		}
		_autoScroll = _atBottom;
		if( !_atBottom ) {
			setTimeout( checkAutoScroll, 300 );
		}
	};
	liveFeed.scroll( liveFeedOnScroll );

	var toggleLogin = function()
	{
		if( inEventID.val().length > 0 && inAccessKey.val().length > 0 ) {
			if( btnOK.hasClass( 'disabled' ) ) {
				btnOK.removeClass( 'disabled' );
			}
		} else {
			if( !btnOK.hasClass( 'disabled' ) ) {
				btnOK.addClass( 'disabled' );
			}
		}
	};

	inEventID.keyup( toggleLogin );
	inEventID.blur( toggleLogin );
	inAccessKey.keyup( toggleLogin );
	inAccessKey.blur( toggleLogin );

	var loginCancelAction = function()
	{
//		console.log( 'LiveTranscript.loginCancelAction' );
		$('#social_chat_tab').trigger( 'click' );
	};
	$('#rcLogin .btnCancel').off().on( 'click', loginCancelAction );

	var resetCancelAction = function()
	{
//		console.log( 'LiveTranscript.resetCancelAction' );
		$('#rcResetWrap').hide();
		btnReset.off().on( 'click', resetPrompt );
	};

	var resetContinueAction = function()
	{
//		console.log( 'LiveTranscript.resetContinueAction' );
		$('#rcResetWrap').hide();
		btnReset.off().hide();
		window.top.webApp.removeItem( 'liveTranscriptAuth' );
		sio.emit( 'resetTranscript', {}, function( res ) {
			if( typeof res === 'object' && res !== null ) {
				if( typeof res.success !== 'undefined' ) {
					if( !res.success ) {
						if( typeof res.error === 'string' && res.error !== null ) {
							$('#rcError .errMsg').html( res.error );
						} else {
							$('#rcError .errMsg').html( 'Operation failed.' );
						}
						tx.reset();
						$('#liveFeed').empty().hide();
						txScrollIcon.hide();
						$('#rcLogin').hide();
						$('#rcError').show();
						$('#rcAuth').show();
					}
				}
			}
		} );
	};

	var resetPrompt = function()
	{
//		console.log( 'LiveTranscript.resetPrompt' );
		$('#rcResetDialog .btnCancel').off().on( 'click', resetCancelAction );
		$('#rcResetDialog .btnOK').off().on( 'click', resetContinueAction );
		$('#rcResetWrap').show();
	};

	var cancelAction = function()
	{
//		console.log( 'LiveTranscript.cancelAction' );
		var rcError = $('#rcError');
		inEventID.val( '' );
		inAccessKey.val( '' );
		toggleLogin();
		if( rcError.is( ':visible' ) ) {
			rcError.hide();
			$('#rcLogin').show();
		}
		$('#social_chat_tab').trigger( 'click' );
	};
	$('#rcError .btnCancel').off().on( 'click', cancelAction );

	var retryAction = function()
	{
		inEventID.val( '' );
		inAccessKey.val( '' );
		toggleLogin();
		$('#rcError').hide();
		$('#rcLogin').show();
		inEventID.focus();
	};
	$('#rcError .btnOK').off().on( 'click', retryAction );

	var animateScrollComplete = function()
	{
//		console.log( 'animateScrollComplete' );
		_isScrolling = false;
	};
	var animateScroll = function()
	{
//		console.log( 'animateScroll', liveFeed.prop( "scrollHeight" ) );
		liveFeed.animate( {"scrollTop":liveFeed.prop( "scrollHeight" )}, 500, 'swing', animateScrollComplete );
	};
	var scrollToBottom = function()
	{
//		console.log( 'scrollToBottom' );
		_isScrolling = true;
		clearTimeout( _scrollTimeout );
		_scrollTimeout = setTimeout( animateScroll, 200 );
	};

	function prepareAuth()
	{
		if( _deposition.class !== 'Deposition' ) {
			return;
		}
		var auth = window.top.webApp.getItem( 'liveTranscriptAuth' );
		if( typeof auth === 'string' && auth !== null ) {
			var authInfo = null;
			try {
				authInfo = JSON.parse( auth );
			} catch( e ) {
				console.error( e );
			}
			if( authInfo === null ) {
				return;
			}
			if( typeof authInfo.depositionID !== 'undefined' && authInfo.depositionID === _deposition.ID ) {
				if( typeof authInfo.userID !== 'undefined' && authInfo.userID === _userID ) {
					if( typeof authInfo.eventID === 'string' && typeof authInfo.accessKey === 'string' ) {
						inEventID.val( authInfo.eventID );
						inAccessKey.val( authInfo.accessKey );
						toggleLogin();
					}
				}
			}
		}
	};

	this.depositionID = parseInt( forDepositionID );

	this.authenticate = function( event, prepare )
	{
		console.log( 'authenticate' );
		prepare = !!prepare;
		if( prepare ) {
			prepareAuth();
		}
		var eventID = inEventID.val();
		var accessKey = inAccessKey.val();
		if( eventID.length <= 0 || accessKey.length <= 0 ) {
//			console.log( 'missing:', [eventID], [accessKey] );
			return;
		}
		sio.emit( 'joinTranscript', {"eventID":eventID,"accessKey":accessKey}, function( res ) {
//			console.log( res );
			if( typeof res === 'object' && res !== null ) {
				if( typeof res.success !== 'undefined' ) {
					if( !!res.success && typeof res.transcriptHistory !== 'undefined' ) {
						_isStreaming = true;
						inEventID.val( '' );
						inAccessKey.val( '' );
						$('#rcAuth').hide();
						$('#liveFeed').show();
						if( _deposition.class === 'Deposition' ) {
							var authInfo = {"depositionID":_deposition.ID, "userID":_userID, "eventID":eventID, "accessKey":accessKey};
							window.top.webApp.setItem( 'liveTranscriptAuth', JSON.stringify( authInfo ) );
						}
						if( !_deposition.parentID && _deposition.ownerID === _userID ) {
							btnReset.off().on( 'click', resetPrompt );
							btnReset.show();
						}
						tx.prepareFromHistory( res.transcriptHistory );
						return;
					} else {
						if( typeof res.error === 'string' && res.error !== null && res.error.length > 0 ) {
							$('#rcError .errMsg').html( res.error );
						} else {
							$('#rcError .errMsg').html( 'The Event ID or Access Key were incorrect.' );
						}
					}
				}
			}
			$('#rcLoading').hide();
			$('#rcError').show();
		} );
		$('#rcLogin').hide();
		$('#rcLoading').show();
	};
	btnOK.off().on( 'click', this.authenticate );

	this.reset = function()
	{
		console.log( 'LiveTranscript::reset' );
		_messageQueue = [];
		_bufferMessages = true;
		_autoScroll = true;
		_history = [];
		_isStreaming = false;
		inEventID.val( '' );
		inAccessKey.val( '' );
		toggleLogin();
		btnReset.off().hide();
		txScrollIcon.hide();
		$('#liveFeed').hide();
		$('#rcError').hide();
		$('#rcLoading').hide();
		$('#rcLogin').show();
		$('#rcAuth').show();
	};

	this.prepareFromHistory = function( history )
	{
		_messageQueue = [];
		$('#rcAuth').hide();
		$('#liveFeed').show();
		_history = history.replace( /\x0d/g, "" ).split( "\n" );
		var messages = ['<div class="txWrap">'];
		for( var i=0; i<_history.length; ++i ) {
			var line = _history[i];
			_lineNum = (i % 25) + 1;
			var pageNum = '';
			if( _lineNum === 1 ) {
				_pageNum = Math.floor( i / 25 ) + 1;
				pageNum = [_pageNum, ":"].join( "" );
			}
			var msg = ['<div id="ln', (i + 1), '" class="txLine"><div class="txMargin">', pageNum, _lineNum, '</div><div class="txMsg">', line, "</div></div>"].join( "" );
			messages.push( msg );
		}
		messages.push( '</div>' );
		liveFeed.html( messages.join( "" ) );
		txWrap = $('#liveFeed .txWrap');
		$('#rcLoading').hide();
		_bufferMessages = false;
		if( _autoScroll ) {
			scrollToBottom();
		}
	};

	this.addMessage = function( message )
	{
		if( _bufferMessages ) {
			_messageQueue.push( message );
			return;
		} else {
			_messageQueue.push( message );
			message = _messageQueue.join( "" );
			_messageQueue = [];
		}
		var lastIdx = _history.length - 1;
		var lastItem = _history.slice( -1 );
		var updateStr = resolveBackspaces( [lastItem, message].join( "" ).replace( /\x0d/g, "" ) );
		var lines = updateStr.split( "\n" );
//		console.log( 'lines', lines );
		var pageNum = '';
		var messages = [];
		for( var i=0; i<lines.length; ++i ) {
			var line = lines[i];
			if( i === 0 ) {
				_history[lastIdx] = line;
				if( _lineNum === 1 ) {
					pageNum = [_pageNum, ":"].join( "" );
				}
				var updateHTML = ['<div class="txMargin">', pageNum, _lineNum, '</div><div class="txMsg">', line, '</div>'].join( "" );
				$('#liveFeed .txWrap #ln' + (lastIdx + 1)).html( updateHTML );
			} else {
				++_lineNum;
				if( _lineNum > 25 ) {
					_lineNum = 1;
					++_pageNum;
					pageNum = [_pageNum, ":"].join( "" );
				}
//				console.log( [line] );
				_history.push( line );
				messages.push( ['<div id="ln', _history.length, '" class="txLine"><div class="txMargin">', pageNum, _lineNum, '</div><div class="txMsg">', line, "</div></div>"].join( "" ) );
			}
		}
//		console.log( 'messages:', messages );
		var updateMsgs = messages.join( "" );
//		console.log( 'addMessage; updateMsgs:', [updateMsgs], updateMsgs.length );
		if( updateMsgs.length > 0 ) {
//			liveFeed.scrollTop( liveFeedEl.scrollHeight );
			txWrap.append( updateMsgs );
		}
//		console.log( 'addMessage; autoScroll:', _autoScroll );
		if( _autoScroll ) {
//			console.log( '1,scrollHeight:', liveFeed.prop( "scrollHeight" ) );
			scrollToBottom();
		}
	};

	tx.reset();
};