
/**
 * WebApp
 */
var WebAppClass = (function() {

	var _instance;
	var _s = {};

	var PORTRAIT = "portrait";
	var LANDSCAPE = "landscape";

	/**
	 * Index; property of object
	 * @param {Object} o
	 * @param {String} i
	 * @returns {o[ i ]}
	 */
	var idx = function( o, i ) {
		if( !isset( o ) ) {
			return;
		}
		return o[ i ];
	};

	function isset( v ) {
		return (typeof v !== "undefined" && v !== null);
	}
	window.top.isset = isset;

	function WebAppClass() {

		if( !(this instanceof WebAppClass) ) {
			return new WebAppClass();
		}

		var self = this;
		var onReadyQueue = [];
		var preWebAppReadyState = true;
		var viewerAPI = null;
		var annotateRegistry = {};
		var _annotateTempFile = 0;
		var ignoreNextUnload = false;
		var presentationEditHistory = null;
		var introduceEditHistory = null;

		var WebAppReady = function() {
//			console.log( 'WebAppClass::WebAppReady()' );
			preWebAppReadyState = false;
			while( onReadyQueue.length ) {
				onReadyQueue.shift().call();
			}
		};

		this.onReady = function onReady( func ) {
//			console.log( 'WebAppClass::onReady()');
			if( typeof func === 'function' ) {
				onReadyQueue.push( func );
			}
			if( window.top.document.readyState === 'interactive' || window.top.document.readyState === 'complete' ) {
				WebAppReady();
			}
		};

		this.setS = function setS( prop, val ) {
			_s[prop] = val;
		};

		this.getS = function getS( prop ) {
			if( typeof prop !== 'string' ) return;
			return prop.split( '.' ).reduce( idx, _s );
		};

		this.bindViewer = function bindViewer( viewer ) {
			self.setS("isBackEvent", false);
			if( ignoreNextUnload && viewer === null ) {
				ignoreNextUnload = false;
				return;
			}
			if( typeof viewer === "object" || viewer === null ) {
				viewerAPI = viewer;
				if( isset( viewerAPI ) && typeof viewerAPI.setActivityCallback === "function" ) {
					viewerAPI.setActivityCallback( function( action ) {
						getActivityFeedback( action );
					} );
					viewerAPI.setThumbnailEventsHandler( onThumbNotice, onThumbPageNotice );
					viewerAPI.hideDuplicateWebAppBtns();
					viewerAPI.onReady( self.viewerOnReady );
					viewerAPI.onSavable( self.viewerOnSavableStateChange );
					viewerAPI.onPresenterMessage( self.viewerOnPresenterMessage );
					viewerAPI.setArrowColorCallback(function(color){$('#btnArrowColorDisplay').css('background-color',color);});
					viewerAPI.setHighlightColorCallback(function(color){$('#btnMarkerColorDisplay').css('background-color',color);});
					viewerAPI.setPencilColorCallback(function(color){$('#btnPencilColorDisplay').css('background-color',color);});
					viewerAPI.setNoteColorCallback(function(color){$('#btnNoteColorDisplay').css('background-color',color);});
				}
				if( isset( viewerAPI ) && typeof viewerAPI.setAppOrientationDelegate === "function" ) {
					viewerAPI.setAppOrientationDelegate( handleAppOrientation );
					if( [ "G", "W" ].indexOf( self.getS( "user.typeID" ) ) >= 0 ) {
						//console.log( "WebApp.bindViewer; stickyOrientation:", self.getItem( "stickyOrientation" ) );
						var sOrientation = self.getItem( "stickyOrientation" );
						var appOrientation = self.getItem( "appOrientation" );
						if( appOrientation !== sOrientation ) {
							handleAppOrientation( {"orientation": sOrientation} );
						}
					}
				}
			}
		};

		this.isAPI = function isAPI() {
			return isset(viewerAPI);
		};

		this.viewerOnReady = function viewerOnReady() {
			if( self.hasItem( "appOrientation" ) && isset( viewerAPI ) ) {
				self.isAPI() && viewerAPI.appOrientationDidChange( self.getItem( "appOrientation" ) );
			}
			if(self.getS('beginIntroduce')==='yes') {
				//console.log("WebApp::viewerOnReady()::introduceSendEditHistory()", introduceEditHistory);
				self.introduceSendEditHistory();
				self.introduce(false);
			}
			if(self.getS('presentation')==='yes') {
				var pData = window.top.webApp.getS( 'joinPresentationData' );
				//console.log( "viewerOnReady; pData:", pData );
				if( window.top.webApp.userIsSpeaker() === true ) {
					self.isAPI() && viewerAPI.activatePresenterMode();
					self.isAPI() && viewerAPI.setPresenterFirstLoad( pData );
					console.log("WebApp::viewerOnReady()::presentationSendEditHistory()", presentationEditHistory);
					self.presentationSendEditHistory();
					if(self.getS("presentationPaused")) {
						viewerAPI.showPauseIndicator();
					}
				} else {
					self.isAPI() && viewerAPI.activatePresentationMode();
				}
				//window.top.hideSpinner();
				var presentationOrientation = (self.getS("presentationOrientation")==="portrait" || (isset(pData) && pData.orientation==="portrait")) ? "portrait" : "landscape";
				self.isAPI() && viewerAPI.setPresentationOrientation(presentationOrientation);
				self.witnessAnnotatorCheck(pData);
				if(self.getS('beginIntroduce')!=='yes') {
					self.presentationPageAdd(pData);
				}
				getAnnotations(presentationGetDepositionID());
				// getCallouts(presentationGetDepositionID()); // disabled for v2.2 release
				window.top.navBar.backButton.onClick( window.top.webApp.backButton );
				!self.isWitness() && !self.isOwner() && window.top.Social.hideChatAction(); // don't allow pass the role
				self.presentationRestorePageState();
			}
			if(window.top.navBar.getPathname()==="/depositions/pdf") {
				if(self.isTempFile()) {
					self.viewerOnSavableStateChange(true);
				}
				if(self.isWitness()) {
					self.setHighlightDrawColor('#0090ff');
					self.setPencilDrawColor('#0090ff');
				}
				if(self.pCloseOut.fileID > 0) {
					self.closeOutSendChangePage();
					self.closeOutSendEditHistory();
				}
			}
			self.setArrowDrawColor(self.getArrowDrawColor());
			self.setHighlightDrawColor(self.getHighlightDrawColor());
			self.setPencilDrawColor(self.getPencilDrawColor());
			self.setNoteDrawColor(self.getNoteDrawColor());
			window.top.dismissLoadingOverlay();
			if(self.getItem('contentSearchTerms')) {
				self.searchMultiple(JSON.parse(self.getItem('contentSearchTerms')));
				self.setItem('contentSearchTerms',false);
			}
		};
		this.viewerOnSavableStateChange = function viewerOnSavableStateChange(savable) {
			if(savable) {
				if( window.top.Datacore.getS( 'depositionStatus' ) === 'I' && self.userIsSpeaker() === true && self.iAmAnnotator()) {
					var fn = self.introduce;
					if( self.getS( 'deposition.class' ) === 'WitnessPrep' || self.getS( 'deposition.class' ) === 'WPDemo' ) {
						fn = self.witnessPrepIntroduce;
					}
					if(window.top.navBar.getPathname() !== '/depositions/introduce') {
						window.top.navBar.introduceButton.label('Introduce').onClick(fn).removeClass('green').addClass('dark').enable();
					}
				}
				// enable
				var userType = self.getS( 'user.userType' );
				if( userType !== 'G' ) {
					window.top.navBar.saveButton.removeClass('dark');
				}
			}
			else {
				window.top.navBar.saveButton.addClass('dark');
				if( window.top.Datacore.getS( 'depositionStatus' ) === 'I' && self.userIsSpeaker() === true && self.isExhibit(self.getS('viewerFile.ID'))) {
					window.top.navBar.introduceButton.onClick(jQuery.noop).disable();
				}
			}
		};
		this.viewerOnPresenterMessage = function viewerOnPresenterMessage(message, data, depoId) {
			if(typeof depoId === "undefined") {
				depoId = presentationGetDepositionID();
			}
			if(typeof self.getS("presentationPaused") === "undefined") {
				self.setS("presentationPaused", false);
			}
			if(typeof self.getS("pauseQueue") === "undefined"
					|| (!!self.getS("presentationPaused")&& message === "PresentationFile")) {
				self.setS("pauseQueue", []);
			}
			if(!!self.getS("presentationPaused")) {
				self.getS("pauseQueue").push([depoId, message, data]);
				return;
			}
			if( window.top.Datacore.s.inPresentation && self.iAmAnnotator() ) {
				console.log( "WebApp::PresenterMessage;", message, data );
				delete data.Message;
				switch( message ) {
					case 'PresentationFile':
						window.top.setPresentationFile( depoId, data );
						break;
					case 'PageInfo':
						if( self.getS( "presentationOrientation" ) && isset( data.orientation ) ) {
							data.orientation = self.getS( "presentationOrientation" );
						}
						if( isset( viewerAPI ) && isset( data.pageOrientation ) ) {
							data.pageScale = viewerAPI.getPresentationPageScale();
						}
						if(JSON.stringify(data) === "{}" || self.getS('beginIntroduce')==='yes') {
							return;
						}
						window.top.pageProperties( depoId, data );
						break;
					case 'ClearAnnotations':
						window.top.clearAll( depoId );
						break;
					case 'UndoAnnotation':
						window.top.undoAnnotation( depoId );
						break;
					case 'InitialAnnotation':
						window.top.initialAnnotation( depoId, data );
						break;
					case 'AddAnnotation':
						window.top.addAnnotation( depoId, data );
						break;
					case 'ModifyAnnotation':
						var Initial = data.Initial;
						if(Initial !== false) {
							Initial.Page = data.Page;
							$.when(window.top.initialAnnotation( depoId, Initial ))
							 .then(function() {
								delete data.Initial;
								window.top.modifyAnnotation( depoId, data ); // modify finally
							 },function() {
								setTimeout(function(){self.viewerOnPresenterMessage(message,data);},3000); // try again in 3 sec
							 });
						}
						else {
							delete data.Initial;
							window.top.modifyAnnotation( depoId, data ); // modify immediate
						}
						break;
					case 'DeleteAnnotation':
						var Initial = data.Initial;
						if(Initial !== false) {
							$.when(window.top.initialAnnotation( depoId, Initial ))
							 .then(function() {
								delete data.Initial;
								window.top.deleteAnnotation( depoId, data ); // delete finally
							 },function() {
								setTimeout(function(){self.viewerOnPresenterMessage(message,data);},3000); // try again in 3 sec
							 });
						}
						else {
							delete data.Initial;
							window.top.deleteAnnotation( depoId, data ); // delete immediate
						}
						break;
					case 'AddCallout':
						window.top.addCallout( depoId, data );
						break;
					case 'ModifyCallout':
						window.top.modifyCallout( depoId, data );
						break;
					case 'DeleteCallout':
						window.top.deleteCallout( depoId, data );
						break;
					case 'SetCalloutZIndexes':
						window.top.setCalloutZIndex( depoId, data );
						break;
				}
			}
		};

		this.inDeposition = function inDeposition( inDepo ) {
			if( typeof inDepo !== 'undefined' && inDepo !== null ) {
				self.setS( 'inDeposition', !!inDepo );
			}
			return true;
		};

		this.getSpeakerID = function getSpeakerID() {
			var inDepo = !!self.getS( 'inDeposition' );
			var speakerID = -1;
			if( inDepo ) {
				speakerID = parseInt( self.getS( 'deposition.speakerID' ) );
			}
			return (inDepo && speakerID > 0) ? speakerID : false;
		};

		this.userIsSpeaker = function userIsSpeaker( userID ) {
			if( typeof userID === 'undefined' || userID === null ) {
				userID = self.getS( 'user.ID' );
				if( typeof userID === 'undefined' || userID === null ) {
					return false;
				}
			}
			var inDepo = !!self.getS( 'inDeposition' );
			var speakerID = self.getSpeakerID();
			return (inDepo && speakerID > 0 && userID === speakerID);
		};

		this.getOwnerID = function getOwnerID() {
			var inDepo = !!self.getS( 'inDeposition' );
			var ownerID = -1;
			if( inDepo ) {
				ownerID = parseInt( self.getS( 'deposition.ownerID' ) );
			}
			return (inDepo && ownerID > 0) ? ownerID : false;
		};

		this.userIsOwner = function userIsOwner( userID ) {
			if( typeof userID === 'undefined' || userID === null ) {
				userID = self.getS( 'user.ID' );
				if( typeof userID === 'undefined' || userID === null ) {
					return false;
				}
			}
			var inDepo = !!self.getS( 'inDeposition' );
			var isParentDepo = ( parseInt( self.getS( 'deposition.parentID' ) ) > 0 ) ? false : true;
			var ownerID = self.getOwnerID();
			return (inDepo && isParentDepo && userID === ownerID);
		};

		this.iAmAnnotator = function iAmAnnotator() {
			if((self.userIsSpeaker() && !self.passedPresentor) || self.iAmPresentor) {
				return true;
			}
			return false;
		};

		this.enterFullscreen = function enterFullscreen() {
			window.top.iPadContainer.addClass( 'fullscreen' );
			$('#file_contents', window.top.iPadContainerFrame.document).addClass( 'fullscreen' );
			window.top.Datacore.s.fullscreen = true;
			self.setS( 'fullscreen', true );
			window.top.navBar.canHideToolbar(true);
		};

		this.exitFullscreen = function exitFullscreen() {
			window.top.iPadContainer.removeClass( 'fullscreen' );
			if( typeof window.top.iPadContainerFrame !== 'undefined' && window.top.iPadContainerFrame !== null ) {
				$('#file_contents', window.top.iPadContainerFrame.document).removeClass( 'fullscreen' );
				if( window.top.iPadContainerFrame.location.pathname !== '/cases' ) {
					$('#attendee_list_container', window.top.document).show();
				}
			}
			if( typeof window.top.Datacore !== 'undefined' && window.top.Datacore !== null ) {
				window.top.Datacore.s.fullscreen = false;
			}
			self.setS( 'fullscreen', false );

		};

		this.enableTranscripts = function enableTranscripts() {
			var enable = false;
			var userType = self.getS( 'user.userType' );
			var liveTranscript = self.getS( 'deposition.liveTranscript' );
			if( typeof userType === 'string' && userType === 'M' ) {
				if( typeof liveTranscript === 'string' ) {
					enable = (liveTranscript === 'Y');
				}
			}
			console.log( 'enableTranscripts:', enable, userType, liveTranscript );
			return enable;
		};

		this.abortTempFile = function abortTempFile() {
			if (!isset(window.top.Datacore.s.tempFile)) {
				return;
			}
			$.getJSON("/depositions/abortTempFile?fileID="+window.top.Datacore.s.tempFile.fileID, null, function(data, textStatus, xhr) {
				//console.log("webApp::abortTempFile()", "fileID:", window.top.Datacore.s.tempFile.fileID, "textStatus:", textStatus, "data:", data);
				if (textStatus === "success") {
					window.top.Datacore.s.tempFile = null;
					window.top.Datacore.syncFolder( window.top.webApp.getPersonalFolderID(), function() {
						console.log("webApp::abortTempFile -- syncFolder", window.top.webApp.getPersonalFolderID());
						window.top.Datacore.refreshFolders();
					});
				} else {
					console.log("webApp::abortTempFile", "Received", textStatus, "from the server");
				}
			})
			.fail(function() {
				window.top.showPlainDialog1Btn( 'Error', 'Failed to remove temp file.', 'Close' );
			});
		};

		this.isTempFile = function isTempFile() {
			//console.log("isTempFile()", isset(window.top.Datacore.s.tempFile));
			if( isset(window.top.Datacore.s.tempFile) ) {
				return true;
			}
			return false;
		};

		this.getPersonalFolderID = function getPersonalFolderID() {
			//console.log( "webApp::getPersonalFolderID" );
			var userType = self.getS( 'user.userType' );
			var folders = window.top.Datacore.getFolders();
			for( var f in folders ) {
				var folder = folders[f];
				if( folder.class === 'Personal' || (userType === 'WM' && folder.class === 'WitnessAnnotations') ) {
					self.setS( "personalFolderID", folder.ID );
					return folder.ID;
				}
			}
			console.warn( "webApp::getPersonalFolderID; Unable to determine personal folder" );
		};

		this.backButton = function backButton() {
			//console.log( 'webApp::backButton', window.top.navBar.pathname );
			self.setS( 'isBackEvent', true );
			switch( window.top.navBar.pathname ) {
				case "/depositions/pdf":
					PDFBackButton();
					break;
				case "/depositions/introduce":
					IntroduceBackButton();
					break;
				case "/depositions/presentation":
					PresentationBackButton();
					break;
			}
		};

		this.introduce = function introduce() {
			var introduce = this;
			var first = false;

			if (window.top.Datacore.s.exhibitTitle.length === 0 && window.top.Datacore.s.exhibitSubTitle.length === 0) {
				var now = new Date();
				window.top.Datacore.s.exhibitTitle = window.top.Datacore.s.depositionOf;
				window.top.Datacore.s.exhibitSubTitle = 'Exhibit_0';
				window.top.Datacore.s.exhibitDate = (now.getMonth()+1)+'/'+now.getDate()+'/'+now.getFullYear();
				first = true;
			}
			if(window.top.Datacore.s.exhibitDate.length === 0) {
				var now = new Date();
				window.top.Datacore.s.exhibitDate = (now.getMonth()+1)+'/'+now.getDate()+'/'+now.getFullYear();
			}
			else {
				// validate date
				var now = new Date();
				var nowMonth = now.getMonth()+1;
				var nowDay = now.getDate();
				var nowYear = now.getFullYear();
				var pieces = window.top.Datacore.s.exhibitDate.split("/");
				var currMonth = parseInt(pieces[0]);
				var currDay = parseInt(pieces[1]);
				var currYear = parseInt(pieces[2]);
				if((currYear < nowYear)
						|| (currYear === nowYear && currMonth < nowMonth)
						|| (currYear === nowYear && currMonth === nowMonth && currDay < nowDay)) {
					window.top.Datacore.s.exhibitDate = (now.getMonth()+1)+'/'+now.getDate()+'/'+now.getFullYear();
				}
			}
			this.data = {
				title: window.top.Datacore.s.exhibitTitle,
				subtitle: window.top.Datacore.s.exhibitSubTitle,
				date: window.top.Datacore.s.exhibitDate,
				orientation: 0,
				centerX:0,
				centerY:0,
				width:0,
				height:116,
				scale:1.0
			};

			this.fileID = self.getS( 'viewerFile.ID' );
			this.exhibitFolderID = 0;
			this.isSkipStamp = false;
			this.blockButtons = false;
			var updatedFilename = null;
			_annotateTempFile = false;
			var overwrite = false;

			getExhibitFolder();

			if(isWitPrepIntroduce() || window.top.navBar.pathname === '/depositions/pdf' || window.top.navBar.pathname === '/depositions/presentation') {
				prepareIntroduce();
			} else {
				startIntroduce();
			}

			function startIntroduce() {
				window.top.navBar.sendButton.disable();
				window.top.navBar.saveButton.disable();
				enableIntroduceButton();
				window.top.navBar.backButton.onClick(function(){introduceBackButton();}).enable();
				window.top.navBar.editorActions.hide();
				var now = new Date();
				var minimumDate = (now.getMonth()+1)+'/'+now.getDate()+'/'+now.getFullYear();
				viewerAPI.introduceStart(window.top.Datacore.s.exhibitTitle, window.top.Datacore.s.exhibitSubTitle, window.top.Datacore.s.exhibitDate, minimumDate, first);
			};

			function prepareIntroduce() {
				window.top.navBar.sendButton.disable();
				window.top.navBar.saveButton.disable();
				enableIntroduceButton();
				window.top.navBar.backButton.onClick(function(){introduceBackButton();}).enable();
				window.top.navBar.editorActions.hide();

				var needToSave = viewerAPI.hasIntroduceChanges();
				if(needToSave) {
					introduceSaveFile();
				} else {
					if( introduce.fileID !== null && typeof window.top.Datacore.tempFiles !== 'undefined' && window.top.Datacore.tempFiles !== null && introduce.fileID in window.top.Datacore.tempFiles ) {
						_annotateTempFile = true;
					}
					introduceFile( introduce.fileID );
					if( window.top.Datacore.s.inPresentation && (window.top.webApp.getS( 'deposition.class' ) !== 'WitnessPrep' && window.top.webApp.getS( 'deposition.class' ) !== 'WPDemo') ) {
						if( typeof window.top.fileSelection !== 'undefined' && window.top.fileSelection !== null ) {
							window.top.fileSelection.disableFileSelection();
						}
					}
				}
				if( window.top.webApp.getS( 'deposition.class' ) === 'WitnessPrep' || window.top.webApp.getS( 'deposition.class' ) === 'WPDemo' ) {
					skipStamp();
				}
			};

			function isWitPrepIntroduce() {
				return (window.top.webApp.getS( 'deposition.class' ) === 'WitnessPrep' || window.top.webApp.getS( 'deposition.class' ) === 'WPDemo'
						|| window.top.webApp.getS( 'class' ) === 'WitnessPrep' || window.top.webApp.getS( 'class' ) === 'WPDemo');
			};

			function introduceSaveFile() {
				introduceEditHistory = null;
				if( self.viewerHasChanges() ) {
					introduceEditHistory = viewerAPI.getPresentationAnnotations();
					console.log(" -- INTRODUCE-CHANGES:", introduceEditHistory);
					if(isset(introduceEditHistory) && typeof introduceEditHistory.then === "function") {
						introduceEditHistory.then();
						delete introduceEditHistory.then;
					}
				}
				introduce.fileID = window.top.Datacore.s.inPresentation ? presentationGetFileID() : self.getS('viewerFile.ID');
				window.top.fileSelection.disableFileSelection();
				introduceFile(introduce.fileID);
			};

			function distributeSaveFile(afterAnnotateFn) {
				var pFileID = self.getS( 'joinPresentationData.fileID' );
				if( typeof pFileID !== 'undefined' && pFileID !== null && pFileID > 0 ) {
					introduce.fileID = pFileID;
				}
				if( updatedFilename === null ) {
					//introduce.fileID = window.top.Datacore.s.inPresentation ? Annotate.fileID : parent.requestedFileID;
					var dcFile = window.top.Datacore.getFile( introduce.fileID );
					if( typeof dcFile === 'undefined' || dcFile === null ) {
						console.log( '-- Introduce getFile is null.' );
						window.top.Datacore.syncAllFolders();
						return;
					}
					var index = dcFile.name.lastIndexOf( "." );
					updatedFilename = dcFile.name.substr( 0, index );
				}
				_annotateTempFile = true;
				var filename = updatedFilename;
				if( filename.substr( 0, 4 ) !== 'tmp_' ) {
					filename = 'tmp_' + filename;
				}
				var annotationChanges = viewerAPI.getAnnotationChanges();
				introduce.fileID = window.top.Datacore.s.inPresentation ? presentationGetFileID() : self.getS('viewerFile.ID');
				annotate(annotationChanges.annotations, introduce.fileID, false, introduce.fileID, filename, self.getPersonalFolderID(), function(newFileID){
					if (_annotateTempFile) {
						window.top.Datacore.s.tempFile = {fileID: parseInt( newFileID ), originalFolderID: parseInt(introduce.fileID), originalFilename: updatedFilename};
						introduce.fileID = window.top.Datacore.s.tempFile.fileID;
					} else {
						updatedFilename = filename;
						window.top.showTooltip( 'Annotations saved to server.', true );
					}
					annotationChanges.then();
					self.setS('viewerFile.ID', introduce.fileID);
					if(typeof afterAnnotateFn === "function") {
						afterAnnotateFn(introduce.fileID);
					}
				});
			};

			function introduceFile(fileID) {
				if( window.top.Datacore.getS( 'class' ) === 'WitnessPrep' || window.top.Datacore.getS( 'class' ) === 'WPDemo' ) {
					//introduceFileSubmit( updatedFilename, fileID, fileID, false );
				} else {
					if(window.top.navBar.pathname === '/depositions/pdf') {
						window.top.iPadContainerFrame.location.replace( '/depositions/introduce?ID=' + fileID + '&fromView=1' );
					}
					else if(window.top.navBar.pathname === '/depositions/presentation') {
						window.top.iPadContainerFrame.location.replace( '/depositions/introduce?ID=' + fileID + '&fromView=2' );
					}
				}
			};

			function cancelIntroduce() {
				if(window.top.navBar.pathname === '/depositions/introduce' && !isWitPrepIntroduce() ) {
					introduceCloseOut();
					var pathname = '/depositions/pdf?ID='+introduce.fileID;
					if( window.top.Datacore.s.inPresentation === true ) {
						pathname = '/depositions/presentation';
					}
					window.top.navBar.introduceButton.onClick(self.introduce).addClass('dark').removeClass('green').label('Introduce').enable();
					window.top.iPadContainerFrame.location.replace( pathname );
				}
				if(window.top.navBar.pathname === '/depositions/introduce' && isWitPrepIntroduce() ) {
					// stay on page until they do something
					window.top.navBar.introduceButton.onClick(skipStamp).removeClass('dark').addClass('green').label('Distribute').enable();
				}
				else if(window.top.navBar.pathname === '/depositions/pdf') {
					//console.log('WebApp::introduce()::cancelIntroduce()','Cancel Introduce from PDF Viewer');
					window.top.navBar.skipstampButton.disable();
					window.top.navBar.cancelButton.disable();
					window.top.navBar.backButton.onClick(function(){PDFBackButton();}).enable();
					window.top.navBar.sendButton.enable();
					window.top.navBar.saveButton.enable();
					window.top.navBar.editorActions.show();
					viewerAPI.showCallouts();
					viewerAPI.introduceStop();
					window.top.navBar.introduceButton.onClick(self.introduce).addClass('dark').removeClass('green').label('Introduce').enable();
				}
			};

			function enableIntroduceButton() {
				if( isWitPrepIntroduce() ) {
					window.top.navBar.introduceButton.onClick(function(){skipStamp();}).removeClass('dark').addClass('green').label('Distribute').enable();
					//window.top.navBar.skipstampButton.disable();
					//window.top.navBar.cancelButton.onClick(function(){cancelIntroduce();return false;}).addClass('dark').enable();
				} else {
					window.top.navBar.introduceButton.onClick(function(){saveIntroduce();}).removeClass('dark').addClass('green').label('Distribute').enable();
					window.top.navBar.skipstampButton.onClick(function(){skipStamp();}).enable();
					window.top.navBar.cancelButton.onClick(function(){cancelIntroduce();return false;}).addClass('dark').enable();
				}
			};

			function disableIntroduceButton() {
				if( isWitPrepIntroduce() ) {
					window.top.navBar.introduceButton.addClass('dark').removeClass('green').label('Introduce').disable();
					window.top.navBar.skipstampButton.disable();
					window.top.navBar.cancelButton.disable();
				} else {
					window.top.navBar.introduceButton.addClass('dark').removeClass('green').label('Introduce').disable();
					window.top.navBar.skipstampButton.disable();
					window.top.navBar.cancelButton.disable();
				}
			};

			var resumeIntroduceFn = null;
			function suspendIntroduce(){
				disableIntroduceButton();
				resumeIntroduceFn = enableIntroduceButton;
			};

			function saveIntroduce() {
				suspendIntroduce();
				if(window.top.webApp.viewerHasChanges()) {
					window.top.showLoadingOverlay("Updating...");
					distributeSaveFile(function(fileID) {
						window.top.loadingOverlayChangeTitle( 'Distributing...' );
						viewerAPI.introduceSave(introduceSaveResult);
					});
				} else {
					window.top.showLoadingOverlay("Distributing...");
					viewerAPI.introduceSave(introduceSaveResult);
				}
			};

			function introduceSaveResult(result) {
				introduce.data = result;
				var filename = [introduce.data.title, introduce.data.subtitle].join( '_' ).replace( /^_+|_+$/gm, '' );
				filename = filename.replace( /\s+/g, '_' );
				introduce.isSkipStamp = false;
				_iValidateFilename( filename );
			};

			function skipStamp() {
				suspendIntroduce();
				var dcFile = window.top.Datacore.getFile( introduce.fileID );
				if(updatedFilename === null || updatedFilename === undefined || updatedFilename === '') {
					if( self.isTempFile() ) {
						introduce.fileID = window.top.Datacore.s.tempFile.fileID;
						updatedFilename = window.top.Datacore.s.tempFile.originalFilename;
					} else if(dcFile) {
						var index = dcFile.name.lastIndexOf(".");
						updatedFilename = dcFile.name.substr(0, index);
					}
				}
				$.each(window.top.dcFolders, function(key, value) {
					if (value.class === 'Exhibit') {
						introduce.exhibitFolderID = value.ID;
					}
				});
				viewerAPI.introduceClear();
				viewerAPI.hideCallouts();
				window.top.setDialogTextInputCancelAction(_myCancelIntroduce);
				window.top.setDialogTextInputCloseAction(_mySkipStamp);
				window.top.setDialogTextInputCloseAddClass('btn_app_distribute');
				window.top.showDialogTextInput("Rename the exhibit", "Distribute", updatedFilename);
				introduce.isSkipStamp = true;

				function _myCancelIntroduce() {
					window.top.setDialogTextInputCloseRemoveClass('btn_app_distribute');
					cancelIntroduce();
				};

				function _mySkipStamp(filename) {
					window.top.setDialogTextInputCloseRemoveClass('btn_app_distribute');
					if(window.top.webApp.viewerHasChanges()) {
						window.top.showLoadingOverlay("Updating...");
						distributeSaveFile(function(fileID) {
							window.top.loadingOverlayChangeTitle( 'Distributing...' );
							_iValidateFilename(filename);
						});
					} else {
						window.top.showLoadingOverlay("Distributing...");
						_iValidateFilename(filename);
					}
				};
			};

			function _iValidateFilename(filename) {
				filename = filename.replace( /[^\w\040\-\.]+/g, '' );
				var folder = window.top.Datacore.getFolder( introduce.exhibitFolderID );
				if( typeof folder === 'undefined' || folder === null ) {
					console.error( 'missing exhibit folder' );
					return;
				}
				var files = folder.getFiles();
				var inConflictID = null;
				for( var f in files ) {
					var shortName;
					var _name = files[f].name;
					var index = _name.lastIndexOf( "." );
					if( index > 0 ) {
						shortName = _name.substr( 0, index );
						if( shortName.toLowerCase() === filename.toLowerCase() ) {
							inConflictID = files[f].ID;
							break;
						}
					}
				}
				filename += ".pdf";
				this.blockButtons = true;
				if( inConflictID !== null ) {
					//Confirm overwrite.
					var dialogButtons = [];
					dialogButtons.push( {label: 'No', id : 'no', class : 'btn_app_cancel'} );
					dialogButtons.push( {label: 'Yes', id : 'replace', class : 'btn_app_normal'} );
					var confirmMsg = 'An Official Exhibit by this name already exists.  Do you want to replace it?';
					console.log("Confirm overwrite");
					window.top.showDialog3Btn( 'File Already Exists!', confirmMsg, dialogButtons, true );
					$('#dialog3Btn #no', window.top.document).off().on("click", function() {
						window.top.dismissDialog3Btn();
						this.blockButtons = false;
						if(typeof resumeIntroduceFn === "function") {
							resumeIntroduceFn();
						}
					});
					$('#dialog3Btn #replace', window.top.document).off().on("click", function() {
						window.top.dismissDialog3Btn();
						introduceFileSubmit( filename, introduce.fileID, inConflictID, true );
					});

				} else {
					introduceFileSubmit( filename, introduce.fileID, introduce.fileID, false );
				}
			};

			function introduceCloseOut() {
				window.top.webApp.pCloseOut.fileID = window.top.webApp.getS('viewerFile.ID');
				window.top.webApp.pCloseOut.pageIndex = window.top.webApp.isAPI() ? viewerAPI.getPageIndex() : 0;
				window.top.webApp.pCloseOut.appOrientation = window.top.webApp.isAPI() ? viewerAPI.getPresentationOrientation() : LANDSCAPE;
				window.top.webApp.pCloseOut.editHistory = window.top.webApp.isAPI() ? viewerAPI.getPresentationAnnotations() : null;
				if(isset(window.top.webApp.pCloseOut.editHistory) && typeof window.top.webApp.pCloseOut.editHistory.then === "function") {
					window.top.webApp.pCloseOut.editHistory.then();
				}
			};

			function introduceFileSubmit(filename, sourceID, fileID, overwrite) {
				var title;
				var subTitle;
				var date;
				if (introduce.isSkipStamp) {
					var now = new Date();
					title = window.top.Datacore.s.exhibitTitle;
					subTitle = window.top.Datacore.s.exhibitSubTitle;
					date = (now.getMonth()+1)+'/'+now.getDate()+'/'+now.getFullYear();
				} else {
					title = introduce.data.title;
					subTitle = introduce.data.subtitle;
					date = introduce.data.date;
				}
				var url = [
					'/depositions/introduceFile?filename=', encodeURIComponent( filename ),
					'&fileID=', fileID,
					'&sourceFileID=', sourceID,
					'&exhibitXOrigin=', (introduce.data.stampX ? introduce.data.stampX : 0.0),
					'&exhibitYOrigin=', (introduce.data.stampY ? introduce.data.stampY : 0.0),
					'&stampWidth=', (introduce.data.width ? introduce.data.width : 150.0 ),
					'&stampHeight=', (introduce.data.height ? introduce.data.height : 100.0 ),
					'&orientation=', (introduce.data.orientation ? introduce.data.orientation : 0),
					'&exhibitTitle=', encodeURIComponent( title ),
					'&exhibitSubTitle=', encodeURIComponent( subTitle ),
					'&exhibitDate=', encodeURIComponent( date ),
					'&skipStamp=', introduce.isSkipStamp,
					'&overwrite=', overwrite
				].join( "" );
				console.log('Introduce:', url);
				$.getJSON( url, null, function(data, textStatus, xhr) {
					this.blockButtons = false;
					if (textStatus === 'success') {
						if (typeof data.error === 'undefined') {
							disableIntroduceButton();
							//window.top.Introduce.cleanEvents();
							// Find the new fileID
							var newFileID = parseInt( data.EDDeposition.introducedFile.ID );
							if (window.top.Datacore.hasFile( newFileID )) {
								window.top.Datacore.dropFile( newFileID );
							}
							window.top.Datacore.addFile( data.EDDeposition.introducedFile );
							updateStamp();
							if (overwrite) {
								window.top.showTooltip("The exhibit was successfully reintroduced!", true);
							} else {
								window.top.showTooltip("The exhibit was successfully introduced!", true);
							}
							window.top.Datacore.syncAllFolders();
							var pData = window.top.webApp.getS( 'joinPresentationData' );
							if( typeof pData !== 'undefined' && pData !== null ) {
								self.setPresentationFile( window.top.webApp.getS( 'inDeposition' ), data.EDDeposition.introducedFile.ID, true );
								window.top.iPadContainerFrame.location.replace( '/depositions/presentation' );
								return;
							}
							if(self.isTempFile()) {
								self.abortTempFile();
							}
							if(typeof resumeIntroduceFn === "function") {
								resumeIntroduceFn();
							}
							introduceRedirect(newFileID);
						} else {
							var errTitle = (typeof data.title !== 'undefined' && data.title !== null && data.title.length > 0) ? data.title : 'Error';
							var errText = (typeof data.message !== 'undefined' && data.message !== null && data.message.length > 0) ? data.message : 'Unable to introduce document.';
							window.top.setDialog1BtnCloseAction(cancelIntroduce);
							window.top.showPlainDialog1Btn( errTitle, errText, 'Close');
							if(typeof resumeIntroduceFn === "function") {
								resumeIntroduceFn();
							}
						}
					}
				} ).fail( function() {
					console.log("Fail block needs to get back to a better state.");
					this.blockButtons = false;
					if(typeof resumeIntroduceFn === "function") {
						resumeIntroduceFn();
					}
					window.top.setDialog1BtnCloseAction(cancelIntroduce);
					window.top.showPlainDialog1Btn( 'Error', 'Unable to introduce document.', 'Close' );
				});
				function getText( obj ) {
					var curText = obj.text();
					curText = curText.slice( 0, 120 );	//max filename length
					obj.text( curText );
					return curText;
				};
				function splitExhibitString( obj ) {
					var index = obj.text.search( /[0-9]+$/ );
					if (index >= 0) {
						var newCount = obj.text.substr(index);
						obj.baseText = obj.text.substr( 0, obj.text.length - newCount.length );
						obj.number = parseInt( newCount, 10 );
					} else {
						obj.baseText = obj.text;
						obj.number = 0;
					}
				};
				function updateStamp() {
					var newTitleText = introduce.data.title;
					var newSubTitleText = introduce.data.subtitle;
					var newInt = 0;
					var oldTitleText = window.top.Datacore.s.exhibitTitle;
					var oldSubTitleText = window.top.Datacore.s.exhibitSubTitle;
					var oldInt = 0;
					var obj;

					// Split out the number from the sub-title or from the title.
					if( newSubTitleText.length > 0) {
						obj = {text: newSubTitleText, baseText: newSubTitleText, number: 0};
						splitExhibitString( obj );
						newSubTitleText = obj.baseText;
						newInt = obj.number;

						obj = {text: oldSubTitleText, baseText: oldSubTitleText, number: 0};
						splitExhibitString( obj );
						oldSubTitleText = obj.baseText;
						oldInt = obj.number;
					} else {
						obj = {text: newTitleText, baseText: newTitleText, number: 0};
						splitExhibitString( obj );
						newTitleText = obj.baseText;
						newInt = obj.number;

						obj = {text: oldTitleText, baseText: oldTitleText, number: 0};
						splitExhibitString( obj );
						oldTitleText = obj.baseText;
						oldInt = obj.number;
					}
					//If the titles are the same and the new number less than or equal to the old number, we don't update the meta data because the user did an overwrite and we
					//leave the counter at it's old value.
					if( oldTitleText === newTitleText && oldSubTitleText === newSubTitleText && newInt <= oldInt ) {
						console.log(' The meta data is a lower number, so do not save.');
						return;
					}

					window.top.Datacore.s.exhibitTitle = newTitleText;
					window.top.Datacore.s.exhibitSubTitle = newSubTitleText;
				};
				function introduceRedirect(newFileID, toDeposition) {
					toDeposition = (typeof toDeposition !== 'undefined' && toDeposition !== null) ? toDeposition : false;
					var fromView = self.getS("fromView");
					if( isset(fromView) && fromView === 3 ) {
						window.top.iPadContainerFrame.location.replace( "/depositions" );
					} else if( !toDeposition ) {
						requestedFileID = self.getS('viewerFile.ID');
						if( typeof newFileID !== undefined && newFileID !== null && newFileID > 0 ) {
							requestedFileID = newFileID;
						}
						var pathname = '/depositions/pdf?ID='+requestedFileID;
						window.top.iPadContainerFrame.location.replace( pathname );
					} else {
						var pathname = '/depositions';
						if( window.top.Datacore.s.inPresentation === true ) {
							pathname = '/depositions/presentation';
						}
						window.top.iPadContainerFrame.location.replace( pathname );
					}
				};
			};
			function introduceBackButton() {
				var dialogButtons = [];
				dialogButtons.push( {"label":"No", "id":"cancel", "class":"btn_app_cancel"} );
				dialogButtons.push( {"label":"Yes", "id":"abort", "action":window.top.dismissDialog3Btn, "class":"btn_app_normal"} );
				var confirmMsg = '<p>You may have changes to this document.<p><p>Are you sure you want to exit without saving?</p>';
				window.top.showDialog3Btn( 'Abort Introduction?', confirmMsg, dialogButtons, false, 'tight padded' );
				$('#dialog3Btn #cancel', window.top.document).off().on( "click", function() {
					window.top.dismissDialog3Btn();
				} );
				$('#dialog3Btn #abort', window.top.document).off().on( "click", function() {
					window.top.dismissDialog3Btn();
					if(window.top.webApp.isTempFile()) {
						window.top.webApp.abortTempFile();
					}
					var pathname = '/depositions';
					if( window.top.Datacore.s.inPresentation === true ) {
						self.pCloseOut.preventCloseout = true;
						self.presentationEnd();
					}
					window.top.iPadContainerFrame.location.replace( pathname );
					window.top.navBar.introduceButton.removeClass('green').addClass('dark').label('Introduce');
				} );
			};
			function getExhibitFolder() {
				$.each(window.top.Datacore.getFolders(), function(key, value) {
					if (value.class === 'Exhibit') {
						introduce.exhibitFolderID = value.ID;
					}
				});
			};
		};

		this.witnessPrepIntroduce = function witnessPrepIntroduce() {
			console.log( "webApp::witnessPrepIntroduce" );
			self.introduce();
		};

		this.introduceSendEditHistory = function introduceSendEditHistory() {
			if(isset(introduceEditHistory) && isset(introduceEditHistory.annotations)) {
				$.each(introduceEditHistory.annotations, function(index, annotations) {
					if(isset(annotations)) {
						$.each(annotations, function(page, annotationArray) {
							if(isset(annotationArray)) {
								$.each(annotationArray, function(i, annotation) {
									//console.log("   -- ANNOTATSEND:", annotation.Page, annotation.Type, annotation.Message);
									self.introduceAnnotation(annotation);
								});
							}
						});
					}
					//console.log(" -- ANNOTATSEND:", page, annotations[page]);
				});
				$.each(introduceEditHistory.callouts, function(index, callout) {
					if(isset(callout)) {
						//console.log("   -- CALLOUTSEND:", callout.Page);
						self.introduceCallout(callout);
					}
				});
				introduceEditHistory = null;
			}
		};

		this.introduceAnnotation = function introduceAnnotation(annotation) {
			if(typeof this.introduceAnnotation.myQueue === "undefined") {
				this.introduceAnnotation.myQueue = [];
			}
			if( !this.isAPI() ) {
				this.introduceAnnotation.myQueue.push(annotation);
				console.log( 'Not a valid pdf-viewer so queueing annotation.' );
				return;
			}
			this.introduceAnnotation.myQueue.push(annotation);
			while(this.introduceAnnotation.myQueue.length > 0) {
				annotation = this.introduceAnnotation.myQueue.pop();
				switch(annotation.Message) {
					case 'DeleteAnnotation':
						viewerAPI.presentationDelete(annotation);
						break;
					case 'ModifyAnnotation':
						viewerAPI.presentationModify(annotation);
						break;
					case 'AddAnnotation':
						viewerAPI.presentationAdd(annotation);
						break;
				}
			}
		};

		this.introduceCallout = function introduceCallout(callout) {
			if(typeof this.introduceCallout.myQueue === "undefined") {
				this.introduceCallout.myQueue = [];
			}
			if( !this.isAPI() ) {
				this.introduceCallout.myQueue.push(callout);
				console.log( 'Not a valid pdf-viewer so queueing callout.' );
				return;
			}
			this.introduceCallout.myQueue.push(callout);
			while(this.introduceCallout.myQueue.length > 0) {
				callout = this.introduceCallout.myQueue.pop();
				viewerAPI.presentationAddCallout(callout);
			}
		};

		this.checkDepositionAttendance = function checkDepositionAttendance(depoID, callback) {
			if(typeof callback !== "function") {
				callback = jQuery.noop;
			}
			$.getJSON( '/depositions/attendDeposition?ID='+depoID, null, function(data, textStatus, xhr) {
				if (textStatus === 'success') {
					if (typeof data.error !== 'undefined') {
						callback(false);
						return;
					}
				} else {
					callback(false);
					return;
				}
				callback(true);
			} );
		};

		this.saveChangesPrompt = function saveChangesPrompt(finallyFn, notificationText, notificationDescription) {
			if(typeof finallyFn !== "function") {
				finallyFn = jQuery.noop;
			}
			if(!isset(notificationText)) {
				notificationText = "Abort Changes?";
			}
			if(!isset(notificationDescription)) {
				notificationDescription = "You may have changes to this document.  Are you sure you want to exit without saving?";
			}
			var hasChanges = self.viewerHasChanges();
			var hasTempFile = self.isTempFile();
			if( isset(viewerAPI) && (hasChanges || hasTempFile) ) {
				viewerAPI.stopAnnotationTools();
				var depoStatus = window.top.Datacore.getS( 'depositionStatus' );
				var userType = self.getS( 'user.userType' );
				if( (depoStatus !== 'F' && (['G','W'].indexOf( userType ) === -1)) || (depoStatus === 'F') ) {
					window.top.showDialog3Btn( notificationText, notificationDescription, [
						{label: "Don't Save", class: 'btn_app_extra', action: clickDontSave},
						{label: 'Cancel', class: 'btn_app_cancel', action: clickCancel},
						{label: 'Save', class: 'btn_app_normal', action: clickSaveChanges}
					] );
				} else if(depoStatus !== 'F' && (['G'].indexOf( userType ) !== -1)) {
					window.top.showDialog3Btn( notificationText, "You have made changes to the document. Are you sure you want to exit without emailing the document?", [
						{label: "Exit", class: 'btn_app_cancel', action: clickDontSave},
						{label: 'Email', class: 'btn_app_normal', action: clickSendChanges}
					] );
				} else if(depoStatus !== 'F' && (['W'].indexOf( userType ) !== -1)) {
					window.top.showDialog3Btn( notificationText, "You have made changes to the document. Are you sure you want to exit without saving the document?", [
						{label: "Exit", class: 'btn_app_cancel', action: clickDontSave},
						{label: 'Save', class: 'btn_app_normal', action: clickShareChanges}
					] );
				} else {
					finallyFn();
				}
			} else {
				finallyFn();
			}
			function clickDontSave() {
				window.top.dismissDialog3Btn();
				finallyFn();
			};
			function clickCancel() {
				window.top.dismissDialog3Btn();
			};
			function clickSaveChanges() {
				window.top.dismissDialog3Btn();
				self.getSaveFilename( finallyFn );
			};
			function clickSendChanges() {
				window.top.dismissDialog3Btn();
				self.send(true, finallyFn);
			};
			function clickShareChanges() {
				window.top.dismissDialog3Btn();
				self.share( finallyFn );
			};
		};

		this.saveAs = function saveAs(finallyFn) {
			window.top.dismissDialog3Btn();
			self.getSaveFilename( finallyFn );
		};

		this.viewFile = function viewFile(fileID, notification) {
			if(!isset(notification)) {
				notification = "Abort Changes?";
			}
			if (!window.top.Datacore.s.inPresentation) {
				self.saveChangesPrompt(viewFileRedirect, notification);
			} else {
				var file = window.top.Datacore.getFile(fileID);
				if( file.mimeType !== 'application/pdf' ) {
					viewFileRedirect();
				} else {
					presentationFileRedirect(fileID);
				}
			}
			function presentationFileRedirect(fileID) {
				window.top.dismissDialog3Btn();
				window.top.webApp.searchClose();
				window.top.webApp.abortTempFile();
				self.setPresentationFile( self.getS( 'inDeposition' ), fileID );
			};
			function viewFileRedirect() {
				window.top.dismissDialog3Btn();
				window.top.webApp.searchClose();
				window.top.webApp.abortTempFile();
				window.top.webApp.setS("isBackEvent", false)
				var file = window.top.Datacore.getFile(fileID);
				if( file.mimeType.indexOf( "audio" ) > -1 ) {
					window.top.iPadContainerFrame.location.href = '/depositions/audio?ID=' + fileID;
				}
				else if( file.mimeType.indexOf( "video" ) > -1 ) {
					window.top.iPadContainerFrame.location.href = '/depositions/video?ID=' + fileID;
				}
				else {
					window.top.iPadContainerFrame.location.href = '/depositions/pdf?ID='+fileID;
				}
			};
		};

		this.download = function download() {
			//window.top.showSpinner();
			var fileID = self.getS( 'viewerFile.ID' );
			_download();

			function _download() {
				var hasChanges = self.viewerHasChanges();
				if( isset(viewerAPI) && hasChanges ) {
					viewerAPI.stopAnnotationTools();
					var depoStatus = window.top.Datacore.getS( 'depositionStatus' );
					var userType = self.getS( 'user.userType' );
					if( (depoStatus !== 'F' && (['G','W'].indexOf( userType ) === -1)) || (depoStatus === 'F') ) {
						window.top.showDialog3Btn( "Save Changes?", "You may have changes to this document.  Do you want to save changes before downloading?", [
							{label: "Don't Save", class: 'btn_app_extra', action: PDFDownloadBtnAbort},
							{label: 'Cancel', class: 'btn_app_cancel', action: window.top.dismissDialog3Btn},
							{label: 'Save', class: 'btn_app_normal', action: PDFDownloadBtnSave}
						] );
						return;
					}
				}
				_downloadFile(fileID);
			};

			function _downloadFile(fileID) {
				var File = window.top.Datacore.getFile( fileID );
				window.top.showTooltip( 'Downloading '+File.name, true );
				ignoreNextUnload = true;
				window.top.iPadContainerFrame.location.href = '/depositions/file?fileID=' + fileID + '&filename=' + encodeURI(File.name);
			};

			function PDFDownloadBtnAbort() {
				window.top.dismissDialog3Btn();
				_downloadFile(fileID);
			};

			function PDFDownloadBtnSave() {
				window.top.dismissDialog3Btn();
				self.getSaveFilename( function(newFileID) {
					if(!newFileID)
						newFileID = self.getS('viewerFile.ID');
					_downloadFile(newFileID);
				} );
			};
		};

		this.send = function send(skipPrompt, callback) {
			//window.top.showSpinner();
			var annotationChanges = null;
			var sendWithChangesState = false;

			if(!isset(skipPrompt)) {
				skipPrompt = false;
			}
			var fileID = self.getS( 'viewerFile.ID' );
			if(!skipPrompt) {
				_sendPrompt();
			} else {
				_sendNoPrompt();
			}

			function _sendPrompt() {
				var hasChanges = self.viewerHasChanges();
				if( isset(viewerAPI) && hasChanges ) {
					viewerAPI.stopAnnotationTools();
					var dialogButtons = [];
					dialogButtons.push( {label: 'No', id : 'no_save', class : 'btn_app_cancel'} );
					dialogButtons.push( {label: 'Yes', id : 'save', class : 'btn_app_normal'} );
					var confirmMsg = 'Modifications have been made to the file, would you like to email the changes with the document?';
					window.top.showDialog3Btn( 'File Changed', confirmMsg, dialogButtons, true );
					$('#dialog3Btn #no_save', window.top.document).on("click", function() {
						window.top.dismissDialog3Btn();
						_sendOriginalFile(fileID);
					});
					$('#dialog3Btn #save', window.top.document).on("click", function() {
						window.top.dismissDialog3Btn();
						_sendWithChanges(fileID);
					});
					return;
				}
				_sendOriginalFile(fileID);
			};

			function _sendNoPrompt() {
				if(viewerAPI.hasAnnotationChanges()) {
					_sendWithChanges(fileID);
				} else {
					_sendOriginalFile(fileID);
				}
			};

			function _sendWithChanges(fileID) {
				window.top.dismissDialog3Btn();
				sendWithChangesState = true;
				var File = window.top.Datacore.getFile( fileID );
				annotationChanges = viewerAPI.getAnnotationChanges();
				window.top.showTooltip( 'Sending with changes '+File.name, true );
				window.top.sendDialog.setFileID( fileID, JSON.stringify(annotationChanges.annotations) );
				window.top.sendDialog.setFinallyFn( _mySentCallback );
				window.top.sendDialog.show();
			};

			function _sendOriginalFile(fileID) {
				window.top.dismissDialog3Btn();
				var File = window.top.Datacore.getFile( fileID );
				window.top.showTooltip( 'Sending '+File.name, true );
				window.top.sendDialog.setFileID( fileID );
				window.top.sendDialog.setFinallyFn( _mySentCallback );
				window.top.sendDialog.show();
			};

			function _mySentCallback() {
				if(sendWithChangesState && isset(annotationChanges) && typeof annotationChanges.then === "function" && self.isGuest()) {
					console.log("Email Sent:", "Annotations Included", "Guest", "annotationChanges.then();");
					annotationChanges.then();
				}
				if(typeof callback === "function") {
					callback();
				}
			};
		};

		this.saveFile = function saveFile() {
			//window.top.showSpinner();
			_save();
			function _save() {
				var hasChanges = self.viewerHasChanges();
				var hasTempFile = self.isTempFile();
				if( isset(viewerAPI) && (hasChanges || hasTempFile) ) {
					viewerAPI.stopAnnotationTools();
					var depoStatus = window.top.Datacore.getS( 'depositionStatus' );
					var userType = self.getS( 'user.userType' );
					if( (depoStatus !== 'F' && (['G','W'].indexOf( userType ) === -1)) || (depoStatus === 'F') ) {
						window.top.navBar.canHideToolbar(false);
						self.getSaveFilename(function(newFileID){saveRedirect(newFileID);});
					}
				}
			};
		};

		this.share = function share( finallyFn, finallyFnOnFail ) {
			if (window.top.Datacore.getS('depositionStatus') === 'F') {
				return;
			}
			if(!isset(finallyFnOnFail)) {
				finallyFnOnFail = false;
			}
			var hasChanges = self.viewerHasChanges();
			if( isset(viewerAPI) && hasChanges ) {
				viewerAPI.stopAnnotationTools();
				var annotationChanges = viewerAPI.getAnnotationChanges();
				var url = '/depositions/witnessUpload?sourceFileID=' + self.getS('viewerFile.ID');
				$.post( url, {annotations : JSON.stringify(annotationChanges.annotations)}, function( data, textStatus, xhr ) {
					console.log( textStatus );
					if (textStatus === 'success') {
						window.top.showTooltip(data['status'], true);
						annotationChanges.then();
						window.top.navBar.saveButton.addClass( 'disabled' );
						var folderID = (data && data.result && data.result.folderID) ? data.result.folderID : 0;
						if(folderID > 0 && self.isTrustedWitness()) {
							window.top.Datacore.syncFolder(folderID, function() {
								console.log("-- Trusted Witness: Save Folder Update");
							});
						}
						runFinally();
					} else {
						console.log( ' -- service failure occurred: ('+textStatus+')' );
						console.log( data );
						parent.parent.showPlainDialog1Btn( 'Error', 'Failed to save annotations.', 'Close' );
						failFinally();
					}
				}, 'json' )
				.fail( function() {
					console.log( ' -- request failure occurred' );
					parent.parent.showPlainDialog1Btn( 'Error', 'Failed to save annotations.', 'Close' );
					failFinally();
				} );
			}

			function runFinally() {
				if(typeof finallyFn === "function") {
					finallyFn();
				}
			};
			function failFinally() {
				if(finallyFnOnFail) {
					if(typeof finallyFn === "function") {
						finallyFn();
					}
				} else {
					self.share(finallyFn, true);
				}
			};
		};

		this.search = function search(termsArray) {
			var search = this;
			var searchEvents = undefined;
			var searchImmediate = typeof termsArray === "undefined" ? false : (jQuery.isArray(termsArray) ? true : false);

			init();

			window.top.navBar.canHideToolbar( false );
			$('.toolbar_title').hide();
			$('.toolbar_right').hide();
			$('.header').hide();
			$('.search_btn').addClass( 'selected' );
			$('#search_ui').show().promise().done( function() { $('#search_text').focus(); } );

			function init() {
				window.top.navBar.searchButton.onClick(_done);
				searchEvents = new viewerAPI.searchEvents();
				$('.navbtn_search_done').off().on( 'click', _done ).html( "Done" );
				$('.navbtn_search_next').off().on( 'click', _next );
				$('.navbtn_search_prev').off().on( 'click', _prev );
				$('#search_text').off().on( 'keyup', _keyup );
				if(searchImmediate) {
					_next();
				}

				function _updateStatus(status) {
					$('.navbtn_search_done').off().on('click',_done).html("Done");
					$('#search_status').html(status);
				};

				function _next() {
					if(isset(window.top.navBar)) {
						window.top.navBar.showToolbar();
					}
					$('.navbtn_search_done').off().on('click',_done).html("Done");
					if($('#search_text').val() !== "") {
						console.log('WebApp::searchNext()');
						_updateStatus('<i class="fa fa-spinner fa-pulse"></i>');
						var term = self.searchParseString($('#search_text').val());
						$.when(searchEvents.next(term)).then(_updateStatus, _updateStatus, _updateStatus);
					}
					else {
						_updateStatus('No Search Terms');
					}
				};

				function _prev() {
					if(isset(window.top.navBar)) {
						window.top.navBar.showToolbar();
					}
					$('.navbtn_search_done').off().on('click',_done).html("Done");
					if($('#search_text').val() !== "") {
						console.log('WebApp::searchPrev()');
						_updateStatus('<i class="fa fa-spinner fa-pulse"></i>');
						var term = self.searchParseString($('#search_text').val());
						$.when(searchEvents.prev(term)).then(_updateStatus, _updateStatus, _updateStatus);
					}
					else {
						_updateStatus('No Search Terms');
					}
				};

				function _done() {
					if(isset(window.top.navBar)) {
						window.top.navBar.canHideToolbar(true);
						window.top.navBar.searchButton.onClick(self.search);
					}
					searchEvents.clear();
					_updateStatus( "" );
					$('#search_text').val("");
					$('#search_ui').hide().promise().done(function(){searchEvents.clear();});
					$('.navbtn_search_done').off().on('click',_done).html("Done");
					$('.toolbar_title').show();
					$('.toolbar_right').show();
					$('.header').show();
					$('.search_btn').removeClass('selected');
					window.top.navBar.adjustTitle();
				};

				function _keyup(event) {
					if(event.keyCode === 13) {
						$('.navbtn_search_next').click();
						$('.navbtn_search_done').off().on('click',_done).html("Done");
					}
					else if(event.keyCode !== 9) {
						if( $("input#search_text").val() !== "" ) {
							$('.navbtn_search_done').off().on('click',function(){$('.navbtn_search_next').click();}).html("Search");
						} else {
							$('.navbtn_search_done').off().on('click',_done).html("Done");
						}
					}
				};

				self.searchDone = _done;
			};
		};

		this.searchMultiple = function searchMultiple(termsArray) {
			var sanitizedTerms = [];
			$.each(termsArray, function(key, value) {
				if(value.indexOf('"') !== -1) {
					value = value.replace('"', '');
				}
				if(value.indexOf(" ") !== -1) {
					value = '"'+value+'"';
				}
				sanitizedTerms.push(value);
			});
			$('#search_text').val(sanitizedTerms.join(' '));
			self.search(termsArray);
		};

		this.searchParseString = function searchParseString( termsString ) {
			var origString = termsString;
			var keywordRegex = /("[^"]+"|[\w]+)/gi;
			var nextExec = [];
			var termsArray = [];
			while( nextExec = keywordRegex.exec(termsString) ) {
				console.log("find:", nextExec, nextExec[0], nextExec[0].replace(/"/g,''));
				termsArray.push(nextExec[0].replace(/"/g,''));
			}
			return termsArray;
		};

		this.searchClose = function searchClose() {
			if( isset(self.searchDone) && typeof self.searchDone === "function") {
				self.searchDone();
			}
		};

		this.intangibleDrawButtons = function intangibleDrawButtons() {
			// simple version, invisible
			//$('.edit_actions').hide();
			//window.top.navBar.clearAllButton.invisible();

			// arrow button
			window.top.navBar.arrowButton.intangible();
			window.top.navBar.colorArrowButton.intangible();
			// highlight button
			window.top.navBar.textSelectButton.intangible();
			window.top.navBar.colorSelectButton.intangible();
			// pencil button
			window.top.navBar.drawingButton.intangible();
			window.top.navBar.colorDrawButton.intangible();
			// note button
			window.top.navBar.noteButton.intangible();
			window.top.navBar.colorNoteButton.intangible();
			// erase button
			window.top.navBar.eraseButton.intangible();
			// undo button
			window.top.navBar.undoButton.intangible();
			// clear all button
			window.top.navBar.clearAllButton.intangible();
			// callout button
			// window.top.navBar.calloutButton.intangible();
		};

		this.tangibleDrawButtons = function tangibleDrawButtons() {
			// simple version, visible
			//$('.edit_actions').show();
			//window.top.navBar.clearAllButton.visible();

			// arrow button
			window.top.navBar.arrowButton.tangible();
			window.top.navBar.colorArrowButton.tangible();
			// highlight button
			window.top.navBar.textSelectButton.tangible();
			window.top.navBar.colorSelectButton.tangible();
			// pencil button
			window.top.navBar.drawingButton.tangible();
			window.top.navBar.colorDrawButton.tangible();
			// note button
			window.top.navBar.noteButton.tangible();
			window.top.navBar.colorNoteButton.tangible();
			// erase button
			window.top.navBar.eraseButton.tangible();
			// undo button
			window.top.navBar.undoButton.tangible();
			// clear all button
			window.top.navBar.clearAllButton.tangible();
			// callout button
			// window.top.navBar.calloutButton.tangible();
		};

		this.thumbMode = function thumbMode() {
			viewerAPI.viewThumbnails();
		};

		this.pageMode = function pageMode() {
			viewerAPI.viewPage();
		};

		this.drawingSetup = function drawingSetup() {
			window.top.navBar.canHideToolbar( false );
			if(isset(window.top.Social)) {
				window.top.Social.hideAttendeeList();
			}
		};

		this.arrowButton = function arrowButton(keepOn) {
			self.drawingSetup();
			window.top.navBar.arrowButton.addClass('selected');
			//window.top.navBar.canHideToolbar( false );
			viewerAPI.arrowButton( function() {
				window.top.navBar.canHideToolbar( true );
				window.top.navBar.arrowButton.removeClass('selected');
			}, keepOn);
		};
		this.colorArrowButton = function colorArrowButton() {
			self.arrowButton(true);
			colorPicker(self.colorArrowButton, viewerAPI.getArrowDrawColor, self.setArrowDrawColor);
		};
		this.setArrowDrawColor = function setArrowDrawColor(color) {
			self.setS('ArrowDrawColor', color);
			$('#btnArrowColorDisplay').css('background-color', color);
			viewerAPI.setArrowDrawColor(color);
		};
		this.getArrowDrawColor = function getArrowDrawColor() {
			var defaultColor = '#ff0000';
			if(self.isWitness()) {
				defaultColor = '#0090ff';
			}
			return self.getS('ArrowDrawColor') ? self.getS('ArrowDrawColor') : defaultColor;
		};

		this.textSelectButton = function textSelectButton(keepOn) {
			self.drawingSetup();
			window.top.navBar.textSelectButton.addClass('selected');
			//window.top.navBar.canHideToolbar( false );
			viewerAPI.highlightButton( function() {
				window.top.navBar.canHideToolbar( true );
				window.top.navBar.textSelectButton.removeClass('selected');
			}, keepOn);
		};
		this.colorSelectButton = function colorSelectButton() {
			self.textSelectButton(true);
			colorPicker(self.textSelectButton, viewerAPI.getHighlightDrawColor, self.setHighlightDrawColor, true);
		};
		this.setHighlightDrawColor = function setHighlightDrawColor(color) {
			self.setS('HighlightDrawColor', color);
			$('#btnMarkerColorDisplay').css('background-color', color);
			viewerAPI.setHighlightDrawColor(color);
		};
		this.getHighlightDrawColor = function getHighlightDrawColor() {
			var defaultColor = '#fff600';
			if(self.isWitness()) {
				defaultColor = '#0090ff';
			}
			return self.getS('HighlightDrawColor') ? self.getS('HighlightDrawColor') : defaultColor;
		};

		this.drawingButton = function drawingButton(keepOn) {
			self.drawingSetup();
			window.top.navBar.drawingButton.addClass('selected');
			//window.top.navBar.canHideToolbar( false );
			viewerAPI.pencilButton( function() {
				window.top.navBar.canHideToolbar( true );
				window.top.navBar.drawingButton.removeClass('selected');
			}, keepOn);
		};
		this.colorDrawButton = function colorDrawButton() {
			self.drawingButton(true);
			colorPicker(self.textSelectButton, viewerAPI.getPencilDrawColor, self.setPencilDrawColor);
		};
		this.setPencilDrawColor = function setPencilDrawColor(color) {
			self.setS('PencilDrawColor', color);
			$('#btnPencilColorDisplay').css('background-color', color);
			viewerAPI.setPencilDrawColor(color);
		};
		this.getPencilDrawColor = function getPencilDrawColor() {
			var defaultColor = '#ff0000';
			if(self.isWitness()) {
				defaultColor = '#0090ff';
			}
			return self.getS('PencilDrawColor') ? self.getS('PencilDrawColor') : defaultColor;
		};

		this.noteButton = function noteButton(keepOn) {
			self.drawingSetup();
			window.top.navBar.noteButton.addClass('selected');
			//window.top.navBar.canHideToolbar( false );
			viewerAPI.noteButton( function() {
				window.top.navBar.canHideToolbar( true );
				window.top.navBar.noteButton.removeClass('selected');
			}, keepOn);
		};
		this.colorNoteButton = function colorNoteButton() {
			self.noteButton(true);
			colorPicker(self.textSelectButton, viewerAPI.getNoteDrawColor, self.setNoteDrawColor);
		};
		this.setNoteDrawColor = function setNoteDrawColor(color) {
			self.setS('NoteDrawColor', color);
			$('#btnNoteColorDisplay').css('background-color', color);
			viewerAPI.setNoteDrawColor(color);
		};
		this.getNoteDrawColor = function getNoteDrawColor() {
			var defaultColor = '#0090ff';
			return self.getS('NoteDrawColor') ? self.getS('NoteDrawColor') : defaultColor;
		};

		this.eraseButton = function eraseButton() {
			self.drawingSetup();
			window.top.navBar.eraseButton.addClass('selected');
			//window.top.navBar.canHideToolbar( false );
			viewerAPI.eraseButton( function() {
				window.top.navBar.canHideToolbar( true );
				window.top.navBar.eraseButton.removeClass('selected');
			});
		};

		this.undoButton = function undoButton() {
			viewerAPI.undoButton();
			window.top.navBar.showToolbar();
			if(window.top.Datacore.s.inPresentation && self.iAmAnnotator()) {
				self.viewerOnPresenterMessage('UndoAnnotation', {Message: 'UndoAnnotation'}, self.getS( 'inDeposition' ));
			}
		};

		// this.calloutButton = function calloutButton() {
		// 	self.drawingSetup();
		// 	window.top.navBar.calloutButton.addClass('selected');
		// 	//window.top.navBar.canHideToolbar( false );
		// 	viewerAPI.calloutButton( function() {
		// 		window.top.navBar.canHideToolbar( true );
		// 		window.top.navBar.calloutButton.removeClass('selected');
		// 	}, false);
		// };

		this.clearAllButton = function clearAllButton() {
			var dialogButtons = [];
			dialogButtons.push( {"label":"No", "id":"cancel", "action":cancelClearAll, "class":"btn_app_cancel"} );
			dialogButtons.push( {"label":"Yes", "id":"abort", "action":confirmClearAll, "class":"btn_app_normal"} );
			var confirmMsg = 'Are you sure you want to clear all annotations and notes from this page?';
			window.top.showDialog3Btn( 'All Annotations Will Be Deleted!', confirmMsg, dialogButtons, false, 'tight padded' );
			function confirmClearAll() {
				window.top.dismissDialog3Btn();
				viewerAPI.clearAll();
			};
			function cancelClearAll() {
				window.top.dismissDialog3Btn();
			};
		};

		this.presentationActive = function presentationActive() {
			if(typeof window.top.Social !== 'undefined') {
				window.top.Social.presentationActive();
			}
		};

		this.presentationStatus = function presentationStatus() {
			if(typeof window.top.Datacore === "undefined") {
				return;
			} else if(preWebAppReadyState) {
				self.onReady(self.presentationStatus());
			} else {
				console.log("-- WebApp::presentationStatus()", "Datacore.s.presentationAvailable:", window.top.Datacore.s.presentationAvailable);
				if(typeof window.top.Datacore.s !== "undefined" && window.top.Datacore.s.presentationAvailable) {
					// turn on active presentation button
					self.toggleAttendPresentationBtn(true, true);
				} else {
					// turn off presentation button
					self.toggleAttendPresentationBtn(false, false);
				}
			}
		};

		this.toggleAttendPresentationBtn = function toggleAttendPresentationBtn( showBtn, withAnimation ) {
			console.log("-- WebApp::toggleAttendPresentationBtn()", "showBtn:", showBtn, "withAnimation:", withAnimation);
			var duration = withAnimation ? 300 : 0;
			if(isset(window.top.navBar) && ["/depositions/pdf",
											"/depositions/video",
											"/depositions/audio"].indexOf(window.top.navBar.getPathname()) !== -1) {
				if(showBtn) {
					// pdf viewer when a presentation is going on
					window.top.navBar.presentationButton.enable().visible().addClass("active").onClick(self.presentationJoinSave);
				} else if(self.userIsSpeaker()) {
					// pdf viewer and leader, button click should start presentation
					window.top.navBar.presentationButton.removeClass('notActive').onClick(self.presentationPromptStart);
				} else {
					// pdf viewer when a presentation is closed
					window.top.navBar.presentationButton.disable().onClick(jQuery.noop);
				}
			} else if (showBtn) {
				$('#ipad_container_frame').contents().find('#attend_presentation').on('click',self.presentationJoinSave);
				$('#ipad_container_frame').contents().find('#attend_presentation').fadeIn(duration).switchClass('notActive', 'available', duration, 'linear');
			} else if(self.userIsSpeaker() || self.isWitness()) {
				$('#ipad_container_frame').contents().find('#attend_presentation').on('click',jQuery.noop);
				$('#ipad_container_frame').contents().find('#attend_presentation').hide();
			} else {
				$('#ipad_container_frame').contents().find('#attend_presentation').on('click',jQuery.noop);
				$('#ipad_container_frame').contents().find('#attend_presentation').fadeIn(duration).switchClass('available', 'notActive', duration, 'linear');
			}
		};

		this.presentationPromptStart = function presentationPromptStart() {
			if( window.top.Datacore.s.inPresentation ) {
				if( self.viewerHasChanges() && self.userIsSpeaker() ) {
					self.saveChangesPrompt(self.presentationStart);
				} else {
					self.saveChangesPrompt(self.presentationStart);
				}
				return;
			}
			if(self.isWitness()) {
				self.setHighlightDrawColor('#0090ff');
				self.setPencilDrawColor('#0090ff');
			}
			var dialogButtons = [];
			dialogButtons.push( {label:'Cancel', action:dismissStartDialog, class:'btn_app_cancel'} );
			dialogButtons.push( {label:'Start', action:self.presentationStart, class:'btn_app_normal'} );
			var confirmMsg = 'Would you like to start a presentation?';
			window.top.webApp.presentationPromptVisible = true;
			window.top.showDialog3Btn( 'Start Presentation!', confirmMsg, dialogButtons, true );
			function dismissStartDialog() {
				window.top.dismissDialog3Btn();
				window.top.webApp.presentationPromptVisible=false;
			};
		};

		this.presentationStart = function presentationStart() {
			window.top.dismissDialog3Btn();
			window.top.webApp.presentationPromptVisible=false;
			var start = this;
			start.fileID = self.getS('viewerFile.ID');
			start.prevPresentation = window.top.Datacore.s.inPresentation;
			var roomID = presentationGetDepositionID() + ".deposition";
			window.top.navBar.backButton.onClick(jQuery.noop);
			self.searchClose();
			setTimeout( function() { presentationAttendeesSelected(true); }, 300 );

			function presentationAttendeesSelected() {
				//console.log( 'RoomID:', roomID );
				presentationEditHistory = null;
				if( self.viewerHasChanges() ) {
					presentationEditHistory = startPresentationWithChanges();
				}
				if( self.viewerHasCallouts() && presentationEditHistory === null ) {
					var callouts = startPresentationWithCallouts();
					presentationEditHistory = {annotations:[], callouts:callouts};
				}
				var pTitle = '';
				var dcFile = window.top.Datacore.getFile(start.fileID);
				if(typeof dcFile !== "undefined" && typeof dcFile.caseFolderID !== 'undefined') {
					var folderID = dcFile.caseFolderID;
					var dcFolder = window.top.Datacore.getFolder(folderID);
					if(isset(dcFolder) && typeof dcFolder.class !== 'undefined') {
						if(dcFolder.class === "Exhibit") {
							pTitle = dcFile.name;
						}
					}
				}
				var pageIndex = 0;
				var orientation = self.getItem( "appOrientation" ) || "landscape";
				if( isset( viewerAPI ) ) {
					pageIndex = viewerAPI.getPageIndex();
					orientation = viewerAPI.getPresentationOrientation();
					self.setS( "presentationOrientation", orientation );
				}
				var pageOffset = (isset( viewerAPI ) ? viewerAPI.getPresentationPageOffset() : {"X":0, "Y":0} );
				var args = {
					"depositionID": presentationGetDepositionID(),
					"roomID": roomID,
					"orientation": orientation,
					"fileID": start.fileID,
					"pageIndex": pageIndex,
					"pageOffset": 1,
					"pageOffsetX": pageOffset.X,
					"pageOffsetY": pageOffset.Y,
					"pageScale": (isset( viewerAPI ) ? viewerAPI.getPresentationPageScale() : 1),
					"pageOrientation": (isset( viewerAPI ) ? viewerAPI.getPresentationPageOrientation() : 0),
					"rectOriginX": 0,
					"rectOriginY": 0,
					"rectSizeWidth": 1.656863,
					"rectSizeHeight": 0.969697,
					"annotatorID": window.top.webApp.getS( 'user.ID' ),
					"title": pTitle
				};
				console.log("==START-PRESENTATION-FILE-ID==", start.fileID, args.fileID);
				window.top.presentationStart( args );
				if( typeof window.top.fileSelection === 'undefined' || window.top.fileSelection === null ) {
					window.top.fileSelection = new window.top.FileSelection();
				}
				if(!start.prevPresentation) {
					window.top.iPadContainerFrame.location.replace( '/depositions/presentation' );
				}
			};

			function startPresentationWithChanges() {
				var changes = viewerAPI.getPresentationAnnotations();
				console.log(" -- PRESENTATION-CHANGES:", changes);
				if(isset(changes) && typeof changes.then === "function") {
					changes.then();
					delete changes.then;
				}
				return changes;
			};

			function startPresentationWithCallouts() {
				var changes = viewerAPI.getPresentationAnnotations();
				return changes.callouts;
			};
		};

		this.presentationEnd = function presentationEnd() {
			try {
				//BUGFIX: if the session is in presentation still (user presents, LOGS OUT, LOGS IN, opens an AUDIO file -you are still on presentation- this throws error because cant properly end a missing presentation, so now we force to continue anyways)
				self.presentationEndState();
			}catch(err) {
				console.log('--------------ERROR ENDING PRESENTATION IN NO PRESENTATION BROWSER');
			}
			window.top.presentationEnd(self.getS('inDeposition'));
		};

		this.presentationEndState = function presentationEndState() {
			console.log('presentationEndState');
			var pData = webApp.getS('joinPresentationData') ? webApp.getS('joinPresentationData') : Datacore.s.myPresentationData;
			console.log(webApp.getS('joinPresentationData'));
			console.log(Datacore.s.myPresentationData);
			console.log(Datacore);
			console.log(pData);

			this.pCloseOut.fileID = pData.fileID;
			this.pCloseOut.pageIndex = this.isAPI() ? viewerAPI.getPageIndex() : 0;
			this.pCloseOut.appOrientation = this.isAPI() ? viewerAPI.getPresentationOrientation() : LANDSCAPE;
			this.pCloseOut.editHistory = this.isAPI() ? viewerAPI.getPresentationAnnotations() : null;
			if(isset(this.pCloseOut.editHistory) && typeof this.pCloseOut.editHistory.then === "function") {
				this.pCloseOut.editHistory.then();
			}
			this.setS("presentationPaused", false);
			this.setS("isBackEvent", false);
			this.isAPI() && viewerAPI.activateDrawMode();
			this.searchClose();
			window.top.navBar.presentationPauseButton.disable();
			window.top.navBar.presentationPlayButton.disable();
			window.top.navBar.fullscreenButton.enable();
			window.top.navBar.noteButton.enable();
			window.top.navBar.colorNoteButton.enable();
			window.top.Social.showChatAction();
		};

		this.presentationSendEditHistory = function presentationSendEditHistory() {
			if(isset(presentationEditHistory) && isset(presentationEditHistory.annotations)) {
				$.each(presentationEditHistory.annotations, function(index, annotations) {
					if(isset(annotations)) {
						$.each(annotations, function(page, annotationArray) {
							if(isset(annotationArray)) {
								$.each(annotationArray, function(i, annotation) {
									//console.log("   -- ANNOTATSEND:", annotation.Page, annotation.Type, annotation.Message);
									self.viewerOnPresenterMessage(annotation.Message,annotation);
									self.presentationAnnotation(annotation);
								});
							}
						});
					}
					//console.log(" -- ANNOTATSEND:", page, annotations[page]);
				});
				$.each(presentationEditHistory.callouts, function(index, callout) {
					if(isset(callout)) {
						//console.log("   -- CALLOUTSEND:", callout.Page);
						self.viewerOnPresenterMessage("AddCallout",callout);
						self.presentationAddCallout(callout);
					}
				});
			}
		};

		this.presentationRestorePageState = function presentationRestorePageState() {
			console.log("WebApp::PresentationRestorePageState()", self.getS('presentSavePageState'));
			var stateString = self.getS('presentSavePageState');
			if(typeof stateString !== 'string') {
				return;
			}
			var state = jQuery.parseJSON(stateString);
			if(isset(state) && self.isAPI()) {
				viewerAPI.presentationPageAdd(state);
			}
			self.setS('presentSavePageState','{}');
		};

		this.isPresentationFileChanges = function isPresentationFileChanges() {
			if(window.top.Datacore.s.inPresentation && self.userIsSpeaker()) {
				viewerChanges = self.viewerHasChanges();
				if(viewerChanges) {
					return true;
				}
			}
			return false;
		};

		this.setPresentationFile = function setPresentationFile(depositionID, fileID, skipChanges) {
			skipChanges = !!skipChanges;
			if(self.viewerHasChanges() && !skipChanges) {
				var dialogButtons = [];
				dialogButtons.push( {label:"Don't Save", id:'no_save', action:spfDontSave, class:'btn_app_extra'} );
				dialogButtons.push( {label:'Cancel', action:spfCancel, class:'btn_app_cancel'} );
				dialogButtons.push( {label:'Save', id:'save', action:spfSave, class:'btn_app_normal'} );
				var confirmMsg = 'You may have changes to this document.  Are you sure you want to continue without saving?';
				window.top.showDialog3Btn( 'Abort Changes?', confirmMsg, dialogButtons );
			} else {
				self.viewerOnPresenterMessage("PresentationFile", fileID, depositionID); // add this to queue after unpause
				var pTitle = '';
				var _file = window.top.Datacore.getFile( fileID );
				self.setItem( 'viewFile', JSON.stringify(_file) );
				window.top.navBar.setTitle( _file.name );
				window.top.navBar.showTitle();
				if( typeof _file !== 'undefined' && _file !== null ) {
					pTitle = _file.name;
				}
				var pData = self.getS( 'joinPresentationData' );
				pData.fileID = fileID;
				pData.title = pTitle;
				pData.pageIndex = 0;
				pData.pageOrientation = 0;
				pData.pageOffsetX = 0;
				pData.pageOffsetY = 0;
				pData.pageScale = 1;
				presentationEditHistory = null;
				self.setS( 'joinPresentationData', pData );
				window.top.navBar.setTitle( pTitle );
				//viewerAPI.presentationClearAll();
				window.top.iPadContainerFrame.location.replace( '/depositions/presentation' );
			}

			function spfDontSave() {
				window.top.dismissDialog3Btn();
				viewerAPI.discardAnnotationChanges();
				self.abortTempFile();
				window.top.webApp.setPresentationFile( depositionID, fileID );
			};

			function spfCancel() {
				window.top.dismissDialog3Btn();
			};

			function spfSave() {
				window.top.dismissDialog3Btn();
				self.getSaveFilename(function(){window.top.webApp.setPresentationFile( depositionID, fileID );});
			};
		};

		this.presentationJoin = function presentationJoin() {
			self.searchClose();
			dismissDialog3Btn();
			window.top.showLoadingOverlay( 'Joining...' );
			window.top.presentationJoin( window.top.webApp.getS( 'inDeposition' ) );
		};

		this.presentationJoinSave = function presentationJoinSave() {
			window.top.dismissDialog3Btn();
			var needToSave = self.viewerHasChanges();
			if(!needToSave) {
				self.presentationJoin();
			} else {
				if(!self.isGuest()) {
					self.saveChangesPrompt( self.presentationJoin );
				} else {
					var dialogButtons = [];
					dialogButtons.push( {label: 'Join Now', action : window.top.webApp.presentationJoin, class : 'btn_app_extra'} );
					dialogButtons.push( {label: 'Don\'t Join', action : function(){dismissDialog3Btn();}, class : 'app_bttn_cancel'} );
					dialogButtons.push( {label: 'Email First', action : function(){window.top.webApp.send(true, window.top.webApp.presentationJoin);}, class : 'app_bttn_new'} );
					var confirmPass = 'You have modified the document. Would you like to email this document before joining the Presentation?';
					showDialog3Btn( 'Presentation Started!', confirmPass, dialogButtons, false, null, 'JoinPresentationStarted' );
				}
			}
		};

		this.presentationPageAdd = function presentationPageAdd( page ) {
			if( !window.top.Datacore.s.inPresentation ) {
				console.log( 'Not in a presentation so ignoring page add.' );
				return;
			}
			if( !isset( viewerAPI ) ) {
				console.log( 'Not a valid pdf-viewer so ignoring page add.' );
				return;
			}
			viewerAPI.presentationPageAdd( page );
		};

		this.presentationAnnotation = function presentationAnnotation(annotation) {
			if(typeof this.presentationAnnotation.myQueue === "undefined") {
				this.presentationAnnotation.myQueue = [];
			}
			if( !window.top.Datacore.s.inPresentation ) {
				console.log( 'Not in a presentation so ignoring annotations.' );
				return;
			}
			if( self.getS("presentationPaused") ) {
				console.log( 'I am the paused presenter so ignoring annotations.' );
				return;
			}
			this.presentationAnnotation.myQueue.push(annotation);
			if( !isset(viewerAPI) ) {
				console.log( 'Not a valid pdf-viewer so queueing annotation.' );
				return;
			}
			while(this.presentationAnnotation.myQueue.length > 0) {
				annotation = this.presentationAnnotation.myQueue.pop();
				if( !isset( annotation.Message ) ) {
					annotation.Message = "AddAnnotation";
				}
				switch(annotation.Message) {
					case 'DeleteAnnotation':
						viewerAPI.presentationDelete(annotation);
						break;
					case 'ModifyAnnotation':
						viewerAPI.presentationModify(annotation);
						break;
					case 'AddAnnotation':
					default:
						viewerAPI.presentationAdd(annotation);
						break;
				}
			}
		};

		this.presentationGetAnnotations = function presentationGetAnnotations(data) {
				if (!window.top.Datacore.s.inPresentation) {
					console.log( 'Not in a presentation so ignoring getAnnotations.' );
					return;
				}
				self.presentationPendingAnnotations = data.annotations;
				self.checkPresentationPendingAnnotations();
		};

		this.checkPresentationPendingAnnotations = function checkPresentationPendingAnnotations() {
			if (!isset( self.presentationPendingAnnotations )) {
				return;
			}
			this.presentationAnnotation.myQueue = [];
			var annotations = self.presentationPendingAnnotations;
			var i, j;
			for (i in annotations) {
				for(j in annotations[i]) {
					self.presentationAnnotation( annotations[i][j] );
				}
			}
			self.presentationPendingAnnotations = null;
		};

		this.presentationClearAll = function presentationClearAll(args) {
			if( !window.top.Datacore.s.inPresentation ) {
				console.log( 'Not in a presentation so ignoring clear all.' );
				return;
			}
			if( !isset(viewerAPI) ) {
				console.log( 'Not a valid pdf-viewer so ignoring clear all.' );
				return;
			}
			viewerAPI.presentationClearAll(args);
		};

		this.presentationModify = function presentationModify(args) {
			if( !window.top.Datacore.s.inPresentation ) {
				console.log( 'Not in a presentation so ignoring annotation modify.' );
				return;
			}
			if( !isset(viewerAPI) ) {
				console.log( 'Not a valid pdf-viewer so ignoring annotation modify.' );
				return;
			}
			viewerAPI.presentationModify(args);
		};

		this.presentationDelete = function presentationDelete(args) {
			if( !window.top.Datacore.s.inPresentation ) {
				console.log( 'Not in a presentation so ignoring annotation delete.' );
				return;
			}
			if( !isset(viewerAPI) ) {
				console.log( 'Not a valid pdf-viewer so ignoring annotation delete.' );
				return;
			}
			viewerAPI.presentationDelete(args);
		};

		this.passedPresentor = false;
		this.checkPresenterState = function checkPresenterState() {
			console.log("-- WebApp::checkPresenterState()", "this.passedPresentor:", self.passedPresentor);
			// This function is used if the leader ends presenation mode while the witness was the annotator
			if(self.passedPresentor) {
				self.passPresenter();
				// TODO: Re-think how this works
				// Set the annotators button bars for the presenter
				$('#content #edit_actions', parent.document).show();
				$('#content #arrow').hide();
				$('#content #colorArrow').hide();
				$('#content #note').hide();
				$('#content #colorNote').hide();
				$('#content #erase').hide();
				$('#content #undo').hide();

				$('#viewerContainer').css( 'top', '' );
				$('#toolbarContainer').show();
				$('#viewerContainer').removeClass( 'viewPresentation' );
				self.iAmPresentor = true;
				if (window.top.Datacore.getVal(window.top.Datacore.s.currentUser, 'userType') !== 'W') {
					//$('#drawerTab').show();
					if( typeof window.top.fileSelection !== 'undefined' && window.top.fileSelection !== null ) {
						window.top.fileSelection.enableFileSelection();
					}
				}
			} else if(self.userIsSpeaker()) {
				console.log("-- WebApp::checkPresenterState()", "this.userIsSpeaker():", self.userIsSpeaker());
				self.toggleAttendPresentationBtn(true, false);
				window.top.navBar.presentationButton.onClick(self.presentationPromptStart);
			}
		},

		this.passPresenter = function passPresenter() {
			console.log( 'passPresenter(): self.passedPresentor = ' + self.passedPresentor );
			var witness = self.getWitnessUser();
			if( witness.blockExhibits && !self.passedPresentor ) {
				window.top.showTooltip("The witness is currently blocked!", true);
				return;
			}
			if( self.passedPresentor ) {
				// Take back.
				self.passedPresentor = false;
				if(window.top.navBar.getPathname() === "/depositions/presentation" || window.top.navBar.getPathname() === "/depositions/pdf") {
					window.top.navBar.allDisable();
					window.top.navBar.showTitle();
					window.top.navBar.backButton.enable();
					window.top.navBar.sendButton.enable();
					window.top.navBar.saveButton.enable().removeClass('dark');
					window.top.navBar.searchButton.enable();
					window.top.navBar.presentationButton.enable().visible();
					window.top.navBar.presentationPlayButton.visible();
					window.top.navBar.presentationPauseButton.enable().visible();
					var file = window.top.Datacore.getFile(presentationGetFileID());
					var showIntroduce = (!isset( file ) || file.isExhibit) ? false : true;
					if(showIntroduce) {
						window.top.navBar.introduceButton.enable();
					}
				}
				if(window.top.navBar.getPathname() === "/depositions/presentation") {
					window.top.navBar.annotatorButton.removeClass( 'revoke' ).enable();
				} else if(window.top.navBar.getPathname() === "/depositions/pdf") {
					// nothing at this time
				} else {
					window.top.navBar.presentationButton.disable();
				}
				$('#file_contents', parent.document).removeClass( 'passAnnotator' );
				$('#viewerContainer').removeClass( 'passAnnotator' );
				$('#toolbarContainer').show();
				window.top.setAnnotator( window.top.webApp.getS('inDeposition'), {userID: window.top.Datacore.getS('currentUserID')} );
				if( typeof window.top.fileSelection !== 'undefined' && window.top.fileSelection !== null ) {
					window.top.fileSelection.enableFileSelection();
				}
			} else {
				self.passedPresentor = true;
				var witnessUser = self.getWitnessUser();
				if( witnessUser === null || !witnessUser.active ) {
					// The witness dropped out of presentation.
					window.top.navBar.annotatorButton.removeClass( 'active' );
					return;
				} else {
					window.top.setAnnotator( self.getS( 'inDeposition' ),  {userID: witnessUser.ID} );
					window.top.navBar.annotatorButton.addClass( 'revoke' );
					$('#ipad_container', window.top.document).removeClass( 'annotator' ); // causes a "resize" event which causes a viewer::redraw()
					$('#file_contents', parent.document).addClass( 'passAnnotator' );
					$('#viewerContainer').addClass( 'passAnnotator' );
					$('#toolbarContainer').hide();
					window.top.navBar.presentationButton.invisible();
					window.top.navBar.presentationPlayButton.invisible();
					window.top.navBar.presentationPauseButton.invisible();
					window.top.navBar.introduceButton.disable();
					window.top.navBar.sendButton.disable();
					window.top.navBar.saveButton.disable();
					window.top.navBar.searchButton.disable();
				}
				if( typeof window.top.fileSelection !== 'undefined' && window.top.fileSelection !== null ) {
					window.top.fileSelection.disableFileSelection();
				}
			}
			//PDFView.ignoreResizeEvent = true; // HACK: forces the resize event to be ignored  (fixes ED-2550)
		};

		this.annotatorChange = function annotatorChange( args ) {
			if( isset( args ) && args.annotatorID === self.getS( 'user.ID' ) ) {
				if(window.top.webApp.getS("blockExhibits")) {
					console.log("Error: Annotator was attempted to be granted, but refused due to my block.");
					return;
				}
				$('#ipad_container', window.top.document).addClass( 'annotator' );
				window.top.navBar.annotatorButton.removeClass( 'revoke' );
				$('#viewerContainer').css( 'top', '' );
				$('#viewerContainer').removeClass( 'viewPresentation' );
				self.iAmPresentor = true;
				if( window.top.webApp.getS( 'user.userType' ) === 'M' ) {
					if(isset(viewerAPI)) {
						viewerAPI.activatePresenterMode();
						if( window.top.Datacore.getS( 'depositionStatus' ) === 'I' && self.userIsSpeaker() === true && viewerAPI.hasAnnotationChanges()) {
							var fn = self.introduce;
							if( self.getS( 'deposition.class' ) === 'WitnessPrep' || self.getS( 'deposition.class' ) === 'WPDemo' ) {
								fn = self.witnessPrepIntroduce;
							}
							window.top.navBar.introduceButton.label('Introduce').onClick(fn).removeClass('green').addClass('dark').enable();
						}
					}
					window.top.navBar.editorActions.show();
					window.top.navBar.noteButton.disable();
					window.top.navBar.colorNoteButton.disable();
					if( typeof window.top.fileSelection !== 'undefined' && window.top.fileSelection !== null ) {
						window.top.fileSelection.enableFileSelection();
					}
				} else {
					if(isset(viewerAPI)) {
						viewerAPI.activateAnnotatorMode();
					}
					$('#toolbarContainer').show();
					window.top.navBar.editorActions.show();
					window.top.navBar.arrowButton.disable();
					window.top.navBar.colorArrowButton.disable();
					window.top.navBar.noteButton.disable();
					window.top.navBar.colorNoteButton.disable();
					window.top.navBar.eraseButton.disable();
					window.top.navBar.undoButton.disable();
					window.top.navBar.clearAllButton.disable();
					// window.top.navBar.calloutButton.disable();
					window.top.navBar.textSelectButton.onClick(window.top.webApp.textSelectButton).enable();
					window.top.navBar.colorSelectButton.onClick(window.top.webApp.colorSelectButton).enable();
					window.top.navBar.drawingButton.onClick(window.top.webApp.drawingButton).enable();
					window.top.navBar.colorDrawButton.onClick(window.top.webApp.colorDrawButton).enable();
					self.setHighlightDrawColor('#0090FF');
					self.setPencilDrawColor('#0090FF');
					window.top.navBar.showToolbar();
					window.top.navBar.thumbModeButton.disable();
					window.top.navBar.pageModeButton.disable();
					self.annotateSingleLineOn();
				}
			} else {
				if( window.top.webApp.userIsSpeaker() === true ) {
					$('#ipad_container', window.top.document).removeClass( 'annotator' );
					window.top.navBar.annotatorButton.addClass( 'revoke' );
				}
				if(isset(viewerAPI)) {
					viewerAPI.activatePresentationMode();
				}
				$('#viewerContainer').css( 'top', '0' );
				$('#viewerContainer').addClass( 'viewPresentation' );
				self.iAmPresentor = false;
				$('#toolbarContainer').hide();
				window.top.navBar.editorActions.hide();
				self.annotateSingleLineOff();
				if( typeof window.top.fileSelection !== 'undefined' && window.top.fileSelection !== null ) {
					window.top.fileSelection.disableFileSelection();
				}
			}
		};

		this.annotateSingleLineOn = function annotateSingleLineOn() {
			self.setS('singleLineMenu',true);
			$('.editor_actions').css({'position':'absolute','top':0,'z-index':15});
		};

		this.annotateSingleLineOff = function annotateSingleLineOff() {
			self.setS('singleLineMenu',false);
			$('.editor_actions').css({'position':'relative','z-index':''});
		};

		this.presentationPause = function presentationPause() {
			console.log('webapp::presentationPause();');
			if(isset(viewerAPI)) {
				viewerAPI.showPauseIndicator(self.presentationResume);
			}
			window.top.navBar.presentationPauseButton.disable();
			window.top.navBar.presentationPlayButton.enable();
			window.top.navBar.annotatorButton.invisible();
			self.setS("presentationPaused", true);
			self.setS("pauseQueue", []);
		};

		this.presentationPauseReset = function presentationResume() {
			console.log('webapp::presentationPauseReset();');
			if(isset(viewerAPI)) {
				viewerAPI.hidePauseIndicator();
			}
			window.top.navBar.presentationPauseButton.enable();
			window.top.navBar.presentationPlayButton.disable();
			window.top.navBar.annotatorButton.visible();
			self.setS("presentationPaused", false);
			self.setS("pauseQueue", []);
		};

		this.presentationResume = function presentationResume() {
			console.log('webapp::presentationResume();');
			if(isset(viewerAPI)) {
				viewerAPI.hidePauseIndicator();
			}
			window.top.navBar.presentationPauseButton.enable();
			window.top.navBar.presentationPlayButton.disable();
			window.top.navBar.annotatorButton.visible();
			self.setS("presentationPaused", false);
			$.each(self.getS("pauseQueue"), function(key,params) {
				self.viewerOnPresenterMessage(params[1], params[2], params[0]);
			});
		};

		this.getWitnessUser = function getWitnessUser() {
			var witness = null;
			var witnesses = [];
			var users = window.top.Datacore.getUsers();
			for( var u in users ) {
				if( users[u].userType === 'W' || users[u].userType === 'WM' ) {
					witnesses.push( users[u] );
				}
			}
			for( var w in witnesses ) {
				witness = witnesses[w];
				//console.log( 'getwitnessUser(): Witness:', witness.ID, 'active:', witness.active );
				if( witness.active ) {
					return witness;
				}
			}
			//console.log( 'getWitnessUser(): NO USER' );
			return witness;
		};

		this.updateSpeaker = function updateSpeaker() {
			//console.log( 'updateSpeaker()' );
			if(!!self.presentationPromptVisible) {
				window.top.dismissDialog3Btn();
				self.presentationPromptVisible=false;
			}
			if(!Datacore.s.myPresentation && !self.userIsSpeaker() && !Datacore.s.presentationAvailable && window.top.navBar.getPathname() === '/depositions/presentation') {
				window.top.webApp.setS("isBackEvent", true);
				console.log("-- redirect /depositions -- webapp::updateSpeaker(01);");
				window.top.iPadContainerFrame.location.replace( '/depositions' );
				return;
			} else if(Datacore.s.myPresentation && !self.userIsSpeaker() && !Datacore.s.presentationAvailable && window.top.navBar.getPathname() === '/depositions/presentation') {
				self.presentationEndState();
				console.log("-- redirect /depositions -- webapp::updateSpeaker(02); -- should pCloseOut for me.", Datacore.s.myPresentationData);
				Datacore.s.myPresentation = false;
				Datacore.s.myPresentationData = null;
				self.setS('myPresentation', false);
				self.presentationStatus();
				self.closeOutPresenterView();
			}
			var path = window.top.navBar.getPathname();
			var viewPaths = ['/depositions/pdf', '/depositions/audio', '/depositions/video'];
			if ( window.top.Datacore.getS('depositionStatus') !== 'I'
				|| ( isset(window.top.navBar) && viewPaths.indexOf(path) < 0 ) ) {
				window.top.navBar.introduceButton.disable();
				window.top.navBar.presentationButton.disable();
				return;
			}
			var depositionLeaderID = ( window.top.Datacore.getS( 'speakerID' ) !== 0 ) ? depositionLeaderID = window.top.Datacore.getS( 'speakerID' ) : window.top.Datacore.getS( 'conductorID' );
			var showIntroduce = true;
			var file = window.top.Datacore.getFile(self.getS("viewerFile.ID"));
			if (!isset( file )) {
				showIntroduce = false;
			} else {
				if (file.isExhibit) {
					showIntroduce = false;
				}
			}
			if ( depositionLeaderID === window.top.Datacore.getS('currentUserID')) {
				//console.log( 'updateSpeaker(): deposition leader === currentUserID (' + depositionLeaderID + ')' );
				if( showIntroduce ) {
					window.top.navBar.introduceButton.onClick( self.introduce ).addClass( 'dark' ).enable();
				} else {
					window.top.navBar.introduceButton.removeClass( 'dark' ).disable();
				}
				window.top.navBar.presentationButton.enable();
			} else {
				window.top.navBar.presentationButton.disable();
				window.top.navBar.introduceButton.disable();
			}
		};

		this.witnessAnnotatorCheck = function witnessAnnotatorCheck(pData) {
			witness = self.getWitnessUser();
			if(typeof pData !== "undefined"
					&& isset(witness)
					&& pData.annotatorID === self.getS("user.ID")
					&& witness.ID === self.getS("user.ID")) {
				self.annotatorChange(pData);
			}
		};

		this.isExhibit = function isExhibit(fileID) {
			var mFile = window.top.Datacore.getFile(fileID);
			if(isset(mFile)) {
				if(mFile.isExhibit) {
					return true;
				}
				var mCaseFolder = window.top.Datacore.getFolder( mFile.caseFolderID );
				if(isset(mCaseFolder) && mCaseFolder.class==="Exhibit" && mCaseFolder.caseID > 0) {
					return true;
				}
			}
			return false;
		};

		this.redirect = function redirect(fileID) {
			if(isset(fileID)) {
				if(self.viewerHasChanges()) {
					self.saveChangesPrompt( function(newFileID){
						window.top.iPadContainerFrame.location.replace( '/depositions/pdf?ID='+newFileID );
					});
				} else {
					window.top.iPadContainerFrame.location.replace( '/depositions/pdf?ID='+fileID );
				}
			} else {
				window.top.iPadContainerFrame.location.replace( '/depositions' );
				return;
			}
		};

		this.removeAll = function removeAll() {
			// taken from Annotate
		};

		function presentationGetDepositionID() {
			var presentationDepositionID = window.top.Datacore.getS('depositionID');
			if ( window.top.Datacore.getS('parentID') !== 0) {
				presentationDepositionID = window.top.Datacore.getS('parentID');
			}
			return presentationDepositionID;
		};

		function PDFBackButton() {
			self.searchClose();
			self.saveChangesPrompt(backRedirect);
			function backRedirect() {
				self.setS( 'isBackEvent', false );
				if(self.isTempFile()) {
					self.abortTempFile();
				}
				window.top.iPadContainerFrame.location.replace( "/depositions" );
			};
		};

		function IntroduceBackButton() {
			console.log("WebApp::IntroduceBackButton()");
			self.setS( 'isBackEvent', true );
			if ( window.top.Datacore.s.inPresentation ) {
				window.top.Datacore.s.inPresentation = false;
				window.top.webApp.removeItem( 'inPresentation' );
				if( self.presentationHost ) {
					self.presentationHost = false;
					window.top.presentationEnd( self.getS('inDeposition') );
					window.top.iPadContainerFrame.location.replace( '/depositions' );
					return;
					//if( typeof fileID !== 'undefined' && fileID > 0 ) {
					//	window.top.iPadContainerFrame.location.replace( '/depositions/pdf?ID=' + fileID );
					//	return;
					//}
				} else {
					window.top.presentationLeave( self.getS('inDeposition') );
					window.top.webApp.setS( 'joinPresentationData', null );
				}
			}
			window.top.iPadContainerFrame.location.replace( "/depositions" );
		};

		function PresentationBackButton() {
			self.searchClose();
			if( self.userIsSpeaker() ) {
				self.presentationPauseReset();
			}
			self.setS( 'isBackEvent', true );
			var fileID = window.top.webApp.getS( 'joinPresentationData.fileID' );
			var userType = window.top.webApp.getS( 'user.userType' );
			if( userType === 'W' || userType === 'WM' ) {
				return;
			}
			if ( window.top.Datacore.s.inPresentation ) {
				window.top.Datacore.s.inPresentation = false;
				window.top.webApp.removeItem( 'inPresentation' );
				if( self.presentationHost ) {
					if(window.top.webApp.viewerHasChanges()) {
						window.top.webApp.saveChangesPrompt(PresentationShutDown);
						return;
					} else {
						return PresentationShutDown();
					}
				} else {
					window.top.presentationLeave( self.getS('inDeposition') );
					window.top.webApp.setS( 'joinPresentationData', null );
				}
			}
			window.top.iPadContainerFrame.location.replace( '/depositions' );

			function PresentationShutDown() {
				window.top.webApp.presentationHost = false;
				window.top.presentationEnd( window.top.webApp.getS('inDeposition') );
				window.top.iPadContainerFrame.location.replace( '/depositions' );
				return true;
			};
		};

		this.getSaveFilename = function getSaveFilename(successFn) {
			if( self.getS( 'processing' ) ) {
				return;
			}
			if( isset(viewerAPI) ) {
				viewerAPI.stopAnnotationTools();
			}
			var fileID = self.getS( 'viewerFile.ID' );
			if( !fileID ) {
				fileID = self.getS( 'viewerFilename' );
			}
			if( !self.getS( "personalFolderID" ) ) {
				self.getPersonalFolderID();
			}
			var dialogSaveFile = new window.top.DialogSaveFile( "Name Your Copy", "Save", fileID, function(filename, folderID){
				validateFilename(filename, folderID, successFn);
			});
			dialogSaveFile.show();
		};

		function validateFilename( filename, folderID, successFn ) {
			var folder;
			var newFileID = null;
			if( !isset( folderID ) ) {
				folderID = self.getPersonalFolderID();
			}
			folder = window.top.Datacore.getFolder( folderID );
			if( !isset( folder ) ) {
				console.log('WebApp::validateFilename()::!folder', folderID);
				return;
			}
			var files = folder.getFiles( folder.getFileIDs() );
			var overwrite = false;
			var sourceFileID = self.getS('viewerFile.ID');
			var overwriteFileID = 0;
			for( var f in files ) {
				var shortName = files[f].name;
				var index = shortName.lastIndexOf( "." );
				if( index >= 0 ) {
					shortName = shortName.substr( 0, index );
				}
				if( shortName.toLowerCase() === filename.toLowerCase() ) {
					overwrite = true;
					overwriteFileID = parseInt( files[f].ID );
					break;
				}
			}
			var annotationChanges = viewerAPI.getAnnotationChanges();
			if( overwrite ) {
				//Confirm overwrite.
				var dialogButtons = [
					{label: "Cancel", class: "btn_app_cancel", action: window.top.dismissDialog3Btn},
					{label: "Replace", class: "btn_app_normal", action: function() {
						window.top.dismissDialog3Btn();

						annotate(annotationChanges.annotations, sourceFileID, overwrite, overwriteFileID, filename, folderID, function(fileID){
							annotationChanges.then();
							window.top.navBar.setTitle( filename+'.pdf' );
							newFileID = fileID ? fileID : window.top.webApp.getS('viewerFile.ID');
							//console.log('WebApp::validateFilename()::isDuplicate', newFileID);
							var pData = window.top.webApp.getS( 'joinPresentationData' );
							if( typeof pData !== 'undefined' && pData !== null && self.userIsSpeaker() ) {
								self.setS('presentSavePageState', self.isAPI() ? JSON.stringify(viewerAPI.getPresentationPageState()) : '{}');
								console.log("-- presentSavePageState:", self.getS('presentSavePageState'));
								self.setPresentationFile(presentationGetDepositionID(), newFileID);
							}
							if(typeof successFn === "function") {
								successFn(newFileID);
							}
						});
					}}
				];
				window.top.showDialog3Btn( 'Document Exists!', "A document by this name already exists.  Are you sure you want to replace the existing document?", dialogButtons, true );
			} else {
				annotate(annotationChanges.annotations, sourceFileID, overwrite, overwriteFileID, filename, folderID, function(fileID) {
					annotationChanges.then();
					window.top.navBar.setTitle( filename+'.pdf' );
					newFileID = fileID ? fileID : window.top.webApp.getS('viewerFile.ID');
					//console.log('WebApp::validateFilename()::else', newFileID);
					var pData = window.top.webApp.getS( 'joinPresentationData' );
					if( typeof pData !== 'undefined' && pData !== null && self.userIsSpeaker() ) {
						self.setS('presentSavePageState', self.isAPI() ? JSON.stringify(viewerAPI.getPresentationPageState()) : '{}');
						console.log("-- presentSavePageState:", self.getS('presentSavePageState'));
						self.setPresentationFile(presentationGetDepositionID(), newFileID);
					}
					if(typeof successFn === "function") {
						successFn(newFileID);
					}
				});
			}
		};

		function annotate(pAnnotations, sourceFileID, overwrite, overwriteFileID, filename, folderID, successFn) {
			var annotations = JSON.stringify(pAnnotations);
			console.log("--::--::--Save::", annotations);
			if(annotateRegistry[filename] === undefined || annotateRegistry[filename] === null || !Array.isArray(annotateRegistry[filename])) {
				annotateRegistry[filename] = [];
			}
			if(annotateRegistry[filename].indexOf(annotations) >= 0) {
				return ; // duplicate
			}
			annotateRegistry[filename].push(annotations);
			var url = '/depositions/annotate?' +
					  'sourceFileID=' + sourceFileID +
					  '&overwrite=' + !!overwrite +
					  '&fileID=' + overwriteFileID +
					  '&fileName=' + filename+'.pdf' +
					  '&folderID=' + folderID +
					  '&isTempFile=' + _annotateTempFile;
			console.log('WebApp::annotate()::post', url, annotations);
			$.post(url, {annotations: annotations}, function(data, textStatus, xhr) {
				console.log('WebApp::annotate()::success', data, textStatus, xhr);
				if(isset(textStatus) && textStatus==="success") {
					if(!isset(data.success) || !!data.success) {
						this.blockButtons = false;
						if (!!overwrite && !_annotateTempFile) {
							window.top.showTooltip("The document was successfully resaved!", true);
						} else if(!_annotateTempFile) {
							window.top.showTooltip("The document was successfully saved!", true);
						}
						var fileID = sourceFileID;
						if(data !== null && data !== undefined) {
							newFileID = parseInt( data.fileID );
							var dcFolder = window.top.Datacore.getFolder(folderID);
							if(isset(dcFolder) && typeof dcFolder.addFile === "function") {
								var depoID = presentationGetDepositionID();
								window.top.Datacore.syncFolder(folderID, function(){
									var fileData = window.top.Datacore.getFile(newFileID);
									if( isset(fileData) ) {
										self.setS( 'viewerFile', fileData );
										self.setS( 'viewerFilename', fileData.name );
									}
									if(typeof successFn === "function") {
										successFn(newFileID);
									}
								});
							}
							fileID = newFileID;
						}
						if(self.isTempFile()) {
							self.abortTempFile();
						}
					} else {
						this.blockButtons = false;
						var idx = annotateRegistry[filename].indexOf(annotations);
						annotateRegistry[filename].splice(idx, 1);
						window.top.showPlainDialog1Btn( 'Error', 'Unable to save document.', 'Close' );
					}
				} else {
					this.blockButtons = false;
					var idx = annotateRegistry[filename].indexOf(annotations);
					annotateRegistry[filename].splice(idx, 1);
					window.top.showPlainDialog1Btn( 'Error', 'Unable to save document.', 'Close' );
				}
			}, "json")
			.fail(function() {
				//console.log('WebApp::annotate()::fail');
				this.blockButtons = false;
				var idx = annotateRegistry[filename].indexOf(annotations);
				annotateRegistry[filename].splice(idx, 1);
				window.top.showPlainDialog1Btn( 'Error', 'Unable to save document.', 'Close' );
//					if(typeof successFn === "function")
//						successFn(0);
			});
		};

		function saveRedirect(newFileID) {
			console.log('WebApp::saveRedirect()', newFileID);
			requestedFileID = newFileID;
			if( typeof newFileID === undefined || isNaN(newFileID) || newFileID === null || newFileID === 0 ) {
				requestedFileID = self.getS('viewerFile.ID');
			}
			else {
				self.setS('viewerFile.ID', newFileID);
			}
			window.top.iPadContainerFrame.location.replace( '/depositions/pdf?ID='+requestedFileID );
		};

		function getActivityFeedback(action) {
			window.top.navBar.registerActivity(action);
		};

		function colorPicker(button, getColorFn, setColorFn, hide_black) {
			if(typeof hide_black === 'undefined') {
				hide_black = false;
			}
			window.top.navBar.canHideToolbar( false );
			var top = 100;
			if(self.getS('singleLineMenu')) {
				top -= 48;
			}
			window.top.showColorPicker(button.offsetLeft, top, getColorFn(), function(color) {
				setColorFn(color);
				window.top.navBar.canHideToolbar( true );
			}, '#ff0000', hide_black);
		};

		function presentationGetFileID() {
			var pData = window.top.webApp.getS( 'joinPresentationData' );
			if( typeof pData === 'undefined' || pData === null || typeof pData.fileID === 'undefined' ) {
				return self.getS('viewerFile.ID');
			}
			return pData.fileID;
		};

		function onThumbNotice() {
			if(window.top.Datacore.s.inPresentation && !self.userIsSpeaker()) {
				return;
			}
			$('.thumbMode').hide();
			$('.pageMode').show();
			self.toolbarHideable(false);
			self.intangibleDrawButtons();
		};

		function onThumbPageNotice() {
			if(window.top.Datacore.s.inPresentation && !self.userIsSpeaker()) {
				return;
			}
			$('.thumbMode').show();
			$('.pageMode').hide();
			self.toolbarHideable(true);
			self.tangibleDrawButtons();
		};

		this.viewerHasChanges = function viewerHasChanges() {
			var hasChanges = false;
			if( isset(viewerAPI) )
				hasChanges = !!viewerAPI.hasAnnotationChanges();
			return hasChanges;
		};

		this.viewerHasCallouts = function viewerHasCallouts() {
			var hasCallouts = false;
			if( isset(viewerAPI) )
				hasCallouts = viewerAPI.hasCallouts();
			return hasCallouts;
		};

		this.toolbarHideable = function toolbarHideable(hideable) {
			window.top.navBar.canHideToolbar(hideable);
		};

		this.getFileID = function getFileID() {
			return self.getS( 'viewerFile.ID' );
		};

		this.isTrustedWitness = function isTrustedWitness() {
			if(isset( window.top.Datacore.s.currentUser ) && window.top.Datacore.s.currentUser.userType === 'WM') {
				return true;
			}
			if(self.getS( 'user.userType' ) === 'WM') {
				return true;
			}
			return false;
		};

		this.isWitness = function isWitness() {
			if(isset( window.top.Datacore.s.currentUser ) && (window.top.Datacore.s.currentUser.userType === 'W' || window.top.Datacore.s.currentUser.userType === 'WM')) {
				return true;
			}
			if(self.getS( 'user.userType' ) === 'W' || self.getS( 'user.userType' ) === 'WM') {
				return true;
			}
			return false;
		};

		this.isGuest = function isGuest() {
			if(isset( window.top.Datacore.s.currentUser ) && (window.top.Datacore.s.currentUser.userType === 'G')) {
				return true;
			}
			if(self.getS( 'user.userType' ) === 'G') {
				return true;
			}
			return false;
		};

		this.isOwner = function isOwner() {
			return ( window.top.Datacore.getS('parentID') === 0 && window.top.Datacore.getS('conductorID') === window.top.Datacore.getS('currentUserID') );
		};

		this.isDialogUp = function isDialogUp() {
			if($('.popupDialog').is(':visible'))
				return true;
			if($('.depo_files_send').is(':visible'))
				return true;
		};

		this.nothing = function() {};

		$(window.top.document).ready( function() {
//			console.log( 'WebAppClass::ready()' );
			WebAppReady();
		} );

		//var storageCookieOpts = '; path=/; domain=.edepoze.com; secure';
		//var storageCookieOpts = '; path=/; domain=.armyargentina.com; secure';
		//var storageCookieOpts = '; path=/; domain=.'+window.currentDomain+'; secure';
		var storageCookieOpts = '; path=/; domain=.'+window.document.domain+'; secure';

		if (window.document.domain != 'edepoze.com'){
			storageCookieOpts = '; path=/; domain=.'+window.document.domain+';';
		}


		//var storageCookieOpts = '; path=/; domain=.armyargentina.com; secure';
		//alert(storageCookieOpts);
		//testt


		this.setItem = function setItem(newKey, newValue) {
			var storageKey = 'storage=';
			var storageObj = getStorageAsObject();

//alert('set item: '+newKey+'  > '+newValue);

			storageObj[newKey] = newValue;

			document.cookie = storageKey + encodeURIComponent( JSON.stringify( storageObj ) ) + storageCookieOpts;
			//console.log(document.cookie);

//alert( storageKey + encodeURIComponent( JSON.stringify( storageObj ) ) + storageCookieOpts);
		};

		this.getItem = function getItem(item) {
//alert(document.cookie);
			var storageObj = getStorageAsObject();
//console.log(storageObj);

			if ( storageObj.hasOwnProperty(item) ) {
//alert('has as: '+	storageObj[item]);
				return storageObj[item];
			}
			return null;
		};

		this.hasItem = function hasItem(item) {
			var storageItem = this.getItem(item);
			if ( storageItem !== 'undefined' && storageItem !== null ) {
				return true;
			}
			return false;
		};

		this.removeItem = function removeItem(item) {
			var storageKey = 'storage=';
			var storageObj = getStorageAsObject();

			delete storageObj[item];

			document.cookie = storageKey + encodeURIComponent( JSON.stringify( storageObj ) ) + storageCookieOpts;
		};

		this.removeAllItems = function removeAllItems() {
			var storageKey = 'storage=';
			var storageObj = getStorageAsObject();
			var persistentItems = ['pdfjs.preferences', 'database', 'fullscreen']; // Values here aren't deleted, behave as if in localStorage

			for (var item in storageObj) {
				if ( storageObj.hasOwnProperty(item) && persistentItems.indexOf(item) === -1 ) {
					delete storageObj[item];
				}
			}

			document.cookie = storageKey + encodeURIComponent( JSON.stringify( storageObj ) ) + storageCookieOpts;
		};

		function getStorageAsObject() {
			var cookies = document.cookie.split( '; ' );

			var storageKey = 'storage=';
			var storageValue = '';
			var storageObj = {};
			for( var i in cookies ) {
				var c = cookies[i];
				if( c.indexOf( storageKey ) === 0 ) {
					storageValue = decodeURIComponent( c.substring( storageKey.length ) );

					/*
					if (document.cookie.indexOf('selectedFolderID') > -1){
						console.log(cookies);
						console.log(c);
						console.log(storageKey.length);
						console.log(c.substring( storageKey.length ) );

						alert(document.cookie + ' >> found ' + storageKey + ' >> ' + c.substring( storageKey.length ) + ' >>> ' + storageValue);

						console.log(storageKey);
						console.log(storageValue);

					}
					*/

					if (storageValue == '{}'){
						//If the storage is an empty object, lets keep looking for other storages (sometimes storage variable is repeated TWICE, and the real data stored in the second one
						//So, no break here
					}else{
						//Found a storage with data, break now
						break;
					}
				}
			}
			if( storageValue.length > 0 ) {
				try {
					storageObj = JSON.parse( storageValue );
				} catch( e ) {
					console.warn( e );
					storageObj = {};
				}
			}
			return storageObj;
		};

		function handleAppOrientation( eventData ) {
			//console.log( 'handleAppOrientation', eventData );
			if( typeof eventData === "object" && eventData !== null && typeof eventData.orientation === "string" ) {
				switch( eventData.orientation ) {
					case PORTRAIT:
						self.setItem( 'appOrientation', eventData.orientation );
						$("#ipad_container, #ipad_container_frame").addClass( PORTRAIT );
						if( typeof window.top.navBar !== "undefined" && window.top.navBar !== null) {
							window.top.navBar.resizeTitle();
						}
						if( typeof window.top.iPadContainerFrame !== "undefined" && window.top.iPadContainerFrame !== null ) {
							window.top.iPadContainerFrame.$("#content").addClass( PORTRAIT );
							try {
								if( typeof window.top.DocViewerFrame !== "undefined" && window.top.DocViewerFrame !== null ) {
									$("body", window.top.DocViewerFrame).addClass( PORTRAIT );
								}
							}
							catch(err) {
								console.log("WebApp::handleAppOrientation() Error:", err);
							}
						}
						break;
					case LANDSCAPE:
					default:
						self.setItem( 'appOrientation', eventData.orientation );
						$('#ipad_container, #ipad_container_frame').removeClass( PORTRAIT );
						if( typeof window.top.iPadContainerFrame !== "undefined" && window.top.iPadContainerFrame !== null ) {
							window.top.iPadContainerFrame.$('#content').removeClass( PORTRAIT );
							try {
								if( typeof window.top.DocViewerFrame !== "undefined" && window.top.DocViewerFrame !== null ) {
									$("body", window.top.DocViewerFrame).removeClass( PORTRAIT );
								}
							}
							catch(err) {
								console.log("WebApp::handleAppOrientation() Error:", err);
							}
						}
						break;
				}
				if( isset( window.top.navBar ) ) {
					window.top.navBar.adjustTitle();
				}
			}
			if( typeof viewerAPI === "object" && viewerAPI !== null ) {
				viewerAPI.appOrientationDidChange( eventData.orientation );
			}
		};

		this.setAppOrientation = function setAppOrientation( eventData ) {
			if( typeof eventData === "object" && eventData !== null && typeof eventData.orientation === "string" && [PORTRAIT, LANDSCAPE].indexOf( eventData.orientation ) >= 0 ) {
				handleAppOrientation( eventData );
			}
		};

		this.pCloseOut = {
			fileID:0,
			pageIndex:0,
			appOrientation:LANDSCAPE,
			editHistory:null,
			preventCloseout:false
		};

		this.closeOutPresenterView = function closeOutPresenterView() {
			//console.log( "webApp::closeOutPresenterView", self.pCloseOut );
			if(self.pCloseOut.preventCloseout) {
				self.pCloseOut.preventCloseout = false;
				self.pCloseOut.fileID = 0;
			}
			if(self.pCloseOut.fileID > 0) {
				window.top.iPadContainerFrame.location.replace( '/depositions/pdf?ID='+self.pCloseOut.fileID );
			}
		};

		this.closeOutSendChangePage = function closeOutSendChangePage() {
			console.log("WebApp::CloseOutSendChangePage");
			this.pCloseOut.fileID = 0;
			if(this.pCloseOut.pageIndex > 0 && this.isAPI()) {
				viewerAPI.presentationPageAdd({pageIndex:this.pCloseOut.pageIndex});
				var confirmPage = viewerAPI.getPageIndex();
				console.log("WebApp::CloseOutSendChangePage", confirmPage, this.pCloseOut.pageIndex);
			}
		};

		this.closeOutSendEditHistory = function closeOutSendEditHistory() {
			console.log("WebApp::CloseOutSendEditHistory");
			this.pCloseOut.fileID = 0;
			if(isset(this.pCloseOut.editHistory)) {
				if(isset(this.pCloseOut.editHistory.annotations)) {
					$.each(this.pCloseOut.editHistory.annotations, function(index, annotations) {
						if(isset(annotations)) {
							$.each(annotations, function(page, annotationArray) {
								if(isset(annotationArray)) {
									$.each(annotationArray, function(i, annotation) {
										self.closeOutAnnotation(annotation);
									});
								}
							});
						}
					});
				}
				if(isset(this.pCloseOut.editHistory.callouts)) {
					$.each(this.pCloseOut.editHistory.callouts, function(index, callout) {
						if(isset(callout)) {
							self.closeOutCallout(callout);
						}
					});
				}
			}
		};

		this.closeOutAnnotation = function closeOutAnnotation(annotation) {
			if(typeof this.closeOutAnnotation.myQueue === "undefined") {
				this.closeOutAnnotation.myQueue = [];
			}
			if( !this.isAPI() ) {
				this.closeOutAnnotation.myQueue.push(annotation);
				console.log( 'Not a valid pdf-viewer so queueing annotation.' );
				return;
			}
			this.closeOutAnnotation.myQueue.push(annotation);
			while(this.closeOutAnnotation.myQueue.length > 0) {
				annotation = this.closeOutAnnotation.myQueue.pop();
				switch(annotation.Message) {
					case 'DeleteAnnotation':
						viewerAPI.presentationDelete(annotation);
						break;
					case 'ModifyAnnotation':
						viewerAPI.presentationModify(annotation);
						break;
					case 'AddAnnotation':
						viewerAPI.presentationAdd(annotation);
						break;
				}
			}
		};

		this.closeOutCallout = function closeOutCallout(callout) {
			if(typeof this.closeOutCallout.myQueue === "undefined") {
				this.closeOutCallout.myQueue = [];
			}
			if( !this.isAPI() ) {
				this.closeOutCallout.myQueue.push(callout);
				console.log( 'Not a valid pdf-viewer so queueing callout.' );
				return;
			}
			this.closeOutCallout.myQueue.push(callout);
			while(this.closeOutCallout.myQueue.length > 0) {
				callout = this.closeOutCallout.myQueue.pop();
				viewerAPI.presentationAddCallout(callout);
			}
		};

		this.closeOutPrevent = function closeOutPrevent(prevent) {
			self.pCloseOut.preventCloseout = !!prevent;
		};

		this.presentationAddCallout = function presentationAddCallout(callout) {
			//console.log("WebApp::presentationAddCallout()", callout);
			if(typeof this.presentationAddCallout.myQueue === "undefined") {
				this.presentationAddCallout.myQueue = [];
			}
			if( !window.top.Datacore.s.inPresentation ) {
				console.log( 'Not in a presentation so ignoring callouts.' );
				return;
			}
			if( self.getS("presentationPaused") ) {
				console.log( 'I am the paused presenter so ignoring callouts.' );
				return;
			}
			if( !isset(viewerAPI) ) {
				this.presentationAddCallout.myQueue.push(callout);
				console.log( 'Not a valid pdf-viewer so queueing callout.' );
				return;
			}
			this.presentationAddCallout.myQueue.push(callout);
			while(this.presentationAddCallout.myQueue.length > 0) {
				callout = this.presentationAddCallout.myQueue.pop();
				viewerAPI.presentationAddCallout(callout);
			}
		};

		this.presentationModifyCallout = function presentationModifyCallout(callout) {
			//console.log("WebApp::presentationModifyCallout()", callout);
			if(typeof this.presentationModifyCallout.myQueue === "undefined") {
				this.presentationModifyCallout.myQueue = [];
			}
			if( !window.top.Datacore.s.inPresentation ) {
				console.log( 'Not in a presentation so ignoring callouts.' );
				return;
			}
			if( self.getS("presentationPaused") ) {
				console.log( 'I am the paused presenter so ignoring callouts.' );
				return;
			}
			if( !isset(viewerAPI) ) {
				this.presentationModifyCallout.myQueue.push(callout);
				console.log( 'Not a valid pdf-viewer so queueing callout.' );
				return;
			}
			this.presentationModifyCallout.myQueue.push(callout);
			while(this.presentationModifyCallout.myQueue.length > 0) {
				callout = this.presentationModifyCallout.myQueue.pop();
				viewerAPI.presentationModifyCallout(callout);
			}
		};

		this.presentationDeleteCallout = function presentationDeleteCallout(callout) {
			//console.log("WebApp::presentationDeleteCallout()", callout);
			if(typeof this.presentationDeleteCallout.myQueue === "undefined") {
				this.presentationDeleteCallout.myQueue = [];
			}
			if( !window.top.Datacore.s.inPresentation ) {
				console.log( 'Not in a presentation so ignoring callouts.' );
				return;
			}
			if( self.getS("presentationPaused") ) {
				console.log( 'I am the paused presenter so ignoring callouts.' );
				return;
			}
			if( !isset(viewerAPI) ) {
				this.presentationDeleteCallout.myQueue.push(callout);
				console.log( 'Not a valid pdf-viewer so queueing callout.' );
				return;
			}
			this.presentationDeleteCallout.myQueue.push(callout);
			while(this.presentationDeleteCallout.myQueue.length > 0) {
				callout = this.presentationDeleteCallout.myQueue.pop();
				viewerAPI.presentationDeleteCallout(callout);
			}
		};

		this.presentationSetCalloutZIndex = function presentationSetCalloutZIndex(callouts) {
			//console.log("WebApp::presentationSetCalloutZIndex()", callout);
			if(typeof this.presentationSetCalloutZIndex.myQueue === "undefined") {
				this.presentationSetCalloutZIndex.myQueue = [];
			}
			if( !window.top.Datacore.s.inPresentation ) {
				console.log( 'Not in a presentation so ignoring callouts.' );
				return;
			}
			if( self.getS("presentationPaused") ) {
				console.log( 'I am the paused presenter so ignoring callouts.' );
				return;
			}
			if( !isset(viewerAPI) ) {
				this.presentationSetCalloutZIndex.myQueue.push(callouts);
				console.log( 'Not a valid pdf-viewer so queueing callout.' );
				return;
			}
			this.presentationSetCalloutZIndex.myQueue.push(callouts);
			while(this.presentationSetCalloutZIndex.myQueue.length > 0) {
				callouts = this.presentationSetCalloutZIndex.myQueue.pop();
				viewerAPI.presentationSetCalloutZIndex(callouts);
			}
		};

		this.presentationGetCallouts = function presentationGetCallouts(data) {
			if (!window.top.Datacore.s.inPresentation) {
				console.log( 'Not in a presentation so ignoring getAnnotations.' );
				return;
			}
			console.log("WebApp::presentationGetCallouts()", data);
			self.presentationPendingCallouts = data.callouts;
			self.checkPresentationPendingCallouts();
		};
		this.checkPresentationPendingCallouts = function checkPresentationPendingCallouts() {
			console.log("WebApp::checkPresentationPendingCallouts", self.presentationPendingCallouts);
			if (!isset( self.presentationPendingCallouts )) {
				return;
			}
			var callouts = self.presentationPendingCallouts;
			var i, j;
			for (i in callouts) {
				for(j in callouts[i]) {
					self.presentationAddCallout( callouts[i][j] );
				}
			}
			self.presentationPendingCallouts = null;
		};

		var draggableGetOffset = function draggableGetOffset() {
			var jFrame = $('#ipad_container_frame', window.top.document);
			var frameOffset = jFrame.offset();
			return {left: frameOffset.left, top: frameOffset.top};
		};
		this.draggableMove = function draggableMove(x, y) {
			var frameOffset = draggableGetOffset();
			var trueX = x + frameOffset.left;
			var trueY = y + frameOffset.top;
			$('.ui-droppable').each(function(index){
				var jObj = $(this);
				var offset = jObj.offset();
				var width = jObj.width();
				var height = jObj.height();
				if(offset.left <= trueX && offset.top <= trueY && offset.left+width >= trueX && offset.top+height >= trueY) {
					jObj.addClass('drag_over');
				} else {
					jObj.removeClass('drag_over');
				}
			});
		};
		this.draggableDrop = function draggableDrop(event,x,y) {
			console.log('-- draggable drop');
			var frameOffset = draggableGetOffset();
			var trueX = x + frameOffset.left;
			var trueY = y + frameOffset.top;
			$('.ui-droppable').each(function(index){
				var jObj = $(this);
				var offset = jObj.offset();
				var width = jObj.width();
				var height = jObj.height();
				if(offset.left <= trueX && offset.top <= trueY && offset.left+width >= trueX && offset.top+height >= trueY) {
					event.currentTarget = jObj;
					jObj.trigger("drop",event);
					jObj.removeClass('drag_over');
				}
			});
		};
		this.touchHandlerHelper = function touchHandlerHelper(event) {
			// source: http://stackoverflow.com/questions/1517924/javascript-mapping-touch-events-to-mouse-events
			event = event.originalEvent;
			var touches = event.changedTouches,
				first = touches[0],
				type = "";
			switch(event.type) {
				case "touchstart": type = "mousedown"; break;
				case "touchmove":  type = "mousemove"; break;
				case "touchend":   type = "mouseup";   break;
				default:           return;
			}
			var simulatedEvent = document.createEvent("MouseEvent");
			simulatedEvent.initMouseEvent(type, true, true, window, 1,
										  first.screenX, first.screenY,
										  first.clientX, first.clientY, false,
										  false, false, false, 0/*left*/, null);
			first.target.dispatchEvent(simulatedEvent);
			//event.stopImmediatePropagation();
			event.preventDefault();
		};
	}

	return {
		init: function()
		{
			if( !_instance ) {
				_instance = WebAppClass.apply( null, arguments );
			}
			return _instance;
		},
		getInstance: function()
		{
			if( !_instance ) {
				return this.init.apply( null, arguments );
			}
			return _instance;
		}
	};

}());

window.top.webApp = WebAppClass.getInstance();

window.top.webApp.onReady( function() {
//	console.log( 'WebApp::onReady' );
	window.top.webApp.setS( 'NavBar', $('#ipad_navbar', window.top.document) );
	window.top.iPadContainer = $('#ipad_container', window.top.document);
	window.top.iPadContainerFrame = window.top.frames['ipad_container_frame'];
	$('iframe#ipad_container_frame').load( function() {
//		console.log( 'webApp -- ipad_container_frame -- loaded' );
		window.top.iPadContainerFrame.$('iframe#doc_viewer_frame').load( function() {
//			console.log( 'webApp -- doc_viewer_frame -- loaded' );
			window.top.DocViewerFrame = window.top.iPadContainerFrame.frames["doc_viewer_frame"];
		} );
		var alc = $('#attendee_list_container', window.top.document);
		if( window.top.webApp.enableTranscripts() === true ) {
			alc.removeClass( 'notabs' ).addClass( 'tabs' );
		} else {
			alc.removeClass( 'tabs' ).addClass( 'notabs' );
		}
	} );
} );

var WitnessLogins = function( userInfo )
{
	var witnessList;
	var witnesses = {};
	var selectedID;

	var allowWitness = function()
	{
//		console.log( 'WitnessLogins::allowWitness', selectedID, witnesses[selectedID] );
		var _w = witnesses[selectedID];
		if( typeof _w !== 'undefined' && _w !== null ) {
			authorizeWitness( window.top.Datacore.s.depositionID, selectedID, true );
		}
		dismissDialog3Btn();
		delete window.top.witnessLogins;
	};

	var denyAll = function()
	{
//		console.log( 'WitnessLogins::denyAll' );
		window.top.sio.emit( 'rejectWitnesses', {}, function( res ) {
			if( typeof res !== 'object' || res === null  || typeof res.success === 'undefined' || res.success === null || !res.success ) {
				console.log( res );
			}
		} );
		dismissDialog3Btn();
		delete window.top.witnessLogins;
	};

	var selectWitness = function( event )
	{
//		console.log( 'WitnessLogins::selectWitness', event.data.ID );
		$('li', witnessList).removeClass( 'selected' );
		event.data.item.addClass( 'selected' );
		selectedID = event.data.ID;
	};

	//dialog3Btn
	dismissDialog3Btn();
	var dialogMsg = '<p>A witness would like to join the session.</p><p>Select the witness to join the session?</p><div class="witnessListWrap"><ul id="witnessList"></ul></div>';
	var dialogButtons = [];
	dialogButtons.push( {label:'Deny All', action:denyAll, class:'btn_app_cancel'} );
	dialogButtons.push( {label:'Allow', action:allowWitness, class:'btn_app_normal'} );
	showDialog3Btn( 'Witness Login!', dialogMsg, dialogButtons, true, 'tight' );
	witnessList = $('#witnessList');

	this.request = function( userInfo )
	{
//		console.log( 'WitnessLogins::request', userInfo );
		var li = $('<li class="witnessSelect"><div class="checkBtn"></div></li>');
		li.attr( 'data-id', userInfo.witnessID );
		li.append( ['<div class="wName">', userInfo.name, '</div>'].join( "" ) );
		li.on( 'click', {"ID":userInfo.witnessID,"item":li}, selectWitness );
		witnessList.append( li );
		witnesses[userInfo.witnessID] = userInfo;
		if( Object.keys( witnesses ).length === 1 ) {
			var _w = witnesses[(Object.keys( witnesses )[0])];
			$("#witnessList li[data-id='" + _w.witnessID + "']").trigger( 'click' );
		}
	};

	this.remove = function( userInfo )
	{
//		console.log( 'WitnessLogins::remove', userInfo );
		var _w = witnesses[userInfo.id];
		if( typeof _w !== 'undefined' && _w !== null ) {
			var item = $("li[data-id='" + _w.witnessID + "']", witnessList);
			var isSelected = item.hasClass( 'selected' );
			delete witnesses[_w.witnessID];
			if( Object.keys( witnesses ).length === 0 ) {
				dismissDialog3Btn();
				delete window.top.witnessLogins;
				return;
			}
			item.remove();
			if( isSelected ) {
				var first = witnesses[(Object.keys( witnesses )[0])];
				$("#witnessList li[data-id='" + first.witnessID + "']").trigger( 'click' );
			}
		}
	};

	if( typeof userInfo === 'object' && userInfo !== null ) {
		this.request( userInfo );
	}
};
