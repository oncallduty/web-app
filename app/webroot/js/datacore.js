/**
 * Datacore.js
 *
 * Interface for data gathered from API services.
 */

if( !Array.isArray ) {
	Array.isArray = function( vArg ) {
		return Object.prototype.toString.call( vArg ) === "[object Array]";
	};
}

/**
 * Implementation of the sequential-considerate approach to trim()
 */
if (!String.prototype.scaLTrim) {
	String.prototype.scaLTrim = function () {
		return this.replace( /^\s\s*/, '' );
	};
}

if (!String.prototype.scaRTrim) {
	String.prototype.scaRTrim = function () {
		var i = this.length;
		var ws = /\s/;
		while (ws.test( this.charAt( --i ) ));
		return this.slice( 0, i+1 );
	};
}

if (!String.prototype.scaTrim) {
	String.prototype.scaTrim = function () {
		return this.scaLTrim().scaRTrim();
	};
}

function isset( v ) {
	return (typeof v !== 'undefined' && v !== null);
}

var dcUsers = {};
var dcRooms = {};
var dcCases = {};
var dcDepositions = {};
var dcFolders = {};
var dcFiles = {};
var dcExhibitHistory = {};
var dcPresentationUsers = {};

function Datacore() {
}

Datacore.s = {};
Datacore.s.inPresentation = (isset(window.top.webApp) && window.top.webApp.getItem( 'inPresentation' ) === 'true');

Datacore.clean = function() {
	dcUsers = {};
	dcRooms = {};
	dcCases = {};
	dcDepositions = {};
	dcFolders = {};
	dcFiles = {};
	dcExhibitHistory = {};
	dcPresentationUsers = {};
	if( typeof window.top.webApp !== 'undefined' && isset( window.top.webApp.getS( 'user' ) ) ) {
		Datacore.addUser( window.top.webApp.getS( 'user' ) );
	}
};

Datacore.getPrimaryDepositionID = function() {
	return Datacore.getRootDepositionID();
};

Datacore.getStoredItems = function( itemStore, itemIDs ) {
	if (typeof itemIDs === 'undefined') {
		itemIDs = null;
	}

	if (itemIDs !== null && !Array.isArray( itemIDs ) ) {
		return [itemStore[itemIDs]];
	}

	var fileObjs = [];

	if (itemIDs === null) {
		for (var isKey in itemStore) {
			fileObjs.push( itemStore[isKey] );
		}
	} else if (itemIDs.length > 0) {
		for (var i=0; i<itemIDs.length; ++i) {
			fileObjs.push( ((itemIDs[i] in itemStore) ? itemStore[itemIDs[i]] : null) );
		}
	}

	return fileObjs;
};

Datacore.loadUsers = function( params ) {
	if (typeof params === 'undefined' || params === null) {
		return;
	}
	if (!Array.isArray( params )) {
		params = [params];
	}

	for (var i=0; i<params.length; ++i) {
		this.addUser( params[i] );
	}
};

Datacore.addUser = function( params ) {
	if (!isset( params.ID ) && !isset( params.id )) {
		return -1;
	}
	var userID = parseInt( (isset( params.ID ) ? params.ID : params.id) );
	var dcUser = Datacore.getUser( userID );
	if( typeof dcUser === "undefined" || dcUser === null ) {
		dcUsers[userID] = new DcUser( params );
	} else {
		dcUsers[userID].init( params );
	}
	return true;
};

Datacore.dropUser = function( userID )
{
	if( typeof userID === 'undefined' || userID === null || typeof dcUsers[userID] === 'undefined' || dcUsers[userID] === null ) {
		return null;
	}
	var userObj = dcUsers[userID];
	delete dcUsers[userID];
	return userObj;
};

Datacore.hasUser = function( userID ) {
	return (userID in dcUsers);
};

Datacore.getUser = function( userID ) {
	return (typeof userID !== 'undefined' && userID !== null) ? this.getStoredItems( dcUsers, userID )[0] : null;
};

Datacore.getUsers = function( userIDs ) {
	return this.getStoredItems( dcUsers, userIDs );
};

Datacore.addPresentationUser = function( userID ) {
	if( !isset( userID ) || !isset( dcUsers[userID] ) ) {
		return null;
	}
	var userID = parseInt( userID );
	dcPresentationUsers[ userID ] = true;
	return true;
};

Datacore.dropPresentationUser = function( userID )
{
	if( !isset( userID ) || !isset( dcUsers[userID] ) ) {
		return null;
	}
	delete dcPresentationUsers[ userID ];
	return true;
};

Datacore.hasPresentationUser = function( userID ) {
	return (userID in dcPresentationUsers);
};

Datacore.loadRooms = function( params ) {
	if (!isset( params )) {
		return;
	}
	if (!Array.isArray( params )) {
		params = [params];
	}

	for (var i=0; i<params.length; ++i) {
		this.addRoom( params[i] );
	}
};

Datacore.removeAllPresentationUsers = function() {
	dcPresentationUsers = {};
}

Datacore.addRoom = function( params ) {
	if (!isset( params.ID ) && !isset( params.id )) {
		return -1;
	}
	var roomID = isset( params.ID ) ? params.ID : params.id;
//	if (roomID in dcRooms) {
//		return false;
//	}
	dcRooms[roomID] = new DcRoom( params );
	return true;
};

Datacore.dropRoom = function( roomID ) {
	if (!isset( roomID ) || !(roomID in dcRooms)) {
		return null;
	}
	var roomObj = dcRooms[roomID];
	delete dcRooms[roomID];
	return roomObj;
};

Datacore.hasRoom = function( roomID ) {
	return (roomID in dcRooms);
};

Datacore.getRoom = function( roomID ) {
	return (isset( roomID ) ? this.getStoredItems( dcRooms, roomID )[0] : null);
};

Datacore.getRooms = function( roomIDs ) {
	return this.getStoredItems( dcRooms, roomIDs );
};

Datacore.getPublicRooms = function() {
	var publicset = [];

	var roomids = Object.keys( dcRooms );

	for (var ri in roomids) {
		var roomID = roomids[ri]; 
		if (dcRooms[roomID].private !== true) {
			publicset.push( dcRooms[roomID] );
		}
	}

	return publicset;
};

// helper functions for Datacore Cases
Datacore.loadCases = function( params ) {
	if (typeof params === 'undefined' || params === null) {
		return;
	}

	if (!Array.isArray( params )) {
		params = [params];
	}

	for (var i=0; i<params.length; ++i) {
		this.addCase( params[i] );
	}
};

Datacore.addCase = function( params ) {
	if (typeof params.ID === 'undefined' || params.ID === null) {
		return -1;
	}
//	if (params.ID in dcCases) {
//		return false;
//	}
	dcCases[params.ID] = new DcCase( params );
	return true;
};

Datacore.dropCase = function( caseID ) {
	if (typeof caseID === 'undefined' || caseID === null || !(caseID in dcCases)) {
		return null;
	}
	var caseObj = dcCases[caseID];
	delete dcCases[caseID];
	return caseObj;
};

Datacore.getCase = function( caseID ) {
	return (typeof caseID !== 'undefined' && caseID !== null) ? Datacore.getStoredItems( dcCases, caseID )[0] : null;
};

Datacore.getCases = function( caseIDs ) {
	return Datacore.getStoredItems( dcCases, caseIDs );
};

// helper functions for Datacore Depositions
Datacore.loadDepositions = function( params ) {
	if (typeof params === 'undefined' || params === null) {
		return;
	}

	if (!Array.isArray( params )) {
		params = [params];
	}

	for (var i=0; i<params.length; ++i) {
		this.addDeposition( params[i] );
	}
};

Datacore.addDeposition = function( params ) {
	if (typeof params !== 'object' || params === null || typeof params.ID === 'undefined' || params.ID === null) {
		return -1;
	}
//	if (params.ID in dcDepositions) {
//		return false;
//	}
	dcDepositions[params.ID] = new DcDeposition( params );
	return true;
};

Datacore.dropDeposition = function( depoID ) {
	if (typeof depoID === 'undefined' || depoID === null || !(depoID in dcDepositions)) {
		return null;
	}
	var depoObj = dcDepositions[depoID];
	delete dcDepositions[depoID];
	return depoObj;
};

Datacore.getDeposition = function( depoID ) {
	return (typeof depoID !== 'undefined' && depoID !== null) ? this.getStoredItems( dcDepositions, depoID )[0] : null;
};

Datacore.getDepositions = function( depoIDs ) {
	return this.getStoredItems( dcDepositions, depoIDs );
};

Datacore.getRootDepositionID = function() {
	var parentID = this.getS('parentID');
	return (parentID !== 0) ? parentID : this.getS('depositionID');
};

Datacore.getS = function( prop ) {
	if ( typeof Datacore.s[prop] !== 'undefined' && Datacore.s[prop] !== null ) {
		return Datacore.s[prop];
	}
	return null;
};

Datacore.getVal = function( obj, prop ) {
	if (typeof obj === "object" && obj.hasOwnProperty(prop) ) {
		return obj[prop];
	}

	return null;
};

Datacore.loadFolders = function( params ) {
	if (typeof params === 'undefined' || params === null) {
		return;
	}
	if (!Array.isArray( params )) {
		params = [params];
	}

	for (var i=0; i<params.length; ++i) {
		this.addFolder( params[i] );
	}
};

Datacore.addFolder = function( params ) {
	var folderID = 0;
	if( typeof params !== 'object' || params === null || typeof params.ID === 'undefined' || params.ID === null ) {
		return -1;
	}
	folderID = parseInt( params.ID );
	if( isNaN( folderID ) || folderID <= 0 ) {
		return -1;
	}
	var dcFolder = Datacore.getFolder( folderID );
	if( typeof dcFolder === 'undefined' || dcFolder === null ) {
		dcFolders[folderID] = new DcFolder( params );
	} else {
		dcFolders[folderID].init( params );
	}
	return true;
};

Datacore.dropFolder = function( folderID )
{
	if( typeof folderID === 'undefined' || folderID === null || !(folderID in dcFolders) ) {
		return null;
	}
	var folderObj = dcFolders[folderID];
	delete dcFolders[folderID];
	return folderObj;
};

Datacore.hasFolder = function( folderID ) {
	return (folderID in dcFolders);
};

Datacore.getFolder = function( folderID ) {
	return (typeof folderID !== 'undefined' && folderID !== null) ? this.getStoredItems( dcFolders, folderID )[0] : null;
};

Datacore.getFolders = function( folderIDs ) {
	return this.getStoredItems( dcFolders, folderIDs );
};

Datacore.syncFolder = function( folderID, callback ) {
//	console.log( 'Datacore.syncFolder;', folderID );
	folderID = parseInt( folderID );
	if( !isset( folderID ) || isNaN( folderID ) ) {
		console.warn( 'Datacore.syncFolder; invalid folderID:', folderID );
		return;
	}
	if( !Datacore.hasFolder( folderID ) ) {
		console.log( 'Datacore.syncFolder; missing folder by ID:', folderID );
		return Datacore.syncAllFolders();
	}
	$.getJSON( '/depositions/files?folderID=' + folderID, null, function( data, status, xhr ) {
//		console.log( 'Datacore.syncFolder; data:', data );
		var Folder = null;
		if (data.status === 'success') {
			if( typeof data.files.length !== 'undefined' ) {
				Folder = Datacore.getFolder( folderID );
				if( isset( Folder ) ) {
					var dFileIDs = [];
					for( var i=0; i<data.files.length; ++i ) {
						Folder.addFile( data.files[i] );
						dFileIDs.push( parseInt( data.files[i].ID ) );
					}
					var fFileIDs = Folder.getFileIDs();
					var removedFileIDs = $(fFileIDs).not(dFileIDs);
					for( var i=0; i<removedFileIDs.length; ++i ) {
						Folder.dropFile( removedFileIDs[i] );
					}
					Folder.numFiles = data.files.length;
//					console.log( 'syncFolder;', Folder.ID, 'files:', Folder.getFileIDs().length, 'of', Folder.numFiles );
				}
			}
		}
		if( typeof callback === 'function' ) {
//			console.log( 'Datacore.syncFolder -- callback' );
			callback( Folder );
		}
	} ).done( function() {
//		console.log( 'syncFolder::done', folderID );
	} ).fail( function() {
//		console.log( 'syncFolder::fail', folderID );
	} ).complete( function() {
//		console.log( 'syncFolder::complete', typeof callback );
	} );
};

Datacore._sync = false;
Datacore.syncCallbackQueue = [];
Datacore.processCallbackQueue = function processCallbackQueue() {
	if( !Array.isArray( Datacore.syncCallbackQueue ) ) {
		Datacore.syncCallbackQueue = [];
	}
	console.log( 'Datacore.processCallbackQueue; length:', Datacore.syncCallbackQueue.length );
	if( Datacore.syncCallbackQueue.length === 0 ) {
		return;
	}
	var fn = Datacore.syncCallbackQueue.shift();
	if( typeof fn === 'function' ) {
		fn();
	}
	if( Datacore.syncCallbackQueue.length > 0 ) {
		Datacore.processCallbackQueue();
	}
};

Datacore.refreshFolders = function refreshFolders() {
	if( typeof window.top.tablesPanel === 'object' && window.top.tablesPanel !== null ) {
		window.top.tablesPanel.sortFolders();
	}
	if( typeof window.top.fileSelection === 'object' && window.top.fileSelection !== null ) {
		window.top.fileSelection.refresh();
	}
};

Datacore.syncAllFolders = function( callback )
{
	if( typeof callback === 'function' ) {
		Datacore.syncCallbackQueue.push( callback );
	}
	if( Datacore._sync ) {
		console.log( 'Datacore.syncAllFolders; -- deferred, sync in progress' );
		return;
	}
	Datacore._sync = true;
	var depositionID = Datacore.s.depositionID;
	console.log( 'Datacore.syncAllFolders', depositionID, typeof callback );
	var deposition = Datacore.getDeposition( depositionID );
	var requests = {};
	$.getJSON( '/depositions/folders?depositionID=' + depositionID, null, function( data ) {
		if( isset( data ) && isset( data.status ) && data.status === 'success' ) {
			if( isset( data.folders ) ) {
				processFolders( data.folders, callback );
			} else {
				Datacore._sync = false;
				Datacore.refreshFolders();
				Datacore.processCallbackQueue();
			}
		}
	} );

	function processFolders( folders, callback ) {
		if( !Array.isArray( folders ) ) {
			console.warn( 'argument is not an array', folders );
			Datacore._sync = false;
			Datacore.refreshFolders();
			Datacore.processCallbackQueue();
			return;
		}
		for( var f=0; f < folders.length; ++f ) {
			var _folder = folders[f];
			var folderID = parseInt( _folder.ID );
			if( isNaN( folderID ) || !(folderID > 0) ) {
				console.log( 'processFolders; invalid folderID:', _folder.ID );
				continue;
			}
			var a = _folder.lastModified.split( /[^0-9]/ );
			var lastModified = new Date( a[0], a[1]-1, a[2], a[3], a[4], a[5] );
			lastModified.setMilliseconds( 0 );
			var dcFolder = Datacore.getFolder( folderID );
			if( isset( dcFolder ) ) {
//				console.log( 'processFolders; folderID:', folderID, 'lastModified:', lastModified );
//				console.log( 'processFolders; dcFolder:', dcFolder.ID, 'lastModified:', dcFolder.lastModified );
				var fLastMod = dcFolder.lastModified;
				fLastMod.setMilliseconds( 0 );
				if( fLastMod.getTime() === lastModified.getTime() && dcFolder.numFiles === dcFolder.getFileIDs().length ) {
//					console.log( 'processFolders;', dcFolder.ID, 'lastModified:', fLastMod, 'already in Datacore. files:', dcFolder.getFileIDs().length, 'of', dcFolder.numFiles );
					continue;
				}
			}
			deposition.addFolder( _folder );
			requests[folderID] = folderID;
//			console.log( 'Datacore.syncAllFolders; syncFolder:', folderID );
			Datacore.syncFolder( folderID, function( folder ) {
				if( isset( folder ) ) {
					delete requests[folder.ID];
				}
				if( Object.keys( requests ).length === 0 ) {
					Datacore._sync = false;
					Datacore.refreshFolders();
					Datacore.processCallbackQueue();
				}
			} );
		}
		if( Object.keys( requests ).length === 0 ) {
			Datacore._sync = false;
			Datacore.refreshFolders();
			Datacore.processCallbackQueue();
		}
	}
};

Datacore.loadFiles = function( params ) {
	if (typeof params === 'undefined' || params === null) {
		return;
	}
	if (!Array.isArray( params )) {
		params = [params];
	}

	for (var i=0; i<params.length; ++i) {
		this.addFile( params[i] );
	}
};

Datacore.addFile = function( params ) {
	var fileID = 0;
	if( typeof params !== 'object' || params === null || typeof params.ID === 'undefined' || params.ID === null ) {
		return -1;
	}
	var fileID = parseInt( params.ID );
	if( isNaN( fileID ) || fileID <= 0 ) {
		return -1;
	}
	var dcFile = Datacore.getFile( fileID );
	if( typeof dcFile === 'undefined' || dcFile === null ) {
		dcFiles[fileID] = new DcFile( params );
	} else {
		dcFiles[fileID].init( params );
	}
	return true;
};

Datacore.dropFile = function( fileID ) {
	if (typeof fileID === 'undefined' || fileID === null || !(fileID in dcFiles)) {
		return null;
	}
	var fileObj = dcFiles[fileID];
	delete dcFiles[fileID];
	return fileObj;
};

Datacore.hasFile = function( fileID ) {
	return (fileID in dcFiles);
};

Datacore.getFile = function( fileID ) {
	return (typeof fileID !== 'undefined' && fileID !== null) ? this.getStoredItems( dcFiles, fileID )[0] : null;
};

Datacore.getFiles = function( fileIDs ) {
	return this.getStoredItems( dcFiles, fileIDs );
};

Datacore.loadExhibitHistory = function( params, callback ) {
	if (typeof params === 'undefined' || params === null) {
		return;
	}
	if (!Array.isArray( params )) {
		params = [params];
	}

	dcExhibitHistory = {};

	for (var i=0; i<params.length; ++i) {
		this.addExhibitHistory( params[i] );
	}

	if( typeof callback === 'function' ) {
		callback();
	}
};

Datacore.addExhibitHistory = function( params ) {
	if (typeof params.ID === 'undefined' || params.ID === null) {
		return -1;
	}
//	if (params.ID in dcExhibitHistory) {
//		return false;
//	}
	dcExhibitHistory[params.ID] = new DcExhibitHistory( params );
	return true;
};

Datacore.dropExhibitHistory = function( exhibitID ) {
	if (typeof exhibitID === 'undefined' || exhibitID === null || !(exhibitID in dcExhibitHistory)) {
		return null;
	}
	var exhibitObj = dcExhibitHistory[exhibitID];
	delete dcExhibitHistory[exhibitID];
	return exhibitObj;
};

Datacore.hasExhibit = function( exhibitID ) {
	return (exhibitID in dcExhibitHistory);
};

Datacore.getExhibit = function( exhibitID ) {
	return (typeof exhibitID !== 'undefined' && exhibitID !== null) ? this.getStoredItems( dcExhibitHistory, exhibitID )[0] : null;
};

Datacore.getExhibit = function( exhibitIDs ) {
	return this.getStoredItems( dcExhibitHistory, exhibitIDs );
};

Datacore.getExhibitByExhibitFileID = function( exhibitFileID ) {
	var keys = Object.keys( dcExhibitHistory );

	// Return the last history item found.
	var result = null;
	for (var k in keys) {
		var depositionID = keys[k];
		if (parseInt( dcExhibitHistory[depositionID].exhibitFileID ) === parseInt( exhibitFileID )) {
			result = dcExhibitHistory[depositionID];
		}
	}
	return result;
};

Datacore.getExhibitsBySourceFileID = function( sourceFileID ) {
//	console.log( 'getExhibitsBySourceFileID:', sourceFileID );
	var keys = Object.keys( dcExhibitHistory );
	var results = [];

	for (var k in keys) {
		var depositionID = keys[k];
		if (parseInt( dcExhibitHistory[depositionID].sourceFileID ) === parseInt( sourceFileID )) {
			results.push( dcExhibitHistory[depositionID] );
		}
	}
//	console.log( 'getExhibitsBySourceFileID; results:', results );
	return results;
};

function DcUser(params) {
	this.uid = null;
	this.ID = null;
	this.clientID = null;
	this.typeID = null;
	this.clientTypeID = null;
	this.name = null;
	this.email = null;
	this.guest = null;
	this.active = null;
	this.userType = null;
	this.blockExhibits = null;
	this.depoParameters = null;

	if (typeof params !== 'undefined' && params !== null) {
		this.init(params);
	}
}

DcUser.prototype.init = function( params ) {
//	console.log( "DcUser.init:", params );
	if (typeof params.uid !== 'undefined' && params.uid !== null) {
		this.uid = params.uid;
	}

	if (typeof params.ID !== 'undefined' && params.ID !== null) {
		this.ID = parseInt( params.ID );
	}

	if (typeof params.id !== 'undefined' && params.id !== null) {
		this.ID = parseInt( params.id );
	}

	if (typeof params.clientID !== 'undefined' && params.clientID !== null) {
		this.clientID = parseInt( params.clientID );
	}

	if (typeof params.clientid !== 'undefined' && params.clientid !== null) {
		this.clientid = parseInt( params.clientid );
	}

	if (typeof params.name !== 'undefined' && params.name !== null) {
		this.name = params.name;
	}

	if (typeof params.email !== 'undefined' && params.email !== null) {
		this.email = params.email;
	}

	if (typeof params.guest !== 'undefined' && params.guest !== null) {
		this.guest = params.guest;
	}

	if (typeof params.active !== 'undefined' && params.active !== null) {
		this.active = !!params.active;
	}

	if (typeof params.typeID !== 'undefined' && params.typeID !== null) {
		this.typeID = params.typeID;
	}

	if( typeof params.clientTypeID !== 'undefined' && params.clientTypeID !== null) {
		this.clientTypeID = params.clientTypeID;
	}

	if (typeof params.userType !== 'undefined' && params.userType !== null) {
		this.userType = params.userType;
	}

	if (typeof params.blockExhibits !== 'undefined' && params.blockExhibits !== null) {
		this.blockExhibits = !!params.blockExhibits;
	}

	if (typeof params.depoParameters !== 'undefined' && params.depoParameters !== null) {
		this.depoParameters = params.depoParamters;
	}
};

function DcRoom( params ) {
	this.ID = null;
	this.creatorUserID = null;
	this.title = null;
	this.private = null;
	this.readOnly = null;
	this.isChatEnabled = null;
	this.unreadMessageCount = null;
	this.unreadChildMessageCount = null;

	var memberUserIDs = [];
	var messages = [];

	this.getMemberIDs = function() {
		return memberUserIDs;
	};

	this.getMemberCount = function() {
		return memberUserIDs.length;
	};

	this.getUserCount = function() {
		return this.getMemberCount();
	};

	this.hasMember = function( userID ) {
		return (memberUserIDs.indexOf( userID ) >= 0);
	};

	this.addMemberID = function( userID ) {
		userID = parseInt( userID );
		if (Datacore.hasUser( userID ) && !this.hasMember( userID )) {
			memberUserIDs.push( userID );
		}
	};

	this.addMember = function( params ) {
		if (this.ID === null) {
			throw 'Cannot add User to uninitialized Room.';
		}

		if (typeof params === 'undefined' || params === null) {
			return null;
		}

		if (typeof params.id !== 'undefined') {
			var memberID = parseInt( params.id );
		} else if (typeof params.ID !== 'undefined') {
			memberID = parseInt( params.ID );
		} else {
			throw 'Room Member ID must be defined.';
		}

		if (!this.hasMember( memberID )) {
			if (!Datacore.hasUser( memberID ) && Datacore.addUser( params ) === -1) {
				return null;
			}

			memberUserIDs.push( memberID );
		}
		return memberID;
	};

	this.dropMember = function( userID ) {
		userID = parseInt( userID );
		if (typeof userID !== 'undefined' && memberUserIDs.indexOf( userID ) >= 0) {
			memberUserIDs.splice( memberUserIDs.indexOf( userID ), 1 );
		}
	};

	this.getMessages = function() {
		return messages;
	};

	this.getMessageCount = function() {
		return messages.length;
	};

	this.addMessage = function( params ) {
		if (!isset( params ) || !isset( params.user ) || !isset( params.user.name )) {
			return;
		}

		messages.push( {
			userName : params.user.name,
			text : (isset( params.message ) ? params.message : ''),
			timestamp : (isset( params.timestamp ) ? params.timestamp : -1),
			system : (isset( params.system ) ? params.system : false)
		} );
	};

	if (isset( params )) {
		this.init( params );
	}
}

DcRoom.prototype.init = function( params ) {
	if (isset( params.id )) {
		this.ID = params.id;
	}

	if (isset( params.ID )) {
		this.ID = params.ID;
	}

	if (isset( params.title )) {
		this.title = params.title;
	}

	if (isset( params.private )) {
		this.private = params.private;
	}

	if (isset( params.readOnly )) {
		this.readOnly = params.readOnly;
	}

	if (isset( params.isChatEnabled )) {
		this.isChatEnabled = params.isChatEnabled;
	}

	if (isset( params.creator )) {
		var userID = isset( params.creator.id ) ? params.creator.id : (isset( params.creator.ID ) ? params.creator.ID : null);
		if (userID && (Datacore.hasUser( userID ) || Datacore.addUser( params.creator ))) {
			this.creatorUserID = userID;
		}
	}

	if (isset( params.messages ) && params.messages.length > 0) {
		for (var m in params.messages) {
			this.addMessage( params.messages[m] );
		}
	}

	this.unreadMessageCount = 0;

	if (this.private === false) {
		this.unreadChildMessageCount = 0;
	}

	if (isset( params.users ) && params.users.length > 0) {
		for (var u in params.users) {
			if (!isNaN( parseInt( params.users[u] ) )) {
				this.addMemberID( params.users[u] );
			} else {
				this.addMember( params.users[u] );
			}
		}
	}

	// for private rooms, set the room title as chat user name
	if (this.private !== null && this.private) {
		if (this.ID !== null) {
			var splitValues = this.ID.split('.');
			var otherUserID = (parseInt( splitValues[2] ) !== Datacore.s.currentUserID) ? parseInt( splitValues[2] ) : parseInt( splitValues[3] );
			if (typeof otherUserID !== 'undefined' && otherUserID !== null && Datacore.hasUser( otherUserID )) {
				this.title = Datacore.getUser( otherUserID ).name;
			}
		}
	}
};

function DcCase(params) {
	// public properties
	this.ID = null;
	this.sortPos = null;

	// private properties
	var caseDepositionIDs = [];

	// privileged methods
	this.getDepositionIDs = function() {
		return caseDepositionIDs;
	};

	this.addDeposition = function( params ) {
		if (this.ID === null) {
			throw 'Cannot add Deposition to uninitialized Case.';
		}
		if (typeof params.caseID !== 'undefined' && params.caseID !== null) {
			if (parseInt(params.caseID) !== this.ID) {
				throw 'Cannot add Deposition to non-owner Case.';
			}
		} else {
			params.caseID = this.ID;
		}
		var status = Datacore.addDeposition( params );
		var depoID = null;
		if( status ) {
			depoID = parseInt( params.ID );
			if( caseDepositionIDs.indexOf( depoID ) === -1 ) {
				caseDepositionIDs.push( depoID );
			}
		}
		return status !== -1 ? depoID : null;
	};

	this.dropDeposition = function( depositionID ) {
		var depoObj = Datacore.dropDeposition( depositionID );
		if( depoObj ) {
			var idxOfID = caseDepositionIDs.indexOf( depoObj.ID );
			if( idxOfID >= 0 ) {
				caseDepositionIDs.splice( idxOfID, 1 );
			}
		}
		return depoObj;
	};

	// constructor
	if (typeof params !== 'undefined' && params !== null) {
		this.init(params);
	}
}

DcCase.prototype.init = function(params) {
	if (typeof params.ID !== 'undefined' && params.ID !== null) {
		this.ID = parseInt( params.ID );
	}

	if (typeof params.clientID !== 'undefined' && params.clientID !== null) {
		this.clientID = params.clientID;
	}

	if (typeof params.typeID !== 'undefined' && params.typeID !== null) {
		this.typeID = params.typeID;
	}

	if( typeof params.class !== 'undefined' && params.class !== null ) {
		this.class = params.class;
	}

	if (typeof params.name !== 'undefined' && params.name !== null) {
		this.name = params.name;
	}

	if (typeof params.number !== 'undefined' && params.number !== null) {
		this.number = params.number;
	}

	if (typeof params.sortPos !== 'undefined' && params.sortPos !== null) {
		this.sortPos = parseInt( params.sortPos ) || this.ID;
	}

	if (typeof params.depositions !== 'undefined' && Array.isArray( params.depositions )) {
		for (var di in params.depositions) {
			this.addDeposition(params.depositions[di]);
		}
	}
};

DcCase.prototype.getDeposition = function( depositionID ) {
	return this.getDepositions( depositionID );
};

DcCase.prototype.getDepositions = function( depositionIDs ) {
	var depoIds = [];
	if (typeof depositionIDs !== 'undefined' && depositionIDs !== null) {
		if (Array.isArray( depositionIDs )) {
			depoIds = depositionIDs;
		} else {
			depoIds.push( depositionIDs );
		}
	}
	return Datacore.getDepositions( (depoIds.length > 0 ? depoIds : this.getDepositionIDs()) );
};

function DcDeposition(params) {
	this.ID = null;
	this.speakerID = null;
	this.ownerID = null;
	this.parentID = null;
	this.created = null;
	this.finished = null;
	this.openDateTime = null;	//scheduled
	this.started = null;
	this.liveTranscript = null;

	var depositionFolderIDs = [];

	this.getFolderIDs = function() {
		return depositionFolderIDs;
	};

	this.addFolder = function( params )
	{
		if (this.ID === null) {
			throw 'Cannot add Folder to uninitialized Deposition.';
		}

		var status = Datacore.addFolder( params );
		var folderID = null;
		if( status ) {
			folderID = parseInt( params.ID );
			if( depositionFolderIDs.indexOf( folderID ) === -1 ) {
				depositionFolderIDs.push( folderID );
			}
		}
		return status !== -1 ? folderID : null;
	};

	this.dropFolder = function( folderID )
	{
		var folderObj = Datacore.dropFolder( folderID );
		if( folderObj ) {
			var idxOfID = depositionFolderIDs.indexOf( folderObj.ID );
			if( idxOfID >= 0 ) {
				depositionFolderIDs.splice( idxOfID, 1 );
			}
		}
		return folderObj;
	};

	if (typeof params !== 'undefined' && params !== null) {
		this.init(params);
	}
	var _folders = Datacore.getFolders();
	for( var f in _folders ) {
		var _folder = _folders[f];
		if( _folder.depositionID === this.ID || (isset(_folder.caseID) && _folder.caseID > 0)) {
			this.addFolder( _folder );
		}
	}
}

DcDeposition.prototype.init = function(params) {
	if (typeof params.ID !== 'undefined' && params.ID !== null) {
		this.ID = parseInt( params.ID );
	}

	if (typeof params.caseID !== 'undefined' && params.caseID !== null) {
		this.caseID = parseInt( params.caseID );
	}

	if (typeof params.depositionOf !== 'undefined' && params.depositionOf !== null) {
		this.depositionOf = params.depositionOf;
	}

	if (typeof params.title !== 'undefined' && params.title !== null) {
		this.title = params.title;
	}

	if (typeof params.volume !== 'undefined' && params.volume !== null) {
		this.volume = params.volume;
	}

	if (typeof params.statusID !== 'undefined' && params.statusID !== null) {
		this.statusID = params.statusID;
	}

	if (typeof params.createdBy !== 'undefined' && params.createdBy !== null) {
		this.createdBy = parseInt( params.createdBy );
	}

	if (typeof params.ownerID !== 'undefined' && params.ownerID !== null) {
		this.ownerID = parseInt( params.ownerID );
	}

	if( typeof params.parentID !== 'undefined' && params.parentID !== null ) {
		this.parentID = parseInt( params.parentID );
	}

	if (typeof params.speakerID !== 'undefined' && params.speakerID !== null) {
		this.speakerID = parseInt( params.speakerID );
	}

	if (typeof params.class !== 'undefined' && params.class !== null) {
		this.class = params.class;
	}

	if( typeof params.created !== 'undefined' && params.created !== null ) {
		if( typeof params.created === 'string' ) {
			var c = params.created.split( /[^0-9]/ );
			this.created = new Date( c[0], c[1]-1, c[2], c[3], c[4], c[5] );
		} else {
			this.created = params.created;
		}
	}

	if( typeof params.started !== 'undefined' && params.started !== null ) {
		if( typeof params.started === 'string' ) {
			var s = params.started.split( /[^0-9]/ );
			this.started = new Date( s[0], s[1]-1, s[2], s[3], s[4], s[5] );
		} else {
			this.started = params.started;
		}
	}

	if( typeof params.finished !== 'undefined' && params.finished !== null ) {
		if( typeof params.finished === 'string' ) {
			var f = params.finished.split( /[^0-9]/ );
			this.finished = new Date( f[0], f[1]-1, f[2], f[3], f[4], f[5] );
		} else {
			this.finished = params.finished;
		}
	}

	if (typeof params.banned !== 'undefined' && params.banned !== null) {
		if( typeof params.banned === 'string' ) {
			var f = params.banned.split( /[^0-9]/ );
			this.banned = new Date( f[0], f[1]-1, f[2], f[3], f[4], f[5] );
		} else {
			this.banned = params.banned;
		}
	}

	if( typeof params.openDateTime !== 'undefined' && params.openDateTime !== null ) {
		if( typeof params.openDateTime === 'string' ) {
			var f = params.openDateTime.split( /[^0-9]/ );
			this.openDateTime = new Date( f[0], f[1]-1, f[2], f[3], f[4], f[5] );
		} else {
			this.openDateTime = params.openDateTime;
		}
	}

	if( typeof params.sortPos !== 'undefined' && params.sortPos !== null ) {
		this.sortPos = parseInt( params.sortPos ) || this.ID;
	}

	if( typeof params.liveTranscript !== 'undefined' && params.liveTranscript !== null ) {
		if( params.liveTranscript === 'Y' || params.liveTranscript === 'N' ) {
			this.liveTranscript = params.liveTranscript;
		}
	}
};

DcDeposition.prototype.getFolder = function( folderID ) {
	return this.getFolders( folderID );
};

DcDeposition.prototype.getFolders = function( folderIDs ) {
	var getIds = [];
	if (typeof folderIDs !== 'undefined' && folderIDs !== null) {
		if (Array.isArray( folderIDs ) ) {
			getIds = folderIDs;
		} else {
			getIds.push( folderIDs );
		}
	}
	return Datacore.getFolders( (getIds.length > 0 ? getIds : this.getFolderIDs()) );
};

function DcFolder( params )
{
	this.ID = null;
	this.caseID = null;
	this.depositionID = null;
	this.createdBy = null;
	this.name = null;
	this.created = null;
	this.lastModified = null;
	this.numFiles = null;
	this.class = null;
	this.sortPos = null;

	var folderFileIDs = [];

	this.getFileIDs = function() {
		return folderFileIDs.slice(0);	//return a clone
	};

	this.addFile = function( params ) {
		if (this.ID === null) {
			throw 'Cannot add Document to uninitialized Folder.';
		}

		if (typeof params.folderID === 'undefined' || params.folderID === null) {
			params.folderID = this.ID;
		}

		var status = Datacore.addFile( params );
		var fileID = null;
		if( status ) {
			fileID = parseInt( params.ID );
			if( folderFileIDs.indexOf( fileID ) === -1 ) {
				folderFileIDs.push( fileID );
				this.numFiles += 1;
			}
		}
		return status !== -1 ? fileID : null;
	};

	this.dropFile = function( fileID ) {
		var fileObj = Datacore.dropFile( fileID );
		if( fileObj ) {
			var idxOfID = folderFileIDs.indexOf( fileObj.ID );
			if( idxOfID >= 0 ) {
				folderFileIDs.splice( idxOfID, 1 );
			}
			this.numFiles -= 1;
		}
		return fileObj;
	};

	if( typeof params !== 'undefined' && params !== null ) {
		this.init( params );
	}
}

DcFolder.prototype.init = function( params )
{
	if( typeof params.ID !== 'undefined' && params.ID !== null ) {
		this.ID = parseInt( params.ID );
	}
	if( typeof params.caseID !== 'undefined' && params.caseID !== null ) {
		this.caseID = parseInt( params.caseID );
	}
	if( typeof params.depositionID !== 'undefined' && params.depositionID !== null ) {
		this.depositionID = parseInt( params.depositionID );
	}
	if (typeof params.created !== 'undefined' && params.created !== null) {
		if( typeof params.created === 'string' ) {
			var a = params.created.split( /[^0-9]/ );
			this.created = new Date( a[0], a[1]-1, a[2], a[3], a[4], a[5] );
		} else {
			this.created = params.created;
		}
	}

	if (typeof params.name !== 'undefined' && params.name !== null) {
		this.name = params.name;
	}

	if( typeof params.lastModified !== 'undefined' && params.lastModified !== null ) {
		if( typeof params.lastModified === 'string' ) {
			var a = params.lastModified.split( /[^0-9]/ );
			this.lastModified = new Date( a[0], a[1]-1, a[2], a[3], a[4], a[5] );
		} else {
			this.lastModified = params.lastModified;
		}
	}

	if (typeof params.numFiles !== 'undefined' && params.numFiles !== null) {
		this.numFiles = parseInt( params.numFiles );
	}
	if (typeof params.class !== 'undefined' && params.class !== null) {
		this.class = params.class;
	}

	if (typeof params.sortPos !== 'undefined' && params.sortPos !== null) {
		this.sortPos = parseInt( params.sortPos ) || this.ID;
	}
};

DcFolder.prototype.getFile = function( fileID ) {
	return this.getFiles( fileID );
};

DcFolder.prototype.getFiles = function( fileIDs ) {
	var getIds = [];
	if (typeof fileIDs !== 'undefined' && fileIDs !== null) {
		if (Array.isArray( fileIDs ) ) {
			getIds = fileIDs;
		} else {
			getIds.push( fileIDs );
		}
	}
	return Datacore.getFiles( (getIds.length > 0 ? getIds : this.getFileIDs() ) );
};

function DcFile(params) {
	this.ID = null;
	this.folderID = null;
	this.name = null;
	this.created = null;
	this.createdBy = null;
	this.isExhibit = null;
	this.isPrivate = null;
	this.mimeType = null;
	this.sortPos = null;
	this.byUser = {
		ID : null,
		firstName : null,
		lastName : null,
		email : null
	};

	if (typeof params !== 'undefined' && params !== null) {
		this.init(params);
	}
}

DcFile.prototype.init = function(params) {
	if (typeof params.ID !== 'undefined' && params.ID !== null) {
		this.ID = parseInt( params.ID );
	}

	if (typeof params.folderID !== 'undefined' && params.folderID !== null) {
		this.folderID = parseInt( params.folderID );
	}

	if (typeof params.caseFolderID !== 'undefined' && params.caseFolderID !== null) {
		this.caseFolderID = parseInt( params.caseFolderID );
	}

	if (typeof params.name !== 'undefined' && params.name !== null) {
		this.name = params.name;
	}

	if (typeof params.created !== 'undefined' && params.created !== null) {
		if( typeof params.created === 'string' ) {
			var a = params.created.split( /[^0-9]/ );
			this.created = new Date( a[0], a[1]-1, a[2], a[3], a[4], a[5] );
		} else {
			this.created = params.created;
		}
	}

	if (typeof params.createdBy !== 'undefined' && params.createdBy !== null) {
		this.createdBy = parseInt( params.createdBy );
	}

	if (typeof params.mimeType !== 'undefined' && params.mimeType !== null) {
		this.mimeType = params.mimeType;
	}

	if (typeof params.isExhibit !== 'undefined' && params.isExhibit !== null) {
		if (typeof params.isExhibit === 'boolean') {
			this.isExhibit = params.isExhibit;
		} else {
			this.isExhibit = params.isExhibit === '1' ? true : false;
		}
	}

	if (typeof params.byUser !== 'undefined' && params.byUser !== null) {
		if (typeof params.byUser.ID !== 'undefined' && params.byUser.ID !== null) {
			this.byUser.ID = params.byUser.ID;
		}

		if (typeof params.byUser.firstName !== 'undefined' && params.byUser.firstName !== null) {
			this.byUser.firstName = params.byUser.firstName;
		}

		if (typeof params.byUser.lastName !== 'undefined' && params.byUser.lastName !== null) {
			this.byUser.lastName = params.byUser.lastName;
		}

		if (typeof params.byUser.email !== 'undefined' && params.byUser.email !== null) {
			this.byUser.email = params.byUser.email;
		}
	}

	if (typeof params.sortPos !== 'undefined' && params.sortPos !== null) {
		this.sortPos = parseInt( params.sortPos ) || this.ID;
	}
};

function DcExhibitHistory(params) {
	this.ID = null;
	this.exhibitFileID = null;
	this.sourceFileID = null;
	this.caseID = null;
	this.depositionID = null;
	this.introducedDate = null;
	this.introducedBy = null;

	if (typeof params !== 'undefined' && params !== null) {
		this.init(params);
	}
}

DcExhibitHistory.prototype.init = function(params) {
	if (typeof params.ID !== 'undefined' && params.ID !== null) {
		this.ID = params.ID;
	}

	if (typeof params.exhibitFileID !== 'undefined' && params.exhibitFileID !== null) {
		this.exhibitFileID = params.exhibitFileID;
	}

	if (typeof params.sourceFileID !== 'undefined' && params.sourceFileID !== null) {
		this.sourceFileID = params.sourceFileID;
	}

	if (typeof params.caseID !== 'undefined' && params.caseID !== null) {
		this.caseID = params.caseID;
	}

	if (typeof params.depositionID !== 'undefined' && params.depositionID !== null) {
		this.depositionID = params.depositionID;
	}

	if (typeof params.introducedDate !== 'undefined' && params.introducedDate !== null) {
		if( typeof params.introducedDate === 'string' ) {
			var a = params.introducedDate.split( /[^0-9]/ );
			this.introducedDate = new Date( a[0], a[1]-1, a[2], a[3], a[4], a[5] );
		} else {
			this.introducedDate = params.introducedDate;
		}
	}

	if (typeof params.introducedBy !== 'undefined' && params.introducedBy !== null) {
		this.introducedBy = params.introducedBy;
	}
};
