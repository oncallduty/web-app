//NO NEED TO USE THIS FILE?
window.document.domain = "edepoze.com";
//window.document.domain = "armyargentina.com";
var Multimedia = window.top.Multimedia = {

	fileID : 0,
	exhibitFolderID : 0,
	isSkipStamp : false,
	firstExhibit : false,
	personalFolderID : null,
	sourceFileID : 0,
	filename : null,
	blockButtons : false,
	introduceStep : false,
	updatedFilename : null,
	exhibitText : null,
	depositionLeaderID : null,
	isDragIntroduce: false,
	exhibitXOrigin : 0,
	exhibitYOrigin : 0,
	isSocialHidden : true,
	stamp : null,
	depoClass: 'Deposition',

	init : function(beginIntroduce) {
		console.log( 'Multimedia.Init()' );
		Multimedia.depoClass = window.top.webApp.getS( 'deposition.class' );
		Multimedia.stamp = $('#stamp');
		Multimedia.exhibitXOrigin = 850; // based on our pre-defined constant screen size
		Multimedia.exhibitYOrigin = 580; // based on our pre-defined constant screen size
		Multimedia.isSocialHidden = $( '#ipad_navbar', window.top.document ).hasClass( 'hideSocialList' );
		this.isDragIntroduce = !!beginIntroduce;
		Multimedia.introduceStep = '1';
		var File = window.top.Datacore.getFile( requestedFileID );
		Multimedia.fileID = File.ID;
		Multimedia.stamp.on( "click", Multimedia.showStampEdit );
		$('#dialogIntroduceInput', window.top.document).on("click", Multimedia.toggleStamp);
		$('#dialogTextInputCancel', window.top.document).on("click", Multimedia.skipStampCancel);

		if ( window.top.Datacore.getS( 'depositionStatus' ) === 'I' ) {
			if( window.top.webApp.userIsSpeaker() === true ) {
				Multimedia.depositionLeaderID = window.top.Datacore.s.speakerID;
			}
			else {
				Multimedia.depositionLeaderID = window.top.Datacore.s.conductorID;
			}
			// Check for first introduction.
			if (window.top.Datacore.s.exhibitTitle.length === 0 && window.top.Datacore.s.exhibitSubTitle.length === 0) {
				window.top.Datacore.s.exhibitTitle = window.top.Datacore.s.depositionOf;
				window.top.Datacore.s.exhibitSubTitle = 'Exhibit_0';
				Multimedia.firstExhibit = true;
			}
			console.log( "Multimedia.firstExhibit = " + Multimedia.firstExhibit );
			Multimedia.exhibitText = {title: window.top.Datacore.s.exhibitTitle, subTitle: window.top.Datacore.s.exhibitSubTitle};
			Multimedia.updateExhibitCounter();
			var folders = window.top.Datacore.getFolders();
			for(var f in folders) {
				if (folders[f].class === 'Exhibit') {
					Multimedia.exhibitFolderID = folders[f].ID;
					window.top.Datacore.syncFolder(Multimedia.exhibitFolderID);
					break;
				}
			}
		}
		window.top.navBar.setTitle( File.name );
	},

	cleanEvents : function() {
		console.log( 'Multimedia.cleanEvents()' );
		Multimedia.stamp.off( "click", Multimedia.showStampEdit );
		window.top.navBar.introduceButton.removeClass('green').addClass('dark').label('Introduce');
		$('#dialogIntroduceInput', window.top.document).off("click", Multimedia.toggleStamp);
		$('#dialogTextInputCancel', window.top.document).off("click", Multimedia.skipStampCancel);
	},

	backButton : function() {
//		console.log( 'Entering Multimedia.backButton()' );
		if( Multimedia.introduceStep === '2' ) {
			var dialogButtons = [];
			dialogButtons.push( {label:"No", action:'dismissDialog3Btn();', class:'btn_app_cancel'} );
			dialogButtons.push( {label:'Yes', id:'backYes', class:'btn_app_normal'} );
			var confirmMsg = 'You may have changes to this document.  Are you sure you want to exit without saving?';
			window.top.showDialog3Btn( 'Abort Introduction?', confirmMsg, dialogButtons );

			$('#dialog3Btn #backYes', window.top.document).on( "click", function() {
				Multimedia.introduceStep = '1';
				window.top.dismissDialog3Btn();
				Multimedia.cleanEvents();
				top.iPadContainerFrame.location.replace( '/depositions' );
			} );
		} else {
			Multimedia.introduceStep = '1';
			Multimedia.cleanEvents();
			top.iPadContainerFrame.location.replace( '/depositions' );
		}
	},

	resize : function( socialHidden ) {
		console.log( 'Multimedia.resize(): socialHidden = ' + socialHidden );
		console.log( 'Multimedia.resize(): $(\'#ipad_navbar\', window.top.document).hasClass(\'hideSocialList\') = ' + $( '#ipad_navbar', window.top.document ).hasClass( 'hideSocialList' ) );
		Multimedia.isSocialHidden = socialHidden;
		Multimedia.exhibitXOrigin = ( Multimedia.isSocialHidden ? 850 : 595 );  // based on our pre-defined constant screen size
		Multimedia.exhibitYOrigin = 580;  // based on our pre-defined constant screen size
		if( Multimedia.stamp.is(":visible") ){
			Multimedia.showStamp( false );
		}
	},

	updateSpeaker : function() {

		if( Multimedia.introduceStep === '2' ){
			window.top.clearDialogIntroduceInputCloseAction();
			window.top.dismissDialogIntroduceInput();
			window.top.showPlainDialog1Btn( 'Leader Role was Passed', 'This exhibit can no longer be introduced', 'Close' );
			Multimedia.cleanEvents();
			window.top.introduceRedirect(0, true);
			return;
		}

		if (window.top.Datacore.s.depositionStatus !== 'I') {
			window.top.navBar.introduceButton.disable();
			return;
		}
		Multimedia.depositionLeaderID = window.top.Datacore.s.conductorID;
		if( typeof window.top.Datacore.s.speakerID !== 'undefined' && window.top.Datacore.s.speakerID !== 0) {
			Multimedia.depositionLeaderID = window.top.Datacore.s.speakerID;
		}

		var file = window.top.Datacore.getFile(Multimedia.fileID);
		var showIntroduce = ( typeof file !== 'undefined' && !file.isExhibit );

//		var showIntroduce = true;
//		var file = window.top.Datacore.getFile(Multimedia.fileID);
//
//		if ( typeof file === 'undefined' ) {
//			showIntroduce = false;
//		} else {
//			if (file.isExhibit) {
//				showIntroduce = false;
//			}
//		}
		if (showIntroduce && Multimedia.depositionLeaderID === window.top.Datacore.s.currentUserID) {
			window.top.navBar.introduceButton.addClass( 'dark' ).enable();
		} else {
			window.top.navBar.introduceButton.removeClass( 'dark' ).disable();
		}
	},

	downloadMultimediaFile : function() {
		console.log( 'Multimedia.downloadMultimediaFile()' );
		var File = window.top.Datacore.getFile( requestedFileID );
		window.top.showTooltip( 'Downloading '+File.name, true );
		window.location.href = '/depositions/file?fileID=' + requestedFileID + '&filename=' + encodeURI(File.name);
	},

	sendMultimediaFile : function() {
		console.log( 'Multimedia.sendMultimediaFile()' );
		window.top.sendDialog.setFileID( Multimedia.fileID, null );
		window.top.sendDialog.show();
	},

	stopPlayback : function(){
		// Stop the playback if in progress
		if( $('#multimediaFileplayer video').length > 0 ){
			$('#multimediaFileplayer video').trigger("pause");
		}
		else {
			$('#multimediaFileplayer').trigger("pause");
		}
	},

	introduce : function() {
		Multimedia.stopPlayback();

		// Go ahead and do the introduce
		if( Multimedia.introduceStep === '2' ){
			Multimedia.introduceStep = '1';
			Multimedia.introduceFile();
		}
		// Show the stamp so I can edit it if I want
		else{
			Multimedia.introduceStep = '2';
			window.top.navBar.sendButton.disable();
			window.top.navBar.downloadButton.disable();
			window.top.fileSelection.disableFileSelection();
			Multimedia.exhibitXOrigin = ( Multimedia.isSocialHidden ? 850 : 595 );  // based on our pre-defined constant screen size
			Multimedia.exhibitYOrigin = 580;  // based on our pre-defined constant screen size
			Multimedia.fileID = parseInt( requestedFileID );

			if( Multimedia.depoClass === 'WitnessPrep' || Multimedia.depoClass === 'WPDemo' ) {
				window.top.navBar.skipstampButton.disable();
				window.top.navBar.introduceButton.removeClass( 'dark' ).addClass('green').label('Distribute').enable();
				return;
			}

			window.top.navBar.saveButton.disable();
			window.top.navBar.skipstampButton.onClick( Multimedia.skipStamp ).enable();
			window.top.navBar.introduceButton.removeClass( 'dark' ).addClass('green').label('Distribute').enable();
			window.top.navBar.cancelButton.enable().addClass('dark').onClick(function() {
				Multimedia.introduceStep = '1';
				window.top.dismissDialog3Btn();
				Multimedia.cleanEvents();
				window.top.navBar.sendButton.enable();
				window.top.navBar.saveButton.enable();
				window.top.navBar.downloadButton.enable();
				window.top.navBar.skipstampButton.disable();
				window.top.navBar.cancelButton.disable();
				Multimedia.hideStamp();
				window.top.fileSelection.enableFileSelection();
			});
			console.log( "Multimedia.firstExhibit = " + Multimedia.firstExhibit );
			if (Multimedia.firstExhibit) {
				editXOrigin = ( Multimedia.isSocialHidden ? 400 : 260 );  // based on our pre-defined constant screen size
				editYOrigin = 300;  // based on our pre-defined constant screen size
				window.top.showDialogIntroduceInput(Multimedia.exhibitText.title, Multimedia.exhibitText.subTitle, window.top.Datacore.s.exhibitDate, editXOrigin + 'px', editYOrigin + 'px');
				window.top.setDialogIntroduceInputCloseAction(Multimedia.finishEdit);
			}
			Multimedia.showStamp( Multimedia.firstExhibit );
		}
	},

	updateStamp : function() {
		console.log( 'Multimedia.updateStamp()' );
		var newTitleText = $('.dialogIntroduce #dialogTitle', window.top.document).val();
		var newSubTitleText = $('.dialogIntroduce #dialogSubtitle', window.top.document).val();
		var oldTitleText = window.top.Datacore.s.exhibitTitle;
		var oldSubTitleText = window.top.Datacore.s.exhibitSubTitle;

		if( oldTitleText == newTitleText && oldSubTitleText == newSubTitleText ) {
			return;
		}

		window.top.Datacore.s.exhibitTitle = newTitleText;
		window.top.Datacore.s.exhibitSubTitle = newSubTitleText;

		$('#stamp #dialogTitle').html( newTitleText );
		$('#stamp #dialogSubtitle').html( window.top.Datacore.s.exhibitSubTitle );
	},

	showStamp : function( firstExhibit) {
		console.log( 'Multimedia.showStamp()' );
		title = window.top.Datacore.s.exhibitTitle;
		subTitle = window.top.Datacore.s.exhibitSubTitle;

		// Get the input and show to the user.
		$('#stamp #dialogTitle').text(title);
		$('#stamp #dialogSubtitle').text(subTitle);
		$('#stamp #date').text(window.top.Datacore.s.exhibitDate);
		Multimedia.stamp[0].style.left = Multimedia.exhibitXOrigin + 'px';
		Multimedia.stamp[0].style.top = Multimedia.exhibitYOrigin + 'px';

		if ( firstExhibit ) {
			Multimedia.stamp.hide();
		} else {
			Multimedia.stamp.show();
		}
	},

	showStampEdit : function() {
		console.log( 'Multimedia.showStampEdit()' );
		Multimedia.showStamp();
		editXOrigin = ( Multimedia.isSocialHidden ? 775 : 514 );  // based on our pre-defined constant screen size
		editYOrigin = 600;  // based on our pre-defined constant screen size
		window.top.showDialogIntroduceInput($('#stamp #dialogTitle').text(), $('#stamp #dialogSubtitle').text(),
				$('#stamp #date').text(), editXOrigin + 'px', editYOrigin + 'px');
		Multimedia.stamp.hide();
	},

	toggleStamp : function(event) {
		console.log( 'Multimedia.toggleStamp()' );
		// Don't do anything if we didn't click out of the editor
		if( event.target.id !== 'dialogIntroduceInput'){
			return;
		}

		if( Multimedia.stamp.is(":visible") ){
			Multimedia.stamp.hide();
		}
		else {
			Multimedia.updateStamp();
			Multimedia.stamp.show();

			// Make sure editor is repositioned.
			//$('#dialogIntroduceInput .popupDialog', window.top.document).css('left', '38px' );
			//$('#dialogIntroduceInput .popupDialog', window.top.document).css('top', '120px' );
		}
	},

	hideStamp : function() {
		console.log( 'Multimedia.hideStamp()' );
		if( Multimedia.stamp.is(":visible") ){
			Multimedia.stamp.hide();
		}
	},

	skipStamp : function() {
		console.log( 'Multimedia.skipStamp()' );
		if (Multimedia.blockButtons) {
			return;
		}
		Multimedia.stamp.hide();
		var fileID = Multimedia.fileID;
		var dcFile = window.top.Datacore.getFile( fileID );
		if (dcFile === null) {
			return;
		}

		var index = dcFile.name.lastIndexOf(".");
		var filename = dcFile.name.substr(0, index);
		for(var f in window.top.dcFolders) {
			if (window.top.dcFolders[f].class == 'Exhibit') {
				Multimedia.exhibitFolderID = window.top.dcFolders[f].ID;
				window.top.Datacore.syncFolder(window.top.dcFolders[f].ID);
				break;
			}
		}
		window.top.setDialogTextInputCloseAction(Multimedia.validateFilename);
		window.top.showDialogTextInput("Rename the exhibit", "Save", filename);
		Multimedia.isSkipStamp = true;
	},

	skipStampCancel : function() {
		console.log( 'Multimedia.skipStampCancel()' );
		if( Multimedia.depoClass !== 'WitnessPrep' && Multimedia.depoClass !== 'WPDemo' ) {
			Multimedia.stamp.show();
		}
		Multimedia.isSkipStamp = false;
	},

	getSaveFilename : function() {
		console.log( 'Multimedia.getSaveFilename()' );
		// Stop the playback if in progress
		Multimedia.stopPlayback();

		var filename = Multimedia.updatedFilename;
		if (filename === null) {
			var fileID = parent.requestedFileID;
			var dcFile = window.top.Datacore.getFile( fileID );
			if (dcFile === null) {
				return;
			}

			var index = dcFile.name.lastIndexOf(".");
			filename = dcFile.name.substr(0, index);
			Multimedia.updatedFilename = filename;
		}
		if (Multimedia.personalFolderID === null) {
			for(var f in window.top.dcFolders) {
				if (window.top.dcFolders[f].class === 'Personal') {
					Multimedia.personalFolderID = window.top.dcFolders[f].ID;
					window.top.Datacore.syncFolder(Multimedia.personalFolderID);
					break;
				}
			}
		}
		window.top.setDialogSaveFileCloseAction(Multimedia.validateSaveFilename);
		window.top.showDialogSaveFile("Name Your Copy", "Save", filename);
	},

	pad: function(num, size) {
    	var s = num+"";
    	while (s.length < size) s = "0" + s;
    	return s;
	},

	incrementText : function(title) {
		var digits = '0123456789';
		var index = title.length - 1;

		//Look for any digits at the end of the string.
		while (index >= 0) {
			var char = title.charAt(index);
			if (digits.indexOf(char) >= 0) {
				index--;
			} else {
				break;
			}
		}

		if (index === title.length - 1) {
			// No digits at the end of the string, so do nothing to the string.
			return title;
		}

		var count;
		if (index === -1) {
			// The whole text is a number.
			count = parseInt(title, 10);
			count++;
			title = Multimedia.pad(count, title.length);
		} else {
			count = parseInt(title.substr(index + 1), 10);
			count++;
			title = title.substr(0, index + 1) + Multimedia.pad(count, title.length - index - 1);
		}
		return title;
	},

	updateExhibitCounter : function() {
		if (typeof Multimedia.exhibitText.title === 'undefined' || Multimedia.exhibitText.title === null) {
			Multimedia.exhibitText.title = window.top.Datacore.s.depositionOf;
		}

		if (typeof Multimedia.exhibitText.subTitle === 'undefined' || Multimedia.exhibitText.subTitle === null || Multimedia.exhibitText.subTitle == '') {
			Multimedia.exhibitText.subTitle = 'Exhibit_0';
		}

		if (Multimedia.exhibitText.subTitle.length > 0) {
			//Get the last subTitle value and add one to it.
			Multimedia.exhibitText.subTitle = Multimedia.incrementText( Multimedia.exhibitText.subTitle );
		} else {
			//Update the last value in the title.
			Multimedia.exhibitText.title = Multimedia.incrementText( Multimedia.exhibitText.title );
		}

		//window.top.Datacore.s.exhibitTitle = Multimedia.exhibitText.title;
		window.top.Datacore.s.exhibitSubTitle = Multimedia.exhibitText.subTitle;
	},

	introduceFile : function() {
		if (Multimedia.blockButtons) {
			return;
		}
		var exTitle = $('#stamp #dialogTitle').text().trim();
		var exSubtitle = $('#stamp #dialogSubtitle').text().trim();
		var filename = [exTitle, exSubtitle].join( '_' );
		filename = filename.replace( /\s+/g, '_' );
		Multimedia.isSkipStamp = false;
		Multimedia.validateFilename(filename);
	},

	validateFilename : function(filename) {
		// Sanitize the filename.  Only allow alphanumerics and underscore, space, hyphen, period.
		// Remove everything else.
		filename = filename.replace(/[^\w\040\-\.]+/g, '');
		var folder = window.top.Datacore.getFolder(Multimedia.exhibitFolderID);
		if (folder === null) {
			return;
		}
		var files = folder.getFiles();
		var duplicate = false;
		var duplicateFileID = 0;
		for(var f in files) {
			var shortName = files[f].name;
			var index = shortName.lastIndexOf(".");
			if (index >= 0) {
				shortName = shortName.substr(0, index);
			}
			if (shortName.toLowerCase() === filename.toLowerCase()) {
				duplicate = true;
				duplicateFileID = parseInt(files[f].ID, 10);
				break;
			}
		}
		Multimedia.blockButtons = true;
		if (duplicate) {
			//Confirm overwrite.
			var dialogButtons = [];
			dialogButtons.push( {label: 'No', id : 'no', class : 'btn_app_cancel'} );
			dialogButtons.push( {label: 'Yes', id : 'replace', class : 'btn_app_normal'} );
			var confirmMsg = 'An Official Exhibit by this name already exists.  Do you want to replace it?';
			window.top.showDialog3Btn( 'File Already Exists!', confirmMsg, dialogButtons, true );
			$('#dialog3Btn #no', window.top.document).on("click", function() {
				window.top.dismissDialog3Btn();
				Multimedia.blockButtons = false;
			});
			$('#dialog3Btn #replace', window.top.document).on("click", function() {
				window.top.dismissDialog3Btn();
				Multimedia.introduceFileSubmit( filename, Multimedia.fileID, duplicateFileID, true );
			});

		} else {
			Multimedia.introduceFileSubmit( filename, Multimedia.fileID, Multimedia.fileID, false );
		}
	},

	validateSaveFilename : function( filename, folderID ) {
		// Sanitize the filename.  Only allow alphanumerics and underscore, space, hyphen, period.
		// Remove everything else.
		filename = filename.replace(/[^\w\040\-\.]+/g, '');
		var folder;
		if ( typeof folderID !== 'undefined' ) {
			folder = window.top.Datacore.getFolder(folderID);
		} else {
			folder = window.top.Datacore.getFolder(Multimedia.personalFolderID);
		}
		if (folder === null) {
			return;
		}
		var files = folder.getFiles(folder.getFileIDs());
		var duplicate = false;
		var duplicateFileID = 0;
		for(var f in files) {
			var shortName = files[f].name;
			var index = shortName.lastIndexOf(".");
			if (index >= 0) {
				shortName = shortName.substr(0, index);
			}
			if (shortName.toLowerCase() === filename.toLowerCase()) {
				duplicate = true;
				duplicateFileID = parseInt(files[f].ID, 10);
				break;
			}
		}
		if (duplicate) {
			//Confirm overwrite.
			var dialogButtons = [];
			dialogButtons.push( {label: 'Cancel', action : 'dismissDialog3Btn();', class : 'btn_app_cancel'} );
			dialogButtons.push( {label: 'Replace', id : 'replace', class : 'btn_app_normal'} );
			var confirmMsg = 'A document by this name already exists.  Are you sure you want to replace the existing document?';
			window.top.showDialog3Btn( 'Document Already Exists!', confirmMsg, dialogButtons, true );
			$('#dialog3Btn #replace', window.top.document).on("click", function() {
				window.top.dismissDialog3Btn();
				Multimedia.fileID = duplicateFileID;
				Multimedia.save(filename, true, folderID);
			});
		} else {
			Multimedia.save(filename, false, folderID);
		}
	},

	save : function(filename, overwrite, folderID) {
		var file = window.top.Datacore.getFile(Multimedia.fileID);
		var extension = file.name.substr( (file.name.lastIndexOf('.') +1) );
		var curNameExt = filename.substr( (filename.lastIndexOf('.') +1) );

		if( extension.toLowerCase() !== curNameExt.toLowerCase() ){
			filename += '.' + extension;
		}

		var text = '[]';
		var url = "annotate?sourceFileID=" + Multimedia.fileID +
				  "&fileName=" + filename +
				  "&overwrite=" + overwrite +
				  "&fileID=" + Multimedia.fileID +
				  "&folderID=" + folderID +
				  '&isTempFile=' + ( typeof window.top.Datacore.s.tempFile !== 'undefined' );

		$.post(url, {annotations: text}, function(data, textStatus, xhr) {
			if (textStatus === "success" && data !== null) {
				window.top.showTooltip( 'File saved to server.', true );
				if (typeof window.top.Datacore.s.tempFile !== 'undefined') {
					Multimedia.deleteTempFile();
				} else {
					Multimedia.cleanEvents();
//					window.top.introduceRedirect( 0, true );
				}
			} else {
				window.top.showPlainDialog1Btn( 'Error', 'Failed to save File.', 'Close' );
			}
		}, "json")
		.fail(function() {
			window.top.showPlainDialog1Btn( 'Error', 'Failed to save File.', 'Close' );
		});
	},

	introduceFileSubmit : function(filename, sourceID, fileID, overwrite) {
		console.log( 'Multimedia.introduceFileSubmit()')
		showSpinner();
		var title;
		var subTitle;
		if (Multimedia.isSkipStamp) {
			title = window.top.Datacore.s.oldExhibitText.title;
			subTitle = window.top.Datacore.s.oldExhibitText.subTitle;
		} else {
			title = Multimedia.getText( $('#stamp #dialogTitle') );
			subTitle = Multimedia.getText( $('#stamp #dialogSubtitle') );
		}

		var file = window.top.Datacore.getFile(fileID);
		var extension = file.name.substr( (file.name.lastIndexOf('.') +1) );
		var curNameExt = filename.substr( (filename.lastIndexOf('.') +1) );

		if( extension.toLowerCase() !== curNameExt.toLowerCase() ){
			filename += '.' + extension;
		}

		console.log( "Multimedia.isSkipStamp = " + Multimedia.isSkipStamp );
		var url = 'introduceFile?filename=' + encodeURIComponent( filename ) +
				  '&fileID=' + fileID +
				  '&sourceFileID=' + sourceID +
				  '&exhibitXOrigin=' + Multimedia.exhibitXOrigin +
				  '&exhibitYOrigin=' + Multimedia.exhibitYOrigin +
				  '&exhibitTitle=' + encodeURIComponent( title ) +
				  '&exhibitSubTitle=' + encodeURIComponent( subTitle ) +
				  '&skipStamp=' + Multimedia.isSkipStamp +
				  '&overwrite=' + overwrite +
				  '&exhibitDate=' + encodeURIComponent( window.top.Datacore.s.exhibitDate ) +
				  '&isMultimedia=' + true;
		$.getJSON( url, null, function(data, textStatus, xhr) {
			hideSpinner();
			Multimedia.blockButtons = false;
			if (textStatus === 'success') {
				if (typeof data.error === 'undefined') {
					Multimedia.cleanEvents();
					// Find the new fileID
					var newFileID = parseInt( data.EDDeposition.introducedFile.ID );
					if (window.top.Datacore.hasFile( newFileID )) {
						window.top.Datacore.dropFile( newFileID );
					}
					window.top.Datacore.addFile( data.EDDeposition.introducedFile );
					//Multimedia.updateStamp();
					window.top.introduceRedirect(newFileID);
					if (overwrite) {
						window.top.showTooltip("The exhibit was successfully reintroduced!", true);
					} else {
						window.top.showTooltip("The exhibit was successfully introduced!", true);
					}
				} else {
					var errTitle = (typeof data.title !== 'undefined' && data.title !== null && data.title.length > 0) ? data.title : 'Error';
					var errText = (typeof data.message !== 'undefined' && data.message !== null && data.message.length > 0) ? data.message : 'Unable to introduce document.';
					window.top.showPlainDialog1Btn( errTitle, errText, 'Close' );
				}
			}
		} ).fail( function() {
			hideSpinner();
			Multimedia.blockButtons = false;
			console.log("Fail Introduce");
			window.top.showPlainDialog1Btn( 'Error', 'Unable to introduce document.', 'Close' );
		});
	},

	// Get the ellipse version of the text for the stamp.
	getText : function( obj ) {
		var savedText = obj.text();
		var curText = obj.text();
		if (obj[0].clientWidth >= obj[0].scrollWidth) {
			return curText;
		}
		curText += "...";
		while(obj[0].clientWidth < obj[0].scrollWidth) {
			curText = curText.slice(0, -4) + "...";
			obj.text( curText );
		}
		obj.text( savedText );
		return curText;
	}

};
