//NO NEED TO USE THIS FILE?
window.document.domain = "edepoze.com";
//window.document.domain = "armyargentina.com";
var Introduce = window.top.Introduce = {

	scrollDiv : null,
	mouseOffset : {x:0, y:0},
	lastMousePos : {x:0, y:0},
	stamp : null,
	canvasRect : {x1: 0, y1: 0, x2: 0, y2: 0},
	arrowOffset : 0,
	dragArrows : null,
	pendingEdit : false,
	firstShow : true,
	exhibitXOrigin : 0,
	exhibitYOrigin : 0,
	fileID : 0,
	exhibitFolderID : 0,
	isSkipStamp : false,
	firstExhibit : false,
	firstRenderDone : false,
	personalFolderID : null,
	sourceFileID : 0,
	filename : null,
	blockButtons : false,

	init : function() {
		Introduce.stamp = $('#stamp');
		Introduce.stamp.mousedown(Introduce.mdown).mouseup(Introduce.mup);
		Introduce.stamp[0].addEventListener("touchstart", Introduce.touchStart, false);
		Introduce.stamp[0].addEventListener("touchend", Introduce.touchEnd, false);
		Introduce.scrollDiv = $('#outerContainer');
		Introduce.dragArrows = $('#drag_arrows');
		window.top.Datacore.s.exhibitXOrigin = 0.778;
		window.top.Datacore.s.exhibitYOrigin = 0.91;
		Introduce.blockButtonActions ( true );

		// Check for first introduction.
		if (window.top.Datacore.s.exhibitTitle.length === 0 && window.top.Datacore.s.exhibitSubTitle.length === 0) {
			window.top.Datacore.s.exhibitTitle = window.top.Datacore.s.depositionOf;
			window.top.Datacore.s.exhibitSubTitle = 'Exhibit_0';
			Introduce.firstExhibit = true;
		}
	},

	cleanEvents : function() {
	},

	blockButtonActions : function(value) {
		Introduce.blockButtons = !( typeof value === 'undefined' || value === false );
	},

	backButton : function() {
		var dialogButtons = [];
		dialogButtons.push( {"label":"No", "id":"cancel", "class":"btn_app_cancel"} );
		dialogButtons.push( {"label":"Yes", "id":"abort", "action":window.top.dismissDialog3Btn, "class":"btn_app_normal"} );
		var confirmMsg = '<p>You may have changes to this document.<p><p>Are you sure you want to exit without saving?</p>';
		window.top.showDialog3Btn( 'Abort Introduction?', confirmMsg, dialogButtons, false, 'tight padded' );
		$('#dialog3Btn #cancel', window.top.document).off().on( "click", function() {
			window.top.dismissDialog3Btn();
			Introduce.cleanEvents();
		} );
		$('#dialog3Btn #abort', window.top.document).off().on( "click", function() {
			window.top.dismissDialog3Btn();
			if( typeof window.top.Datacore.s.tempFile !== 'undefined' && window.top.Datacore.s.tempFile !== null ) {
				Introduce.deleteTempFile();
			}
			var pathname = '/depositions';
			if( window.top.Datacore.s.inPresentation === true ) {
				pathname = '/depositions/presentation';
			}
			window.top.iPadContainerFrame.location.replace( pathname );
		} );
	},

	getSaveFilename : function() {
		var filename = parent.introduceFilename;
		var index = filename.lastIndexOf(".");
		filename = filename.substr(0, index);
		Introduce.sourceFileID = Introduce.fileID;
		if (Introduce.personalFolderID === null) {
			for(var f in window.top.dcFolders) {
				if (window.top.dcFolders[f].class === 'Personal') {
					Introduce.personalFolderID = window.top.dcFolders[f].ID;
					break;
				}
			}
		}
		window.top.setDialogSaveFileCloseAction(Introduce.validateSaveFilename);
		window.top.showDialogSaveFile("Name Your Copy", "Save", filename);
	},

	validateSaveFilename : function( filename, folderID ) {
		var folder;
		if (typeof folderID !== 'undefined' && folderID !== null) {
			folder = window.top.Datacore.getFolder( folderID );
		} else {
			folder = window.top.Datacore.getFolder( Inroduce.personalFolderID );
		}
		if (folder === null) {
			return;
		}
		var files = folder.getFiles(folder.getFileIDs());
		var duplicate = false;
		var duplicateFileID = 0;
		for(var f in files) {
			var shortName = files[f].name;
			var index = shortName.lastIndexOf(".");
			if (index >= 0) {
				shortName = shortName.substr(0, index);
			}
			if (shortName.toLowerCase() === filename.toLowerCase()) {
				duplicate = true;
				duplicateFileID = parseInt(files[f].ID, 10);
				break;
			}
		}
		if (duplicate) {
			//Confirm overwrite.
			var dialogButtons = [];
			dialogButtons.push( {label: 'Cancel', action : 'dismissDialog3Btn();', class : 'btn_app_cancel'} );
			dialogButtons.push( {label: 'Replace', id : 'replace', class : 'btn_app_normal'} );
			var confirmMsg = 'A document by this name already exists.  Are you sure you want to replace the existing document?';
			window.top.showDialog3Btn( 'Document Already Exists!', confirmMsg, dialogButtons, true );
			$('#dialog3Btn #replace', window.top.document).on("click", function() {
				window.top.dismissDialog3Btn();
				Introduce.fileID = duplicateFileID;
				Introduce.save(filename, true, folderID);
				window.top.navBar.setTitle( filename + '.pdf' );
			});
		} else {
			Introduce.save(filename, false, folderID);
			window.top.navBar.setTitle( filename + '.pdf' );
		}
	},

	save : function(filename, overwrite, folderID) {
		var text = '[]';
		var url = "annotate?sourceFileID=" + Introduce.sourceFileID +
				  "&filename=" + filename + ".pdf" +
				  "&overwrite=" + overwrite +
				  "&fileID=" + Introduce.fileID +
				  "&folderID=" + folderID +
				  '&isTempFile=' + ( typeof window.top.Datacore.s.tempFile !== 'undefined' );
		$.post(url, {annotations: text}, function(data, textStatus, xhr) {
			if (textStatus === "success" && data !== null) {
				window.top.showTooltip( 'File saved to server.', true );
				if (typeof window.top.Datacore.s.tempFile !== 'undefined') {
					Introduce.deleteTempFile();
				} else {
					Introduce.cleanEvents();
					window.top.introduceRedirect( 0, true );
				}
			} else {
				window.top.showPlainDialog1Btn( 'Error', 'Failed to save File.', 'Close' );
			}
		}, "json")
		.fail(function() {
			window.top.showPlainDialog1Btn( 'Error', 'Failed to save File.', 'Close' );
		});
	},

	deleteTempFile : function() {
		window.top.webApp.abortTempFile();
	},

	showStamp : function(title, subtitle) {
		// Get the input and show to the user.
		Introduce.stamp.show();
		$('#stamp #dialogTitle').text(title);
		$('#stamp #dialogSubtitle').text(subtitle);
		$('#stamp #date').text(window.top.Datacore.s.exhibitDate);
		Introduce.arrowOffset = -Introduce.dragArrows[0].offsetLeft;
		var canvas = $('#page-1')[0];
		Introduce.canvasRect = {x1: canvas.offsetLeft + Introduce.arrowOffset, y1: canvas.offsetTop, x2: canvas.offsetLeft + canvas.offsetWidth, y2: canvas.offsetTop + canvas.offsetHeight};
		if (Introduce.firstShow) {
			Introduce.firstShow = false;
			Introduce.stamp[0].style.left = Introduce.exhibitXOrigin * canvas.offsetWidth + 'px';
			Introduce.stamp[0].style.top = Introduce.exhibitYOrigin * canvas.offsetHeight + 'px';
			window.scrollBy(Introduce.exhibitXOrigin * canvas.offsetWidth, Introduce.exhibitYOrigin * canvas.offsetHeight);
		}

		var currPos = {x: parseInt(Introduce.stamp[0].style.left), y: parseInt(Introduce.stamp[0].style.top)};
		// Move the stamp to this position and re-layout and then do the out of bounds check.
		Introduce.stamp[0].style.left = Introduce.canvasRect.x1 + 'px';
		Introduce.stamp[0].style.top = Introduce.canvasRect.y1 + 'px';
		if (Introduce.outOfBounds(currPos.x, currPos.y)) {
			Introduce.clamp(currPos);
		}
		Introduce.stamp[0].style.left = currPos.x + 'px';
		Introduce.stamp[0].style.top = currPos.y + 'px';
	},

	finishEdit : function(title, subTitle) {
		Introduce.showStamp( title, subTitle );
		var canvas = $('#page-1')[0];
		Introduce.exhibitXOrigin = (Introduce.stamp[0].offsetLeft - canvas.offsetLeft) / canvas.offsetWidth;
		Introduce.exhibitYOrigin = (Introduce.stamp[0].offsetTop - canvas.offsetTop) / canvas.offsetHeight;
		console.log("finishEdit-exhibitXOrigin:"+Introduce.exhibitXOrigin+",exhibitYOrigin:"+Introduce.exhibitYOrigin);
	},

	touchStart : function(e) {
		e.preventDefault();
		var fakeEvent = { target: e.target, clientX: e.targetTouches[0].clientX, clientY: e.targetTouches[0].clientY };
		Introduce.mdown( fakeEvent );
	},

	touchMove : function(e) {
		e.preventDefault();
		var fakeEvent = { target: e.target, clientX: e.targetTouches[0].clientX, clientY: e.targetTouches[0].clientY };
		Introduce.mmove( fakeEvent );
	},

	touchEnd : function(e) {
		e.preventDefault();
		var fakeEvent = { target: e.target, clientX: 0, clientY: 0 };
		Introduce.mup( fakeEvent );
		Introduce.stamp[0].removeEventListener("touchmove", Introduce.touchMove, false);
	},

	mdown : function(e) {
		if (e.target !== Introduce.dragArrows[0]) {
			Introduce.pendingEdit = true;
			return;
		}
		$(document.body).css("overflow", "hidden");
		Introduce.stamp.mousemove(Introduce.mmove).mouseout(Introduce.mout);
		Introduce.stamp[0].addEventListener("touchmove", Introduce.touchMove, false);
		Introduce.scrollDiv.mousemove(Introduce.mmove).mouseout(Introduce.moutDocument);
		Introduce.scrollDiv[0].addEventListener("touchmove", Introduce.touchMove, false);
		Introduce.mouseOffset.x = e.clientX - Introduce.stamp[0].offsetLeft;
		Introduce.mouseOffset.y = e.clientY - Introduce.stamp[0].offsetTop;
		Introduce.lastMousePos = {x: e.clientX, y: e.clientY};
		var canvas = $('#page-1')[0];
		Introduce.canvasRect = {x1: canvas.offsetLeft + Introduce.arrowOffset, y1: canvas.offsetTop, x2: canvas.offsetLeft + canvas.offsetWidth, y2: canvas.offsetTop + canvas.offsetHeight};
//		console.log("mdown: "+ Introduce.mouseOffset.x+","+Introduce.mouseOffset.y );
	},

	mup : function(e) {
		if (Introduce.pendingEdit) {
			Introduce.pendingEdit = false;
			var offset = Introduce.stamp.offset();
			var p = $(window);
			var offsetParent = $('#doc_viewer', parent.document).offset();
			var offset2 = $('#ipad_container_frame', window.top.document).position();
			window.top.showDialogIntroduceInput($('#stamp #dialogTitle').text(), $('#stamp #dialogSubtitle').text(), $('#stamp #date').text(),
				( offset.left - p.scrollLeft() + offsetParent.left + offset2.left )+ 'px', ( offset.top - p.scrollTop() + offsetParent.top + offset2.top ) + 'px');
			window.top.setDialogIntroduceInputCloseAction(Introduce.finishEdit);
			return;
		}
		// Save percent offset.
		var canvas = $('#page-1')[0];
		Introduce.exhibitXOrigin = (Introduce.stamp[0].offsetLeft - canvas.offsetLeft) / canvas.offsetWidth;
		Introduce.exhibitYOrigin = (Introduce.stamp[0].offsetTop - canvas.offsetTop) / canvas.offsetHeight;
		console.log("mup-exhibitXOrigin:"+Introduce.exhibitXOrigin+",exhibitYOrigin:"+Introduce.exhibitYOrigin);
		$(document.body).css("overflow", "auto");
		Introduce.stamp.off('mousemove').off('mouseout');
		Introduce.scrollDiv.off('mousemove').off('mouseout');
//		console.log("mup");
	},

	mout : function(e) {
		var currX = e.clientX - Introduce.mouseOffset.x + Introduce.scrollDiv.scrollLeft();
		var currY = e.clientY - Introduce.mouseOffset.y + Introduce.scrollDiv.scrollTop();
//		console.log("mout: "+currX+","+currY);
		if (Introduce.outOfBounds(currX, currY)) {
			var mmove = Introduce.mmove.bind(this, e);
			mmove();
			Introduce.mup(e);
		} else if (e.toElement.nodeName === "HTML") {
			var mmove = Introduce.mmove.bind(this, e);
			mmove();
			Introduce.mup(e);
			return;
		}
	},

	moutDocument : function(e) {
		//See if we moved out of the document.
		var currX = e.clientX - Introduce.mouseOffset.x + Introduce.scrollDiv.scrollLeft();
		var currY = e.clientY - Introduce.mouseOffset.y + Introduce.scrollDiv.scrollTop();
//		console.log("moutDocument: "+currX+","+currY);
		if (Introduce.outOfBounds(currX, currY)) {
			var mmove = Introduce.mmove.bind(this, e);
			mmove();
			Introduce.mup(e);
			return;
		}
	},

	mmove : function(e) {
		if (e.clientX === Introduce.lastMousePos.x && e.clientY === Introduce.lastMousePos.y) {
			return;
		}
		var currPos = {x: 0, y: 0};
		currPos.x = e.clientX - Introduce.mouseOffset.x + Introduce.scrollDiv.scrollLeft();
		currPos.y = e.clientY - Introduce.mouseOffset.y + Introduce.scrollDiv.scrollTop();
		if (Introduce.outOfBounds(currPos.x, currPos.y)) {
			Introduce.clamp(currPos);
		}

		Introduce.stamp[0].style.left = currPos.x + 'px';
		Introduce.stamp[0].style.top = currPos.y + 'px';
		Introduce.lastMousePos = {x: e.clientX, y: e.clientY};
//		console.log("mmove: "+ currPos.x+","+currPos.y );
	},

	outOfBounds : function(x, y) {
		var stampRect = {x1: x, y1: y, x2: x + Introduce.stamp[0].offsetWidth, y2: y + Introduce.stamp[0].offsetHeight};
		//See if the stamp has gone out of bounds.
		if (stampRect.x1 < Introduce.canvasRect.x1 ||
			stampRect.x2 > Introduce.canvasRect.x2 ||
			stampRect.y1 < Introduce.canvasRect.y1 ||
			stampRect.y2 > Introduce.canvasRect.y2) {
			return true;
		}
		return false;
	},

	clamp : function(curPos) {
		var stampRect = {x1: curPos.x, y1: curPos.y, x2: curPos.x + Introduce.stamp[0].offsetWidth, y2: curPos.y + Introduce.stamp[0].offsetHeight};
		//See if the stamp has gone out of bounds and clamp to canvasRect.
		curPos.x = Math.max(stampRect.x1, Introduce.canvasRect.x1);
		curPos.y = Math.max(stampRect.y1, Introduce.canvasRect.y1);
		if (stampRect.x2 !== Math.min(stampRect.x2, Introduce.canvasRect.x2)) {
			curPos.x = Introduce.canvasRect.x2 - Introduce.stamp[0].offsetWidth;
		}
		if (stampRect.y2 !== Math.min(stampRect.y2, Introduce.canvasRect.y2)) {
			curPos.y = Introduce.canvasRect.y2 - Introduce.stamp[0].offsetHeight;
		}
	},

	renderStart : function() {
		Introduce.blockButtonActions( true );
		Introduce.stamp.hide();
	},

	renderDone : function() {
		(!Introduce.firstShow) ? Introduce.stamp.show() : Introduce.stamp.hide();
		var canvas = $('#page-1')[0];
		Introduce.stamp[0].style.left = ( Introduce.exhibitXOrigin * canvas.offsetWidth + canvas.offsetLeft ) + 'px';
		Introduce.stamp[0].style.top = ( Introduce.exhibitYOrigin * canvas.offsetHeight + canvas.offsetTop ) +'px';
		var currPos = {x: parseInt(Introduce.stamp[0].style.left), y: parseInt(Introduce.stamp[0].style.top)};
		Introduce.canvasRect = {x1: canvas.offsetLeft + Introduce.arrowOffset, y1: canvas.offsetTop, x2: canvas.offsetLeft + canvas.offsetWidth, y2: canvas.offsetTop + canvas.offsetHeight};

		// Move the stamp to this position and re-layout and then do the out of bounds check.
		Introduce.stamp[0].style.left = Introduce.canvasRect.x1 + 'px';
		Introduce.stamp[0].style.top = Introduce.canvasRect.y1 + 'px';
		if (Introduce.outOfBounds(currPos.x, currPos.y)) {
			Introduce.clamp(currPos);
		}
		Introduce.stamp[0].style.left = currPos.x + 'px';
		Introduce.stamp[0].style.top = currPos.y + 'px';
		if (!Introduce.firstRenderDone && !Introduce.firstShow) {
			Introduce.firstRenderDone = true;
			Introduce.exhibitXOrigin = (Introduce.stamp[0].offsetLeft - canvas.offsetLeft) / canvas.offsetWidth;
			Introduce.exhibitYOrigin = (Introduce.stamp[0].offsetTop - canvas.offsetTop) / canvas.offsetHeight;
		}

		var _x = Introduce.exhibitXOrigin * canvas.offsetWidth;
		var _y = Introduce.exhibitYOrigin * canvas.offsetHeight;
		window.scrollTo( _x, _y );
//		console.log("renderDone-exhibitXOrigin:"+Introduce.exhibitXOrigin+",exhibitYOrigin:"+Introduce.exhibitYOrigin, _x , _y );
		Introduce.blockButtonActions ( false );
	},

	skipStamp : function() {
		if (Introduce.blockButtons) {
			return;
		}
		var fileID = Introduce.fileID;
		var dcFile = window.top.Datacore.getFile( fileID );
		if (dcFile === null) {
			return;
		}

		var index = dcFile.name.lastIndexOf(".");
		var filename = dcFile.name.substr(0, index);
		for(var f in window.top.dcFolders) {
			if (window.top.dcFolders[f].class === 'Exhibit') {
				Introduce.exhibitFolderID = window.top.dcFolders[f].ID;
				break;
			}
		}
		Introduce.stamp.hide();
		window.top.setDialogTextInputCancelAction(Introduce.unHideStamp);
		window.top.setDialogTextInputCloseAction(Introduce.validateFilename);
		window.top.showDialogTextInput("Rename the exhibit", "Save", filename);
		Introduce.isSkipStamp = true;
	},

	unHideStamp : function() {
		Introduce.stamp.show();
	},

	introduceFile: function() {
		if( Introduce.blockButtons ) {
			return;
		}
		var exTitle = $('#stamp #dialogTitle').text().trim();
		var exSubtitle = $('#stamp #dialogSubtitle').text().trim();
		var filename = [exTitle, exSubtitle].join( '_' );
		filename = filename.replace( /\s+/g, '_' );
		Introduce.isSkipStamp = false;
		Introduce.validateFilename( filename );
	},

	validateFilename : function( filename ) {
//		console.log( 'Introduce.validateFilename:', filename );
		// Sanitize the filename.  Only allow alphanumerics and underscore, space, hyphen, period.
		// Remove everything else.
		filename = filename.replace( /[^\w\040\-\.]+/g, '' );
		var folder = window.top.Datacore.getFolder( Introduce.exhibitFolderID );
		if( typeof folder === 'undefined' || folder === null ) {
			console.error( 'missing exhibit folder' );
			return;
		}
		var files = folder.getFiles();

		var inConflictID = null;
		for( var f in files ) {
			var shortName;
			var _name = files[f].name;
			var index = _name.lastIndexOf( "." );
			if( index > 0 ) {
				shortName = _name.substr( 0, index );
				if( shortName.toLowerCase() === filename.toLowerCase() ) {
					inConflictID = files[f].ID;
					break;
				}
			}
		}
		filename += ".pdf";
		Introduce.blockButtonActions ( true );
		if( inConflictID !== null ) {
			//Confirm overwrite.
			var dialogButtons = [];
			dialogButtons.push( {label: 'No', id : 'no', class : 'btn_app_cancel'} );
			dialogButtons.push( {label: 'Yes', id : 'replace', class : 'btn_app_normal'} );
			var confirmMsg = 'An Official Exhibit by this name already exists.  Do you want to replace it?';
			console.log("Confirm overwrite");
			window.top.showDialog3Btn( 'File Already Exists!', confirmMsg, dialogButtons, true );
			$('#dialog3Btn #no', window.top.document).on("click", function() {
				window.top.dismissDialog3Btn();
				Introduce.blockButtonActions ( false );
			});
			$('#dialog3Btn #replace', window.top.document).on("click", function() {
				window.top.dismissDialog3Btn();
				Introduce.introduceFileSubmit( filename, Introduce.fileID, inConflictID, true );
			});

		} else {
			Introduce.introduceFileSubmit( filename, Introduce.fileID, Introduce.fileID, false );
		}
	},

	introduceFileSubmit : function(filename, sourceID, fileID, overwrite) {
		window.top.showLoadingOverlay('Introducing...');
		var title;
		var subTitle;
		if (Introduce.isSkipStamp) {
			title = window.top.Datacore.s.oldExhibitText.title;
			subTitle = window.top.Datacore.s.oldExhibitText.subTitle;
		} else {
			title = Introduce.getText( $('#stamp #dialogTitle') );
			subTitle = Introduce.getText( $('#stamp #dialogSubtitle') );
		}
		var url = 'introduceFile?filename=' + encodeURIComponent( filename ) +
				  '&fileID=' + fileID +
				  '&sourceFileID=' + sourceID +
				  '&exhibitXOrigin=' + Introduce.exhibitXOrigin +
				  '&exhibitYOrigin=' + Introduce.exhibitYOrigin +
				  '&exhibitTitle=' + encodeURIComponent( title ) +
				  '&exhibitSubTitle=' + encodeURIComponent( subTitle ) +
				  '&skipStamp=' + Introduce.isSkipStamp +
				  '&overwrite=' + overwrite +
				  '&exhibitDate=' + encodeURIComponent( window.top.Datacore.s.exhibitDate );
		$.getJSON( url, null, function(data, textStatus, xhr) {
			hideSpinner();
			Introduce.blockButtonActions( false );
			if (textStatus === 'success') {
				if (typeof data.error === 'undefined') {
					Introduce.cleanEvents();
					// Find the new fileID
					var newFileID = parseInt( data.EDDeposition.introducedFile.ID );
					if (window.top.Datacore.hasFile( newFileID )) {
						window.top.Datacore.dropFile( newFileID );
					}
					window.top.Datacore.addFile( data.EDDeposition.introducedFile );
					Introduce.updateStamp();
					if (overwrite) {
						window.top.showTooltip("The exhibit was successfully reintroduced!", true);
					} else {
						window.top.showTooltip("The exhibit was successfully introduced!", true);
					}
					var pData = window.top.webApp.getS( 'joinPresentationData' );
					if( typeof pData !== 'undefined' && pData !== null ) {
						window.top.setPresentationFile( window.top.webApp.getS( 'inDeposition' ), data.EDDeposition.introducedFile.ID );
						window.top.iPadContainerFrame.location.replace( '/depositions/presentation' );
						return;
					}
					window.top.introduceRedirect(newFileID);
				} else {
					var errTitle = (typeof data.title !== 'undefined' && data.title !== null && data.title.length > 0) ? data.title : 'Error';
					var errText = (typeof data.message !== 'undefined' && data.message !== null && data.message.length > 0) ? data.message : 'Unable to introduce document.';
					window.top.showPlainDialog1Btn( errTitle, errText, 'Close' );
				}
			}
		} ).fail( function() {
			hideSpinner();
			Introduce.blockButtonActions ( false );
			console.log("Fail Introduce");
			window.top.showPlainDialog1Btn( 'Error', 'Unable to introduce document.', 'Close' );
		});
	},

	// Get the ellipse version of the text for the stamp.
	getText : function( obj ) {
		var curText = obj.text();
		curText = curText.slice( 0, 120 );	//max filename length
		obj.text( curText );
		return curText;
	},

	splitExhibitString : function ( obj ) {
		var index = obj.text.search( /[0-9]+$/ );
		if (index >= 0) {
			var newCount = obj.text.substr(index);
			obj.baseText = obj.text.substr( 0, obj.text.length - newCount.length );
			obj.number = parseInt( newCount, 10 );
		} else {
			obj.baseText = obj.text;
			obj.number = 0;
		}
	},

	updateStamp : function() {
		var newTitleText = $('#stamp #dialogTitle').text();
		var newSubTitleText = $('#stamp #dialogSubtitle').text();
		var newInt = 0;
		var oldTitleText = window.top.Datacore.s.exhibitTitle;
		var oldSubTitleText = window.top.Datacore.s.exhibitSubTitle;
		var oldInt = 0;
		var obj;

		// Split out the number from the sub-title or from the title.
		if( $('#stamp #dialogSubtitle').text().length > 0) {
			obj = {text: newSubTitleText, baseText: newSubTitleText, number: 0};
			Introduce.splitExhibitString( obj );
			newSubTitleText = obj.baseText;
			newInt = obj.number;

			obj = {text: oldSubTitleText, baseText: oldSubTitleText, number: 0};
			Introduce.splitExhibitString( obj );
			oldSubTitleText = obj.baseText;
			oldInt = obj.number;
		} else {
			obj = {text: newTitleText, baseText: newTitleText, number: 0};
			Introduce.splitExhibitString( obj );
			newTitleText = obj.baseText;
			newInt = obj.number;

			obj = {text: oldTitleText, baseText: oldTitleText, number: 0};
			Introduce.splitExhibitString( obj );
			oldTitleText = obj.baseText;
			oldInt = obj.number;
		}
		console.log( 'StampTitle: ' + $('#stamp #dialogTitle').text() + ' StampSubTitle: ' + $('#stamp #dialogSubtitle').text() + ' Title: ' + window.top.Datacore.s.exhibitTitle +
			' SubTitle: ' + window.top.Datacore.s.exhibitSubTitle );
		console.log( 'newTitleText: ' + newTitleText + ' oldTitleText: ' + oldTitleText + ' newSubTitleText: ' + newSubTitleText +
			' oldSubTitleText: ' + oldSubTitleText + ' newInt: ' + newInt + ' oldInt: ' + oldInt);
		//If the titles are the same and the new number less than or equal to the old number, we don't update the meta data because the user did an overwrite and we
		//leave the counter at it's old value.
		if( oldTitleText === newTitleText && oldSubTitleText === newSubTitleText && newInt <= oldInt ) {
			console.log(' The meta data is a lower number, so do not save.');
			return;
		}

		window.top.Datacore.s.exhibitTitle = $('#stamp #dialogTitle').text();
		window.top.Datacore.s.exhibitSubTitle = $('#stamp #dialogSubtitle').text();
	},

	updateSpeaker : function() {
		if (window.top.Datacore.s.speakerID !== window.top.Datacore.s.currentUserID) {
			window.top.clearDialogIntroduceInputCloseAction();
			window.top.dismissDialogIntroduceInput();
			window.top.showPlainDialog1Btn( 'Leader Role was Passed', 'This exhibit can no longer be introduced', 'Close' );
			Introduce.cleanEvents();
			window.top.introduceRedirect(0, true);
		}
	},

	depositionEnded : function() {
		window.top.clearDialogIntroduceInputCloseAction();
		window.top.dismissDialogIntroduceInput();
		Introduce.cleanEvents();
		window.top.introduceRedirect(0, true);
	}
};

$(document).ready( function() {
	Introduce.init();
	window.onbeforeunload = function()
	{
		delete window.top.Introduce;
	};
	window.top.Datacore.syncFolders( null, function() {
		var folders = window.top.Datacore.getFolders();
		for( var f in folders ) {
			if( folders[f].class === 'Exhibit' ) {
				Introduce.exhibitFolderID = folders[f].ID;
				break;
			}
		}
	} );
});
