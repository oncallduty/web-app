function loginSuccess( data, textStatus )
{
	console.log( 'loginSuccess(' + textStatus + ')' );
	$("#loading").hide();
	try {
		var response = jQuery.parseJSON( data );
	} catch( err ) {
		displayError( err.message );
	}

	if( typeof( response ) === 'undefined' ) {
		//oops!
		displayError( null, 'Invalid response data' );
		return;
	}

	if (response.hasOwnProperty( 'success' )) {
		if (response.success !== true) {
			if (response.hasOwnProperty( 'error' )) {
				if (response.hasOwnProperty( 'code' )) {
					var title;
					switch (parseInt( response.code )) {
						case 0:
						case 1001:
							title = 'Incorrect Session Credentials';
							break;
						case 401:
							title = 'Invalid User Credentials';
							break;
						case 403:
							title = 'Invalid User Credentials';
							break;
						case 418:
							title = 'Security Update Notice';
							break;
						default:
							title = 'Access Denied';
					}
				}
				displayError( response.error, null, title );
			}
			return;
		} else {
			console.log( '-- success! Okay to go.' );
		}
	}

	if (response.hasOwnProperty( 'needToLinkDepo' ) && response.needToLinkDepo
		&& response.hasOwnProperty( 'deposition' ) && response.hasOwnProperty( 'deposition' )) {
		console.log('-- NeedToLinkDepo');
		var dialogButtons = [];
		if (response.casesLength > 0) {
			dialogButtons.push( {label: 'Browse files', class: 'app_bttn_new', action : 'depoLinkBrowse();'} );
		} else {
			dialogButtons.push( {label: 'Browse files', inactive : true} );
		}
		dialogButtons.push( {label: 'Cancel', action : 'depoLinkCancel();'} );
		dialogButtons.push({label: 'Create New', class: 'app_bttn_new', action: 'depoLinkNew();'});
		var depoLinkInstructions = 'If you already set up a case and/or session for this witness in your eDepoze account, select &quot;Browse Files&quot; and choose your existing case or session.'
				+ '<br /><br />Or Select &quot;Create New&quot; to create a new case and session in your eDepoze account.';
		var depoType = (response.deposition.class === 'WitnessPrep' || response.deposition.class === 'WPDemo') ? 'Witness Prep' : 'Deposition';
		showDialog3Btn( 'Welcome to the '+response.deposition.depositionOf+' '+depoType+'.', depoLinkInstructions, dialogButtons );
		return;
	}

	if( response.hasOwnProperty( 'redirect' ) ) {
		console.log( '-- redirecting to: ' + response.redirect );
		window.location.assign( response.redirect );	//TODO: location.replace
		return;
	}

	console.log( '-- should not be here, primary conditions were not met' );
}

function loginError( data, textStatus )
{
	console.log( 'loginError(' + textStatus + ')' );
	$("#loading").hide();
	var response = jQuery.parseJSON( data );
	console.log( response );
}

function depoLinkBrowse()
{
	$('#dialog3Btn').fadeOut( 200 );

	console.log( '-- redirecting to Linked Deposition : Case Selection');
	window.location.assign( '/cases/browse' );
}

function depoLinkCancel()
{
	$('#dialog3Btn').fadeOut( 200 );
	window.location = '/users/logout?redirect=/members&ul=1';
}

function depoLinkNew()
{
	$('#dialog3Btn').fadeOut( 200 );

	$("#loading").fadeIn( 200 );

	console.log( '-- redirecting to Linked Deposition : New Case');
	$.ajax( '/cases/browse?cID=0&dID=0' ).done( function( data, status ) {
		$("#loading").fadeOut( 200 );

		try {
			var response = jQuery.parseJSON( data );
		} catch( err ) {
			displayError( err.message );
		}

		var redirect = null;

		if (typeof( response ) === 'undefined' || response === null) {
			displayError( null, 'Invalid response data' );
			redirect = '/users/logout?ul=2';
		}

		if (response.hasOwnProperty( 'redirect' )) {
			console.log( '-- redirecting to: ' + response.redirect );
			redirect = response.redirect;
		}

		if (redirect) {
			window.location.assign( redirect );
			return;
		}

		if( response.hasOwnProperty( 'success' ) ) {
			if( response.success !== true ) {
				if( response.hasOwnProperty( 'error' ) ) {
					displayError( response.error );
				}
				return;
			} else {
				console.log( '-- success! Okay to go.' );
			}
		}
	} );
}

function resetSuccess( data, textStatus )
{
	console.log( 'resetSuccess(' + textStatus + ')' );

	$("#loading").hide();

	try {
		var response = jQuery.parseJSON( data );
	} catch( err ) {
		displayError( err.message );
	}

	if (typeof( response ) === 'undefined') {
		//oops!
		displayError( null, 'Invalid response data' );
		return;
	}

	if (response.hasOwnProperty( 'success' )) {
		if (!response.success) {
			if (response.hasOwnProperty( 'error' )) {
				displayError( response.error );
			}
			return;
		}
		console.log( '-- success! Okay to go.' );
	}

	if (response.hasOwnProperty( 'tooltip' )) {
		console.log( '-- show tooltip: ' + response.tooltip );
		window.top.showTooltip( response.tooltip );
		delete response.tooltip;
		setTimeout(function() {
			resetSuccess(JSON.stringify(response), textStatus);
		}, 2000);
		return;
	}

	if (response.hasOwnProperty( 'redirect' )) {
		console.log( '-- redirecting to: ' + response.redirect );
		window.location.assign( response.redirect );
		return;
	}

	console.log( '-- should not be here, primary conditions were not met' );
}

function resetError( data, textStatus )
{
	console.log( 'resetError(' + textStatus + ')' );
	$("#loading").hide();
	var response = jQuery.parseJSON( data );
	console.log( response );
}

function displayError( error_msg, log_msg, error_title ) {
	error_msg = (typeof error_msg !== 'undefined' && error_msg !== null ? error_msg : '');
	log_msg = (typeof log_msg !== 'undefined' && log_msg !== null ? log_msg : '');
	error_title = (typeof error_title !== 'undefined' && error_title !== null ? error_title : 'Access Denied');

	console.log( '-- [ERROR] '+(log_msg.length > 0 ? log_msg : (error_msg.length > 0 ? error_msg : 'Undefined error.')) );

	if (error_msg.length > 0) {
		if (typeof( showDialog1Btn ) === 'function') {
			setDialog1BtnCloseAction( function() {
				if ($('#EDUserUsername').length > 0) {
					$('#EDUserUsername').focus();
				} else if ($('#EDUserDepositionID').length > 0) {
					$('#EDUserDepositionID').focus();
				}
			} );
			showDialog1Btn(error_title , error_msg, 'OK' );
		} else {
			alert( error_msg );
		}
	}
}

function witnessLoginSuccess( data, textStatus )
{
//	console.log( 'witnessLoginSuccess(' + textStatus + ')' );
	$("#loading").hide();
	try {
		var response = jQuery.parseJSON( data );
	} catch( err ) {
		displayError( err.message );
	}

	if( typeof( response ) === 'undefined' ) {
		displayError( null, 'Invalid response data' );
		return;
	}

	if( response.hasOwnProperty( 'success' ) ) {
		if( response.success !== true ) {
			if( response.hasOwnProperty( 'error' ) ) {
				if( response.hasOwnProperty( 'code' ) ) {
					switch( parseInt( response.code ) ) {
						case 0:
						case 1001:
							var title = 'Incorrect Session ID';
							break;
						case 1003:
							$("#loginClass").val( "Witness Prep" );
							$('#witnessPrepLoginDialog').show();
							$('#wpPasscode').val('').focus();
							return;
						case 1005:
							$("#loginClass").val( "Trial Binder" );
							$('#trialBinderLoginDialog').show();
							$('#tbPasscode').val('').focus();
							return;
						default:
							title = 'Access Denied';
					}
				}
				displayError( response.error, null, title );
			}
			return;
		} else {
			console.log( '-- success! Okay to go.', response );
			Datacore.s.witnessDepositionID = response.depositionID;
			Datacore.s.sKey = response.sKey;
			Datacore.s.currentUserID = response.witnessID;
			if( typeof connectWithSKey === 'function' && response.userType === "WM" ) {
				sio_callback = witness_authorization;
				if( response.hasOwnProperty( 'userType' ) ) {
					window.location.replace( '/i' );
					return;
				}
				connectWithSKey();
				return;
			} else if( response.userType === "W" ) {
				window.location.replace( '/i' );
			} else {
				sio_callback = witness_authorization;
				connectWithSKey();
			}
		}
	}

}
