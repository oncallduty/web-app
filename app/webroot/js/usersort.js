/**
 * User Sort
 * @param {String} type Cases|Depositions|Folders|Files
 * @param {jQuery} headerElement
 * @param {Function} sortCallback
 * @param {jQuery} listController
 * @returns {UserSort}
 */
function UserSort( type, headerElement, sortCallback, listController )
{
//	console.log( 'UserSort', type, 'init' );
	var bind = this;
	var top = window.top;
	var memberTypeOptions = {
		"Cases":["Name","Case Number"],
		"Depositions":["Name","Type","Date"],
		"Folders":["Name","Date","Custom"],
		"Files":["Name","Date","Custom"]
	};
	var nonmemberTypeOptions = {
		"Folders":["Name"],
		"Files":["Name","Date"]
	};
	var sortFields = {
		"Cases":{"Name":"name", "Case Number":"number"},
		"Depositions":{"Name":"depositionOf", "Type":"class", "Date":"date"},
		"Folders":{"Name":"name", "Date":"created", "Custom":"sortPos"},
		"Files":{"Name":"name", "Date":"created", "Custom":"sortPos"}
	};
	var depoFilterOptions = ["All", "Scheduled", "Completed"];

	var showMenu = function( items )
	{
		var lineItemHTML = '<li><div class="check"></div></li>';
		var liLabelHTML = '<div class="label"></div>';
		var subMenuHTML = '<div class="submenu"></div>';
		var customEditHTML = '<div class="custom"></div>';
		menuList.empty();
		for( var i in items ) {
			var item = items[i];
			var li = $( lineItemHTML );
			var liLabel = $( liLabelHTML );
			var itemLabel = item;
			var isBranch = false;
			if( typeof item === 'object' ) {
				itemLabel = Object.keys( item )[0];
				isBranch = true;
			}
			liLabel.attr( 'data-sortby', itemLabel );
			liLabel.text( itemLabel );
			if( !isBranch ) {
				liLabel.off().on( 'click', {"sortBy":item}, function( event ) { selectOption( event.data.sortBy ); } );
			} else {
				liLabel.off().on( 'click', {"items":item[itemLabel]}, function( event ) { showMenu( event.data.items ); } );
			}
			li.append( liLabel );
			if( isBranch ) {
				li.append( subMenuHTML );
			}
			if( item === 'Custom' ) {
				var customEdit = $( customEditHTML );
				customEdit.off().on( 'click', {"sortObject":sortObject, "sortBy":item}, function( event ) {
//					console.log( event.data.sortObject );
					selectOption( event.data.sortBy );
					if( typeof listController !== 'undefined' && listController !== null && typeof listController.startCustomSort === 'function' ) {
						listController.startCustomSort( event.data.sortObject );
					}
					var doneBtn = $('.doneButton', header);
					doneBtn.off();
					doneBtn.on( 'click', {"sortObject":sortObject}, function( event ) {
						if( typeof listController !== 'undefined' && listController !== null && typeof listController.endCustomSort === 'function' ) {
							listController.saveCustomSort( event.data );
							listController.endCustomSort( event.data.sortObject );
						}
						doneBtn.hide();
					} );
					doneBtn.on( 'abortCustomSort', {"sortObject":sortObject}, function( event ) {
//						console.log( 'event::abortCustomEdit', event.data );
						doneBtn.hide();
					} );
					doneBtn.show();
				} );
				li.append( customEdit );
			}
			if( sortBy === item || sortBy.indexOf( itemLabel ) === 0 ) {
				$('.check', li).addClass( 'selected' );
			}
			menuList.append( li );
		}
		window.top.userSortMenu.css( 'visibility', 'hidden' );
		window.top.userSortMenu.show();
		var topPos;
		var leftPos;
		var tRect = sortKey[0].getBoundingClientRect();
		var mHeight = menuWrap.height();
		if( !$.contains( window.top.iPadContainer[0], sortKey[0] ) ) {
			menuWrap.css( 'position', 'absolute' );
			var navBar = window.top.webApp.getS( 'NavBar' );
			var nRect = navBar[0].getBoundingClientRect();
			topPos = Math.round( (nRect.height + tRect.top + (tRect.height / 2)) - (mHeight / 2) ) + 'px';
			leftPos = Math.round( tRect.left + tRect.width ) + 'px';
		} else {
			menuWrap.css( 'position', 'fixed' );
			topPos = Math.round( tRect.top + (tRect.height /2) - (mHeight / 2) ) + 'px';
			leftPos = Math.round( tRect.left + tRect.width ) + 'px';
		}
		menuWrap.css( 'left', leftPos );
		menuWrap.css( 'top', topPos );
		window.top.userSortMenu.hide();
		window.top.userSortMenu.css( 'visibility', '' );	//remove css property
		window.top.userSortMenu.show();
	};

	var showDepoFilterMenu = function()
	{
		var lineItemHTML = '<li><div class="check"></div></li>';
		var liLabelHTML = '<div class="label"></div>';
		dfMenuList.empty();
		for( var i in depoFilterOptions ) {
			var item = depoFilterOptions[i];
			var li = $( lineItemHTML );
			var liLabel = $( liLabelHTML );
			var itemLabel = item;
			liLabel.attr( 'data-filter', itemLabel );
			liLabel.text( item );
			liLabel.off().on( 'click', {"filter":item}, function( event ) { depoFilterSelectOption( event.data.filter ); } );
			li.append( liLabel );
			if( depoFilter === i ) {
				$('.check', li).addClass( 'selected' );
			}
			dfMenuList.append( li );
		}
		window.top.depoFilterMenu.show();
	};

	var selectOption = function( option )
	{
//		console.log( 'selectOption', option );
		var isValid = false;
		if( options.indexOf( option ) >= 0 ) {
			isValid = true;
		} else {
			for( var o in options ) {
				if( typeof options[o] !== 'object' ) {
					continue;
				}
				var first = Object.keys( options[o] )[0];
				if( options[o][first].indexOf( option ) >= 0 ) {
					isValid = true;
					break;
				}
			}
		}
		if( isValid ) {
			if( typeof listController !== 'undefined' && listController !== null && typeof listController.getInCustomSort === 'function' && listController.getInCustomSort() ) {
				$('.doneButton', header).trigger( 'click' );
			}
			sortBy = option;
			sortKey.text( sortBy );
			sortKey.attr( 'title', 'Sort By: ' + sortBy );
			if( sortBy === 'Custom' ) {
				sortOrder = 'Asc';
				sortArrow.addClass( 'up' );
				sortArrow.hide();
			} else {
				sortArrow.show();
			}
		}
		if( sortCallback && typeof sortCallback === 'function' ) {
			sortCallback();
		}
		window.top.userSortMenu.hide();
		storeSortPrefs();
		saveUserSortPreferences();
	};

	var depoFilterSelectOption = function( option )
	{
		console.log( 'depoFilterSelectOption', option );
		if( depoFilterOptions.indexOf( option ) >= 0 ) {
			depoFilter = depoFilterOptions.indexOf( option ) + "";
			var df = depoFilterOptions[depoFilter];
			depoFilterLabel.html( df );
			window.top.casesView.selectDepoFilter( df );
			saveUserPreferences();
		}
		window.top.depoFilterMenu.hide();
	};

	var saveUserSortPreferences = function()
	{
		var userType = window.top.webApp.getS( 'user.userType' );
		if( userType !== 'M' ) {
			return;
		}
		$.getJSON( "/users/setSortPreference", {"sortObject":sortObject, "sortBy":sortBy, "sortOrder":sortOrder}, function( data, textStatus ) {
			if( textStatus === "success" ) {
				if( data !== null && typeof data.EDUser.sortPreferences !== 'undefined' ) {
					window.top.webApp.setS( 'sortPreferences', data.EDUser.sortPreferences );
				}
			}
		} );
	};

	var saveUserPreferences = function()
	{
		var userType = window.top.webApp.getS( 'user.userType' );
		if( userType !== 'M' ) {
			return;
		}
		$.getJSON( "/users/setUserPreference", {"prefKey":"DepositionsFilter", "value":depoFilter}, function( data, textStatus ) {
			if( textStatus === "success" ) {
				if( data !== null && typeof data.EDUser.userPreferences !== 'undefined' ) {
					window.top.webApp.setS( 'userPreferences', data.EDUser.userPreferences );
				}
			}
		} );
	};

	var _sort = function( a, b ) {
		if( typeof a === 'undefined' || typeof b === 'undefined' || a === null || b === null ) {
//			console.log( a, b );
			return 0;
		}
		var sortField = 'ID';
		var aField;
		var bField;
		if( typeof sortFields[sortObject][sortBy] !== 'undefined' ) {
			sortField = sortFields[sortObject][sortBy];
		}
		if( sortField === 'class' ) {
			sortField = 'depositionOf';
		}
		aField = a[sortField];
		bField = b[sortField];
		if( sortField === 'date' ) {
			if( a.finished !== null ) {
				aField = a.finished;
			} else if( a.started !== null ) {
				aField = a.started;
			} else {
				aField = a.openDateTime;
			}

			if( b.finished !== null ) {
				bField = b.finished;
			} else if( b.started !== null ) {
				bField = b.started;
			} else {
				bField = b.openDateTime;
			}
		}
		if( aField > bField ) {
			return 1;
		} else if( aField < bField ) {
			return -1;
		} else {
			if( a.ID > b.ID ) {
				return 1;
			} else if( a.ID < b.ID ) {
				return -1;
			}
			return 0;
		}
	};

	this.sort = function( source )
	{
		if( typeof source !== 'object' || source === null ) {
			return;
		}
		var sortField = 'ID';
		if( typeof sortFields[sortObject][sortBy] !== 'undefined' ) {
			sortField = sortFields[sortObject][sortBy];
		}
		if( sortField === 'class' ) {
			sortField = 'depositionOf';
		}

		var result = (sortField !== 'date' && sortField !== 'created') ? naturalSort(source, sortField) : source.sort( _sort );
		if( sortOrder === 'Desc' ) {
			return result.reverse();
		}
		return result;
	};

	this.getSortByField = function()
	{
		return sortFields[sortObject][sortBy];
	};

	this.getSortOrder = function()
	{
		return sortOrder;
	};

	var userType = window.top.webApp.getS( 'user.userType' );
	var typeOptions = (userType === 'M') ? memberTypeOptions : nonmemberTypeOptions;

	if( typeof typeOptions[type] === 'undefined' ) {
		return null;
	}
	if( !headerElement || typeof headerElement !== 'object' || !headerElement.jquery ) {
		return null;
	}
	if( typeof window.top.iPadContainer === 'undefined' ) {
		return null;
	}
	var sortObject = type;
	var options = typeOptions[sortObject];
	var sortBy = options[0];
	var sortOrder = 'Asc';
	var header = headerElement;
	var depoFilter = "0";
	var depoFilterCtrl;
	var depoFilterLabel;

	var userType = window.top.webApp.getS( 'user.userType' );
	if( userType === 'W' ) {
		sortBy = 'Date';
		sortOrder = 'Desc';
	} else if( userType === 'WM' ) {
		sortBy = 'Name';
		sortOrder = 'Asc';
	}

	header.html( '<div class="sortKey"></div><div class="doneButton">Done</div><div class="sortArrow up"></div>' );

	if( typeof window.top.userSortMenu === 'undefined' ) {
		var t = $('.userSortMenu', window.top.document);
		if( t.length > 0 ) {
			window.top.userSortMenu = t;
		} else {
			window.top.iPadContainer.append( '<div class="userSortMenu" style="display:none;"><div class="shadow"></div><div class="sortMenuWrap"><div class="marker"></div><ul></ul></div></div>' );
			window.top.userSortMenu = $('.userSortMenu', window.top.document);
		}
	}

	var sortKey = $('.sortKey', header);
	sortKey.off().on( 'click', function() { showMenu( options ); } );

	var sortArrow = $('.sortArrow', header);
	sortArrow.off().on( 'click', function() {
		sortOrder = (sortOrder === 'Asc') ? 'Desc' : 'Asc';
		if( sortOrder === 'Desc' ) {
			sortArrow.removeClass( 'up' );
		} else {
			sortArrow.addClass( 'up' );
		}
		if( sortCallback && typeof sortCallback === 'function' ) {
			sortCallback();
		}
		storeSortPrefs();
		saveUserSortPreferences();
	} );

	var menuList = $('ul', window.top.userSortMenu);
	var menuWrap = $('.sortMenuWrap', window.top.userSortMenu);
	$('.shadow', window.top.userSortMenu).off().on( 'click', function() { window.top.userSortMenu.hide(); } );
	var dfMenuList;

	if( window.top.webApp.hasItem( 'sortPreferences.' + sortObject ) ) {
		//session sortPreferences
		var sortPrefValue = window.top.webApp.getItem( 'sortPreferences.' + sortObject );
		try {
			var sortPrefObj = JSON.parse( sortPrefValue );
			if( typeof sortPrefObj !== 'undefined' && sortPrefObj !== null ) {
				if( sortPrefObj.hasOwnProperty( 'sortBy' ) ) {
					if( options.indexOf( sortPrefObj.sortBy ) >= 0 ) {
						sortBy = sortPrefObj.sortBy;
					}
				}
				if( sortPrefObj.hasOwnProperty( 'sortOrder' ) ) {
					if( ['Asc','Desc'].indexOf( sortPrefObj.sortOrder ) >= 0 ) {
						sortOrder = sortPrefObj.sortOrder;
					}
				}
			}
		} catch( e ) {
			console.warn( 'unable to parse sort preferences for:', sortObject, e, sortPrefValue );
		}
	}
	var sortPrefs = window.top.webApp.getS( 'sortPreferences' );
	if( typeof sortPrefs !== 'undefined' && sortPrefs !== null ) {
		for( var i in sortPrefs ) {
			var sortPref = sortPrefs[i];
			if( sortPref.sortObject === sortObject ) {
				sortBy = sortPref.sortBy;
				sortOrder = sortPref.sortOrder;
				break;
			}
		}
	}
	delete sortPrefs;

	if( sortObject === 'Depositions' ) {
		header.append( '<div class="depoFilter"><div class="dfLabel"></div></div>' );
		if( typeof window.top.depoFilterMenu === 'undefined' ) {
			var t = $('.depoFilterMenu', window.top.document);
			if( t.length > 0 ) {
				window.top.depoFilterMenu = t;
			} else {
				window.top.iPadContainer.append( '<div class="depoFilterMenu" style="display:none;"><div class="shadow"></div><div class="depoFilterMenuWrap"><ul></ul><div class="marker"></div></div></div>' );
				window.top.depoFilterMenu = $('.depoFilterMenu', window.top.document);
			}
		}
		depoFilterCtrl = $('.depoFilter', header);
		depoFilterLabel = $('.dfLabel', depoFilterCtrl);
		depoFilterLabel.append( depoFilterOptions[depoFilter] );
		dfMenuList = $('ul', window.top.depoFilterMenu);
		$('.shadow', window.top.depoFilterMenu).off().on( 'click', function() { window.top.depoFilterMenu.hide(); } );
		var userPrefs = window.top.webApp.getS( 'userPreferences' );
		if( typeof userPrefs !== 'undefined' && userPrefs !== null ) {
			for( var i in userPrefs ) {
				var userPref = userPrefs[i];
				if( userPref.hasOwnProperty( 'prefKey' ) ) {
					if( userPref.prefKey === 'DepositionsFilter' ) {
						var depoFilterIdx = userPref.value;
						depoFilter = ((typeof depoFilterOptions[depoFilterIdx] !== 'undefined') ? depoFilterIdx : "0") + "";
						var df = depoFilterOptions[depoFilter];
						depoFilterLabel.html( df );
					}
				}
			}
		}
		delete userPrefs;
		depoFilterCtrl.off().on( 'click', function() { showDepoFilterMenu(); } );
	}

	sortKey.text( sortBy );
	sortKey.attr( 'title', 'Sort By: ' + sortBy );
	if( sortOrder === 'Desc' ) {
		sortArrow.removeClass( 'up' );
	} else {
		sortArrow.addClass( 'up' );
	}
	if( sortBy === 'Custom' ) {
		sortArrow.hide();
	}

	function storeSortPrefs()
	{
//		console.log( 'storeSortPrefs', sortObject, sortBy, sortOrder );
		var sortPref = {"sortBy":sortBy, "sortOrder":sortOrder};
		window.top.webApp.setItem( 'sortPreferences.' + sortObject, JSON.stringify( sortPref ) );
	};

	storeSortPrefs();

	function naturalSort(list, sortField) {
		var a, b, a1, b1, rx=/(\d+)|(\D+)/g, rd=/\d+/;
		var aField;
		var bField;

		return list.sort( function(as, bs) {
			aField = as[sortField];
			bField = bs[sortField];
//			if( sortField === 'openDateTime' ) {
//				if( as.started !== null ) {
//					aField = as.started;
//				}
//				if( bs.started !== null ) {
//					bField = bs.started;
//				}
//			}

			a = String(aField).toLowerCase().match(rx);
			b = String(bField).toLowerCase().match(rx);
			while (a.length && b.length) {
				a1 = a.shift();
				b1 = b.shift();
				if (rd.test(a1) || rd.test(b1)) {
					if (!rd.test(a1)) return 1;
					if (!rd.test(b1)) return -1;
					if (a1 != b1) return a1-b1;
				}
				else if (a1 != b1) return a1 > b1 ? 1 : -1;
			}
			return a.length - b.length;
		});
	};
//	console.log( 'UserSort', sortObject, 'Okay to go' );
};