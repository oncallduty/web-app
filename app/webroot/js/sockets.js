/* global Datacore,Social */

var nodeURL;
var sio = null;
var sio_callback;
var sio_events = [
	{event: 'error', handler: sio_error},
	{event: 'addAnnotation', handler: sio_add_annotation},
	{event: 'attendee_kick', handler: sio_attendee_kick},
	{event: 'cases_updated', handler: sio_case_updated},
	{event: 'clearAnnotations', handler: sio_clear_annotations},
	{event: 'connect', handler: sio_connect},
	{event: 'disconnect', handler: sio_disconnect},
	{event: 'deleteAnnotation', handler: sio_delete_annotation},
	{event: 'deposition_deleted_folder', handler: sio_deposition_deleted_folder},
	{event: 'deposition_deleted_file', handler: sio_deposition_deleted_file},
	{event: 'deposition_end', handler: sio_deposition_end},
	{event: 'deposition_link', handler: sio_deposition_link},
	{event: 'deposition_setspeaker', handler: sio_deposition_setspeaker},
	{event: 'deposition_start', handler: sio_deposition_start},
	{event: 'deposition_uploaded_files', handler: sio_deposition_uploaded_files},
	{event: 'endPresentation', handler: sio_presentation_ended},
	{event: 'exhibits_block', handler: sio_exhibits_block},
	{event: 'file_modified', handler: sio_file_modified},
	{event: 'files_shared', handler: sio_files_shared},
	{event: 'introduced_exhibit', handler: sio_introduced_exhibit},
	{event: 'invite', handler: sio_invite},
	{event: 'message_to_room', handler: sio_message_to_room},
	{event: 'modifyAnnotation', handler: sio_modify_annotation},
	{event: 'reloadPresentationSource', handler: sio_reload_presentation_source},
	{event: 'room_closed', handler: sio_room_closed},
	{event: 'room_renamed', handler: sio_room_renamed},
	{event: 'setAnnotator', handler: sio_presentation_annotator},
	{event: 'setPageProperties', handler: sio_presentation_page},
	{event: 'shared_file', handler: sio_files_shared},
	{event: 'shared_files', handler: sio_files_shared},
	{event: 'shared_folder', handler: sio_files_shared},
	{event: 'sharing_canceled', handler: sio_sharing_canceled},
	{event: 'sharing_completed', handler: sio_sharing_completed},
	{event: 'sharing_confirmed', handler: sio_sharing_confirmed},
	{event: 'sharing_updated', handler: sio_sharing_updated},
	{event: 'startPresentation', handler: sio_presentation_started},
	{event: 'transcriptMessage', handler: sio_transcript_message},
	{event: 'liveTranscriptHistory', handler: sio_transcript_history},
	{event: 'transcriptReset', handler: sio_transcript_reset},
	{event: 'user_did_connect', handler: sio_user_did_connect},
	{event: 'user_did_connect_to_deposition', handler: sio_user_did_connect_to_deposition},
	{event: 'user_did_disconnect', handler: sio_user_did_disconnect},
	{event: 'user_did_disconnect_from_deposition', handler: sio_user_did_disconnect_from_deposition},
	{event: 'user_kicked', handler: sio_user_kicked},
	{event: 'witness_uploaded', handler: sio_witness_uploaded},
	{event: 'witness_login', handler: sio_witness_login},
	{event: 'authorized_witness', handler: sio_authorized_witness},
	{event: 'rejected_witness', handler: sio_rejected_witness},
	{event: 'vacate_deposition', handler: sio_deposition_demo_end},
	{event: 'userDidJoinPresentation', handler: sio_user_did_join_presentation},
	{event: 'userDidLeavePresentation', handler: sio_user_did_leave_presentation},
	{event: 'addCallout', handler: sio_add_callout},
	{event: 'modifyCallout', handler: sio_modify_callout},
	{event: 'deleteCallout', handler: sio_delete_callout},
	{event: 'setCalloutZIndex', handler: sio_set_callout_zindex}
];

$(document).ready( function() {
	if( typeof Datacore === 'undefined' || !Datacore.s || !Datacore.s.nodeURL || !Datacore.s.sKey || !Datacore.s.currentUserID ) return;
	connectWithSKey();
} );

function connectWithSKey()
{
	//console.log( 'sockets::connectWithSKey'+ Datacore.s.sKey);
	console.log( 'sockets::nodeURL'+ nodeURL);

	if( typeof Datacore === 'undefined' || !Datacore.s || !Datacore.s.sKey || !Datacore.s.currentUserID ) return;
	if( typeof nodeURL === 'undefined' && !Datacore.s.nodeURL ) return;
	if( !nodeURL && Datacore.s.nodeURL ) {
		nodeURL = Datacore.s.nodeURL;
	}

	if (typeof authUserLog == 'undefined'){authUserLog = '';}

	sio = io.connect( nodeURL, {"query":"sKey=" + Datacore.s.sKey + "&id=" + Datacore.s.currentUserID+'&aul='+authUserLog} );
	for( var se in sio_events ) {
		sio.on( sio_events[se].event, sio_events[se].handler );
	}
}

function sendChatMessage( roomMsg, roomID ) {
	console.log( '-- send "'+roomMsg+'" to room '+roomID );
	sio.emit( 'message_to_room', {
		roomID : roomID,
		message : roomMsg,
		system : false
	}, function(d){ console.log(d); } );
}

function getFileParams( fileargs, provider ) {
	var sharedByNames = provider.name.split( ' ' );
	return {
		ID : fileargs.ID,
		name : fileargs.name,
		isExhibit : fileargs.isExhibit,
		created : fileargs.created,
		createdBy : provider.id,
		// isPrivate : ??,
		mimeType : fileargs.mimetype,
		byUser : {
			ID : provider.id,
			email : provider.email,
			firstName : (sharedByNames.length > 0 ? sharedByNames.shift() : 'undefined'),
			lastName : (sharedByNames.length > 0 ? sharedByNames.join( ' ' ) : '')
		}
	};
}

function sio_error( reason ) {
	console.error( 'Unable to connect', reason );
}

function sio_attendee_kick( args ) {
	console.log( '-- attendee_kick: ' + JSON.stringify( args ) );
	var dialogText = '';
	if (typeof args.anotherUserLoggedIn !== 'undefined' && args.anotherUserLoggedIn === true) {
		dialogText = 'Another user logged in under your account';
	} else {
		if (!Datacore.s.connectedToDeposition) {
			console.log( '-- Not processing message because we are not connected to a session.' );
			return;
		}
		if (typeof args.permanently !== 'undefined' && args.permanently) {
			dialogText = 'You have been permanently removed from the session';
		} else {
			dialogText = 'You have been removed from the session';
		}
	}
	if (dialogText.length <= 0) {
		window.location.replace( '/users/logout?ul=3' );
	} else {
		setDialog1BtnCloseAction( function() { window.location.replace( '/users/logout?ul=3' ); } );
		showDialog1Btn( dialogText, '', 'OK' );
	}
	sio.disconnect();
}

function sio_case_updated( args ) {
	console.log( ' -- case_updated:', '01', args );
	// Only update cases if we are on the cases screen.
	var pathname = $('#ipad_container_frame')[0].contentWindow.location.pathname;
	if (pathname === '/cases') {
		$('#ipad_container_frame').attr( 'src', pathname );
	} else if(pathname === '/depositions' || pathname === '/depositions/pdf') {
		window.top.webApp.checkDepositionAttendance(Datacore.s.depositionID, function(valid) {
			if(!valid && !window.top.webApp.isOwner()) {
				$('#ipad_container_frame').attr( 'src', '/cases' );
				return;
			}
			window.top.dcFolders = {};
			window.top.Datacore.syncAllFolders(function() {
				if(window.top.navBar.getPathname() === 'depositions') {//$('#ipad_container_frame')[0].contentWindow.location.pathname === '/depositions') {
					$('#ipad_container_frame').attr( 'src', "/depositions" ); // reload
				} else {
					if(window.top.webApp.getS("viewerFile.folderID")) {
						var folder = window.top.Datacore.getFolder(window.top.webApp.getS("viewerFile.folderID"));
						if(!isset(folder)) {
							$('#ipad_container_frame').attr( 'src', '/depositions' );
						}
					}
				}
			});
		});
	}
	dialogTooltip.show( 'Notice!', 'A Case or Session has been modified or deleted.' );
}

function sio_connect() {
	console.log( ' -- connect' );
	if( (typeof Datacore.s.connectedToDeposition === 'undefined' || !Datacore.s.connectedToDeposition) && typeof Datacore.s.depositionID !== 'undefined' ) {
		connect_to_deposition();
	}
	if( typeof sio_callback === 'function' ) {
		sio_callback();
		sio_callback = null;
	}
}

function sio_disconnect() {
	// The socket has disconnected
	console.log( '-- sio_disconnect()' );
	// Make sure we don't think we are connected to a deposition
	Datacore.s.connectedToDeposition = false;
	// reset attendee list so all users are inactive
	var users = Datacore.getUsers();
	for (var i=0; i<users.length; ++i) {
		users[i].active = false;
	}
	if (typeof Social !== 'undefined' && typeof Social.drawAttendeeList === 'function') {
		Social.drawAttendeeList();
	}
}

function sio_deposition_deleted_folder( args ) {
	console.log( '-- sio_deposition_deleted_folder: ' + JSON.stringify( args ) );

	if ( !Datacore.hasFolder( args.folderIDs[0] ) ) {
		return;
	}

	var _depo = Datacore.getDeposition( Datacore.s.depositionID );
	_depo.dropFolder( args.folderIDs[0] );
	Datacore.syncAllFolders();
	showTooltip( 'A folder has been removed from this session', true );
}

function sio_deposition_deleted_file( args ) {
	console.log( '-- sio_deposition_deleted_file: ', args );

	if( typeof args !== "object" || args === null || typeof args.folderIDs !== "object" || !jQuery.isArray( args.folderIDs ) ) {
		console.warn( 'missing folderIDs' );
		return;
	}

	for( var f in args.folderIDs ) {
		var fID = args.folderIDs[f];
		if( !Datacore.hasFolder( fID ) ) {
			console.warn( 'folder not in DataCore: ', fID );
			return;
		}
		var depo = Datacore.getDeposition( Datacore.s.depositionID );
		Datacore.syncAllFolders();
	}

	showTooltip( 'Document(s) have been removed from this session', true );
}

function sio_deposition_end( args )
{
	console.log( '-- deposition_end: ' + JSON.stringify( args ) );
	if (typeof Datacore.s.connectedToDeposition !== 'undefined' && Datacore.s.connectedToDeposition) {
		Datacore.s.depositionStatus = 'F';
		Datacore.s.connectedToDeposition = false;
		if (typeof Social !== 'undefined' && $('#social_list_container').length > 0 && $('#social_list_container').is( ':visible' )) {
			Social.toggleAttendeeList();
			Social.hideChat();
		}
		if ($('#attendee_list_container').length > 0) {
			$('#attendee_list_container').hide();
		}
//		if (isset( _titleActions )) {
//			_titleActions.hideIntroduce();
//			_titleActions.redraw();
//		}
		navBar.introduceButton.disable();
		if( typeof Annotate !== 'undefined' && Annotate !== null ) {
			Annotate.depositionEnded();
		}
		if( typeof Introduce !== 'undefined' && Introduce !== null ) {
			Introduce.depositionEnded();
		}

		showDialog1Btn( 'Session Ending', 'This session has been marked as finished.<br /><br />Please be sure to save any annotated documents before logging out.', 'Close' );

		$.getJSON( '/depositions/updateDeposition', null, function( data, status, xhr ) {
			if (status === 'success') {
				console.log( '-- Updated deposition on server.' );
			}
		} );
	} else {
		// If not connected to a deposition, refresh the content iframe
		$('#ipad_container_frame').attr( 'src', $('#ipad_container_frame').attr( 'src' ) );
	}
}

function sio_deposition_demo_end( args )
{
	console.log( '-- deposition_demo_end: ' + JSON.stringify( args ) );
	if (typeof Datacore.s.connectedToDeposition !== 'undefined' && Datacore.s.connectedToDeposition) {
		Datacore.s.depositionStatus = 'N';
		Datacore.s.connectedToDeposition = false;
		if (typeof Social !== 'undefined' && $('#social_list_container').length > 0 && $('#social_list_container').is( ':visible' )) {
			Social.toggleAttendeeList();
			Social.hideChat();
		}
		if ($('#attendee_list_container').length > 0) {
			$('#attendee_list_container').hide();
		}
//		if (isset( _titleActions )) {
//			_titleActions.hideAll();
//			_titleActions.showLogout();
//			_titleActions.redraw();
//		}
		navBar.allDisable();
		navBar.logoutButton.enable();
		if( typeof Annotate !== 'undefined' ) {
			Annotate.depositionEnded();
			Annotate.normalscreen();
			Annotate.cleanEvents();
		}
		if (typeof window.frames[0] !== 'undefined' && typeof window.frames[0].frames[0] !== 'undefined' && typeof window.frames[0].frames[0].Introduce !== 'undefined') {
			window.frames[0].frames[0].Introduce.depositionEnded();
		}

		setDialog1BtnCloseAction( function() {
			//Redirect with a timeout so we let any callbacks finish before we change the web page.
			setTimeout( window.resetredirect(), 500 );
		});
		showDialog1Btn( 'Notice!', 'This session has ended.', 'Close' );
	} else {
		// If not connected to a deposition, refresh the content iframe (should never happen)
		$('#ipad_container_frame').attr( 'src', $('#ipad_container_frame').attr( 'src' ) );
	}
}

function sio_deposition_link( args )
{
	console.log( '-- deposition_link: ' + JSON.stringify( args ) );
}

function sio_deposition_setspeaker( args )
{
	console.log( '-- deposition_setspeaker: ', args );
	if (!Datacore.s.connectedToDeposition) {
		console.log( '-- Not processing message because we are not connected to a deposition.' );
		return;
	}
	var _depo = webApp.getS( 'deposition' );
	_depo.speakerID = Datacore.s.speakerID = parseInt( args.newSpeakerID );

	if (typeof args.exhibitTitle !== 'undefined') {
		Datacore.s.exhibitTitle = args.exhibitTitle;
	}
	if (typeof args.exhibitSubTitle !== 'undefined') {
		Datacore.s.exhibitSubTitle = args.exhibitSubTitle;
	}
	if (typeof args.exhibitXOrigin !== 'undefined') {
		Datacore.s.exhibitXOrigin = parseFloat(args.exhibitXOrigin);
	}
	if (typeof args.exhibitYOrigin !== 'undefined') {
		Datacore.s.exhibitYOrigin = parseFloat(args.exhibitYOrigin);
	}
	if( webApp.userIsSpeaker() === true ) {
		if(Datacore.s.inPresentation) {
			webApp.presentationEnd();
		} else {
			webApp.presentationStatus();
		}
		//webApp.toggleAttendPresentationBtn(false, true);
		showPlainDialog1Btn( 'You are now the leader!', '', 'Close' );
		if( typeof args.witnesses !== 'undefined' && args.witnesses !== null ) {
			for( var w in args.witnesses ) {
				var _witness = args.witnesses[w];
				var witnessID = parseInt( _witness.id );
				var user = Datacore.getUser( witnessID );
				if( typeof user !== 'undefined' && user !== null ) {
					user.blockExhibits = _witness.blockExhibits;
				}
			}
		}
	} else {
		var newLeader = Datacore.getUser( Datacore.s.speakerID );
		showTooltip( 'Leader role has been passed to '+newLeader.name+'.', true );
		if(Datacore.s.inPresentation) {
			webApp.presentationEnd();
		} else {
			webApp.presentationStatus();
		}
		//webApp.toggleAttendPresentationBtn(true, true);
	}

	if( typeof Social !== 'undefined' && Social !== null ) {
		Social.drawAttendeeList();
	}
	if( typeof tablesPanel !== 'undefined' && tablesPanel !== null ) {
		tablesPanel.sortFolders();	//redefine draggable
	}
	if( typeof Annotate !== 'undefined' && Annotate !== null ) {
		Annotate.updateSpeaker();
	}
	if( typeof Introduce !== 'undefined' && Introduce !== null ) {
		Introduce.updateSpeaker();
	}
	if( typeof Multimedia !== 'undefined' && Multimedia !== null ) {
		Multimedia.updateSpeaker();
	}
	if( typeof window.top.webApp !== 'undefined' && window.top.webApp !== null ) {
		window.top.webApp.updateSpeaker();
	}
}

function sio_deposition_start( args )
{
	console.log( '-- deposition_start:', args );
	if( webApp.getS( 'inDeposition' ) !== args.deposition.id ) {
		console.warn( 'wrong deposition!', webApp.getS( 'inDeposition' ), args.deposition );
		return;
	}
	if( webApp.userIsSpeaker() !== true ) {
		console.warn( 'wrong leader!', webApp.getS( 'deposition.speakerID' ) );
		return;
	}
	var deposition = Datacore.getDeposition( args.deposition.id );
	deposition.statusID = 'I';
	Datacore.s.depositionStatus = 'I';
	if( typeof Datacore.s.started === 'undefined' || Datacore.s.started === null ) {
		$('#btn_start_deposition', iPadContainerFrame.document).hide();
		$('#btn_finish_deposition', iPadContainerFrame.document).show();
		$('.deposition_passcode', iPadContainerFrame.document).css( 'display', 'inline-block' );
		$('#attendee_list_toggle_wrap').show();
		$('#attendee_list_container').show();
	}
}

function sio_deposition_uploaded_files( args )
{
	console.log( '-- deposition_uploaded_files: ' + JSON.stringify( args ) );

	if (parseInt(args.depositionID) !== window.top.webApp.getS( 'inDeposition' )) {
		return;
	}
	if (typeof args.folderID !== 'undefined') {
		Datacore.syncAllFolders();
		if(window.top.webApp.getPersonalFolderID() !== parseInt(args.folderID)) {
			showTooltip( 'Document(s) have been uploaded and added to this session', true );
		}
	}
}

function sio_exhibits_block( args )
{
	console.log( '-- exhibits_block: ' + JSON.stringify( args ) );
	if (!Datacore.s.connectedToDeposition) {
		console.log( '-- Not processing message because we are not connected to a deposition.' );
		return;
	}
	if (typeof args.depositionID !== 'undefined' && Datacore.s.depositionID === parseInt( args.depositionID )) {
		var user = Datacore.getUser( parseInt( args.userID ) );
		if (user.ID !== null && typeof args.block !== 'undefined') {
			user.blockExhibits = (typeof args.block === 'boolean') ? args.block : (parseInt( args.block ) !== 0);
			window.top.webApp.setS("blockExhibits", user.blockExhibits);
		}

		// if a blocked user is viewing a pdf or text doc, force back to the depositions page
		if( user.blockExhibits && iPadContainerFrame.location.pathname !== '/depositions' ) {
			iPadContainerFrame.location.replace( '/depositions' );
		} else if( typeof tablesPanel !== 'undefined' && tablesPanel !== null ) {
			tablesPanel.sortFolders();
			tablesPanel.sortFiles();
			checkPresentation();	//TODO: verify and check typeof first
		}
	}
}

function sio_file_modified( args )
{
	console.log( '-- file_modified: ' + JSON.stringify( args ) );
	if (parseInt(args.fileID) === window.top.webApp.getFileID()) {
		showTooltip( 'File modified, reloading', true );
		window.top.webApp.viewFile(args.fileID, "File Updated, Abort Changes?");
	}
}

function sio_files_shared( args )
{
	console.log( '-- files_shared:', args );
	if( !Datacore.s.connectedToDeposition ) {
		console.log( '-- Not processing message because we are not connected to a deposition.' );
		return;
	}

	if( args.files.length > 0 ) {
		var sharedFolder = false;
		var _depo = Datacore.getDeposition( Datacore.s.depositionID );
		// update Datacore with potential new Folder
		if( !Datacore.hasFolder( args.folder.ID ) ) {
			_depo.addFolder( args.folder );
			sharedFolder = true;
		}
		var sharingFolder = Datacore.getFolder( args.folder.ID );

		var firstShare = false;

		if( typeof sharingFolder !== 'undefined' && sharingFolder !== null ) {
			// update Datacore with any new Files
			for( var af in args.files ) {
				if( Datacore.hasFile( args.files[af].ID ) ) {
					sharingFolder.dropFile( args.files[af].ID);	//Remove old entry
				} else {
					firstShare = true;
				}
				sharingFolder.addFile( getFileParams( args.files[af], args.sharedBy ) );
				if( isset( args.files[af].exhibitHistory ) ) {
					if( Datacore.hasExhibit( args.files[af].exhibitHistory.ID )) {
						Datacore.dropExhibitHistory( args.files[af].exhibitHistory.ID );
					}
					Datacore.addExhibitHistory( args.files[af].exhibitHistory );
				}
			}
		}

		// update deposition page and display dialog
		if( typeof tablesPanel !== 'undefined' && tablesPanel !== null ) {
			tablesPanel.sortFolders();
		}

		if( parseInt( args.sharedBy.id ) === Datacore.s.currentUserID ) {
			// Don't show updates from ourself.
			return;
		}

		// prepare notification dialog
		var dialogTitle = "";
		var dialogContent = "";
		var isExhibit = false;

		if( sharedFolder ) {
			dialogTitle = 'Incoming Folder';
			dialogContent = args.sharedBy.name + ' shared a folder with you.';
		} else if( args.files[0].isExhibit ) {
			isExhibit = true;
			if( args.files.length > 1 ) {
				dialogTitle = 'Incoming Exhibits';
				dialogContent = args.sharedBy.name+' introduced ' + args.files.length + ' exhibits.';
			} else {
				dialogTitle = 'Incoming Exhibit';
				dialogContent = args.sharedBy.name + (firstShare ? ' introduced ' : ' re-introduced ') + args.files[0].name + '.';
			}
		} else {
			if( args.files.length > 1 ) {
				dialogTitle = 'Incoming Documents';
				dialogContent = args.sharedBy.name + ' shared ' + args.files.length + ' Documents with you.';
			} else {
				dialogTitle = 'Incoming Document';
				dialogContent = args.sharedBy.name + ' shared a Document with you.';
			}
		}
		if( !isExhibit ) {
			showDialog1Btn( dialogTitle, dialogContent, 'OK' );
		} else {
			dialogTooltip.show( dialogTitle, dialogContent );
		}
	}
}

function sio_introduced_exhibit( args )
{
	console.log( '-- introduced_exhibit:', args );
	if( !Datacore.s.connectedToDeposition ) {
		console.log( '-- Not processing message because we are not connected to a deposition.' );
		return;
	}


	var sharingFolder = Datacore.getFolder( args.folder.ID );
	var firstShare = false;

	if( typeof sharingFolder !== 'undefined' && sharingFolder !== null ) {
		// update Datacore with any new Files
		for( var af in args.files ) {
			if( Datacore.hasFile( args.files[af].ID ) ) {
				sharingFolder.dropFile( args.files[af].ID);	//Remove old entry
			} else {
				firstShare = true;
			}
			sharingFolder.addFile( getFileParams( args.files[af], args.sharedBy ) );
			if( isset( args.files[af].exhibitHistory ) ) {
				if( Datacore.hasExhibit( args.files[af].exhibitHistory.ID )) {
					Datacore.dropExhibitHistory( args.files[af].exhibitHistory.ID );
				}
				Datacore.addExhibitHistory( args.files[af].exhibitHistory );
			}
		}
	}

	// update deposition page
	if( typeof tablesPanel !== 'undefined' && tablesPanel !== null ) {
		tablesPanel.sortFolders();
	}

	// update the file selection drawer.
	if(isset(window.top.fileSelection)) {
		window.top.fileSelection.refresh();
	}

	// Don't show updates from ourself
	if( parseInt( args.sharedBy.id ) === Datacore.s.currentUserID ) {
		return;
	}

	// prepare notification dialog
	var dialogTitle = 'Incoming Exhibit';
	var dialogContent = args.sharedBy.name + (firstShare ? ' introduced ' : ' re-introduced ') + args.files[0].name + '.';
	dialogTooltip.show( dialogTitle, dialogContent );

	//console.log(args);
	//console.log(args.files[0]);
	//console.log(args.files[0].ID);
	//console.log($('#file_item_'+args.files[0].ID));

	var dialogButtons = [];
	dialogButtons.push({
		label: 'Open New Exhibit',
		id: 'open_introduced_file',
		class: 'btn_app_normal',
		style: 'width:160px'
	});

	dialogButtons.push({
		label: 'Stay On Current View',
		id: 'dont_open_introduced_file',
		class: 'btn_app_cancel',
		style: 'width:195px'
	});



	if (!Datacore.s.inPresentation){

		var confirmMsg = 'Do you want to open the exhibit file '+args.files[0].name;
		showDialog3Btn('New Exhibit introduced', confirmMsg, dialogButtons, true);

		$('#dialog3Btn #open_introduced_file').attr('style','width:50%');
		$('#dialog3Btn #dont_open_introduced_file').attr('style','width:50%');

		$('#dialog3Btn #open_introduced_file', window.top.document).on("click", function() {
			console.log(args);
			dismissDialog3Btn();
			webApp.setItem('viewFile', JSON.stringify(args.files[0]));

			var fileID = args.files[0].ID;
			var mimeType = args.files[0].mimetype;
			webApp.setItem( 'selectedFileID', fileID );
			if( mimeType.indexOf( "audio" ) > -1 ) {
				window.parent.frames["ipad_container_frame"].location = '/depositions/audio?ID=' + fileID;
			}
			else if( mimeType.indexOf( "video" ) > -1 ) {
				window.parent.frames["ipad_container_frame"].location = '/depositions/video?ID=' + fileID;
			}
			else {
				window.parent.frames["ipad_container_frame"].location = '/depositions/pdf?ID=' + fileID;
			}

		});
		$('#dialog3Btn #dont_open_introduced_file', window.top.document).on("click", function() {
			dismissDialog3Btn();
		});
	}else{
		console.log('In Presentation, skipping incoming exhibit popup');
	}


}

function sio_invite( args )
{
	console.log( '-- invite: ' + JSON.stringify( args ) );
	if (!Datacore.s.connectedToDeposition) {
		console.log( '-- Not processing message because we are not connected to a deposition.' );
		return;
	}
	if (!Datacore.hasRoom( args.room.id )) {
		if (args.room.users.length === 1) {
			if (args.room.users[0].id !== Datacore.s.currentUserID) {
				args.room.users.push( {id : Datacore.s.currentUserID} );
			}
		} else {
			var userExists = false;
			for (var aru in args.room.users) {
				if (args.room.users[aru].id === Datacore.s.currentUserID) {
					userExists = true;
					break;
				}
			}
			if (!userExists) {
				args.room.users.push( {id : Datacore.s.currentUserID} );
			}
		}
		Datacore.addRoom( args.room );
		Social.drawAttendeeList();
	}
	sio.emit( 'join_to_room', {
		id : args.room.id,
		title : args.room.title,
		creatorId : args.room.creator.id,
		private : args.room.private
	}, function( result ) {
		// result reports an error, however join_to_room does succeed
		// console.log( '-- joined room: ' + JSON.stringify( result ) );
	} );

}

function sio_message_to_room( args ) {
	console.log( '-- message_to_room: ' + JSON.stringify( args ) );

	// add the message to Datacore
	if (!Datacore.hasRoom( args.room.id)) {
		// throw an exception ??
	}

	var room = Datacore.getRoom( args.room.id );
	room.addMessage( args );

	if (typeof Social !== 'undefined') {
		if (isset( Social.currentRoom ) && Social.currentRoom === room.ID && Social.isCurrentRoomVisible()) {
			Social.addChatMessage( {
				userName : args.user.name,
				text : args.message,
				timestamp : args.timestamp,
				system : args.system
			} );
		} else {
			// update the group/room badge count
			room.unreadMessageCount += 1;
			if (room.private) {
				var allAttendees = Datacore.getRoom( Datacore.getPrimaryDepositionID()+'.deposition' );
				allAttendees.unreadChildMessageCount += 1;
			}
			Social.reportUnreadMessages();
			if( typeof Annotate !== 'undefined' ) {
				Annotate.newChatMessage();
			}
		}
	}
}

function sio_user_did_join_presentation( args ) {
	console.log( '-- user_did_join_presentation:', args );
	if( Datacore.addPresentationUser( args.user.id ) && typeof Social !== 'undefined' && typeof Social.drawAttendeeList === 'function' ) {
		Social.drawAttendeeList();
	}
}

function sio_user_did_leave_presentation( args ) {
	console.log( '-- user_did_leave_presentation: ' + JSON.stringify( args ) );
	if( Datacore.dropPresentationUser( args.user.id ) && typeof Social !== 'undefined' && typeof Social.drawAttendeeList === 'function' ) {
		Social.drawAttendeeList();
	}
}

function sio_presentation_started( args )
{
	console.log( '-- presentation_started: ' + JSON.stringify( args ) );
	if (window.top.webApp.isWitness()) {
		window.top.webApp.presentationJoinSave();
	} else {
		if (typeof window.top.webApp !== 'undefined') {
			window.top.webApp.presentationActive();
		}
	}
}

function sio_presentation_ended()
{
	console.log( '-- presentation_ended()' );
	if (Datacore.s.presentationAvailable) {
		if( Datacore.s.inPresentation === true ) {
			if( $('#dialog3Btn').is( ':visible' ) ) {
				dismissDialog3Btn();
			}
//			showPlainDialog1Btn( 'Presentation Ended!', 'The Presentation Session has closed.', 'Close', 'PresentationEnded' );
			dialogTooltip.show( 'Presentation Ended!', 'The Presentation Session has closed.' );
		} else {
			if( $('#dialog3Btn').attr( 'data-tag' ) === 'JoinPresentationStarted' ) {
				dismissDialog3Btn();
			}
		}
		Datacore.s.presentationAvailable = false;
	}

	Datacore.s.inPresentation = false;
	window.top.webApp.removeItem( 'inPresentation' );
	if (typeof Social !== 'undefined') {
		Social.presentationEnd();
	}

//	if( typeof fileSelection !== 'undefined' && fileSelection !== null ) {
//		fileSelection.disableFileSelection();
//		delete fileSelection;
//	}

	var pData = webApp.getS( 'joinPresentationData' );
	if( typeof pData !== 'undefined' && pData !== null ) {
		if( typeof window.top.webApp !== 'undefined' ) {
			console.log( '-- presentation_ended()', {Speaker:window.top.webApp.userIsSpeaker(),isBackEvent:window.top.webApp.getS("isBackEvent")} );
			if(!!Datacore.s.myPresentation) {
				Datacore.s.myPresentationData = pData;
			}
			window.top.webApp.setS( 'joinPresentationData', null );
			if(window.top.webApp.getS("isBackEvent") || !window.top.webApp.userIsSpeaker()) {
				// not speaker, or hit back button as speaker
				window.top.webApp.setS("isBackEvent", false);
				console.log("-- redirect /depositions -- sockets::sio_presentation_ended(01);");
				window.top.iPadContainerFrame.location.replace( '/depositions' );
			}
			else if(window.top.webApp.userIsSpeaker()) {
				// user is speaker, enable save for changes.
				window.top.navBar.saveButton.enable();
			}
		}
	}
}

function sio_presentation_page( args ) {
	// console.log( "-- sio_presentation_page; args:", args );
	if( isset( window.top.webApp ) ) {
		window.top.webApp.presentationPageAdd( args );
	}
}

function sio_presentation_annotator( args )
{
	console.log( '-- sio_presentation_annotator:', args );
	var pData = window.top.webApp.getS( 'joinPresentationData' );
	if( typeof pData !== 'undefined' && pData !== null ) {
		pData.annotatorID = args.annotatorID;
		pData.annotationIndex = args.annotationIndex;
		window.top.webApp.setS( 'joinPresentationData', pData );
	}
	if( window.top.Datacore.s.inPresentation !== true ) {
		return;
	}
	if( typeof window.top.webApp !== 'undefined' ) {
		window.top.webApp.annotatorChange( args );
	}
}

function sio_add_annotation( args ) {
	console.log( "-- sio_add_annotation:", args );
	var annotation = args;
	annotation.Message = "AddAnnotation";
	if( typeof window.top.webApp !== "undefined" ) {
		window.top.webApp.presentationAnnotation( annotation );
	}
}

function sio_clear_annotations( args ) {
	console.log( "-- sio_clear_annotations:", args );
	if( typeof window.top.webApp !== "undefined" ) {
		window.top.webApp.presentationClearAll( args );
	}
}

function sio_delete_annotation( args ) {
	console.log( "-- sio_delete_annotation:", args );
	var annotation = args;
	annotation.Message = "DeleteAnnotation";
	if( typeof window.top.webApp !== "undefined" ) {
		window.top.webApp.presentationDelete( annotation );
	}
}

function sio_modify_annotation( args ) {
	console.log( "-- sio_modify_annotation:", args );
	var annotation = args;
	annotation.Message = "ModifyAnnotation";
	if( typeof window.top.webApp !== "undefined" ) {
		window.top.webApp.presentationModify( annotation );
	}
}

function sio_reload_presentation_source( args ) {
	console.log( '-- sio_reload_presentation_source', args );

	var depositionID = Datacore.getRootDepositionID();
	presentationJoin( depositionID, true );
}

function sio_room_closed( args )
{
	console.log( '-- room_closed: ' + JSON.stringify( args ) );
	if (!Datacore.s.connectedToDeposition) {
		console.log( '-- Not processing message because we are not connected to a session.' );
		return;
	}
	if (typeof args.room.id !== 'undefined' && Datacore.hasRoom( args.room.id )) {
		Datacore.dropRoom( args.room.id );
		if (typeof Social !== 'undefined') {
			Social.drawAttendeeList();
		}
	}
}

function sio_room_renamed( args )
{
	console.log( '-- room_renamed: ' + JSON.stringify( args ) );
	if (!Datacore.s.connectedToDeposition) {
		console.log( '-- Not processing message because we are not connected to a deposition.' );
		return;
	}
	if (typeof args.room !== 'undefined' && args.room !== null) {
		var roomID = isset( args.room.id ) ? args.room.id : null;
		var roomTitle = isset( args.room.title ) ? args.room.title : '';
		if (roomID !== null && roomTitle.length > 1) {
			var Room = Datacore.getRoom( roomID );
			if (Room.title !== roomTitle) {
				Room.title = roomTitle;
				var identifier = '#group_'+(roomID.replace( /[^a-zA-Z0-9]/g, '_' ))+' .list_item_title';
				if ($(identifier).length > 0) {
					$(identifier).text( Room.title+' ('+(parseInt( Room.getMemberCount() )-1)+')' );
				}
			}
			if (typeof args.users !== 'undefined' && args.users !== null) {
				for (var u in args.users) {
					Room.addMember( args.users[u] );
				}
			}
			if (typeof Social !== 'undefined') {
				Social.drawAttendeeList();
			}
		}
	}
}

function sio_sharing_canceled( args )
{
	console.log( '-- sharing_canceled: ' + JSON.stringify( args ) );
}

function sio_sharing_completed( args )
{
	console.log( '-- sharing_completed: ' + JSON.stringify( args ) );
}

function sio_sharing_confirmed( args )
{
	console.log( '-- sharing_confirmed: ' + JSON.stringify( args ) );
}

function sio_sharing_updated( args )
{
	console.log( '-- sharing_updated: ' + JSON.stringify( args ) );
}

function sio_user_did_connect( args ) {
	console.log( '-- user_did_connect: ' + JSON.stringify( args ) );
	// update the User stored in Datacore
	if (dcUsers[args.user.id])
		Datacore.dropUser( args.user.id );
	Datacore.addUser( args.user );
	// refresh the User room for chatserver
	var roomsToJoin = (!Array.isArray( args.room ) ? [args.room] : args.room);
	for (var r in roomsToJoin) {
		if (!Datacore.hasRoom( roomsToJoin[r].id )) {
			Datacore.addRoom( roomsToJoin[r] );
		}
		var room = Datacore.getRoom( roomsToJoin[r].id );
		room.addMember( args.user );
	}
	if (typeof Social !== 'undefined' && typeof Social.drawAttendeeList === 'function') {
		Social.drawAttendeeList();
	}
	if( typeof Annotate !== 'undefined' ) {
		Annotate.updateUsers();
	}
}

function sio_user_did_connect_to_deposition( args )
{
	console.log( '-- user_did_connect_to_deposition:', args );
}

function sio_user_did_disconnect( args )
{
	console.log( '-- user_did_disconnect:', args );
	var _room = Datacore.getRoom( args.room.id );
	if( typeof _room === 'undefined' || _room === null ) {
		return;
	}
	_room.dropMember( args.user.id );
	if( typeof Social !== 'undefined' && Social !== null ) {
		Social.drawAttendeeList();
	}
	if( typeof Annotate !== 'undefined' && Annotate !== null ) {
		Annotate.updateUsers();
	}
}

function sio_user_did_disconnect_from_deposition( args )
{
	console.log( '-- user_did_disconnect_from_deposition:', args );
	if( typeof window.top.witnessLogins !== 'undefined' && window.top.witnessLogins !== null ) {
		window.top.witnessLogins.remove( args.user );
	}
	if( Datacore.hasUser( args.user.id ) ) {
		Datacore.dropUser( args.user.id );
	}
	Datacore.addUser( args.user );
	if( typeof Social !== 'undefined' && typeof Social.drawAttendeeList === 'function' ) {
		Social.drawAttendeeList();
	}
	if( typeof Annotate !== 'undefined' && typeof Annotate.updateUsers === 'function' ) {
		Annotate.updateUsers();
	}
}

function sio_user_kicked( args )
{
	console.log( '-- user_kicked():', args );

	// update the User stored in Datacore
	if( Datacore.hasUser( args.user.id ) ) {
		Datacore.dropUser( args.user.id );
	}

	if ( typeof Social !== 'undefined' && typeof Social.drawAttendeeList === 'function' ) {
		Social.drawAttendeeList();
	}
	if( typeof Annotate !== 'undefined' ) {
		Annotate.updateUsers();
	}
}

function sio_witness_uploaded( args )
{
	console.log( '-- witness_uploaded: ' + JSON.stringify( args ) );
	if (!Datacore.s.connectedToDeposition) {
		console.log( '-- Not processing message because we are not connected to a deposition.' );
		return;
	}
	if( typeof args.folderID !== 'undefined' ) {
		Datacore.syncFolder( args.folderID, function() {
			Datacore.refreshFolders();
		} );
		var dialogUpdate = {
			title : 'Witness Annotations Saved!',
			content : 'Annotated exhibit has been saved to<br />the Witness Annotations Folder',
			button : 'Close'
		};
		showDialog1Btn( dialogUpdate.title, dialogUpdate.content, dialogUpdate.button );
	}
}

function sio_witness_login( args )
{
	console.log( '-- witness_login:', args );
	if( typeof window.top.witnessLogins === 'undefined' || window.top.witnessLogins === null ) {
		window.top.witnessLogins = new WitnessLogins( args );
	} else {
		window.top.witnessLogins.request( args );
	}
}

function authorizeWitness( depositionID, witnessID, authorize )
{
	console.log( depositionID, witnessID, authorize );
	$.post( 'authorizeWitness', {depositionID:depositionID, witnessID:witnessID, authorize:authorize}, function( data, textStatus, xhr )
	{
		console.log( textStatus, data );
		if( textStatus === 'success' && data !== null ) {
			console.log( 'Okay to go' );
		} else {
			console.log( 'sad day ...' );
		}
	}, 'json' ).fail( function()
	{
		console.log( 'failed ...' );
	} );
}

function sio_authorized_witness( args )
{
	if( typeof window.top.closeFunction === "function" ) {
		window.top.closeFunction = null;
	}
	dismissDialog1Btn();
	window.location.replace( '/i' );
}

function sio_rejected_witness( args )
{
//	console.log( '-- sio_rejected_witness' );
	setDialog1BtnCloseAction( function() { window.location.replace( '/signin' ); } );
	showDialog1Btn( 'Authorization Failed!', 'The Taking Attorney did not authorize your login.', 'Close' );
	sio.disconnect();
	sio = null;
}

function sio_transcript_message( args )
{
//	console.log( '-- transcript_message:', JSON.stringify( args ) );

	if( typeof liveTranscript !== 'undefined' && liveTranscript !== null ) {
		liveTranscript.addMessage( args.message );
	}
}

function sio_transcript_history( args )
{
	console.log( '-- transcript_history:', args );
	if( typeof liveTranscript !== 'undefined' && liveTranscript !== null ) {
		setTimeout( function() { liveTranscript.prepareFromHistory( args.transcriptHistory ); }, 100 );
	}
}

function sio_transcript_reset( args )
{
	console.log( '-- transcript_reset:', JSON.stringify( args ) );

	if( typeof liveTranscript !== 'undefined' && liveTranscript !== null ) {
		window.top.webApp.removeItem( 'liveTranscriptAuth' );
		liveTranscript.reset();
	}
}

function connect_to_deposition( callback )
{
//	console.log( 'sockets::connect_to_deposition' );
	selectDeposition( function() {
		// Check to see if we have an unknown user type or a blocked witness and then redirect accordingly.
		if( Datacore.getS( 'depositionStatus' ) === 'I' ) {
			$('#attendee_list_toggle_wrap').show();
			var currentUser = Datacore.s.currentUser;
			if( typeof currentUser === 'undefined' || currentUser === null || ((currentUser.userType === 'W' || currentUser.userType === 'WM') && currentUser.blockExhibits) ) {
				redirect();
			} else {
				checkPresentation();
			}
		}
	} );
}

function selectDeposition( callback )
{
//	console.log( 'sockets::selectDeposition' );
	var rootDepoID = Datacore.getPrimaryDepositionID();
	sio.emit( 'selectDeposition', {"deposition":rootDepoID}, function( deposition ) {
//		console.log( 'selectDeposition -- connected to deposition' );	//: ' + JSON.stringify( deposition ) );
		if( typeof deposition !== 'undefined' && deposition !== null ) {
			Datacore.s.conductorID = (deposition.hasOwnProperty( 'conductorID' )) ? parseInt( deposition.conductorID ) : 0;
			if( deposition.hasOwnProperty( 'users' ) && deposition.users !== null && Array.isArray( deposition.users ) ) {
				for( var idx in deposition.users ) {
					if( Datacore.addUser( deposition.users[idx] ) !== -1 && parseInt( deposition.users[idx].id ) === Datacore.s.currentUserID ) {
						Datacore.s.currentUser = Datacore.getUser( Datacore.s.currentUserID );
						webApp.setS( 'user', Datacore.s.currentUser );
					}
				}
			}
			window.top.dcRooms = {};
			if( deposition.hasOwnProperty( 'rooms' ) ) {
				Datacore.loadRooms( deposition.rooms );
			}
			if( typeof Social !== 'undefined' && Social !== null ) {
				Social.drawAttendeeList();
			}
		}
		Datacore.s.connectedToDeposition = true;
		if( webApp.enableTranscripts() === true ) {
			if( typeof liveTranscript !== 'undefined' && liveTranscript !== null ) {
				liveTranscript.reset();
				if( webApp.getS( 'deposition.class' ) === 'Deposition' ) {
					liveTranscript.authenticate( null, true );
				}
			}
		}
		if( typeof callback === 'function' ) {
			callback();
		}
	} );
}

function disconnect_from_deposition() {
	console.log( '-- disconnect_from_deposition' );
	sio.emit( 'deselectDeposition', {}, function( status ) {
		console.log( 'disconnected from deposition: ', JSON.stringify( status ) );
		Datacore.s.connectedToDeposition = false;
	} );
}

function sio_add_callout( args ) {
	console.log( 'sockets::sio_add_callout', args );
	window.top.webApp.presentationAddCallout(args);
};

function sio_modify_callout( args ) {
	console.log( 'sockets::sio_modify_callout', args );
	window.top.webApp.presentationModifyCallout(args);
};

function sio_delete_callout( args ) {
	console.log( 'sockets::sio_delete_callout', args );
	window.top.webApp.presentationDeleteCallout(args);
};

function sio_set_callout_zindex( args ) {
	console.log( 'sockets::sio_set_callout_zindex', args );
	window.top.webApp.presentationSetCalloutZIndex(args);
};

function sendBlockExhibits( depositionID, userID, block ) {
	console.log( '-- send block exhibits Deposition: '+depositionID+' UserID: '+userID+' block:'+block );
	sio.emit( 'exhibitsBlock', {
		depositionID : depositionID,
		userID : userID,
		block : block
	}, function(d){ console.log(d); } );
}

function checkPresentation() {
	var depositionID = Datacore.getRootDepositionID();
	var pData = webApp.getS( 'joinPresentationData' );
	if( typeof pData !== 'undefined' && pData !== null ) {
		if( webApp.userIsSpeaker() === true ) {
			if(!window.top.navBar || ['/depositions/presentation','/depositions/introduce'].indexOf(window.top.navBar.getPathname()) === false ) {
				presentationEnd( depositionID );
			}
		} else if ( webApp.isWitness() ) {
			presentationStatus( depositionID );
		} else {
			if (!window.top.navBar || ['/depositions/presentation'].indexOf(window.top.navBar.getPathname()) === false) {
				// leave a presentation only if you aren't in the presentation.
				presentationLeave( depositionID );
				Datacore.s.presentationAvailable = true;
			}
		}
		webApp.setS( 'joinPresentationData', null );
	} else if( typeof Datacore.s.presentationAvailable === 'undefined' || Datacore.s.presentationAvailable !== true ) {
		presentationStatus( depositionID );
	}
}

function presentationStatus( depositionID, callback ) {
//	console.log( '-- send presentation status Deposition:', depositionID );
	sio.emit( 'presentationStatus', {"depositionID": depositionID}, function( status ) {
//		console.log( 'presentationStatus:', status );
		if( isset( status ) && status.success ) {
			if( isset( status.presentationInfo ) ) {
				webApp.setS( 'joinPresentationData', status.presentationInfo );
				if ( webApp.getS( 'user.userType' ) === 'W' || webApp.getS( 'user.userType' ) === 'WM' ) {
					presentationJoin( depositionID );
					return;
				}
				if( typeof Social !== 'undefined' ) {
					if( webApp.userIsSpeaker() === true ) {
						Social.presentationStart( status );
					} else {
						Social.presentationActive();
					}
				}
			} else if( typeof Social !== 'undefined' ) {
				Social.presentationEnd();
			}
			if( typeof callback === 'function' ) {
				callback( status.presentationInfo );
			}
		}
	} );
}

function checkPresentationStatus( e, callback ) {
//	console.log( 'checkPresentationStatus' );
	var depoID = webApp.getS( 'inDeposition' );
	presentationStatus( depoID, callback );
}

function presentationJoin( depositionID, forceReload ) {
	forceReload = !!forceReload;
	if( Datacore.s.inPresentation && !forceReload ) {
		// prevent "presentationJoin" calls from being processed when already joined a presentation
		//return;
	}
	if(window.top.webApp.getS("blockExhibits")) {
		console.log("Error: Blocked, refusing to join presentation.");
		window.top.dismissLoadingOverlay();
		return;
	}
	console.log( '-- send presentation join Deposition: ', depositionID, forceReload );
	sio.emit( 'joinPresentation', {"depositionID": depositionID}, function( status ) {
		console.log( 'joinPresentation:', status );
		if( typeof status.success !== 'undefined' && status.success && typeof status.presentationInfo === 'object' && status.presentationInfo !== null ) {
			webApp.setS( 'joinPresentationData', status.presentationInfo );
			Datacore.s.inPresentation = true;
			window.top.webApp.setItem( 'inPresentation', 'true' );
			window.top.webApp.setItem( 'presentationOrientation', status.presentationInfo.orientation);
			if( forceReload || window.top.iPadContainerFrame.location.pathname !== '/depositions/presentation' ) {
				window.top.iPadContainerFrame.location.replace('/depositions/presentation');
			}
			navBar.setTitle( status.presentationInfo.title );
		} else {
			Datacore.s.inPresentation = false;
			window.top.webApp.removeItem( 'inPresentation' );
			window.top.webApp.removeItem( 'presentationOrientation' );
			iPadContainerFrame.location.replace( '/depositions' );
			window.top.dismissLoadingOverlay();
		}
	} );
}

function presentationLeave( depositionID ) {
	console.log( '-- send presentation leave Deposition: '+depositionID );
	sio.emit( 'leavePresentation', {"depositionID": depositionID}, function( status ) {
		console.log( 'leavePresentation:', status );
		Datacore.s.inPresentation = false;
		window.top.webApp.removeItem( 'inPresentation' );
		$('#ipad_container').removeClass( 'annotator' );
		if (typeof Social !== 'undefined') {
			Social.presentationLeave();
		}
	} );
}

function presentationStart( args ) {
	console.log( '-- send presentation start:', args );
	sio.emit( 'startPresentation', args, function( status ) {
		Datacore.s.inPresentation = true;
		Datacore.s.myPresentation = true;
		window.top.webApp.setItem( 'inPresentation', 'true' );
		window.top.webApp.setItem( 'myPresentation', true );
		console.log( 'startPresentation:',  status );
		if (typeof Social !== 'undefined') {
//			showPlainDialog1Btn( 'Presentation Started!', 'Members of the group may now participate in the Presentation.', 'Close' );
			Social.presentationActive();
			Social.presentationStart( args );
		}
		if( webApp.userIsSpeaker() === true ) {
			$('#ipad_container').addClass( 'annotator' );
			navBar.fullscreenButton.disable();
			navBar.presentationButton.addClass( 'active' ).enable();
			navBar.annotatorButton.enable();
			navBar.editorActions.show();
			navBar.noteButton.disable();
			navBar.colorNoteButton.disable();
			navBar.enterFullscreen( false );
		}
	} );
}

function presentationEnd( depositionID ) {
	console.log( '-- send presentation end for Deposition: ' + depositionID );
	sio.emit( 'endPresentation', {"depositionID": depositionID}, function( status ) {
		console.log( 'endPresentation:', status );
		webApp.setS( 'joinPresentationData', null );
		Datacore.s.inPresentation = false;
		window.top.webApp.removeItem( 'inPresentation' );
		navBar.annotatorButton.removeClass( 'revoke' );
//		if( typeof fileSelection !== 'undefined' && fileSelection !== null ) {
//			fileSelection.disableFileSelection();
//		}
//		showPlainDialog1Btn( 'Presentation Ended!', 'The Presentation Session will be closed.', 'Close', 'PresentationEnded' );
		window.top.dialogTooltip.show( 'Presentation Ended!', 'The Presentation Session has closed.' );
		//window.top.showDialog3Btn( 'Start Presentation!', confirmMsg, dialogButtons, true );
		Datacore.s.presentationAvailable = false;
		if( typeof window.top.webApp !== 'undefined' ) {
			// Make sure the presenters toolbars get reset in case the witness has annotation ability when the presentation ended
			window.top.webApp.checkPresenterState();
		}
		if( typeof Social !== 'undefined' ) {
			Social.presentationEnd();
		}
		$('#ipad_container').removeClass( 'annotator' );
		if( typeof window.top.webApp !== 'undefined' ) {
			// Make sure the presenters toolbars get reset in case the witness has annotation ability when the presentation ended
			window.top.webApp.presentationStatus();
			window.top.webApp.closeOutPresenterView();
		}
	} );
}

function setPresentationFile( depositionID, fileID ) {
	//if(typeof window.top.webApp === "undefined" && !!window.top.webApp.getS("presentationPaused")) {
	//}
	console.log( '-- set presentation file Deposition:', depositionID, 'fileID:', fileID );
//	if( typeof window.top.webApp !== 'undefined' && window.top.webApp.viewerHasChanges() ) {
//		var dialogButtons = [];
//		dialogButtons.push( {label:"Don't Save", id:'no_save', class:'btn_app_extra'} );
//		dialogButtons.push( {label:'Cancel', action:'dismissDialog3Btn();', class:'btn_app_cancel'} );
//		dialogButtons.push( {label:'Save', id:'save', class:'btn_app_normal'} );
//		var confirmMsg = 'You may have changes to this document.  Are you sure you want to exit without saving?';
//		window.top.showDialog3Btn( 'Abort Changes?', confirmMsg, dialogButtons );
//		$('#dialog3Btn #no_save', window.top.document).on( "click", function() {
//			window.top.dismissDialog3Btn();
//			window.top.setPresentationFile( depositionID, fileID );
//		} );
//		$('#dialog3Btn #save', window.top.document).on( "click", function() {
//			window.top.dismissDialog3Btn();
//			window.top.webApp.saveChangesPrompt(function(){window.top.setPresentationFile( depositionID, fileID );});
//		} );
//	} else {
		var pTitle = '';
		var _file = Datacore.getFile( fileID );
		if( typeof _file !== 'undefined' && _file !== null && window.top.webApp.isExhibit(fileID) ) {
			pTitle = _file.name;
		}
		sio.emit( 'setPresentationFile', {"depositionID":depositionID, "fileID":fileID, "title":pTitle, "pageIndex":0, "pageOrientation":0, "pageOffsetX":0, "pageOffsetY":0, "pageScale":1}, function( status ) {
			console.log( 'setPresentationFile:', status );
			if( typeof status === 'object' && status !== null && typeof status.success !== 'undefined' && status.success === true ) {
//				var pData = webApp.getS( 'joinPresentationData' );
//				pData.fileID = fileID;
//				pData.title = pTitle;
//				pData.pageIndex = 0;
//				pData.pageOrientation = 0;
//				pData.pageOffsetX = 0;
//				pData.pageOffsetY = 0;
//				pData.pageScale = 1;
//				webApp.setS( 'joinPresentationData', pData );
//				navBar.setTitle( pTitle );
//				iPadContainerFrame.location.replace( '/depositions/presentation' );
			}
		} );
//	}
}

function initialAnnotation( depositionID, args ) {
	var dfd = jQuery.Deferred();
	console.log( '-- send initial annotation Deposition:', depositionID, 'args:', args );
	sio.emit( 'initialAnnotation', {"depositionID":depositionID, "eventData":args}, function( data ) {
		dfd.resolve('success');
	} );
	return dfd;
}

function addAnnotation( depositionID, args ) {
	var dfd = jQuery.Deferred();
	console.log( '-- send add annotation Deposition:', depositionID, 'args:', args );
	sio.emit( 'addAnnotation', {"depositionID":depositionID, "eventData":args}, function( data ) {
		dfd.resolve('success');
	} );
	return dfd;
}

function deleteAnnotation( depositionID, args ) {
	var dfd = jQuery.Deferred();
	console.log( '-- send delete annotation Deposition:', depositionID, 'args:', args );
	sio.emit( 'deleteAnnotation', {"depositionID":depositionID, "eventData":args}, function( data ) {
		dfd.resolve('success');
	} );
	return dfd;
}

function pageProperties( depositionID, args ) {
	console.log( '-- send page Properties Deposition:', depositionID, 'args:', args );
	sio.emit( 'setPageProperties', {"depositionID":depositionID, "eventData":args}, function( data ) {
//		console.log( data );
	} );
}

function getAnnotations( depositionID ) {
//	console.log( '-- send get Annotations Deposition: ' + depositionID );
	sio.emit( 'getAnnotations', {"depositionID":depositionID}, function( data ) {
//		console.log( 'getAnnotations:', data );
		if( window.top.webApp !== 'undefined' ) {
			window.top.webApp.presentationGetAnnotations( data );
		}
	} );
}

function setAnnotator( depositionID, args ) {
	console.log( '-- send set Annotator Deposition:', depositionID, 'args:', args );
	sio.emit( 'setAnnotator', {"depositionID":depositionID, "eventData":args}, function( data ) {
//		console.log( data );
		if( typeof data !== 'undefined' && data !== null && typeof data.success !== 'undefined' && !!data.success ) {
			if( args.userID === webApp.getS( 'user.ID' ) ) {
				//revoke annotator back to speaker
				if( typeof fileSelection !== 'undefined' && fileSelection !== null ) {
					fileSelection.enableFileSelection();
				}
			} else {
				//set witness as annotator
				if( typeof fileSelection !== 'undefined' && fileSelection !== null ) {
					fileSelection.disableFileSelection();
				}
			}
		}
	} );
}

function clearAll( depositionID ) {
	console.log( '-- send clearAll Deposition: ' + depositionID );
	sio.emit( 'clearAnnotations', {
		depositionID : depositionID
	}, function( data ){
		console.log( 'clearAll: ' + JSON.stringify( data ) );
		if( typeof Annotate !== 'undefined' ) {
			Annotate.presentationClearAll( data.annotationIndexes );
		}
	} );
}

function undoAnnotation( depositionID ) {
	console.log( '-- send undoAnnotation Deposition:', depositionID );
	sio.emit( 'annotationUndo', {
		depositionID : depositionID
	}, function( data ){
		console.log( 'undoAnnotation:', data );
		// if( typeof Annotate !== 'undefined' ) {
		// 	Annotate.updateUndoCounter( data );
		// }
	} );
}

function modifyAnnotation( depositionID, args ) {
	console.log( '-- send modify annotation Deposition: ' + depositionID + ' args:' + JSON.stringify( args ) );
	sio.emit( 'modifyAnnotation', {
		depositionID : depositionID,
		eventData : args,
	}, function( data ){
		console.log( 'modifyAnnotation: ' + JSON.stringify( data ) );
		//if( typeof Annotate !== 'undefined' ) {
		//	Annotate.updateUndoCounter( data );
		//}
	} );
}

function addCallout( depositionID, args ) {
	var dfd = jQuery.Deferred();
	console.log( '-- send add callout Deposition:', depositionID, 'args:', args );
	sio.emit( 'addCallout', {"depositionID":depositionID, "eventData":args}, function( data ) {
		dfd.resolve('success');
	} );
	return dfd;
}

function modifyCallout( depositionID, args ) {
	var dfd = jQuery.Deferred();
	console.log( '-- send modify callout Deposition: ' + depositionID + ' args:' + JSON.stringify( args ) );
	sio.emit( 'modifyCallout', {"depositionID":depositionID, "eventData":args}, function( data ){
		dfd.resolve('success');
	} );
	return dfd;
}

function deleteCallout( depositionID, args ) {
	var dfd = jQuery.Deferred();
	console.log( '-- send delete callout Deposition:', depositionID, 'args:', args );
	sio.emit( 'deleteCallout', {"depositionID":depositionID, "eventData":args}, function( data ) {
		dfd.resolve('success');
	} );
	return dfd;
}

function setCalloutZIndex( depositionID, args ) {
	var dfd = jQuery.Deferred();
	console.log( '-- send set callout z-index Deposition: ' + depositionID + ' args:' + JSON.stringify( args ) );
	sio.emit( 'setCalloutZIndex', {"depositionID":depositionID, "eventData":args}, function( data ){
		dfd.resolve('success');
	} );
	return dfd;
}

function getCallouts( depositionID ) {
	var dfd = jQuery.Deferred();
	console.log( '-- send get callouts Deposition: ' + depositionID );
	sio.emit( 'getCallouts', {"depositionID":depositionID}, function( data ){
		if( window.top.webApp !== 'undefined' ) {
			window.top.webApp.presentationGetCallouts( data );
		}
		dfd.resolve('success');
	} );
	return dfd;
}

function joinTranscript( depositionID )
{
	console.log( '-- join Transcript Deposition: ' + depositionID );
	sio.emit( 'joinTranscript', {depositionID:depositionID}, function( data ) {
		console.log( 'joinTranscript: ' + JSON.stringify( data ) );
		if( data.success ) {
			if( typeof liveTranscript !== 'undefined'  && liveTranscript !== null ) {
				liveTranscript.prepareFromHistory( data.transcriptHistory );
			}
		}
	} );
}

function witness_authorization()
{
	console.log( 'witness_authorization ...' );
	var witnessName = $('#witnessName').val();
	sio.emit( 'witnessLogin', {depositionID:Datacore.s.witnessDepositionID, witnessID:Datacore.s.currentUserID, name:witnessName}, function( result ) {
		if( typeof result === 'object' && result.hasOwnProperty( 'success' ) ) {
			if( result.success ) {
				console.log( 'waiting for authorization...' );
				setDialog1BtnCloseAction( function() { $('#dialog1BtnClose').removeClass('btn_app_extra'); window.location.replace( '/signin' );  } );
				showDialog1Btn( 'Authorizing', 'Waiting for the Taking Attorney to confirm.', 'Cancel' );
				$('#dialog1BtnClose').addClass('btn_app_extra');
			} else {
				console.log( 'witness authorization failed' );
				if( result.hasOwnProperty( 'error' ) ) {
					setDialog1BtnCloseAction( function() { window.location.replace( '/signin' ); } );
					showDialog1Btn( 'Error', result.error, 'Close' );
				}
			}
		}
	} );
}
