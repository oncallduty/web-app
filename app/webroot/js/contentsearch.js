/**
 * Content Search
 * @param {String} listType Session|Slideout
 * @param {jQuery} containerElement
 * @param {jQuery} panel
 * @param {jQuery} list
 * @param {jQuery} listController
 * @returns {ContentSearch}
 */
function ContentSearch( listType, containerElement, panel, list, listController )
{
	var type = listType;
	var container = containerElement;
	var tablesPanel = panel;
	var filesList = list;
	var listController = listController;
	var searching = false;
	var inSearch = false;
	var files = [];
	var searchTerms = [];
	var selectedFiles = {};
	var searchOptions = {};
	var oldSearchOptions = {};
	var userType = window.top.webApp.getS( 'user.userType' );
	if( window.top.webApp.hasItem( 'csOptions' ) ) {
		var _options = window.top.webApp.getItem( 'csOptions' );
		searchOptions = JSON.parse( _options );
	} else {
		searchOptions = {
			"searchAllDocs": ( userType === 'W' || userType === 'G' ) ? false : true,
			"searchContent": true,
			"includeFilename": false
		};
	}
	var bind = this;

	var containerHTML = '<div class="search_bar_container"><input type="text" id="content_search" placeholder="Search"/><div class="clear_button""></div></div><div id="done_search" class="csDoneBtn">Done</div><div title="Configure your search options" class="configure_search_icon"></div>';

	container.html(containerHTML);

	var csPopupHTML = '<div class="cs_popup_container"><div class="shadow"></div><div class="cs_popup">';
	if( typeof window.top.csPopup === 'undefined' ) {
		var t = $('.cs_popup_container', window.top.document);
		if( t.length > 0 ) {
			window.top.csPopup = t;
		} else {
			if( userType !== 'W' && userType !== 'G' ) {
				csPopupHTML += '<div class="cs_popup_options top"><div class="cs_option_row allDocs"><div class="radio"></div><div class="option_text">All Documents</div></div><div class="cs_option_row folder"><div class="radio"></div><div class="option_text"></div></div></div>';
			}
			csPopupHTML += '<div class="cs_popup_options bottom"><div class="cs_option_row contentSearch"><div class="checkBox"></div><div class="option_text">Document Content</div></div><div class="cs_option_row docName"><div class="checkBox"></div><div class="option_text">Document Name</div></div><div class="cs_btn_row"><div id="apply_options" class="csDoneBtn">Apply</div></div></div></div>';
			window.top.iPadContainer.append( csPopupHTML );
			window.top.csPopup = $('.cs_popup_container', window.top.document);
		}
	}

	var popup = $('.cs_popup', window.top.csPopup);
	var searchIcon = $('.configure_search_icon', container);
	var shadow = $('.shadow', window.top.csPopup);
	var applyOptionsBtn = $('#apply_options', window.top.csPopup);
	var input = $('input', container);
	var doneSearchBtn = $('#done_search', container);
	var clearBtn = $('.clear_button', container);
	var contentSearchRow = $('.cs_option_row.contentSearch', window.top.csPopup);
	var contentSearchBtn = $('.cs_option_row.contentSearch .checkBox', window.top.csPopup);
	var docNameRow = $('.cs_option_row.docName', window.top.csPopup);
	var docNameBtn = $('.cs_option_row.docName .checkBox', window.top.csPopup);
	var allDocsRow = $('.cs_option_row.allDocs', window.top.csPopup);
	var allDocsBtn = $('.cs_option_row.allDocs .radio', window.top.csPopup);
	var folderRow = $('.cs_option_row.folder', window.top.csPopup);
	var folderBtn = $('.cs_option_row.folder .radio', window.top.csPopup);

	var showDoneBtn = function( skipAnimation ) {
		var duration = ( skipAnimation ) ? 0 : 200;
		input.switchClass( 'inactive', 'active', {"duration":duration, "easing":"linear"} );
		doneSearchBtn.show( {"effect":"fade", "duration":duration, "easing":"easeInQuart"} );
	};

	var showOptions = function() {
		oldSearchOptions = {
			"searchAllDocs": searchOptions.searchAllDocs,
			"searchContent": searchOptions.searchContent,
			"includeFilename": searchOptions.includeFilename
		};
		var folderName = 'Folder: ';
		var folders = window.top.Datacore.getFolders();
		var selectedFolderID = window.top.webApp.getItem( 'selectedFolderID' );
		for( var f in folders ) {
			if( folders[f].ID === selectedFolderID ) {
				folderName += folders[f].name;
				break;
			}
		}
		var folderText = $('.cs_option_row.folder .option_text', window.top.csPopup);
		folderText.text(folderName);
		window.top.csPopup.show();

		var topPos;
		var leftPos;
		var tRect = searchIcon[0].getBoundingClientRect();
		var pHeight = popup.height();
		var pWidth = popup.width();
		if( !$.contains( window.top.iPadContainer[0], searchIcon[0] ) ) {
			popup.css( 'position', 'absolute' );
			var navBar = window.top.webApp.getS( 'NavBar' );
			var nRect = navBar[0].getBoundingClientRect();
			topPos = Math.round( (nRect.height + tRect.top + (tRect.height / 2)) - (pHeight / 2) ) + 'px';
			leftPos = Math.round( tRect.left - pWidth ) + 'px';
		} else {
			popup.css( 'position', 'fixed' );
			topPos = Math.round( tRect.top + (tRect.height /2) - (pHeight / 2) ) + 'px';
			leftPos = Math.round( tRect.left - pWidth )  + 'px';
		}
		popup.css( 'left', leftPos );
		popup.css( 'top', topPos );
	};

	var closeCSPopup = function() {
		window.top.csPopup.hide();
		window.top.webApp.setItem( 'csOptions', JSON.stringify( searchOptions ) );

		if( inSearch && (searchOptions.searchAllDocs !== oldSearchOptions.searchAllDocs || searchOptions.searchContent !== oldSearchOptions.searchContent || searchOptions.includeFilename !== oldSearchOptions.includeFilename) ) {
			search();
		}
		oldSearchOptions = {};
	};

	var view_file = function( fileID, mimeType )
	{
		window.top.webApp.setItem( 'selectedFileID', fileID );
		if( mimeType.indexOf( "audio" ) > -1 ) {
			window.top.iPadContainerFrame.location.replace('/depositions/audio?ID=' + fileID);
		}
		else if( mimeType.indexOf( "video" ) > -1 ) {
			window.top.iPadContainerFrame.location.replace('/depositions/video?ID=' + fileID);
		}
		else {
			window.top.webApp.setItem( 'contentSearchTerms', JSON.stringify( searchTerms ) );
			window.top.iPadContainerFrame.location.replace('/depositions/pdf?ID=' + fileID);
		}
	};

	this.buildFileList = function()
	{
		var filesListMinLength = 10;
		abortSelectFiles( true );
		filesList.empty();
		var userType = window.top.webApp.getS( 'user.userType' );
		var blockExhibits = !!window.top.webApp.getS( 'user.blockExhibits' );
		var legendText = (userType === 'W') ? 'Exhibits' : 'Documents';
		$('.filesPanel .legend', tablesPanel).text( legendText + ' (0)' );

		filesList.addClass( 'loading' );
		tablesPanel.addClass( 'contentSearch' );

		if( files.length >= 1 ) {
			$('#sendFolder, #saveFolder').removeClass( 'inactive' );
		} else {
			$('#sendFolder, #saveFolder').addClass( 'inactive' );
		}

		if( !blockExhibits ) {
			var clientTimezoneOffset = new Date().getTimezoneOffset();
			for( var f in files ) {
				var File = files[f];

				// Custom second line for members and the folder is not exhibit.
				var secondLine;
				if( userType === 'M' || userType === 'WM' ) {
					var exhibitHistoryList = window.top.Datacore.getExhibitsBySourceFileID( File.ID );
					if( exhibitHistoryList.length === 0 ) {
						secondLine = '<p class="name">&nbsp;</p>';
					} else {
						exhibitHistoryList = exhibitHistoryList.reverse();
						var first = true;
						secondLine = '<p class="exhibitHistory">';
						for( var i in exhibitHistoryList ) {
							var exHistory = exhibitHistoryList[i];
							var srcFile = window.top.Datacore.getFile( exHistory.exhibitFileID );
							if( typeof srcFile !== 'undefined' && srcFile !== null ) {
								if( first ) {
									first = false;
									secondLine += 'As Exhibit: ';
								} else {
									secondLine += ', ';
								}
								secondLine += srcFile.name;
							}
						}
						secondLine += '&nbsp;</p>';
					}
				} else if( userType === 'G' ) {
					secondLine = '<p class="name">&nbsp;</p>';
				} else {
					var exhibitHistory = window.top.Datacore.getExhibitByExhibitFileID( File.ID );
					var localTime;
					var name;
					if( exhibitHistory === null ) {
						secondLine = '<p class="name">&nbsp;</p>';
					} else {
						localTime = new Date( exhibitHistory.introducedDate );
						name = exhibitHistory.introducedBy;
						localTime.setMinutes( localTime.getMinutes() - clientTimezoneOffset );
						var hours = localTime.getHours();
						var ampm = "AM";
						if( hours >= 12 ) {
							hours -= 12;
							ampm = "PM";
						}
						if( hours === 0 ) {
							hours = 12;
						}
						var minutes = ('0' + localTime.getMinutes()).slice( -2 );
						var seconds = ('0' + localTime.getSeconds()).slice( -2 );

						secondLine = '<p class="exhibitHistory">'+((localTime.getMonth()+1) + '/' + localTime.getDate() + '/' + localTime.getFullYear()) + ' ' + hours + ':' + minutes + ':' + seconds + ' ' + ampm + ' By: '+ name + '</p>';
					}
				}

				var fileName = File.name;
//				if( searchOptions.includeFilename )	{
//					fileName = highlightFileName( fileName );
//				}
				var li = $( '<li class="sortable"></li>' );
				li.attr( 'data-id', File.ID );

				var dragHandle = $( '<div class="dragBox"><div class="handle"></div></div>' );
				var folderInfo = $( '<div class="folderInfo">In Folder: ' + File.folderName + '</div>' );
				var rank = $( '<div class="rank">' + (Math.round(File.score * 100)) + '</div>' );
				li.append( dragHandle );

				if( type === 'Session' ) {
					li.on( 'click', {file:File}, function( event ) {
						window.top.webApp.setItem( 'viewFile', JSON.stringify( event.data.file ) );
						view_file( event.data.file.ID, event.data.file.mimeType );
					});
					li.attr( 'id', 'file_item_'+File.ID );
					li.attr( 'data-mimetype', File.mimeType );
					var selectDiv = $( '<div class="fileSelect"><div class="checkBtn"></div></div>' );
					var file_info = $( '<div class="file_info"><p class="fileName">' + fileName + '</p> ' + secondLine + '</div>' );
					li.append( selectDiv );
					li.append( file_info );
				} else {
					li.on( 'click', {file:File}, function( event ) {
						if( window.top.Datacore.s.inPresentation && event.data.file.mimeType !== 'application/pdf' ) {
							window.top.showDialog3Btn( "Abort Presentation?", "Presentation Mode does not support multimedia files. Opening this multimedia file will end the presentation.", [
								{label: "Cancel", class: 'btn_app_cancel', action: function(){window.top.fileSelection.closeFileSelection();window.top.dismissDialog3Btn();}},
								{label: "Continue", class: 'btn_app_normal', action: function(){window.top.fileSelection.closeFileSelection();window.top.dismissDialog3Btn();window.top.webApp.closeOutPrevent(true);window.top.webApp.viewFile(File.ID);}}
							]);
						} else {
							if (!window.top.Datacore.s.inPresentation) {
								window.top.webApp.setItem( 'viewFile', JSON.stringify( event.data.file ) );
							}
							window.top.webApp.viewFile(event.data.file.ID);
							window.top.webApp.setItem( 'selectedFileID', event.data.file.ID );
							window.top.fileSelection.toggleFileSelection();
						}
					});
					var label = $( '<div class="fileLabel"></div>' );
					label.append( File.name );
					li.append( label );
					li.append( secondLine );
				}
				li.append( folderInfo );
				li.append( rank );
				filesList.append( li );
			}

			var listLength = $('li', filesList).length;
			$('.filesPanel .legend', tablesPanel).text( legendText + ' (' + listLength + ')' );
			if( listLength < filesListMinLength ) {
				for( var i = (filesListMinLength - listLength); i > 0; --i ) {
					filesList.append( '<li></li>' );
				}
			}
		}
		filesList.removeClass( 'loading' );
		if( type === 'Session' ) {
			var okayBtn = $('.folderActions .okayBtn', tablesPanel);
			var cancelBtn = $('.folderActions .cancelBtn', tablesPanel);
			var emailBtn = $('.folderActions .emailBtn', tablesPanel);
			var downloadBtn = $('.folderActions .downloadBtn', tablesPanel);
			var sendPortalInviteBtn = $('.folderActions .sendPortalInviteBtn', tablesPanel);

			okayBtn.off().on( 'click', sendFiles );
			cancelBtn.off().on( 'click', function() { abortSelectFiles( true ); } );
			downloadBtn.off().addClass('disabled');
			sendPortalInviteBtn.off().hide();

			if( files.length > 0 && userType !== 'W' ) {
				emailBtn.removeClass('disabled');
				emailBtn.on( 'click', emailSelectFiles );
			} else {
				emailBtn.off().addClass('disabled');
			}

			if( userType === 'W' || userType === 'WM' ) {
				emailBtn.off().hide();
				if ( blockExhibits ) {
					$('#witnessBlock').show();
				} else {
					$('#witnessBlock').hide();
				}
			}
		}

		if( userType === 'W' ) {
			$('#sortCSFiles', tablesPanel).hide();
		} else {
			$('#sortCSFiles', tablesPanel).show();
		}

		highlightSearchTerms();
	};

	var highlightSearchTerms = function() {
		if( !!searchOptions.includeFilename ) {
//			var context = document.querySelector('#filesList');
			var instance = new Mark(filesList);
//			console.log(searchTerms);
			instance.mark(searchTerms, {"accuracy": "partially", "separateWordSearch": true});
		}
	};

	var search = function() {
		var searchTerm = input.val();
		if( searchTerm === '' ) return;
		searching = true;
		setCancelSearchFunction( function() {
			searching = false;
			clearBtn.trigger( 'click' );
		});
		showSearchDialog( 'Searching Documents', 'Press Cancel to end the search before it completes.', 'Cancel' );
		var folderID = ( !searchOptions.searchAllDocs ) ? window.top.webApp.getItem( 'selectedFolderID' ) : '0';
		var sessionID = window.top.webApp.getS( 'deposition.ID' );
		// Something casts the bools true and false to string "true" and "false" during post (which PHP then casts both to bool true)
		// So we add 0 to cast bools to string "1" or "0" (which PHP then correctly casts to bool true or false)
		var options = {
			"searchContent": (searchOptions.searchContent + 0),
			"includeFilename": (searchOptions.includeFilename + 0),
			"folderID": folderID
		};
		$.post("/depositions/search",{"terms":searchTerm,"sessionID":sessionID,"options":options}, function( data, textStatus ) {
			dismissSearchDialog();
			changeBtnToDone();
			if( searching ) {
				if( textStatus !== "success" && data.search_result === null ) {
					dialogTooltip.show( 'Server Error!', 'Unknown error occurred', 400 );
					files = [];
				} else {
					if( data.search_result.total === 0 ) {
						files = [];
					} else {
						getFilesFromHits( data.search_result.hits );
					}
					if( files.length === 0 ) {
						dialogTooltip.show( 'Nothing Found!', 'Nothing was found for ' + searchTerm, 400 );
					}
				}
				searching = false;
				inSearch = true;
				searchTerms = data.search_result.searchedTerms;
				bind.buildFileList();
			}
		}, "json");
	};

	var endSearch = function() {
		searching = false;
		input.val('');
		files = [];
		searchTerms = [];
		window.top.searchValues = null;
		clearBtn.hide();
		tablesPanel.removeClass( 'contentSearch' );
		$('.folderActions .emailBtn', tablesPanel).removeClass('disabled');
		if( inSearch ) {
			listController.selectFolder( window.top.webApp.getItem( 'selectedFolderID' ) );
			inSearch = false;
		}
		input.switchClass( 'active', 'inactive', {"duration":200, "easing":"linear"} );
		doneSearchBtn.hide( {"effect":"fade", "duration":200, "easing":"easeOutQuart"} );
	};

	var changeBtnToDone = function() {
		doneSearchBtn.text( 'Done' );
		doneSearchBtn.off().on( 'click', function() {
			endSearch();
		});
	};

	var changeBtnToSearch = function() {
		doneSearchBtn.text( 'Search' );
		doneSearchBtn.off().on( 'click', function() {
			search();
		});
	};

	var getFilesFromHits = function( hits ) {
		files = [];
		for ( var i in hits ) {
			var hit = hits[i];
			var File = window.top.Datacore.getFile( hit.fileID );
			if( !File ) {
				console.log('search returned file id that is not in datacore');
				continue;
			}
			var _file = {
				"ID": File.ID,
				"folderID": File.folderID,
				"name": File.name,
				"created": File.created,
				"createdBy": File.createdBy,
				"isExhibit": File.isExhibit,
				"isPrivate": File.isPrivate,
				"mimeType": File.mimeType,
				"sortPos": File.sortPos,
				"byUser": {
					"ID" : File.byUser.ID,
					"firstName" : File.byUser.firstName,
					"lastName" : File.byUser.lastName,
					"email" : File.byUser.email
				},
				"score": hit._score,
				"folderName": hit.folderName
			};
			files.push( _file );
		}
	};

	var emailSelectFiles = function()
	{
		filesList.addClass( 'selectEdit' );
		addSelectFileClickToRows();
		selectedFiles = {};
		$('li.sortable', filesList).each(function() {
			var fileID = $(this).attr('data-id');
			selectedFiles[fileID] = parseInt(fileID);
		});
		$('.fileSelect', filesList).addClass( 'selected' );
		$('.folderActions .actionBtns', tablesPanel).show();
	};

	var abortSelectFiles = function( skipBuild )
	{
		skipBuild = !!skipBuild;
		$('.folderActions .actionBtns', tablesPanel).hide();
		filesList.removeClass( 'selectEdit' );
		if( !skipBuild ) {
			bind.buildFileList();
		}
	};

	var sendFiles = function()
	{
		if( Object.keys(selectedFiles).length < 1 ) {
			window.top.showTooltip( 'Please select one or more files.', true );
			return;
		}
		var selectedFileIDs = [];
		var visibleFiles = [];
		// only email files that are selected and that are visible (not filtered out with document name search)
		$('li.sortable', filesList).each(function() {
			var fileID = $(this).attr('data-id');
			visibleFiles[fileID] = parseInt(fileID);
		});
		for( var fID in selectedFiles ) {
			if( selectedFiles.hasOwnProperty( fID ) && visibleFiles.hasOwnProperty( fID )) {
				selectedFileIDs.push( selectedFiles[fID] );
			}
		};
		window.top.sendDialog.setFileIDs( selectedFileIDs );
		window.top.sendDialog.show();
		abortSelectFiles();
	};

	var addSelectFileClickToRows =  function()
	{
		var items = $('li', filesList);
		items.each( function(i) {
			$(this).off().on( 'click', selectFile );
		});
		$('.file_info', items).removeAttr( 'draggable' );
	};

	var selectFile = function( event ) {
		console.log( "ContentSearch.selectFile;" );
		if (event && event.stopPropagation) {
			event.stopPropagation();
		}
		else if (window.event) {
			window.event.cancelBubble = true;
		}
		var target = $( event.currentTarget );
		var fileID = parseInt( target.attr( 'data-id' ) );
		if( isNaN( fileID ) ) {
			console.warn( "ContentSearch.selectFile; invalid fileID:", fileID );
			return;
		}
		var hasFile = false;
		for( var f in files ) {
			if( files[f].ID === fileID ) {
				hasFile = true;
				break;
			}
		}
		if( !hasFile ) {
			console.warn( "ContentSearch.selectFile; missing fileID:", fileID );
			return;
		}
		if( typeof selectedFiles[fileID] !== 'undefined' ) {
			delete selectedFiles[fileID];
			$('.fileSelect', target).removeClass( 'selected' );
		} else {
			selectedFiles[fileID] = fileID;
			$('.fileSelect', target).addClass( 'selected' );
		}
	};

	var sortFiles = function()
	{
		files = searchSort.sortList( files );
		bind.buildFileList();
	};

	this.getInSearch = function() {
		return inSearch;
	};

	this.storeSearchValues = function() {
		var searchValues = {
			"searchTerms": searchTerms,
			"files": files,
			"sort": searchSort.getSortValues()
		};
		window.top.searchValues = searchValues;
	};

	// SET CLICK HANDLERS
	searchIcon.off().on( 'click', showOptions );
	shadow.off().on( 'click', closeCSPopup );
	applyOptionsBtn.off().on( 'click', closeCSPopup );
	doneSearchBtn.off().on( 'click', endSearch );
	input.off().on( 'click', function() {
		showDoneBtn( false );
	});
	input.keyup( function( event ) {
		if(event.keyCode == 13) {
			search();
		} else {
			var searchTerm = input.val();
			if( searchTerm !== '' ) {
				changeBtnToSearch();
				clearBtn.show();
			} else {
				changeBtnToDone();
				clearBtn.hide();
			}
		}
	});
	clearBtn.off().on( 'click', function() {
		input.val('');
		input.focus();
		changeBtnToDone();
		clearBtn.hide();
	});
	contentSearchRow.off().on( 'click', function() {
		if( searchOptions.searchContent === true && searchOptions.includeFilename === true ) {
			searchOptions.searchContent = false;
			contentSearchBtn.removeClass('checked');
		} else {
			searchOptions.searchContent = true;
			contentSearchBtn.addClass('checked');
		}
	});
	docNameRow.off().on( 'click', function() {
		if( searchOptions.includeFilename === true ) {
			searchOptions.includeFilename = false;
			docNameBtn.removeClass('checked');
			if( searchOptions.searchContent === false ) {
				searchOptions.searchContent = true;
				contentSearchBtn.addClass('checked');
			}
		} else {
			searchOptions.includeFilename = true;
			docNameBtn.addClass('checked');
		}
	});
	if( userType !== 'W' && userType !== 'G' ) {
		allDocsRow.off().on( 'click', function() {
			allDocsBtn.addClass('checked');
			folderBtn.removeClass('checked');
			searchOptions.searchAllDocs = true;
		});
		folderRow.off().on( 'click', function() {
			folderBtn.addClass('checked');
			allDocsBtn.removeClass('checked');
			searchOptions.searchAllDocs = false;
		});
	}

	// SET UP UI
	if( userType !== 'W' && userType !== 'G' ) {
		if( searchOptions.searchAllDocs ) {
			allDocsBtn.addClass('checked');
			folderBtn.removeClass('checked');
		} else {
			allDocsBtn.removeClass('checked');
			folderBtn.addClass('checked');
		}
	}
	if( searchOptions.searchContent ) {
		contentSearchBtn.addClass('checked');
	} else {
		contentSearchBtn.removeClass('checked');
	}
	if( searchOptions.includeFilename ) {
		docNameBtn.addClass('checked');
	} else {
		docNameBtn.removeClass('checked');
	}

	// LOAD SEARCH STATE IF NEEDED
	var searchValues = window.top.searchValues;
	var sortValues = null;
	if( searchValues !== null && typeof searchValues !== 'undefined' ) {
		searchTerms = searchValues.searchTerms;
		var searchTermsString = searchTerms.join( ' ' );
		input.val(searchTermsString);
		files = searchValues.files;
		inSearch = true;
		sortValues = searchValues.sort;
		showDoneBtn( true );
		clearBtn.show();
		bind.buildFileList();
		if( files.length === 0 ) {
			dialogTooltip.show( 'Nothing Found!', 'Nothing was found for ' + searchTermsString, 400 );
		}
	}

	var searchSort = new window.top.ContentSearchSort( tablesPanel, sortFiles, this, sortValues );
};

function ContentSearchSort( panel, sortCallback, contentSearchController, sortValues )
{
	var userType = window.top.webApp.getS( 'user.userType' );
	var memberOptions = ['Name','Date','Custom','Rank'];
	var nonmemberOptions = ['Name','Date','Rank'];
	var options = (userType === 'M') ? memberOptions : nonmemberOptions;
	var sortFields = {"Name":"name", "Date":"created", "Custom":"sortPos", "Rank":"score"}
	var sortBy = 'Rank';
	var sortOrder = 'Desc';
	if( sortValues !== null && typeof sortValues !== 'undefined' ) {
		sortBy = sortValues.sortBy;
		sortOrder = sortValues.sortOrder;
	}

	var tablesPanel = panel;
	var sortHeader = $('#sortCSFiles', tablesPanel);
	sortHeader.html( '<div class="sortKey"></div><div class="doneButton">Done</div><div class="sortArrow up"></div>' );

	var sortKey = $('.sortKey', sortHeader);
	sortKey.off().on( 'click', function() { showMenu( options ); } );

	var sortArrow = $('.sortArrow', sortHeader);
	sortArrow.off().on( 'click', function() {
		sortOrder = (sortOrder === 'Asc') ? 'Desc' : 'Asc';
		if( sortOrder === 'Desc' ) {
			sortArrow.removeClass( 'up' );
		} else {
			sortArrow.addClass( 'up' );
		}
		if( sortCallback && typeof sortCallback === 'function' ) {
			sortCallback();
		}
	} );

	if( typeof window.top.csSortMenu === 'undefined' ) {
		var t = $('.csSortMenu', window.top.document);
		if( t.length > 0 ) {
			window.top.csSortMenu = t;
		} else {
			window.top.iPadContainer.append( '<div class="csSortMenu" style="display:none;"><div class="shadow"></div><div class="sortMenuWrap"><div class="marker"></div><ul></ul></div></div>' );
			window.top.csSortMenu = $('.csSortMenu', window.top.document);
		}
	}

	var menuList = $('ul', window.top.csSortMenu);
	var menuWrap = $('.sortMenuWrap', window.top.csSortMenu);
	$('.shadow', window.top.csSortMenu).off().on( 'click', function() { window.top.csSortMenu.hide(); } );

	sortKey.text( sortBy );
	sortKey.attr( 'title', 'Sort By: ' + sortBy );
	if( sortOrder === 'Desc' ) {
		sortArrow.removeClass( 'up' );
	} else {
		sortArrow.addClass( 'up' );
	}

	var showMenu = function( items ) {
		var lineItemHTML = '<li><div class="check"></div></li>';
		var liLabelHTML = '<div class="label"></div>';
		var subMenuHTML = '<div class="submenu"></div>';
		menuList.empty();
		for( var i in items ) {
			var item = items[i];
			var li = $( lineItemHTML );
			var liLabel = $( liLabelHTML );
			var itemLabel = item;
			var isBranch = false;
			if( typeof item === 'object' ) {
				itemLabel = Object.keys( item )[0];
				isBranch = true;
			}
			liLabel.attr( 'data-sortby', itemLabel );
			liLabel.text( itemLabel );
			if( !isBranch ) {
				liLabel.off().on( 'click', {"sortBy":item}, function( event ) { selectOption( event.data.sortBy ); } );
			} else {
				liLabel.off().on( 'click', {"items":item[itemLabel]}, function( event ) { showMenu( event.data.items ); } );
			}
			li.append( liLabel );
			if( isBranch ) {
				li.append( subMenuHTML );
			}
			if( item === 'Custom' ) {
				continue;
			}
			if( sortBy === item || sortBy.indexOf( itemLabel ) === 0 ) {
				$('.check', li).addClass( 'selected' );
			}
			menuList.append( li );
		}
		window.top.csSortMenu.css( 'visibility', 'hidden' );
		window.top.csSortMenu.show();
		var topPos;
		var leftPos;
		var tRect = sortKey[0].getBoundingClientRect();
		var mHeight = menuWrap.height();
		if( !$.contains( window.top.iPadContainer[0], sortKey[0] ) ) {
			menuWrap.css( 'position', 'absolute' );
			var navBar = window.top.webApp.getS( 'NavBar' );
			var nRect = navBar[0].getBoundingClientRect();
			topPos = Math.round( (nRect.height + tRect.top + (tRect.height / 2)) - (mHeight / 2) ) + 'px';
			leftPos = Math.round( tRect.left + tRect.width ) + 'px';
		} else {
			menuWrap.css( 'position', 'fixed' );
			topPos = Math.round( tRect.top + (tRect.height /2) - (mHeight / 2) ) + 'px';
			leftPos = Math.round( tRect.left + tRect.width ) + 'px';
		}
		menuWrap.css( 'left', leftPos );
		menuWrap.css( 'top', topPos );
		window.top.csSortMenu.hide();
		window.top.csSortMenu.css( 'visibility', '' );	//remove css property
		window.top.csSortMenu.show();
	};

	var selectOption = function( option ) {
//			console.log( 'selectOption', option );
		var isValid = false;
		if( options.indexOf( option ) >= 0 ) {
			isValid = true;
		} else {
			for( var o in options ) {
				if( typeof options[o] !== 'object' ) {
					continue;
				}
				var first = Object.keys( options[o] )[0];
				if( options[o][first].indexOf( option ) >= 0 ) {
					isValid = true;
					break;
				}
			}
		}
		if( isValid ) {
			sortBy = option;
			sortKey.text( sortBy );
			sortKey.attr( 'title', 'Sort By: ' + sortBy );
			if( sortBy === 'Custom' ) {
				sortOrder = 'Asc';
				sortArrow.addClass( 'up' );
				sortArrow.hide();
			} else {
				sortArrow.show();
			}
		}
		if( sortCallback && typeof sortCallback === 'function' ) {
			sortCallback();
		}
		window.top.csSortMenu.hide();
	};

	var _sort = function( a, b ) {
		if( typeof a === 'undefined' || typeof b === 'undefined' || a === null || b === null ) {
//			console.log( a, b );
			return 0;
		}
		var sortField = 'ID';
		var aField;
		var bField;
		if( typeof sortFields[sortBy] !== 'undefined' ) {
			sortField = sortFields[sortBy];
		}
		aField = a[sortField];
		bField = b[sortField];
		if( aField > bField ) {
			return 1;
		} else if( aField < bField ) {
			return -1;
		} else {
			if( a.ID > b.ID ) {
				return 1;
			} else if( a.ID < b.ID ) {
				return -1;
			}
			return 0;
		}
	};

	this.sortList = function( source ) {
		if( typeof source !== 'object' || source === null ) {
			return;
		}
		var sortField = 'ID';
		if( typeof sortFields[sortBy] !== 'undefined' ) {
			sortField = sortFields[sortBy];
		}

		var result = ( sortField === 'name' ) ? naturalSort(source, sortField) : source.sort( _sort );
		if( sortOrder === 'Desc' ) {
			return result.reverse();
		}
		return result;
	};

	function naturalSort(list, sortField) {
		var a, b, a1, b1, rx=/(\d+)|(\D+)/g, rd=/\d+/;
		var aField;
		var bField;

		return list.sort( function(as, bs) {
			aField = as[sortField];
			bField = bs[sortField];
			a = String(aField).toLowerCase().match(rx);
			b = String(bField).toLowerCase().match(rx);
			while (a.length && b.length) {
				a1 = a.shift();
				b1 = b.shift();
				if (rd.test(a1) || rd.test(b1)) {
					if (!rd.test(a1)) return 1;
					if (!rd.test(b1)) return -1;
					if (a1 != b1) return a1-b1;
				}
				else if (a1 != b1) return a1 > b1 ? 1 : -1;
			}
			return a.length - b.length;
		});
	};

	this.getSortValues = function() {
		return {sortBy: sortBy, sortOrder: sortOrder};
	};
};