CanvasRenderingContext2D.prototype.dashedLine = function (x1, y1, x2, y2, dashLen) {
	//From stack overflow.  Remove when all browsers support dashed lines.
    if (dashLen == undefined) dashLen = 2;
    this.moveTo(x1, y1);

    var dX = x2 - x1;
    var dY = y2 - y1;
    var dashes = Math.floor(Math.sqrt(dX * dX + dY * dY) / dashLen);
    var dashX = dX / dashes;
    var dashY = dY / dashes;

    var q = 0;
    while (q++ < dashes) {
        x1 += dashX;
        y1 += dashY;
        this[q % 2 == 0 ? 'moveTo' : 'lineTo'](x1, y1);
    }
    this[q % 2 == 0 ? 'moveTo' : 'lineTo'](x2, y2);
};

function DrawObject(hitThreshold) {
	this.drawType = null;	// "pencil", "highlight", "marker", "arrow", or "note";
	this.page = null;		// Which canvas this object should be drawn on.  This will be the html ID of the canvas.
	this.color = "black";	// The color this object should be drawn with.
	this.lineCount = 0;		// Current array count in the pointList.
	this.pointLists = [];	// For lines, this is an array of lines that have an array of points.  For rects, this is an array of rects stored as upper left hand corner with with and height.
	this.boundingBox = [];	// Upper left and lower right corners for the bounding box.
	this.lineWidth = 4;		// How wide is the pencil.
	this.date = null;		// The date of the annotation.
	this.note = null;		// The note text.
	this.author = null;		// Shown on notes.
	this.index = 0;			// Index for sharing in presentations.
	this.opacity = 1;		// From 0 to 1.
	this.isSelected = false;// Is the object currently selected.
	this.dragPoints = [];	// Drag points for some objects.
	this.isSegmented = 0;	// 0:non-segmented,1:first segment,2:additional segment,3:end of segments

	if (typeof hitThreshold !== "number" || isNaN(hitThreshold)) {
		this.hitThreshold = 0;
	} else {
		this.hitThreshold = hitThreshold;
	}

	this.setDrawType = function( drawType, lineWidth ) {
		if (drawType === "pencil" || drawType === "highlight" || drawType === "marker" || drawType === "arrow" || drawType === 'note') {
			this.drawType = drawType;
		} else {
			console.log("Bad drawType: "+drawType);
		}
		if (isset( lineWidth )) {
			this.lineWidth = lineWidth;
		}
	}

	this.addObject = function() {
		this.pointLists[this.lineCount] = [];
		this.lineCount++;
	}

	this.addPoints = function( x, y, w, h ) {
		if (!isset( w ) && !isset( h ))  {
			this.pointLists[this.lineCount - 1].push([x, y]);
		} else {
			this.pointLists[this.lineCount - 1].push([x, y, w, h]);
		}
	}

	this.createBoundingBox = function() {
		if (this.drawType === "pencil" || this.drawType === "arrow" || this.drawType === "marker") {
			this.createLineBBox();
		} else if (this.drawType === "highlight") {
			this.createRectBBox();
		} else if (this.drawType === "note") {
			this.createNoteBBox();
		}
	}

	this.createLineBBox = function() {
		var x1, x2, y1, y2;
		x1 = x2 = this.pointLists[0][0][0];
		y1 = y2 = this.pointLists[0][0][1];

		var i = 0, j = 0;
		for(i = 0; i < this.pointLists.length; i++) {
			for(j = 0; j < this.pointLists[i].length; j++) {
				x1 = Math.min(x1, this.pointLists[i][j][0]);
				y1 = Math.min(y1, this.pointLists[i][j][1]);
				x2 = Math.max(x2, this.pointLists[i][j][0]);
				y2 = Math.max(y2, this.pointLists[i][j][1]);
			}
		}
		this.storeBoundingBox(x1, y1, x2, y2);
	}

	this.createRectBBox = function() {
		var x1, x2, y1, y2;
		x1 = x2 = this.pointLists[0][0][0];
		y1 = y2 = this.pointLists[0][0][1];

		var i = 0, j = 0;
		for(i = 0; i < this.pointLists.length; i++) {
			for(j = 0; j < this.pointLists[i].length; j++) {
				x1 = Math.min(x1, this.pointLists[i][j][0]);
				y1 = Math.min(y1, this.pointLists[i][j][1]);
				x2 = Math.max(x2, this.pointLists[i][j][0] + this.pointLists[i][j][2]);
				y2 = Math.max(y2, this.pointLists[i][j][1] + this.pointLists[i][j][3]);
			}
		}
		this.storeBoundingBox(x1, y1, x2, y2);
	}

	this.createNoteBBox = function() {
		var x1, x2, y1, y2;
		x1 = this.pointLists[0][0][0];
		y1 = this.pointLists[0][0][1];
		x2 = x1 + 28;
		y2 = y1 + 28;
		this.storeBoundingBox(x1, y1, x2, y2);
	}

	this.storeBoundingBox = function(x1, y1, x2, y2) {
		this.boundingBox = [];
		this.boundingBox.push([x1, y1]);
		this.boundingBox.push([x2, y2]);
	}

	this.drawBoundingBox = function(ctx, color) {
		if (this.boundingBox.length !== 2) {
			return;
		}
		var x1, x2, y1, y2;
		x1 = this.boundingBox[0][0];
		y1 = this.boundingBox[0][1];
		x2 = this.boundingBox[1][0];
		y2 = this.boundingBox[1][1];
		ctx.beginPath();
		ctx.moveTo(x1, y1);
		ctx.lineTo(x2, y1);
		ctx.lineTo(x2, y2);
		ctx.lineTo(x1, y2);
		ctx.closePath();
		ctx.strokeStyle = color;
		ctx.lineWidth = 2;
		ctx.stroke();
	}

	this.hit = function(x, y) {
		//Check bounding box first.
		if (( this.boundingBox[0][0] - this.hitThreshold <= x && x <= this.boundingBox[1][0] + this.hitThreshold ) &&
			( this.boundingBox[0][1] - this.hitThreshold <= y && y <= this.boundingBox[1][1] + this.hitThreshold )) {
			if (this.drawType === "pencil") {
				return this.hitLine( x, y );
			} else if (this.drawType === "arrow" || this.drawType === "marker") {
				return this.hitArrow( x, y ); // marker is same as arrow but no arrowhead
			} else if (this.drawType === "highlight") {
				return this.hitRect( x, y );
			} else if (this.drawType === "note") {
				return true;
			}
		}
		return false;
	}

	this.hitLine = function( x, y ) {
		var i = 0, j = 0;
		for(i = 0; i < this.pointLists.length; i++) {
			for(j = 0; j < this.pointLists[i].length; j++) {
				var x1 = this.pointLists[i][j][0];
				var y1 = this.pointLists[i][j][1];
				if (this.hitThreshold >= Math.sqrt(Math.pow(x1 - x, 2) + Math.pow(y1 - y, 2))) {
					return true;
				}
			}
		}
		return false;
	}

	this.hitArrow = function(x, y) {
		var x1 = this.pointLists[0][0][0]
		var y1 = this.pointLists[0][0][1];
		var x2 = this.pointLists[0][1][0];
		var y2 = this.pointLists[0][1][1];
		var distance = Math.abs( ( x2 - x1 )* ( y1 - y ) - ( x1 - x ) * ( y2 - y1 ) ) / Math.sqrt( Math.pow( x2 - x1, 2 ) + Math.pow( y2 - y1, 2 ) );
		if (this.hitThreshold >= distance) {
			return true;
		}
		return false;
	}

	this.hitRect = function(x, y) {
		var i = 0, j = 0;
		for(i = 0; i < this.pointLists.length; i++) {
			for(j = 0; j < this.pointLists[i].length; j++) {
				var x1 = this.pointLists[i][j][0];
				var y1 = this.pointLists[i][j][1];
				var x2 = this.pointLists[i][j][0] + this.pointLists[i][j][2];
				var y2 = this.pointLists[i][j][1] + this.pointLists[i][j][3];
				if ((x1 <= x && x <= x2) && (y1 <= y && y <= y2)) {
					return true;
				}
			}
		}
		return false;
	}

	this.updateDragPoints = function() {
		if (this.drawType === 'arrow' || this.drawType === 'marker' ) {
			var currX, currY, prevX, prevY;
			prevX = this.pointLists[0][0][0];
			prevY = this.pointLists[0][0][1];
			currX = this.pointLists[0][1][0];
			currY = this.pointLists[0][1][1];
			var lineAngle = Math.atan2( currY - prevY, currX - prevX );

			var x, y;
			x = -Annotate.dragIconOffset * Math.cos( lineAngle ) + prevX;
			y = -Annotate.dragIconOffset * Math.sin( lineAngle ) + prevY;
			this.dragPoints[0] = [x, y];
			x = Annotate.dragIconOffset * Math.cos( lineAngle ) + currX;
			y = Annotate.dragIconOffset * Math.sin( lineAngle ) + currY;
			this.dragPoints[1] = [x, y];
		} else if (this.drawType === 'pencil') {
			// Create the 8 drag points, starting in the top left and going clockwise.
			var dragBox = [];
			dragBox[0] = [this.boundingBox[0][0] - Annotate.dragIconOffset, this.boundingBox[0][1] - Annotate.dragIconOffset];
			dragBox[1] = [this.boundingBox[1][0] + Annotate.dragIconOffset, this.boundingBox[1][1] + Annotate.dragIconOffset];
			this.dragPoints[0] = [dragBox[0][0], dragBox[0][1]];
			this.dragPoints[1] = [dragBox[0][0] + ( dragBox[1][0] - dragBox[0][0] ) / 2, dragBox[0][1]];
			this.dragPoints[2] = [dragBox[1][0], dragBox[0][1]];
			this.dragPoints[3] = [dragBox[1][0], dragBox[0][1] + ( dragBox[1][1] - dragBox[0][1] ) / 2];
			this.dragPoints[4] = [dragBox[1][0], dragBox[1][1]];
			this.dragPoints[5] = [dragBox[0][0] + ( dragBox[1][0] - dragBox[0][0] ) / 2, dragBox[1][1]];
			this.dragPoints[6] = [dragBox[0][0], dragBox[1][1]];
			this.dragPoints[7] = [dragBox[0][0], dragBox[0][1] + ( dragBox[1][1] - dragBox[0][1] ) / 2];
		}
	}

	this.hitDragPoint = function( x, y) {
		for (var i = 0; i < this.dragPoints.length; i++) {
			var x1 = this.dragPoints[i][0];
			var y1 = this.dragPoints[i][1];
			if (this.hitThreshold >= Math.sqrt(Math.pow(x1 - x, 2) + Math.pow(y1 - y, 2))) {
				return i;
			}
		}
		return false;
	}

	this.translate = function( x, y ) {
		var i = 0, j = 0;
		for(i = 0; i < this.pointLists.length; i++) {
			for(j = 0; j < this.pointLists[i].length; j++) {
				this.pointLists[i][j][0] += x;
				this.pointLists[i][j][1] += y;
			}
		}

	}

	this.clone = function() {
		var newObject = new DrawObject( this.hitThreshold );
		newObject.author = this.author;
		newObject.boundingBox = JSON.parse( JSON.stringify( this.boundingBox ) );
		newObject.color = this.color;
		newObject.date = this.date;
		newObject.drawType = this.drawType;
		newObject.index = this.index;
		newObject.lineCount = this.lineCount;
		newObject.lineWidth = this.lineWidth;
		newObject.note = this.note;
		newObject.opacity = this.opacity;
		newObject.page = this.page;
		newObject.pointLists = JSON.parse( JSON.stringify( this.pointLists ) );
		newObject.dragPoints = JSON.parse( JSON.stringify( this.dragPoints ) );

		return newObject;
	}

	this.debugShape = function() {
		console.log("Page: "+this.page);
		console.log("Color: "+this.color);
		console.log("LineCount: "+this.lineCount);
		console.log("DrawType: "+this.drawType);
		console.log("BoundingBox: "+this.boundingBox.toString());
		console.log("Hit Threshold: "+this.hitThreshold.toString());
		console.log("Index: "+this.index);

		var i = 0, j = 0;
		for(i = 0; i < this.pointLists.length; i++) {
			for(j = 0; j < this.pointLists[i].length; j++) {
				if (this.drawType === "pencil") {
					console.log("{ "+this.pointLists[i][j][0]+", "+this.pointLists[i][j][1]+" },");
				}
				if (this.drawType === "arrow" || this.drawType === "marker") {
					console.log("{ "+this.pointLists[i][j][0]+", "+this.pointLists[i][j][1]+" },");
				}
				if (this.drawType === "highlight") {
					console.log("{ "+this.pointLists[i][j][0]+", "+this.pointLists[i][j][1]+", "+this.pointLists[i][j][2]+", "+this.pointLists[i][j][3]+" },");
				}
			}
		}
	}
}

function isset( v ) {
	return (typeof v !== 'undefined' && v !== null);
}

function exportObject( drawObject )
{
	this.drawType = drawObject.drawType;
	var c = Annotate.splitColor( drawObject.color );
	if( !isset( c.a ) ) {
		this.color = Annotate.rgbToHex( c.r, c.g, c.b );
	} else {
		this.color = Annotate.rgbaToHex( c.r, c.g, c.b, c.a );
	}
	if( this.drawType === "pencil" || this.drawType === "arrow" || this.drawType === "marker" ) {
		this.lineWidth = drawObject.lineWidth;
	}
	this.opacity = drawObject.opacity * 100;
	this.pointLists = drawObject.pointLists;
	this.date = drawObject.date;
	this.note = drawObject.note;
	this.index = drawObject.index;
}

function CanvasObject(canvas, offset, ID) {
	this.canvas = canvas;
	this.width = canvas.width;
	this.height = canvas.height;
	this.ctx = canvas.getContext("2d");
	this.offsetX = offset.left;
	this.offsetY = offset.top;
	this.ID = ID;
	this.scale = 1.0;	// How much bigger or smaller the current cavas is to the last canvas.
	this.rotation = 0;	// 0 = up, 90 = rotate right, 180 = upsidedown, 270 = rotate left.

	this.updateCanvas = function(canvas, offset, newRotation) {
		this.canvas = canvas;
		var delta = ( newRotation - this.rotation + 360 ) % 360;
		if (delta === 90 || delta === 270) {
			this.scale = canvas.width / this.height;
		} else {
			this.scale = canvas.width / this.width;
		}
		this.width = canvas.width;
		this.height = canvas.height;
		this.ctx = canvas.getContext("2d");
		this.offsetX = offset.left;
		this.offsetY = offset.top;
	};
}

var DEFAULT_LINE_WIDTH = 4;
var DEFAULT_MARKER_WIDTH = 12;
var DEFAULT_ARROWHEAD_LENGTH = 14;

var Annotate = window.top.Annotate = {
	canvasList : [],
	canvas : null,
	ctx : null,
	flag : false,
	prevX : 0,
	currX : 0,
	prevY : 0,
	currY : 0,
	width : 0,
	height : 0,
	offsetX : 0,
	offsetY : 0,
	scrollDiv : null,
	deviceRatio : 1.0,
	dragIcon : null,				// The draw image to draw when moving annotations.

	drawColor : "#ff0000",
	selectColor : "#fff600",
	arrowColor : "#ff0000",
	noteColor : "#ff0000",
	lineWidth : DEFAULT_LINE_WIDTH,
	markerWidth : DEFAULT_MARKER_WIDTH,
	clickThreshold : 10,		// How close does a person need to click on something to select it.
	arrowHeadAngle : 2.618,		// 150 Degrees in radians.
	arrowHeadLength : DEFAULT_ARROWHEAD_LENGTH,
	dragIconOffset : 15,		// How far away do we place the drag icons.
	scale : 1,

	currentShape : null,
	drawList : [],
	undoList : [],
	undoCounter : 0,			// How many undo items do we have.
	annotationIndex : 0,		// The annotation index counter.
	drawStateEnum : { None: 0, Select: 1, Draw: 2, Arrow: 3, Note: 4, Erase: 5, Marker: 6 },
	drawState : null,
	selectedNodes : null,		// What nodes are selected for highlighting.
	selectedStartNode : null,	// The first node selected.
	selectedEndNode : null,		// The last node selected.
	selectMoveOld : {x: 0, y:0 },
	selectTextThreshold : 100,	// How big does the selection range need to be before we let the user know there is no text to select.
	showTextMessage : true,
	updatedFilename : null,		// Filename for annotated document without the .pdf at the end.
	fileID : 0,					// FileID of the current document.  This will be updated after each save.
	sourceFileID: 0,			// File ID of originating document.
	currentFile: null,			// Datecore file object for the current document
	personalFolderID : null,
	nextStateEnum : {"None": 0, "Exit": 1, "Send": 2, "Download": 3, "StartPresentation": 4, "Introduce": 5,  "WPIntroduce": 6, "SetPresentationFile":7},	//What should happen next after a save.
	nextState : null,
	needToSave : false,			// Did the user change the annotations?
	setPresentionFileID : 0,	// Used to hold he ID od the new file selected from within a presentation
	isTempFile : false,			// Is this a temp file for Introducing.
	savedPageScale : 'auto',
	firstRender : true,			// Have we rendered our first page yet?
	selectedAnnotation : null,	// A copy of an annotation that is currently selected.  This is always drawn last to be on top.
	selectedDragPoint : null,	// Has the user clicked on a drag point and which one.

	inPresentation : false,			// moved to Datacore.s.inPresentation
	presentationHost : false,		// Is everyone following me?
	passedPresentor : false,		// Did we pass the presentor role?
	iAmPresentor : false,			// Am i the presentor

	presentationPageIndex : 0,		// What page is the presentation on.
	presentationPageTopOffset : 0,	// What is the top offset of the current page.
	presentationPendingPageSettings : [], //Any pending settings that need to wait for a render to finish before they can be processed.
	presentationPortrait : false,	// Is the presentation is portrait view.
	presentationScale : 1,			// What is the scale value we use with pdf.js.
	presentationCurrentPageScale : 1,	// What was the current Page scale passed in.
	presentationReferenceScale : 1,		// Value used to convert pdf.js page scale to iPad page scale.
	presentationDepositionID : 0,	// The depositionID for the presentation.
	presentationPendingAnnotations : null,	//Annotations that are waiting for a page render before they can be added.

	scrollTimerID : null,			// Timer to see if the user has stopped scrolling.
	pageChangeTimerID : null,		// Timer for page changes.
	uiTimeout : 200,				// How long do we wait to see if the ui has stopped updating.
	processing : false,				// Are we processing some other long request.

	isBackEvent: false,

	mouseMove : function(e) {
		if (Annotate.drawState === Annotate.drawStateEnum.Arrow) {
			Annotate.findxyArrow( 'move', e );
		} else if (Annotate.drawState === Annotate.drawStateEnum.Marker) {
			Annotate.findxyMarker( 'move', e );
		} else {
			Annotate.findxy( 'move', e );
		}
	},
	mouseDown : function(e) {
		if (Annotate.drawState === Annotate.drawStateEnum.Arrow) {
			Annotate.findxyArrow( 'down', e );
		} else if (Annotate.drawState === Annotate.drawStateEnum.Marker) {
			Annotate.findxyMarker( 'down', e );
		} else {
			Annotate.findxy( 'down', e );
		}
	},
	mouseUp : function(e) {
		if (Annotate.drawState === Annotate.drawStateEnum.Arrow) {
			Annotate.findxyArrow( 'up', e );
		} else if (Annotate.drawState === Annotate.drawStateEnum.Marker) {
			Annotate.findxyMarker( 'up', e );
		} else {
			Annotate.findxy( 'up', e );
		}
	},
	mouseOut : function(e) {
		if (Annotate.drawState === Annotate.drawStateEnum.Arrow) {
			Annotate.findxyArrow( 'out', e );
		} else if (Annotate.drawState === Annotate.drawStateEnum.Marker) {
			Annotate.findxyMarker( 'out', e );
		} else {
			Annotate.findxy( 'out', e );
		}
	},
	touchStart : function(e) {
		e.preventDefault();
		var annotateEvent = {target: e.target, clientX: e.targetTouches[0].clientX, clientY: e.targetTouches[0].clientY};
		if (Annotate.drawState === Annotate.drawStateEnum.Arrow) {
			Annotate.findxyArrow( 'down', annotateEvent );
		} else if (Annotate.drawState === Annotate.drawStateEnum.Marker) {
			Annotate.findxyMarker( 'down', annotateEvent );
		} else {
			Annotate.findxy( 'down', annotateEvent );
		}
	},
	touchMove : function(e) {
		e.preventDefault();
		var annotateEvent = {target: e.target, clientX: e.targetTouches[0].clientX, clientY: e.targetTouches[0].clientY};
		if (Annotate.drawState === Annotate.drawStateEnum.Arrow) {
			Annotate.findxyArrow( 'move', annotateEvent );
		} else if (Annotate.drawState === Annotate.drawStateEnum.Marker) {
			Annotate.findxyMarker( 'move', annotateEvent );
		} else {
			Annotate.findxy( 'move', annotateEvent );
		}
	},
	touchEnd : function(e) {
		e.preventDefault();
		var annotateEvent = {target: e.target, clientX: 0, clientY: 0};
		if (Annotate.drawState === Annotate.drawStateEnum.Arrow) {
			Annotate.findxyArrow( 'up', annotateEvent );
		} else if (Annotate.drawState === Annotate.drawStateEnum.Marker) {
			Annotate.findxyMarker( 'up', annotateEvent );
		} else {
			Annotate.findxy( 'up', annotateEvent );
		}
	},
	touchleave : function(e) {
		e.preventDefault();
		var annotateEvent = {target: e.target, clientX: 0, clientY: 0};
		if (Annotate.drawState === Annotate.drawStateEnum.Arrow) {
			Annotate.findxyArrow( 'out', annotateEvent );
		} else if (Annotate.drawState === Annotate.drawStateEnum.Marker) {
			Annotate.findxyMarker( 'out', annotateEvent );
		} else {
			Annotate.findxy( 'out', annotateEvent );
		}
	},

	startDrawing : function() {
		var i = 0, len = Annotate.canvasList.length;
		for(i = 0; i < len; i++) {
			Annotate.canvasList[i].canvas.addEventListener("mousemove", Annotate.mouseMove, false);
			Annotate.canvasList[i].canvas.addEventListener("mousedown", Annotate.mouseDown, false);
			Annotate.canvasList[i].canvas.addEventListener("mouseup", Annotate.mouseUp, false);
			Annotate.canvasList[i].canvas.addEventListener("mouseout", Annotate.mouseOut, false);
			Annotate.canvasList[i].canvas.addEventListener("touchstart", Annotate.touchStart, false);
			Annotate.canvasList[i].canvas.addEventListener("touchmove", Annotate.touchMove, false);
			Annotate.canvasList[i].canvas.addEventListener("touchend", Annotate.touchEnd, false);
		}
		$(Annotate.scrollDiv).css("overflow", "hidden");
	},

	stopDrawing : function() {
		var i = 0, len = Annotate.canvasList.length;
		for(i = 0; i < len; i++) {
			Annotate.canvasList[i].canvas.removeEventListener("mousemove", Annotate.mouseMove, false);
			Annotate.canvasList[i].canvas.removeEventListener("mousedown", Annotate.mouseDown, false);
			Annotate.canvasList[i].canvas.removeEventListener("mouseup", Annotate.mouseUp, false);
			Annotate.canvasList[i].canvas.removeEventListener("mouseout", Annotate.mouseOut, false);
			Annotate.canvasList[i].canvas.removeEventListener("touchstart", Annotate.touchStart, false);
			Annotate.canvasList[i].canvas.removeEventListener("touchmove", Annotate.touchMove, false);
			Annotate.canvasList[i].canvas.removeEventListener("touchend", Annotate.touchEnd, false);
			$(Annotate.canvasList[i].canvas).css("cursor", "auto");
		}
		$(Annotate.scrollDiv).css("overflow", "auto");
	},

	draw : function() {
		Annotate.ctx.beginPath();
		Annotate.ctx.moveTo(Annotate.prevX, Annotate.prevY);
		Annotate.ctx.lineTo(Annotate.currX, Annotate.currY);
		Annotate.ctx.strokeStyle = Annotate.drawColor;
		Annotate.ctx.lineWidth = Annotate.lineWidth * PDFView.currentScale;
		Annotate.ctx.stroke();
		Annotate.ctx.closePath();
	},

	drawRect : function(xRect, yRect, wRect, hRect) {
		Annotate.ctx.lineWidth = 4;
		Annotate.ctx.strokeStyle = "#ff0000";
		Annotate.ctx.strokeRect(xRect, yRect, wRect, hRect);
	},

	findCanvasObj : function(canvas) {
		return Annotate.findCanvasObjByID(canvas.id);
	},

	findCanvasObjByID : function(id) {
		var canvasObj = null;
		for(var i = 0; i < Annotate.canvasList.length; i++) {
			if (id === Annotate.canvasList[i].ID) {
				canvasObj = Annotate.canvasList[i];
				break;
			}
		}

		return canvasObj;
	},

	findCanvasObjByPage : function( page ) {
	// Pages count at 0, but PDF pages start at 1.
		var id = (page + 1).toString();
		return Annotate.findCanvasObjByID( id );
	},

	findxy : function(res, e) {
		if( res === "down" ) {
//			console.log( 'Annotate::findxy', res );
			Annotate.clearSelection();
			var canvasObj = Annotate.findCanvasObj( e.target );
			if( canvasObj === null ) {
				return;
			}
			Annotate.canvas = canvasObj.canvas;
			Annotate.ctx = canvasObj.ctx;
			Annotate.prevX = Annotate.currX;
			Annotate.prevY = Annotate.currY;
			Annotate.currX = e.clientX - canvasObj.offsetX + Annotate.scrollDiv.scrollLeft();
			Annotate.currY = e.clientY - canvasObj.offsetY + Annotate.scrollDiv.scrollTop();
			$(Annotate.canvas).css("cursor", "default");
			if (Annotate.currentShape === null) {
				Annotate.currentShape = new DrawObject(Annotate.clickThreshold);
				Annotate.currentShape.page = canvasObj.ID;
				Annotate.currentShape.color = Annotate.drawColor;
				Annotate.currentShape.setDrawType("pencil", Annotate.lineWidth);
				var indexPage = parseInt( canvasObj.ID ) - 1;
				Annotate.currentShape.index = Annotate.annotationIndex++;
				Annotate.currentShape.isSegmented = 1;
			} else {
				Annotate.currentShape.isSegmented = 2;
			}
			Annotate.currentShape.addObject();
			Annotate.currentShape.addPoints(Annotate.currX, Annotate.currY);
			Annotate.flag = true;
			if (window.top.Datacore.getVal(window.top.Datacore.s.currentUser, 'userType') === 'W' && !Annotate.needToSave && window.top.Datacore.getS('depositionStatus') !== 'F') {
				window.top.navBar.saveButton.removeClass( 'disabled' );
			}
			Annotate.needToSave = true;
		}
		if( res === "up" || res === "out" ) {
//			console.log( 'Annotate::findxy', res );
			if( Annotate.flag !== true ) {
				return;
			}
			Annotate.flag = false;
			$(Annotate.canvas).css( "cursor", "auto" );
			if( Annotate.currentShape !== null && Annotate.currentShape.lineCount > 0 ) {
				if( Annotate.currentShape.pointLists[Annotate.currentShape.lineCount - 1].length === 1 ) {
					//Add a second point to make a dot.
					Annotate.prevX = Annotate.currX;
					Annotate.prevY = Annotate.currY;
					Annotate.currY += Annotate.lineWidth;
					Annotate.draw();
					Annotate.currentShape.addPoints( Annotate.currX, Annotate.currY );
				}
//				console.log("Last Shape Length: " + Annotate.currentShape.pointLists[Annotate.currentShape.lineCount - 1].length);
			}
			if( Annotate.iAmPresentor ) {
				Annotate.currentShape.createBoundingBox();
				Annotate.currentShape.updateDragPoints();
				Annotate.sendDraw( Annotate.currentShape );
			}
		}
		if( res === "move" ) {
			if( Annotate.flag ) {
				var canvasObj = Annotate.findCanvasObj( e.target );
				if( canvasObj === null ) {
					return;
				}
				Annotate.canvas = canvasObj.canvas;
				Annotate.ctx = canvasObj.ctx;
				Annotate.prevX = Annotate.currX;
				Annotate.prevY = Annotate.currY;
				Annotate.currX = e.clientX - canvasObj.offsetX + Annotate.scrollDiv.scrollLeft();
				Annotate.currY = e.clientY - canvasObj.offsetY + Annotate.scrollDiv.scrollTop();
				Annotate.draw();
				Annotate.currentShape.addPoints( Annotate.currX, Annotate.currY );
			}
		}
	},

	sendDraw : function( drawObj ) {
		var canvasObj = Annotate.findCanvasObjByID( drawObj.page );
		var points = [];
		for( var i=0; i<drawObj.pointLists.length; ++i ) {
			var _p = [];
			for( var k=0; k<drawObj.pointLists[i].length; ++k ) {
				_p.push( Annotate.formatPoint( drawObj.pointLists[i][k], canvasObj.width, canvasObj.height, canvasObj.rotation ) );
			}
			points.push( _p );
		}
		var drawData = {
			"Operation": "add",
			"Type": 9,
			"Page": parseInt( drawObj.page ) - 1,
			"Opacity": Math.floor( drawObj.opacity * 100 ),
			"LineWidth": drawObj.lineWidth / Annotate.scale,
			"Color": Annotate.colorToInt( drawObj.color ),
			"Index": drawObj.index,
			"Points": points,
			"isSegmented": drawObj.isSegmented
		};
		switch( drawData.isSegmented ) {
			case 2:		//isSegmented=2
				window.top.modifyAnnotation( Annotate.presentationDepositionID, drawData );
				break;
			default:	//isSegmented=0,1,3
				window.top.addAnnotation( Annotate.presentationDepositionID, drawData );
				break;
		}
	},


	// If page is passed, we only want to redraw that one page.
	redraw : function( page ) {
		var i, j, d;
		var rgb_value;
		// Clear views
		for(i = 0; i < Annotate.canvasList.length; i++) {
			if (isset( page ) && page !== Annotate.canvasList[i].ID) {
				continue;
			}
			Annotate.ctx = Annotate.canvasList[i].ctx;
			Annotate.ctx.clearRect(0, 0, Annotate.canvasList[i].width, Annotate.canvasList[i].height);
		}

		// Redraw saved data.
		for (d = 0; d <= Annotate.drawList.length; d++) {
			// Draw the selected annotation last so it is on top.
			if (d === Annotate.drawList.length) {
				if (isset( Annotate.selectedAnnotation )) {
					Annotate.currentShape = Annotate.selectedAnnotation;
				} else {
					continue;
				}
			} else {
				if (isset( page ) && page !== Annotate.drawList[d].page) {
					continue;
				}
				Annotate.currentShape = Annotate.drawList[d];
			}
			var canvasObj = Annotate.findCanvasObjByID(Annotate.currentShape.page);
			if (canvasObj === null) {
				continue;
			} else {
				Annotate.ctx = canvasObj.ctx;
			}
			Annotate.ctx.beginPath();
			rgb_value = Annotate.splitColor( Annotate.currentShape.color );
			if (rgb_value !== null) {
				Annotate.ctx.strokeStyle = 'rgba(' + rgb_value.r + ',' + rgb_value.g + ',' + rgb_value.b + ',' + Annotate.currentShape.opacity + ')';
			} else {
				Annotate.ctx.strokeStyle = 'rgba(255, 255, 0, ' + Annotate.currentShape.opacity + ')';
			}
			if (Annotate.currentShape.drawType === "pencil") {
				var currX, currY;
				if (Annotate.currentShape.isSelected) {
					Annotate.ctx.shadowColor = '#666';
					Annotate.ctx.shadowBlur = 5;
					Annotate.ctx.shadowOffsetX = 5;
					Annotate.ctx.shadowOffsetY = 5;
				}
				for (i = 0; i < Annotate.currentShape.pointLists.length; i++) {
					for (j = 0; j < Annotate.currentShape.pointLists[i].length; j++) {
						currX = Annotate.currentShape.pointLists[i][j][0];
						currY = Annotate.currentShape.pointLists[i][j][1];
						if (j === 0) {
							Annotate.ctx.moveTo(currX, currY);
						} else {
							Annotate.ctx.lineTo(currX, currY);
						}
					}
				}
			} else if (Annotate.currentShape.drawType === "highlight") {
				var rgb_value = Annotate.splitColor( Annotate.currentShape.color );
				if (rgb_value !== null) {
					Annotate.ctx.fillStyle = 'rgba(' + rgb_value.r + ',' + rgb_value.g + ',' + rgb_value.b + ',' + ( Annotate.currentShape.opacity / 2 ) + ')';
				} else {
					Annotate.ctx.fillStyle = 'rgba(255, 255, 0, 0.4)';
				}
				var xRect, yRect, wRect, hRect;
				for (i = 0; i < Annotate.currentShape.pointLists.length; i++) {
					for (j = 0; j < Annotate.currentShape.pointLists[i].length; j++) {
						switch(canvasObj.rotation) {
							case 90:
								hRect = Annotate.currentShape.pointLists[i][j][2];
								wRect = Annotate.currentShape.pointLists[i][j][3];
								xRect = Annotate.currentShape.pointLists[i][j][0] - wRect;
								yRect = Annotate.currentShape.pointLists[i][j][1];
								break;

							case 180:
								wRect = Annotate.currentShape.pointLists[i][j][2];
								hRect = Annotate.currentShape.pointLists[i][j][3];
								xRect = Annotate.currentShape.pointLists[i][j][0] - wRect;
								yRect = Annotate.currentShape.pointLists[i][j][1] - hRect;
								break;

							case 270:
								hRect = Annotate.currentShape.pointLists[i][j][2];
								wRect = Annotate.currentShape.pointLists[i][j][3];
								xRect = Annotate.currentShape.pointLists[i][j][0];
								yRect = Annotate.currentShape.pointLists[i][j][1] - hRect;
								break;

							default:
								wRect = Annotate.currentShape.pointLists[i][j][2];
								hRect = Annotate.currentShape.pointLists[i][j][3];
								xRect = Annotate.currentShape.pointLists[i][j][0];
								yRect = Annotate.currentShape.pointLists[i][j][1];
								break;
						}
						Annotate.ctx.fillRect(xRect, yRect, wRect, hRect);
						if (Annotate.currentShape.isSelected) {
							Annotate.ctx.strokeStyle = 'rgba(169, 169, 169, 1)';
							Annotate.ctx.lineWidth = 1;
							Annotate.ctx.strokeRect(xRect, yRect, wRect, hRect);
						}
					}
				}
			} else if (Annotate.currentShape.drawType === "arrow" || Annotate.currentShape.drawType === "marker") {
				var currX, currY, prevX, prevY;
				prevX = Annotate.currentShape.pointLists[0][0][0];
				prevY = Annotate.currentShape.pointLists[0][0][1];
				currX = Annotate.currentShape.pointLists[0][1][0];
				currY = Annotate.currentShape.pointLists[0][1][1];
				var lineAngle = Math.atan2( currY - prevY, currX - prevX );
				if (Annotate.currentShape.isSelected) {
					Annotate.ctx.shadowColor = '#666';
					Annotate.ctx.shadowBlur = 5;
					Annotate.ctx.shadowOffsetX = 5;
					Annotate.ctx.shadowOffsetY = 5;
				}
				Annotate.ctx.moveTo( prevX, prevY );
				Annotate.ctx.lineTo( currX, currY );
				if (Annotate.currentShape.drawType === "arrow") {
					Annotate.ctx.moveTo( currX, currY );
					var x = Annotate.arrowHeadLength * Math.cos( lineAngle + Annotate.arrowHeadAngle ) + currX;
					var y = Annotate.arrowHeadLength * Math.sin( lineAngle + Annotate.arrowHeadAngle ) + currY;
					Annotate.ctx.lineTo( x, y );
					Annotate.ctx.moveTo( currX, currY );
					x = Annotate.arrowHeadLength * Math.cos( lineAngle - Annotate.arrowHeadAngle ) + currX;
					y = Annotate.arrowHeadLength * Math.sin( lineAngle - Annotate.arrowHeadAngle ) + currY;
					Annotate.ctx.lineTo( x, y );
					Annotate.ctx.lineCap = 'square';
				} else {
					Annotate.ctx.lineCap = 'butt';
				}
				if (Annotate.currentShape.isSelected) {
					Annotate.ctx.lineWidth = Annotate.currentShape.lineWidth / Annotate.deviceRatio;
					Annotate.ctx.stroke();
					Annotate.ctx.beginPath();
					Annotate.ctx.shadowColor = null;
					Annotate.ctx.shadowBlur = 0;
					Annotate.ctx.shadowOffsetX = 0;
					Annotate.ctx.shadowOffsetY = 0;
					for(var p = 0; p < Annotate.currentShape.dragPoints.length; p++) {
						x = Annotate.currentShape.dragPoints[p][0] - Annotate.dragIcon.naturalWidth / 2;
						y = Annotate.currentShape.dragPoints[p][1] - Annotate.dragIcon.naturalHeight / 2;
						Annotate.ctx.drawImage( Annotate.dragIcon, x, y);
					}
				}
			}  else if (Annotate.currentShape.drawType === "note") {
				var currX, currY;
				currX = Annotate.currentShape.pointLists[0][0][0];
				currY = Annotate.currentShape.pointLists[0][0][1];
				Annotate.ctx.closePath(); // Close off started path because the drawNoteIcon code already does the path.
				Annotate.drawNoteIcon( currX, currY, Annotate.currentShape.color );
				Annotate.addNoteMouseOver( Annotate.currentShape );
				Annotate.ctx.beginPath();
			}
			Annotate.ctx.lineWidth = Annotate.currentShape.lineWidth / Annotate.deviceRatio;
			Annotate.ctx.stroke();

			//Reset the shadowColor
			Annotate.ctx.shadowColor = null;
			Annotate.ctx.shadowBlur = 0;
			Annotate.ctx.shadowOffsetX = 0;
			Annotate.ctx.shadowOffsetY = 0;

			if (Annotate.currentShape.drawType === "pencil" && Annotate.currentShape.isSelected) {
				Annotate.drawPencilBox();
			}
		}
		Annotate.currentShape = null;
	},

	drawPencilBox : function() {
		Annotate.ctx.beginPath();
		var dragBox = [];
		dragBox[0] = [Annotate.currentShape.boundingBox[0][0] - Annotate.dragIconOffset, Annotate.currentShape.boundingBox[0][1] - Annotate.dragIconOffset];
		dragBox[1] = [Annotate.currentShape.boundingBox[1][0] + Annotate.dragIconOffset, Annotate.currentShape.boundingBox[1][1] + Annotate.dragIconOffset];

		Annotate.ctx.dashedLine( dragBox[0][0], dragBox[0][1], dragBox[1][0], dragBox[0][1], 3);
		Annotate.ctx.dashedLine( dragBox[1][0], dragBox[0][1], dragBox[1][0], dragBox[1][1], 3);
		Annotate.ctx.dashedLine( dragBox[1][0], dragBox[1][1], dragBox[0][0], dragBox[1][1], 3);
		Annotate.ctx.dashedLine( dragBox[0][0], dragBox[1][1], dragBox[0][0], dragBox[0][1], 3);

		var rgb_value = Annotate.splitColor( Annotate.currentShape.color );
		if (rgb_value !== null) {
			Annotate.ctx.strokeStyle = 'rgba(' + rgb_value.r + ',' + rgb_value.g + ',' + rgb_value.b + ',' + Annotate.currentShape.opacity + ')';
		} else {
			Annotate.ctx.strokeStyle = 'rgba(255, 255, 0, 0.4)';
		}
		Annotate.ctx.lineWidth = 2;
		Annotate.ctx.stroke();

		for(var p = 0; p < Annotate.currentShape.dragPoints.length; p++) {
			x = Annotate.currentShape.dragPoints[p][0] - Annotate.dragIcon.naturalWidth / 2;
			y = Annotate.currentShape.dragPoints[p][1] - Annotate.dragIcon.naturalHeight / 2;
			Annotate.ctx.drawImage( Annotate.dragIcon, x, y);
		}
	},

	setScale : function(scale) {
		// Adjust the base values for the given 'absolute' scale
		Annotate.scale = scale;
		Annotate.lineWidth = DEFAULT_LINE_WIDTH * scale;
		Annotate.markerWidth = DEFAULT_MARKER_WIDTH * scale;
		Annotate.arrowHeadLength = DEFAULT_ARROWHEAD_LENGTH * scale;
//		console.log( 'Annotate::setScale(): scale = ', scale, ' lineWidth =  ', Annotate.lineWidth, ' markerwidth = ', Annotate.markerWidth, ' arrowHeadLength = ', Annotate.arrowHeadLength );
	},

	
	setPageScale : function( pageNumber, newScale ) {
//		console.log( 'annotate::setPageScale(): pageNumber = ', pageNumber, ' newScale = ', newScale );
		var i, j, k;
		for (j = 0; j < Annotate.drawList.length; j++) {
			Annotate.currentShape = Annotate.drawList[j];
//			console.log( 'annotate::setPageScale(): Annotate.currentShape.page = ', Annotate.currentShape.page );
			if (pageNumber !== Annotate.currentShape.page) {
				continue;
			}
			if (Annotate.currentShape.drawType === "pencil" || Annotate.currentShape.drawType === "arrow" || Annotate.currentShape.drawType === "marker" || Annotate.currentShape.drawType === "note") {
				for (i = 0; i < Annotate.currentShape.pointLists.length; i++) {
					for (k = 0; k < Annotate.currentShape.pointLists[i].length; k++) {
						Annotate.currentShape.pointLists[i][k][0] *= newScale;
						Annotate.currentShape.pointLists[i][k][1] *= newScale;
					}
				}
			} else if (Annotate.currentShape.drawType === "highlight") {
				for (i = 0; i < Annotate.currentShape.pointLists.length; i++) {
					for (k = 0; k < Annotate.currentShape.pointLists[i].length; k++) {
						Annotate.currentShape.pointLists[i][k][0] *= newScale;
						Annotate.currentShape.pointLists[i][k][1] *= newScale;
						Annotate.currentShape.pointLists[i][k][2] *= newScale;
						Annotate.currentShape.pointLists[i][k][3] *= newScale;
					}
				}
			}
			if (Annotate.currentShape.drawType !== "note") {
				Annotate.currentShape.lineWidth = Math.round(Annotate.currentShape.lineWidth * newScale);
			}
			Annotate.currentShape.createBoundingBox();
			Annotate.currentShape.updateDragPoints();
		}
		Annotate.currentShape = null;
	},
	
	resizeDrawList : function(canvasObj) {
		var i, j, k;

		// Update the scale for draw list items on this canvas.
		for (j = 0; j < Annotate.drawList.length; j++) {
			Annotate.currentShape = Annotate.drawList[j];
			if (canvasObj.ID !== Annotate.currentShape.page) {
				continue;
			}
			if (Annotate.currentShape.drawType === "pencil" || Annotate.currentShape.drawType === "arrow" || Annotate.currentShape.drawType === "marker" || Annotate.currentShape.drawType === "note") {
				for (i = 0; i < Annotate.currentShape.pointLists.length; i++) {
					for (k = 0; k < Annotate.currentShape.pointLists[i].length; k++) {
						Annotate.currentShape.pointLists[i][k][0] *= canvasObj.scale;
						Annotate.currentShape.pointLists[i][k][1] *= canvasObj.scale;
					}
				}
			} else if (Annotate.currentShape.drawType === "highlight") {
				for (i = 0; i < Annotate.currentShape.pointLists.length; i++) {
					for (k = 0; k < Annotate.currentShape.pointLists[i].length; k++) {
						Annotate.currentShape.pointLists[i][k][0] *= canvasObj.scale;
						Annotate.currentShape.pointLists[i][k][1] *= canvasObj.scale;
						Annotate.currentShape.pointLists[i][k][2] *= canvasObj.scale;
						Annotate.currentShape.pointLists[i][k][3] *= canvasObj.scale;
					}
				}
			}
			if (Annotate.currentShape.drawType !== "note") {
				Annotate.currentShape.lineWidth = Math.round(Annotate.currentShape.lineWidth * canvasObj.scale);
			}
			Annotate.currentShape.createBoundingBox();
			Annotate.currentShape.updateDragPoints();
		}
		Annotate.currentShape = null;
	},

	rotateDrawList : function( canvasObj, newRotation ) {
//		console.log( 'annotate::rotateDrawList() --> newRotation = ', newRotation );
		var i, j, k;
		var delta = ( newRotation - canvasObj.rotation + 360 ) % 360;
		// Update the x and y values for draw list items on this canvas.
		for (j = 0; j < Annotate.drawList.length; j++) {
			Annotate.currentShape = Annotate.drawList[j];
			if (canvasObj.ID !== Annotate.currentShape.page) {
				continue;
			}
			for (i = 0; i < Annotate.currentShape.pointLists.length; i++) {
				for (k = 0; k < Annotate.currentShape.pointLists[i].length; k++) {
					Annotate.rotateToNewRotation( Annotate.currentShape.pointLists[i][k], canvasObj, delta );
				}
			}
			Annotate.currentShape.createBoundingBox();
			Annotate.currentShape.updateDragPoints();
		}
		Annotate.currentShape = null;
	},

	rotateToNewRotation : function( point, canvasObj, delta ) {
//		console.log( 'annotate::rotatetoNewRotation() --> delta = ', delta );
		var newX, newY;
		switch( delta ) {
			case 90:
				newX = canvasObj.width - point[1];
				newY = point[0];
				break;

			case 180:
				newX = canvasObj.width - point[0];
				newY = canvasObj.height - point[1];
				break;

			case 270:
				newX = point[1];
				newY = canvasObj.height - point[0];
				break;

			default:
				newX = point[0];
				newY = point[1];
				return;
		}
		point[0] = newX;
		point[1] = newY;
	},

	toggleDrawing : function()  {
		if (Annotate.drawState === Annotate.drawStateEnum.Draw) {
			Annotate.stopDrawing();
			Annotate.drawState = Annotate.drawStateEnum.None;
			if( Annotate.currentShape !== null ) {
				Annotate.currentShape.isSegmented = 3;
				Annotate.drawList.push( Annotate.currentShape );
				Annotate.undoList.push( {operation:'delete', obj:Annotate.currentShape} );
				Annotate.currentShape.createBoundingBox();
				Annotate.currentShape.updateDragPoints();
//				Annotate.currentShape.debugShape();
				if( Annotate.iAmPresentor ) {
					Annotate.sendDraw( Annotate.currentShape );
				} else {
					Annotate.undoCounter++;
				}
			}
			Annotate.currentShape = null;
			$("#content #drawing", parent.document).removeClass("selected");
			window.top.navBar.drawingButton.removeClass( 'selected' );
			window.top.navBar.canHideToolbar( true );
		} else {
			Annotate.stopAnnotations();
			Annotate.startDrawing();
			Annotate.drawState = Annotate.drawStateEnum.Draw;
			$("#content #drawing", parent.document).addClass("selected");
			window.top.navBar.drawingButton.addClass( 'selected' );
			window.top.navBar.canHideToolbar( false );
		}
		Annotate.updateUndoButton();
	},

	undo : function() {
		if ( Annotate.undoCounter === 0 ) {
			// console.log( 'Annotate::undo() nothing to undo');
			return;
		}
		Annotate.stopAnnotations();
		if (window.top.Datacore.s.inPresentation && Annotate.presentationHost) {
			window.top.undoAnnotation( window.top.webApp.getS( 'inDeposition' ) );
			return;
		}
		var undoOperation = Annotate.undoList.pop();
		Annotate.undoCounter--;
		if( Annotate.undoCounter === 0 ) {
			Annotate.needToSave = false;
		}
		if (undoOperation.operation === 'delete') {
			var oldAnnotation = undoOperation.obj;
			for(var i = 0; i < Annotate.drawList.length; i++) {
				if (Annotate.drawList[i].index === oldAnnotation.index && Annotate.drawList[i].page === oldAnnotation.page) {
					var drawObj = Annotate.drawList.splice( i, 1 );
					if (drawObj[0].drawType === 'note') {
						Annotate.removeNoteMouseOver( drawObj[0] );
					}
					break;
				}
			}
			if (window.top.Datacore.s.inPresentation && Annotate.presentationHost) {
				Annotate.sendDelete( oldAnnotation );
			}
		}
		if (undoOperation.operation === 'add') {
			Annotate.drawList.push( undoOperation.obj );
		}
		if (undoOperation.operation === 'modify') {
			var newAnnotation = undoOperation.obj;
			for(var i = 0; i < Annotate.drawList.length; i++) {
				if (Annotate.drawList[i].index === newAnnotation.index && Annotate.drawList[i].page === newAnnotation.page) {
					if (newAnnotation.drawType === 'note') {
						Annotate.removeNoteMouseOver( Annotate.drawList[i] );
					}
					Annotate.drawList[i] = newAnnotation;
					if (newAnnotation.drawType === 'note') {
						Annotate.addNoteMouseOver( Annotate.drawList[i] );
					}
					break;
				}
			}
			if (window.top.Datacore.s.inPresentation && Annotate.presentationHost) {
				Annotate.sendModify( newAnnotation );
			}
		}

		if ( window.top.Datacore.getVal(window.top.Datacore.s.currentUser, 'userType') === 'W' ) {
			if (Annotate.drawList.length > 0) {
				Annotate.needToSave = true;
				window.top.navBar.saveButton.removeClass( 'disabled' );
			} else {
				Annotate.needToSave = false;
				window.top.navBar.saveButton.addClass( 'disabled' );
			}
		}
		Annotate.redraw();
		Annotate.updateUndoButton();
	},

	removeAll : function() {
		Annotate.clearSelection();
		Annotate.stopSelect();
		if (Annotate.drawState === Annotate.drawStateEnum.Draw) {
			Annotate.toggleDrawing();
		}
		Annotate.stopEraseMode();
		while( Annotate.drawList.length > 0) {
			var oldAnnotation = Annotate.drawList.pop();
			if (oldAnnotation.drawType === 'note') {
				Annotate.removeNoteMouseOver( oldAnnotation );
			}
			if (window.top.Datacore.s.inPresentation && Annotate.presentationHost) {
				Annotate.sendDelete( oldAnnotation );
			}
		}
		Annotate.undoList = [];
		Annotate.undoCounter = 0;
		if ( window.top.Datacore.getVal(window.top.Datacore.s.currentUser, 'userType') === 'W' ) {
			window.top.navBar.saveButton.addClass( 'disabled' );
		}
		Annotate.needToSave = false;
		Annotate.redraw();
		Annotate.updateUndoButton();
	},

	sendDelete : function( deleteObj ) {
		var deleteData = {Operation: 'delete', Page: parseInt( deleteObj.page ) - 1, Index: deleteObj.index };
		parent.parent.deleteAnnotation( Annotate.presentationDepositionID, deleteData );
	},

	updateUndoButton : function() {
		if( Annotate.undoCounter === 0 ) {
			Annotate.needToSave = false;
			$('#content #undo', parent.document).addClass('disabled');
			window.top.navBar.undoButton.addClass( 'disabled' );
		} else {
			$('#content #undo', parent.document).removeClass('disabled');
			window.top.navBar.undoButton.removeClass( 'disabled' );
		}
	},

	mouseUpErase : function(e) {
		var canvasObj = Annotate.findCanvasObj(e.target);
		if (canvasObj === null) {
			return;
		}
		Annotate.canvas = canvasObj.canvas;
		Annotate.ctx = canvasObj.ctx;
		Annotate.currX = e.clientX - canvasObj.offsetX + Annotate.scrollDiv.scrollLeft();
		Annotate.currY = e.clientY - canvasObj.offsetY + Annotate.scrollDiv.scrollTop();

		var i = 0;
		while (i < Annotate.drawList.length) {
			// See if we are inside of the bounding rect for the object.
			var drawObj = Annotate.drawList[i];
			if (drawObj.hit(Annotate.currX, Annotate.currY)) {
				Annotate.drawList.splice(i, 1);
				if (drawObj.drawType === 'note') {
					Annotate.removeNoteMouseOver( drawObj );
				}
				if (window.top.Datacore.s.inPresentation && Annotate.presentationHost) {
					Annotate.sendDelete( drawObj );
				}
				Annotate.undoList.push( {operation: 'add', obj: drawObj} );
				Annotate.undoCounter++;
				Annotate.needToSave = true;
				continue;
			}
			i++;
		}
		Annotate.redraw();
		if ( window.top.Datacore.getVal(window.top.Datacore.s.currentUser, 'userType') === 'W' && Annotate.needToSave ) {
			window.top.navBar.saveButton.removeClass( 'disabled' );
		}
		if (Annotate.drawList.length === 0) {
			Annotate.stopEraseMode();
		}
	},

	promptRemoveAll : function() {
		var dialogButtons = [{label:'No', action:'dismissDialog3Btn();', class:'btn_app_cancel'}, {label:'Yes', id:'removeall', class:'btn_app_normal'}];
		var confirmMsg = 'Are you sure you want to clear all annotations and notes from this document?';
		parent.parent.showDialog3Btn( 'All Annotations Will Be Deleted!', confirmMsg, dialogButtons, true );
		$('#dialog3Btn #removeall', parent.parent.document).on("click", function() {
				window.top.dismissDialog3Btn();
				if (window.top.Datacore.s.inPresentation && Annotate.presentationHost) {
					Annotate.clearSelection();
					Annotate.stopSelect();
					if (Annotate.drawState === Annotate.drawStateEnum.Draw) {
						Annotate.toggleDrawing();
					}
					Annotate.stopEraseMode();
					window.top.clearAll( window.top.webApp.getS( 'inDeposition' ) );
				} else {
					Annotate.removeAll();
				}
		});
	},

	promptStartPresentationWithAnnotations: function()
	{
		var dialogButtons = [
			{"label":"Don't Save", "action":"window.top.Annotate.startPresentationWithChanges( true );", class:"btn_app_extra"},
			{"label":"Cancel", action:"dismissDialog3Btn();", class:"btn_app_cancel"},
			{"label":"Save and Start", action:"window.top.Annotate.startPresentationWithChanges( false );", class:"btn_app_normal"}
		];
		var confirmMsg = "You have changes to this document. Would you like to save the changes before starting the Presentation?";
		window.top.showDialog3Btn( 'Save Changes?', confirmMsg, dialogButtons, false );
	},

	startPresentationWithChanges: function( abortChanges )
	{
		window.top.dismissDialog3Btn();
		abortChanges = !!abortChanges;
		if( abortChanges ) {
			Annotate.removeAll();
			Annotate.presentationAttendeesSelected( Annotate.presentationDepositionID + ".deposition" );
			return;
		}
		Annotate.nextState = Annotate.nextStateEnum.StartPresentation;
		Annotate.getSaveFilename();
	},

	startEraseMode : function() {
		Annotate.clearSelection();
		var i = 0, len = Annotate.canvasList.length;
		for(i = 0; i < len; i++) {
			Annotate.canvasList[i].canvas.addEventListener("mouseup", Annotate.mouseUpErase, false);
		}
		Annotate.drawState = Annotate.drawStateEnum.Erase;
		$("#content #erase", parent.document).addClass("eraseMode");
		window.top.navBar.eraseButton.addClass( 'eraseMode' );
		window.top.navBar.canHideToolbar( false );
		$(Annotate.scrollDiv).css("overflow", "hidden");
	},

	stopEraseMode : function() {
		if (Annotate.drawState === Annotate.drawStateEnum.Erase) {
			var i = 0, len = Annotate.canvasList.length;
			for(i = 0; i < len; i++) {
				Annotate.canvasList[i].canvas.removeEventListener("mouseup", Annotate.mouseUpErase, false);
			}
			Annotate.drawState = Annotate.drawStateEnum.None;
			$("#content #erase", parent.document).removeClass("eraseMode");
			window.top.navBar.eraseButton.removeClass( 'eraseMode' );
//			parent.setCanHideFullscreenToolBar( true );
			window.top.navBar.canHideToolbar( true );
			$(Annotate.scrollDiv).css("overflow", "auto");
		}
	},

	erase : function() {
		if (Annotate.drawState === Annotate.drawStateEnum.Erase) {
			Annotate.stopEraseMode();
		} else {
			if (Annotate.drawState === Annotate.drawStateEnum.Draw) {
				Annotate.toggleDrawing();
			}
			if (Annotate.drawState === Annotate.drawStateEnum.Select) {
				Annotate.stopSelect();
			}
			if (Annotate.drawState === Annotate.drawStateEnum.Arrow) {
				Annotate.stopArrow();
			}
			if (Annotate.drawState === Annotate.drawStateEnum.Marker) {
				Annotate.stopMarker();
			}
			if (Annotate.drawState === Annotate.drawStateEnum.Note) {
				Annotate.stopNote();
			}
			Annotate.startEraseMode();
		}
	},

	colorPickTypeEnum : { None: 0, Select: 1, Draw: 2, Arrow: 3, Note: 4 },
	colorPickType : null,

	colorPicker : function() {
		Annotate.stopAnnotations();
		if( this.id === "colorSelect" || $(this).hasClass( 'colorSelect' ) ) {
			Annotate.colorPickType = Annotate.colorPickTypeEnum.Select;
			Annotate.currentColor = Annotate.selectColor;
		} else if( this.id === "colorArrow" || $(this).hasClass( 'colorArrow' ) ) {
			Annotate.colorPickType = Annotate.colorPickTypeEnum.Arrow;
			Annotate.currentColor = Annotate.arrowColor;
		} else if( this.id === "colorNote" || $(this).hasClass( 'colorNote' ) ) {
			Annotate.colorPickType = Annotate.colorPickTypeEnum.Note;
			Annotate.currentColor = Annotate.noteColor;
		} else {
			Annotate.colorPickType = Annotate.colorPickTypeEnum.Draw;
			Annotate.currentColor = Annotate.drawColor;
		}
		Annotate.clearSelection();
		window.top.navBar.canHideToolbar( false );
		var defaultColor = "#ff0000";
		var userType = window.top.webApp.getS( 'user.userType' );
		if( userType === 'W' || userType === 'WM' ) {
			defaultColor = "#0090FF";
		} else if( Annotate.colorPickType == Annotate.colorPickTypeEnum.Select ) {
			defaultColor = "#FFF600";
		}
		window.top.showColorPicker( this.offsetLeft, 100, Annotate.currentColor, Annotate.colorPickerClose, defaultColor, Annotate.colorPickType === Annotate.colorPickTypeEnum.Select );
	},

	colorPickerClose : function(currentColor) {
		if (Annotate.colorPickType === Annotate.colorPickTypeEnum.Select) {
			Annotate.selectColor = currentColor;
//			Annotate.startSelect(); // old selection model
			Annotate.startMarker();
		} else if (Annotate.colorPickType === Annotate.colorPickTypeEnum.Arrow) {
			Annotate.arrowColor = currentColor;
			if (Annotate.drawState !== Annotate.drawStateEnum.Arrow) {
				Annotate.startArrow();
			}
		} else if (Annotate.colorPickType === Annotate.colorPickTypeEnum.Note) {
			Annotate.noteColor = currentColor;
			if (Annotate.drawState !== Annotate.drawStateEnum.Note) {
				Annotate.selectNotePosition();
			}
		} else {
			if (Annotate.drawState === Annotate.drawStateEnum.Draw && Annotate.drawColor !== currentColor) {
				// Changing the color creates a new draw object for line drawing.
				Annotate.toggleDrawing();
			}
			Annotate.drawColor = currentColor;
			// Start new draw object.
			Annotate.toggleDrawing();
		}
		Annotate.updateUndoButton();
	},

	startSelect : function() {
		if (Annotate.drawState === Annotate.drawStateEnum.Select) {
			Annotate.stopSelect();
			return;
		}
		if (Annotate.drawState === Annotate.drawStateEnum.Draw) {
			Annotate.toggleDrawing();
		}
		if( Annotate.drawState === Annotate.drawStateEnum.Arrow) {
			Annotate.stopArrow();
		}
		if (Annotate.drawState === Annotate.drawStateEnum.Marker) {
			Annotate.stopMarker();
		}
		Annotate.stopEraseMode();
		Annotate.stopNote();
		Annotate.clearSelection();

		window.top.navBar.canHideToolbar( false );
		Annotate.drawState = Annotate.drawStateEnum.Select;
		Annotate.selectRect = {x1: 0, y1: 0, x2: 0, y2: 0};
		Annotate.selectedNodes = [];
		Annotate.selectedStartNode = {node: null, offset: 0, div: null};
		Annotate.selectedEndNode = {node: null, offset: 0, div: null};
		// IE 10 does not let the user select the textlayer, so we go up the chain.
		$('.textLayer').parent().each(function() {
			$(this).mousedown(Annotate.selectDown);
			$(this).mouseup(Annotate.stopSelect);
			$(this).on("touchstart", Annotate.touchSelectDown);
			$(this).on("touchend", Annotate.stopSelect);
		});
		// By adding #content, both buttons will be updated.
		$("#content #textSelect", parent.document).addClass("selected");
		window.top.navBar.textSelectButton.addClass( 'selected' );
		$(Annotate.scrollDiv).css("overflow", "hidden");
		Annotate.stopModify();
	},

	stopSelect : function() {
		if (Annotate.drawState === Annotate.drawStateEnum.Select) {
			$('.textLayer').parent().each(function() {
				$(this).off();
				this.removeEventListener("touchmove", Annotate.touchSelectMove, false);
			});
			window.top.navBar.canHideToolbar( true );
			Annotate.drawState = Annotate.drawStateEnum.None;
			$("#content #textSelect", parent.document).removeClass("selected");
			window.top.navBar.textSelectButton.removeClass( 'selected' );
			// Get canvas over text.
			var canvas = $(this).find('.annotate')[0];
			if (isset( canvas )) {
				var canvasObj = Annotate.findCanvasObj(canvas);
				if (canvasObj !== null) {
					Annotate.canvas = canvasObj.canvas;
					Annotate.ctx = canvasObj.ctx;
					Annotate.selectText(canvasObj.ID);
				}
			}
			$(Annotate.scrollDiv).css("overflow", "auto");
			Annotate.startModify();
		}
	},

	touchSelectDown : function(e) {
		e.preventDefault();
		var annotateEvent = {currentTarget: e.currentTarget, clientX: e.originalEvent.targetTouches[0].clientX, clientY: e.originalEvent.targetTouches[0].clientY};
		Annotate.selectDown(annotateEvent);
	},

	selectDown : function (event) {
		var textLayer = $(event.currentTarget).children('.textLayer');
		var obj = textLayer[0];
		Annotate.showTextMessage = true;
		var offset = {x: 0, y: 0};
		while((obj = obj.offsetParent) !== null) {
			offset.x += obj.offsetLeft + obj.clientLeft;
			offset.y += obj.offsetTop + obj.clientTop;
		}
		Annotate.selectRect.x1 = event.clientX - offset.x + Annotate.scrollDiv.scrollLeft();
		Annotate.selectRect.y1 = event.clientY - offset.y + Annotate.scrollDiv.scrollTop();

		// See if the document is rotated.  If so, do not let them select text.
		var style = $(textLayer[0].childNodes[0]).attr("style");
		if (typeof style !== 'undefined') {
			var rotateIndex = style.indexOf("rotate(");
			if (rotateIndex > 0) {
				// Pull out the rotation value.
				var rotate = parseInt(style.substr(rotateIndex+7));
				if (rotate !== 0) {
					parent.parent.showTooltip( "Can't highlight text in a rotated document.", true );
					Annotate.stopSelect();
					return;
				}
			}
		}
		$(event.currentTarget).mousemove(Annotate.selectMove);
		$(event.currentTarget).mouseup(Annotate.selectOff);
		event.currentTarget.addEventListener("touchmove", Annotate.touchSelectMove, false);
		$(event.currentTarget).on("touchleave", Annotate.selectOff);
	},

	updateRange : function(range, rect) {
//		range.x1 = Math.min(range.x1, rect.x1);
		range.y1 = Math.min(range.y1, rect.y1);
//		range.x2 = Math.max(range.x2, rect.x2);
		range.y2 = Math.max(range.y2, rect.y2);
	},

	touchSelectMove : function(e) {
		e.preventDefault();
		var annotateEvent = {currentTarget: e.currentTarget, clientX: e.targetTouches[0].clientX, clientY: e.targetTouches[0].clientY};
		Annotate.selectMove(annotateEvent);
	},

	selectMove : function(event) {
		var textLayer = $(event.currentTarget).children('.textLayer');
		var i = 0;
		if (Annotate.selectMoveOld.x === event.clientX && Annotate.selectMoveOld.y === event.clientY) {
			// Skip checking of selected text if the mouse has not moved.
			return;
		} else {
			Annotate.selectMoveOld = {x: event.clientX, y: event.clientY };
		}
		var obj = textLayer[0];
		var offset = {x: 0, y: 0};
		while((obj = obj.offsetParent) !== null) {
			offset.x += obj.offsetLeft + obj.clientLeft;
			offset.y += obj.offsetTop + obj.clientTop;
		}
		Annotate.selectRect.x2 = event.clientX - offset.x + Annotate.scrollDiv.scrollLeft();
		Annotate.selectRect.y2 = event.clientY - offset.y + Annotate.scrollDiv.scrollTop();

		var lineY = Number.MAX_VALUE;
		if (Annotate.selectedStartNode.div !== null) {
			$(Annotate.selectedStartNode.div).remove();
			Annotate.selectedStartNode.div = null;
		}
		if (Annotate.selectedEndNode.div !== null) {
			$(Annotate.selectedEndNode.div).remove();
			Annotate.selectedEndNode.div = null;
		}
		// Clear any old selections.
		if (Annotate.selectedNodes.length > 0) {
			for(i = 0; i < Annotate.selectedNodes.length; i++) {
				$(Annotate.selectedNodes[i]).css("background-color", "transparent");
			}
		}
		Annotate.selectedNodes = [];
		var selectedRange = {x1: Annotate.selectRect.x1, x2: Annotate.selectRect.x2, y1: Annotate.selectRect.y1, y2: Annotate.selectRect.y2};
		for(i = 0, ii = textLayer[0].childNodes.length; i < ii; i++) {
			var n = textLayer[0].childNodes[i];
			var nRect = {x1: n.offsetLeft / Annotate.deviceRatio, y1: n.offsetTop / Annotate.deviceRatio,
						 x2: ( n.offsetLeft + n.offsetWidth ) / Annotate.deviceRatio, y2: ( n.offsetTop + n.offsetHeight ) / Annotate.deviceRatio};
			// Test for start and end nodes.
			if (nRect.x1 <= Annotate.selectRect.x1 && Annotate.selectRect.x1 <= nRect.x2  &&
				nRect.y1 <= Annotate.selectRect.y1 && Annotate.selectRect.y1 <= nRect.y2) {

				Annotate.selectedStartNode.node = n;
				Annotate.selectedStartNode.offset = Annotate.selectRect.x1 - nRect.x1;
				var width = parseFloat(parseFloat($(n).attr("data-canvas-width")).toFixed(1));
				if (isNaN(width)) {
					width = n.offsetWidth
				}

				var partNode = $('<div width="'+(width - Annotate.selectedStartNode.offset)+'" height="'+n.offsetHeight+'">&nbsp;</div>');
				partNode.css({ 'left': n.offsetLeft + Annotate.selectedStartNode.offset, 'top': n.offsetTop, 'width': width - Annotate.selectedStartNode.offset, 'height': n.offsetHeight,
					'position': 'absolute', 'background-color': 'rgba(0,0,255,0.4)' });
				Annotate.selectedStartNode.div = partNode;
				Annotate.selectedNodes.push(n);
				Annotate.updateRange(selectedRange, nRect);
			} else if (nRect.x1 <= Annotate.selectRect.x2 && Annotate.selectRect.x2 <= nRect.x2  &&
				nRect.y1 <= Annotate.selectRect.y2 && Annotate.selectRect.y2 <= nRect.y2) {

				Annotate.selectedEndNode.node = n;
				Annotate.selectedEndNode.offset = Annotate.selectRect.x2 - nRect.x1;
				var width = parseFloat(parseFloat($(n).attr("data-canvas-width")).toFixed(1));
				if (isNaN(width)) {
					width = n.offsetWidth
				}

				var partNode = $('<div width="'+Annotate.selectedEndNode.offset+'" height="'+n.offsetHeight+'">&nbsp;</div>');
				partNode.css({ 'left': n.offsetLeft, 'top': n.offsetTop, 'width': Annotate.selectedEndNode.offset, 'height': n.offsetHeight,
					'position': 'absolute', 'background-color': 'rgba(0,0,255,0.4)' });
				Annotate.selectedEndNode.div = partNode;
				Annotate.selectedNodes.push(n);
				Annotate.updateRange(selectedRange, nRect);
			// Test for nodes inside of the selection.
			} else if ((selectedRange.x1 <= nRect.x1 && nRect.x1 <= selectedRange.x2) &&
				(nRect.x2 <= selectedRange.x2) &&
				(selectedRange.y1 <= nRect.y1 && nRect.y1 <= selectedRange.y2) &&
				(nRect.y2 <= selectedRange.y2)) {

				$(n).css("background-color", "rgba(0,0,255,0.4)");
				Annotate.selectedNodes.push(n);
				lineY = Math.min(lineY, nRect.y2);
				Annotate.updateRange(selectedRange, nRect);
//				Annotate.drawRect(nRect.x1, nRect.y1, nRect.x2 - nRect.x1, nRect.y2 - nRect.y1);
//				console.log('nRect: ' + nRect.x1 + ', ' + nRect.y1);
			}
			// Test for nodes that intersect the selection rect.
			else if (selectedRange.x1 <= nRect.x2 && selectedRange.x2 >= nRect.x1 && selectedRange.y1 <= nRect.y2 && selectedRange.y2 >= nRect.y1) {
				$(n).css("background-color", "rgba(0,0,255,0.4)");
				Annotate.selectedNodes.push(n);
				lineY = Math.min(lineY, nRect.y2);
				Annotate.updateRange(selectedRange, nRect);
		}
			// Also add any nodes that are to the right of the selection and in the middle of the selection.
			else if (selectedRange.x2 <= nRect.x1 && (selectedRange.y1 <= nRect.y1 || (nRect.y1 <= selectedRange.y1 && selectedRange.y1 <= nRect.y2)) && (selectedRange.y2 - 5) >= nRect.y2) {
				$(n).css("background-color", "rgba(0,0,255,0.4)");
				Annotate.selectedNodes.push(n);
				lineY = Math.min(lineY, nRect.y2);
				Annotate.updateRange(selectedRange, nRect);
			}
			// Also add any nodes that are to the left of the selection, below any selected line, and in the middle of the selection.
			else if (nRect.x1 <= selectedRange.x1 && lineY <= nRect.y1 && selectedRange.y1 <= nRect.y1 && nRect.y1 <= selectedRange.y2 && (selectedRange.y2 <= nRect.y2 || selectedRange.y2 >= nRect.y2)) {
				$(n).css("background-color", "rgba(0,0,255,0.4)");
				Annotate.selectedNodes.push(n);
				Annotate.updateRange(selectedRange, nRect);
			}
		}
		// Add these display nodes outside of the loop so we don't process them again as part of the selection test.
		if (Annotate.selectedStartNode.div !== null) {
			$(textLayer).append(Annotate.selectedStartNode.div);
		}
		if (Annotate.selectedEndNode.div !== null) {
			$(textLayer).append(Annotate.selectedEndNode.div);
		}
		// Check for no selectable text.
		if (Annotate.selectedNodes.length === 0 && Annotate.showTextMessage) {
			var dist = Math.sqrt(Math.pow((selectedRange.x1 - selectedRange.x2), 2) + Math.pow((selectedRange.y1 - selectedRange.y2), 2));
			if (dist > Annotate.selectTextThreshold) {
				parent.parent.showTooltip( 'No text found to select.', true );
				Annotate.showTextMessage = false;
			}
		}
	},

	selectOff : function(event) {
		$(this).off("mousemove");
		$(this).off("mouseup");
		this.removeEventListener("touchmove", Annotate.touchSelectMove, false);
		$(this).off("touchleave");
	},

	splitColor : function(color) {
		if (typeof color === "string") {
			if (color[0] === "#") {
				return Annotate.hexToRgb(color);
			} else if (color[0] === "r") {
				return Annotate.splitRgb(color);
			}
		}
		return null;
	},

	hexToRgb : function(hex) {
		var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
		return result ? {
			r: parseInt(result[1], 16),
			g: parseInt(result[2], 16),
			b: parseInt(result[3], 16)
		} : null;
	},

	rgbToHex : function (r, g, b) {
		return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
	},

	rgbaToHex : function (r, g, b, a) {
		return "#" + (a * 255).toString(16) + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
	},

	intToHex : function ( i ) {
		var hex = i.toString( 16 );
		while (hex.length < 6) hex = "0" + hex;
		if (hex.length === 8) {
			// Remove alpha at the front.
			hex = hex.substring(2);
		}
		return '#' + hex;
	},

	hexToInt : function( h ) {
		if (h[0] === '#') {
			h = h.substring( 1 );
		}
		return parseInt( h, 16 );
	},

	colorToInt : function( c ) {
		// This will convert any color to an Int value.
		if (c[0] === '#') {
			return Annotate.hexToInt( c );
		}
		var splitColor;
		if (c.substring(0, "rgba".length) == "rgba") {
			splitColor = Annotate.splitRgb( c );
			return Annotate.hexToInt( Annotate.rgbToHex( splitColor.r, splitColor.g, splitColor.b ) );
		}
		if (c.substring(0, "rgb".length) == "rgb") {
			splitColor = Annotate.splitRgb( c );
			return Annotate.hexToInt( Annotate.rgbToHex( splitColor.r, splitColor.g, splitColor.b ) );
		}
		return null;
	},

	splitRgb : function(color) {
        var colors;
		if (color.substring(0, "rgba".length) == "rgba") {
			colors = color.substring(5, color.length - 1 ).split(",");
			return {
				r: parseInt(colors[0].trim(), 10),
				g: parseInt(colors[1].trim(), 10),
				b: parseInt(colors[2].trim(), 10),
				a: parseFloat(colors[3].trim())
				};
		}
        colors = color.substring(4, color.length - 1 ).split(",");
        return {
            r: parseInt(colors[0].trim(), 10),
            g: parseInt(colors[1].trim(), 10),
            b: parseInt(colors[2].trim(), 10)
            };
	},

	formatPoint : function ( point, width, height, rotation ) {
//		console.log( 'annotate::formatPoint(', point[0], point[1], width, height, rotation, ')' );
		var newX = point[0];
		var newY = point[1];
		var newWidth = width;
		var newHeight = height;
		if( typeof rotation !== 'undefined' ) {
			switch( rotation ) {
				case 90:
					// coordinate system top-left
					newWidth = height;
					newHeight = width;
					newX = point[1];
					newY = newHeight - point[0];
					break;
				case 180:
					// coordinate system top-right
					newX = width - point[0];
					newY = height - point[1];
					break;
				case 270:
					// coordinate system bottom-right
					newWidth = height;
					newHeight = width;
					newX = newWidth - point[1];
					newY = point[0];
					break;
				default:
					// coordinate system bottom-left
					break;
			}
		}
//		console.log( 'annotate::formatPoint() --> newX = ', newX, ' newY = ', newY, ' newWidth = ', newWidth, ' newHeight = ', newHeight );
		var pointString = '{' + ( newX / newWidth ).toFixed(6) + ', ' + ( 1.0 - newY / newHeight ).toFixed(6) + '}';
		console.log( 'annotate::formatPoint() --> pointString', pointString );
		return pointString;
	},

	formatBoundPoint : function ( point, width, height ) {
		return '{' + ( point[0] / width ).toFixed(6) + ', ' + ( point[1] / height ).toFixed(6) + '}';
	},

	formatBounds : function ( boundPoints, canvasObj ) {
//		console.log( 'annotate::formatBounds( {{', boundPoints[0][0], boundPoints[0][1],'}{', boundPoints[1][0], boundPoints[1][1], '}}, ', canvasObj.width, ', ', canvasObj.height, ', ', canvasObj.rotation, ')' );
		var boundWidth = boundPoints[1][0] - boundPoints[0][0];
		var boundHeight = boundPoints[1][1] - boundPoints[0][1];
		var newX = boundPoints[0][0];
		var newY = boundPoints[0][1];
		var newWidth = canvasObj.width;
		var newHeight = canvasObj.height;
		switch( canvasObj.rotation ) {
			case 90:
				// coordinate system top-left
				boundWidth = boundPoints[1][1] - boundPoints[0][1];
				boundHeight = boundPoints[1][0] - boundPoints[0][0];
				newWidth = canvasObj.height;
				newHeight = canvasObj.width;
				newX = boundPoints[0][1];
				newY = newHeight - boundPoints[0][0] - boundHeight;
				break;
			case 180:
				// coordinate system top-right
				newX = canvasObj.width - boundPoints[0][0] - boundWidth;
				newY = canvasObj.height - boundPoints[0][1] - boundHeight;
				break;
			case 270:
				// coordinate system bottom-right
				boundWidth = boundPoints[1][1] - boundPoints[0][1];
				boundHeight = boundPoints[1][0] - boundPoints[0][0];
				newWidth = canvasObj.height;
				newHeight = canvasObj.width;
				newX = newWidth - boundPoints[0][1] - boundWidth;
				newY = boundPoints[0][0];
				break;
			default:
				// coordinate system bottom-left
				break;
		}
//		console.log( 'annotate::formatBounds() --> newX = ', newX, ' newY = ', newY, ' newWidth = ', newWidth, ' newHeight = ', newHeight );
		var pointString = '{{' + ( newX / newWidth ).toFixed(6) + ', ' + ( 1.0 - newY / newHeight ).toFixed(6) + '}, {' + (boundWidth / newWidth).toFixed(6) + ', ' + (boundHeight / newHeight).toFixed(6) + '}}';
		console.log( 'annotate::formatBounds() --> pointString', pointString );
		return pointString;
	},

	selectText : function(page) {
		var range = null;
		if (window.getSelection) {
			if (Annotate.selectedNodes.length === 0) {
				// The user selected nothing.
				return;
			}
			Annotate.currentShape = new DrawObject(Annotate.clickThreshold);
			Annotate.currentShape.page = page;
			Annotate.currentShape.setDrawType("highlight");
			var indexPage = parseInt( page ) - 1;
			Annotate.currentShape.index = Annotate.annotationIndex++;
			Annotate.currentShape.addObject();

			var rgb_value = Annotate.splitColor(Annotate.selectColor);
			if (rgb_value !== null) {
				Annotate.ctx.fillStyle = "rgba("+rgb_value.r+","+rgb_value.g+","+rgb_value.b+", 0.4)";
			} else {
				Annotate.ctx.fillStyle = "rgba(255, 255, 0, 0.4)";
			}
			for(var i = 0; i < Annotate.selectedNodes.length; i++) {
				var currentNode = Annotate.selectedNodes[i];
				$(currentNode).css("background-color", "transparent");

				var x, y, w, h = 0;
				x = currentNode.offsetLeft / Annotate.deviceRatio;
				y = currentNode.offsetTop / Annotate.deviceRatio;
				h = currentNode.offsetHeight / Annotate.deviceRatio;
				w = parseFloat(parseFloat($(currentNode).attr("data-canvas-width")).toFixed(1));
				if (isNaN(w)) {
					//IE does not have data-canvas-width, so parse the style to get the scale of the text.
					w = currentNode.offsetWidth / Annotate.deviceRatio;
					var transform = $(currentNode).attr("style");
					var index = transform.indexOf("scale(");
					if (index >= 0) {
						index += 6;	// Skip over the scale( in the string.
						var index2 = transform.indexOf(")", index);
						if (index2 >= 0) {
							var numbers = transform.substr(index, index2 - index).split(",");
							var xScale = parseFloat(numbers[0]);
							var yScale = parseFloat(numbers[1]);
							if (!isNaN(xScale)) {
								w = parseFloat((w * xScale).toFixed(1)) / Annotate.deviceRatio;
							}
							if (!isNaN(yScale)) {
								h = parseFloat((h * yScale).toFixed(1)) / Annotate.deviceRatio;
							}
						}
					}
				} else {
					w /=  Annotate.deviceRatio;
				}
				if (currentNode === Annotate.selectedStartNode.node) {
					//Ajust for a partial node selected.
					var scaleWidth = Annotate.selectedStartNode.offset;
					x += scaleWidth;
					w -= scaleWidth;
					$(Annotate.selectedStartNode.div).remove();
				} else if (currentNode === Annotate.selectedEndNode.node) {
					//Ajust for a partial node selected.
					var scaleWidth = Annotate.selectedEndNode.offset;
					w = scaleWidth;
					$(Annotate.selectedEndNode.div).remove();
				}
				Annotate.ctx.beginPath();
				Annotate.ctx.fillRect(x, y, w, h);
				Annotate.currentShape.addPoints(x, y, w, h);
				Annotate.ctx.closePath();
			}
			Annotate.currentShape.color = Annotate.ctx.fillStyle;
			Annotate.clearSelection();
//			console.log("Last Shape Length: " + Annotate.currentShape.pointLists.length);
			Annotate.drawList.push(Annotate.currentShape);
			Annotate.currentShape.createBoundingBox();
			Annotate.undoList.push( {operation: 'delete', obj: Annotate.currentShape} );
			Annotate.undoCounter++;
			Annotate.updateUndoButton();
//			Annotate.currentShape.debugShape();
			if (Annotate.iAmPresentor) {
				Annotate.sendHighlight( Annotate.currentShape );
			}
			Annotate.currentShape = null;
			if (window.top.Datacore.getVal(window.top.Datacore.s.currentUser, 'userType') === 'W' && !Annotate.needToSave && window.top.Datacore.getS('depositionStatus') !== 'F') {
				window.top.navBar.saveButton.removeClass( 'disabled' );
			}
			Annotate.needToSave = true;
		} else {
			alert("Browser is too old.  Text selection is not available.");
		}
	},

	sendHighlight : function( hightlightObj ) {
		var canvasObj = Annotate.findCanvasObjByID( hightlightObj.page );
		var quads = [];
		for (var i = 0; i < hightlightObj.pointLists[0].length; i++) {
			var points = [];
			points.push( [hightlightObj.pointLists[0][i][0], hightlightObj.pointLists[0][i][1]] );
			points.push( [hightlightObj.pointLists[0][i][0] + hightlightObj.pointLists[0][i][2], hightlightObj.pointLists[0][i][1]] );
			points.push( [hightlightObj.pointLists[0][i][0], hightlightObj.pointLists[0][i][1] + hightlightObj.pointLists[0][i][3]] );
			points.push( [hightlightObj.pointLists[0][i][0] + hightlightObj.pointLists[0][i][2], hightlightObj.pointLists[0][i][1] + hightlightObj.pointLists[0][i][3]] );
			var quad = [];
			for (var j = 0; j < points.length; j++) {
				quad.push( Annotate.formatPoint( points[j], canvasObj.width, canvasObj.height ) );
			}
			quads.push( quad );
		}
		var hightlightData = {Operation: 'add', Type: 5, Page: parseInt( hightlightObj.page ) - 1, Opacity: Math.floor( hightlightObj.opacity * 100 ),
							Color: Annotate.colorToInt( hightlightObj.color ), Index: hightlightObj.index, Quads: quads };

		parent.parent.addAnnotation( Annotate.presentationDepositionID, hightlightData );
	},

	clearSelection : function() {
		if ( window && window.getSelection ) {
			if (window.getSelection().removeAllRanges) {
				window.getSelection().removeAllRanges();
			} else if (window.getSelection().empty) {
				window.getSelection().empty();		// Safari
			}
		} else if (document.selection && document.selection.empty) {
			document.selection.empty();		// IE
		}
	},

	startArrow : function() {
		if (Annotate.drawState === Annotate.drawStateEnum.Arrow) {
			Annotate.stopArrow();
			return;
		}
		Annotate.stopAnnotations();
		window.top.navBar.canHideToolbar( false );
		Annotate.drawState = Annotate.drawStateEnum.Arrow;
		// By adding #content, both buttons will be updated.
		$("#content #arrow", parent.document).addClass("selected");
		window.top.navBar.arrowButton.addClass( 'selected' );
		$(Annotate.scrollDiv).css("overflow", "hidden");
		Annotate.startDrawing();
	},

	stopArrow : function() {
//		parent.setCanHideFullscreenToolBar( true );
		window.top.navBar.canHideToolbar( true );
		Annotate.drawState = Annotate.drawStateEnum.None;
		$("#content #arrow", parent.document).removeClass("selected");
		window.top.navBar.arrowButton.removeClass( 'selected' );
		$(Annotate.scrollDiv).css("overflow", "auto");
		Annotate.stopDrawing();
		Annotate.updateUndoButton();
	},

	findxyArrow : function(res, e) {
		e.preventDefault();
		e.stopPropagation();
		e.cancelBubble = true;
		window.top.lastEvent = e;
		var canvasObj = Annotate.findCanvasObj(e.target);
		if (canvasObj === null) {
			return;
		}
		if (res == 'down') {
			Annotate.clearSelection();
			Annotate.canvas = canvasObj.canvas;
			Annotate.ctx = canvasObj.ctx;
			Annotate.currX = e.clientX - canvasObj.offsetX + Annotate.scrollDiv.scrollLeft();
			Annotate.currY = e.clientY - canvasObj.offsetY + Annotate.scrollDiv.scrollTop();
			Annotate.prevX = Annotate.currX;
			Annotate.prevY = Annotate.currY;
			$(Annotate.canvas).css("cursor", "default");
			Annotate.flag = true;
			if (window.top.Datacore.getVal(window.top.Datacore.s.currentUser, 'userType') === 'W' && !Annotate.needToSave && window.top.Datacore.getS('depositionStatus') !== 'F') {
				window.top.navBar.saveButton.removeClass( 'disabled' );
			}
			Annotate.needToSave = true;
		}
		if (res == 'up' || res == "out") {
			if( Annotate.flag !== true ) {
				return;
			}
			Annotate.flag = false;
			$(Annotate.canvas).css("cursor", "auto");
			if (Annotate.currentShape === null) {
				Annotate.currentShape = new DrawObject(Annotate.clickThreshold);
				Annotate.currentShape.page = canvasObj.ID;
				Annotate.currentShape.color = Annotate.arrowColor;
				Annotate.currentShape.setDrawType("arrow", Annotate.lineWidth);
				var indexPage = parseInt(canvasObj.ID) - 1;
				Annotate.currentShape.index = Annotate.annotationIndex++;
			}
			Annotate.currentShape.addObject();
			Annotate.currentShape.addPoints(Annotate.prevX, Annotate.prevY);
			Annotate.currentShape.addPoints(Annotate.currX, Annotate.currY);
			// Save off shape and get ready to start a new shape.
			Annotate.drawList.push(Annotate.currentShape);
			Annotate.undoList.push( {operation: 'delete', obj: Annotate.currentShape} );
			Annotate.undoCounter++;
			Annotate.currentShape.createBoundingBox();
			Annotate.currentShape.updateDragPoints();
//			Annotate.currentShape.debugShape();
			if (Annotate.iAmPresentor) {
				Annotate.sendArrow( Annotate.currentShape );
			}
			Annotate.currentShape = null;
			if (res == 'up') {
				Annotate.stopArrow();
			}
		}
		if (res == 'move') {
			if (Annotate.flag) {
				Annotate.redraw();
				Annotate.canvas = canvasObj.canvas;
				Annotate.ctx = canvasObj.ctx;
				Annotate.currX = e.clientX - canvasObj.offsetX + Annotate.scrollDiv.scrollLeft();
				Annotate.currY = e.clientY - canvasObj.offsetY + Annotate.scrollDiv.scrollTop();
				Annotate.drawArrow();
			}
		}
	},

	drawArrow : function() {
		var lineAngle = Math.atan2( Annotate.currY - Annotate.prevY, Annotate.currX - Annotate.prevX );
		Annotate.ctx.beginPath();
		Annotate.ctx.moveTo( Annotate.prevX, Annotate.prevY );
		Annotate.ctx.lineTo( Annotate.currX, Annotate.currY );
		Annotate.ctx.moveTo( Annotate.currX, Annotate.currY );
		var x = Annotate.arrowHeadLength * Math.cos( lineAngle + Annotate.arrowHeadAngle ) + Annotate.currX;
		var y = Annotate.arrowHeadLength * Math.sin( lineAngle + Annotate.arrowHeadAngle ) + Annotate.currY;
		Annotate.ctx.lineTo( x, y );
		Annotate.ctx.moveTo(Annotate.currX, Annotate.currY);
		x = Annotate.arrowHeadLength * Math.cos( lineAngle - Annotate.arrowHeadAngle ) + Annotate.currX;
		y = Annotate.arrowHeadLength * Math.sin( lineAngle - Annotate.arrowHeadAngle ) + Annotate.currY;
		Annotate.ctx.lineTo( x, y );
		Annotate.ctx.strokeStyle = Annotate.arrowColor;
		Annotate.ctx.lineWidth = Annotate.lineWidth * PDFView.currentScale;
		Annotate.ctx.lineCap = 'square';
		Annotate.ctx.stroke();
		Annotate.ctx.closePath();
	},

	sendArrow : function( arrowObj ) {
		var canvasObj = Annotate.findCanvasObjByID( arrowObj.page );
		var start = Annotate.formatPoint( arrowObj.pointLists[0][0], canvasObj.width, canvasObj.height, canvasObj.rotation );
		var end = Annotate.formatPoint( arrowObj.pointLists[0][1], canvasObj.width, canvasObj.height, canvasObj.rotation );
		var boundingBox = Annotate.formatBounds( arrowObj.pointLists[0], canvasObj );
		var arrowData = {Operation: 'add', Type: 1, Page: parseInt( arrowObj.page ) - 1, Opacity: Math.floor( arrowObj.opacity * 100 ), Color: Annotate.colorToInt( arrowObj.color ),
						 Index: arrowObj.index, LineWidth: arrowObj.lineWidth / Annotate.scale, StartPoint: start, EndPoint: end, Bounds: boundingBox };

		parent.parent.addAnnotation( Annotate.presentationDepositionID, arrowData );
	},

	startMarker : function() {
		if (Annotate.drawState === Annotate.drawStateEnum.Marker) {
			Annotate.stopMarker();
			return;
		}
		Annotate.stopAnnotations();
		window.top.navBar.canHideToolbar( false );
		Annotate.drawState = Annotate.drawStateEnum.Marker;
		// By adding #content, both buttons will be updated.
		$("#content #textSelect", parent.document).addClass("selected");
		window.top.navBar.textSelectButton.addClass( 'selected' );
		$(Annotate.scrollDiv).css("overflow", "hidden");
		Annotate.startDrawing();
	},

	stopMarker : function() {
		window.top.navBar.canHideToolbar( true );
		Annotate.drawState = Annotate.drawStateEnum.None;
		$("#content #textSelect", parent.document).removeClass("selected");
		window.top.navBar.textSelectButton.removeClass( 'selected' );
		$(Annotate.scrollDiv).css("overflow", "auto");
		Annotate.stopDrawing();
		Annotate.updateUndoButton();
	},

	findxyMarker : function(res, e) {
		var canvasObj = Annotate.findCanvasObj(e.target);
		if (canvasObj === null) {
			return;
		}
		if (res == 'down') {
			Annotate.clearSelection();
			Annotate.canvas = canvasObj.canvas;
			Annotate.ctx = canvasObj.ctx;
			Annotate.currX = e.clientX - canvasObj.offsetX + Annotate.scrollDiv.scrollLeft();
			Annotate.currY = e.clientY - canvasObj.offsetY + Annotate.scrollDiv.scrollTop();
			Annotate.prevX = Annotate.currX;
			Annotate.prevY = Annotate.currY;
			$(Annotate.canvas).css("cursor", "default");
			Annotate.flag = true;
			if (window.top.Datacore.getVal(window.top.Datacore.s.currentUser, 'userType') === 'W' && !Annotate.needToSave && window.top.Datacore.getS('depositionStatus') !== 'F') {
				window.top.navBar.saveButton.removeClass( 'disabled' );
			}
			Annotate.needToSave = true;
		}
		if (res == 'up' || res == 'out' ) {
			if( Annotate.flag !== true ) {
				return;
			}
			Annotate.flag = false;
			if ( Annotate.currentShape === null ) {
				Annotate.currentShape = new DrawObject(Annotate.clickThreshold);
				Annotate.currentShape.page = canvasObj.ID;
				Annotate.currentShape.color = Annotate.selectColor;
				Annotate.currentShape.setDrawType("marker", Annotate.markerWidth);
				Annotate.currentShape.index = Annotate.annotationIndex++;
				Annotate.currentShape.opacity = .35; // default marker opacity value
				Annotate.currentShape.addObject();
				Annotate.currentShape.addPoints(Annotate.prevX, Annotate.prevY);
				Annotate.currentShape.addPoints(Annotate.currX, Annotate.currY);
				// Save off shape and get ready to start a new shape.
				Annotate.drawList.push(Annotate.currentShape);
				Annotate.undoList.push( {operation: 'delete', obj: Annotate.currentShape} );
				Annotate.undoCounter++;
				Annotate.currentShape.createBoundingBox();
				Annotate.currentShape.updateDragPoints();
	//			Annotate.currentShape.debugShape();
				if (Annotate.iAmPresentor ) {
					Annotate.sendMarker( Annotate.currentShape );
				}
				Annotate.updateUndoButton();
			}
			Annotate.currentShape = null;
			$(Annotate.canvas).css("cursor", "auto");
		}
		if (res == 'move') {
			if (Annotate.flag) {
				Annotate.redraw();
				Annotate.canvas = canvasObj.canvas;
				Annotate.ctx = canvasObj.ctx;
				Annotate.currX = e.clientX - canvasObj.offsetX + Annotate.scrollDiv.scrollLeft();
				Annotate.currY = e.clientY - canvasObj.offsetY + Annotate.scrollDiv.scrollTop();
				Annotate.drawMarker();
			}
		}
	},

	drawMarker : function() {
		var markerOpacity = .35; // default marker opacity value
		var lineAngle = Math.atan2( Annotate.currY - Annotate.prevY, Annotate.currX - Annotate.prevX );
		Annotate.ctx.beginPath();
		Annotate.ctx.moveTo( Annotate.prevX, Annotate.prevY );
		Annotate.ctx.lineTo( Annotate.currX, Annotate.currY );
		Annotate.ctx.lineWidth = Annotate.markerWidth * PDFView.currentScale;
		Annotate.ctx.lineCap = 'butt';
		var rgb_value = Annotate.splitColor( Annotate.selectColor );
		if (rgb_value !== null) {
			Annotate.ctx.strokeStyle = 'rgba(' + rgb_value.r + ',' + rgb_value.g + ',' + rgb_value.b + ',' + markerOpacity + ')';
		} else {
			Annotate.ctx.strokeStyle = 'rgba(255, 255, 0, ' + markerOpacity + ')';
		}
		Annotate.ctx.stroke();
		Annotate.ctx.closePath();
	},

	sendMarker : function( markerObj ) {
		var canvasObj = Annotate.findCanvasObjByID( markerObj.page );
		var start = Annotate.formatPoint( markerObj.pointLists[0][0], canvasObj.width, canvasObj.height, canvasObj.rotation );
		var end = Annotate.formatPoint( markerObj.pointLists[0][1], canvasObj.width, canvasObj.height, canvasObj.rotation );
		var boundingBox = Annotate.formatBounds( markerObj.pointLists[0], canvasObj );
		var markerData = {Operation: 'add', Type: 5, Page: parseInt( markerObj.page ) - 1, Opacity: Math.floor( markerObj.opacity * 100 ), Color: Annotate.colorToInt( markerObj.color ),
						 Index: markerObj.index, LineWidth: markerObj.lineWidth / Annotate.scale, StartPoint: start, EndPoint: end, Bounds: boundingBox };

		parent.parent.addAnnotation( Annotate.presentationDepositionID, markerData );
	},

	selectNotePosition : function() {
		if (Annotate.drawState === Annotate.drawStateEnum.Draw) {
			Annotate.toggleDrawing();
		}
		if (Annotate.drawState === Annotate.drawStateEnum.Note) {
			Annotate.stopNote();
			return;
		}
		Annotate.stopEraseMode();
		Annotate.stopSelect();
		Annotate.stopArrow();
		Annotate.stopMarker();
		Annotate.clearSelection();
		window.top.navBar.canHideToolbar( false );
		Annotate.drawState = Annotate.drawStateEnum.Note;
		// By adding #content, both buttons will be updated.
		$("#content #note", parent.document).addClass("selected");
		window.top.navBar.noteButton.addClass( 'selected' );
		$(Annotate.scrollDiv).css("overflow", "hidden");

		var i = 0, len = Annotate.canvasList.length;
		for(i = 0; i < len; i++) {
			Annotate.canvasList[i].canvas.addEventListener("mouseup", Annotate.noteMouseUp, false);
			Annotate.canvasList[i].canvas.addEventListener("touchend", Annotate.noteTouchEnd, false);
		}
	},

	stopNote : function() {
		window.top.navBar.canHideToolbar( true );
		Annotate.drawState = Annotate.drawStateEnum.None;
		$("#content #note", parent.document).removeClass("selected");
		window.top.navBar.noteButton.removeClass( 'selected' );
		$(Annotate.scrollDiv).css("overflow", "auto");
		Annotate.updateUndoButton();
		Annotate.unhookNoteEvents();
	},

	unhookNoteEvents : function() {
		var i = 0, len = Annotate.canvasList.length;
		for(i = 0; i < len; i++) {
			Annotate.canvasList[i].canvas.removeEventListener("mouseup", Annotate.noteMouseUp, false);
			Annotate.canvasList[i].canvas.removeEventListener("touchend", Annotate.noteTouchEnd, false);
		}
	},

	noteMouseUp : function(e) {
		Annotate.clearSelection();
		var canvasObj = Annotate.findCanvasObj(e.target);
		if (canvasObj === null) {
			return;
		}
		Annotate.canvas = canvasObj.canvas;
		Annotate.ctx = canvasObj.ctx;
		Annotate.currX = e.clientX - canvasObj.offsetX + Annotate.scrollDiv.scrollLeft();
		Annotate.currY = e.clientY - canvasObj.offsetY + Annotate.scrollDiv.scrollTop();
		// Center icon
		Annotate.currX -= 18;
		Annotate.currY -= 18;
		$(Annotate.canvas).css("cursor", "default");
		if (Annotate.currentShape === null) {
			Annotate.currentShape = new DrawObject(Annotate.clickThreshold);
			Annotate.currentShape.page = canvasObj.ID;
			Annotate.currentShape.color = Annotate.noteColor;
			Annotate.currentShape.author = Annotate.getUsername( window.top.Datacore.getS('userID') );
			Annotate.currentShape.setDrawType( "note", 1 );
			var indexPage = parseInt(canvasObj.ID) - 1;
			Annotate.currentShape.index = Annotate.annotationIndex++;
		}
		Annotate.currentShape.addObject();
		Annotate.currentShape.addPoints(Annotate.currX, Annotate.currY);
		Annotate.flag = true;
		if ( window.top.Datacore.getVal(window.top.Datacore.s.currentUser, 'userType') === 'W' && !Annotate.needToSave && window.top.Datacore.getS('depositionStatus') !== 'F') {
			window.top.navBar.saveButton.removeClass( 'disabled' );
		}
		Annotate.needToSave = true;
		Annotate.unhookNoteEvents();
		Annotate.showNoteEntry();
	},

	showNoteEntry : function() {
		parent.parent.showDialogNoteEntry( Annotate.currentShape.author, '', '');
		parent.parent.setDialogNoteEntryCloseAction( Annotate.closeNoteEntry );
	},

	convertDate2NoteString : function( date ) {
		var localDate = new Date( date );
		var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
		var result = monthNames[ localDate.getMonth() ] + ' ' + localDate.getDate() + ', ' + localDate.getFullYear() + ', ';
		var hour = localDate.getHours();
		var meridian = 'AM';
		if (hour >= 13) {
			hour -= 12;
			meridian = 'PM';
		} else if (hour == 0) {
			hour += 12;
		}
		result += hour + ':' + localDate.getMinutes() + ' ' + meridian;

		return result;
	},

	closeNoteEntry : function( note, trash ) {
		Annotate.flag = false;
		if (!trash) {
			if (note.length === 0) {
				// Nothing has changed in the text, so don't save anything.
				Annotate.currentShape = null;
				Annotate.stopNote();
				return;
			}
			Annotate.currentShape.date = Date.now();
			Annotate.currentShape.note = note;
			Annotate.drawList.push( Annotate.currentShape );
			Annotate.undoList.push( {operation: 'delete', obj: Annotate.currentShape} );
			Annotate.undoCounter++;
			Annotate.currentShape.createBoundingBox();
			Annotate.drawNoteIcon( Annotate.currX, Annotate.currY, Annotate.currentShape.color );
			Annotate.addNoteMouseOver( Annotate.currentShape );
			if (Annotate.presentationHost) {
				Annotate.sendNote( Annotate.currentShape );
			}
		}
		Annotate.currentShape = null;
		Annotate.stopNote();
	},

	sendNote : function( noteObj ) {
		var canvasObj = Annotate.findCanvasObjByID( noteObj.page );
		var bounds = '{' + Annotate.formatPoint( noteObj.pointLists[0][0], canvasObj.width, canvasObj.height ) + ',{0.030, 0.0225}}';
		var noteData = {Operation: 'add', Type: 8, Page: parseInt( noteObj.page ) - 1, Opacity: Math.floor( noteObj.opacity * 100 ), Color: Annotate.colorToInt( noteObj.color ),
						 Index: noteObj.index, Bounds: bounds, Content: noteObj.note };

		parent.parent.addAnnotation( Annotate.presentationDepositionID, noteData );
	},

	drawNoteIcon : function(currX, currY, noteColor) {
		var pageContainer = $(Annotate.canvas).parents( '.page ');
		var scale = $(pageContainer).width() / 947;
		Annotate.ctx.fillStyle = noteColor;
		Annotate.ctx.fillRect(currX + 1, currY + 1, 32 * scale, 25 * scale);
		Annotate.ctx.beginPath();
		Annotate.ctx.moveTo(currX + 2 * scale, currY + 0.5 * scale);
		Annotate.ctx.lineTo(currX + 32 * scale, currY + 0.5 * scale);
		Annotate.ctx.moveTo(currX + 0.5 * scale, currY + 2 * scale);
		Annotate.ctx.lineTo(currX + 0.5 * scale, currY + 25 * scale);
		Annotate.ctx.moveTo(currX + 33.5 * scale, currY + 2 * scale);
		Annotate.ctx.lineTo(currX + 33.5 * scale, currY + 25 * scale);
		Annotate.ctx.moveTo(currX + 2 * scale, currY + 26.5 * scale);
		Annotate.ctx.lineTo(currX + 32 * scale, currY + 26.5 * scale);
		var i;
		for(i = 0; i < 4 * scale; i++) {
			Annotate.ctx.moveTo(currX + 5 * scale, currY + 27.5 * scale + i);
			Annotate.ctx.lineTo(currX + 14 * scale - i, currY + 27.5 * scale + i);
		}
		for(i = 0; i < 3 * scale; i++) {
			Annotate.ctx.moveTo(currX + 6 * scale, currY + 31.5 * scale + i);
			Annotate.ctx.lineTo(currX + 10 * scale - i, currY + 31.5 * scale + i);
		}
		Annotate.ctx.strokeStyle = noteColor;
		Annotate.ctx.lineWidth = 1;
		Annotate.ctx.stroke();
		var image = new Image();
		image.src = '/img/ipad/icons/note-icon.png';
		image.onload = function() {
			Annotate.ctx.drawImage(image, currX, currY, 34 * scale, 34 * scale);
		}
	},

	addNoteMouseOver : function( noteObject ) {
		//Add annotation layer
		var pageContainer = $(Annotate.canvas).parents( '.page ');
		if ($(pageContainer).children( '.annotationLayer').length < 1) {
			$(pageContainer).append( '<div class="annotationLayer"></div>' );
		}
		var annotationLayer = $(pageContainer).children( '.annotationLayer');
		var noteID = 'note_' + noteObject.date;

		//Add note hit spot and note.
		$(annotationLayer).append( '<section id="' + noteID + '" class="annotText"></section>');
		var annotText = $(annotationLayer).children( '#' + noteID );
		var scale = $(pageContainer).width() * 1.54 / 947;
		var scaleAdjustment = 0;
		// Ajust the click box due to matrix scaling.
		if (scale < 1) {
			scaleAdjustment = 0;
		} else {
			scaleAdjustment = (scale - 1) * 10;
		}
		$(annotText).css({'width': '20px', 'height': '20px', 'position': 'absolute', 'left': ( noteObject.pointLists[0][0][0] + scaleAdjustment )  + 'px',
							'top': ( noteObject.pointLists[0][0][1] + scaleAdjustment ) + 'px' } );
		$(annotText).css('-webkit-transform', 'matrix(' + scale + ', 0, 0, ' + scale + ', 0, 0)');
		$(annotText).append( '<div class="annotTextContentWrapper" style="left: 27px; top: -10px;"><div class="annotTextContent" style="display: none;"></div></div>');
		var annotTextContent = $(annotText).find( '.annotTextContent' );
		$(annotTextContent).css("background-color", noteObject.color);
		$(annotTextContent).append( '<h1>' + noteObject.author + '</h1>' );
		var text = '<p><span>';
		var lines = noteObject.note.split(/\r\n|\r|\n/g);
		for (var l in lines) {
			text += lines[l] + '<br />';
		}
		text += '</span></p>';
		$(annotTextContent).append( text );

		//Add note mouse over.
		$(annotText).mouseenter(function(e) {
			var x = $(e.target).find( '.annotTextContent');
			$(e.target).find( '.annotTextContent').show();
		});
		$(annotText).mouseleave(function(e) {
			$(e.target).find( '.annotTextContent').hide();
		});
	},

	removeNoteMouseOver : function( noteObject ) {
		//Remove hit spot and note.
		var noteID = 'note_' + noteObject.date;
		var canvasObj = Annotate.findCanvasObjByID( noteObject.page );
		var pageContainer = $(canvasObj.canvas).parents( '.page ' );
		var annotationLayer = $(pageContainer).children( '.annotationLayer' );
		var annotText = $(annotationLayer).children( '#' + noteID );
		$(annotText).remove();
	},

	stopAnnotations : function() {
		switch( Annotate.drawState ) {
			case Annotate.drawStateEnum.Draw:
				Annotate.toggleDrawing();
				break;
			case Annotate.drawStateEnum.Erase:
				Annotate.stopEraseMode();
				break;
			case Annotate.drawStateEnum.Arrow:
				Annotate.stopArrow();
				break;
			case Annotate.drawStateEnum.Marker:
				Annotate.stopMarker();
				break;
			case Annotate.drawStateEnum.Note:
				Annotate.stopNote();
				break;
			case Annotate.drawStateEnum.Select:
				Annotate.stopSelect();
				break;
			case Annotate.drawStateEnum.None:
				if( $('#menuModifyAnnotation').is(':visible') ) {
					$('#menuModifyAnnotation').trigger( 'mousedown' );
				}
				break;
		}
		//Annotate.stopSelect();
		Annotate.clearSelection();
	},

	getSaveFilename : function() {
		if( Annotate.processing ) {
			return;
		}

		Annotate.stopAnnotations();
		var filename = Annotate.updatedFilename;
		if( filename === null ) {
			var fileID = (window.top.Datacore.s.inPresentation) ? Annotate.fileID : parent.requestedFileID;
			var dcFile = window.top.Datacore.getFile( fileID );
			if( typeof dcFile === 'undefined' || dcFile === null ) {
				console.error( 'missing file', fileID, dcFile );
				return;
			}
			var index = dcFile.name.lastIndexOf( "." );
			filename = dcFile.name.substr( 0, index );
			Annotate.updatedFilename = filename;
		}
		if( Annotate.personalFolderID === null ) {
			Annotate.getPersonalFolderID();
		}
		window.top.setDialogSaveFileCloseAction( Annotate.validateFilename );
		window.top.showDialogSaveFile( "Name Your Copy", "Save", filename );
	},

	validateFilename : function( filename, folderID ) {
		var folder;
		if (isset( folderID )) {
			folder = window.top.Datacore.getFolder(folderID);
		} else {
			folder = window.top.Datacore.getFolder(Annotate.personalFolderID);
		}
		if (folder === null) {
			return;
		}
		var files = folder.getFiles(folder.getFileIDs());
		var duplicate = false;
		var duplicateFileID = 0;
		for(var f in files) {
			var shortName = files[f].name;
			var index = shortName.lastIndexOf(".");
			if (index >= 0) {
				shortName = shortName.substr(0, index);
			}
			if (shortName.toLowerCase() === filename.toLowerCase()) {
				duplicate = true;
				duplicateFileID = parseInt(files[f].ID, 10);
				break;
			}
		}
		if (duplicate) {
			//Confirm overwrite.
			var dialogButtons = [];
			dialogButtons.push( {label: 'Cancel', action : 'dismissDialog3Btn();', class : 'btn_app_cancel'} );
			dialogButtons.push( {label: 'Replace', id : 'replace', class : 'btn_app_normal'} );
			var confirmMsg = 'A document by this name already exists.  Are you sure you want to replace the existing document?';
			parent.parent.showDialog3Btn( 'Document Already Exists!', confirmMsg, dialogButtons, true );
			$('#dialog3Btn #replace', window.top.document).on("click", function() {
				parent.parent.dismissDialog3Btn();
				Annotate.fileID = duplicateFileID;
				Annotate.save(filename, true, folderID);
				$('#doc_title', parent.document).html( filename + '.pdf' );
			});
		} else {
			Annotate.save(filename, false, folderID);
			$('#doc_title', parent.document).html( filename + '.pdf' );
		}
	},

	save : function(filename, overwrite, folderID) {
		var text = Annotate.getAnnotations();
		var sourceFileID = (window.top.Datacore.s.inPresentation === true) ? Annotate.sourceFileID : parent.requestedFileID;
		var url = "annotate?sourceFileID=" + sourceFileID +
				  "&filename=" + filename + ".pdf" +
				  "&overwrite=" + overwrite +
				  "&fileID=" + Annotate.fileID +
				  "&folderID=" + folderID +
				  '&isTempFile=' + Annotate.isTempFile;
		$.post(url, {annotations: text}, function(data, textStatus, xhr) {
			if (textStatus === "success" && data !== null) {
				Annotate.fileID = data.fileID;
				if (Annotate.isTempFile) {
					window.top.Datacore.s.tempFile = {fileID: parseInt( Annotate.fileID ), originalFilename: Annotate.updatedFilename};
				} else {
					Annotate.updatedFilename = filename;
					window.top.showTooltip( 'Annotations saved to server.', true );
				}
				Annotate.needToSave = false;
				// See if we need to create a minimum entry for a new file.
				var file = window.top.Datacore.getFile( data.fileID );
				if (!isset( file )) {
					var newFile = {ID: data.fileID, folderID: folderID, name: Annotate.updatedFilename+".pdf", mimeType: "application/pdf"};
					var folder = window.top.Datacore.getFolder( folderID );
					if (folder !== null) {
						folder.addFile(newFile);
					}
				}
				switch( Annotate.nextState ) {
					case Annotate.nextStateEnum.Exit:
						Annotate.redirect();
						break;
					case Annotate.nextStateEnum.Send:
						Annotate.sendFile( Annotate.fileID );
						break;
					case Annotate.nextStateEnum.Download:
						Annotate.downloadFile( Annotate.fileID );
						break;
					case Annotate.nextStateEnum.Introduce:
						Annotate.introduceFile( Annotate.fileID );
						break;
					case Annotate.nextStateEnum.StartPresentation:
						Annotate.presentationAttendeesSelected( Annotate.presentationDepositionID + ".deposition" );
						break;
					case Annotate.nextStateEnum.WPIntroduce:
						Annotate.witnessPrepIntroduce( null, null );
						break;
					case Annotate.nextStateEnum.SetPresentationFile:
						window.top.setPresentationFile( window.top.webApp.getS( 'inDeposition' ), Annotate.setPresentionFileID );
						Annotate.setPresentionFileID = 0;
						break;
				}
			} else {
				window.top.showPlainDialog1Btn( 'Error', 'Failed to save annotations.', 'Close' );
			}
			Annotate.nextState = Annotate.nextStateEnum.None;
		}, "json")
		.fail(function() {
			window.top.showPlainDialog1Btn( 'Error', 'Failed to save annotations.', 'Close' );
			Annotate.nextState = Annotate.nextStateEnum.None;
		});
	},

	send : function() {
		if (Annotate.processing) {
			return;
		}
		Annotate.stopAnnotations();
		if (Annotate.needToSave) {
			var dialogButtons = [];
			dialogButtons.push( {label: 'No', id : 'no_save', class : 'btn_app_cancel'} );
			dialogButtons.push( {label: 'Yes', id : 'save', class : 'btn_app_normal'} );
			var confirmMsg = 'Modifications have been made to the file, would you like to email the changes with the document?';
			window.top.showDialog3Btn( 'File Changed', confirmMsg, dialogButtons, true );
			$('#dialog3Btn #no_save', window.top.document).on("click", function() {
				window.top.dismissDialog3Btn();
				Annotate.sendFile(parent.requestedFileID);
			});
			$('#dialog3Btn #save', window.top.document).on("click", function() {
				window.top.dismissDialog3Btn();
				if (window.top.Datacore.getVal(window.top.Datacore.s.currentUser, 'userType') === 'G') {
					Annotate.sendFile( Annotate.fileID, Annotate.getAnnotations() );
				} else {
					Annotate.nextState = Annotate.nextStateEnum.Send;
					Annotate.getSaveFilename();
				}
			});
		} else {
			Annotate.sendFile(Annotate.fileID);
		}
	},

	sendFile : function(fileID, annotations) {
		// since the embedded pdf viewer doesn't play well with the document layout, hide it from view
		$('#doc_viewer').hide();
		parent.parent.sendDialog.setFileID( fileID, annotations );
		parent.parent.sendDialog.show();
	},

	share : function() {
		if (Annotate.processing) {
			return;
		}

		//Save the annotations by page.
		Annotate.stopAnnotations();
		if (!Annotate.needToSave || window.top.Datacore.getS('depositionStatus') === 'F') {
			return;
		}

		var text = Annotate.getAnnotations();
		var url = 'witnessUpload?sourceFileID=' + parent.requestedFileID;
		$.post( url, {annotations : text}, function( data, textStatus, xhr ) {
			console.log( textStatus );
			if (textStatus === 'success') {
				parent.parent.showTooltip( data['status'], true );
				Annotate.needToSave = false;
				window.top.navBar.saveButton.addClass( 'disabled' );
			} else {
				console.log( ' -- service failure occurred: ('+textStatus+')' );
				console.log( data );
				parent.parent.showPlainDialog1Btn( 'Error', 'Failed to save annotations.', 'Close' );
			}
		}, 'json' )
		.fail( function() {
			console.log( ' -- request failure occurred' );
			parent.parent.showPlainDialog1Btn( 'Error', 'Failed to save annotations.', 'Close' );
		} );
	},

	getAnnotations : function () {
		//Save the annotations by page.
		var exportList = {};
		for(var i = 0; i < Annotate.drawList.length; i++) {
			 var o = new exportObject(Annotate.drawList[i]);
			 if (!isset( exportList[Annotate.drawList[i].page] )) {
				 exportList[Annotate.drawList[i].page] = [];
			 }
			 // Make adjustments so fpdf_annotate saves more like what was displayed
			 if ( o.drawType === 'arrow' ) {
				 o.lineWidth = DEFAULT_LINE_WIDTH - 1;
			 } else if( o.drawType === 'marker' ) {
				 o.lineWidth = DEFAULT_MARKER_WIDTH - 3;
				 o.opacity = o.opacity - 5;
			 } else if( o.drawType === 'pencil' ) {
				 o.lineWidth = DEFAULT_LINE_WIDTH + 1;
			 }
			 exportList[Annotate.drawList[i].page].push(o);
		}
		var annotations = [];
		for (var p in exportList) {
			var page = {};
			var pagesize = {width:Annotate.width, height:Annotate.height};
			var canvasObj = Annotate.findCanvasObjByID(p);
			if (canvasObj !== null) {
				pagesize = {width:canvasObj.width, height:canvasObj.height};
			}
			page[p] = exportList[p];
			page[p].push( pagesize );
			page[p].push( {username: Annotate.getUsername( window.top.Datacore.getS('userID') )} );
			annotations.push( page );
		}
		var text = JSON.stringify( annotations );
		console.log("text:"+text);
		return text;
	},

	getUsername : function( userID ) {
		var user = window.top.Datacore.getUser( userID );
		if (user != null) {
			if (user.userType === 'G' || user.userType === 'W') {
				return user.email;
			} else
			if (isset( user.username )) {
				return user.username;
			} else {
				return user.name;
			}
		}
		return '';
	},

	download : function() {
		Annotate.stopAnnotations();
		if (Annotate.needToSave && window.top.Datacore.getVal(window.top.Datacore.s.currentUser, 'userType') !== 'G') {
			Annotate.nextState = Annotate.nextStateEnum.Download;
			Annotate.getSaveFilename();
		} else {
			Annotate.downloadFile(Annotate.fileID);
		}
	},

	downloadFile : function(fileID) {
		var File = window.top.Datacore.getFile( fileID );
		parent.parent.showTooltip( 'Downloading '+File.name, true );

		parent.window.location.href = '/depositions/file?fileID=' + fileID + '&filename=' + encodeURI(File.name);
	},


	backButton : function() {
		Annotate.isBackEvent = true;
		Annotate.stopAnnotations();
//		if( !parent ) return;
		var depoStatus = window.top.Datacore.getS( 'depositionStatus' );
		var userType = window.top.webApp.getS( 'user.userType' );
		if( (depoStatus !== 'F' && userType !== 'G' && userType !== 'W' && Annotate.needToSave ) || (depoStatus === 'F' && Annotate.needToSave) ) {
			//Confirm the early exit.
			var dialogButtons = [];
			dialogButtons.push( {label:"Don't Save", id:'no_save', class:'btn_app_extra'} );
			dialogButtons.push( {label:'Cancel', action:'dismissDialog3Btn();', class:'btn_app_cancel'} );
			dialogButtons.push( {label:'Save', id:'save', class:'btn_app_normal'} );
			var confirmMsg = 'You may have changes to this document.  Are you sure you want to exit without saving?';
			window.top.showDialog3Btn( 'Abort Changes?', confirmMsg, dialogButtons );
			$('#dialog3Btn #no_save', window.top.document).on( "click", function() {
				Annotate.cleanEvents();
				window.top.dismissDialog3Btn();
				Annotate.redirect();
			} );
			$('#dialog3Btn #save', window.top.document).on( "click", function() {
				window.top.dismissDialog3Btn();
				Annotate.getSaveFilename();
				Annotate.nextState = Annotate.nextStateEnum.Exit;
			} );
		} else {
			Annotate.cleanEvents();
			Annotate.redirect();
		}
		if (Annotate.isTempFile) {
			window.top.webApp.abortTempFile();
		}
	},

	redirect : function() {
		var fileID = window.top.webApp.getS( 'joinPresentationData.fileID' );
		if ( window.top.Datacore.s.inPresentation ) {
			window.top.Datacore.s.inPresentation = false;
			window.top.webApp.removeItem( 'inPresentation' );
			if( Annotate.presentationHost ) {
				Annotate.presentationHost = false;
				window.top.presentationEnd( Annotate.presentationDepositionID );
				if( !Annotate.isBackEvent  && typeof fileID !== 'undefined' && fileID > 0 ) {
					window.top.iPadContainerFrame.location.replace( '/depositions/pdf?ID=' + fileID );
					return;
				}
			} else {
				window.top.presentationLeave( Annotate.presentationDepositionID );
				window.top.webApp.setS( 'joinPresentationData', null );
			}
		}
//		window.top.redirect();
		window.top.iPadContainerFrame.location.replace( '/depositions' );
	},

	updateSpeaker : function() {
//		console.log( 'updateSpeaker()' );
		if (window.top.Datacore.getS('depositionStatus') !== 'I') {
			window.top.navBar.introduceButton.disable();
			window.top.navBar.presentationButton.disable();
			return;
		}
		var depositionLeaderID = ( window.top.Datacore.getS( 'speakerID' ) !== 0 ) ? depositionLeaderID = window.top.Datacore.getS( 'speakerID' ) : window.top.Datacore.getS( 'conductorID' );
		var showIntroduce = true;
		var file = window.top.Datacore.getFile(Annotate.fileID);
		if (!isset( file )) {
			showIntroduce = false;
		} else {
			if (file.isExhibit) {
				showIntroduce = false;
			}
		}
		if ( depositionLeaderID === window.top.Datacore.getS('currentUserID') ) {
//			console.log( 'updateSpeaker(): deposition leader === currentUserID (' + depositionLeaderID + ')' );
			if( showIntroduce ) {
				window.top.navBar.introduceButton.onClick( Annotate.introduce ).addClass( 'dark' ).enable();
			} else {
				window.top.navBar.introduceButton.removeClass( 'dark' ).disable();
			}
			window.top.navBar.presentationButton.enable();
		} else {
			window.top.navBar.presentationButton.disable();
			window.top.navBar.introduceButton.disable();
		}
	},

	updateUsers : function() {
		console.log( 'updateUsers()' );
		if( window.top.Datacore.s.inPresentation && Annotate.presentationHost ) {
			window.top.navBar.annotatorButton.enable();
			var witnessUser = Annotate.getWitnessUser();
			if( witnessUser !== null && witnessUser.active ) {
				window.top.navBar.annotatorButton.addClass( 'active' );
			} else {
				window.top.navBar.annotatorButton.removeClass( 'active' );
			}
		}
	},

	getWitnessUser : function() {
		var witness = null;
		var witnesses = [];
		var users = window.top.Datacore.getUsers();
		for( var u in users ) {
			if( users[u].userType === 'W' || users[u].userType === 'WM' ) {
				witnesses.push( users[u] );
			}
		}
		for( var w in witnesses ) {
			witness = witnesses[w];
//			console.log( 'getwitnessUser(): Witness:', witness.ID, 'active:', witness.active );
			if( witness.active ) {
				return witness;
			}
		}
		console.log( 'getWitnessUser(): NO USER' );
		return witness;
	},

	introduce : function() {
		Annotate.stopAnnotations();
		var pFileID = window.top.webApp.getS( 'joinPresentationData.fileID' );
		if( typeof pFileID !== 'undefined' && pFileID !== null && pFileID > 0 ) {
			Annotate.sourceFileID = pFileID;
		}
		if( Annotate.updatedFilename === null ) {
			var fileID = window.top.Datacore.s.inPresentation ? Annotate.fileID : parent.requestedFileID;
			var dcFile = window.top.Datacore.getFile( fileID );
			if( typeof dcFile === 'undefined' || dcFile === null ) {
				console.log( '-- Introduce getFile is null.' );
				window.top.Datacore.syncAllFolders();
				return;
			}
			var index = dcFile.name.lastIndexOf( "." );
			Annotate.updatedFilename = dcFile.name.substr( 0, index );
		}
		if( Annotate.needToSave ) {
			var dialogButtons = [];
			dialogButtons.push( {label:'No', action:'dismissDialog3Btn();', class:'btn_app_cancel'} );
			dialogButtons.push( {label:'Yes', id:'save', class:'btn_app_normal'} );
			var confirmMsg = 'This document has been modified with annotations.  All annotations will be saved when this document is introduced.  Do you want to continue?';
			window.top.showDialog3Btn( 'Document Changed', confirmMsg, dialogButtons, true );
			$('#dialog3Btn #save', window.top.document).off().on( "click", function() {
				window.top.dismissDialog3Btn();
				Annotate.introduceSaveFile();
				if( window.top.Datacore.s.inPresentation ) {
					if( typeof window.top.fileSelection !== 'undefined' && window.top.fileSelection !== null ) {
						window.top.fileSelection.disableFileSelection();
					}
				}
			} );
		} else {
// ED-2640; witness prep introduce should not have any dependancy on personal folder
//			var folder = window.top.Datacore.getFolder( Annotate.personalFolderID );
//			if( typeof folder === 'undefined' || folder === null ) {
//				console.log('-- Introduce getFolder is null.');
//				window.top.Datacore.syncFolders();
//				return;
//			}
			if( Annotate.fileID !== null && typeof window.top.Datacore.tempFiles !== 'undefined' && window.top.Datacore.tempFiles !== null && Annotate.fileID in window.top.Datacore.tempFiles ) {
				Annotate.isTempFile = true;
			}
			Annotate.introduceFile( Annotate.fileID );
			if( window.top.Datacore.s.inPresentation && (window.top.webApp.getS( 'deposition.class' ) !== 'WitnessPrep' && window.top.webApp.getS( 'deposition.class' ) !== 'WPDemo') ) {
				if( typeof window.top.fileSelection !== 'undefined' && window.top.fileSelection !== null ) {
					window.top.fileSelection.disableFileSelection();
				}
			}
		}
	},

	introduceSaveFile : function()
	{
		Annotate.isTempFile = true;
		var filename = Annotate.updatedFilename;
		if( filename.substr( 0, 4 ) !== 'tmp_' ) {
			filename = 'tmp_' + filename;
		}
		var overwrite = Annotate.introduceCheckOverwrite( filename );
		Annotate.nextState = Annotate.nextStateEnum.Introduce;
		Annotate.save( filename, overwrite, Annotate.personalFolderID );
	},

	introduceCheckOverwrite : function( filename ) {
		var folder = window.top.Datacore.getFolder(Annotate.personalFolderID);
		if (folder === null) {
			return false;
		}
		var files = folder.getFiles(folder.getFileIDs());
		var duplicate = false;
		for(var f in files) {
			var shortName = files[f].name;
			var index = shortName.lastIndexOf(".");
			if (index >= 0) {
				shortName = shortName.substr(0, index);
			}
			if (shortName.toLowerCase() === filename.toLowerCase()) {
				duplicate = true;
				Annotate.fileID = parseInt(files[f].ID, 10);
				break;
			}
		}
		return duplicate;
	},

	introduceFile : function( fileID ) {
		if( window.top.Datacore.getS( 'class' ) === 'WitnessPrep' || window.top.Datacore.getS( 'class' ) === 'WPDemo' ) {
			Annotate.introduceSave( Annotate.updatedFilename, fileID, fileID, false );
		} else {
			window.top.iPadContainerFrame.location.replace( '/depositions/introduce?ID=' + fileID + '&fromView=1' );
		}
	},

	introduceSave : function( filename, sourceID, fileID, overwrite )
	{
		if( Annotate.processing ) {
			return;
		}
		Annotate.processing = true;
		showSpinner();
		var date = new Date ();
		window.top.Datacore.s.exhibitDate = (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear().toString().substr(2);
		filename = filename.replace( /[^\w\040\-\.]+/g, '' ) + '.pdf';
		var url = 'introduceFile?filename=' + encodeURIComponent( filename ) +
				  '&fileID=' + fileID +
				  '&sourceFileID=' + sourceID +
				  '&exhibitXOrigin=0.0' +
				  '&exhibitYOrigin=0.0' +
				  '&exhibitTitle=' +
				  '&exhibitSubTitle=' +
				  '&skipStamp=true' +
				  '&overwrite=' + overwrite +
				  '&exhibitDate=' + encodeURIComponent( window.top.Datacore.getS( 'exhibitDate' ) );
		$.getJSON( url, null, function(data, textStatus, xhr) {
			hideSpinner();
			Annotate.processing = false;
			if (textStatus === 'success') {
				if (typeof data.error === 'undefined') {
					// Find the new fileID
					var newFileID = parseInt( data.EDDeposition.introducedFile.ID );
//					if (window.top.Datacore.hasFile( newFileID )) {
//						window.top.Datacore.dropFile( newFileID );
//					}
					window.top.Datacore.addFile( data.EDDeposition.introducedFile );
					if( overwrite ) {
						window.top.showTooltip("The exhibit was successfully reintroduced!", true);
					} else {
						window.top.showTooltip("The exhibit was successfully introduced!", true);
					}
					Annotate.cleanEvents();
					if( window.top.Datacore.s.inPresentation === true ) {
						window.top.setPresentationFile( window.top.webApp.getS( 'inDeposition' ), newFileID );
						window.top.iPadContainerFrame.location.replace( '/depositions/presentation' );
						return;
					}
					parent.view_file( newFileID );
				} else {
					var errTitle = (typeof data.title !== 'undefined' && data.title !== null && data.title.length > 0) ? data.title : 'Error';
					var errText = (typeof data.message !== 'undefined' && data.message !== null && data.message.length > 0) ? data.message : 'Unable to introduce document.';
					window.top.showPlainDialog1Btn( errTitle, errText, 'Close' );
				}
			}
		} ).fail( function() {
			Annotate.processing = false;
			hideSpinner();
			console.log("Fail Introduce");
			window.top.showPlainDialog1Btn( 'Error', 'Unable to introduce document.', 'Close' );
		});
	},

	witnessPrepIntroduce: function( e, fileName )
	{
		Annotate.stopAnnotations();
		if( typeof fileName === 'undefined' || fileName === null ) {
			var fName = Annotate.currentFile.name.substr( 0, Annotate.currentFile.name.lastIndexOf( "." ) );
			console.log( 'Annotate::witnessPrepIntroduce -- Annotate.needToSave:', Annotate.needToSave );
			if( Annotate.needToSave ) {
				var dialogButtons = [];
				dialogButtons.push( {label:'No', action:window.top.dismissDialog3Btn, class:'btn_app_cancel'} );
				dialogButtons.push( {label:'Yes', id:'introduceSaveBtn', class:'btn_app_normal'} );
				var confirmMsg = '<p>This document has been modified with annotations.<p> All annotations will be saved when this document is</p><p>introduced.  Do you want to continue?</p>';
				window.top.showDialog3Btn( 'Document Changed', confirmMsg, dialogButtons, false, 'tight padded' );
				$('#dialog3Btn #introduceSaveBtn', window.top.document).off().on( "click", function() {
					window.top.dismissDialog3Btn();
					Annotate.isTempFile = true;
					var filename = fName;
					if( filename.substr( 0, 4 ) !== 'tmp_' ) {
						filename = 'tmp_' + filename;
					}
					var overwrite = Annotate.introduceCheckOverwrite( filename );
					Annotate.nextState = Annotate.nextStateEnum.WPIntroduce;
					Annotate.save( filename, overwrite, Annotate.personalFolderID );
				} );
				return;
			}
			window.top.showDialogTextInput( 'Rename the document', 'Save', fName );
			$('#dialogTextInputClose', window.top.document).off().on( 'click', function() {
				var fName = $('#dialogInput', window.top.document).val();
				Annotate.witnessPrepIntroduce( null, fName );
			} );
		} else {
			var exhibitFolder;
			var depoID = window.top.webApp.getS( 'deposition.ID' );
			var folders = window.top.Datacore.getFolders();
			if( typeof folders !== 'undefined' && folders !== null ) {
				for( var f in folders ) {
					var _folder = folders[f];
					if( _folder.depositionID === depoID && _folder.class === 'Exhibit' ) {
						exhibitFolder = _folder;
						break;
					}
				}
			}
			if( typeof exhibitFolder === 'undefined' || exhibitFolder === null ) {
				console.error( 'Missing exhibit folder' );
				return;
			}
			var inConflict = Annotate.folderCheckFilenameOverwrite( exhibitFolder.ID, fileName );
			Annotate.updatedFilename = fileName;
			if( !inConflict ) {
				Annotate.introduce();
				return;
			}
			//TODO: handle file exists & overwrite
			var dialogButtons = [];
			dialogButtons.push( {label:'No', action:window.top.dismissDialog3Btn, class:'btn_app_cancel'} );
			dialogButtons.push( {label:'Yes', action:function(){ window.top.Annotate.witnessPrepIntroduceOverwrite( inConflict ); }, class:'btn_app_normal'} );
			var confirmMsg = '<p>An introduced document by this name already exists.</p><p>Do you want to replace it?</p>';
			window.top.showDialog3Btn( 'File Already Exists!', confirmMsg, dialogButtons, false, 'tight padded' );
		}
	},

	witnessPrepIntroduceOverwrite: function( exhibitID )
	{
		window.top.dismissDialog3Btn();
		Annotate.introduceSave( Annotate.updatedFilename, Annotate.fileID, exhibitID, true );
	},

	folderCheckFilenameOverwrite: function( folderID, filename )
	{
		var folder = window.top.Datacore.getFolder( folderID );
		if( typeof folder === 'undefined' || folder === null ) {
			return false;
		}
		var files = folder.getFiles();
		for( var f in files ) {
			var shortName;
			var _name = files[f].name;
			var index = _name.lastIndexOf( "." );
			if( index > 0 ) {
				shortName = _name.substr( 0, index );
				if( shortName.toLowerCase() === filename.toLowerCase() ) {
					return files[f].ID;
				}
			}
		}
		return false;
	},

	drawInit : function(canvas, offset) {
		var foundCanvas = false;
		var c;
		// See if we are replacing a canvasObject.
		for(var i = 0; i < Annotate.canvasList.length; i++) {
			if (Annotate.canvasList[i].ID === canvas.attr("id")) {
				foundCanvas = true;
				c = Annotate.canvasList[i];
				var pageNumber = parseInt( c.ID );
				var newRotation = PDFView.pages[pageNumber - 1].rotation;
				c.updateCanvas( canvas[0], offset, newRotation );
				Annotate.resizeDrawList(c);
				if (newRotation !== c.rotation) {
					Annotate.rotateDrawList( c, newRotation );
					c.rotation = newRotation;
					if (window.top.Datacore.s.inPresentation && Annotate.iAmPresentor && pageNumber === PDFView.page ) {
						Annotate.sendRotation( newRotation );
					}
				}
				if (window.top.Datacore.s.inPresentation && Annotate.iAmPresentor ) {
					if (Annotate.presentationCurrentPageScale !== PDFView.currentScale) {
						Annotate.scrollEvent();
					}
				}
				break;
			}
		}
		if (!foundCanvas) {
			c = new CanvasObject(canvas[0], offset, canvas.attr("id"));
			Annotate.canvasList.push(c);
		}
		Annotate.canvas = c.canvas
		Annotate.ctx = c.ctx;
		Annotate.width = Annotate.canvas.width;
		Annotate.height = Annotate.canvas.height;
		Annotate.updateUndoButton();

		if (foundCanvas) {
			Annotate.redraw();
		}
	},

	depositionEnded : function() {
		if (window.top.Datacore.getVal(window.top.Datacore.s.currentUser, 'userType') === 'W') {
			window.top.navBar.saveButton.addClass( 'disabled' );
		}
	},

	fullscreen : function() {
		if (typeof parent === 'undefined' || parent === null || typeof top === 'undefined' || top === null) {
			console.log('fullscreen(): WARNING parent is undefined or null');
			return;
		}
		if( $('#social_list_container', window.top.document).is( ':visible' ) ) {
			window.top.Social.toggleAttendeeList( true );
		}
		Annotate.savedPageScale = $('#scaleSelect').val();
		$('#scaleSelect').val( 'page-width' );
	},

	normalscreen : function( exitScreen ) {
		window.top.navBar.canHideToolbar( false );
		return;
		exitScreen = (isset( exitScreen ) && exitScreen === true );
		$('#viewerContainer').off( 'click', Annotate.shouldShowToolBar );
		$('#ipad_container', window.top.document).removeClass('fullscreen');
		$('#file_contents', parent.document).removeClass('fullscreen');
		$('#scaleSelect').val( Annotate.savedPageScale );
		if (exitScreen) {
			//Hide pdf so we don't flash so much.
			$('#outerContainer').hide();
		} else {
			//Save preference.
			window.top.Datacore.s.fullscreen = false;
		}
	},

	shouldShowToolBar : function() {
		if (Annotate.drawState === Annotate.drawStateEnum.Draw || Annotate.drawState === Annotate.drawStateEnum.Select) {
			return;
		}
		if (parent.isShowingFullscreenToolBar()) {
			parent.dismissFullscreenToolBar();
		} else {
			var fileName = '';
			var File = window.top.Datacore.getFile( Annotate.fileID );
			if ( typeof File !== 'undefined' && typeof File.name !== 'undefined' ) {
				fileName = File.name;
			}
			parent.showFullscreenToolBar( fileName );
		}
	},

	newChatMessage : function() {
		if ($('#ipad_container', parent.parent.document).hasClass('fullscreen')) {
			parent.parent.showTooltip('You have received a chat message', true);
		}
	},

	updateButtons : function() {
//		console.log('updateButtons(): inPresentation = ' + window.top.Datacore.s.inPresentation + '; Host = ' + Annotate.presentationHost);
		if( window.top.Datacore.s.inPresentation ) {
			if ( !Annotate.presentationHost ) {
				window.top.navBar.allDisable();
				if( window.top.webApp.getS( 'user.userType' ) !== 'W' ) {
					window.top.navBar.backButton.enable();
				}
				$('#toolbarContainer').hide();
				window.top.navBar.editorActions.hide();
				$('#viewerContainer').css( 'top', '0' );
				$('#viewerContainer').addClass( 'viewPresentation' );
			} else {
//				console.log( 'updateButtons(): presenterHost' );
				window.top.navBar.updateDefaults();
				$('#toolbarContainer').show();
			}
		}
	},

	// Convert a normalized point from the server and converts and rotates the
	// point to the correct position on the current canvas.
	stringToPoint : function ( stringPoint, canvasObj ) {
//		console.log( 'annotate::stringToPoint() --> stringPoint = ', stringPoint );
		var splitPoint = stringPoint.replace( /{/g, '' ).replace( /}/g, '' ).split( ',' );
		var point;
		switch( canvasObj.rotation ) {
			case 90:
				point = [ parseFloat( splitPoint[1] ) * canvasObj.width, parseFloat( splitPoint[0] ) * canvasObj.height ];
				break;

			case 180:
				point = [ (1 - parseFloat( splitPoint[0] ) ) * canvasObj.width, parseFloat( splitPoint[1] ) * canvasObj.height];
				break;

			case 270:
				point = [ ( 1 - parseFloat( splitPoint[1] ) ) * canvasObj.width, ( 1 - parseFloat( splitPoint[0] ) ) * canvasObj.height ];
				break;

			default:
				point = [ parseFloat( splitPoint[0] ) * canvasObj.width, ( 1 - parseFloat( splitPoint[1] ) ) * canvasObj.height];
				break;
		}
		return point;
	},

	stringToBoundPoint : function ( stringPoint, canvasObj ) {
//		console.log( 'annotate::stringToPoint() --> stringPoint = ', stringPoint );
		var splitPoint = stringPoint.replace( /{/g, '' ).replace( /}/g, '' ).split( ',' );
		var point;
		switch( canvasObj.rotation ) {
			case 90:
				point = [ parseFloat( splitPoint[1] ) * canvasObj.width, parseFloat( splitPoint[0] ) * canvasObj.height ];
				break;

			case 180:
				point = [ parseFloat( splitPoint[0] ) * canvasObj.width, parseFloat( splitPoint[1] ) * canvasObj.height];
				break;

			case 270:
				point = [ parseFloat( splitPoint[1] ) * canvasObj.width, parseFloat( splitPoint[0] ) * canvasObj.height ];
				break;

			default:
				point = [ parseFloat( splitPoint[0] ) * canvasObj.width, parseFloat( splitPoint[1] ) * canvasObj.height];
				break;
		}
		return point;
	},

	presentationAnnotation : function ( annotation ) {
		if( !window.top.Datacore.s.inPresentation ) {
			console.log( 'Not in a presentation so ignoring annotations.' );
			return;
		}
		switch ( annotation.Type ) {
			case 9: // Draw
				Annotate.presentationDraw( annotation );
				break;

			case 1: // Arrow
				Annotate.presentationArrow( annotation );
				break;

			case 5: // Select
				Annotate.presentationMarker( annotation );
				//Annotate.presentationSelect( annotation );
				break;

			case 8: // Note
				Annotate.presentationNote( annotation );
				break;

			default:
				console.log( 'Unknown annotation type: ' + annotation.Type );
				break;
		}
		if( Annotate.presentationHost ) {
			Annotate.needToSave = true;
		}
	},

	presentationGetAnnotations : function( data ) {
//		console.log( 'Start --> presentationGetAnnotations(): PDFView.page = ' + PDFView.page );
		if (!window.top.Datacore.s.inPresentation) {
			console.log( 'Not in a presentation so ignoring getAnnotations.' );
			return;
		}
		// Load all of the pages before trying to add the annotations.
		for(var i in data.annotations) {
			var page = parseInt( i, 10 ) + 1;
			PDFView.page = page;
		}

		Annotate.presentationPendingAnnotations = data.annotations;
		Annotate.checkPresentationPendingAnnotations();
//		console.log( 'End --> presentationGetAnnotations(): PDFView.page = ' + PDFView.page );
	},

	checkPresentationPendingAnnotations : function() {
		if (!isset( Annotate.presentationPendingAnnotations )) {
			return;
		}

		// See if the pages have been rendered.
		var annotations = Annotate.presentationPendingAnnotations;
		var i, j;
		// Load all of the pages before trying to add the annotations.
		for (i in annotations) {
			var page = ( parseInt( i, 10 ) + 1 ).toString();
			if (Annotate.findCanvasObjByID( page ) === null) {
				// We are still waiting on a page to render.
				PDFView.page = parseInt( i, 10 ) + 1;
				return;
			}
		}

		for (i in annotations) {
			for(j in annotations[i]) {
				Annotate.presentationAnnotation( annotations[i][j] );
			}
		}
		Annotate.presentationPendingAnnotations = null;
//???		Annotate.presentationPageProcess();
	},

	presentationDraw : function( annotation ) {
		if (annotation.Operation === 'add') {
			var canvasObj = Annotate.findCanvasObjByPage( annotation.Page );
			var lineWidth = annotation.LineWidth * PDFView.currentScale;
			Annotate.canvas = canvasObj.canvas;
			Annotate.ctx = canvasObj.ctx;
			Annotate.currentShape = new DrawObject( Annotate.clickThreshold );
			Annotate.currentShape.page = canvasObj.ID;
			Annotate.currentShape.color = Annotate.intToHex( annotation.Color );
			Annotate.currentShape.opacity = annotation.Opacity / 100.0;
			Annotate.currentShape.setDrawType( "pencil", lineWidth );
			Annotate.currentShape.index = annotation.Index;
			for(var i = 0; i < annotation.Points.length; i++) {
				Annotate.currentShape.addObject();
				var linePoints = annotation.Points[i];
				for(var j = 0; j < linePoints.length; j++) {
					var point = Annotate.stringToPoint( linePoints[j], canvasObj );
					Annotate.currentShape.addPoints( point[0], point[1] );
				}
				if (linePoints.length === 1) {
					// Add a second point.
					Annotate.currentShape.addPoints( point[0] + lineWidth, point[1] + lineWidth );
				}
			}
			Annotate.drawList.push( Annotate.currentShape );
			Annotate.currentShape.createBoundingBox();
			Annotate.currentShape.updateDragPoints();
//			Annotate.currentShape.debugShape();

			if (isset( annotation.Bounds )) {
				Annotate.drawUpdateBounds( annotation.Bounds, Annotate.currentShape, canvasObj );
			}
			Annotate.currentShape = null;
		}
		Annotate.redraw();
	},

	presentationArrow : function( annotation ) {
		if (annotation.Operation === 'add') {
			var canvasObj = Annotate.findCanvasObjByPage( annotation.Page );
			var lineWidth = annotation.LineWidth * PDFView.currentScale;
			Annotate.canvas = canvasObj.canvas;
			Annotate.ctx = canvasObj.ctx;
			Annotate.currentShape = new DrawObject( Annotate.clickThreshold );
			Annotate.currentShape.page = canvasObj.ID;
			Annotate.currentShape.color = Annotate.intToHex( annotation.Color );
			Annotate.currentShape.opacity = annotation.Opacity / 100.0;
			Annotate.currentShape.setDrawType( "arrow", lineWidth );
			Annotate.currentShape.index = annotation.Index;
			Annotate.currentShape.addObject();

			var point;
			point = Annotate.stringToPoint( annotation.StartPoint, canvasObj );
			Annotate.currentShape.addPoints( point[0], point[1] );

			point = Annotate.stringToPoint( annotation.EndPoint, canvasObj );
			Annotate.currentShape.addPoints( point[0], point[1] );

			Annotate.drawList.push( Annotate.currentShape );
			Annotate.currentShape.createBoundingBox();
			Annotate.currentShape.updateDragPoints();
//			Annotate.currentShape.debugShape();
			Annotate.currentShape = null;
		}
		Annotate.redraw();
	},

	presentationMarker : function( annotation ) {
		if (annotation.Operation === 'add') {
			var canvasObj = Annotate.findCanvasObjByPage( annotation.Page );
			var lineWidth = annotation.LineWidth * PDFView.currentScale;
			Annotate.canvas = canvasObj.canvas;
			Annotate.ctx = canvasObj.ctx;
			Annotate.currentShape = new DrawObject( Annotate.clickThreshold );
			Annotate.currentShape.page = canvasObj.ID;
			Annotate.currentShape.color = Annotate.intToHex( annotation.Color );
			Annotate.currentShape.opacity = annotation.Opacity / 100.0;
			Annotate.currentShape.setDrawType( "marker", lineWidth );
			Annotate.currentShape.index = annotation.Index;
			Annotate.currentShape.addObject();

			var point;
			point = Annotate.stringToPoint( annotation.StartPoint, canvasObj );
			Annotate.currentShape.addPoints( point[0], point[1] );

			point = Annotate.stringToPoint( annotation.EndPoint, canvasObj );
			Annotate.currentShape.addPoints( point[0], point[1] );

			Annotate.drawList.push( Annotate.currentShape );
			Annotate.currentShape.createBoundingBox();
			Annotate.currentShape.updateDragPoints();
//			Annotate.currentShape.debugShape();
			Annotate.currentShape = null;
		}
		Annotate.redraw();
	},

	presentationSelect : function( annotation ) {
		if (annotation.Operation === 'add') {
			var canvasObj = Annotate.findCanvasObjByPage( annotation.Page );

			Annotate.canvas = canvasObj.canvas;
			Annotate.ctx = canvasObj.ctx;
			Annotate.currentShape = new DrawObject( Annotate.clickThreshold );
			Annotate.currentShape.page = canvasObj.ID;
			Annotate.currentShape.opacity = annotation.Opacity / 100.0;
			var rgb_value = Annotate.splitColor( Annotate.intToHex( annotation.Color ) );
			if (rgb_value !== null) {
				Annotate.currentShape.color = 'rgba('+rgb_value.r+','+rgb_value.g+','+rgb_value.b+','+Annotate.currentShape.opacity+')';
			} else {
				Annotate.currentShape.color = "rgba(255, 255, 0, 0.4)";
			}
			Annotate.currentShape.setDrawType( "highlight" );
			Annotate.currentShape.index = annotation.Index;
			Annotate.currentShape.addObject();

			for(var i = 0; i < annotation.Quads.length; i++) {
				var point1, point2, width, height;
				point1 = Annotate.stringToPoint( annotation.Quads[i][0], canvasObj );
				point2 = Annotate.stringToPoint( annotation.Quads[i][3], canvasObj );
				width = Math.abs( point2[0] - point1[0] );
				height = Math.abs( point2[1] - point1[1] );

				// Swap width and height per rotation.
				switch( canvasObj.rotation ) {
					case 90:
					case 270:
						Annotate.currentShape.addPoints( point1[0], point1[1], height, width );
						break;

					default:
						Annotate.currentShape.addPoints( point1[0], point1[1], width, height );
						break;

				}
			}

			Annotate.drawList.push( Annotate.currentShape );
			Annotate.currentShape.createBoundingBox();
//			Annotate.currentShape.debugShape();
			Annotate.currentShape = null;
		}
		Annotate.redraw();
	},

	presentationNote : function( annotation ) {
		if (annotation.Operation === 'add') {
			var canvasObj = Annotate.findCanvasObjByPage( annotation.Page );

			Annotate.canvas = canvasObj.canvas;
			Annotate.ctx = canvasObj.ctx;
			Annotate.currentShape = new DrawObject( Annotate.clickThreshold );
			Annotate.currentShape.page = canvasObj.ID;
			Annotate.currentShape.color = Annotate.intToHex( annotation.Color );
			Annotate.currentShape.opacity = annotation.Opacity / 100.0;
			Annotate.currentShape.setDrawType( "note", 1 );
			Annotate.currentShape.index = annotation.Index;
			Annotate.currentShape.date = Date.now();
			Annotate.currentShape.note = annotation.Content;
			Annotate.currentShape.author = Annotate.getUsername( window.top.Datacore.getS('speakerID') );
			Annotate.currentShape.addObject();

			var point = Annotate.stringToPoint( annotation.Bounds, canvasObj );
			Annotate.currentShape.addPoints( point[0], point[1] );

			Annotate.drawList.push( Annotate.currentShape );
			Annotate.currentShape.createBoundingBox();
//			Annotate.currentShape.debugShape();
			Annotate.currentShape = null;
		}
		Annotate.redraw();
	},

	presentationPageAdd : function( page ) {
		Annotate.presentationPendingPageSettings.push( page );
		Annotate.presentationPageProcess();
	},

	presentationPageProcess : function() {
//		console.log( '1> presentationPageProcess(): presentationPendingPageSettings count = ', Annotate.presentationPendingPageSettings.length );
		if (isset( Annotate.presentationPendingAnnotations ) || Annotate.presentationPendingPageSettings.length < 1) {
			return;
		}
//		console.log( '2> presentationPageProcess(): Annotate.presentationPageIndex = ', Annotate.presentationPageIndex );
		var canvasObj = Annotate.findCanvasObjByPage( Annotate.presentationPageIndex );
		if (canvasObj === null) {
			// Force the page to render.
			PDFView.page = Annotate.presentationPageIndex + 1;
//			console.log( '>> presentationPageProcess(): canvas is null --> wait for render of page: ', Annotate.presentationPageIndex );
			return;
		}

		var leftOffset = canvasObj.canvas.offsetParent.clientLeft;
		var page = Annotate.presentationPendingPageSettings.shift();
//		console.log( '3> presentationPageProcess(): page = ' + JSON.stringify( page ) );
//		console.log( '4> presentationPageProcess(): PDFView.page = ' + PDFView.page );

		if (isset( page.pageIndex )) {
//			console.log( '5> presentationPageProcess(): page.pageIndex = ', page.pageIndex, ' PDFView.page = ', PDFView.page, ' presentationPageTopOffset = ', Annotate.presentationPageTopOffset );
			if (PDFView.page !== page.pageIndex + 1) {
				//Save the settings because we need will re-render to update the scale.
				PDFView.page = page.pageIndex + 1;
				Annotate.presentationPageIndex = page.pageIndex;
				Annotate.presentationScale = 1;
				Annotate.presentationCurrentPageScale = 1;
				Annotate.presentationPageTopOffset = $('#pageContainer' + PDFView.page).get(0).offsetTop;
				// Delete so we do not process this property again.
				delete page.pageIndex;
//				console.log( '5-1> presentationPageProcess(): changing pages --> wait for render of page: PageIndex = ', Annotate.presentationPageIndex, ' presentationPageTopOffset = ', Annotate.presentationPageTopOffset );
			}
		}
		if (isset( page.orientation )) {
//			console.log( '6> presentationPageProcess(): page.orientation = ', page.orientation, ' PDFView.page = ', PDFView.page, ' presentationPageTopOffset = ', Annotate.presentationPageTopOffset );
			if (page.orientation === 'portrait') {
				Annotate.presentationPortrait = true;
				if (PDFView.currentScaleValue !== 'page-fit') {
					//Save the settings because we need will re-render to update the scale.
					$('#viewerContainer').addClass( 'portrait' );
					Annotate.presentationCurrentPageScale = 1;
					PDFView.setPageScale( 'page-fit' );
					// Scroll back to top of page.
					PDFView.page = PDFView.page;
					Annotate.presentationPageTopOffset = $('#pageContainer' + PDFView.page).get(0).offsetTop;
//					console.log( '>> presentationPageProcess(): changing orientation --> wait for render of page: presentationPageTopOffset = ', Annotate.presentationPageTopOffset );
				}
				delete page.orientation;
			}
		}
		if (isset( page.pageScale )) {
//			console.log( '7> presentationPageProcess(): page.pageScale = ', page.pageScale, ' PDFView.page = ', PDFView.page, ' presentationPageTopOffset = ', Annotate.presentationPageTopOffset );
			Annotate.presentationCurrentPageScale = page.pageScale;
			var _pageScale = parseFloat( parseFloat( page.pageScale ).toFixed( 12 ) );
//			console.log( '7-1> presentationPageProcess(): pageScale === 1 --> presentationPageTopOffset = ', Annotate.presentationPageTopOffset );
//			console.log( '7-2> PDFView.currentScaleValue = ', PDFView.currentScaleValue, 'PDFView.currentScale = ', PDFView.currentScale );
			if( _pageScale === 1 ) {
				if( Annotate.presentationPortrait) {
					if( PDFView.currentScaleValue !== 'page-fit' ) {
//						console.log( '===> Calling PDFView.setPageScale( \'page-fit\' )' );
						PDFView.setPageScale( 'page-fit' );
						PDFView.page = PDFView.page;
						Annotate.presentationPageTopOffset = $('#pageContainer' + PDFView.page).get(0).offsetTop;
					}
				} else if( PDFView.currentScaleValue !== 'page-width' ) {
//					console.log( '===> Calling PDFView.setPageScale( \'page-width\' )' );
					PDFView.setPageScale( 'page-width' );
					PDFView.page = PDFView.page;
					Annotate.presentationPageTopOffset = $('#pageContainer' + PDFView.page).get(0).offsetTop;
				}
//				console.log( '7-3> presentationPageProcess(): --> currentScaleValue = ', PDFView.currentScaleValue, 'PDFView.currentScale = ', PDFView.currentScale, ' presentationPageTopOffset = ', Annotate.presentationPageTopOffset );
			} else {
//				console.log( '7-5> presentationPageProcess(): --> currentScaleValue = ', PDFView.currentScaleValue, 'PDFView.currentScale = ', PDFView.currentScale, ' presentationPageTopOffset = ', Annotate.presentationPageTopOffset );
				var magicScaleFactor = (Annotate.presentationPortrait) ? 0.705 : 1.25;	//TODO: fix this
				var scale = parseFloat( parseFloat( _pageScale * magicScaleFactor ).toFixed( 12 ) );
				PDFView.setPageScale( scale );
				Annotate.presentationScale = PDFView.currentScale;
				Annotate.presentationPageTopOffset = $('#pageContainer' + PDFView.page).get(0).offsetTop;
			}
//			console.log( '7-6> presentationPageProcess(): currentScale = ', PDFView.currentScale, ' Annotate.presentationScale = ', Annotate.presentationScale, ' presentationPageTopOffset = ', Annotate.presentationPageTopOffset);
//			console.log( '7-7> presentationPageProcess(): --> currentScaleValue = ', PDFView.currentScaleValue, ' presentationPageTopOffset = ', Annotate.presentationPageTopOffset );
			// Don't process the rest of the message until "pageScale" is finished so the offsets are correct
			delete page.pageScale;
			Annotate.presentationPendingPageSettings.unshift( page );
			if (Annotate.presentationPendingPageSettings.length > 0) {
				setTimeout( Annotate.presentationPageProcess, 200 );
			}
			return;
		}
		if (isset( page.pageOrientation )) {
//			console.log( '8> presentationPageProcess(): page.pageOrientation = ', page.pageOrientation, ' PDFView.page = ', PDFView.page, ' presentationPageTopOffset = ', Annotate.presentationPageTopOffset );
			var currentRotation = PDFView.pageRotation;
			var newRotation;
			switch ( page.pageOrientation ) {
				case 1: // Right 90
					newRotation = 90;
					break;
				case 2: // 180
					newRotation = 180;
					break;
				case 3: // Left 90
					newRotation = 270;
					break;
				default: // upright.
					newRotation = 0;
					break;
			}
			var delta = newRotation - currentRotation;
			if (delta !== 0) {
				// Only rotate the page if it needs it
				PDFView.rotatePage( delta );
				console.log( '8-1> presentationPageProcess(): rotation delta changing --> wait for render of page: presentationPageTopOffset = ', Annotate.presentationPageTopOffset );
				// Don't process the rest of the message until "rotate" is finished so the offsets are correct
				delete page.pageOrientation;
				Annotate.presentationPendingPageSettings.unshift( page );
				if (Annotate.presentationPendingPageSettings.length > 0) {
					setTimeout( Annotate.presentationPageProcess, 200 );
				}
				return;
			}
		}
		var scrollOffset = 0;
		var scrollPos = 0;
		var pageBorder = 9;
		if ( isset( page.pageOffsetTop ) && isset( canvasObj ) ) {
			var originalPageTopOffset = $('#pageContainer' + PDFView.page).get(0).offsetTop;
			Annotate.presentationPageTopOffset = originalPageTopOffset - (( page.pageOffsetTop / 10000 ) * canvasObj.height );
		}
		if (isset( page.pageOffsetX ) && isset( canvasObj )) {
//			console.log( '9> presentationPageProcess(): page.pageOffsetX = ', page.pageOffsetX, ' PDFView.page = ', PDFView.page, ' presentationPageTopOffset = ', Annotate.presentationPageTopOffset );
			switch ( PDFView.pageRotation ) {
				case 90: // Right 90
					scrollPos = (Annotate.presentationPageTopOffset + (page.pageOffsetX * canvasObj.height)) + pageBorder;	//account for 9px border/margin
					Annotate.scrollDiv.scrollTop( scrollPos );
					break;
				case 180: // 180
					scrollOffset = canvasObj.width - Annotate.scrollDiv.innerWidth();
					Annotate.scrollDiv.scrollLeft( leftOffset + scrollOffset - page.pageOffsetX * canvasObj.width );
					break;
				case 270: // Left 90
					scrollOffset = canvasObj.height - Annotate.scrollDiv.innerHeight();
					Annotate.scrollDiv.scrollTop( Annotate.presentationPageTopOffset + scrollOffset - page.pageOffsetX * canvasObj.height );
					break;
				default: // upright.
					Annotate.scrollDiv.scrollLeft( leftOffset + page.pageOffsetX * canvasObj.width );
					break;
			}
		}
		if (isset( page.pageOffsetY ) && isset( canvasObj )) {
//			console.log( '10> presentationPageProcess(): page.pageOffsetY = ', page.pageOffsetY, ' PDFView.page = ', PDFView.page, ' presentationPageTopOffset = ', Annotate.presentationPageTopOffset );
			switch ( PDFView.pageRotation ) {
				case 90: // Right 90
					scrollOffset = canvasObj.width - Annotate.scrollDiv.innerWidth();
					scrollPos = (leftOffset + scrollOffset - (page.pageOffsetY * canvasObj.width));
					Annotate.scrollDiv.scrollLeft( scrollPos );
					break;
				case 180: // 180
					scrollOffset = canvasObj.height - Annotate.scrollDiv.innerHeight();
					scrollPos = (Annotate.presentationPageTopOffset + scrollOffset - (page.pageOffsetY * canvasObj.height)) + pageBorder;	//account for 9px border/margin
					Annotate.scrollDiv.scrollTop( scrollPos );
					break;
				case 270: // Left 90
					scrollPos = (leftOffset + (page.pageOffsetY * canvasObj.width)) - pageBorder;	//account for 9px border/margin
					Annotate.scrollDiv.scrollLeft( scrollPos );
					break;
				default: // upright.
					scrollPos = (Annotate.presentationPageTopOffset + (page.pageOffsetY * canvasObj.height)) + pageBorder;	//account for 9px border/margin
					Annotate.scrollDiv.scrollTop( scrollPos );
					break;
			}
		}
		// See if we have more page changes to process.
		if (Annotate.presentationPendingPageSettings.length > 0) {
			setTimeout( Annotate.presentationPageProcess, 200 );
		}
	},

	presentationPromptStart: function()
	{
		console.log( 'presentationPromptStart', window.top.Datacore.s.inPresentation );
		if( window.top.Datacore.s.inPresentation ) {
			if( Annotate.needToSave && Annotate.presentationHost ) {
				Annotate.stopAnnotations();
				var dialogButtons = [];
				dialogButtons.push( {label:"Don't Save", id:'no_save', class:'btn_app_extra'} );
				dialogButtons.push( {label:'Cancel', action:'dismissDialog3Btn();', class:'btn_app_cancel'} );
				dialogButtons.push( {label:'Save', id:'save', class:'btn_app_normal'} );
				var confirmMsg = 'You may have changes to this document.  Are you sure you want to exit without saving?';
				window.top.showDialog3Btn( 'Abort Changes?', confirmMsg, dialogButtons );
				$('#dialog3Btn #no_save', window.top.document).on( "click", function() {
					window.top.dismissDialog3Btn();
					Annotate.redirect();
					if( typeof window.top.fileSelection !== 'undefined' && window.top.fileSelection !== null ) {
						window.top.fileSelection.disableFileSelection();
					}
				} );
				$('#dialog3Btn #save', window.top.document).on( "click", function() {
					window.top.dismissDialog3Btn();
					Annotate.getSaveFilename();
					Annotate.nextState = Annotate.nextStateEnum.Exit;
					if( typeof window.top.fileSelection !== 'undefined' && window.top.fileSelection !== null ) {
						window.top.fileSelection.disableFileSelection();
					}
				} );
			} else {
				Annotate.redirect();
				if( typeof window.top.fileSelection !== 'undefined' && window.top.fileSelection !== null ) {
					window.top.fileSelection.disableFileSelection();
				}
			}
			return;
		}
		var dialogButtons = [];
		dialogButtons.push( {label:'Cancel', action:'window.top.dismissDialog3Btn()', class:'btn_app_cancel'} );
		dialogButtons.push( {label:'Start', action:'window.top.Annotate.presentationStart()', class:'btn_app_normal'} );
		var confirmMsg = 'Would you like to start a presentation?';
		window.top.showDialog3Btn( 'Start Presentation!', confirmMsg, dialogButtons, true );
	},

	presentationStart: function()
	{
		window.top.dismissDialog3Btn();
		var roomID = Annotate.presentationDepositionID + ".deposition";
		setTimeout( function() { Annotate.presentationAttendeesSelected( roomID ); }, 300 );
	},

	presentationAttendeesSelected : function( roomID ) {
		console.log( 'RoomID:', roomID );
		if( Annotate.needToSave ) {
			Annotate.promptStartPresentationWithAnnotations();
			return;
		}
		var pTitle = '';
		var _file = window.top.Datacore.getFile( Annotate.fileID );
		if( typeof _file === 'object' && _file !== null ) {
			pTitle = _file.name;
		}
		var args = {
			"depositionID": Annotate.presentationDepositionID,
			"roomID": roomID,
			"orientation": "landscape",
			"fileID": Annotate.fileID,
			"pageIndex": PDFView.page - 1,
			"pageOffsetX": 0,
			"pageOffsetY": 0,
			"pageScale": 1,
			"pageOrientation": 0,
			"rectOriginX": 0,
			"rectOriginY": 0,
			"rectSizeWidth": 1.656863,
			"rectSizeHeight": 0.969697,
			"annotatorID": window.top.webApp.getS( 'user.ID' ),
			"title": pTitle
		};
		Annotate.cleanEvents();
		window.top.presentationStart( args );
		if( typeof window.top.fileSelection === 'undefined' || window.top.fileSelection === null ) {
			window.top.fileSelection = new window.top.FileSelection();
		}
//		window.top.fileSelection.enableFileSelection();
	},

	presentationDelete : function ( deleteData ) {
		for(var i = 0; i < Annotate.drawList.length; i++) {
			var id = (deleteData.Page + 1).toString();
			var index = parseInt( deleteData.Index, 10 );
			if (Annotate.drawList[i].page === id) {
				if (Annotate.drawList[i].index === index) {
					var drawObj = Annotate.drawList.splice( i, 1 );
					if (drawObj[0].drawType === 'note') {
						Annotate.removeNoteMouseOver( drawObj[0] );
					}
					//back up one to check all of the objects in the drawList.
					i--;
				}
			}
		}
		Annotate.redraw();
	},

	presentationClearAll : function ( clearData ) {
		for(var key in clearData ) {
			var page = ( parseInt( key, 10 ) + 1).toString();
			for(var v = 0; v < clearData[key].length; v++) {
				var index = parseInt( clearData[key][v], 10 );
				for(var i = 0; i < Annotate.drawList.length; i++) {
					if (Annotate.drawList[i].page === page) {
						if (Annotate.drawList[i].index === index) {
							var drawObj = Annotate.drawList.splice( i, 1 );
							if (drawObj[0].drawType === 'note') {
								Annotate.removeNoteMouseOver( drawObj[0] );
							}
							// End looking for the item and skip to the next item in the loop.
							break;
						}
					}
				}
			}
		}
		Annotate.undoList = [];
		Annotate.undoCounter = 0;
		Annotate.needToSave = false;
		Annotate.redraw();
		Annotate.updateUndoButton();
	},

	presentationModify : function ( modifyData ) {
//		console.log( 'annotate::presentationModify() --> modifyData = ', modifyData );
		for(var d = 0; d < Annotate.drawList.length; d++) {
			var id = (modifyData.Page + 1).toString();
			if (Annotate.drawList[d].page === id && Annotate.drawList[d].index === modifyData.Index) {
				var drawObj = Annotate.drawList[d];
				var canvasObj = Annotate.findCanvasObjByID( drawObj.page );
				switch(drawObj.drawType) {
					case 'arrow':
					case 'marker':
						drawObj.color = Annotate.intToHex( modifyData.Color );
						drawObj.opacity = modifyData.Opacity / 100.0;
						drawObj.lineWidth = modifyData.LineWidth + 1;
						drawObj.pointLists[0][0] = Annotate.stringToPoint( modifyData.StartPoint, canvasObj );
						drawObj.pointLists[0][1] = Annotate.stringToPoint( modifyData.EndPoint, canvasObj );
						drawObj.createBoundingBox();
						break;

					case 'pencil':
						if (isset( modifyData.Operation ) && modifyData.Operation === 'add') {
							Annotate.drawList.splice( d, 1 );
							Annotate.presentationDraw( modifyData );
							return;
						}
						drawObj.color = Annotate.intToHex( modifyData.Color );
						drawObj.opacity = modifyData.Opacity / 100.0;
						drawObj.lineWidth = modifyData.LineWidth + 1;
						Annotate.drawUpdateBounds( modifyData.Bounds, drawObj, canvasObj );
						break;

					case 'highlight':
						drawObj.color = Annotate.intToHex( modifyData.Color );
						drawObj.opacity = modifyData.Opacity / 100.0;
				}
			}
		}
		Annotate.redraw();
	},

	// Giving a bounding rect, resize the draw image.
	drawUpdateBounds: function( bounds, drawObj, canvasObj ) {
//		console.log( 'annotate::drawUpdateBounds() --> bounds = ', bounds );
//		console.log( 'annotate::drawUpdateBounds() --> drawObj.boundingBox ', drawObj.boundingBox[0], drawObj.boundingBox[1] );
//		console.log( 'annotate::drawUpdateBounds() --> width = ', canvasObj.width, ' height = ', canvasObj.height );
		var parseBounds = bounds.replace( '{{', '{' ).replace( '},', '}X').replace( '}}', '}' );
		var splitPoints = parseBounds.split( 'X' );
		var point = Annotate.stringToPoint( splitPoints[0], canvasObj );
		var scale = Annotate.stringToBoundPoint( splitPoints[1], canvasObj );
		var scaleX = scale[0] / (drawObj.boundingBox[1][0] - drawObj.boundingBox[0][0]);
		var scaleY = scale[1] / (drawObj.boundingBox[1][1] - drawObj.boundingBox[0][1]);
		var deltaX = 0;
		var deltaY = 0;
		var newBoundingBoxPoint = [ 0, 0 ];
		
		if( canvasObj.rotation === 0 ) {
			deltaX = drawObj.boundingBox[0][0] - point[0];
			deltaY = drawObj.boundingBox[0][1] - point[1];
			newBoundingBoxPoint = [ drawObj.boundingBox[0][0] - deltaX, drawObj.boundingBox[0][1] - deltaY ];
		}
		else if( canvasObj.rotation === 90 ) {
			deltaX = drawObj.boundingBox[1][0] - point[0];
			deltaY = drawObj.boundingBox[0][1] - point[1];
			newBoundingBoxPoint = [ drawObj.boundingBox[1][0] - deltaX, drawObj.boundingBox[0][1] - deltaY ];
		}
		else if( canvasObj.rotation === 180 ) {
			deltaX = drawObj.boundingBox[1][0] - point[0];
			deltaY = drawObj.boundingBox[1][1] - point[1];
			newBoundingBoxPoint = [ drawObj.boundingBox[1][0] - deltaX, drawObj.boundingBox[1][1] - deltaY ];
		}
		else if( canvasObj.rotation === 270 ) {
			deltaX = drawObj.boundingBox[0][0] - point[0];
			deltaY = drawObj.boundingBox[1][1] - point[1];
			newBoundingBoxPoint = [ drawObj.boundingBox[0][0] - deltaX, drawObj.boundingBox[1][1] - deltaY ];
		}
		else {
			console.log( 'annotate::drawUpdateBounds(): ERROR - unknown rotation value ', canvasObj.rotation );
			return;
		}
//		console.log( 'annotate::drawupdateBounds() --> point = ', point, ' deltaXY = ', deltaX, ',', deltaY, ' Rotation = ', canvasObj.rotation );
//		console.log( 'annotate::drawupdateBounds() --> scale = ', scale, ' scaleXY = ', scaleX, ',', scaleY, ' Rotation = ', canvasObj.rotation );
		
		var i, j;
		for(i = 0; i < drawObj.pointLists.length; i++) {
			for(j = 0; j < drawObj.pointLists[i].length; j++) {
				// Translate
				drawObj.pointLists[i][j][0] -= deltaX;
				drawObj.pointLists[i][j][1] -= deltaY;
				drawObj.pointLists[i][j][0] = newBoundingBoxPoint[0] + ( drawObj.pointLists[i][j][0] - newBoundingBoxPoint[0] ) * scaleX;
				drawObj.pointLists[i][j][1] = newBoundingBoxPoint[1] + ( drawObj.pointLists[i][j][1] - newBoundingBoxPoint[1] ) * scaleY;
			}
		}
		drawObj.createBoundingBox();
		drawObj.updateDragPoints();
	},

	updateUndoCounter : function( data ) {
		if (isset( data.undoStackDepth )) {
			Annotate.undoCounter = data.undoStackDepth;
			Annotate.updateUndoButton();
		}
	},

	// This is called each time a page Change happens.  We wait until the user is done changing the page before processing the event.
	pageChange : function( event ) {
//		console.log( 'annotate::pageChange() new page = ', event.originalEvent.pageNumber, ' previous page = ', PDFView.previousPageNumber );
		var eventPage = event.originalEvent.pageNumber;
		if (PDFView.previousPageNumber !== eventPage) {
			if (window.top.Datacore.s.inPresentation && ( /*Annotate.presentationHost ||*/ Annotate.iAmPresentor )) {
				if (Annotate.pageChangeTimerID !== null) {
					window.clearTimeout( Annotate.pageChangeTimerID );
				}
				Annotate.pageChangeTimerID = window.setTimeout( Annotate.pageChangeProcess, Annotate.uiTimeout, event );
			}
			Annotate.updatePageNumControl();
		}
	},

	// This is called one the user has stopped changing the page.
	pageChangeProcess : function( event ) {
//		console.log( 'annotate::pageChangeProcess()' );
		Annotate.pageChangeTimerID = null;
		if ( window.top.Datacore.s.inPresentation ) {
			if ( Annotate.iAmPresentor ) {
				var eventPage = event.originalEvent.pageNumber;
				var pageData = {pageIndex: eventPage - 1 };
				Annotate.presentationPageIndex = eventPage - 1;
				parent.parent.pageProperties( Annotate.presentationDepositionID, pageData );
			} else {
				// Reset the page being displayed
				PDFView.page = PDFView.page;
			}
		}
	},
	
	sendRotation : function( newRotation ) {
//		console.log( 'annotate::sendRotation()--> newRotation = ', newRotation );
		if (!window.top.Datacore.s.inPresentation || !Annotate.iAmPresentor) {
			return;
		}
		var args = {pageOffsetY: 0, pageOffsetX: 0, rectOriginX: 0, rectOriginY: 0, rectSizeWidth: 1.656863, rectSizeHeight: 0.969697 };
		switch( newRotation ) {
			case 90:
				args.pageOrientation = 1;
				args.rectSizeWidth = 1;
				args.rectSizeHeight = 0.58;
				break;

			case 180:
				args.pageOrientation = 2;
				args.rectSizeWidth = 1;
				args.rectSizeHeight = 0.58;
				break;

			case 270:
				args.pageOrientation = 3;
				args.rectSizeWidth = 1;
				args.rectSizeHeight = 0.58;
				break;

			default:
				args.pageOrientation = 0;
				break;
		}
		parent.parent.pageProperties( Annotate.presentationDepositionID, args );
		Annotate.scrollEvent();
	},

	scrollEvent : function() {
		if (window.top.Datacore.s.inPresentation && Annotate.iAmPresentor ) {
			// Cancel any active scroll timer.
			if (Annotate.scrollTimerID !== null) {
				window.clearTimeout( Annotate.scrollTimerID );
			}
			// Set a timer so we wait until the user is done scrolling.
			Annotate.scrollTimerID = window.setTimeout( Annotate.processScroll, Annotate.uiTimeout );
		}
	},

	processScroll : function() {
		Annotate.scrollTimerID = null;

		var args = {pageOffset: 1, pageOffsetX: 0, pageOffsetY: 0, pageOffsetTop: 0};
		var scrollOffset = 0;
		var scrollLeft = Annotate.scrollDiv.scrollLeft();
		var scrollTop = Annotate.scrollDiv.scrollTop();

		var canvasObj = Annotate.findCanvasObjByPage( Annotate.presentationPageIndex );
		if (!isset( canvasObj )) {
			//We have not finished rendering this page yet.
			return;
		}
		var leftOffset = canvasObj.canvas.offsetParent.clientLeft;
		Annotate.presentationPageTopOffset = $(canvasObj.canvas).parents( '.page' )[0].offsetTop;
		if (Annotate.presentationCurrentPageScale != PDFView.currentScale) {
			// if we have a scale change, only send that out.
			delete args.pageOffset;
			args.pageScale = parseFloat( parseFloat( PDFView.currentScale / Annotate.presentationReferenceScale ).toFixed( 12 ) );
			Annotate.presentationCurrentPageScale = PDFView.currentScale;
		}
		// Send pageProperties.
		switch ( PDFView.pageRotation ) {
			case 90: // Right 90
				args.pageOffsetX = Math.max( ( scrollTop - Annotate.presentationPageTopOffset ) / canvasObj.height, 0 );
				scrollOffset = canvasObj.width - Annotate.scrollDiv.innerWidth();
				args.pageOffsetY = Math.max( ( leftOffset + scrollOffset - scrollLeft ) /canvasObj.width, 0 );
				if( !args.pageOffsetX && scrollTop < Annotate.presentationPageTopOffset ) {
					args.pageOffsetTop = ((Annotate.presentationPageTopOffset - scrollTop) / canvasObj.height) * 10000;
				}
				break;
			case 180: // 180
				scrollOffset = canvasObj.width - Annotate.scrollDiv.innerWidth();
				args.pageOffsetX = Math.max( ( leftOffset + scrollOffset - scrollLeft ) / canvasObj.width, 0 );
				scrollOffset = canvasObj.height - Annotate.scrollDiv.innerHeight();
				args.pageOffsetY = Math.max( ( Annotate.presentationPageTopOffset + scrollOffset - scrollTop ) / canvasObj.height, 0 );
				if( !args.pageOffsetY && scrollTop < Annotate.presentationPageTopOffset ) {
					args.pageOffsetTop = ((scrollTop - ( Annotate.presentationPageTopOffset + scrollOffset )) / canvasObj.height) * 10000;
				}
				break;
			case 270: // Left 90
				scrollOffset = canvasObj.height - Annotate.scrollDiv.innerHeight();
				args.pageOffsetX = Math.max( ( Annotate.presentationPageTopOffset + scrollOffset - scrollTop ) / canvasObj.height, 0 );
				args.pageOffsetY = Math.max( ( scrollLeft - leftOffset ) / canvasObj.width, 0 );
				if( !args.pageOffsetX && scrollTop < Annotate.presentationPageTopOffset ) {
					args.pageOffsetTop = ((scrollTop - ( Annotate.presentationPageTopOffset + scrollOffset )) / canvasObj.height) * 10000;
				}
				break;
			default: // upright.
				args.pageOffsetX = Math.max( ( scrollLeft - leftOffset ) / canvasObj.width, 0 );
				args.pageOffsetY = Math.max( ( scrollTop - Annotate.presentationPageTopOffset ) / canvasObj.height, 0 );
				if( !args.pageOffsetY && scrollTop < Annotate.presentationPageTopOffset ) {
					args.pageOffsetTop = ((Annotate.presentationPageTopOffset - scrollTop) / canvasObj.height) * 10000;
				}
				break;
		}
		if (window.top.Datacore.s.inPresentation && Annotate.iAmPresentor ) {
//			console.log( 'annotate:processScroll() --> Annotate.iAmPresentor', Annotate.iAmPresentor );
			window.top.pageProperties( Annotate.presentationDepositionID, args );
		}
	},

	checkPresenterState : function() {
		console.log( 'checkPresenterState()' );
		// This function is used if the leader ends presenation mode while the witness was the annotator
		if (Annotate.passedPresentor) {
			Annotate.passPresenter();
			// TODO: Re-think how this works
			// Set the annotators button bars for the presenter
			$('#content #edit_actions', parent.document).show();
			$('#content #arrow').hide();
			$('#content #colorArrow').hide();
			$('#content #note').hide();
			$('#content #colorNote').hide();
			$('#content #erase').hide();
			$('#content #undo').hide();

			$('#viewerContainer').css( 'top', '' );
			$('#toolbarContainer').show();
			$('#viewerContainer').removeClass( 'viewPresentation' );
			Annotate.iAmPresentor = true;
			if (window.top.Datacore.getVal(window.top.Datacore.s.currentUser, 'userType') !== 'W') {
//				$('#drawerTab').show();
				if( typeof window.top.fileSelection !== 'undefined' && window.top.fileSelection !== null ) {
					window.top.fileSelection.enableFileSelection();
				}
			}
			// Update canvas offsets.
			for(var i = 0; i < Annotate.canvasList.length; i++) {
				var pdf_canvas = $(Annotate.canvasList[i].canvas);
				var offset = pdf_canvas.offset();
				Annotate.canvasList[i].offsetX = offset.left + Annotate.scrollDiv.scrollLeft();
				Annotate.canvasList[i].offsetY = offset.top + Annotate.scrollDiv.scrollTop();
			}
		} else {
			// ASSUMPTION: not in presentation mode so hide pass_annotation button
		}
	},

	passPresenter : function() {
//		console.log( 'passPresenter(): Annotate.passedPresentor = ' + Annotate.passedPresentor );
		if( Annotate.passedPresentor ) {
			// Take back.
			window.top.navBar.allDisable();
			window.top.navBar.showTitle();
			window.top.navBar.backButton.enable();
			window.top.navBar.presentationButton.enable();
			window.top.navBar.annotatorButton.removeClass( 'revoke' ).enable();
			var showIntroduce = true;
			var file = window.top.Datacore.getFile(Annotate.fileID);
			if (!isset( file )) {
				showIntroduce = false;
			} else {
				if (file.isExhibit) {
					showIntroduce = false;
				}
			}
			if (showIntroduce) {
				window.top.navBar.introduceButton.enable();
			}
			window.top.navBar.sendButton.enable();
			if ( window.top.Datacore.getVal(window.top.Datacore.s.currentUser, 'userType') !== 'G' ) {
				window.top.navBar.saveButton.enable();
			}
			$('#file_contents', parent.document).removeClass( 'passAnnotator' );
			$('#viewerContainer').removeClass( 'passAnnotator' );
			$('#toolbarContainer').show();
			Annotate.passedPresentor = false;
			window.top.setAnnotator( window.top.webApp.getS( 'inDeposition' ), {userID: window.top.Datacore.getS('currentUserID')} );
			window.top.navBar.presentationButton.visible();
		} else {
			var witnessUser = Annotate.getWitnessUser();
			if( witnessUser === null || !witnessUser.active ) {
				// The witness dropped out of presentation.
				window.top.navBar.annotatorButton.removeClass( 'active' );
				return;
			}
			window.top.navBar.annotatorButton.addClass( 'revoke' );
			$('#ipad_container', window.top.document).removeClass( 'annotator' ); // causes a "resize" event which causes a viewer::redraw()
			$('#file_contents', parent.document).addClass( 'passAnnotator' );
			$('#viewerContainer').addClass( 'passAnnotator' );
			$('#toolbarContainer').hide();
			$(Annotate.scrollDiv).css("overflow", "");
			Annotate.passedPresentor = true;
			window.top.setAnnotator( window.top.webApp.getS( 'inDeposition' ),  {userID: witnessUser.ID} );
			window.top.navBar.presentationButton.invisible();
			window.top.navBar.introduceButton.disable();
			window.top.navBar.sendButton.disable();
			window.top.navBar.saveButton.disable();
		}
		PDFView.ignoreResizeEvent = true; // HACK: forces the resize event to be ignored  (fixes ED-2550)
	},

	annotatorChange : function( args ) {
		Annotate.stopAnnotations();
		if( isset( args ) && args.annotatorID === window.top.webApp.getS( 'user.ID' ) ) {
			// ASUMPTION: this can only execute for the leader or witness whoever is annotator at the time
			// Set the annotators button bars for the new annotator
//			console.log( 'annotatorChange(): Presentor user type:', window.top.webApp.getS( 'user.userType' ) );
			$('#ipad_container', window.top.document).addClass( 'annotator' );
			window.top.navBar.annotatorButton.removeClass( 'revoke' );
//			$('#content #edit_actions', parent.document).show();
//			$('#content #arrow').hide();
//			$('#content #colorArrow').hide();
//			$('#content #note').hide();
//			$('#content #colorNote').hide();
//			$('#content #erase').hide();
//			$('#content #undo').hide();

			$('#viewerContainer').css( 'top', '' );
//			$('#toolbarContainer').show();
			$('#viewerContainer').removeClass( 'viewPresentation' );
			Annotate.iAmPresentor = true;
			if( window.top.webApp.getS( 'user.userType' ) === 'M' ) {
				window.top.navBar.editorActions.show();
				window.top.navBar.noteButton.disable();
				window.top.navBar.colorNoteButton.disable();
				if( typeof window.top.fileSelection !== 'undefined' && window.top.fileSelection !== null ) {
					window.top.fileSelection.enableFileSelection();
				}
			} else {
				$('#toolbarContainer').show();
				window.top.navBar.editorActions.show();
				window.top.navBar.arrowButton.disable();
				window.top.navBar.colorArrowButton.disable();
				window.top.navBar.noteButton.disable();
				window.top.navBar.colorNoteButton.disable();
				window.top.navBar.eraseButton.disable();
				window.top.navBar.undoButton.disable();
				window.top.navBar.clearAllButton.disable();
			}

			// Update canvas offsets.
			for(var i = 0; i < Annotate.canvasList.length; i++) {
				var pdf_canvas = $(Annotate.canvasList[i].canvas);
				var offset = pdf_canvas.offset();
				Annotate.canvasList[i].offsetX = offset.left + Annotate.scrollDiv.scrollLeft();
				Annotate.canvasList[i].offsetY = offset.top + Annotate.scrollDiv.scrollTop();
			}
		} else {
			// ASUMPTION: this could execute for all attendees in the presentation
			// Set the NON-annotators button bars
			console.log( 'annotatorChange(): NOT presentor user type:', window.top.webApp.getS( 'user.userType' ) );
			// Normal screen button bars
			if( window.top.webApp.userIsSpeaker() === true ) {
				$('#ipad_container', window.top.document).removeClass( 'annotator' );
				window.top.navBar.annotatorButton.addClass( 'revoke' );
			}
//			$('#content #edit_actions', parent.document).hide();
			$('#viewerContainer').css( 'top', '0' );
//			$('#toolbarContainer').hide();
			$('#viewerContainer').addClass( 'viewPresentation' );
			Annotate.iAmPresentor = false;
			$('#toolbarContainer').hide();
			window.top.navBar.editorActions.hide();
			if( typeof window.top.fileSelection !== 'undefined' && window.top.fileSelection !== null ) {
				window.top.fileSelection.disableFileSelection();
			}
		}
		if (isset( args.annotationIndex )) {
			Annotate.annotationIndex = args.annotationIndex + 1;
		}
	},

	addModifyListener : function( canvas ) {
		if( window.top.webApp.getS( 'user.userType' ) === 'W' ) {
			return;
		}
		canvas[0].addEventListener("mouseup", Annotate.modifyEvent, false);
//		canvas[0].addEventListener("touchend", Annotate.modifyTouchEvent, false);
	},

	startModify : function() {
		for(var i = 0; i < Annotate.canvasList.length; i++) {
			var id = 'page' + Annotate.canvasList[i].ID;
			$('#' + id).css( 'z-index', '10' );
		}
	},

	stopModify : function() {
		for(var i = 0; i < Annotate.canvasList.length; i++) {
			var id = 'page' + Annotate.canvasList[i].ID;
			$('#' + id).css( 'z-index', '' );
		}
	},

	modifyEvent : function( event ) {
		// See if the user is doing anything else.
		if (Annotate.drawState !== Annotate.drawStateEnum.None) {
			return;
		}

		if( window.top.Datacore.s.inPresentation === true ) {
			var pData = window.top.webApp.getS( 'joinPresentationData' );
			if( typeof pData !== 'undefined' && pData !== null ) {
				if( pData.annotatorID !== window.top.webApp.getS( 'user.ID' ) ) {
					return;
				}
			}
		}

		// Get page for event.
		var canvasObj = Annotate.findCanvasObjByID( event.target.id );
		if (canvasObj === null) {
			return;
		}

		// Get x, y position of event.
		var currX = event.clientX - canvasObj.offsetX + Annotate.scrollDiv.scrollLeft();
		var currY = event.clientY - canvasObj.offsetY + Annotate.scrollDiv.scrollTop();

		// See if we hit an annotation.
		var i = 0;
		var drawObj = null;
		while (i < Annotate.drawList.length) {
			// See if we are inside of the bounding rect for the object.
			drawObj = Annotate.drawList[i];
			if( canvasObj.ID === drawObj.page && drawObj.hit(currX, currY) ) {
				break;
			}
			drawObj = null;
			i++;
		}
		if (drawObj === null) {
			// We did not find an object -- look for note annotations
			function passThrough( e ) {
				for( var i=0; i<notes.length; ++i ) {
					// check if clicked point (taken from event) is inside element
					var mouseX = e.pageX;
					var mouseY = e.pageY;
					var obj = notes[i];
					var pageScale = PDFView.pages[PDFView.page - 1].scale;
					var width = (obj.clientWidth  * pageScale * 1.33333333 );
					var height = (obj.clientHeight * pageScale * 1.33333333 );
					var offsets = $(obj).offset();
					if( mouseX > offsets.left && mouseX < offsets.left + width && mouseY > offsets.top && mouseY < offsets.top + height ) {
						$('img', $(obj)).trigger( 'click' );
						e.stopPropagation();
						e.cancelBubble = true;
						window.top.lastEvent = e;
					}
				}
			};
			var notes = document.getElementsByClassName("annotText");
			passThrough( event );
			return;
		}

		event.stopPropagation();
		event.cancelBubble = true;
		window.top.lastEvent = event;

		if (drawObj.drawType === 'note') {
			parent.parent.showDialogNoteEntry( drawObj.author, Annotate.convertDate2NoteString( drawObj.date ), drawObj.note );
			Annotate.currentShape = drawObj;
			parent.parent.setDialogNoteEntryCloseAction( Annotate.modifyNoteEntry );
		} else {
			// Show modify annotation menu.
			showMenuModifyAnnotation( drawObj, $(canvasObj.canvas).offset(), Annotate.menuModifyClose, Annotate.menuModifyDown );
			Annotate.selectedAnnotation = drawObj; //.clone();
			Annotate.selectedAnnotation.isSelected = true;
			Annotate.redraw( drawObj.page );
		}
	},

	menuModifyClose : function( id, drawObj ) {
		if (id === 'color') {
			var canvasObj = Annotate.findCanvasObjByID( drawObj.page );
			if (canvasObj === null) {
				return;
			}
			showMenuColor( drawObj, $(canvasObj.canvas).offset(), Annotate.menuColorClose );
		} else if (id === 'thickness') {
			var canvasObj = Annotate.findCanvasObjByID( drawObj.page );
			if (canvasObj === null) {
				return;
			}
			if( drawObj.drawType === 'marker' ) {
				drawObj.lineWidth = ( drawObj.lineWidth - (( drawObj.lineWidth / 2 ) + 3 ));
			}
			showMenuThickness( drawObj, $(canvasObj.canvas).offset(), Annotate.menuThicknessClose );
		} else if (id === 'opacity') {
			var canvasObj = Annotate.findCanvasObjByID( drawObj.page );
			if (canvasObj === null) {
				return;
			}
			showMenuOpacity( drawObj, $(canvasObj.canvas).offset(), Annotate.menuOpacityClose );
		} else if (id === 'delete') {
			for (var i = 0; i < Annotate.drawList.length; i++) {
				if (drawObj.page === Annotate.drawList[i].page && drawObj.index === Annotate.drawList[i].index) {
					Annotate.drawList[i].isSelected = false;
					break;
				}
			}
			Annotate.drawList.splice(i, 1);
			if (drawObj.drawType === 'note') {
				Annotate.removeNoteMouseOver( drawObj );
			}
			if (window.top.Datacore.s.inPresentation && Annotate.presentationHost) {
				Annotate.sendDelete( drawObj );
			}
			Annotate.undoList.push( {operation: 'add', obj: drawObj} );
			Annotate.undoCounter++;
			Annotate.needToSave = true;
			Annotate.selectedAnnotation = null;
			Annotate.redraw( drawObj.page );
		}
	},

	menuModifyDown : function( drawObj, event ) {
		// See if the user clicked on one of the buttons.
		if ($(event.target).hasClass( 'modifyButton' )) {
			return false;
		}
		// See if the user clicked on annotation.
		var canvasObj = Annotate.findCanvasObjByID( Annotate.selectedAnnotation.page );
		if (canvasObj === null) {
			return false;
		}
		var oldDrawObj;
		if (Annotate.selectedAnnotation.drawType === 'arrow' || Annotate.selectedAnnotation.drawType === 'marker') {
			Annotate.currX = event.clientX - canvasObj.offsetX + Annotate.scrollDiv.scrollLeft();
			Annotate.currY = event.clientY - canvasObj.offsetY + Annotate.scrollDiv.scrollTop();
			// Check for drag points.
			var point = Annotate.selectedAnnotation.hitDragPoint( Annotate.currX, Annotate.currY );
			if (point !== false) {
				console.log( 'drag Point Arrow hit :' + point );
				Annotate.selectedDragPoint = point;
				canvasObj.canvas.addEventListener("mouseup", Annotate.modifyMoveUp, false);
				canvasObj.canvas.addEventListener("mouseout", Annotate.modifyMoveUp, false);
				canvasObj.canvas.addEventListener("mousemove", Annotate.modifyMove, false);
				$(Annotate.scrollDiv).css("overflow", "hidden");
				// Save off the current annotation.
				oldDrawObj = Annotate.selectedAnnotation.clone();
				Annotate.undoList.push( {operation: 'modify', obj: oldDrawObj} );
				return true;
			}
			if (Annotate.selectedAnnotation.hit( Annotate.currX, Annotate.currY )) {
				// Capture mouse move, mouseup, and mouseout events.
				canvasObj.canvas.addEventListener("mouseup", Annotate.modifyMoveUp, false);
				canvasObj.canvas.addEventListener("mouseout", Annotate.modifyMoveUp, false);
				canvasObj.canvas.addEventListener("mousemove", Annotate.modifyMove, false);
				$(Annotate.scrollDiv).css("overflow", "hidden");
				// Save off the current annotation.
				oldDrawObj = Annotate.selectedAnnotation.clone();
				Annotate.undoList.push( {operation: 'modify', obj: oldDrawObj} );
				return true;
			}
		} else if (Annotate.selectedAnnotation.drawType === 'pencil') {
			Annotate.currX = event.clientX - canvasObj.offsetX + Annotate.scrollDiv.scrollLeft();
			Annotate.currY = event.clientY - canvasObj.offsetY + Annotate.scrollDiv.scrollTop();
			// Check for drag points.
			var point = Annotate.selectedAnnotation.hitDragPoint( Annotate.currX, Annotate.currY );
			if (point !== false) {
				console.log( 'drag Point Pencil hit :' + point );
				Annotate.selectedDragPoint = point;
				canvasObj.canvas.addEventListener("mouseup", Annotate.modifyMoveUp, false);
				canvasObj.canvas.addEventListener("mouseout", Annotate.modifyMoveUp, false);
				canvasObj.canvas.addEventListener("mousemove", Annotate.modifyMove, false);
				$(Annotate.scrollDiv).css("overflow", "hidden");
				// Save off the current annotation.
				oldDrawObj = Annotate.selectedAnnotation.clone();
				Annotate.undoList.push( {operation: 'modify', obj: oldDrawObj} );
				return true;
			}
			if (( Annotate.selectedAnnotation.boundingBox[0][0] - Annotate.dragIconOffset <= Annotate.currX && Annotate.currX <= Annotate.selectedAnnotation.boundingBox[1][0] + Annotate.dragIconOffset ) &&
				( Annotate.selectedAnnotation.boundingBox[0][1] - Annotate.dragIconOffset <= Annotate.currY && Annotate.currY <= Annotate.selectedAnnotation.boundingBox[1][1] + Annotate.dragIconOffset )) {
				// Capture mouse move, mouseup, and mouseout events.
				canvasObj.canvas.addEventListener("mouseup", Annotate.modifyMoveUp, false);
				canvasObj.canvas.addEventListener("mouseout", Annotate.modifyMoveUp, false);
				canvasObj.canvas.addEventListener("mousemove", Annotate.modifyMove, false);
				$(Annotate.scrollDiv).css("overflow", "hidden");
				// Save off the current annotation.
				oldDrawObj = Annotate.selectedAnnotation.clone();
				Annotate.undoList.push( {operation: 'modify', obj: oldDrawObj} );
				return true;
			}
		}
		Annotate.modifyAnnotationUpdate();
		return true;
	},

	modifyMoveUp : function() {
		// Unhook the events.
		var canvasObj = Annotate.findCanvasObjByID( Annotate.selectedAnnotation.page );
		if (canvasObj !== null) {
			canvasObj.canvas.removeEventListener("mouseup", Annotate.modifyMoveUp, false);
			canvasObj.canvas.removeEventListener("mouseout", Annotate.modifyMoveUp, false);
			canvasObj.canvas.removeEventListener("mousemove", Annotate.modifyMove, false);
			$(Annotate.scrollDiv).css("overflow", "auto");
			Annotate.selectedDragPoint = null;

			if (Annotate.iAmPresentor) {
				Annotate.sendModify( Annotate.selectedAnnotation );
			} else {
				Annotate.undoCounter++;
			}
			showMenuModifyAnnotation( Annotate.selectedAnnotation, $(canvasObj.canvas).offset(), Annotate.menuModifyClose, Annotate.menuModifyDown );
			Annotate.redraw( Annotate.selectedAnnotation.page );
		}
	},

	modifyMove : function( event ) {
		var canvasObj = Annotate.findCanvasObjByID( Annotate.selectedAnnotation.page );
		if (canvasObj !== null) {
			Annotate.prevX = Annotate.currX;
			Annotate.prevY = Annotate.currY;
			Annotate.currX = event.clientX - canvasObj.offsetX + Annotate.scrollDiv.scrollLeft();
			Annotate.currY = event.clientY - canvasObj.offsetY + Annotate.scrollDiv.scrollTop();
			var deltaX = Annotate.currX - Annotate.prevX;
			var deltaY = Annotate.currY - Annotate.prevY;
			if (Annotate.selectedAnnotation.drawType === 'arrow' || Annotate.selectedAnnotation.drawType === 'marker' ) {
				if (Annotate.selectedDragPoint !== null) {
					Annotate.selectedAnnotation.pointLists[0][Annotate.selectedDragPoint][0] += deltaX;
					Annotate.selectedAnnotation.pointLists[0][Annotate.selectedDragPoint][1] += deltaY;
				} else {
					Annotate.selectedAnnotation.translate( deltaX, deltaY );
				}
			} else if (Annotate.selectedAnnotation.drawType === 'pencil') {
				if (Annotate.selectedDragPoint !== null) {
					var bounds = JSON.parse( JSON.stringify( Annotate.selectedAnnotation.boundingBox ) );
					switch( Annotate.selectedDragPoint) {
						case 0:	// Top left
							bounds[0][0] += deltaX;
							bounds[0][1] += deltaY;
							break;

						case 1: // Top middle
							bounds[0][1] += deltaY;
							break;

						case 2: // Top right
							bounds[1][0] += deltaX;
							bounds[0][1] += deltaY;
							break;

						case 3: // Middle right
							bounds[1][0] += deltaX;
							break;

						case 4: // Bottom right
							bounds[1][0] += deltaX;
							bounds[1][1] += deltaY;
							break;

						case 5: // Bottom middle
							bounds[1][1] += deltaY;
							break;

						case 6: // Bottom left
							bounds[0][0] += deltaX;
							bounds[1][1] += deltaY;
							break;

						case 7: // Left middle
							bounds[0][0] += deltaX;
							break;
					}
					// Convert bounds to message format.
					var width = bounds[1][0] - bounds[0][0];
					var height = bounds[1][1] - bounds[0][1];

					var strBounds = '{' + Annotate.formatPoint( bounds[0], canvasObj.width, canvasObj.height ) + ',' +
										  Annotate.formatBoundPoint([width, height], canvasObj.width, canvasObj.height ) + '}';
					Annotate.drawUpdateBounds( strBounds, Annotate.selectedAnnotation, canvasObj );
				} else {
					Annotate.selectedAnnotation.translate( deltaX, deltaY );
				}
			}
		}
		Annotate.selectedAnnotation.createBoundingBox();
		Annotate.selectedAnnotation.updateDragPoints();
		Annotate.redraw( Annotate.selectedAnnotation.page );
	},

	menuColorClose : function( color, drawObj ) {
		if(!isset( color )) {
			// User clicked out of the menu.
			Annotate.modifyAnnotationUpdate();
			return;
		}
		// Save old version of drawObj in undo list.
		var oldDrawObj = drawObj.clone();
		Annotate.undoList.push( {operation: 'modify', obj: oldDrawObj} );
		Annotate.selectedAnnotation.color = color
		if (Annotate.iAmPresentor) {
			Annotate.sendModify( Annotate.selectedAnnotation );
		} else {
			Annotate.undoCounter++;
		}
		Annotate.modifyAnnotationUpdate();
	},

	menuThicknessClose : function( thickness, drawObj ) {
		if(!isset( thickness )) {
			// User clicked out of the menu.
			Annotate.modifyAnnotationUpdate();
			return;
		}
		// Save old version of drawObj in undo list.
		var oldDrawObj = drawObj.clone();
		Annotate.undoList.push( {operation: 'modify', obj: oldDrawObj} );
		if( Annotate.selectedAnnotation.drawType === 'marker' ) {
			Annotate.selectedAnnotation.lineWidth = ( 2 * thickness ) + 6;
		} else {
			Annotate.selectedAnnotation.lineWidth = thickness;
		}
		if (Annotate.iAmPresentor) {
			Annotate.sendModify( Annotate.selectedAnnotation );
		} else {
			Annotate.undoCounter++;
		}
		Annotate.modifyAnnotationUpdate();
	},

	menuOpacityClose : function( opacity, drawObj ) {
		if(!isset( opacity )) {
			// User clicked out of the menu.
			Annotate.modifyAnnotationUpdate();
			return;
		}
		// Save old version of drawObj in undo list.
		var oldDrawObj = drawObj.clone();
		Annotate.undoList.push( {operation: 'modify', obj: oldDrawObj} );
		if( Annotate.selectedAnnotation.drawType === 'marker' ) {
			opacity = (( Math.round( opacity / 25) - 1 ) * 15 ) + 20;
		}
		Annotate.selectedAnnotation.opacity = opacity / 100.0;
		if (Annotate.iAmPresentor) {
			Annotate.sendModify( Annotate.selectedAnnotation );
		} else {
			Annotate.undoCounter++;
		}
		Annotate.modifyAnnotationUpdate();
	},

	modifyAnnotationUpdate : function() {
		// Update the original draw object with the current values.
		var i = 0;
		var page = null;
		while (i < Annotate.drawList.length) {
			if (Annotate.drawList[i].page === Annotate.selectedAnnotation.page && Annotate.drawList[i].index === Annotate.selectedAnnotation.index) {
				Annotate.drawList[i] = Annotate.selectedAnnotation;
				Annotate.drawList[i].isSelected = false;
				page = Annotate.selectedAnnotation.page;
				break;
			}
			i++;
		}
		Annotate.selectedAnnotation = null;
		Annotate.redraw( page );
	},

	modifyNoteEntry : function( note, trash ) {
		if (!trash) {
			if (note === Annotate.currentShape.note) {
				// Nothing has changed in the text, so don't save anything.
				Annotate.currentShape = null;
				return;
			}
			var oldDrawObj = Annotate.currentShape.clone();
			Annotate.undoList.push( {operation: 'modify', obj: oldDrawObj} );
			Annotate.currentShape.date = Date.now();
			Annotate.currentShape.note = note;
			Annotate.undoCounter++;
			Annotate.currentShape.createBoundingBox();
			Annotate.removeNoteMouseOver( Annotate.currentShape );
			Annotate.addNoteMouseOver( Annotate.currentShape );
		} else {
			Annotate.menuModifyClose( 'delete', Annotate.currentShape );
		}
		Annotate.currentShape = null;
	},

	sendModify : function( drawObj ) {
		if (drawObj.drawType === 'highlight') {
			var drawData = {Page: parseInt( drawObj.page ) - 1, Opacity: Math.floor( drawObj.opacity * 100 ), LineWidth: drawObj.lineWidth,
							Color: Annotate.colorToInt( drawObj.color ), Index: drawObj.index, StartPoint: "{0,0}", EndPoint: "{0,0}" };
			parent.parent.modifyAnnotation( Annotate.presentationDepositionID, drawData );
		} else if (drawObj.drawType === 'arrow' || drawObj.drawType === 'marker') {
			var canvasObj = Annotate.findCanvasObjByID( drawObj.page );
			var start = Annotate.formatPoint( drawObj.pointLists[0][0], canvasObj.width, canvasObj.height, canvasObj.rotation );
			var end = Annotate.formatPoint( drawObj.pointLists[0][1], canvasObj.width, canvasObj.height, canvasObj.rotation );
			var boundingBox = Annotate.formatBounds( drawObj.pointLists[0], canvasObj );
			console.log( 'annotate::sendModify() --> boundingBox = ', boundingBox );

			var drawData = {Page: parseInt( drawObj.page ) - 1, Opacity: Math.floor( drawObj.opacity * 100 ), LineWidth: drawObj.lineWidth / Annotate.scale,
							Color: Annotate.colorToInt( drawObj.color ), Index: drawObj.index, StartPoint: start, EndPoint: end, Bounds: boundingBox };
			parent.parent.modifyAnnotation( Annotate.presentationDepositionID, drawData );
		} else if (drawObj.drawType === 'pencil') {
			var canvasObj = Annotate.findCanvasObjByID( drawObj.page );
			var width = (drawObj.boundingBox[1][0] - drawObj.boundingBox[0][0]);
			var height = (drawObj.boundingBox[1][1] - drawObj.boundingBox[0][1]);
			var boundingBox = Annotate.formatBounds( drawObj.boundingBox, canvasObj );
			console.log( 'annotate::sendModify() --> boundingBox = ', boundingBox );

			var drawData = {Page: parseInt( drawObj.page ) - 1, Opacity: Math.floor( drawObj.opacity * 100 ), LineWidth: drawObj.lineWidth / Annotate.scale,
							Color: Annotate.colorToInt( drawObj.color ), Index: drawObj.index, Bounds: boundingBox, StartPoint: "{0,0}", EndPoint: "{0,0}" };
			parent.parent.modifyAnnotation( Annotate.presentationDepositionID, drawData );
		}
	},

	canvasSetup : function(event) {
//		console.log( 'annotate::canvasSetup() --> event = ', event );
		Annotate.stopAnnotations();
		var pdf_canvas = $(event.target).find('.canvasWrapper > canvas');
		var offset = pdf_canvas.offset();
		Annotate.offsetX = offset.left = offset.left + Annotate.scrollDiv.scrollLeft();
		Annotate.offsetY = offset.top = offset.top + Annotate.scrollDiv.scrollTop();
		pdf_canvas.css("position", "absolute");
		var width = pdf_canvas[0].width / Annotate.deviceRatio;
		var height = pdf_canvas[0].height / Annotate.deviceRatio;
		var annotate_canvas = $('<canvas id="'+pdf_canvas.attr("id").substr(4)+'" style="z-index: 10;" class="annotate" width="'+width+'" height="'+height+'"></canvas>');
		annotate_canvas.css("width", width);
		annotate_canvas.css("height", height);
		annotate_canvas.css("position", "absolute");
		$(event.target).find('.canvasWrapper').append(annotate_canvas);
		// Turn off text selecting at each page with text.
		$('.textLayer').parent().each(function() {
			$(this).css({'-moz-user-select':'-moz-none',
				'-moz-user-select':'none',
				'-o-user-select':'none',
				'-khtml-user-select':'none',
				'-webkit-user-select':'none',
				'-ms-user-select':'none',
				'user-select':'none'});
		});
		Annotate.drawInit(annotate_canvas, offset);
		$('#viewerContainer').removeClass('loading');
		if (Annotate.firstRender) {
			Annotate.firstRender = false;

			// Get any annotations I missed.
			if (window.top.Datacore.s.inPresentation) {
				parent.parent.getAnnotations( Annotate.presentationDepositionID );
			}
			if (PDFView.currentScaleValue === 'page-width' || PDFView.currentScaleValue === 'auto') {
				Annotate.presentationReferenceScale = PDFView.currentScale;
			}
		}

		if (window.top.Datacore.s.inPresentation) {
			if (Annotate.presentationPendingPageSettings.length > 0) {
				Annotate.presentationPageProcess();
			}
			Annotate.checkPresentationPendingAnnotations();
			if (Annotate.presentationHost) {
				Annotate.addModifyListener( annotate_canvas );
			}
		} else {
			Annotate.addModifyListener( annotate_canvas );
		}
		Annotate.updatePageNumControl();
	},

	cleanEvents : function() {
//		$('#ipad_navbar_back a', parent.parent.document).off("click", Annotate.backButton);
//		$('#ipad_navbar_download', parent.parent.document).off("click", Annotate.download);
//		$('#ipad_navbar_presentation', parent.parent.document).off("click", Annotate.presentationSelectAttendees);
//		$('#ipad_navbtn_pass_annotation', parent.parent.document).off("click", Annotate.passPresenter);
//		$('#ipad_navbar_fullscreen', parent.parent.document).off("click", Annotate.fullscreen);
//		$('#ipad_navbtn_introduce', parent.parent.document).off("click");
//		$('#ipad_navbtn_send', parent.parent.document).off("click");
//		$('#ipad_navbtn_save', parent.parent.document).off("click");
		$(window).off( 'resize', Annotate.resizeEvent );
	},

	resizeEvent : function() {
//		console.log( 'annotate:: --> handling resize event' );
		Annotate.stopAnnotations();
		if (isset( window.devicePixelRatio )) {
			Annotate.deviceRatio = window.devicePixelRatio;
		} else {
			Annotate.deviceRatio = screen.deviceXDPI / screen.logicalXDPI;
		}
	},

	init: function()
	{
//		console.log( 'Annotate.init' );
		// Should be called only once.

		//main toolbar
		window.top.navBar.backButton.onClick( Annotate.backButton );
		window.top.navBar.downloadButton.onClick( Annotate.download );
		window.top.navBar.presentationButton.onClick( Annotate.presentationPromptStart );
		window.top.navBar.annotatorButton.onClick( Annotate.passPresenter );

		//editor actions
		window.top.navBar.arrowButton.onClick( Annotate.startArrow ).removeClass( 'selected' );
		window.top.navBar.colorArrowButton.onClick( Annotate.colorPicker );
//		window.top.navBar.textSelectButton.onClick( Annotate.startSelect );
		window.top.navBar.textSelectButton.onClick( Annotate.startMarker ).removeClass( 'selected' );
		window.top.navBar.colorSelectButton.onClick( Annotate.colorPicker );
		window.top.navBar.drawingButton.onClick( Annotate.toggleDrawing ).removeClass( 'selected' );
		window.top.navBar.colorDrawButton.onClick( Annotate.colorPicker );
		window.top.navBar.noteButton.onClick( Annotate.selectNotePosition ).removeClass( 'selected' );
		window.top.navBar.colorNoteButton.onClick( Annotate.colorPicker );
		window.top.navBar.eraseButton.onClick( Annotate.erase ).removeClass( 'eraseMode' );
		window.top.navBar.undoButton.onClick( Annotate.undo );
		window.top.navBar.clearAllButton.onClick( Annotate.promptRemoveAll );

		window.top.navBar.canHideToolbar( true );

		$(window).on( 'resize', Annotate.resizeEvent );
		Annotate.dragIcon = document.getElementById( 'dragIcon' );

		var userType = window.top.webApp.getS( 'user.userType' );
		if ( userType === 'W' || userType === 'WM' ) {
			Annotate.selectColor = "#0090ff";
			Annotate.drawColor =  "#0090ff";
			Annotate.arrowColor =  "#0090ff";
			Annotate.noteColor =  "#0090ff";
		}
		if ( userType === 'W' ) {
			window.top.navBar.saveButton.onClick( Annotate.share ).enable();
		} else {
			if( window.top.webApp.userIsSpeaker() === true ) {
				var introduceFn = (window.top.webApp.getS( 'deposition.class' ) === 'WitnessPrep' || window.top.webApp.getS( 'deposition.class' ) === 'WPDemo') ? Annotate.witnessPrepIntroduce : Annotate.introduce;
				window.top.navBar.introduceButton.onClick( introduceFn ).addClass( 'dark' ).enable();
			}
			window.top.navBar.sendButton.onClick( Annotate.send ).enable();
			window.top.navBar.downloadButton.enable();
			if ( userType !== 'G' || window.top.Datacore.getS('depositionStatus') === 'F') {
				window.top.navBar.saveButton.onClick( Annotate.getSaveFilename ).enable();
			}
		}
		if( typeof hideSpinner === 'function' ) {
			hideSpinner();
		}

		var pData = window.top.webApp.getS( 'joinPresentationData' );
		Annotate.scrollDiv = $('#viewerContainer');
		Annotate.scrollDiv.scroll( Annotate.scrollEvent );

		Annotate.drawState = Annotate.drawStateEnum.None;
		Annotate.nextState = Annotate.nextStateEnum.None;
		Annotate.colorPickType = Annotate.colorPickTypeEnum.None;
		var folders = window.top.Datacore.getFolders();
		if( typeof folders === 'undefined' || folders === null || folders.length === 0 ) {
			window.top.Datacore.syncFolders( null, Annotate.getPersonalFolderID );
		} else {
			Annotate.getPersonalFolderID();
		}
		if (!isset( window.top.Datacore.s.fullscreen )) {
			window.top.Datacore.s.fullscreen = false;
		}
//		if (window.top.Datacore.s.fullscreen) {
//			window.top.navBar.enterFullscreen( false );
//		}
		if(isset( window.top.Datacore.s.tempFile )) {
			delete window.top.Datacore.s.tempFile;
		}
		Annotate.updateSpeaker();
		Annotate.resizeEvent();
		if ( window.top.Datacore.getVal(window.top.Datacore.s.currentUser, 'userType') === 'W' && !Annotate.needToSave) {
			window.top.navBar.saveButton.addClass( 'disabled' );
		}

		if ( window.top.Datacore.getS('parentID') !== 0) {
			Annotate.presentationDepositionID = window.top.Datacore.getS('parentID');
		} else {
			Annotate.presentationDepositionID = window.top.Datacore.getS('depositionID');
		}
		if( typeof pData !== 'undefined' && pData !== null && window.top.Datacore.s.inPresentation === true ) {
			Annotate.sourceFileID = pData.fileID;
			if( window.top.webApp.userIsSpeaker() === true ) {
				Annotate.presentationHost = true;
				if( typeof window.top.fileSelection !== 'undefined' && window.top.fileSelection !== null ) {
					window.top.fileSelection.enableFileSelection();
				}
			} else {
				// Force subscribers into fullscreen for viewing presentations
				console.log( 'Init(): Subscriber going to fullscreen (window.top.Datacore.s.fullscreen = ' + window.top.Datacore.s.fullscreen + ')' );
				window.top.Datacore.s.subscriberFullscreen = (typeof window.top.Datacore.s.fullscreen === 'undefined') ? false : window.top.Datacore.s.fullscreen;
				window.top.Datacore.s.fullscreen = true;
			}
			Annotate.updateButtons();
			if( Annotate.presentationHost ) {
				// this code block fixes a rotate issue when the presentor refreshes the browser but also causes
				// a subscriber to not scroll up correctly
				var pageChange = {pageScale:pData.pageScale, pageIndex:pData.pageIndex, pageOffsetX:pData.pageOffsetX, pageOffsetY:pData.pageOffsetY, orientation:pData.orientation};
				 if (isset( pData.pageOrientation )) {
					 pageChange.pageOrientation = pData.pageOrientation;
				 }
				 Annotate.presentationPendingPageSettings.push( pageChange );
			}
			 console.log( 'init: presentationPendingPageSettings count: ' + Annotate.presentationPendingPageSettings.length );
			 if (isset( pData.annotatorID )) {
				 Annotate.annotatorChange( pData );
			 }
			 if (isset( pData.undoStackDepth )) {
				 Annotate.undoCounter = pData.undoStackDepth;
				 Annotate.updateUndoButton();
			 }
		} else {
			window.top.Datacore.s.inPresentation = false;
			window.top.webApp.removeItem( 'inPresentation' );
		}
		Annotate.fileID = (window.top.Datacore.s.inPresentation) ? pData.fileID : parent.requestedFileID;
		Annotate.currentFile = window.top.Datacore.getFile( Annotate.fileID );
		if( !window.top.Datacore.s.inPresentation && window.top.webApp.userIsSpeaker() !== true ) {
			if( typeof window.top.Datacore.s.subscriberFullscreen !== 'undefined' && window.top.Datacore.s.subscriberFullscreen !== null ) {
				// Return presentation subscribers to their previous screen
				console.log( 'Init(): Subscriber return to previous screen' );
				window.top.Datacore.s.fullscreen = window.top.Datacore.s.subscriberFullscreen;
				delete window.top.Datacore.s.subscriberFullscreen;
			}
		}
		Annotate.updateUsers();
	},

	getPersonalFolderID: function()
	{
		var userType = window.top.webApp.getS( 'user.userType' );
		var folders = window.top.Datacore.getFolders();
		for( var f in folders ) {
			var _folder = folders[f];
			if( _folder.class === 'Personal' || (userType === 'WM' && _folder.class === 'WitnessAnnotations') ) {
				Annotate.personalFolderID = _folder.ID;
				return;
			}
		}
	},

	updatePageNumControl: function()
	{
		var pageNumCtrl = $('#pageNumDisplay');
		if( typeof pageNumCtrl !== 'undefined' && pageNumCtrl !== null && pageNumCtrl.length > 0 ) {
			$('#pageNumPage', pageNumCtrl).text( PDFView.page );
			$('#pageNumTotal', pageNumCtrl).text( PDFView.pdfDocument.numPages );
			pageNumCtrl.show();
		}
	}
};

// Used by all of the modify menus to position onscreen.
function positionMenu(menu, drawObj, offset) {
	var width = menu.width();
	var height = menu.height();
	var objWidth = drawObj.boundingBox[1][0] - drawObj.boundingBox[0][0];
	var toolbarHeight = $('#mainContainer .toolbar').height();
	var padding = 30;
	var x, y;
	x = drawObj.boundingBox[0][0] + objWidth / 2 + offset.left - width / 2;
	var viewWidth = $('#mainContainer').width();

	//Do on screen bounds check
	if (drawObj.boundingBox[0][1] + offset.top - padding - toolbarHeight < height) {
		var viewHeight = $('#mainContainer').height();
		if (drawObj.boundingBox[1][1] + offset.top + padding + toolbarHeight + height > viewHeight) {
			// Position in the middle of the annotation.
			y = drawObj.boundingBox[0][1] + (drawObj.boundingBox[1][1] - drawObj.boundingBox[0][1]) / 2 + offset.top - height;
		} else {
			// Position at the bottom of the annotation.
			y = drawObj.boundingBox[1][1] + offset.top + padding;
		}
	} else {
		// Position at the top.
		y = drawObj.boundingBox[0][1] + offset.top - height - padding
	}
	x = Math.min(Math.max(padding, x), viewWidth - padding - width);
	menu.css('left', x + 'px');
	menu.css('top', y + 'px');
}

$(document).ready( function() {
	window.onbeforeunload = function()
	{
		delete window.top.Annotate;
		delete window.top.lastEvent;
	};
} );