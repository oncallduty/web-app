<?php

App::uses( 'AppController', 'Controller' );
App::uses( 'AppAuthComponent', 'Controller/Component' );
App::uses( 'AppAuthenticate', 'Controller/Component/Auth' );
App::uses( 'EDUser', 'Model' );
App::uses( 'EDCase', 'Model' );
App::uses( 'BrandingComponent', 'Controller/Component' );

class UsersController extends AppController
{
	public $name = 'Users';
	public $helpers = array( 'Html', 'Form', 'Js' );
	public $components = array(
		'Session',
		'Auth' => [
			'authenticate' => [
				'App' => ['userModel' => 'EDUser'],
				'Guest' => ['userModel' => 'EDUser'],
				'Witness' => ['userModel' => 'EDUser'],
			]
		],
		'Branding',
		'Cookie'
	);

	/**
	 * @var EDDeposition
	 */
	protected $EDDeposition;

	public function __construct( $request=NULL, $response=NULL )
	{
		$this->layout = 'users';
		parent::__construct( $request, $response );
		$this->EDDeposition = ClassRegistry::init( 'EDDeposition' );
	}

	public function index()
	{
		if ($this->request->params['pass']) {
			parent::index();
			return;
		}

		$this->log( 'Users::index; unset session and redirect to signin', 'webapp' );
		session_unset();
		$this->redirect( '/signin' );
	}

	public function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow( 'signin', 'members','nonmembers', 'guests', 'witness', 'reset', 'login', 'logout', 'applyBranding' );
		$this->Auth->userModel = 'EDUser';
	}

	public function wildIndex()
	{
		$this->log( 'Users::wildIndex; redirect to /', 'webapp' );
		$this->redirect( '/' );
	}

	public function signin()
	{
		$last_error = null;

		if ($this->Cookie->check('last_error')){
			$current_views = $this->Cookie->read('last_error_views');
			$this->Cookie->write('last_error_views',$current_views+1);

			if ($current_views >= 2){
				$this->Cookie->delete('last_error');
				$this->Cookie->delete('last_error_views');
			}else{
				//$last_error = $this->Cookie->read('last_error').'('.$current_views.')';
				$last_error = $this->Cookie->read('last_error');
			}
		}




		$this->Session->destroy();
		$this->layout = 'usersSignin';
		$this->view = 'roles';
		$this->set( 'brand', $this->Branding->brand );
		$this->set('last_error',$last_error);
	}

	public function nonmembers()
	{
		
		if( !$this->request->is( 'post' ) ) {
			$this->Session->destroy();
			$sessionID = (int)$this->request->query( 'SessionID' ) ? (int)$this->request->query( 'SessionID' ) : '';
			$this->layout = 'userslogin';
			$this->set( 'brand', $this->Branding->brand );
			$siteConfig = Configure::read( 'SiteConfig' );
			$this->set( 'nodeJS', $siteConfig['nodeJS'] );
			$this->set( 'sessionID', $sessionID );
			return;
		}

		$this->layout = 'ajax';
		$this->view = 'ajax_login';

		//Remove any sKey that is set.
		$this->Session->delete( 'EDSource.sKey' );
		$this->Session->delete( 'Auth.User' );
		$this->Session->id();	//start session
		if (!$this->Auth->login())
		{
			$error = EDUser::$lastResponse['error'];
			$message = $error['description'];
			if ($error['code'] === 1001 || $error['code'] === 0) {
				$message = 'The Deposition ID or passcode could not be verified';
			}
			$this->set( 'lastResponse', json_encode( ['success' => false, 'code' => $error['code'], 'error' => $message,] ) );
			return;
		}

		// get the API response for $this->Auth->login()
		$lastResponse = EDUser::$lastResponse;

		$result = ['success' => $lastResponse['success']];

		if (isset( $lastResponse['result']['sKey'] ) && $lastResponse['result']['sKey'])
		{
			$this->Session->write( 'EDSource.sKey', $lastResponse['result']['sKey'] );
			$result['sKey'] = $lastResponse['result']['sKey'];
			$result['witnessID'] = $lastResponse['result']['witnessID'];
			$result['depositionID'] = $lastResponse['result']['depositionID'];
			$result['userType'] = $lastResponse['result']['userType'];
			$this->Session->write( 'Depositions.ID', $result['depositionID'] );
		}

		if( isset( $lastResponse['result']['tutorialVideos'] ) ) {
			$this->Session->write( 'User.tutorialVideos', $lastResponse['result']['tutorialVideos'] );
		}

		if (isset( $lastResponse['result']['deposition'] ))
		{
			// login directly to deposition
			$this->Session->write( 'Depositions.ID', $lastResponse['result']['depositionID'] );
			$this->Session->write( 'Depositions.deposition', $lastResponse['result']['deposition'] );
			if (isset( $lastResponse['result']['folders'] ) ) {
				$this->Session->write( 'Depositions.folders', $lastResponse['result']['folders'] );
			}
			$result['redirect'] = '/i';
		}

		$authUser = $this->Session->read( 'Auth.User' );
		$sortPreferences = $authUser['sortPreferences'];
		if (!isset( $sortPreferences )) {
			$sortPreferences = [];
		}
		$this->Session->write( 'User.sortPreferences', $sortPreferences );
		$this->log( 'Login successful. userID: ' . $authUser['ID'] . ' name:' . $authUser['firstName'] . ' ' . $authUser['lastName'], 'webapp' );
		$this->set( 'lastResponse', json_encode( $result ) );

	}

	public function members()
	{
		if( !$this->request->is( 'post' ) ) {
			$this->Session->destroy();
			$this->layout = 'userslogin';
			$this->set( 'brand', $this->Branding->brand );
			return;
		}

		$this->layout = 'ajax';
		$this->view = 'ajax_login';

		//Remove any sKey that is set.
		$this->Session->delete( 'EDSource.sKey' );
		if( !$this->Auth->login() ) {
			$error = EDUser::$lastResponse['error'];
			$message = $error['description'];
			if( $error['code'] === 401 || $error['code'] === 403 ) {
				$message = 'The username or password could not be verified';
			} elseif( $error['code'] === 418 ) {
				$message = 'Because of security improvements a password change confirmation has been sent to the account owner.';
			} elseif( $error['code'] === 1001 ) {
				$message = 'The Session ID or passcode could not be verified';
			}
			$this->set( 'lastResponse', json_encode( ['success' => false, 'code' => $error['code'], 'error' => $message,] ) );
			return;
		}

		// must be called after $this->Auth->login()
		$lastResponse = EDUser::$lastResponse;

		$result = ['success' => $lastResponse['success']];

		//sKey
		if (isset( $lastResponse['result']['sKey'] ) && $lastResponse['result']['sKey']) {
			$this->Session->write( 'EDSource.sKey', $lastResponse['result']['sKey'] );
		}

		// link deposition -- create new or browse case/deposition
		if (isset( $lastResponse['result']['needToLinkDepo'] ) && $lastResponse['result']['needToLinkDepo']) {
			$result['needToLinkDepo'] = (bool)$lastResponse['result']['needToLinkDepo'];
			$this->Session->write( 'Depositions.class', $lastResponse['result']['deposition']['class'] );
		}

		if( isset( $lastResponse['result']['tutorialVideos'] ) ) {
			$this->Session->write( 'User.tutorialVideos', $lastResponse['result']['tutorialVideos'] );
		}

		if (!isset( $lastResponse['result']['deposition'] )) {
			if (!isset( $result['needToLinkDepo'] )) {
				$result['redirect'] = '/i';
			}
		} else {
			$this->Session->write( 'Depositions.ID', $lastResponse['result']['deposition']['ID'] );
			$this->Session->write( 'Depositions.deposition', $lastResponse['result']['deposition'] );

			if (isset( $lastResponse['result']['folders'] ) ) {
				$this->Session->write( 'Depositions.folders', $lastResponse['result']['folders'] );
			}

			if (!isset( $result['needToLinkDepo'] )) {
				// login directly to deposition
				$result['redirect'] = '/i';
			} else {
				$result['deposition'] = $lastResponse['result']['deposition'];
				// get the number of Cases for the user to decide if Browse will be an available option
//				$foundCases = EDCase::getFilteredCases( ['conditions' => ['onlyScheduled' => false]], EDCase::STATUS_NOTSTARTED, true );
				$foundCases = EDCase::getAllCases();

				// Do we need to eliminate any demo cases?
				if( $result['deposition']['class'] == 'Deposition' ){
					foreach( $foundCases as $key=>$case ){
						if( $case['class'] == 'Demo' ){
							unset($foundCases[$key]);
						}
					}
				}

				$this->Session->write( 'Depositions.needToLinkDepo', $result['deposition']['class'] );
				$result['casesLength'] = count( $foundCases );
			}
		}

		$authUser = $this->Session->read( 'Auth.User' );
		$this->set( 'user', $authUser );
		$sortPreferences = (isset( $authUser['sortPreferences'] )) ? $authUser['sortPreferences'] : [];
		$userPreferences = (isset( $authUser['userPreferences'] )) ? $authUser['userPreferences'] : [];
		$this->Session->write( 'User.sortPreferences', $sortPreferences );
		$this->Session->write( 'User.userPreferences', $userPreferences );
		$this->log( 'Login successful. userID:' . $authUser['ID'] . ' username: ' . $authUser['username'] . ' name:' . $authUser['firstName'] . ' ' . $authUser['lastName'], 'webapp' );
		$this->set( 'lastResponse', json_encode( $result ) );
	}

	public function guests()
	{
		if( !$this->request->is( 'post' ) ) {
			$this->Session->destroy();
			$this->layout = 'userslogin';
			$this->set( 'brand', $this->Branding->brand );
			return;
		}

		$this->layout = 'ajax';
		$this->view = 'ajax_login';

		//Remove any sKey that is set.
		$this->Session->delete( 'EDSource.sKey' );
		if (!$this->Auth->login())
		{
			$error = EDUser::$lastResponse['error'];
			$message = $error['description'];
			if ($error['code'] === 1001 || $error['code'] === 0) {
				$message = 'The Deposition ID or passcode could not be verified';
			}
			$this->set( 'lastResponse', json_encode( ['success' => false, 'code' => $error['code'], 'error' => $message,] ) );
			return;
		}

		// get the API response for $this->Auth->login()
		$lastResponse = EDUser::$lastResponse;

		$result = ['success' => $lastResponse['success']];

		if (isset( $lastResponse['result']['sKey'] ) && $lastResponse['result']['sKey'])
		{
			$this->Session->write( 'EDSource.sKey', $lastResponse['result']['sKey'] );
		}

		if( isset( $lastResponse['result']['tutorialVideos'] ) ) {
			$this->Session->write( 'User.tutorialVideos', $lastResponse['result']['tutorialVideos'] );
		}

		if (isset( $lastResponse['result']['deposition'] ))
		{
			// login directly to deposition
			$this->Session->write( 'Depositions.ID', $lastResponse['result']['depositionID'] );
			$this->Session->write( 'Depositions.deposition', $lastResponse['result']['deposition'] );
			if (isset( $lastResponse['result']['folders'] ) ) {
				$this->Session->write( 'Depositions.folders', $lastResponse['result']['folders'] );
			}
			$result['redirect'] = '/i';
		}

		$authUser = $this->Session->read( 'Auth.User' );
		$sortPreferences = $authUser['sortPreferences'];
		if (!isset( $sortPreferences )) {
			$sortPreferences = [];
		}
		$this->Session->write( 'User.sortPreferences', $sortPreferences );
		$this->log( 'Login successful. userID: ' . $authUser['ID'] . ' name:' . $authUser['firstName'] . ' ' . $authUser['lastName'], 'webapp' );
		$this->set( 'lastResponse', json_encode( $result ) );
	}

	public function witness()
	{
		if( !$this->request->is( 'post' ) ) {
			$this->Session->destroy();
			$sessionID = (int)$this->request->query( 'SessionID' ) ? (int)$this->request->query( 'SessionID' ) : '';
			$this->layout = 'userslogin';
			$this->set( 'brand', $this->Branding->brand );
			$siteConfig = Configure::read( 'SiteConfig' );
			$this->set( 'nodeJS', $siteConfig['nodeJS'] );
			$this->set( 'sessionID', $sessionID );
			return;
		}

		$this->layout = 'ajax';
		$this->view = 'ajax_login';

		//Remove any sKey that is set.
		$this->Session->delete( 'EDSource.sKey' );
		$this->Session->delete( 'Auth.User' );
		$this->Session->id();	//start session
		if( !$this->Auth->login() ) {
			$error = EDUser::$lastResponse['error'];
			$message = $error['description'];
			if (($error['code'] === 1001 || $error['code'] === 0) && trim($message) == '') {
				$message = 'Invalid Session ID';
			}
			$this->set( 'lastResponse', json_encode( ['success' => false, 'code' => $error['code'], 'error' => $message] ) );
			return;
		}

		// get the API response for $this->Auth->login()
		$lastResponse = EDUser::$lastResponse;

		$result = ['success' => $lastResponse['success']];

		if( isset( $lastResponse['result']['sKey'] ) && $lastResponse['result']['sKey'] ) {
			$this->Session->write( 'EDSource.sKey', $lastResponse['result']['sKey'] );
			$result['sKey'] = $lastResponse['result']['sKey'];
			$result['witnessID'] = $lastResponse['result']['witnessID'];
			$result['depositionID'] = $lastResponse['result']['depositionID'];
			$result['userType'] = $lastResponse['result']['userType'];
			$this->Session->write( 'Depositions.ID', $result['depositionID'] );
		}

		if( isset( $lastResponse['result']['tutorialVideos'] ) ) {
			$this->Session->write( 'User.tutorialVideos', $lastResponse['result']['tutorialVideos'] );
		}

		$authUser = $this->Session->read( 'Auth.User' );
		$this->log( 'Login successful. userID: ' . $authUser['ID'] . ' name:' . $authUser['firstName'] . ' ' . $authUser['lastName'], 'webapp' );

		$this->set( 'lastResponse', json_encode( $result ) );
	}

	public function authorizeWitness()
	{
		$this->layout = 'ajax';
		$this->view = 'json';

		$authUser = $this->Session->read( 'Auth.User' );

		$depositionID = (int)$this->request->data['depositionID'];
		$witnessID = (int)$this->request->data['witnessID'];
		$authorize = ($this->request->data['authorize'] == 'true');

		$response = $this->EDDeposition->authorizeWitness( $depositionID, $witnessID, $authorize );
		$this->set( 'response', $response );
	}

	public function frame()
	{
		$this->view = 'frameset';

		// data for the chat manager
		$siteConfig = Configure::read( 'SiteConfig' );

		if( $this->Session->check( 'ipad_container_frame.src' ) ) {
			$frameSrc = $this->Session->read( 'ipad_container_frame.src' );
		} elseif( $this->Session->check( 'Depositions.introduceFileID' ) ) {
			$frameSrc = '/depositions/introduce';
		} elseif( $this->Session->check( 'Depositions.fileID' ) ) {
			$frameSrc = '/depositions/pdf';
		} elseif( $this->Session->check( 'Depositions.ID' ) ) {
			$frameSrc = '/depositions';
		} else {
			$frameSrc = '/cases';
		}

		// For going back to cases when backing out of browse view
		if( isset( $this->request->query['redirect'] ) && $this->request->query['redirect'] == '/cases' ) {
			$frameSrc = '/cases';
		}

		$this->log( "Users::frame; frameSrc: {$frameSrc}", 'webapp' );

		$authUser = $this->Session->read( 'Auth.User' );

		$this->set( 'sKey', $this->Session->read( 'EDSource.sKey' ) );
		$this->set( 'userID', $authUser['ID'] );
		$this->set( 'userType', $authUser['typeID'] );
		$this->set( 'sortPreferences', $this->Session->read( 'User.sortPreferences' ) );
		$this->set( 'userPreferences', $this->Session->read( 'User.userPreferences' ) );
		$this->set( 'nodeJS', $siteConfig['nodeJS'] );
		$this->set( 'frameSrc', $frameSrc );
//		$this->set( 'showTutorial', $this->Session->check( 'Guest.tutorialURL' ) || $this->Session->check( 'Member.tutorialOwnerURL' ) || $this->Session->check( 'Member.tutorialMemberURL' ) );
		$this->set( 'brand', $this->Branding->brand );
		unset( $authUser['sortPreferences'], $authUser['userPreferences'] );
		$authUser['userType'] = (in_array( $authUser['typeID'], ['C','CU','RPT','JDG','OFF'] )) ? 'M' : $authUser['typeID'];
		$this->set( 'authUser', $authUser );
	}

	public function reset()
	{
		if ($this->request->is( 'post' ))
		{
			$this->layout = 'ajax';
			$this->view = 'ajax_login';

			$EDUser = ClassRegistry::init( 'EDUser' );
			$EDUser->passwordReset( $this->request->data( 'EDUser.username' ) );
			$lastResponse = EDUser::$lastResponse;

			$result = ['success' => $lastResponse['success']];

			if (isset( $lastResponse['error'] ))
			{
				$result['error'] = isset( $lastResponse['error']['description'] ) ? $lastResponse['error']['description'] : 'Unknown error';
			} else {
				$resetMessage = 'Password change confirmation has been sent to the account owner.';
				$result['tooltip'] = $resetMessage;
				$result['redirect'] = '/members';
			}

			$this->set( 'lastResponse', json_encode( $result ) );
		} else {
			$this->set( 'brand', $this->Branding->brand );
		}
	}

	public function login()
	{
		if ($this->request->is( 'post' )) {
			$this->members();
		} else {
//			throw new Exception( 'login() has been deprecated, please update references to members()' );
			$this->log( 'Users::login; deprecated, redirect to signin', 'webapp' );
			$this->redirect( '/signin' );
		}
	}

	public function logout()
	{

		$authUser = $this->Session->read( 'Auth.User' );

		$error_client_number = 'ERR#'.rand(10000,99999);


		$ul = $this->request->query( 'ul' );
		if ($ul && strlen( $ul ) > 0) {
			if ($ul === '1') {
				$this->log( 'Logout because user logged out. userID:' . $authUser['ID'] . ' username: ' . $authUser['username'] . ' name:' . $authUser['firstName'] . ' ' . $authUser['lastName'], 'webapp' );
			} else if ($ul === '2') {

				$this->Cookie->write('last_error', $error_client_number);
				$this->log( '['.$error_client_number.'] '.'Logout because of error trying to link depositions. userID:' . $authUser['ID'] . ' username: ' . $authUser['username'] . ' name:' . $authUser['firstName'] . ' ' . $authUser['lastName'], 'webapp' );
			} else if ($ul === '3') {

				$this->Cookie->write('last_error', $error_client_number);
				$this->log( '['.$error_client_number.'] '.'Logout because of attendee kick. userID:' . $authUser['ID'] . ' username: ' . $authUser['username'] . ' name:' . $authUser['firstName'] . ' ' . $authUser['lastName'], 'webapp' );
			} else if ($ul === '4') {

				$this->Cookie->write('last_error', $error_client_number);
				$this->log( '['.$error_client_number.'] '.'Logout because demo deposition ended. userID:' . $authUser['ID'] . ' username: ' . $authUser['username'] . ' name:' . $authUser['firstName'] . ' ' . $authUser['lastName'], 'webapp' );
			} else if ($ul === '5') {

				$this->Cookie->write('last_error', $error_client_number);
				$this->log( '['.$error_client_number.'] '.'Logout because DepositionsController::updateDeposition() received a null response. userID:' . $authUser['ID'] . ' username: ' . $authUser['username'] . ' name:' . $authUser['firstName'] . ' ' . $authUser['lastName'], 'webapp' );
			} else if ($ul === '6') {

				$this->Cookie->write('last_error', $error_client_number);
				$this->log( '['.$error_client_number.'] '.'Logout because DepositionsController::loadDatacore() failed with EDDeposition->attendByDepositionID(). userID:' . $authUser['ID'] . ' username: ' . $authUser['username'] . ' name:' . $authUser['firstName'] . ' ' . $authUser['lastName'], 'webapp' );
			} else if ($ul === '7') {

				$this->Cookie->write('last_error', $error_client_number);
				$this->log( '['.$error_client_number.'] '.'Logout because DepositionsController::loadDatacore() failed with this->getDepositionFromLogin(). userID:' . $authUser['ID'] . ' username: ' . $authUser['username'] . ' name:' . $authUser['firstName'] . ' ' . $authUser['lastName'], 'webapp' );
			} else if ($ul === '8') {

				$this->Cookie->write('last_error', $error_client_number);
				$this->log( '['.$error_client_number.'] '.'Logout because DepositionsController::beforeFilter() failed to find session value Depositions.ID. userID:' . $authUser['ID'] . ' username: ' . $authUser['username'] . ' name:' . $authUser['firstName'] . ' ' . $authUser['lastName'], 'webapp' );
			}

		}


		$url = $this->Auth->logout();
		$redirect = $this->request->query( 'redirect' );
		if ($redirect && strlen( $redirect ) > 0) {
			$url = $redirect;
		}
		$this->Session->destroy();


		$this->redirect( $url );
	}

	public function applyBranding()
	{
		$this->Branding->setBrand();
		$this->redirect( '/i' );
	}

	public function setSortPreference()
	{
		$this->layout = 'ajax';
		$this->view = 'json';

		$EDUser = ClassRegistry::init( 'EDUser' );
		$response = $EDUser->setSortPreference( $this->request->query( 'sortObject' ), $this->request->query( 'sortBy' ), $this->request->query( 'sortOrder' ) );
		$lastResponse = EDUser::$lastResponse;

		if( !isset( $lastResponse['error'] ) ) {
			// Update the saved user settings.
			$sortPreference = $response['EDUser']['sortPreferences'];
			$this->Session->write( 'User.sortPreferences', $sortPreference );
		}

		$this->set( 'response', $response );
	}

	public function setUserPreference()
	{
		$this->layout = 'ajax';
		$this->view = 'json';

		$EDUser = ClassRegistry::init( 'EDUser' );
		$response = $EDUser->setUserPreference( $this->request->query( 'prefKey' ), $this->request->query( 'value' ) );
		$lastResponse = EDUser::$lastResponse;

		if( !isset( $lastResponse['error'] ) ) {
			// Update the saved user settings.
			$userPreferences = $response['EDUser']['userPreferences'];
			$this->Session->write( 'User.userPreferences', $userPreferences );
		}

		$this->set( 'response', $response );
	}

	public function setCustomSort()
	{
		$this->layout = 'ajax';
		$this->view = 'json';

		$EDUser = ClassRegistry::init( 'EDUser' );
		$response = $EDUser->setCustomSort( $this->request->data( 'sortObject' ), $this->request->data( 'list' ), $this->request->data( 'sessionID' ) );
		$lastResponse = EDUser::$lastResponse;

		if( isset( $lastResponse['error'] ) ) {
			$result['error'] = isset( $lastResponse['error']['description'] ) ? $lastResponse['error']['description'] : 'Unknown error';
		}

		$this->set( 'response', $response );
	}
}
