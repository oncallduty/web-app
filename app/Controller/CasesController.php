<?php

App::uses( 'EDCase', 'Model' );
App::uses( 'EDDeposition', 'Model' );
App::uses( 'BrandingComponent', 'Controller/Component' );

class CasesController extends AppController
{
	public $helpers = array( 'Html', 'Form', 'Js' );
	public $components = array(
		'Branding'
	);

	public function __construct( $request=NULL, $response=NULL) {
		parent::__construct( $request, $response );
		$this->layout = 'cases';
		$this->view = 'index';
		$this->set( 'title_for_layout', 'Cases' );
	}

	public function beforeFilter()
	{
		parent::beforeFilter();

		$authUser = $this->Session->read( 'Auth.User' );
		$this->set( 'name', "{$authUser['firstName']} {$authUser['lastName']}" );
	}

	public function wildIndex()
	{
		$this->redirect( '/cases/index' );
	}

	public function index()
	{
		if ($this->request->params['pass']) {
			parent::index();

			return;
		}

		$this->scheduled();
	}

	public function scheduled()
	{
		$this->Session->delete( 'Depositions.fileID' );
		$this->Session->delete( 'Depositions.ID' );
		$this->Session->delete( 'Depositions.deposition' );
		$this->Session->write( 'ipad_container_frame.src', '/cases' );

//		$casesList = EDCase::getFilteredCases( [], [EDCase::STATUS_NOTSTARTED, EDCase::STATUS_INPROGRESS] );
		$casesList = EDCase::getAllCases();

		// empty cases list may simply be empty, or may indicate error interacting with API.
		if (empty( $casesList ))
		{
			$response = json_decode( EDSource::$lastResponse );
			if (isset( $response->success ) && $response->success === false)
			{
				if (isset( $response->error->code ))
				{
					$this->Session->setFlash( $this->prepareErrorMessage( $response->error->code ), 'default', [], 'error' );
				}
				$this->redirect( ['controller' => 'depositions', 'action' => 'index'] );
				return;
			}
		}

		$this->set( 'api_data_class', $casesList );
		$this->set( 'case_status', 'new' );
	}

	public function past()
	{
		$this->scheduled();
		$this->set( 'case_status', 'past' );
	}

	public function deposition() {
		$depositionID = $this->request->query( 'ID' );
		$this->Session->write( 'Depositions.ID', $depositionID );
		$this->redirect( ['controller' => 'depositions', 'action' => 'index'] );
	}

	public function attendDeposition() {
		$depositionID = $this->request->query( 'ID' );
		$this->Session->write( 'Depositions.ID', $depositionID );
		$this->redirect( ['controller' => 'depositions', 'action' => 'attendDeposition'] );
	}

	public function browse()
	{
		if( !$this->Session->check( 'Depositions.ID' ) ) {
			$this->scheduled();
			return;
		}

		$caseID = $this->request->query( 'cID' );
		$depositionID = $this->request->query( 'dID' );
//		$onlyScheduled = $this->Session->read( 'Depositions.needToLinkDepo' ) ? FALSE : TRUE;

		// if both caseID and depositionID are undefined, display the browse form
		if( $caseID === null && $depositionID === null ) {
			$this->layout = 'users';
			$this->view = 'browse';

//			$foundCases = EDCase::getFilteredCases( ['conditions' => ['onlyScheduled' => $onlyScheduled]], EDCase::STATUS_NOTSTARTED, true );
			$foundCases = EDCase::getAllCases();

			$this->set( 'api_data_class', $foundCases );
			$this->set( 'brand', $this->Branding->brand );
			$this->set( 'depositionClass', $this->Session->read( 'Depositions.deposition.class' ) );
			$this->set( 'isJoin', (isset( $_REQUEST['join'] ) && $_REQUEST['join'] == 1) );
			return;
		}

		$this->layout = 'ajax';
		$this->view = 'browse_ajax';

		$result = [];

		// if either caseID or depositionID are undefined (but not BOTH), suppress potential error, clear the query and display the browse form
		if ($caseID === null || $depositionID === null) {
			$result['success'] = true;
			$result['redirect'] = '/cases/browse';
		} else {
			// send the link request to the api and return the results
			$args = ['sourceDepositionID'=>$this->Session->read( 'Depositions.ID' )];
			if ($caseID !== 0) {
				$args['targetCaseID'] = $caseID;
				if ($depositionID !== 0) {
					$args['targetDepositionID'] = $depositionID;
				}
			}

			$EDDeposition = ClassRegistry::init( 'EDDeposition' );
			$EDDeposition->setLink( $args, TRUE );
			$lastResponse = $EDDeposition::$lastResponse;

			$result['success'] = $lastResponse['success'];
			if (!$lastResponse['success']) {
				$result['error'] = ($lastResponse['error']['code'] > 1000 ? $lastResponse['error']['description'] : 'Invalid selection, please try again.');
			} else {
				$this->Session->write( 'Depositions.ID', $lastResponse['result']['targetDeposition']['ID'] );
				$this->Session->write( 'Depositions.deposition', $lastResponse['result']['targetDeposition'] );
				$parentID = (int)(($lastResponse['result']['targetDeposition']['parentID']) ? $lastResponse['result']['targetDeposition']['parentID'] : $lastResponse['result']['targetDeposition']['ID']);
				$this->Session->write( 'Depositions.attending', $parentID );

				if (isset( $lastResponse['result']['folders'] ) ) {
					$this->Session->write( 'Depositions.folders', $lastResponse['result']['targetDeposition']['folders'] );
				}

				$this->Session->write( 'ipad_container_frame.src', '/depositions' );
				$result['redirect'] = '/i';
			}
		}

		$this->set( 'response', json_encode( $result ) );
	}

	protected function prepareErrorMessage( $code )
	{
		switch ($code)
		{
			case 401:
				$msg = 'Unable to authorize current user. Please login again.';
				break;
			default:
				$msg = '';
		}
		return $msg;
	}
}