<?php

App::uses( 'EDDeposition', 'Model' );
App::uses( 'EDCase', 'Model' );
App::uses( 'EDFolder', 'Model' );
App::uses( 'EDFile', 'Model' );
App::uses( 'EDSearch', 'Model' );
App::uses( 'BrandingComponent', 'Controller/Component' );

class DepositionsController extends AppController
{
	public $helpers = ['Html', 'Form', 'Js'];
	private $depositionID;
	public $components = ['Branding','Cookie'];
	protected static $nonDemoSessions = ['Depositions','WitnessPrep','Trial','Arbitration','Hearing','Mediation'];

	public function __construct( $request=NULL, $response=NULL)
	{
		parent::__construct( $request, $response );
		$this->layout = 'depositions';
		$this->set( 'title_for_layout', 'Depositions' );
		$this->EDDeposition = ClassRegistry::init( 'EDDeposition' );
		$this->EDFolder = ClassRegistry::init( 'EDFolder' );
		$this->EDFile = ClassRegistry::init( 'EDFile' );
		$this->EDSearch = ClassRegistry::init( 'EDSearch' );
		ClassRegistry::init( 'ConnectionManager' );
	}

	// remove this after testing
	public function beforeFilter()
	{
		parent::beforeFilter();

		// Depositions.ID MUST be set before accessing the Depositions controller
		$this->depositionID = $this->Session->read( 'Depositions.ID' );
		if( !$this->depositionID && $this->request->url !== 'depositions/join' ) {
			$authUser = $this->Session->read( 'Auth.User' );
			$this->log( 'Logout because no depositionID in session. userID:' . $authUser['ID'] . ' username: ' . $authUser['username'] . ' name:' . $authUser['firstName'] . ' ' . $authUser['lastName'], 'webapp' );
			$this->redirect( '/users/logout?ul=8' );
		}

		$authUser = $this->Session->read( 'Auth.User' );
		$this->set( 'name', "{$authUser['firstName']} {$authUser['lastName']}" );
	}

	public function index()
	{

		if (isset( $this->request->params['pass'] ) && !empty( $this->request->params['pass'] ))
		{
			parent::index();
			return;
		}

		$this->view = 'index';

		$this->Session->delete( 'Depositions.fileID' );
		$this->Session->delete( 'Depositions.introduceFileID' );
		$this->Session->delete( 'Depositions.introducefromView' );
		$this->Session->delete( 'Depositions.rootDepositionID' );
		$this->Session->write( 'ipad_container_frame.src', '/depositions' );

		$authUser = $this->Session->read( 'Auth.User' );

		$Deposition = $this->loadDatacore();

		$this->Session->write( 'Depositions.ownerID',  intval( $Deposition->data['EDDeposition']['ownerID'] ));

		$this->set( 'brand', $this->Branding->brand );
		$this->set( 'userType', $authUser['typeID'] );
	}

	public function pdf() {
		$fileID = $this->request->query( 'ID' );
		if( !$fileID ) {
			if( !$this->Session->check( 'Depositions.fileID', $fileID ) ) {
				$this->redirect( '/depositions' );
			} else {
				// detected page refresh, reload datacore
				$fileID = $this->Session->read( 'Depositions.fileID' );
			}
		}

		$this->loadDatacore();

		$this->view = 'pdf';

		$this->Session->write( 'Depositions.fileID', $fileID );
		$this->Session->write( 'ipad_container_frame.src', "/depositions/pdf" );

		$this->Session->delete( 'Depositions.introduceFileID' );
		$this->Session->delete( 'Depositions.introducefromView' );

		$authUser = $this->Session->read( 'Auth.User' );

		$this->set( 'fileID', $fileID );
//		$this->set( 'fileURL', $this->EDFile->getFileLink( ['fileID'=>$fileID, 'embed'=>TRUE] ) );	//No! Bad dog!
		$this->set( 'brand', $this->Branding->brand );
		$this->set( 'userType', $authUser['typeID'] );

		if ($this->Session->check( 'Depositions.tempFiles' )) {
			$tempFiles = $this->Session->read( 'Depositions.tempFiles' );
		} else {
			$tempFiles = array();
		}
		$this->set( 'tempFiles', json_encode( $tempFiles ) );

		$siteCfg = Configure::read( 'SiteConfig' );
		$this->set( 'pdfViewerURL', "{$siteCfg['PDFViewer']}/" );
	}

	public function presentation()
	{
		$this->layout = 'presentation';
		$this->view = 'presentation';

		$this->Session->write( 'ipad_container_frame.src', '/depositions/presentation' );

		$this->loadDatacore();

		$authUser = $this->Session->read( 'Auth.User' );
		$userType = (in_array( $authUser['typeID'], ['C','CU','RPT','JDG','OFF'] )) ? 'M' : $authUser['typeID'];

		$this->set( 'brand', $this->Branding->brand );
		$this->set( 'userType', $userType );

		$siteCfg = Configure::read( 'SiteConfig' );
		$this->set( 'pdfViewerURL', "{$siteCfg['PDFViewer']}/" );
	}

	public function multimedia( $type='video' )
	{
		$fileID = $this->request->query( 'ID' );
		$beginIntroduce = $this->request->query( 'beginIntroduce' );

		if ($fileID === null){
			if (!$this->Session->check( 'Depositions.fileID', $fileID )){
				$this->redirect( '/depositions' );
			} else{
				// detected page refresh, reload datacore
				$fileID = $this->Session->read( 'Depositions.fileID', $fileID );
				$this->loadDatacore();
			}
		}
		$this->loadDatacore();

		$this->Session->write( 'Depositions.fileID', $fileID );
		$this->Session->delete( 'Depositions.introduceFileID' );
		$this->Session->delete( 'Depositions.introducefromView' );
		$this->Session->write( 'ipad_container_frame.src', "/depositions/{$type}" );

		$authUser = $this->Session->read( 'Auth.User' );

		if ($this->Session->check( 'Depositions.tempFiles' )) {
			$tempFiles = $this->Session->read( 'Depositions.tempFiles' );
		} else {
			$tempFiles = array();
		}

		$this->view = 'multimedia';
		$this->set( 'fileType', $type );
		$this->set( 'beginIntroduce', $beginIntroduce );
		$this->set( 'fileID', $fileID );
//		$this->set( 'fileName', $fileName );
		$this->set( 'brand', $this->Branding->brand );
		$this->set( 'userType', $authUser['typeID'] );
		$this->set( 'tempFiles', json_encode( $tempFiles ) );
		$this->set( 'fileURL', $this->EDFile->getFileLink( ['fileID'=>$fileID, 'embed'=>TRUE] ) );
	}

	public function audio()
	{
		$this->multimedia('audio');
	}

	public function video()
	{
		$this->multimedia('video');
	}

	public function pdfviewer()
	{
		$this->layout = 'pdfviewer';
		$this->view = 'pdfviewer';

		$this->set( 'fileID', $this->request->query( 'ID' ) );
	}

	public function folders()
	{
		$this->layout = 'ajax';
		$this->view = 'json';

		$depositionID = $this->request->query( 'depositionID' );
		$EDFolders = $this->EDFolder->getFolders( $depositionID );

		$folders = [];
		if ($EDFolders) {
			foreach ($EDFolders as $EDFolder) {
				$folders[] = $EDFolder->data['EDFolder'];
			}
		}

		$this->set( 'response', (object)[
			'status' => 'success',
			'folders' => $folders,
		] );
	}

	public function sendFolder()
	{
		$this->layout = 'ajax';
		$this->view = 'json';

		// TODO - define condition for response if request is not POST

		if ($this->request->isPost())
		{
			$params = [
				'folderID' => $this->request->data( 'folderID' ),
				'selectedFileIDs' => $this->request->data( 'selectedFileIDs' ),
				'sendTo' => $this->request->data( 'sendTo' ),
				'sendCc' => $this->request->data( 'sendCc' ),
				'sendBcc' => $this->request->data( 'sendBcc' ),
				'sendSubject' => $this->request->data( 'sendSubject' ),
				'sendContents' => $this->request->data( 'sendContents' ),
			];

			$response = $this->EDFile->sendFolder( $params );
		}

		$this->set( 'response', $response );
	}

	public function files()
	{
		$this->layout = 'ajax';
		$this->view = 'json';

		$folderID = $this->request->query( 'folderID' );
		$EDFiles = $this->EDFile->getFiles( ['folderID' => $folderID, 'sortBy' => 'created'] );

		$files = [];
		if ($EDFiles) {
			foreach ($EDFiles as $EDFile) {
				$files[] = $EDFile->data['EDFile'];
			}
		}

		$this->set( 'response', (object)[
			'status' => 'success',
			'folderID' => $folderID,
			'files' => $files,
		] );
	}

	public function saveFolder()
	{
		$folderID = $this->request->query( 'folderID' );
		$folderName = $this->request->query( 'folderName' );

		header( 'Content-type: application/zip' );
		header( 'Content-disposition: attachment; filename='.($folderName && strlen( $folderName ) > 0 ? $folderName : 'tmp').'.zip' );
		$this->EDFile->saveFolder( $this->request->query( 'folderID' ) );
		exit;
	}

	public function sendFile()
	{
		$this->layout = 'ajax';
		$this->view = 'json';

		// TODO - define condition for response if request is not POST

		if ($this->request->isPost())
		{
			$params = [
				'fileID' => $this->request->data( 'fileID' ),
				'sendTo' => $this->request->data( 'sendTo' ),
				'sendCc' => $this->request->data( 'sendCc' ),
				'sendBcc' => $this->request->data( 'sendBcc' ),
				'sendSubject' => $this->request->data( 'sendSubject' ),
				'sendContents' => $this->request->data( 'sendContents' ),
			];

			$response = $this->EDFile->sendFile( $params );
			$lastResponse = EDDeposition::$lastResponse;
			if (isset ($lastResponse['success'] ) && !$lastResponse['success'] && isset( $lastResponse['error'] ))
			{
				$response = [
					'error' => isset( $lastResponse['error']['code'] ) ? $lastResponse['error']['code'] : -1,
					'title' => 'Could not email document',
					'message' => isset( $lastResponse['error']['description'] ) ? $lastResponse['error']['description'] : 'An unknown error occurred',
				];
			}
		}
		$this->set( 'response', $response );
	}

	public function sendFileWithAnnotations()
	{
		$this->layout = 'ajax';
		$this->view = 'json';

		if ($this->request->isPost())
		{
			$params = [
				'fileID' => $this->request->data( 'fileID' ),
				'sendTo' => $this->request->data( 'sendTo' ),
				'sendCc' => $this->request->data( 'sendCc' ),
				'sendBcc' => $this->request->data( 'sendBcc' ),
				'sendSubject' => $this->request->data( 'sendSubject' ),
				'sendContents' => $this->request->data( 'sendContents' ),
				'depositionID' => $this->depositionID,
				'sourceFileID' => $this->request->data( 'sourceFileID' ),
				'annotations' => $this->request->data( 'annotations' ),
			];

			$response = $this->EDFile->sendFileWithAnnotations( $params );
			$lastResponse = EDDeposition::$lastResponse;
			if (isset ($lastResponse['success'] ) && !$lastResponse['success'] && isset( $lastResponse['error'] ))
			{
				$response = [
					'error' => isset( $lastResponse['error']['code'] ) ? $lastResponse['error']['code'] : -1,
					'title' => 'Could not email document',
					'message' => isset( $lastResponse['error']['description'] ) ? $lastResponse['error']['description'] : 'An unknown error occurred',
				];
			}
		}

		$this->set( 'response', $response );
	}

	// This is used by the pdf.js code to download the pdf as well as the download button.
	// As of ED-1122 also used to download multimedia files.
	public function file() {
		// Test to see if we are viewing a presentation, if so we get the file a different way.
		if ($this->request->query( 'presentationClient' )) {
			$rootDepositionID = $this->getRootDepositionID();
			$results = $this->EDFile->getPresentationSource( $rootDepositionID );
		} else {
			$results = $this->EDFile->getFile( $this->request->query( 'fileID' ) );
		}
		header( 'Content-Type: ' . $results['headers']['Content-Type'] );
		header( 'Content-Length: ' . $results['headers']['Content-Length'] );
		header( 'Content-Disposition: '. $results['headers']['Content-Disposition'] );
		echo $results['results'];

		exit;
	}

	public function sendPortalInvite()
	{
		$this->layout = 'ajax';
		$this->view = 'json';

		if ($this->request->isPost())
		{
			$sessionID = (int)$this->request->data( 'sessionID' );
			$emailAddress = $this->request->data( 'emailAddress' );

			$response = $this->EDDeposition->sendPortalInvite( $sessionID, $emailAddress );
		}

		$this->set( 'response', $response );
	}

	public function presentationSource()
	{
		$rootDepositionID = $this->getRootDepositionID();
		$results = $this->EDFile->getPresentationSource( $rootDepositionID );
		header( 'Content-Type: ' . $results['headers']['Content-Type'] );
		header( 'Content-Length: ' . $results['headers']['Content-Length'] );
		header( 'Content-Disposition: '. $results['headers']['Content-Disposition'] );
		echo $results['results'];
		exit;
	}

	public function setSpeaker()
	{
		$this->layout = 'ajax';
		$this->view = 'json';

		$userID = $this->request->query( 'userID' );

		$response = $this->EDDeposition->setSpeaker( $this->getRootDepositionID(), $userID );
		$this->set( 'response', $response );
	}

	public function attendDeposition()
	{
		$this->layout = 'ajax';
		$this->view = 'json';

		$response = $this->EDDeposition->attendByDepositionID( $this->depositionID );
		//Ezequiel: bugfix? we will never receive success from this call, so we should use:
		//if( $response ) { instead of if( $response->success ) {
		//For now added to the functions called to have the ->success parameter as true if has data

		if( $response->success ) {
			$this->Session->write( 'Depositions.deposition', $response );
			$parentID = (int)(($response->data['EDDeposition']['parentID']) ? $response->data['EDDeposition']['parentID'] : $response->data['EDDeposition']['ID']);
			$this->Session->write( 'Depositions.attending', $parentID );
		}else{
			//$this->log( 'DepositionsController.php (396) - attendDeposition ERROR: '.print_r($response,true));
		}

		$this->set( 'response', $response );
	}

	public function startDeposition()
	{
		$this->layout = 'ajax';
		$this->view = 'json';

		$authUser = $this->Session->read( 'Auth.User' );
		$ownerID = $this->Session->read( 'Depositions.ownerID' );
		// Double check to see that we are the deposition owner.
		$response = (intval($authUser['ID']) === $ownerID) ? $this->EDDeposition->startDeposition( $this->depositionID ) : null;

		if (!$response)
		{
			$lastResponse = EDDeposition::$lastResponse;
			if (isset ($lastResponse['success'] ) && !$lastResponse['success'] && isset( $lastResponse['error'] ))
			{
				$response = [
					'error' => isset( $lastResponse['error']['code'] ) ? $lastResponse['error']['code'] : -1,
					'title' => 'Could not start Deposition',
					'message' => isset( $lastResponse['error']['description'] ) ? $lastResponse['error']['description'] : 'An unknown error occurred',
				];

				if (isset( $lastResponse['error']['code'] ) && $lastResponse['error']['code'] == 1101)
				{
					$errResponse = $lastResponse['error'];
					$activeDepositionID = isset( $errResponse['data']['activeDeposition']['ID'] ) ? $errResponse['data']['activeDeposition']['ID'] : 0;
					$depositionOf = isset( $errResponse['data']['activeDeposition']['depositionOf'] ) ? 'Deposition of ' . $errResponse['data']['activeDeposition']['depositionOf'] : 'Deposition ID';
					$case = isset( $errResponse['data']['activeDeposition']['name'] ) ? 'in Case ' . $errResponse['data']['activeDeposition']['name'] : '';
					$response['title'] = 'Only one active session is allowed';
					$response['message'] = ($activeDepositionID > 0) ? "$depositionOf (#$activeDepositionID) $case is currently active and must be finished before you can<br />start another." : 'Another Deposition is currently active.<br />You may not begin this deposition.';
				}
			}
		}
		$this->set( 'response', $response );
	}

	public function finishDeposition()
	{
		$this->layout = 'ajax';
		$this->view = 'json';

		$authUser = $this->Session->read( 'Auth.User' );

		// We send an empty string so we don't change the court reporter email.
		$response = $this->EDDeposition->finishDeposition( $this->depositionID, '' );
		$this->set( 'response', $response );
	}

	public function join()
	{
		$this->layout = 'ajax';
		$this->view = 'json';

		$authUser = $this->Session->read( 'Auth.User' );

		$this->EDDeposition->join( $this->request->data['EDUser']['depositionID'], $this->request->data['EDUser']['depositionPasscode'] );
		$response = EDDeposition::$lastResponse;

		if (isset($response['error'])) {
			if ($response['error']['code'] === 1001 || $response['error']['code'] === 1004) {
				$response['error']['description'] = 'The Session ID or passcode could not be verified';
			} else if ($response['error']['code'] === 1002) {
				$response['error']['description'] = 'Cannot join, Session not started yet';
			} else if ($response['error']['code'] === 1003) {
				$response['error']['description'] = 'Cannot join, Session is finished';
			} else if ($response['error']['code'] === 201) {
				$response['error']['description'] = 'Users must specify Session ID and Passcode';
			}
			$this->set( 'response', $response );
			return;
		}

		if( !isset( $response['result']['deposition'] ) ) {
			if (!isset( $response['result']['needToLinkDepo'] ) || !$response['result']['needToLinkDepo'] ) {
				$response['result']['redirect'] = '/i';
			}
		} else {
			$this->Session->write( 'Depositions.ID', $response['result']['deposition']['ID'] );
			$this->Session->write( 'Depositions.deposition', $response['result']['deposition'] );

			if( isset( $response['result']['folders'] ) ) {
				$this->Session->write( 'Depositions.folders', $response['result']['folders'] );
			}

			if( !isset( $response['result']['needToLinkDepo'] ) || !$response['result']['needToLinkDepo'] ) {
				// login directly into deposition
				$this->Session->write( 'ipad_container_frame.src', '/depositions' );
				$response['result']['redirect'] = '/i';
			} else {
				// get the number of Cases for the user to decide if Browse will be an available option
//				$foundCases = EDCase::getFilteredCases( ['conditions' => ['onlyScheduled' => false]], EDCase::STATUS_NOTSTARTED, true );
				$foundCases = EDCase::getAllCases();

				// Do we need to eliminate any cases?
				foreach( $foundCases as $key=>$case ) {
					if(
						( !in_array( $response['result']['deposition']['class'], $this->demoSessions ) && $case['class'] == 'Demo' ) ||
						( in_array( $response['result']['deposition']['class'], $this->demoSessions ) && $case['class'] == 'Case' )
					) {
						unset( $foundCases[$key] );
					}
				}

				$this->Session->write( 'Depositions.class', $response['result']['deposition']['class'] );
				$this->Session->write( 'Depositions.needToLinkDepo', $response['result']['needToLinkDepo'] );
				$response['result']['casesLength'] = count( $foundCases );
			}
		}
		$sortPreferences = (isset( $authUser['sortPreferences'] )) ? $authUser['sortPreferences'] : [];
		$userPreferences = (isset( $authUser['userPreferences'] )) ? $authUser['userPreferences'] : [];
		$this->Session->write( 'User.sortPreferences', $sortPreferences );
		$this->Session->write( 'User.userPreferences', $userPreferences );
		$this->set( 'response', $response );
	}

	public function kickAttendee()
	{
		$this->layout = 'ajax';
		$this->view = 'json';

		$userID = $this->request->query( 'userID' );
		$guestID = $this->request->query( 'guestID' );
		$ban = $this->request->query( 'ban' ) === "true" ? true : false;

		$response = $this->EDDeposition->kickAttendee( $this->depositionID, $userID, $guestID, $ban );
		$this->set( 'response', $response );
	}

	public function shareFile()
	{
		$this->layout = 'ajax';
		$this->view = 'json';

		$fileID = $this->request->query( 'id' );
		$userIDs = explode( ',', $this->request->query( 'userIDs' ) );
		$fileName = rawurldecode( $this->request->query( 'customName' ) );

		$response = $this->EDDeposition->shareFile( $this->depositionID, $userIDs, $fileID, $fileName );
		$this->set( 'response', $response );
	}

	public function shareFolder()
	{
		$this->layout = 'ajax';
		$this->view = 'json';

		$folderID = $this->request->query( 'id' );
		$userIDs = explode( ',', $this->request->query( 'userIDs' ) );
		$folderName = rawurldecode( $this->request->query( 'folderName' ) );

		$response = $this->EDDeposition->shareFolder( $this->depositionID, $userIDs, $folderID, $folderName );
		$this->set( 'response', $response );
	}

	public function abortTempFile()
	{
		$this->layout = 'ajax';
		$this->view = 'json';

		$fileID = $this->request->query( 'fileID' );

		$response = $this->EDFile->abortTempFile( $this->getRootDepositionID(), $fileID );
		$this->set( 'response', $response );
	}

	public function annotate()
	{
		$this->layout = 'ajax';
		$this->view = 'json';

		$params = [
			'depositionID' => $this->depositionID,
			'sourceFileID' => $this->request->query( 'sourceFileID' ),
			'annotations' => $this->request->data( 'annotations' ),
			'overwrite' => $this->request->query( 'overwrite' ) === "true" ? true : false,
			'fileName' => $this->request->query( 'fileName' ),
			'folderID' => $this->request->query( 'folderID' ),
			'isTempFile' => $this->request->query( 'isTempFile' ),
		];

		if ($params['overwrite']) {
			$params['fileID'] = $this->request->query( 'fileID' );
		}

		$response = $this->EDFile->annotate( $params );

		if ($this->request->query( 'isTempFile' ) === "true") {
			if ($response !== null && $response['fileID'] > 0) {
				if ($this->Session->check( 'Depositions.tempFiles' )) {
					$tempFiles = $this->Session->read( 'Depositions.tempFiles' );
				} else {
					$tempFiles = array();
				}
				$tempFiles[$response['fileID']] = $this->request->query( 'sourceFileID' );
				$this->Session->write( 'Depositions.tempFiles', $tempFiles );
			}
		}

		$this->set( 'response', $response );
	}

	public function introduce()
	{
		$fileID = $this->request->query( 'ID' );
		if ($fileID === null)
		{
			if (!$this->Session->check( 'Depositions.introduceFileID' ))
			{
				$this->redirect( '/depositions' );
			} else {
				// detected page refresh, reload datacore
				$fileID = $this->Session->read( 'Depositions.introduceFileID' );
				$this->loadDatacore();
			}
		}

		$fromView = $this->request->query( 'fromView' );
		if ($fromView === null) {
			if ($this->Session->check( 'Depositions.introducefromView' )) {
				$fromView = $this->Session->read( 'Depositions.introducefromView' );
			} else {
				$fromView = 0;
			}
		}

		$this->view = 'introduce';

		$this->Session->write( 'Depositions.introduceFileID', $fileID );
		$this->Session->write( 'Depositions.introducefromView', $fromView );
		$this->Session->write( 'ipad_container_frame.src', "/depositions/introduce" );

		$authUser = $this->Session->read( 'Auth.User' );

		$this->loadDatacore();

		$this->set( 'fileID', $fileID );
//		$this->set( 'fileURL', $this->EDFile->getFileLink( ['fileID' => $fileID,'embed' => true,] ) );
		$this->set( 'brand', $this->Branding->brand );
		$this->set( 'userType', $authUser['typeID'] );
		$this->set( 'fromView', $fromView );

		if ($this->Session->check( 'Depositions.tempFiles' )) {
			$tempFiles = $this->Session->read( 'Depositions.tempFiles' );
		} else {
			$tempFiles = array();
		}
		$this->set( 'tempFiles', json_encode( $tempFiles ) );
		$siteCfg = Configure::read( 'SiteConfig' );
		$this->set( 'pdfViewerURL', "{$siteCfg['PDFViewer']}/" );
	}

	public function introduceviewer()
	{
		$this->layout = 'introduceviewer';
		$this->view = 'introduceviewer';

		$this->set( 'fileID', $this->request->query( 'ID' ) );
	}

	public function introduceFile() {
		$this->layout = 'ajax';
		$this->view = 'json';

		if ($this->Session->check( 'Depositions.tempFiles' )) {
			$tempFiles = $this->Session->read( 'Depositions.tempFiles' );
		}
		$tempSourceID = 0;
		if (isset( $tempFiles )) {
			if (array_key_exists( $this->request->query( 'sourceFileID' ), $tempFiles )) {
				$tempSourceID = intval( $tempFiles[$this->request->query( 'sourceFileID' )] );
			}
		}
		$params = [
			'depositionID' => $this->getRootDepositionID(),
			'name' => rawurldecode( $this->request->query( 'filename' ) ),
			'fileID' => $this->request->query( 'fileID' ),
			'sourceFileID' => $this->request->query( 'sourceFileID' ),
			'exhibitXOrigin' => floatval( $this->request->query( 'exhibitXOrigin' ) ),
			'exhibitYOrigin' => floatval( $this->request->query( 'exhibitYOrigin' ) ),
			'stampWidth' => floatval( $this->request->query( 'stampWidth' ) ),
			'stampHeight' => floatval( $this->request->query( 'stampHeight' ) ),
			'orientation' => intval( $this->request->query( 'orientation' ) ),
			'exhibitTitle' => rawurldecode( $this->request->query( 'exhibitTitle' ) ),
			'exhibitSubTitle' => rawurldecode( $this->request->query( 'exhibitSubTitle' ) ),
			'exhibitDate' => rawurldecode($this->request->query( 'exhibitDate' ) ),
			'isExhibit' => true,
			'overwrite' => $this->request->query( 'overwrite' ) === "true" ? true : false,
			'tempSourceID' => $tempSourceID,
			'skipStamp' => $this->request->query( 'skipStamp' ) === "true" ? true : false,
			'isMultimedia' => $this->request->query( 'isMultimedia' ) === "true" ? true : false,
		];

		$response = $this->EDDeposition->introduceFile( $params );
		if ($response) {
			if ($tempSourceID > 0 && isset( $tempFiles )) {
				unset( $tempFiles[$this->request->query( 'sourceFileID' )] );
				$this->Session->write( 'Depositions.tempFiles', $tempFiles );
			}
		} else {
			$lastResponse = EDDeposition::$lastResponse;
			if (isset ($lastResponse['success'] ) && !$lastResponse['success'] && isset( $lastResponse['error'] ))
			{
				$response = [
					'error' => isset( $lastResponse['error']['code'] ) ? $lastResponse['error']['code'] : -1,
					'title' => 'Could not Introduce',
					'message' => isset( $lastResponse['error']['description'] ) ? $lastResponse['error']['description'] : 'An unknown error occurred',
				];
			}
		}

		$this->set( 'response', $response );
	}

	protected function getRootDepositionID () {
		if ($this->Session->check( 'Depositions.rootDepositionID' )) {
			return $this->Session->read( 'Depositions.rootDepositionID' );
		}
		return $this->depositionID;
//		if (!$this->Session->check( 'Depositions.deposition' ))
//		{
//			$this->Session->write( 'Depositions.deposition', $this->EDDeposition->getDeposition( $this->depositionID, true ) );
//		}
//		$Deposition = $this->getDepositionFromLogin();
//		if ($Deposition === null)
//		{
//			$authUser = $this->Session->read( 'Auth.User' );
//			$this->log( 'Logout because no deposition data from getDepositionFromLogin(). userID:' . $authUser['ID'] . ' username: ' . $authUser['username'] . ' name:' . $authUser['firstName'] . ' ' . $authUser['lastName'], 'webapp' );
//			$this->redirect( '/users/logout' );
//		}
//
//		$parentDepositionID = intval( $datacore['parentID'] );
//		$rootDepositionID = $parentDepositionID > 0 ? $parentDepositionID : $this->depositionID;
//		$this->Session->write( 'Depositions.rootDepositionID', $rootDepositionID );
//
//		return $rootDepositionID;
	}

	public function witnessUpload()
	{
		$this->layout = 'ajax';
		$this->view = 'json';

		// upload the source doc and annotations to a temp file
		$params = [
			'depositionID' => $this->depositionID,
			'sourceFileID' => $this->request->query( 'sourceFileID' ),
			'annotations' => $this->request->data( 'annotations' ),
			'iswitness' => true,
		];

		$response = $this->EDFile->annotate( $params );

		$this->set( 'response', (object)[
			'status' => 'Your annotations have been sent to the Session Leader.',
			'result' => $response
		] );
	}

	public function getKey()
	{
		$this->layout = 'ajax';
		$this->view = 'json';

		$response = $this->EDDeposition->getKey( $this->depositionID );
		$this->set( 'response', $response );
	}

	public function updateDeposition() {
		$this->layout = 'ajax';
		$this->view = 'json';

		$response = $this->EDDeposition->getDeposition( $this->depositionID, true );
		if ($response === null) {
			$authUser = $this->Session->read( 'Auth.User' );
			$this->log( 'Logout because no deposition data from API call getDeposition. userID:' . $authUser['ID'] . ' username: ' . $authUser['username'] . ' name:' . $authUser['firstName'] . ' ' . $authUser['lastName'], 'webapp' );
			$this->redirect( '/users/logout?ul=5' );
		}
		$this->Session->write( 'Depositions.deposition',  $response);
		$this->set( 'response', $response );
	}

	protected function getDepositionFromLogin() {
		$sessionDeposition = $this->Session->read( 'Depositions.deposition' );
		$sessionFolders = $this->Session->read( 'Deposition.folders' );

		$Deposition = new EDDeposition();
		$Deposition->set( (isset( $sessionDeposition->data ) ? $sessionDeposition->data['EDDeposition'] : $sessionDeposition) );

		$foldersToAdd = array();
		if (!$sessionFolders) {
			$foldersToAdd = $this->EDFolder->getFolders( $this->depositionID );
		} else {
			foreach ($sessionFolders as $folderData) {
				$folder = new EDFolder();
				$folder->set( $folderData );
				$foldersToAdd[$folderData['ID']] = $folder;
			}
		}
		$Deposition->data['folders'] = $foldersToAdd;

		$this->Session->delete( 'Depositions.folders' );
		// For Witnesses, Guests and Court Reporters, we don't delete the deposition session information.
		$authUser = $this->Session->read( 'Auth.User' );
		if ($authUser['typeID'] !== "W" && $authUser['typeID'] !== "G" && $authUser['typeID'] !== 'R') {
			$this->Session->delete( 'Depositions.deposition' );
		}

		return $Deposition;
	}

	protected function loadDatacore()
	{

		if( !$this->Session->check( 'Depositions.deposition' ) || !$this->Session->check( 'Depositions.attending' ) ) {
			if( $this->Session->read( 'Depositions.attending' ) != $this->depositionID  ) {

				$result = $this->EDDeposition->attendByDepositionID( $this->depositionID );
				if( !$result ) {
					//Check if hes owner, might be a closed depo, in that case, we allow him to connect to it to see it
					$depositionTest2 = $this->EDDeposition->getDeposition( $this->depositionID, FALSE );
					if ($this->Session->read( 'Auth.User' )['ID'] == $depositionTest2->data['EDDeposition']['ownerID'] || $this->Session->read( 'Auth.User' )['ID'] == $depositionTest2->data['EDDeposition']['createdBy']){
						//Its the owner, can access closed depos, lets check again
					}else{

						$authUser = $this->Session->read( 'Auth.User' );
						$this->log( 'Logout because no result from API call attendByDepositionID(). userID:' . $authUser['ID'] . ' username: ' . $authUser['username'] . ' name:' . $authUser['firstName'] . ' ' . $authUser['lastName'].' Debug api: '.print_r($result,true), 'webapp' );

						//Ezequiel: Removed logout error, improved this code but needs to be checked
						//$this->redirect( '/users/logout?ul=6' );
						$this->redirect( '/cases' );
					}

				}
			}
			$deposition = $this->EDDeposition->getDeposition( $this->depositionID, TRUE );
			$this->Session->write( 'Depositions.deposition', $deposition );
			$parentID = (int)(($deposition->data['EDDeposition']['parentID']) ? $deposition->data['EDDeposition']['parentID'] : $deposition->data['EDDeposition']['ID']);
			$this->Session->write( 'Depositions.attending', $parentID );
		}


		//$Deposition = $this->getDepositionFromLogin();
		$Deposition = $this->EDDeposition->getDeposition( $this->depositionID, TRUE );

		if( !$Deposition ) {
			$authUser = $this->Session->read( 'Auth.User' );
			$this->log( 'Logout because no depositionData from  getDepositionFromLogin(). userID:' . $authUser['ID'] . ' username: ' . $authUser['username'] . ' name:' . $authUser['firstName'] . ' ' . $authUser['lastName'], 'webapp' );
			$this->redirect( '/users/logout?ul=7' );
		}

		$datacore = [
			'class' => $Deposition->data['EDDeposition']['class'],
			'depositionOf' => $Deposition->data['EDDeposition']['depositionOf'],
			'depositionStatus' => $Deposition->data['EDDeposition']['statusID'],
			'depositionOwnerID' => intval( $Deposition->data['EDDeposition']['ownerID'] ),
			'parentID' => intval( $Deposition->data['EDDeposition']['parentID'] ),
			'speakerID' => intval( $Deposition->data['EDDeposition']['speakerID'] ),
			'exhibitTitle' => $Deposition->data['EDDeposition']['exhibitTitle'],
			'exhibitSubTitle' => $Deposition->data['EDDeposition']['exhibitSubTitle'],
			'exhibitDate' => $Deposition->data['EDDeposition']['exhibitDate'],
			'exhibitXOrigin' => floatval( $Deposition->data['EDDeposition']['exhibitXOrigin'] ),
			'exhibitYOrigin' => floatval( $Deposition->data['EDDeposition']['exhibitYOrigin'] ),
			'started' => $Deposition->data['EDDeposition']['started'],
			'username' => $this->Session->read( 'Auth.User' )['username']
		];

		$parentDepositionID = intval ($datacore['parentID'] );
		$this->Session->write( 'Depositions.rootDepositionID', ( $parentDepositionID > 0 ? $parentDepositionID : $this->depositionID ) );

		$authUser = $this->Session->read( 'Auth.User' );
		$datacore['userID'] = intval( $authUser['ID'] );
		$datacore['userType'] = (in_array( $authUser['typeID'], ['C','CU','RPT','JDG','OFF'] )) ? 'M' : $authUser['typeID'];

		$this->set( 'datacore', $datacore );
		$this->set( 'depositionID', $this->depositionID );
		$this->set( 'inDeposition', (intval( $Deposition->data['EDDeposition']['parentID'] ) ? intval( $Deposition->data['EDDeposition']['parentID'] ) : intval( $Deposition->data['EDDeposition']['ID'] )) );
		if (isset( $Deposition->data['EDDeposition']['exhibitHistory'] )) {
			$this->set( 'exhibitHistory', json_encode( $Deposition->data['EDDeposition']['exhibitHistory'] ) );
		} else {
			$this->set( 'exhibitHistory', '[]' );
		}
		$depo = $Deposition->data['EDDeposition'];
		unset( $depo['case'], $depo['exhibitHistory'] );
		$this->set( 'deposition', $depo );
		return $Deposition;
	}

	// ex: $.post("/depositions/search",{"terms":"example","sessionID":1}, function( data, status ) { console.log( "search result:", data ); }, "json");

	public function search() {
		$this->layout = 'ajax';
		$this->view = 'json';

		$terms = $this->request->data( 'terms' );
		$sessionID = $this->request->data( 'sessionID' );
		$options = $this->request->data( 'options' );

		$response = $this->EDSearch->search( $terms, $sessionID, $options );
		$this->set( 'response', $response );
	}
}
