<?php

App::uses( 'BaseAuthenticate', 'Controller/Component/Auth' );
App::uses( 'EDModel', 'Model' );

/**
 * An authentication adapter for AuthComponent. Provides the ability to authenticate using POST
 * data. Can be used by configuring AuthComponent to use it via the AuthComponent::$authenticate setting.
 *
 * {{{
 *	$this->Auth->authenticate = array(
 *		'Form' => array(
 *			'scope' => array('User.active' => 1)
 *		)
 *	)
 * }}}
 *
 * When configuring FormAuthenticate you can pass in settings to which fields, model and additional conditions
 * are used. See FormAuthenticate::$settings for more information.
 *
 * @package       Cake.Controller.Component.Auth
 * @since 2.0
 * @see AuthComponent::$authenticate
 */
class AppAuthenticate extends BaseAuthenticate {

	public function __construct( ComponentCollection $collection, $settings )
	{
		parent::__construct( $collection, $settings );
		$this->EDModel = ClassRegistry::init( 'EDModel' );
	}

/**
 * Checks the fields to ensure they are supplied.
 *
 * @param CakeRequest $request The request that contains login information.
 * @param string $model The model used for login verification.
 * @param array $fields The fields to be checked.
 * @return boolean False if the fields have not been supplied. True if they exist.
 */
	protected function _checkFields( CakeRequest $request, $model, $fields )
	{
		if (empty( $request->data[$model] )) {
			return false;
		}
		foreach( array( $fields['username'], $fields['password'] ) as $field ) {
			$value = $request->data( "{$model}.{$field}" );
			if (empty( $value ) || !is_string( $value )) {
				return false;
			}
		}
		return true;
	}

/**
 * Authenticates the identity contained in a request. Will use the `settings.userModel`, and `settings.fields`
 * to find POST data that is used to find a matching record in the `settings.userModel`. Will return false if
 * there is no post data, either username or password is missing, or if the scope conditions have not been met.
 *
 * @param CakeRequest $request The request that contains login information.
 * @param CakeResponse $response Unused response object.
 * @return mixed False on login failure. An array of User data on success.
 */
	public function authenticate( CakeRequest $request, CakeResponse $response )
	{
		if( $request->url !== 'members' ) {
			return false;
		}
		$userModel = $this->settings['userModel'];
		list(, $model) = pluginSplit( $userModel );

		$fields = $this->settings['fields'];
		if( !$this->_checkFields( $request, $model, $fields ) ) {
			return false;
		}

		$EDModel = ClassRegistry::init($userModel);
		$result = $EDModel->memberLogin( array( 'conditions'=>$request->data[$model] ) );
		if ($result && isset( $result[$model] )) {
			//$EDModel::$lastResponse = $result[$model];
			if (isset( $result[$model]['user'] )) {
				return $result[$model]['user'];
			}
		}
		return false;
	}

}
