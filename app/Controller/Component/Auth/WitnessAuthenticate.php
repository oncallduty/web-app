<?php

App::uses( 'BaseAuthenticate', 'Controller/Component/Auth' );
App::uses( 'EDModel', 'Model' );

/**
 * An authentication adapter for AuthComponent. Provides the ability to authenticate using POST
 * data. Can be used by configuring AuthComponent to use it via the AuthComponent::$authenticate setting.
 *
 * {{{
 *	$this->Auth->authenticate = array(
 *		'Form' => array(
 *			'scope' => array('User.active' => 1)
 *		)
 *	)
 * }}}
 *
 * When configuring FormAuthenticate you can pass in settings to which fields, model and additional conditions
 * are used. See FormAuthenticate::$settings for more information.
 *
 * @package       Cake.Controller.Component.Auth
 * @since 2.0
 * @see AuthComponent::$authenticate
 */
class WitnessAuthenticate extends BaseAuthenticate {

	public function __construct( ComponentCollection $collection, $settings )
	{
		parent::__construct( $collection, $settings );
		$this->EDModel = ClassRegistry::init( 'EDModel' );
	}

/**
 * Authenticates the identity contained in a request. Will use the `settings.userModel`, and `settings.fields`
 * to find POST data that is used to find a matching record in the `settings.userModel`. Will return false if
 * there is no post data, either username or password is missing, or if the scope conditions have not been met.
 *
 * @param CakeRequest $request The request that contains login information.
 * @param CakeResponse $response Unused response object.
 * @return mixed False on login failure. An array of User data on success.
 */
	public function authenticate( CakeRequest $request, CakeResponse $response )
	{
		if( $request->url !== 'witness' ) {
			return false;
		}
		$userModel = $this->settings['userModel'];
		list(, $model) = pluginSplit( $userModel );

		// check that the API required fields have been provided
		$required_fields = ['depositionID'];
		foreach( $required_fields as $field ) {
			$value = $request->data( "{$model}.{$field}" );
			if( empty( $value ) || !is_string( $value ) ) {
				return FALSE;
			}
		}

		$EDModel = ClassRegistry::init( $userModel );
		$result = $EDModel->witnessLogin( ['conditions'=>$request->data[$model]] );

		if( $result && isset( $result[$model] ) ) {
			if( isset( $result[$model]['witnessID'] ) && $result[$model]['witnessID'] > 0 ) {
				$nameParse = explode( ' ', $result[$model]['name'] );
				return [
					'ID' => $result[$model]['witnessID'],
					'firstName' => (count( $nameParse ) >= 1 ? array_shift( $nameParse ) : ''),
					'lastName' => (count( $nameParse ) > 0 ? implode( ' ', $nameParse ) : ''),
					'typeID' => ($result[$model]['userType'] === 'TW') ? 'W' : $result[$model]['userType'],
					'clientID' => null,
					'username' => null,
				];
			}
		}

		return FALSE;
	}
}

