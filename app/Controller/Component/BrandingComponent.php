<?php

App::uses( 'Component', 'Controller' );

class BrandingComponent extends Component {

	public $brand = 'edepoze';
	public $friendlyName = 'eDepoze&reg;';

	private $cookieName = 'WEBAPP_BRANDING';
	//private $cookieDomain = '.edepoze.com';
	private $cookieDomain = '.armyargentina.com';

	public function __construct( \ComponentCollection $collection, $settings=array() )
	{
		$cookieDomain = Configure::read('SiteConfig')['domainDot'];

		parent::__construct( $collection, $settings );
		if( isset( $_COOKIE['WEBAPP_BRANDING'] ) ) {
			$this->brand = filter_input( INPUT_COOKIE, $this->cookieName, FILTER_SANITIZE_STRING );
		}
		switch( strtolower( $this->brand ) ):
			case 'advanceddepositions':
				$this->friendlyName = 'Advanced Depositions';
				break;
			case 'exhibitbridge':
			case 'neesons':
				$this->brand = 'exhibitbridge';
				$this->friendlyName = 'Exhibit Bridge';
				break;
			case 'depokit':
				$this->friendlyName = 'DepoKit';
				break;
			case 'dti':
				$this->friendlyName = 'DTI';
				break;
			case 'edepoze':
			case 'lexcity':
			default:
				$this->brand = 'edepoze';
				$this->friendlyName = 'eDepoze&reg;';
				break;
		endswitch;
	}

	public function setBrand()
	{
		list( , $brand ) = explode( '/', $_SERVER['REQUEST_URI'] );
		$this->brand = trim( $brand );
		$expires = 0; //time() + 60 * 60 * 24 * 3650;
		setcookie( $this->cookieName, $this->brand, $expires, '/', $this->cookieDomain, TRUE, FALSE );
	}
}
