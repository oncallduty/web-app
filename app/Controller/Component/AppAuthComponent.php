<?php

App::uses( 'AuthComponent', 'Controller/Component' );

class AppAuthComponent extends AuthComponent
{
	public function identify( CakeRequest $request,  CakeResponse $response )
	{
		if (empty( $this->_authenticateObjects )) {
			$this->constructAuthenticate();
		}

		foreach ($this->_authenticateObjects as $auth) {
			$result = $auth->authenticate( $request, $response );
			if (!empty( $result ) && is_array( $result )) {
				return $result;
			}
		}
		return false;
	}

	public function login( $user=NULL )
	{
		$this->_setDefaults();

		if (empty( $user )) {
			$user = $this->identify( $this->request, $this->response );
		}

		if ($user) {
			$this->Session->renew();
			$this->Session->write( self::$sessionKey, $user );
		}
		return $this->loggedIn();
	}
}