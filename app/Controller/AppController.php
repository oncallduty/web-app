<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses( 'Controller', 'Controller' );
App::uses( 'AppAuthComponent', 'Controller/Component' );
App::uses( 'AppAuthenticate', 'Controller/Component/Auth' );

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	public $components = array(
		'Session',
		'Auth' => array(
			'className' => 'AppAuth',
			'userModel' => 'EDUser',
			'loginAction' => array( 'controller'=>'users', 'action'=>'signin' ),
			'loginRedirect' => array( 'controller'=>'users', 'action'=>'index' ),
			'logoutRedirect' => array( 'controller'=>'users', 'action'=> 'index' ),
			'authError' => 'Error: Unauthorized',
			'authorize' => array( 'Controller' ),
			'authenticate' => array( 'App'=>array( 'userModel'=>'EDUser' ) ),
		),
	);

	public function isAuthorized( $user )
	{
		if( $user && (int)$user['ID'] ) {
			return TRUE;
		}
		return FALSE;
	}

	public function beforeFilter() {
		$this->set( 'title_for_layout', $this->Branding->friendlyName );
		//$this->Auth->authenticate = array( 'App'=>array( 'userModel'=>'EDUser' ) );
		//$this->Auth->userModel = 'EDUser';

		$authUser = $this->Session->read( 'Auth.User' );
		if ($authUser){
			if (!$this->Session->check( 'Auth.UserLog' )){
				$authUserLog = rand(10000,99999).' > '.$authUser['ID'].' ) ';
				$this->Session->write('Auth.UserLog',$authUserLog);
			}else{
				$authUserLog = $this->Session->read('Auth.UserLog');
			}

			$this->set('authUserLog',$authUserLog);
		}else{
			$this->set('authUserLog','');
		}



	}

	public function index() {
		$pass = $this->request->params['pass'];

		if ($pass) {
			$action = $pass['0'];

			if (method_exists($this, $action) ) {
				$this->$action();
			} else {
				$this->redirect('/');
			}

			return;
		}
	}
}
